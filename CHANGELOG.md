# Changelog

All notable changes to [ppr_prodige_admincarto](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto) project will be documented in this file.

## [4.4.37](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.36...4.4.37) - 2024-09-12

## [4.4.36](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.35...4.4.36) - 2024-05-16

## [4.4.35](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.34...4.4.35) - 2024-04-19

## [4.4.34](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.33...4.4.34) - 2024-04-19

## [4.4.33](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.32...4.4.33) - 2024-03-13

## [4.4.32](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.31...4.4.32) - 2024-02-20

## [4.4.31](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.30...4.4.31) - 2023-12-13

## [4.4.30](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.29...4.4.30) - 2023-11-23

## [4.4.29](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.28...4.4.29) - 2023-11-14

## [4.4.28](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.27...4.4.28) - 2023-11-14

## [4.4.27](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.26...4.4.27) - 2023-11-14

## [4.4.26](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.25...4.4.26) - 2023-11-14

### Changed

- add Timestamp to import module

## [4.4.25](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.24...4.4.25) - 2023-11-13

## [4.4.24](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.23...4.4.24) - 2023-11-13

## [4.4.23](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.22...4.4.23) - 2023-10-26

### Fix

- geocoding with BAN

## [4.4.22](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.21...4.4.22) - 2023-10-19

## [4.4.21](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.20...4.4.21) - 2023-10-19

## [4.4.20](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.19...4.4.20) - 2023-10-19

### Fix

- clean wxs files

## [4.4.19](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.18...4.4.19) - 2023-10-13

## [4.4.18](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.17...4.4.18) - 2023-09-21

## [4.4.17](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.16...4.4.17) - 2023-09-21

## [4.4.16](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.15...4.4.16) - 2023-09-21

## [4.4.15](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.14...4.4.15) - 2023-07-18

## [4.4.14](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.13...4.4.14) - 2023-07-18

## [4.4.14](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.13...4.4.14) - 2023-07-18

## [4.4.13](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.12...4.4.13) - 2023-06-29

## [4.4.12](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.11...4.4.12) - 2023-06-23

## [4.4.11](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/5.0.28...4.4.11) - 2023-06-07

## [5.0.28](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.10...5.0.28) - 2023-06-06

## [4.4.10](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.9...4.4.10) - 2023-05-23

## [4.4.9](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.8...4.4.9) - 2023-05-09

## [4.4.8](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.7...4.4.8) - 2023-04-28

## [4.4.7](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.6...4.4.7) - 2023-04-28

## [4.4.6](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.5...4.4.6) - 2023-04-06

## [4.4.5](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.4...4.4.5) - 2023-04-06

## [4.4.4](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.3...4.4.4) - 2023-04-05

## [4.4.3](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.2...4.4.3) - 2023-04-05

## [4.4.2](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.1...4.4.2) - 2023-03-30

## [4.4.1](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc53...4.4.1) - 2023-03-15

## [4.4.0_rc53](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc52...4.4.0_rc53) - 2023-02-24

## [4.4.0_rc52](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc51...4.4.0_rc52) - 2023-02-06

## [4.4.0_rc51](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc50...4.4.0_rc51) - 2023-01-26

## [4.4.0_rc50](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc49...4.4.0_rc50) - 2023-01-26

## [4.4.0_rc49](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc48...4.4.0_rc49) - 2023-01-26

## [4.4.0_rc48](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc47...4.4.0_rc48) - 2023-01-24

## [4.4.0_rc47](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc46...4.4.0_rc47) - 2023-01-10

## [4.4.0_rc46](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc45...4.4.0_rc46) - 2023-01-10

## [4.4.0_rc45](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc44...4.4.0_rc45) - 2023-01-10

## [4.4.0_rc44](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc43...4.4.0_rc44) - 2023-01-10

## [4.4.0_rc43](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc42...4.4.0_rc43) - 2022-12-19

## [4.4.0_rc42](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc41...4.4.0_rc42) - 2022-12-19

## [4.4.0_rc41](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc40...4.4.0_rc41) - 2022-12-19

## [4.4.0_rc40](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc39...4.4.0_rc40) - 2022-12-19

## [4.4.0_rc39](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc38...4.4.0_rc39) - 2022-12-19

## [4.4.0_rc38](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc37...4.4.0_rc38) - 2022-12-08

## [4.4.0_rc37](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc36...4.4.0_rc37) - 2022-12-08

## [4.4.0_rc36](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc35...4.4.0_rc36) - 2022-12-08

## [4.4.0_rc35](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc34...4.4.0_rc35) - 2022-12-08

## [4.4.0_rc34](https://gitlab.adullact.net/prodige/ppr_prodige_admincarto/compare/4.4.0_rc33...4.4.0_rc34) - 2022-12-08

