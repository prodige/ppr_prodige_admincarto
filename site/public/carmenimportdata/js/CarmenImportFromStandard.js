/**
 * 
 */
function _CarmenImportFromStandard(){
  ////////////////////// VARIABLE //////////////////////////////////////////////
  /** */
  var _formConfigImport = null;

  /** */
  var _formChooseConfig = null;
  
  /** */
  var _metadata_id = null; 
  
  /** */
  var _table_name = null; 
  
  /** */
  var _url_back = null;

  /** */
  var _data_type = null;

  ///////////////////////////// OBJECT /////////////////////////////////////
  
  /** Type de config */
  var _storeConfigChoose = Ext.create('Ext.data.Store', {
    fields: ['value', 'name'],
    data : [
      {"value":"fromStandard", "name":"Création à partir d'un standard"},
      {"value":"default", "name":"Définition complète de la couche"},
    ]
  });

  /** Liste des projection */
  var _storeProjection = Ext.create('Ext.data.JsonStore', {
    fields : [{name:'text', mapping:'display'}, {name:'value', mapping:'epsg'}],
    proxy : {
      type : 'ajax',
      url : Routing.generate('prodige_getProjections')
    },
    autoLoad : true
  });

  /** Liste des standards */
  var _storeStandards = Ext.create('Ext.data.JsonStore', {
    fields : [{name:'name', mapping:'name'}, {name:'value', mapping:'id'}],
    proxy : {
      type : 'ajax',
      url : Routing.generate('carmen_tablestructure_standard')
    },
    autoLoad : true
  });

  /** Combo pour choisir une config */
  var _comboConfigChoose = {
    fieldLabel: 'Mode',
    store: _storeConfigChoose,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    //value: 'default'
  };

  /** Combo pour choisir une projection */
  var _comboProjection = {
    xtype: 'combobox',
    name : 'projection',
    fieldLabel : 'Projection',
    store : _storeProjection,
    value : PRODIGE_DEFAULT_EPSG,
    queryMode : 'local', //important
    allowBlank : false,
    editable : false,
    valueField : 'value',
    displayField : 'text'
  }

  /** Combo pour choisir le standard */
  var _comboStandards = {
    xtype: 'combobox',
    name : 'standardId',
    fieldLabel : 'Standard',
    store : _storeStandards,
    queryMode : 'local', //important
    allowBlank : false,
    editable : false,
    valueField : 'value',
    displayField : 'name'
  }

  ///////////////////////////// FONCTION ////////////////////////////////////

  /** */
  function getConfig(){
    return {
      renderTo : 'form_panel_to_render', // id for a html div
      title: 'Simple Form',
      width : '50%',
      x : '50%',
      align : 'center',
      heigth : 150,
      bodyPadding : 10,
      renderTo : 'form_panel_to_render', // id for a html div
      fieldDefaults : {
        labelWidth : 200,
        width : '100%',
        labelAlign : 'right',
        labelSeparator : ''
      },
      defaultType: 'textfield',
      items: [],
      buttons: []
    }
  }

  /** */
  function destroyForm(form){
    if( form && typeof form.destroy === 'function'){
      form.destroy();
    }
  }

  /** Interface pour l'import de donnée depuis un standard*/
  function showImportFromStandard(){
    var config = getConfig();
    // ajout du titre
    config.title = "Création à partir d'un standard";

    // ajout des items
    config.items = [
      _comboStandards, 
      ,{ name: 'prefix',  fieldLabel : "Préfixe des tables",  allowBlank: true },
      ,{ name: 'suffix',  fieldLabel : "Postfixe des tables", allowBlank: true},
      ,{ name: 'schema',  fieldLabel : "Nom du schéma en base de données",   allowBlank: false },
      ,{ name: 'metadataId',  fieldLabel : "metadataId", allowBlank: false, id: "metadataIdTextField", hidden: true},
      _comboProjection
    ];

    // ajout de l'url 
    config.url = Routing.generate('carmen_tablestructure_createFromStandard');

    // Ajout des boutons
    config.buttons = [{
      xtype: 'button',
      text: 'Retour',
      handler: function() {
        destroyForm(_formConfigImport);
        showFormToChooseConfig();
      }
    },{
      xtype: 'button',
      text: 'valider',
      handler: function(){
        var form = this.up('form').getForm();
        if (form.isValid()) {
          form.submit({
            success: function(form, action) {
              Ext.Msg.alert('Success', 'Création des données réussie');
            },
            failure: function(form, action) {
              try{
                Ext.Msg.alert('Failed', JSON.parse(action.response.responseText).msg );
              }catch(e){
                console.error(e);
                Ext.Msg.alert('Failed', "Erreur pendant l'analyse" );
              } 
            }
          });
        }
      }
    },{
      text : "Fermer",
      xtype: 'button',
      handler : function(){
        window.close();
      }
    }],
  
    // Création du formulaire
    _formConfigImport = Ext.create('Ext.form.Panel', config);

    // Pour envoyer l'id du metadata au serveur
    var metadataIdTextField = _formConfigImport.getComponent('metadataIdTextField');
    if(metadataIdTextField){
      metadataIdTextField.setValue( _metadata_id );
    }
    
    console.log(metadataIdTextField);


  }

  /** Interface pour choisir le type d'import de donnée */
  function showFormToChooseConfig(){
    var config = getConfig();
   
    // création du combo
    var comboInstance = Ext.create('Ext.form.ComboBox', _comboConfigChoose);

    // Titre du formulaire
    config.title = "Choix du type d'import";

    // Choix des items
    config.items = [comboInstance];

    // Bouton du formulaire
    config.buttons = [{
      xtype: 'button',
      text: "valider",

      handler: function(){
        if(comboInstance.getValue() ===  'fromStandard'){
          destroyForm(_formConfigImport);
          showImportFromStandard();
        }
        else if(comboInstance.getValue() ===  'default') {
          destroyForm(_formConfigImport);
          console.log(_data_type);
          init(_metadata_id, _table_name, _url_back, _data_type);
        }
      }
    }];

    _formConfigImport = Ext.create('Ext.form.Panel', config);
  }

  /** */
  function run(metadata_id, table_name, url_back, data_type){
    console.log("Run Import From Standard");

    _metadata_id = metadata_id; 
    _table_name = table_name; 
    _url_back = url_back;
    _data_type = data_type;
    console.log('run', _data_type)
    showFormToChooseConfig();
  }
  
  return {
    run: run
  }
};

/**
 * 
 */
var CarmenImportFromStandard = (function () {
  var _instance;

  function createInstance() {
    var carmenImportFromStandard = new _CarmenImportFromStandard();
    return carmenImportFromStandard;
  }

 
  return {
    getInstance: function () {
      if (!_instance) {
        _instance = createInstance();
      }

      return _instance;
    }
  };

})();

