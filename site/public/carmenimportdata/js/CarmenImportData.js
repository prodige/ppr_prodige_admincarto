Ext.onReady(
  function connectToCatalogue(){
    Ext.getBody().appendChild({
      tag    : 'iframe', 
      src    : Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "")+"/prodige/connect?casuser="+Carmen.user.username, 
      onload : 'Ext.get(this).destroy();' 
    });
  }
);

CarmenImportData = window.CarmenImportData || {};
CarmenImportData.verifyIntegrity = true;

function positionMarkLibrary(){/*Marqueur de position pour le code global*/}
/**
 * Liste des fonctions appliquées sur les composants 
 * @type Object
 */
CarmenImportData._library_ =  {
  afterLayerAddToWebService : window.afterLayerAddToWebService = function(pk_couche_donnees, data, window){
    var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
    if ( !store ) {if ( window ) window.close();return;};
    var record = store.findRecord('pk_couche_donnees', pk_couche_donnees);
    if ( !record ) {if ( window ) window.close();return;};
    record.set(data, {commit:true});
    if ( window ) window.close();
  },
  /*FORM GRID VIEW*/
  timestampRenderer: function(val) {
    return val;
  },
  
  ouiNonrenderer: function(val, meta, record) {
    if ( record instanceof CoucheDonnees )
      return (!!val ? 'Oui' : 'Non');
    return val;
  },
  
  importZIP : function(){
    var formGrid = this;
    var windowClass = "Ext.ux.FileBrowserWindow";
    var cmpId = windowClass.replace(/\./g, '-');
    var window = Ext.getCmp(cmpId) 
      || Ext.create(windowClass, {
        id: cmpId,
        selectExtension: FILETYPES['ZIP'].selectExtension,
        uploadExtension: FILETYPES['ZIP'].uploadExtension,
        dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
        defaultPath: '/Root/Publication/',
        relativeRoot: '/Root/Publication/temp',
        listeners: {  
          selectedfile: CarmenImportData.library.readSelectedArchive,
          scope : formGrid
        }
      });
    window.show();
  
  },
  
  onSelectionChange: function(model, td, cellIndex, rec) {
    var formGrid = this;
    var form = formGrid.getSubForm();
    if ( rec && rec instanceof CarmenImportData.model.Metadata ) return;
    if (rec && form) {
      if ( form.getRecord() && form.getRecord()!=rec && form.isDirty()  ){
        Ext.Msg.confirm(
          'Changement de couche éditée - Sauvegarde avant remplacement'
        , 'Une couche est déjà en cours d\'édition.' +
          '<br>Voulez-vous sauvegarder sa configuration avant de la remplacer par la couche sélectionnée ?'
        , function(btn){
            if ( btn=='yes' ){
              if ( !CarmenImportData.library.saveLayerConfiguration.call(formGrid) ) return;
            } else {
              CarmenImportData.library.cancelLayerConfiguration.call(formGrid);
            }
            CarmenImportData.library.loadLayer.call(formGrid, rec);
          }, formGrid);
        
      } else {
        CarmenImportData.library.loadLayer.call(formGrid, rec);
      }
    }
  },
  
  createLayerOnMetadata : function(grid, rowIndex, colIndex, item, evt, record, htmlRow){
    var formGrid = this;
    return CarmenImportData.library.createLayer.call(formGrid, record, OPEN_SERVER_BROWSER);
  },
  
  
  createLayer : function(parentNode, actionOnCreate){
    var formGrid = this;
    var grid = formGrid.getGrid(),
        store = grid.getStore(),
        prefix = 'Couche de données n°';
    parentNode = (parentNode instanceof Ext.data.TreeModel ? parentNode : store.getRootNode());
    var childCount = parentNode.childNodes.length;
    var maxCount = childCount;
    parentNode.eachChild(function(record){
      var couchd_nom = record.get('couchd_nom');
      if ( couchd_nom.indexOf(prefix)!=-1 ){
        maxCount = Math.max(maxCount, parseInt(couchd_nom.replace(prefix, '')));
      }
    });
    var title = (parentNode.isRoot() ? (Carmen.metadata ? Carmen.metadata.metadata_title : 'Couche ') : parentNode.get('nom'));
    
    /* hil : layerType = Modele relationnel, le store du formGrid sera rempli par le nom des tables du fichier sql à importer ... */
    if( formGrid.layertype.toUpperCase() != 'MODELE' ) {
      var record = Ext.create('CarmenImportData.model.CoucheDonnees', {
        couchd_nom : title + (childCount==0 ? '' : ' - Donnée n°'+(maxCount+1))
      });

      parentNode.appendChild([record]);
      parentNode.expand();
      grid.getView().focusRow(record);
    }

    !formGrid.getLayer() && CarmenImportData.library.loadLayer.call(formGrid, record, actionOnCreate);
  },
  
  loadLayer : function(record, actionOnCreate){
    var formGrid = this;
    formGrid.getSubForm().up('panel').setDisabled(record===null);
    formGrid.getSubForm().setDisabled(record===null);

    var res = (record ? formGrid.getSubForm().loadRecord(record) : !(formGrid.getSubForm().getForm()._record=null));
    CarmenImportData.library.setLayerType.call(formGrid, (record ? record.getLayerType() : formGrid.layertype), true);
    formGrid.fireEvent('recordloaded');
    if ( record || formGrid.layertype.toUpperCase() == 'MODELE' ){
      if ( actionOnCreate==OPEN_SERVER_BROWSER ){
        formGrid.down('[name=file_source_button]').openServerBrowser( ( formGrid.layertype.toUpperCase() == 'MODELE' ? CarmenImportData.library.importSelectedSqlFile : null ) );
      }
      if ( actionOnCreate==OPEN_CLIENT_BROWSER ){
        formGrid.down('[name=file_source_button]').openClientBrowser( ( formGrid.layertype.toUpperCase() == 'MODELE' ? CarmenImportData.library.importSelectedSqlFile : null ) );
      }
    }

    /*console.log(record);

    // set cgu text in ckeditor
    var cgumsg = record.get('couchd_cgu_message');
    var cgueditor = Ext.getCmp("cgu_editor");
    cgueditor.setValue(cgumsg);
    cgueditor.msg = cgumsg;*/

    return res;
  },
  
  deleteLayer : function(grid, rowIndex, colIndex, item, evt, record, htmlRow){
    var formGrid = this;
    var store = grid.getStore();
    var name = '"'+record.get('couchd_nom')+'"'+ (record.get('couchd_emplacement_stockage') ? ' ['+record.get('couchd_emplacement_stockage')+']' : record.get('file_source'));
    Ext.Msg.confirm("Suppression d'une couche", "Confirmez-vous la suppression de la couche "+name+' ?', function(btn){
      if (btn!="yes") return;
      if ( Ext.Number.from(record.get('pk_couche_donnees'), 0)==0 ){
        if ( formGrid.getLayer()===record ) CarmenImportData.library.cancelLayerConfiguration.call(formGrid);
        record.remove();
        grid.refresh();
        return;
      }
      
      grid.mask('Suppression en cours...');
      
      Ext.Ajax.request({
        method : 'GET',
        cors: true,
        crossDomain: true,
        withCredentials : true,
        timeout: 500000,
        type : 'json',
        url : Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "")+ 
              "/geosource/dellayersfrommap" +
              "/"+ record.get('couchd_type_stockage'),
        params : {
          couche : (record.get('couchd_emplacement_stockage') || encodeURIComponent(record.get('file_source')))
        },
        success : function(response){
          if ( !response || !response.responseText ) {grid.unmask();return};
          response = Ext.decode(response.responseText);
          if ( response.sucess ){
            
            Ext.Ajax.request({
              method : 'GET',
              cors: true,
              crossDomain: true,
              withCredentials : true,
              type : 'json',
              url : Routing.generate('carmen_ws_deletelayer', {
                fmeta_id : Carmen.metadata.id,
                pk_couche_donnees : record.get('pk_couche_donnees')
              }),
              success : function(response){
                grid.unmask();
                if ( !response || !response.responseText ) return;
                response = Ext.decode(response.responseText);
                if ( response.success ){
                  if ( formGrid.getLayer()===record ) CarmenImportData.library.cancelLayerConfiguration.call(formGrid);
                  record.remove();
                  grid.refresh();
                } else {
                  Ext.Msg.alert('Echec de suppression de la couche '+name, response.message);
                };
              },
              failure : function(){
                grid.unmask();
              }
            });
            
          } else {
            Ext.Msg.alert('Echec de suppression de la couche '+name, response.message);
          }
        },
        failure : function(){
          grid.unmask();
        }
      });

    });
  },

  geocodageInverse : function(grid, rowIndex, colIndex, item, evt, record, htmlRow){
    var name = '"'+record.get('couchd_nom')+'"'+ (record.get('couchd_emplacement_stockage') ? ' ['+record.get('couchd_emplacement_stockage')+']' : record.get('file_source'));

    Ext.Msg.confirm("Géocodage inverse", "Confirmez-vous l'application du géocodage inverse pour "+name+'? Ce process va créer des colonnes d\'adressage et remplir leur contenu par une opération de géoocodage inverse (API BAN).', function(btn){
      if (btn!="yes") return;
      
      grid.mask('Import en cours...');

      var endFunction = function(){
        grid.unmask();
      };
            
      Ext.Ajax.request({
        method : 'GET',
        cors: true,
        crossDomain: true,
        withCredentials : true,
        type : 'json',
        url : Routing.generate('carmen_ws_geocodageinverse', {
          fmeta_id : Carmen.metadata.id,
          pk_couche_donnees : record.get('pk_couche_donnees')
        }),
        success : function(response){
          if ( !response || !response.responseText ) return;
          try {
            response = Ext.decode(response.responseText);
          } catch(error){endFunction();return;}
          if ( !(response instanceof Object) ) {endFunction();return;}
          Ext.suspendLayouts();
          record.set('_status_', response.status);
          if ( response.status=="SUCCESS" ){
            record.set('color', 'green');
          } else if ( response.status=="FAILURE" ){
            record.set('color', 'red');
            if(response.import_result && response.import_result == "Fail, table name already exists") {
                Ext.Msg.alert(response.import_status);
                Ext.Ajax.abortAll();
            }
            record.set('details', response.data);
          } else {
            record.set('color', '');
          }
          record.set(response.result);
          Ext.resumeLayouts();
          record.commit();
          endFunction();
        },
        failure : function(){
          grid.unmask();
        }
      });
    });
  },
  
  onLayerLoaded : function(){
    var formGrid = this;
    var importaction, record_action;
    var record = formGrid.getLayer();
    var couchd_emplacement_stockage = Ext.ComponentQuery.query('[name=couchd_emplacement_stockage]')[0];
    if ( couchd_emplacement_stockage) {
      couchd_emplacement_stockage.originalValue = (record ? record.get('original_couchd_emplacement_stockage'): null);
      couchd_emplacement_stockage.old_value = null;
    }
    if ( !record || record.get('import_action')=="create" ){
      importaction = 'new';
      Ext.getCmp('import_action_create').setValue(true);
    } else {
      importaction = record.get('used_in_views') ? 'used' : 'created'; 
      record_action = record.get('import_action') || "update";
      if ( record.get('unique_fields').getCount()==0 ){
        record_action = "replace";
      }
      Ext.getCmp('import_action_'+record_action).setValue(true);
    }
    /*hide form field if it is not listed for the current record state (new/created) */
    Ext.each(formGrid.query('[importaction]'), function(field){
      if ( field.checkImportAction ) {
        field.checkImportAction(importaction)
      } else {
        field.setVisible( !field.importaction || field.importaction.indexOf(importaction)!=-1 );
      }
    });
    if ( formGrid.getSubForm().up().rendered ){
      if ( record ) {
        
        formGrid.getGrid().setDisabled(true);
        
        formGrid.up("form-importdata").setScrollY(formGrid.getSubForm().down('button:last').getY());
        formGrid.getSubForm().down('field').focus();
        formGrid.getSubForm().up().setTitle('Paramétrage - '+record.get('couchd_nom')+(" ["+record.getTypePath().join(' / ')+"]"));

      } else {
        formGrid.getGrid().setDisabled(false);
        formGrid.up("form-importdata").setScrollY(0);
        formGrid.getSubForm().up().setTitle(DEFAULT_TITLE);
      }
    }
    
    if ( Ext.getCmp('showData') ) Ext.getCmp('showData').close();
      
    var fullPathName = formGrid.down('[name=file_name]').getValue();
    var relativePath = formGrid.down('[name=file_source]').getValue();
    var fileSheet = (formGrid.down('[name=file_sheet]').isDisabled() ? null : formGrid.down('[name=file_sheet]').getValue());
    if ( record && record.get('autoLoad') && fullPathName && relativePath ){
      CarmenImportData.library.readSelectedFile.call(formGrid, null, null, null, fullPathName, relativePath, null, fileSheet || null);
    }
  },
  
  
  
  /**
   * Action au niveau de la métadata d'entrée
   * @param {} wind
   * @param {} fileRecord
   * @param {} fileName
   * @param {} fullPathName
   * @param {} relativePath
   * @param {} absolutePath
   */
  readSelectedArchive : function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath){
    var formGrid = this;
    
    wind.close();
    formGrid.mask('Analyse du fichier ZIP '+fullPathName+" en cours...");
    
    Ext.Ajax.request({
      method : 'GET',
      type : 'json',
      timeout : 24*60*60*1000,
      url : Routing.generate('carmen_ws_readzip', {
        uuid : Carmen.metadata.uuid,
        layertype : formGrid.up().down('[name=metadata_layertype]').getValue().toUpperCase(), 
        filezip : encodeURIComponent(relativePath) 
      }),
      success : function(response){
        formGrid.unmask();
      	if ( !response || !response.responseText ) return;
        response = Ext.decode(response.responseText);
        if ( !Ext.isArray(response.children) ) return;
        var root = formGrid.getGrid().getStore().getRoot();
        response.children.forEach(function(child){
          root.appendChild(root.createNode(child));
        });
      },
      failure : function(){
        formGrid.unmask();
      }
    });
  },
  
  setLayerType : function(layertype, byRecord){
    var formGrid = this;
    formGrid.layertype = layertype;
    
    var passed = [];
    CarmenImportData.library.onChangeLayertype.call(formGrid, layertype, passed);
    
    Ext.each(Ext.ComponentQuery.query('[exclude_layertype], [including_layertype], [checkLayerType]', formGrid), function(component){
    	if ( passed.indexOf(component.id)!=-1 ) {return;}
    	
      //hide the form field if it is listed for the excluded layertype 
      var excluded = (component.exclude_layertype ? (Ext.isArray(component.exclude_layertype) ? component.exclude_layertype : [component.exclude_layertype]) : null);
      var included = (component.including_layertype ? (Ext.isArray(component.including_layertype) ? component.including_layertype : [component.including_layertype]) : null);
      !component.is('hidden') && console.log(layertype, component.xtype, component.dataIndex || component.name || component.text, component.id, "setVisible",  (!excluded || excluded.indexOf(layertype.toLowerCase())==-1) || (included && included.indexOf(layertype.toLowerCase())!=-1) );
      !component.is('hidden') && component.setVisible( (!excluded || excluded.indexOf(layertype.toLowerCase())==-1) || (included && included.indexOf(layertype.toLowerCase())!=-1) );
      if ( Ext.isDefined(component.allowBlank) ){
        if ( !Ext.isDefined(component.default_allowBlank) ) component.default_allowBlank = component.allowBlank; 
        component.allowBlank = ( !excluded || excluded.indexOf(layertype.toLowerCase())==-1 ? component.default_allowBlank : true );
        //console.log(layertype, component.xtype, component.dataIndex || component.name || component.text, component.id, "allowBlank",  ( !excluded || excluded.indexOf(layertype.toLowerCase())==-1 ? component.default_allowBlank : true ));
      }
      
      if ( component.checkLayerType ){component.checkLayerType(layertype);/*console.log(layertype, component.xtype, component.dataIndex || component.name || component.text, component.id, "checkLayerType()");*/}
    });
    if ( !byRecord ){
      formGrid.getSubForm().setDisabled(true);
      formGrid.getGrid().getStore().removeAll();
      formGrid.getSubForm().reset();
      formGrid.fireEvent('recordloaded');
    }
    return formGrid;
  },
  
  onChangeLayertype : function (layertype, passed){
    ONCHANGE_LAYERTYPE = 'marker';
    
    var setVisible = 'setVisible';
    var setDisabled = 'setDisabled';
    var allowBlank = 'allowBlank';
    var checkLayerType = 'checkLayerType';
    var setText = 'setText';
    var setFieldLabel = 'setFieldLabel';
    var submitValue = 'submitValue';
    var setValue = 'setValue';
    
    var formGrid = this;
    var setState = function(selector, action, state){
    	var elt = Ext.getCmp(selector.replace('#', '')) || formGrid.down(selector);
    	if ( !elt ) return;
    	passed.push(elt.id);
    	if ( Ext.isFunction(elt[action]) ) elt[action](state);
    	else elt[action] = state;
    }
  
    var allowedTypes = function(){
      var types = Array.prototype.splice.call(arguments, 0);
      return types.indexOf(layertype) != -1
    };
    var forbiddenTypes = function(){
      var types = Array.prototype.splice.call(arguments, 0);
      return types.indexOf(layertype) == -1
    };
    /*GRID CONFIG*/
    setState('treecolumn'                  , setVisible, allowedTypes('multi') );
    setState('gridcolumn[dataIndex=file_name]'  , setVisible, allowedTypes('raster') );
    setState('gridcolumn[dataIndex=stockage]'   , setVisible, forbiddenTypes('raster', 'modele') );  
    //setState('checkcolumn[dataIndex=couchd_wfs]', setVisible, forbiddenTypes('mnt', 'tabulaire', 'raster', 'modele') );
    //setState('checkcolumn[dataIndex=couchd_wms]', setVisible, forbiddenTypes('mnt', 'tabulaire', 'modele') );

    setState('#tbar_label' , setText   , (forbiddenTypes('multi') ? "Ajout d'une nouvelle donnée SIG " : "Création de métadonnées enfants et de leurs données SIG")+" : " );
    setState('#tbar_upload', setVisible, forbiddenTypes('multi') );
    setState('#tbar_select', setVisible, forbiddenTypes('multi') );
    setState('checkcolumn[dataIndex=couchd_wfs]', setVisible, forbiddenTypes('mnt', 'tabulaire', 'raster', 'modele') );
    setState('checkcolumn[dataIndex=couchd_wms]', setVisible, forbiddenTypes('mnt', 'tabulaire', 'modele') );
    
    // hil
    setState('#tbar_archive', setVisible, forbiddenTypes('modele') );

    setState('#shema_nom', setVisible, allowedTypes('modele') );
    setState('#radiogroup_mode_action', setVisible, allowedTypes('modele') );

    setState('#cb_action_db', setVisible, allowedTypes('modele') );

    setState('gridcolumn[dataIndex=status]'  , setVisible, forbiddenTypes('modele') );
    setState('actioncolumn'  , setVisible, forbiddenTypes('modele') );

    setState('cid-fieldcontainer[name=fieldcontainer_file]', setVisible, forbiddenTypes('modele') );
    setState('textfield[name=file_name]', setVisible, forbiddenTypes('modele') );
    setState('button[name=file_source_button]', setVisible, forbiddenTypes('modele') );
    setState('button[name=file_vide_button]', setVisible, forbiddenTypes('modele') );
    //

    setState('#bbar_separator', setVisible, forbiddenTypes("multi", "mnt", "tabulaire", "modele") );
    setState('#default_representation', setVisible, forbiddenTypes("multi", "mnt", "tabulaire", "modele") );

    /*FORM LEFT COLUMN CONFIG*/
    setState('cid-fieldcontainer[name=fieldcontainer_emplacement]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('cid-fieldcontainer[name=fieldcontainer_emplacement]', allowBlank, allowedTypes('raster', 'modele'));

    setState('textfield[name=couchd_emplacement_stockage]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('textfield[name=couchd_emplacement_stockage]', allowBlank, allowedTypes('raster', 'modele'));

    setState('radiogroup[name=radiogroup_import_action]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('radiogroup[name=radiogroup_import_action]', allowBlank, allowedTypes('raster', 'modele'));

    setState('#import_action_create', setVisible, forbiddenTypes('raster') );
    setState('#import_action_create', checkLayerType, layertype);

    setState('#import_action_replace', setVisible, forbiddenTypes('raster') );
    setState('#import_action_replace', checkLayerType, layertype);

    setState('#import_action_update', setVisible, forbiddenTypes('raster') );

    /*FORM RIGHT COLUMN CONFIG*/
    /**/
    setState('form radiogroup[name=file_georeferenced]', setVisible, allowedTypes('raster', 'vector', 'multi'));
    setState('form radiogroup[name=file_georeferenced]', setDisabled, !allowedTypes('raster', 'vector', 'multi', 'mnt'));
    setState('form radiogroup[name=file_georeferenced]', setFieldLabel, (
        allowedTypes('raster')
      ? 'Tuilage raster'+UNSAVED_MARK
      : 'Données tabulaires'+UNSAVED_MARK
    ));
    if ( allowedTypes('mnt') ){
      setState('form radiogroup[name=file_georeferenced]', setValue, true);
    }

    setState('[dataIndex=xField]', setVisible, allowedTypes('mnt') || this.down('form radiogroup[name=file_georeferenced]').getValue().typeExport);
    setState('[dataIndex=yField]', setVisible, allowedTypes('mnt') || this.down('form radiogroup[name=file_georeferenced]').getValue().typeExport);
    setState('[dataIndex=adrField]', setVisible, allowedTypes('mnt') || this.down('form radiogroup[name=file_georeferenced]').getValue().typeExport);
    
    setState('#fieldcontainer_file_name', checkLayerType, layertype);
    setState('#fieldcontainer_file_name', setVisible, forbiddenTypes('modele') ); // hil
    setState('fieldcontainer[name=sheets]', setVisible, forbiddenTypes('raster', 'mnt', 'modele') );
    setState('fieldcontainer[name=sheets]', checkLayerType, layertype);
    
    setState('combo[name=encoding_source]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('combo[name=encoding_source]', allowBlank, allowedTypes('raster', 'modele'));
    
    setState('combo[name=projection_cible]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('combo[name=projection_cible]', allowBlank, allowedTypes('raster', 'modele'));
    
    setState('combo[name=projection_source]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('combo[name=projection_source]', allowBlank, allowedTypes('raster', 'modele'));
    setState('combo[name=projection_source]', checkLayerType, layertype);
    
    /**/
    setState('#fieldcontainer_layerfields', setVisible, forbiddenTypes('raster', 'modele') );
    setState('cid-fieldcontainer[name=layerfields]', setVisible, forbiddenTypes('raster', 'modele') );
    setState('cid-fieldcontainer[name=layerfields]', allowBlank, allowedTypes('raster', 'modele'));

  },
  /*IMPORT DATA VIEW*/
  initialiserImportData : function(importDataView){
    return function(){
      var btn = this;
      if ( !(Carmen.metadata && Carmen.metadata.id) ) return;
      var layertype = importDataView.down('[name=metadata_layertype]');
      if ( layertype.getValue()=="join" ){
        if ( !importDataView.down('form').isValid() ) return;
        var view_name_field = importDataView.down('[name=view_name]');
        if ( !view_name_field ) return;
        var pk_couche_donnees_join = importDataView.down('[name=pk_couche_donnees_join]');
        if ( !pk_couche_donnees_join ) return;
        
        var openViewJoin = function(){
            //metadata_id, desc_vue, nom_vue, urlBack, color_theme_id, svrAdmin
            var params = {
              pk_view : Carmen.metadata.id,
              view_title : Carmen.metadata.metadata_title,
              view_name : view_name_field.getValue(),
              url_back : window.location.href,
              view_type : -4
            }
            
            var jsonParams = Ext.encode(params);
            var token = TextEncode(jsonParams);
            
            var url = Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/join/" + token ;
          
            window.location.href = url;
        };
        if ( pk_couche_donnees_join.getValue() ){
        	openViewJoin();
        	return;
        }
        Ext.Ajax.request({
          url : Routing.generate('carmen_initviewjoin', {
            uuid : Carmen.metadata && Carmen.metadata.uuid,
            view_name : view_name_field.getValue()
          }),
          method : 'POST',
          jsonData : {
              view_title : Carmen.metadata.metadata_title
          },
          success : function(response){
            if ( !response || !response.responseText ) return;
          	//console.log('success',response);
          	response = Ext.decode(response.responseText);
          	if ( response.success ){
          		pk_couche_donnees_join.setValue(response.pk_couche_donnees);
          		openViewJoin();
          	} else if ( response.message ){
          	  Ext.Msg.alert('ERREUR - Initialisation de la vue de jointure', response.message).setIcon(Ext.Msg.ERROR);
          	}
          	
          },
          failure : function(response){
            //console.log('failure',response);
          }
        })
        return;
      };
      
      
      var doAction = function(){
        var insert = !importDataView.formGrid;
        
        
        importDataView.formGrid = importDataView.formGrid || Ext.create('CarmenImportData.view.form.FormGrid', {
          buttons : CarmenImportData.library.getButtonsImport.call(importDataView)
        });
        importDataView.formGrid.on('afterrender', function(){
          CarmenImportData.library.setLayerType.call(importDataView.formGrid, layertype.getValue());
          importDataView.formGrid.loadStore();
        })
        insert && importDataView.add(importDataView.formGrid);
        layertype.setReadOnly(true);
        btn.hide();
      };
      if ( btn.confirm ){
        Ext.Msg.confirm('Initialisation des données', 'ATTENTION : Le choix du type de données est non modifiable ultérieurement.<br><br>Confirmez-vous votre choix ?', function(confirm){
          if ( confirm == "yes" ){
            doAction();
          }
        });
      } else {
        doAction();
      }
    };
  },
  
  /*VIEW ACTIONS*/
  
  getButtonsImport : function(){
    var importDataView = this;
    return [{
    	id : 'directImport',
      text : "Importer immédiatement",
      scope : importDataView,
      handler : CarmenImportData.library.directImport
    }, {
      id : 'deferImport',
      text : "Différer l'import",
      scope : importDataView,
      handler : CarmenImportData.library.deferImport
    }, {
      id : 'cancelImport',
      text : "Fermer",
      scope : importDataView,
      handler : CarmenImportData.library.cancelImport
    }]
  },
  
  changeModeActionState: function(){

    Ext.getCmp('mode_action_create').setDisabled(true);
    Ext.getCmp('mode_action_create').setValue(false);

    Ext.getCmp('mode_action_replace').setDisabled(false);

    Ext.Ajax.request({
      method : 'GET',
      url : Routing.generate('carmen_ws_checkDbExist', {
        uuid: (Carmen.metadata ? Carmen.metadata.uuid : 'uuid') 
      }),
      success : function(response){
        if ( !response || !response.responseText ) return;
        response = Ext.decode(response.responseText);
        if( response['exist'] ) {
          Ext.getCmp('shema_nom').setDisabled(true);
          Ext.getCmp('mode_action_add').setDisabled(false);
          Ext.getCmp('mode_action_add').setValue(true);
        }
        else {
          Ext.getCmp('mode_action_replace').setValue(true);
        }
      },
      failure : function(response) { /**/ }
    });

    Ext.getCmp('action_delete_db').setDisabled(false);

  },
  
  directImport : function(){
    var importDataView = this;
    var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
    if ( !store ) return;
    if ( store.getCount()==0 ) {
      Ext.Msg.alert('Import immédiat', 'Aucune donnée à importer.')
      return;
    }
    var grid = importDataView.formGrid.getGrid();
    var form = importDataView.formGrid.getSubForm().up();
    grid.getSelectionModel().deselectAll();
    var doSave = function(){
      grid.suspendEvents(false);
      grid.getSelectionModel().suspendEvents(false);
      form.up('panel').mask('Import des données en cours...');
      var index = 0;
      var allCount = store.getCount();
      store.each(function(couche, recordIndex){
       if ( !(couche instanceof CoucheDonnees) ) {index++;return;}
        //if ( !couche.get('file_source') ) return;
        var data = couche.getData({persist : true, serialize : true});
        if(data.layerfields.adrField != "" && data.layerfields.adrField != null){
          data.file_georeferenced = 2;
        }
        if(data.layerfields.xField != "" && data.layerfields.xField != null){
          data.file_georeferenced = 1;
        }
        var rowElt =grid.getView().getCell(couche, grid.down('[dataIndex=status]'));
        rowElt.mask('Import en cours...');
        var endFunction = function(){
          rowElt.unmask();
          index++;
          if ( index==allCount ){
            form.up('panel').unmask();
            grid.resumeEvents();
            grid.getSelectionModel().resumeEvents();
          }
        };
        Ext.Function.createDelayed(function(){
          Ext.Ajax.request({
            method : 'POST',
            url : Routing.generate('carmen_ws_directimport', {
              layertype : couche.getLayerType().toUpperCase(), 
              uuid : couche.getUUID() 
            }),
            timeout : 24*60*60*1000,
            jsonData : data,
            success : function(response){
              if ( !response || !response.responseText ) {endFunction();return;}
              try {
                response = Ext.decode(response.responseText);
              } catch(error){endFunction();return;}
              if ( !(response instanceof Object) ) {endFunction();return;}
              Ext.suspendLayouts();
              couche.set('_status_', response.status);
              if ( response.status=="SUCCESS" ){
                couche.set('color', 'green');
              } else if ( response.status=="FAILURE" ){
                couche.set('color', 'red');
                if(response.import_result && response.import_result == "Fail, table name already exists") {
                    Ext.Msg.alert(response.import_status);
                    Ext.Ajax.abortAll();
                }
                couche.set('details', response.data);
              } else {
                couche.set('color', '');
              }
              couche.set(response.result);
              Ext.resumeLayouts();
              couche.commit();
              endFunction();
            },
            failure : function(response){
              if ( response.timedout ){
                Ext.Msg.alert('Import interrompu', "La donnée "+couche.get('couchd_nom')+" est trop longue à importer.<br/><br/>Veuillez procéder à un import différé.");
                Ext.Ajax.abortAll();
              }
              endFunction();
            }
          });
        }, (recordIndex+1)*700)();
      });
    };

    var schemaDestination = Ext.getCmp('shema_nom').getValue();

    var doSaveDump = function() {
      var file_source = '';
      var data        = [];
      store.each(function(couche){
          //data.push(couche.getData({persist : true, serialize : true}));
          data.push( {
            couchd_nom: couche.getData().couchd_nom,
            couchd_emplacement_stockage: ( couche.getData().couchd_emplacement_stockage.indexOf('.') > 0 ? couche.getData().couchd_emplacement_stockage : schemaDestination + '.' + couche.getData().couchd_emplacement_stockage ),
            couchd_description: couche.getData().couchd_description,
            couchd_id: couche.getData().couchd_id
          } );

          if( file_source == '') {
            file_source = couche.getData().file_source;
          }

      });

      /*var initStore = function() {
        var store  = importDataView.formGrid.getGrid().getStore();
        parentNode = store.getRootNode();
        parentNode.removeAll();
        
        Ext.getCmp('shema_nom').setValue('');
      };*/

      importDataView.formGrid.mask('Transfert du schéma en cours...');

      Ext.Ajax.request({
        method : 'POST',
        url : Routing.generate('carmen_ws_directimport', {
          layertype : importDataView.formGrid.layertype.toUpperCase(),
          uuid : Carmen.metadata.uuid,
          schemaDestination: schemaDestination,
          mode: Ext.getCmp('radiogroup_mode_action').items.get(0).getGroupValue(),
          action: Ext.getCmp('cb_action_db').items.get(0).getValue() ? 'delete' : null
        }),
        timeout : 24*60*60*1000,
        jsonData : {file_source: encodeURIComponent(file_source), data: data},
        success : function(response){
          importDataView.formGrid.unmask();
          if ( !response || !response.responseText ) return;
          //initStore();
          CarmenImportData.library.changeModeActionState();
          Ext.Msg.alert('Réussi', "Import terminé avec succès.");
        },
        failure : function(response){
          importDataView.formGrid.unmask();
          if ( !response || !response.responseText ) return;
          response = Ext.decode(response.responseText);
          Ext.Msg.alert(response.error, response.description).setIcon(Ext.Msg.ERROR);
        }
      });
    }
    
    grid.expandAll(function(){

      if( importDataView.formGrid.layertype.toUpperCase() == 'MODELE' ) {
        if ( !CarmenImportData.library.saveDataConfiguration.apply(importDataView.formGrid) ) return;
        doSaveDump();
      }
      else {
        if ( importDataView.formGrid.getLayer() ){
          Ext.Msg.confirm("Donnée en cours d'édition", "Une donnée est en cours d'édition. Voulez-vous valider sa configuration actuelle ?"
          , function(btn){
              if ( btn=="yes" ){
                if ( !CarmenImportData.library.saveLayerConfiguration.apply(importDataView.formGrid) ) return;
              } else {
                CarmenImportData.library.cancelLayerConfiguration.apply(importDataView.formGrid)
              }
              doSave();
            }, importDataView)
        } else {
          doSave();
        } 
      }
    });
  },
  
  
  
    
    
  deferImport : function(){
    var importDataView = this;
    var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
    if ( !store ) return;
    if ( store.getCount()==0 ) {
      Ext.Msg.alert('Import différé', 'Aucune donnée à importer.')
      return;
    }
    var grid = importDataView.formGrid.getGrid();
    var form = importDataView.formGrid.getSubForm().up();
    grid.getSelectionModel().deselectAll();
    var doSave = function(){
      grid.suspendEvents(false);
      grid.getSelectionModel().suspendEvents(false);
      var index = 0;
      var submit = [];
      store.each(function(couche){
        //if ( !couche.get('file_source') ) return;
        submit.push(couche.getData({persist : true, serialize : true}));
      });
    
      var endFunction = function(){
        grid.resumeEvents();
        grid.getSelectionModel().resumeEvents();
      };

      Ext.Ajax.request({
        method : 'POST',
        url : Routing.generate('carmen_ws_deferimport', {
          layertype : importDataView.down('[name=metadata_layertype]').getValue().toUpperCase(), 
          uuid : Carmen.metadata.uuid 
//layertype : couche.getLayerType().toUpperCase(),
          //uuid : Carmen.metadata.uuid,
        }),
        timeout :  24*60*60*1000,
        jsonData : {couches : submit},
        success : function(response){
          if ( !response || !response.responseText ) {endFunction();return;}
          try {
            response = Ext.decode(response.responseText);
          } catch(error){endFunction();return;}
          if ( !(response instanceof Object) ) {endFunction();return;}
          store.each(function(couche, index){
            var result = response.results[index];
            if ( !Ext.isDefined(result) || !(result instanceof Object) ) return;
            couche.set(result);
            couche.commit();
          });
          endFunction();
        },
        failure : function(){
          endFunction();
        }
      });
    };

    var schemaDestination = Ext.getCmp('shema_nom').getValue();

    var doSaveDump = function() {
      var file_source = '';
      var data        = [];
      store.each(function(couche){
          //data.push(couche.getData({persist : true, serialize : true}));
          data.push( {
            couchd_nom: couche.getData().couchd_nom,
            couchd_emplacement_stockage: schemaDestination + '.' + couche.getData().couchd_emplacement_stockage,
            couchd_description: couche.getData().couchd_description
          } );
          if( file_source == '') {
            file_source = couche.getData().file_source;
          }
      });

      Ext.Ajax.request({
        method : 'POST',
        url : Routing.generate('carmen_ws_deferimport', {
          layertype : importDataView.formGrid.layertype.toUpperCase(),
          uuid : Carmen.metadata.uuid,
          schemaDestination: schemaDestination,
          mode: Ext.getCmp('radiogroup_mode_action').items.get(0).getGroupValue(),
          action: Ext.getCmp('cb_action_db').items.get(0).getValue() ? 'delete' : null
        }),
        timeout : 24*60*60*1000,
        jsonData : {file_source: encodeURIComponent( file_source ), data: data},
        success : function(response){
          if ( !response || !response.responseText ) return;
          Ext.Msg.alert('Réussi', "l'import différé a bien été enregistré.");
        },
        failure : function(response){
          if ( !response || !response.responseText ) return;
          response = Ext.decode(response.responseText);
          Ext.Msg.alert(response.error, response.description).setIcon(Ext.Msg.ERROR);
        }
      });
    }
    
    if( importDataView.formGrid.layertype.toUpperCase() == 'MODELE' ) {
      if ( !CarmenImportData.library.saveDataConfiguration.apply(importDataView.formGrid) ) return;
      doSaveDump();
    }
    else {
      if ( importDataView.formGrid.getLayer() ){
        Ext.Msg.confirm("Donnée en cours d'édition", "Une donnée est en cours d'édition. Voulez-vous valider sa configuration actuelle ?"
        , function(btn){
            if ( btn=="yes" ){
              if ( !CarmenImportData.library.saveLayerConfiguration.apply(importDataView.formGrid) ) return;
            } else {
              CarmenImportData.library.cancelLayerConfiguration.apply(importDataView.formGrid)
            }
            doSave();
          }, importDataView)
      } else {
        doSave();
      } 
    }
  },
  
  confirmBeforeClose : function(){
    return ("Voulez-vous fermer la fenêtre et perdre toutes les modifications apportées ?");
  },
  
  cancelImport : function(){
    window.close();
  },
  
  getDefaultMapButton : function(formGrid){
    return {
      text : "Paramétrer la représentation par défaut",
      id : 'default_representation',
      enable : function(){
        var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
        if ( !store ) return this;
        if ( store.find('import_action', 'update')==-1 && store.find('import_action', 'replace')==-1 )
          return this;
        return Ext.button.Split.prototype.enable.apply(this, arguments);
      },
      handler: CarmenImportData.library.editMap, scope : formGrid,
      xtype : 'splitbutton',
      menu: new Ext.menu.Menu({
          items: [
              // these will render as dropdown menu items when the arrow is clicked:
              {text: 'Paramétrer la représentation par défaut', handler: CarmenImportData.library.editMap, scope : formGrid},
              {text: 'Réinitialiser la représentation par défaut', handler: CarmenImportData.library.resetMap, scope : formGrid}
          ]
      })
    };
  },
  
  editMap : function(){
    var formGrid = this;
    if ( !Carmen.metadata ) return;
    window.open(Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/geosource/layerAddToMap/"+ Carmen.metadata.uuid);
  },
  resetMap : function(btn){
    var formGrid = this;
    if ( !Carmen.metadata ) return;
    
    btn.up('splitbutton').mask();
    Ext.Ajax.request({
      cors: true,
      crossDomain: true,
      withCredentials : true,
      url : Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/geosource/mapDelete/"+ Carmen.metadata.uuid,
      success : function(response) {
        btn.up('splitbutton').unmask();
        if ( !response || !response.responseText ) return;
        response = Ext.decode(response.responseText);
        //Ext.Msg.alert("La représentation par défaut a été réinitialisée");
        Ext.Msg.alert('Représentation par défaut', "La représentation par défaut "+(response.success ? "a été" : "n'a pas été")+" réinitialisée", function(){});
      },
      failure : function() {
        btn.up('splitbutton').unmask();
        Ext.Msg.alert('Représentation par défaut', "La représentation par défaut n'a pas été réinitialisée", function(){}).setIcon(Ext.Msg.ERROR);
      }
    });
  },
  
  changeTableName : function(button, evt, additionnalMsg){
    var formGrid = this;
    if ( !additionnalMsg ) additionnalMsg = "";
    
    var record = formGrid.getLayer();
    if ( !record ) return;
    var old_value = record.get('couchd_emplacement_stockage');
    
    var prompt = Ext.Msg.prompt('Renommage de la table de stockage', additionnalMsg+
      '<b>Attention</b> : Cette action est immédiate sur les données.' +
      '<br/><br/>' +
      'Saisissez le nouveau nom de table : ',
      function(btn, value){
        if ( btn!="ok" ) return;
        value = value.toLowerCase();
        if ( value == old_value ){
          CarmenImportData.library.changeTableName.call(formGrid, null, null, "<b>Indiquez un nouveau nom de table.</b><br/><br/>");
          return;
        }
        
        prompt.hide();
        formGrid.mask('Renommage en cours...');
        Ext.defer(function(){
          Ext.Ajax.request({
            url : Routing.generate('carmen_ws_changetablename', {
              pk_couche_donnees : Ext.Number.from(formGrid.getLayer().get('pk_couche_donnees'), -1), 
              new_tablename : value , 
              old_tablename : old_value 
            }),
            type : 'json',
            failure : function(){
              formGrid.unmask();
            },
            success : function(response){
              formGrid.unmask();
              if ( !response || !response.responseText ) return;
              response = Ext.decode(response.responseText);
              if ( !response.success ){
                Ext.Msg.alert("Changement du nom de la table de stockage", response.message).setIcon(Ext.Msg.ERROR);
                return;
              }
              Ext.Msg.alert("Changement du nom de la table de stockage", "Renommage réussi").setIcon(Ext.Msg.INFO);
              formGrid.getLayer().set('couchd_emplacement_stockage', response.tablename, {commit : true});
              formGrid.down('[name=couchd_emplacement_stockage]').setValue(response.tablename);
            }
          });
        }, 1000)
      },
      formGrid, 
      false,
      old_value
    );
      
  },
  
  verify_unicity_schema : function(textfield) {

    var value = textfield.getValue();

    var displayErrors = function() {
      var errors = textfield.getErrors(value);
      if( errors.length ) {
        textfield.markInvalid(errors);
        //textfield.getTrigger('errorSchema').setVisible(true);
      }
      else {
        textfield.clearInvalid();
        //textfield.getTrigger('errorSchema').setVisible(false);
      }
    }
    
    /* initialisation config textfield */
    textfield.existing = false;

    if (value.length && !value.match(textfield.config.regex)) {
      textfield.getTrigger('errorSchema').setVisible(true);
    } else {
      Ext.Ajax.request({
        method : 'GET',
        url : Routing.generate('carmen_ws_checkSchemaName', { 
          uuid: (Carmen.metadata ? Carmen.metadata.uuid : 'uuid') 
        }) + '?q=' + textfield.getValue(),
        success : function(response){
          if ( !response || !response.responseText ) return;
          response = Ext.decode(response.responseText);
          if( !response['valid'] ) {
            textfield.existing = true;            
          }
          displayErrors();
        },
        failure : function(response) { /**/ }
      });
    }
  },

  verify_unicity_emplacement : function(textfield){
    if ( !textfield || !textfield.isReadOnly || textfield.isReadOnly() ) return;
    var formGrid = this;
    var old_value = textfield.originalValue;
    var new_value = textfield.getValue();
    if ( !new_value ) return;
    textfield.setValue(String(textfield.getValue()).toLowerCase());
    new_value = textfield.getValue();
    var radios = Ext.ComponentQuery.query('radio[name=import_action][importaction]');

    var onSuccess = function(unique, table_exists, used_in_views){
      if ( !unique ){
        textfield.existing = true;
        textfield.markInvalid ( textfield.getErrors(new_value) );
        Ext.each(radios
        , function(radio){
            radio.setDisabled(true);
            radio.setValue(false);
          }
        );
        return;
      } 

      var importaction = "created";
      if ( !table_exists ) importaction = "new";
      if ( !used_in_views ) importaction = "used";
      Ext.each(radios
      , function(radio){
          radio.checkImportAction(importaction);
        }
      );
      if ( importaction=="new" ){
        Ext.getCmp('import_action_create').setValue(true);
      } else if ( !Ext.getCmp('import_action_replace').getValue() ){
        Ext.getCmp('import_action_update').setValue(true);
      }
    };
    if ( new_value && new_value!=textfield.old_value ){
      
      //reinit config
      textfield.existing = false;
      var errors = textfield.getErrors(new_value);
      if ( errors.length ) textfield.markInvalid(errors)
      else                 textfield.clearInvalid();
      Ext.each(radios
      , function(radio){
          radio.setDisabled(false);
        }
      );
      
      
      Ext.Ajax.request({
        async : false,
        url : Routing.generate('carmen_ws_uniquestorage', {
          pk_couche_donnees : (formGrid.getLayer ? Ext.Number.from(formGrid.getLayer().get('pk_couche_donnees'), -1) : -1), 
          couchd_emplacement_stockage : new_value 
        }),
        type : 'json',
        timeout :  24*60*60*1000,
        failure : function(){onSuccess(true, formGrid.getLayer().get('import_action'));},
        success : function(response){
          if ( !response || !response.responseText ) return;
          response = Ext.decode(response.responseText);
          onSuccess(response.unique, response.table_exists, response.used_in_views);
        }
      });
      textfield.old_value = new_value;
    }
  },
    
  fields_actions : {
    
    getShemaNom : function(formGrid) {
      return {
        shema_nom : {
         listeners : {
           beforerender : function add_specific_error(){
             var field = this;
             Ext.override(field, {
               isValid: function(new_value){
                 return field.callParent(arguments) && !field.existing;
               },
               getErrors: function(new_value){
                 var error = 'Ce nom de schéma est déjà utilisé.';
                 var errors = this.callParent(arguments) || [];
                 if ( field.existing ){
                   errors.push(error);
                 } else {
                   Ext.Array.remove(errors, error);
                 }
                 return errors;
               }
             });
           },
           blur : {
             fn : CarmenImportData.library.verify_unicity_schema,
             scope : formGrid
           }
         }
       },  
      }
    },

    getCoucheEmplacement : function(formGrid, layertype){
      return {
        couchd_emplacement_stockage : {
          checkImportAction : function(importaction){
            var field = this;
            field.setReadOnly(field.importaction.indexOf(importaction)==-1);
            field.allowBlank = !field.isVisible() || (field.importaction.indexOf(importaction)==-1);
          },
          listeners : {
            beforerender : function add_specific_error(){
              var field = this;
              Ext.override(field, {
                isValid: function(new_value){
                  return field.callParent(arguments) && !field.existing;
                },
                getErrors: function(new_value){
                  var error = 'Ce nom de table est déjà utilisé par une autre donnée.';
                  var errors = this.callParent(arguments) || [];
                  if ( field.existing ){
                    errors.push(error);
                  } else {
                    Ext.Array.remove(errors, error);
                  }
                  return errors;
                }
              });
            },
            
            blur         : {
              fn : CarmenImportData.library.verify_unicity_emplacement,
              scope : formGrid
            }
          }
        },
        action_change_tablename : {
          handler : CarmenImportData.library.changeTableName,
          scope : formGrid
        },
        action_existing_tablename : {
          handler : function(){
            var formGrid = this;
        
            var store = Ext.create('Ext.data.JsonStore', {
              autoLoad : true,
              fields : ["couchd_emplacement_stockage"],
              proxy : {
                type : 'ajax',
                enablePaging: true,
                reader: {
                  rootProperty: 'data'
                },
                url : Routing.generate('carmen_ws_gettablelist')
              }
            });
            
            var filter = formGrid.down("[name=couchd_emplacement_stockage]").getValue();
            Ext.create('Ext.window.Window', {
              closeAction : 'destroy',
              id : 'showTableList',
              autoShow : true,
              width : 800,
              title : 'Liste des noms de tables déjà utilisés',
              layout : 'fit',
              items : [{
                columnLines : true,
                plugins: 'gridfilters',
                xtype : 'grid',
                reserveScrollbar : true,
                height : 500,
                width : '100%',
                viewConfig : {stripeRows : true, emptyText : 'Aucune table'},
                tbar : ['->', {
                  labelSeparator : '',
                  labelWidth : 225,
                  labelAlign : 'left',
                  xtype : 'textfield',
                  fieldLabel : 'Filtrer sur les tables commençant par ',
                  enableKeyEvents :true,
                  value : filter,
                  emptyText : 'Aucun filtre',
                  filterFn : function(){
                    var grid = this.up('grid');
                    if ( !grid ) return;
                    var value = Ext.String.trim(this.getValue().toLowerCase());
                    grid.getStore().clearFilter(true);
                    grid.getView().emptyText = ('Aucune table');
                    if ( value!="" && value!==null && value!="null" ){
                      grid.getView().emptyText = ('Aucune table correspondant au filtre "'+value+'"');
                      grid.getStore().filterBy(function(rec){return rec.get('couchd_emplacement_stockage').indexOf(value)==0;});
                    }
                  },
                  listeners : {
                    change : function(textfield){
                      textfield.filterFn()
                    },
                    afterrender : function(textfield){
                      textfield.filterFn()
                    }
                  }
                }, '->'],
                columns : [{
                  dataIndex : 'couchd_emplacement_stockage',
                  header : 'Nom de la table',
                  width : '100%'
                }],
                store : store,
                listener : {
                  afterRender: function(){
                      this.getStore().load();
                  }
                }
              }]
            }).show();
          },
          scope : formGrid
        }
        
//      , action_show_database : {
//          hidden : true,
//          handler : function(){
//            CarmenImportData.library.showData.call(formGrid, 'database');
//          },
//          scope : formGrid
//        }
      };
    },
    
    getCoucheImportAction : function(formGrid, layertype){
      return {
        create : {
          checkLayerType : function(layertype){
            var label = (this.labels[layertype.toLowerCase()] || this.labels.others);
            this.setBoxLabel(label);
          },
          checkImportAction : function(importaction){
            var field = this;
            var old_disabled = this.isDisabled();
            this.setDisabled(this.importaction.indexOf(importaction)==-1);
            if ( old_disabled!=this.isDisabled() && !this.isDisabled() )this.setValue(true);
          }
        },
        
        update : {
          fieldcontainer : {
            checkImportAction : function(importaction){
              var field = this;
              var disabled = this.importaction.indexOf(importaction)==-1 || this.down('combo').getStore().getCount()==0;
              Ext.each(this.query('field'), function(){
                var old_disabled = this.isDisabled();
                this.setDisabled(disabled);
                if ( old_disabled!=this.isDisabled() && !this.isDisabled() && this.is('radio') ){
                  this.setValue(true);
                  this.fireEvent('change', this, this.getValue());
                }
              })
            },
          
            isFormField : true,
//            resetOriginalValue: function() {
//                this.originalValue = this.getValue();
//                this.validate();
//            },
            reset : function(){
              var field = this;
              this.down('combo').getStore().removeAll();
              //this.down('combo').reset();
            },
            isDirty : function(){
              var field = this;
              return this.down('combo').isDirty();
            },
            getValue : function(){ 
              var field = this;
              return this.down('combo').getStore().getRange();
            },
            getModelData : function(){
              var field = this;
              return {};
            },
            getSubmitData : function(){
              var field = this;
              return {};
            },
            setValue : function(unique_fields){ 
              var record = formGrid.getLayer();
              var importaction = record.get('used_in_views') ? 'used' : 'created';
              var field = this;
              var combo = this.down('combo');
              var comboValue = combo.getValue();
              combo.setValue(null);
              var store = combo.getStore();
              store.removeAll();
              if ( unique_fields instanceof Array ){
                store.loadRawData(unique_fields);
                combo.setValue(comboValue);
                field.checkImportAction(importaction);
              }
              else if ( unique_fields instanceof Ext.data.Store ){
                if ( !record.get('couchd_emplacement_stockage') ) {
                  combo.setValue(comboValue);
                  field.checkImportAction(importaction);
                  return;
                }
                store.setProxy({
                  autoLoad : false,
                  type : 'ajax',
                  timeout :  24*60*60*1000,
                  url : Routing.generate('carmen_ws_uniquefields', {
                    fmeta_id : record.getMetadataId(),
                    couchd_emplacement_stockage : record.get('couchd_emplacement_stockage')
                  })
                });
                if ( unique_fields.loadCount==0 ){
                  store.on('load', function(store, records){
                  	if ( !records ) return;
                    unique_fields.removeAll();
                    unique_fields.loadRawData(Ext.Array.pluck(records, "data"));
                    combo.setValue(comboValue);
                    field.checkImportAction(importaction);
                    if ( records && records.length==0 ){
                      field.unmask();
                      field.mask('<b>Aucun champ à valeurs uniques</b>');
                      Ext.Function.defer(function(){field.unmask()}, 3000);
                    } else {
                      field.unmask();
                    }
                  })
                  field.mask('Recherche des champs à valeurs uniques');
                  store.reload();
                } else {
                  store.loadData(unique_fields.getRange());
                  combo.setValue(comboValue);
                  field.checkImportAction(importaction);
                }
              }
            },
            isValid : function(){var field = this; return !this.down('radio').getValue() || this.down('combo').isValid() },
            validate : function(){var field = this; return this.isValid();}
          },
          radio : {
            listeners : {
              change : function(radio, checked){
                var field = this;
                radio.nextSibling('combo').allowBlank = !checked;
                radio.nextSibling('combo').setDisabled(!checked);
              },
              afterrender: function(me) {
                 // Register the new tip with an element's ID
                 Ext.tip.QuickTipManager.register({
                   target: me.getId(), // Target button's ID
                   title : me.boxLabel, 
                   text  : "La couche doit posséder la même structure (même nom des champs, même type de champs) que la table dans PostGIS." +
                           "<br/>Cette opération ajoute les lignes qui ne sont pas déjà dans la table en se basant sur la clé."
                 });
              }
            }
          },
          combo : {}
        },
        
        replace : {
          checkLayerType : function(layertype){
            var label = (this.labels[layertype.toLowerCase()] || this.labels.others);
            this.setBoxLabel(label);
          },
          checkImportAction : function(importaction){
            var field = this;
            var old_disabled = this.isDisabled();
            this.setDisabled(this.importaction.indexOf(importaction)==-1);
            if ( old_disabled!=this.isDisabled() && !this.isDisabled() )this.setValue(true);
          },
          listeners : {
            afterrender: function(me) {
               // Register the new tip with an element's ID
               Ext.tip.QuickTipManager.register({
                 target: me.getId(), // Target button's ID
                 title : me.boxLabel, 
                 text  : "La couche doit posséder la même structure (même nom des champs, même type de champs) que la table dans PostGIS." +
                         "<br/>Tous les enregistrements sont supprimés puis l'import est fait à partir de la couche."
               });
            }
          }
        }
      };
    },//getCoucheImportAction
    
    getCoucheVisibility : function(formGrid, layertype, type){
      return {
        active_webservice : {
          setValue : function(value){
            var field = this;
            if ( this.rendered ) Ext.form.field.Checkbox.prototype.setValue.call(this, !value); 
            Ext.form.field.Checkbox.prototype.setValue.call(this, value)
          },
          listeners : {
            change : function(checkbox, checked){
              var field = this;
              formGrid.down('[name=config_'+type+']').setDisabled(!checked);
            }
          }
        },
        configure_webservice : function(btnType){
          var formGrid = this;
          if(Ext.StoreMgr.lookup('CoucheDonneesStore') !== undefined) {
              var mystore = Ext.StoreMgr.lookup('CoucheDonneesStore'); 
              var win = Ext.create('CarmenAddToWebService', {
                metadata:Carmen.metadata, 
                coucheStore : mystore,//Ext.StoreMgr.lookup('CoucheDonneesStore'), 
                type:type
              });                 
              
              win.show(); 
          } else {
            var mystore = new CarmenImportData.data.TreeStore({
                model:'CarmenImportData.model.Metadata', 
                id:'CoucheDonneesStore', 
                listeners : {
                    load: function ( ) {
                        var win = Ext.create('CarmenAddToWebService', {
                          metadata:Carmen.metadata, 
                          coucheStore : mystore,//Ext.StoreMgr.lookup('CoucheDonneesStore'), 
                          type:type
                        });       
                        win.show(); 
                    }
                }
            });   
            
            mystore.load();
          }
          
          return;
        }
      };
    }//getCoucheVisibility
    
    
  }
  
//, showData : function(origin){
//    var formGrid = this;
//    var layerfields = formGrid.down('[name=layerfields]').getValue()
//        , fields = [], columns = [];
//        
//    var layertype = formGrid.up().down('[name=metadata_layertype]').getValue().toUpperCase();
//    var relativePath = formGrid.down('[name=file_source]').getValue();
//    if ( origin=='file' && (relativePath=="" || relativePath==null) ) return;
//    
//    var fileSheet = (!formGrid.down('[name=file_sheet]').isDisabled() ? formGrid.down('[name=file_sheet]').getValue() || null : null);
//    var data = formGrid.getLayer().getData({persist : true, serialize : true});
//    
//    Ext.each(layerfields, function(field){
//      var field_name = field.get('field_name');
//      fields.push(field_name);
//      columns.push({header : field_name, dataIndex : field_name, shrinkWrap :1});
//    });
//    
//    var args = {}, dataOrigin;
//    if ( origin=="file" ){
//      dataOrigin = " du fichier "+formGrid.down('[name=file_name]').getValue();
//      args = {
//        pk_couche_donnees : data.pk_couche_donnees,
//        couchd_type_stockage : data.couchd_type_stockage,
//        layertype : layertype, 
//        layerfile : encodeURIComponent(relativePath),
//        layersheet : (fileSheet=="" ? null : fileSheet)
//      };
//    } else {
//      args = {
//        couchd_emplacement_stockage : formGrid.down('[name=couchd_emplacement_stockage]').getValue()
//      };
//      dataOrigin = " de la table "+args.couchd_emplacement_stockage;
//    }
//
//    var store = Ext.create('Ext.data.JsonStore', {
//      autoLoad : true,
//      fields : fields, 
//      pageSize: 20,
//      listeners: {
//        'metachange': function(store, meta) {
//          Ext.getCmp('showData').down('grid').reconfigure(store, meta.columns);
//        }
//      },
//      proxy : {
//        type : 'ajax',
//        enablePaging: true,
//        reader: {
//          rootProperty: 'data',
//          totalProperty: 'total',
//          metaProperty : 'metaData'
//        },
//        url : Routing.generate('carmen_ws_data_'+origin, args)
//      }
//    });
//    
//    Ext.create('Ext.window.Window', {
//      closeAction : 'destroy',
//      id : 'showData',
//      autoShow : true,
//      width : 800,
//      title : 'Visualisation des données'+dataOrigin,
//      layout : 'fit',
//      items : [{
//        columnLines : true,
//        xtype : 'grid',
//        reserveScrollbar : true,
//        height : 500,
//        width : '100%',
//        viewConfig : {stripeRows : true, emptyText : 'Aucune donnée'},
//        columns : columns,
//        bbar: {
//          xtype: 'pagingtoolbar',
//          pageSize: 10,
//          store: store,
//          displayInfo: true
//        },
//        store : store,
//        listener : {
//          afterRender: function(){
//              this.getStore().load();
//          }
//        }
//      }]
//    }).show();
//  }    
};
CarmenImportData.library = Ext.apply(CarmenImportData.library||{}, CarmenImportData._library_);

function positionMarkViews(){/*Marqueur de position pour le code global*/}
/**
 * This Form demonstrates the fact that by virtue of inheriting from the Ext.Container
 * class, an Ext.form.Panel can contain any Ext.Component. This includes all the
 * subclasses of Ext.Panel, including the GridPanel.
 *
 * The Grid demonstrates the use of creation of derived fields in a Record created using a
 * custom `convert` function, and the use of column renderers.
 *
 * The Form demonstrates the use of radio buttons grouped by name being set by the value
 * of the derived rating field.
 */
Ext.define('CarmenImportData.view.form.FormGrid', {
    extend: 'Ext.form.Panel',
    xtype: 'form-grid',
    
    layertype : CarmenImportData.Datatypes.enums.vector,
        
    
    initGrid : function(config){
    	var funcDataChanged = function(store){
        Ext.getCmp('default_representation').setDisabled(store.find('import_action', 'update')==-1 && store.find('import_action', 'replace')==-1);
        Ext.each(['wms', 'wfs'], function(type){
            try{
              if(typeof(Ext.getCmp('couchd_'+type))!=="undefined"){
                Ext.getCmp('couchd_'+type).setChecked(
                Ext.Array.some(store.collect('couchd_'+type), function(item){return item;}));
              }
            }catch (error){
              console.log(error);
            }
        });  
        if( Carmen.metadata.metadata_datatype !== null && Carmen.metadata.metadata_datatype !== undefined) {
          if( Carmen.metadata.metadata_datatype.toUpperCase() == 'MODELE' ) {
            if( store.count() > 0 ) {
              CarmenImportData.library.changeModeActionState();
            }
          }
        }
      };

      var formGrid = this;
      var layertype = this.layertype;

      var grid = {
        reserveScrollbar : true,
        forceFit : true,
        rootVisible: false,
        rowLines : true,   
        bufferedRenderer : false,
        //region : 'center',
        id : 'importdatagrid',
        //dockedItems: [ ],
        xtype: 'treepanel',
        viewConfig : {
        	loadingText : 'Chargement en cours...',
        	loadMask : true,
          emptyText : 'Aucune donnée', 
          deferEmptyText : false,
          getRowClass : function( record , index , rowParams , store ){
          	if ( record instanceof Metadata )return 'row-metadata';
          },
          listeners : {
          	cellclick : function(view, td, cellIndex, record, tr, rowIndex, evt){
          		if ( evt.target.tagName=="IMG" ) return;
          		if( record instanceof Metadata ) (record.isExpanded() ? record.collapse() : record.expand());
          	}
          }
        },
        store: new CarmenImportData.data.TreeStore({model:'CarmenImportData.model.Metadata', id:'CoucheDonneesStore', listeners : {
          load : funcDataChanged,
          datachanged : funcDataChanged,
          update : funcDataChanged
        }}),
        overflow:'y',
        height: 350,
        border : true,
        columnLines : true,
        
        columns: [
        {
          xtype: 'treecolumn',
//          including_layertype : ['multi'],
          width:170,
          dataIndex : 'mtype',
          renderer : function(value, meta, record){
          	var data = record.getTypePath() || [];
          	data = Ext.Array.remove(data, null);
          	data = Ext.Array.remove(data, undefined);
          	return data[data.length-1];
          }
        },
        {
          text: 'Donnée',
          cellWrap : true,
          flex: 0.55,
          sortable: true,
          dataIndex: 'nom',
          renderer : function(value, meta, record){
            var tpl = '{nom}';
            if ( record.get('description') ) tpl = '{nom}<br/><i>{description}</i>';
            return '<div style="display:inline-block">'+new Ext.XTemplate(tpl).apply(record.data)+'</div>';
          }
        }, {
//          including_layertype : ['raster'],
          text: 'Fichier source',
          flex: 0.52,
          sortable: true,
          dataIndex: 'file_name'
        }, {
//          exclude_layertype : 'raster',
          text: 'Table de stockage',
          flex: 0.52,
          sortable: true,
          dataIndex: 'stockage'
        }]
        //.concat(this.getCoucheVisibility(formGrid, layertype))
        .concat([{
          text: "Statut",
          cellWrap : true,
          align : 'left',
          flex: 0.47,
          sortable: true,
          dataIndex: 'status',
          renderer : function(value, meta, record, rowIndex){
            meta.tdCls = "import-data-status "+(record.get('color') || "");
            var details = null;
            if ( record.get('_status_')=="FAILURE" && (details = record.get('details'))){
            	return "<div>" +
            			"<div id='errors_collapsed_"+rowIndex+"'>"+value+"<br/><a href='javascript:switchErrorDetails("+rowIndex+", true);void(0);'>Détails</a></div>"+
            			"<div class='x-hidden' id='errors_expanded_"+rowIndex+"'>"+details.join('<br/>')+
            			"<br/><a href='javascript:switchErrorDetails("+rowIndex+", false);void(0);'>Réduire</a></div>"+
            			"</div>";
            }
            return value;
          }
        }, {
          xtype : 'actioncolumn',
          flex: 0.1,
          text : 'Actions',
          align : 'center',
          items : [{
          	getClass : function(v, meta, record){
          		if ( record instanceof  CarmenImportData.model.CoucheDonnees ) return "x-hidden";
          		return "";
          	},
            icon : '/images/add.png',
            tooltip : 'Ajouter une donnée SIG à la métadonnée',
            scope: formGrid,
            handler : CarmenImportData.library.createLayerOnMetadata
          },{
            getClass : function(v, meta, record){
              if ( !(record instanceof  CarmenImportData.model.CoucheDonnees) ) return "x-hidden";
              return "";
            },
            tooltip : 'Supprimer la donnée SIG et les données importées associées',
            icon : '/images/delete.png',
            scope: formGrid,
            handler : CarmenImportData.library.deleteLayer
          },{
            getClass : function(v, meta, record){
              if ( !(record instanceof  CarmenImportData.model.CoucheDonnees) ) return "x-hidden";
              if((record.getData()["typeGeom"] != "MULTIPOINT" && record.getData()["typeGeom"] != "POINT")) return "x-hidden";
              return "";
            },
            tooltip : 'Géocodage inverse',
            icon : '/images/geoloc.png',
            scope: formGrid,
            handler : CarmenImportData.library.geocodageInverse,
          }]
        }]),
        listeners: {
          scope: formGrid,
          cellclick: CarmenImportData.library.onSelectionChange,
          beforecellclick : function(grid, td){
          	var available = td.innerHTML.indexOf('errors_expanded_')==-1;
          	if ( !available ){
          		grid.suspendEvent('itemclick');
          	} else if ( grid.isSuspended('itemclick') ) {
          		grid.resumeEvent('itemclick', false);
          	}
          	return available;
          }
        }
      };
      Ext.apply(grid, config);
      
      var functions = CarmenImportData.library.fields_actions.getShemaNom(formGrid);
      
      var panel = {
        title : 'Données SIG',
        xtype : 'panel',
        dockedItems : [{
          xtype: 'toolbar',
          dock: 'top',
          defaultButtonUI : 'default',
          items : [{
            id : 'tbar_label',
            xtype :'tbtext'
          }, {
            id : 'tbar_upload',
            text : 'Téléverser et Sélectionner',
            tooltip : 'Téléverser un fichier depuis son poste local vers le serveur',
            handler : function(){CarmenImportData.library.createLayer.call(formGrid, null, OPEN_CLIENT_BROWSER)},
            scope : formGrid
          }, {
            id : 'tbar_select',
            text : 'Sélectionner depuis le serveur',
            tooltip : "Sélectionner un fichier déjà déposé sur le serveur",
            handler : function(){CarmenImportData.library.createLayer.call(formGrid, null, OPEN_SERVER_BROWSER)},
            scope : formGrid
          }, {
            id : 'tbar_archive',
            text : "Ajout multiple à partir d'une archive (ZIP)",
            tooltip : "Transférer un fichier ZIP d'un serveur distant vers le serveur",
            scope : formGrid,
            handler : CarmenImportData.library.importZIP
          }]
        }],
        items : [{
          xtype: 'fieldcontainer',
          padding : 10,
          items: [
            Ext.apply(functions.shema_nom, {
              id : 'shema_nom',
              xtype : 'textfield',
              name : 'shema_nom',
              fieldLabel: 'Nom du schéma*',
              hidden: true,
              allowBlank: false,
              maskRe : /[a-z0-9_]/i,
              regex : /^[a-z][a-z0-9_]*$/,
              regexText : 'Seuls les lettres minuscules non accentuées, les chiffres et le souligné sont autorisés.<br/>Le nom doit commencer par une lettre.',
              value: (Carmen.metadata ? Carmen.metadata.schema : null)/*,
              triggers: {
                errorSchema: {
                  weight: 1,
                  hidden: true, 
                  cls: 'x-form-clear-trigger'
                  //triggerCls: ''
                }
              }*/
            }), {
            id : 'radiogroup_mode_action',
            xtype : 'radiogroup',
            allowBlank : false,
            fieldLabel : "Mode d'action",
            vertical : true,
            columns : 1,
            hidden: true,
            items : [{
              xtype : 'radio',
              name : 'mode_action',
              id : 'mode_action_create',
              inputValue : 'action_create',
              boxLabel : 'Mode création',
              checked : true
            }, {
              xtype : 'radio',
              name : 'mode_action',
              id : 'mode_action_add',
              inputValue : 'action_add',
              boxLabel : 'Mode ajout',
              disabled: true
            }, {
              xtype : 'radio',
              name : 'mode_action',
              id : 'mode_action_replace',
              inputValue : 'action_replace',
              boxLabel : 'Mode annule et remplace',
              disabled: true
            }],
            listeners : {
              change: function(rd, value) {
                if( value.mode_action == 'action_add' ) {
                  Ext.getCmp('shema_nom').setDisabled(true);
                }
                else {
                  Ext.getCmp('shema_nom').setDisabled(false);
                }
              }
            }
          }, {
            xtype: 'fieldcontainer',
            fieldLabel: 'Effacer la base tampon',
            defaultType: 'checkboxfield',
            id: 'cb_action_db',
            hidden: true,
            items: [{              
              name : 'action_delete_db',
              id: 'action_delete_db',
              inputValue : '1',
              disabled: true,
              listeners : {
                afterrender: function(me) {
                   // Register the new tip with an element's ID
                   Ext.tip.QuickTipManager.register({
                     target: me.getId(), // Target button's ID
                     title : me.boxLabel, 
                     text  : "Le dump est importé dans un base tampon avant import définitif." +
                             "<br/>Cette base tampon peut être conservée pour réaliser des imports successifs (mode ajout)." +
                             "<br/>Dans le cas contraire, il n'est pas possible de réaliser des imports en mode ajout."
                   });
                }
              }
            }]
          }] 
        }, 
        grid]
      };
      
      /*var tmp =[];
      var layertype = Ext.getCmp('metadata_layertype').getValue();
      grid.columns.forEach(function(column){
      	if ( (column.exclude_layertype ? column.exclude_layertype==layertype : false) 
      	  || (column.including_layertype ? column.including_layertype.indexOf(layertype)==-1 : false) ) return;
      	tmp.push(column);
      })
      grid.columns = tmp;*/
      return panel;
    },
    
    getCoucheDescription: function(formGrid, layertype)
    {
      return [{
        xtype : 'textfield',
        fieldLabel: 'Nom de la couche',
        name : 'couchd_nom'
      },{
        xtype : 'textarea',
        fieldLabel: 'Description',
        name : 'couchd_description',
        allowBlank : true
      }];
    },
    
    getCoucheEmplacement : function(formGrid, layertype)
    {
      var functions = CarmenImportData.library.fields_actions.getCoucheEmplacement(formGrid, layertype);
      // //////////////////////////////////
      return [{
        xtype : 'fieldcontainer',
        name : 'saut_de_ligne',
        value : '',
        style : 'min-height:10px !important'
      }, {
        labelWidth : 150,
        fieldLabel : 'Table POSTGIS',
//        exclude_layertype : 'raster',
        xtype : 'cid-fieldcontainer',
        name : 'fieldcontainer_emplacement',
        layout : {
          type : 'table',
          tableAttrs : {
            width : '100%'
          }
        },
        items : [
          Ext.apply(functions.couchd_emplacement_stockage, {
//            exclude_layertype : 'raster',
            allowBlank : false,
            width : '100%',
            xtype : 'textfield',
            vtype : 'identifier',
            name : 'couchd_emplacement_stockage',
    
            maskRe : /[a-z0-9_]/i,
            regex : /^[a-z][a-z0-9_]*$/,
            regexText : 'Seuls les lettres minuscules non accentuées, les chiffres et le souligné sont autorisés.<br/>Le nom doit commencer par une lettre.',
              
            importaction : 'new',
            margin : '0 10 0 0'
              // end Table
          }),
  
          Ext.apply(functions.action_change_tablename, {
            width : 90,
            xtype : 'button',
            disabled : true,
            importaction : ['created', 'used'],
            text : 'Renommer',
            scope : formGrid
          }),
  
          Ext.apply(functions.action_existing_tablename, {
            xtype : 'button',
            text : 'Tables existantes',
            scope : formGrid
          })
  
//        , Ext.apply(functions.action_show_database, {
//            xtype : 'button',
//            disabled : true,
//            margin : '0 0 0 10',
//            // importaction : 'created',
//            //            exclude_layertype : 'raster',
//            text : 'Afficher les valeurs',
//            scope : formGrid
//          })
        ]
      }]
    },//getCoucheEmplacement
    
    getCoucheImportAction : function(formGrid, layertype)
    {
      var labelVector;
      var functions = CarmenImportData.library.fields_actions.getCoucheImportAction(formGrid, layertype);
      ////////////////////////////////////
      return [{
//        exclude_layertype : 'raster',
        name : 'radiogroup_import_action',
        xtype : 'radiogroup',
        allowBlank : false,
        fieldLabel : "Action d'import"+UNSAVED_MARK,
        vertical : true,
        columns : 1,
        items : [
          Ext.apply(functions.create, {
//            exclude_layertype : 'raster',
            xtype : 'radio',
            name : 'import_action',
            id : 'import_action_create',
            inputValue : 'create',
            importaction : 'new',
            labels : {
              others : (labelVector = 'Création de la table POSTGIS'),
              raster : 'Création du raster'
            },
            boxLabel : labelVector,
            labelWidth : 0
          // end Create table
          }), 
          Ext.apply(functions.update.fieldcontainer, {
            xtype : 'fieldcontainer',
            name : 'unique_fields',
            layout: {
              type: 'column',
              pack: 'start',
              align: 'stretch'
            },
            importaction : 'created',
            items : [
              Ext.apply(functions.update.radio, {
//                exclude_layertype : 'raster',
                xtype : 'radio',
                name : 'import_action',
                id : 'import_action_update',
                inputValue : 'update',
                fieldAlign : 'top',
                boxLabel : 'Mise à jour de la table POSTGIS par rapport à la clé&nbsp;',
                labelWidth : 0
              // end Create table
              }),
              Ext.apply(functions.update.combo, {
                xtype : 'combo',
                queryMode : 'local',
                store :  Ext.create('Ext.data.JsonStore', {model : 'CarmenImportData.model.ChampDonnee'}),
                name : 'field_key', 
                emptyText : 'Aucune clé',
                displayField : 'field_name',
                valueField : 'field_name'
              // end Create table
              })
            ]
          }), 
          Ext.apply(functions.replace, {
//            exclude_layertype : 'raster',
            xtype : 'radio',
            name : 'import_action',
            id : 'import_action_replace',
            inputValue : 'replace',
            labels : {
              others : (labelVector = 'Annulation et remplacement de la table POSTGIS'),
              raster : 'Annulation et remplacement du raster'
            },
            boxLabel : labelVector,
            importaction : ['created', 'used'],
            labelWidth : 0
          // end Create table
          })
        ]
      }]
    },

    getUiCguEditor : function(formGrid, layertype){
      return {
        scope : formGrid,
        id: 'cgu_panel',
        xtype: 'panel',
        frame: true,
        layout: 'vbox',
        padding : 10,
        height: 400,
        title: "Conditions Générales d'Utilisation",
        items : [{
          xtype: 'fieldcontainer',
          height: 50,
          layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
          },
          defaults: {
            labelSeparator: ' '
          },
          items: [{
            xtype: 'checkboxfield',
            labelWidth : 300,
            fieldLabel: "Afficher les CGU",
            name: 'couchd_cgu_display'
          }]
        }, {
          id: 'cgu_editor',
          xtype: 'ckeditor',
          disabled : false,
          width : '100%',
          name: 'couchd_cgu_message',
          listeners: {
            render: function() {
              var panel = Ext.getCmp('cgu_panel');
              var editor = Ext.getCmp('cgu_editor');

              editor.setHeight(300);
              panel.doLayout();
            }
          }
        }]
      };
    },
    
    getCoucheVisibility : function(formGrid, layertype)
    {
      var functions = CarmenImportData.library.fields_actions.getCoucheVisibility(formGrid, layertype, 'wms');
      return {
        text : "Diffusion WMS/WFS",
        id : 'couche_visibility',
        disabled : (Ext.getCmp("metadata_layertype").getValue()==="tabulaire"  ? true : false),
        enable : function(){
          var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
          
          
          // empecher la diffusion WMS/WFS si la donnée et de type table attributaire
          var layer_type = Ext.ComponentQuery.query('[name=metadata_layertype]')[0].rawValue; 
          if(layer_type === "Table attributaire") {
              return this; 
          }
          
          if ( !store ) return this;
          if ( store.find('import_action', 'update')==-1 && store.find('import_action', 'replace')==-1 )
            return this;
          return Ext.button.Split.prototype.enable.apply(this, arguments);
        },
        handler: functions.configure_webservice, 
        scope : formGrid,
        xtype : 'splitbutton',
        menu: new Ext.menu.Menu({
          defaults : {xtype:'menucheckitem', checkChangeDisabled : true},
          items: []
        .concat(formGrid.getCoucheVisibilityByType(formGrid, layertype, 'wms', '(ouverture des droits en consultation)'))
        .concat(formGrid.getCoucheVisibilityByType(formGrid, layertype, 'wfs', '(ouverture des droits en téléchargement)'))
        })
      };
    },
    
    getCoucheVisibilityByType : function(formGrid, layertype, type, legend)
    {
      var TYPE = type.toUpperCase();
      if(Ext.getCmp("metadata_layertype").getValue()==="raster" && TYPE ==="WFS" || Ext.getCmp("metadata_layertype").getValue()==="tabulaire" ){
        return [];
      }
      var functions = CarmenImportData.library.fields_actions.getCoucheVisibility(formGrid, layertype, type);
      //formGrid.up().down('[name=metadata_layertype]').getValue().toUpperCase()
      ////////////////////////////////////
      return [{
        text : 'Diffusion '+TYPE,
        id : 'couchd_'+type,
//        bind : {
//          checked : "{hasDiffusion}"
//        },
//        formulas : {
//          hasDiffusion : function(get){
//            var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
//            if ( !store ) return false;
//            console.log(store.collect('couchd_'+type.toLowerCase()));
//            return Ext.Array.some(store.collect('couchd_'+type.toLowerCase()), function(item){return item});
//          }
//        },
//        checked : function(){
//            var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
//            if ( !store ) return false;
//            console.log(store.collect('couchd_'+type.toLowerCase()));
//            return Ext.Array.some(store.collect('couchd_'+type.toLowerCase()), function(item){return item});
//          },
        handler: functions.configure_webservice,
        scope : formGrid
      }];
    },
    
    initForm : function(config){
    
      var layertype = this.layertype;
      var formGrid = this;
      
      var columnLeft = [].concat(
        this.getCoucheDescription(formGrid, layertype)
      , this.getCoucheEmplacement(formGrid, layertype)
      , this.getCoucheImportAction(formGrid, layertype)
      , this.getUiCguEditor(formGrid, layertype)
      );
      
      
      Ext.each(columnLeft, function(field, index){
        columnLeft[index] = Ext.ComponentManager.create(field, 'textfield');
      });
//      Ext.each(columnRight, function(field, index){
//        columnRight[index] = Ext.ComponentManager.create(field, 'textfield');
//      });
      
      // configuration of fields visibility according to the state of record and the layertype of the metadata
      
      
      
      // FORM
      var form = {
          //region : 'south',
        width: '100%',
        title:DEFAULT_TITLE,
        defaults : {
          margin : "0 10 0 10"
        },
        xtype : 'panel',
        disabled : true,
        border : true,
        items : [
       //   {html : "<span class='div-info'>(*) Signale les champs de configuration non enregistrés sauf dans le cas de l'import différé des données.</span>"}, 
          {
            xtype: 'form',
            //trackResetOnLoad : true,
            id : 'importdataform',
            layout: 'column',
            buttonAlign : 'center',
            buttons : [{
              text : 'Valider le paramétrage',
              handler : CarmenImportData.library.saveLayerConfiguration,
              scope : formGrid
            }, {
              text : 'Annuler',
              handler : CarmenImportData.library.cancelLayerConfiguration,
              scope : formGrid
            }],
            
            defaults: {
              layout: 'form',
              defaultType: 'textfield',
              columnWidth : 0.5,
              margin : 10
            },
            
            items: [{
              columnWidth : 0.5, 
              fieldDefaults : {allowBlank : false},
              defaults : {allowBlank : false},
              id : 'form-column-left', 
              items : columnLeft
            } , {
              id : 'form-column-right', 
              xtype: 'carmenimportconfigurator',
              formGrid : formGrid
            }]
          }]
      };
      return Ext.apply(form, config);
    },
    
    // In this example, configuration is applied at initComponent time
    // because it depends on themeInfo object that is generated for the
    // FormGrid instance.
    initComponent: function() {
      var layertype = this.layertype;
      var formGrid = this;
      var buttons = this.buttons || [];
      delete this.buttons;
      Ext.apply(this, {
        width: '100%',
        fieldDefaults: {
          labelAlign: 'left',
          labelWidth: 150,
          anchor: '100%',
          msgTarget: 'side'
          
        },
        frame: true,
        bodyPadding: 10,
        defaults : {padding: 10,border : true},
        
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
         items: [
          this.initGrid({
            bbar : {
              defaultButtonUI : 'default',
              items : ['->',
              {
                xtype: "tbtext",
                text : "<u>Sur l'ensemble des couches configurées </u>"
              },'-']
              .concat(buttons.slice(0,2))
              .concat([{
              	id : 'bbar_separator',
                xtype:'tbseparator'
              },
            	Ext.apply({
                checkLayerType : function(layertype){
                  var metadata_layertype = Ext.getCmp("metadata_layertype").getValue();
                  this.setVisible( ["multi", "mnt", 'tabulaire'].indexOf(metadata_layertype)==-1 );
                }
            	}, CarmenImportData.library.getDefaultMapButton(formGrid))
              , '-'
              ])
              .concat([this.getCoucheVisibility(formGrid, layertype), '-'])
              .concat(buttons.slice(2))
            }
          }) 
          , this.initForm({})
        ],
        buttonAlign : 'center'
      });
      this.callParent();
      this.on('recordloaded', CarmenImportData.library.onLayerLoaded, this);
    },
    
    
    getGrid : function(){return this.down('treepanel#importdatagrid');},
    getSubForm : function(){return this.down('form#importdataform');},
    getLayer : function(){return this.getSubForm().getRecord();},
    loadStore : function(){
      var grid = this.getGrid(),
          store = grid.getStore();
      store.load();
    },
    setStore : function(data){
      var grid = this.getGrid(),
          store = grid.getStore();
      if ( data instanceof Ext.data.Store ){
        store.loadData(data.getRange(), false);
      }
      else {
        store.loadRawData(data, false);
      }
      store.fireEvent('load', store, store.getRange());
      store.each(function(record){record.set('couchd_type_stockage', this.layertype)}, this);
    }
});


Ext.define('CarmenImportData.view.form.ImportData', {
	requires : ['CarmenImportConfigurator'],
    extend: 'Ext.panel.Panel',
    xtype : 'form-importdata',
    scrollable : 'y',
    viewModel : true,
    initComponent : function(){
      var importDataView = this;
      Ext.apply(this, {
      	//height : (Ext.dom.Element.getViewportHeight()),
        items : [{
          title : 'Importation des données',
          //region : 'north',
          layout: {
              type: 'vbox',
              pack: 'start',
              align: 'center'
          },
          items : [{
            width: '50%',
            xtype : 'form',
            id : 'metadata-form',
            fieldDefaults: {
              labelAlign: 'left',
              msgTarget: 'side'
            },
            defaults : {
              labelWidth: 150,
              width : 600
            },
            items : [{
              xtype : 'displayfield',
              fieldLabel : 'Identifiant',
              name : 'uuid',
              value : (Carmen.metadata ? Carmen.metadata.uuid : null)
            }, {
              xtype : 'displayfield',
              fieldLabel : 'Nom de la métadonnée',
              name : 'metadata_title',
              value : (Carmen.metadata ? Carmen.metadata.metadata_title : null)
            }, {
              xtype : 'combo',
              fieldLabel : 'Type de données',
              name : 'metadata_layertype',
              id : 'metadata_layertype',
              readOnlyCls : 'x-form-readonly-noborder',
              queryMode : 'local',
              store : CarmenImportData.Datatypes.getStore('valueField', 'displayField'),
              valueField : 'valueField', 
              displayField : 'displayField',
    
              value : (Carmen.metadata ? Carmen.metadata.metadata_datatype : null) || 'vector',
              importaction : 'new',
              changeState : function(enable){
                this.setDisabled(!enable);
              },
              listeners : {
              	change : function(combo){
              		var isJoin = combo.getValue()=="join";
                  importDataView.down('[name=view_name]').setVisible(isJoin);
              		importDataView.down('[name=view_name]').allowBlank = !(isJoin);
              		importDataView.down('button#initialiser').setText(isJoin ? "Paramétrer la jointure" : "Initialiser");
              	}
              }
            }, {
              xtype : 'hiddenfield',
              name : 'pk_couche_donnees_join',
              value : (Carmen.metadata ? Carmen.metadata.pk_couche_donnees_join : null)
            }, {
              readOnlyCls : 'x-form-readonly-noborder',
            	hidden : (Carmen.metadata ? Carmen.metadata.metadata_datatype : null)!="join",
              xtype : 'textfield',
              fieldLabel : 'Nom de la vue',
              name : 'view_name',
    
              allowBlank: (Carmen.metadata ? Carmen.metadata.metadata_datatype : null)!="join" ,
              vtype : 'identifier',
              maskRe : /[a-z0-9_]/i,
              regex : /^[a-z][a-z0-9_]*$/,
              regexText : 'Seuls les lettres minuscules non accentuées, les chiffres et le souligné sont autorisés.<br/>Le nom doit commencer par une lettre.',
              
              value : (Carmen.metadata ? Carmen.metadata.view_name : null),
              listeners : {
              	blur : {
                  fn : CarmenImportData.library.verify_unicity_emplacement,
                  scope : importDataView
              	}
              }
            }],buttons : [{
              text : (Carmen.metadata && Carmen.metadata.view_name ? "Paramétrer la jointure" : 'Initialiser'),
              id : 'initialiser',
              importaction : 'new',
              changeState : function(enable){
                this.setVisible(enable);
              },
              confirm : true,
              handler : CarmenImportData.library.initialiserImportData(importDataView)
            }].concat(
            
            Carmen.metadata && Carmen.metadata.view_name
            ? ['->', 
              CarmenImportData.library.getDefaultMapButton(importDataView)
              ].concat( importDataView.getCoucheVisibility(importDataView) )
            : []
            )
          }]
        }]
      })
      this.callParent();
      if ( !(Carmen.metadata && Carmen.metadata.id) ) this.hide();
    },
    
    
    getCoucheVisibility : function(formGrid, layertype)
    {
      return []
      .concat(formGrid.getCoucheVisibilityByType(formGrid, layertype, 'wms', '(ouverture des droits en consultation)'))
      .concat(formGrid.getCoucheVisibilityByType(formGrid, layertype, 'wfs', '(ouverture des droits en téléchargement)'));
    },
    
    getCoucheVisibilityByType : function(formGrid, layertype, type, legend)
    {
      var TYPE = type.toUpperCase();
     
      var functions = CarmenImportData.library.fields_actions.getCoucheVisibility(formGrid, layertype, type);
      ////////////////////////////////////
      return [{
      	xtype:'button',
        flex : 0.1,
        text : TYPE,
        handler : function(btn){
          window.afterLayerAddToWebService = function(pk_couche_donnees, data, the_window){
          	Ext.Ajax.request({
          		url : Routing.generate("carmen_savediffusionwxs_join", {
          		  pk_couche_donnees : pk_couche_donnees,
          		  wxs_type : type.toLowerCase(),
          		  wxs_enable : data['couchd_'+type.toLowerCase()]
          		}),
          		callback : function(){
          			if ( the_window ) the_window.close();
          		}
          	})
          };
          
          functions.configure_webservice(btn, Ext.apply(Ext.create('CoucheDonnees'), {
          	getUUID : function(){return Carmen.metadata.uuid},
          	get : function(field){
          		switch(field){
          			case "pk_couche_donnees"           : return Carmen.metadata.pk_couche_donnees_join; 
          			case "couchd_emplacement_stockage" : return Carmen.metadata.view_name; 
                case "couchd_type_stockage"        : return Carmen.metadata.metadata_datatype; 
          		}
          	},
          	set : function(){}
          }));
        }
      }];
    }
});

function positionMarkReadyFunction(){/*Marqueur de position pour le code global*/}
var DEV = true;
Ext.onReady(function() {
  Ext.tip.QuickTipManager.init();
  var importDataView = Ext.create('Ext.container.Viewport', {
  	layout : 'fit',
    renderTo : 'main',
    items:[Ext.create('CarmenImportData.view.form.ImportData')],
    listeners : {
      afterrender : function(){
        //if ( DEV ) return;
        var button = this.down('button#initialiser');
        if ( Carmen.metadata && Carmen.metadata.metadata_datatype!="multi" ){
           var store = CarmenImportData.Datatypes.getStore('valueField', 'displayField');
           store.removeAt(store.find('valueField', 'multi'));
        } 
        if ( Carmen.metadata && Carmen.metadata.metadata_datatype && button ){
        	if ( Carmen.metadata.metadata_datatype!="join" ){
            button.confirm = false;
            button.handler();
          } else {
            var layertype = this.down('[name=metadata_layertype]');
            var view_name = this.down('[name=view_name]');
        
            layertype && layertype.setReadOnly(true);
            view_name && view_name.setReadOnly(true);
        	}
        }
      }
    }
  });
  
});


function switchErrorDetails(rowIndex, open){
	var collapsed = Ext.get('errors_collapsed_'+rowIndex);
	var expanded = Ext.get('errors_expanded_'+rowIndex);
	if ( collapsed && expanded){
		collapsed[!open ? 'removeCls' : 'addCls']('x-hidden');
		expanded [ open ? 'removeCls' : 'addCls']('x-hidden');
	}
	return false;
}
window.onunload = function(){Ext.Ajax.abortAll()};
if ( !DEV ) 
window.onbeforeunload = CarmenImportData.library.confirmBeforeClose;
  
