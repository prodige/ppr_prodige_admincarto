var _T_LAYER_LOCALDATA = function (key) {
  return _t("Layer.forms.GTabLocalData." + key);
};

Carmen.Model.Layer.TYPES.LOCAL_DATA = 'LocalData';
/**
 * Carmen.Model.LayerPostgis
 */
Ext.define('Carmen.Model.LayerLocalData', {
  extend: 'Carmen.Model.LayerVectorial',
  idProperty: 'layerId',
  fields: [
    {name: "layerType",   type: "string", defaultValue: 'POSTGIS', convert : function(value){if ( Ext.isObject(value) ) return value.layerTypeCode; return value;}},

    {name: "msLayerConnectionType",  type: "string", defaultValue: "POSTGIS"}, // mapfile data : CONNECTIONTYPE
    
    {name: "msLayerPgSchema",        type: "string"}, // mapfile data : DATA (SCHEMA_NAME part)
    {name: "msLayerPgTable",         type: "string"}, // mapfile data : DATA (TABLE_NAME part)
    {name: "msLayerPgGeometryField", type: "string"}, // mapfile data : DATA (THE_GEOM part)
    {name: "msLayerPgIdField",       type: "string"}, // mapfile data : DATA (GID part)
    {name: "msLayerPgProjection",    type: "string"} // mapfile data : DATA (SRID part)
    
  ]});

/**
 * Generic tab for type of postgis layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabLocalData', {
  extend: 'CarmenImportConfiguratorForm',
  
  title: _T_LAYER_LOCALDATA('title'),
  height: 'auto',
  scrollable : 'y',
  modelName: 'Carmen.Model.LayerLocalData',
  
  suffixRoute : '_localdata',
  // methods
  initComponent: function() {
    var me = this;

    this.configuratorConfig = {
      suffixRoute : '_localdata',
      items : [{
        xtype: 'textfield',
        name: 'layerTitle',

        fieldLabel: _T_LAYER_PGSQL('fields.title'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_PGSQL('fields.name'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [registerWarningOnIdentifierLength({
            xtype: 'textfield',
            name: 'layerName',
            allowBlank: false,
            /*  recommended that the name not contain spaces, special characters, or begin with a number */
            vtype: 'identifier',
            flex: 5
          }), {
            xtype: 'button',
            name : 'layerName_button',
            text: _T_LAYER_PGSQL('fields.auto'),
            margin: '0 10px 0 0',
            /**
             * auto calculation of layerName field
             */
            handler: function() {
              var ctrlFrom = me.down('[name=layerTitle]');
              var ctrlTo = me.down('[name=layerName]');
              if ( ctrlFrom && ctrlTo ) {
                ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
              }
            },
            flex: 1
          }, {
            xtype: 'textfield',
            name: 'layerIdentifier',
            fieldLabel: _T_LAYER_PGSQL('fields.ident'),
            allowBlank: false,
            /*  recommended that the name not contain spaces, special characters, or begin with a number */
            vtype: 'identifier',
            flex: 5
          }, {
            xtype: 'button',
            name : 'layerIdentifier_button',
            text: _T_LAYER_PGSQL('fields.auto'),
            /**
             * auto calculation of layerName field
             */
            handler: function() {
              var ctrlFrom = me.down('[name=layerTitle]');
              var ctrlTo = me.down('[name=layerIdentifier]');
              if ( ctrlFrom && ctrlTo ) {
                ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
              }
            },
            flex: 1
          }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_VECTOR('fields.type'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          flex : 11,
          margin: '0 10px 0 0',
        xtype: 'combobox',
        name: 'msGeometryType',
        queryMode: 'local',
        displayField: 'displayField',
        valueField: 'valueField',
        store: Carmen.Dictionaries.GEOMETRYTYPES,
        autoLoadOnValue: true,
        autoSelect: true,
        editable: false
        }, {
          flex:1, 
          xtype : 'hiddenfield', 
          name : 'layerDataEncoding' 
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_PGSQL('fields.scales'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
            xtype: 'numberfield',
            name: 'layerMinscale',
            fieldLabel: _T_LAYER_PGSQL('fields.min'),
            allowBlank: false,
            maxLength: 10,
            enforceMaxLength: true,
            value: Carmen.Defaults.MINSCALE,
            minValue: Carmen.Defaults.MINSCALE,
            maxValue: Carmen.Defaults.MAXSCALE,
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowOnlyWhitespace: false,
            allowDecimals: false,
            allowExponential: false,
            flex: 6,
            margin: '0 10px 0 0'
          }, {
            xtype: 'numberfield',
            name: 'layerMaxscale',
            vtype: '',
            fieldLabel: _T_LAYER_PGSQL('fields.max'),
            allowBlank: false,
            maxLength: 10,
            enforceMaxLength: true,
            value: Carmen.Defaults.MAXSCALE,
            minValue: Carmen.Defaults.MINSCALE,
            maxValue: Carmen.Defaults.MAXSCALE,
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowOnlyWhitespace: false,
            allowDecimals: false,
            allowExponential: false,
            flex: 6
          }]
      }]
    };
    this.callParent(arguments);
    this.on('afterrender', function(){
    	var me = this;
    	this.up('window').saveAllForms = function(){me.saveAllForms(this)}
    }, this)
  },  
  
  readSelectedFile : function(response){
  	this.down('[name=msGeometryType]').setValue(response.msGeometryType || 'POLYGON');
  },
  
  saveAllForms : function(oWindow){
    var form = this;
    if (!form) return;
    if (CarmenImportData.library.saveLayerConfiguration.call(form, false)) {
      var couche = form.getLayer();
      if (!couche) return;
      var layer = Ext.create('Carmen.Model.LayerLocalData', form.getForm().getFieldValues());
      var data = Ext.apply(
        layer.getData({persist : true, serialize : true}), 
        couche.getData({persist : true, serialize : true})
      );

      data.mapId = Carmen.user.map.get('mapId');
      
      form.mask('Import des données locales...');
      Ext.Ajax.request({
        method : 'POST',
        cors : true,
        withCredentials : true,
        url : Routing.generate('carmen_ws_importlocaldata', {returnType:"entity"}),
        timeout :  24*60*60*1000,
        jsonData : data,
        success : function(response) {
          form.unmask();
          response = Ext.decode(response.responseText);
          oWindow.postSaveSuccess(new Ext.data.operation.Create({}), response);
          return;
        },
        failure : function(response) {
          form.unmask();
          if ( Carmen.notAuthenticatedException(response) ) return;
          var title = oWindow.getConfig().title ? oWindow.getConfig().title : _t("form.msgFailureTitle");
          Ext.Msg.alert(title, _t("form.msgFailure"))
          oWindow.postSaveFailure(response);
        }
      });
    }
        
  }
});
