CarmenImportData = window.CarmenImportData || {};

function positionMarkLibrary(){/*Marqueur de position pour le code global*/}
/**
 * Liste des fonctions appliquées sur les composants 
 * @type Object
 */
CarmenImportData.library = Ext.apply(CarmenImportData.library||{}, {
  
	afterLayerAddToWebService : window.afterLayerAddToWebService = function(pk_couche_donnees, data, window){
    var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
    if ( !store ) return;
    var record = store.findRecord('pk_couche_donnees', pk_couche_donnees);
    if ( !record ) return;
    record.set(data, {commit:true});
    if ( window ) window.close();
	},
  /*FORM GRID VIEW*/
  timestampRenderer: function(val) {
    return val;
  },
  
  ouiNonrenderer: function(val, meta, record) {
  	if ( record instanceof CoucheDonnees )
      return (!!val ? 'Oui' : 'Non');
    return val;
  },
  
  importZIP : function(){
    var formGrid = this;
    var windowClass = "Ext.ux.FileBrowserWindow";
    var cmpId = windowClass.replace(/\./g, '-');
    var window = Ext.getCmp(cmpId) 
      || Ext.create(windowClass, {
        id: cmpId,
        selectExtension: FILETYPES['ZIP'].selectExtension,
        uploadExtension: FILETYPES['ZIP'].uploadExtension,
        dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
        defaultPath: '/Root/Publication/',
        relativeRoot: '/Root/Publication/temp',
        listeners: {  
          selectedfile: CarmenImportData.library.readSelectedArchive,
          scope : formGrid
        }
      });
    window.show();
  
  },
  
  onSelectionChange: function(model, rec) {
    var formGrid = this;
    var form = formGrid.getForm();
    if ( rec && rec instanceof CarmenImportData.model.Metadata ) return;
    if (rec && form) {
      if ( form.getRecord() && form.getRecord()!=rec && form.isDirty()  ){
        Ext.Msg.confirm(
          'Changement de couche éditée - Sauvegarde avant remplacement'
        , 'Une couche est déjà en cours d\'édition.' +
          '<br>Voulez-vous sauvegarder sa configuration avant de la remplacer par la couche sélectionnée ?'
        , function(btn){
            if ( btn=='yes' ){
              if ( !CarmenImportData.library.saveLayerConfiguration.call(formGrid) ) return;
            } else {
              CarmenImportData.library.cancelLayerConfiguration.call(formGrid);
            }
            CarmenImportData.library.loadLayer.call(formGrid, rec);
          }, formGrid)
        
      } else {
        CarmenImportData.library.loadLayer.call(formGrid, rec);
      }
    }
  },
  
  createLayerOnMetadata : function(grid, rowIndex, colIndex, item, evt, record, htmlRow){
    var formGrid = this;
  	return CarmenImportData.library.createLayer.call(formGrid, record, OPEN_SERVER_BROWSER);
  },
  
  
  createLayer : function(parentNode, actionOnCreate){
    var formGrid = this;
    var grid = formGrid.getGrid(),
        store = grid.getStore(),
        prefix = 'Couche de données n°';
    parentNode = (parentNode instanceof Ext.data.TreeModel ? parentNode : store.getRootNode());
    var childCount = parentNode.childNodes.length;
    var maxCount = childCount;
    parentNode.eachChild(function(record){
      var couchd_nom = record.get('couchd_nom');
      if ( couchd_nom.indexOf(prefix)!=-1 ){
        maxCount = Math.max(maxCount, parseInt(couchd_nom.replace(prefix, '')));
      }
    });
    var title = (parentNode.isRoot() ? (Carmen.metadata ? Carmen.metadata.metadata_title : 'Couche ') : parentNode.get('nom'));
    var record = Ext.create('CarmenImportData.model.CoucheDonnees', {
      couchd_nom : title + (childCount==0 ? '' : ' - Donnée n°'+(maxCount+1))
    });
    
    parentNode.appendChild([record]);
    parentNode.expand();
    grid.getView().focusRow(record);
    !formGrid.getLayer() && CarmenImportData.library.loadLayer.call(formGrid, record, actionOnCreate);
  },
  
  loadLayer : function(record, actionOnCreate){
    var formGrid = this;
    formGrid.getForm().up('panel').setDisabled(record===null);
    formGrid.getForm().setDisabled(record===null);
    var res = (record ? formGrid.getForm().loadRecord(record) : !(formGrid.getForm().getForm()._record=null));
    CarmenImportData.library.setLayerType.call(formGrid, (record ? record.getLayerType() : formGrid.layertype), true);
    formGrid.fireEvent('recordloaded');
    if ( record ){
      if ( actionOnCreate==OPEN_SERVER_BROWSER ){
        formGrid.down('[name=file_source_button]').openServerBrowser();
      }
      if ( actionOnCreate==OPEN_CLIENT_BROWSER ){
      	formGrid.down('[name=file_source_button]').openClientBrowser();
      }
    }
    return res;
  },
  
  deleteLayer : function(grid, rowIndex, colIndex, item, evt, record, htmlRow){
    var formGrid = this;
    var store = grid.getStore();
    var name = '"'+record.get('couchd_nom')+'"'+ (record.get('couchd_emplacement_stockage') ? ' ['+record.get('couchd_emplacement_stockage')+']' : '');
    Ext.Msg.confirm("Suppression d'une couche", "Confirmez-vous la suppression de la couche "+name+' ?', function(btn){
      if (btn!="yes") return;
      if ( Ext.Number.from(record.get('pk_couche_donnees'), 0)==0 ){
        if ( formGrid.getLayer()===record ) CarmenImportData.library.cancelLayerConfiguration.call(formGrid);
        record.remove();
        grid.refresh();
        return;
      }
      
      grid.mask('Suppression en cours...');
      Ext.Ajax.request({
        method : 'GET',
        type : 'json',
        timeout: 500000,
        url : Routing.generate('carmen_ws_deletelayer', {
          fmeta_id : Carmen.metadata.id,
          pk_couche_donnees : record.get('pk_couche_donnees')
        }),
        success : function(response){
          grid.unmask();
          response = Ext.decode(response.responseText);
          if ( response.success ){
            if ( formGrid.getLayer()===record ) CarmenImportData.library.cancelLayerConfiguration.call(formGrid);
            record.remove();
            grid.refresh();
          } else {
            Ext.Msg.alert('Echec de suppression de la couche '+name, response.message);
          }
        },
        failure : function(){
          grid.unmask();
        }
      })
    });
  },
  
  onLayerLoaded : function(){
    var formGrid = this;
    var importaction;
    var record = formGrid.getLayer();
    var couchd_emplacement_stockage = Ext.ComponentQuery.query('[name=couchd_emplacement_stockage]')[0];
    if ( couchd_emplacement_stockage) {
      couchd_emplacement_stockage.originalValue = (record ? record.get('original_couchd_emplacement_stockage'): null);
      couchd_emplacement_stockage.old_value = null;
    }
    if ( !record || record.get('import_action')=="create" ){
      importaction = 'new';
      Ext.getCmp('import_action_create').setValue(true);
    } else {
      importaction = 'created'; 
      record_action = record.get('import_action') || "update";
      if ( record.get('unique_fields').getCount()==0 ){
        record_action = "replace";
      }
      Ext.getCmp('import_action_'+record_action).setValue(true);
    }
    /*hide form field if it is not listed for the current record state (new/created) */
    Ext.each(formGrid.query('[importaction]'), function(field){
      if ( field.checkImportAction ) {
        field.checkImportAction(importaction)
      } else {
        field.setVisible( !field.importaction || field.importaction==importaction );
      }
    });
    if ( formGrid.getForm().up().rendered ){
      if ( record ) {
      	
        formGrid.getGrid().setDisabled(true);
        
        formGrid.up("form-importdata").setScrollY(formGrid.getForm().down('button:last').getY());
        formGrid.getForm().down('field').focus();
        formGrid.getForm().up().setTitle('Paramétrage - '+record.get('couchd_nom')+(" ["+record.getTypePath().join(' / ')+"]"));
      } else {
        formGrid.getGrid().setDisabled(false);
        formGrid.up("form-importdata").setScrollY(0);
        formGrid.getForm().up().setTitle(DEFAULT_TITLE);
      }
    }
    
    if ( Ext.getCmp('showData') ) Ext.getCmp('showData').close();
      
    var fullPathName = formGrid.down('[name=file_name]').getValue();
    var relativePath = formGrid.down('[name=file_source]').getValue();
    var fileSheet = (formGrid.down('[name=file_sheet]').isDisabled() ? null : formGrid.down('[name=file_sheet]').getValue());
    if ( record && record.get('autoLoad') && fullPathName && relativePath ){
      CarmenImportData.library.readSelectedFile.call(formGrid, null, null, null, fullPathName, relativePath, null, fileSheet || null);
    }
    
    
  },
  
  saveLayerConfiguration : function(record){
    var formGrid = this;
    var form = formGrid.getForm();
    if ( !form ) return false;
    if ( !form.getRecord() ) return false;
    if ( !form.isValid() ) {
      var errors = [];
      var glue = '';
      form.getForm().getFields().each(function(field){
        if ( !field.validate() /*&& (field.isDisabled && !field.isDisabled()) && (field.isReadOnly && !field.isReadOnly())*/ ){
          var previousNode;
          var label = field.boxLabel || field.fieldLabel
                   || (field.is('field')
                     ? field.up().boxLabel || field.up().fieldLabel
                       || ((previousNode = field.previousNode('[fieldLabel], [boxLabel]')) ? previousNode.boxLabel || previousNode.fieldLabel: field.name)
                     : '');
          if (!label)  return;
         // console.log(field, previousNode);
          errors.push( [glue, label, ' : ', field.getActiveErrors().join(',<br>')].join('') );
          glue = '<br/><br/>'; 
        }
      })
      if ( errors.length ){
        Ext.Msg.alert("Erreurs rencontrées", "Des erreurs existent sur le(s) champ(s) : <li>"+errors.join('</li><li>')+"</li>");
      }
      return false;
    }
    form.updateRecord(); 
    var status = form.getRecord().get('import_status').split(/<br\/?>/);
    var keep = "import";
    var modif = 'Configuration modifiée';
    var old_modif = status.indexOf(modif)!=-1;
    var tmp = [];
    for (var i=0; i<status.length; i++){
      if ( status[i].toLowerCase().indexOf(keep)==0 && status[i].toLowerCase().indexOf("importation")==-1 && status[i].toLowerCase().indexOf("(")==-1){
        tmp.push(status[i]);
      }
//      if ( status[i].toLowerCase().indexOf("configuration")==-1 && ' '+status[i].toLowerCase().indexOf(" erreur ")==-1 ){
//        tmp.push(status[i]);
//      }
    }
    if ( old_modif || form.isDirty() ) 
      tmp.push(modif);
      
    form.getRecord().set('import_status', tmp.join('<br/>'));
    form.getRecord().set('color', '');
    form.getRecord().set('autoLoad', false);
    form.getRecord().commit();
    CarmenImportData.library.loadLayer.call(formGrid, null);
    return true;
  },
  
  cancelLayerConfiguration : function(){
    var formGrid = this;
    var form = formGrid.getForm();
    if ( !form ) return;
    CarmenImportData.library.loadLayer.call(formGrid, null);
  }, 

  readSelectedFile : function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath, fileSheet){
    var formGrid = this;
    if ( !Ext.isDefined(fileSheet) || !Ext.isString(fileSheet) ) fileSheet = null;
    
    var ctrlTo = formGrid.down('[name=file_name]');
    ctrlTo.setValue(fullPathName);
    var ctrlTo = formGrid.down('[name=file_source]');
    ctrlTo.setValue(relativePath);
    wind && wind.close();
    
    var layertype = formGrid.getLayer().getLayerType().toUpperCase();
    if ( !FILETYPES[layertype].readFile ) return;
    if ( relativePath=="" || relativePath==null ) return;
    
    formGrid.down('[name=layerfields]  grid').mask('Détection des champs par analyse du fichier...');
    
    var data = formGrid.getLayer().getData({persist : true, serialize : true});
    
    if ( Ext.getCmp('showData') ) Ext.getCmp('showData').close();
    Ext.Ajax.request({
      method : 'GET',
      type : 'json',
      url : Routing.generate('carmen_ws_readfile', {
        pk_couche_donnees : data.pk_couche_donnees,
        couchd_type_stockage : data.couchd_type_stockage,
        layertype : layertype, 
        layerfile : encodeURIComponent(relativePath),
        layersheet : (fileSheet=="" ? null : fileSheet)
      }),
      success : function(response){
        response = Ext.decode(response.responseText);
        response.encoding && formGrid.down('[name=encoding_detected]').setValue(response.encoding);
        formGrid.down('[name=sheets]').setValue(response.sheets);
        if ( response.sheets.length>0 && !fileSheet ){
          formGrid.down('[name=layerfields]').down('grid').getView().emptyText = '<div class="x-grid-empty">Sélectionnez la feuille du fichier</div>';
        } else {
          formGrid.down('[name=layerfields]').down('grid').getView().emptyText = '<div class="x-grid-empty">Aucun champ</div>';
        }
        formGrid.down('[name=layerfields]').setValue(response.layerfields);
        formGrid.down('[name=layerfields]  grid').unmask();
        
        if ( response.import_action ){
          var import_actions = response.import_action.split(',');
          var current_action = formGrid.down('[name=import_action][value=true]');
          if ( current_action ){
            if ( !current_action.isVisible() || current_action.isDisabled() )
              current_action = null;
            else
              current_action = current_action.inputValue;
          } 
          else current_action = null;
          var available_actions = ['create', 'update', 'replace'];
          if (formGrid.getLayer() && !formGrid.getLayer().get('unique_fields').getCount()){ Ext.Array.remove(import_actions, 'update'); }
          
          Ext.each(available_actions, function(action){
            Ext.getCmp('import_action_'+action).setDisabled(false);
            if ( import_actions.indexOf(action)==-1 ){
              Ext.getCmp('import_action_'+action).setDisabled(true);
            } else if ( import_actions.indexOf(action)==0 ){ 
              Ext.getCmp('import_action_'+action).setValue(true);
            }
          })
          if ( current_action && import_actions.indexOf(current_action)!=-1 ){ 
            Ext.getCmp('import_action_'+current_action).setValue(true);
          }
        }
      },
      failure : function(){
        formGrid.down('[name=layerfields]  grid').unmask();
      }
    })
  },
  
  /**
   * Action au niveau de la métadata d'entrée
   * @param {} wind
   * @param {} fileRecord
   * @param {} fileName
   * @param {} fullPathName
   * @param {} relativePath
   * @param {} absolutePath
   */
  readSelectedArchive : function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath){
    var formGrid = this;
    
    wind.close();
    formGrid.mask('Analyse du fichier ZIP '+fullPathName+" en cours...");
    
    Ext.Ajax.request({
      method : 'GET',
      type : 'json',
      timeout :  24*60*60*1000,
      url : Routing.generate('carmen_ws_readzip', {
        uuid : Carmen.metadata.uuid,
        layertype : formGrid.up().down('[name=metadata_layertype]').getValue().toUpperCase(), 
        filezip : encodeURIComponent(relativePath) 
      }),
      success : function(response){
        response = Ext.decode(response.responseText);
        formGrid.unmask();
        if ( !Ext.isArray(response.children) ) return;
        var root = formGrid.getGrid().getStore().getRoot();
        response.children.forEach(function(child){
        	root.appendChild(root.createNode(child));
        });
      },
      failure : function(){
        formGrid.unmask();
      }
    });
  },
  
  setLayerType : function(layertype, byRecord){
    var formGrid = this;
    formGrid.layertype = layertype;
    
    Ext.each(Ext.ComponentQuery.query('[exclude_layertype], [including_layertype], [checkLayerType]', formGrid), function(component){
      /*hide the form field if it is listed for the excluded layertype*/
      component.setVisible( (!component.exclude_layertype || component.exclude_layertype!=layertype) || (component.including_layertype && component.including_layertype.indexOf(layertype)!=-1) );
      if ( Ext.isDefined(component.allowBlank) ){
        if ( !Ext.isDefined(component.default_allowBlank) ) component.default_allowBlank = component.allowBlank; 
        component.allowBlank = ( !component.exclude_layertype || component.exclude_layertype!=layertype ? component.default_allowBlank : true );
      }
      
      if ( component.checkLayerType ){component.checkLayerType(layertype);}
    });
    if ( !byRecord ){
      formGrid.getForm().setDisabled(true);
      formGrid.getGrid().getStore().removeAll();
      formGrid.getForm().reset();
      formGrid.fireEvent('recordloaded');
    }
    return formGrid;
  },
  
  /*IMPORT DATA VIEW*/
  initialiserImportData : function(importDataView){
    return function(){
      var btn = this;
      if ( !(Carmen.metadata && Carmen.metadata.id) ) return;
      var layertype = importDataView.down('[name=metadata_layertype]');
      if ( layertype.getValue()=="join" ){
        if ( !importDataView.down('form').isValid() ) return;
        //metadata_id, desc_vue, nom_vue, urlBack, color_theme_id, svrAdmin
        var params = {
          pk_view : Carmen.metadata.id,
          view_title : Carmen.metadata.metadata_title,
          view_name : importDataView.down('[name=view_name]').getValue(),
          url_back : window.location.href,
          view_type : -4
        }
        
        var jsonParams = Ext.encode(params);
        var token = TextEncode(jsonParams);
        
        var url = Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/join/" + token + "?casuser="+Carmen.user.username;
      
        layertype.setReadOnly(true);
        importDataView.down('[name=view_name]').setReadOnly(true);
        var win = window.open(url, 'administration_carto_adminView');
        win.focus();
        return;
      };
      
      
      var doAction = function(){
        var insert = !importDataView.formGrid;
        
        
        importDataView.formGrid = importDataView.formGrid || Ext.create('CarmenImportData.view.form.FormGrid', {
          buttons : CarmenImportData.library.getButtonsImport.call(importDataView)
        });
        importDataView.formGrid.on('afterrender', function(){
          CarmenImportData.library.setLayerType.call(importDataView.formGrid, layertype.getValue());
          importDataView.formGrid.loadStore();
        })
        insert && importDataView.add(importDataView.formGrid);
        layertype.setReadOnly(true);
        btn.hide();
      };
      if ( btn.confirm ){
        Ext.Msg.confirm('Initialisation des données', 'ATTENTION : Le choix du type de données est non modifiable ultérieurement.<br><br>Confirmez-vous votre choix ?', function(confirm){
          if ( confirm == "yes" ){
            doAction();
          }
        });
      } else {
        doAction();
      }
    };
  },
  
  /*VIEW ACTIONS*/
  
  getButtonsImport : function(){
    var importDataView = this;
    return [{
      text : "Importer immédiatement",
      scope : importDataView,
      handler : CarmenImportData.library.directImport
    }, {
      text : "Différer l'import",
      scope : importDataView,
      handler : CarmenImportData.library.deferImport
    }, {
      text : "Fermer",
      scope : importDataView,
      handler : CarmenImportData.library.cancelImport
    }]
  },
  
  directImport : function(){
    var importDataView = this;
    var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
    if ( !store ) return;
    var grid = importDataView.formGrid.getGrid();
    var form = importDataView.formGrid.getForm().up();
    grid.getSelectionModel().deselectAll();
    var doSave = function(){
      grid.suspendEvents(false);
      grid.getSelectionModel().suspendEvents(false);
      form.mask('Import des données en cours...');
      var index = 0;
      var allCount = store.getCount();
      store.each(function(couche){
      	if ( !(couche instanceof CoucheDonnees) ) {index++;return;}
        //if ( !couche.get('file_source') ) return;
        var data = couche.getData({persist : true, serialize : true});
        var rowElt =grid.getView().getCell(couche, grid.down('[dataIndex=status]'));
        rowElt.mask('Import en cours...');
        var endFunction = function(){
           rowElt.unmask();
          index++;
          if ( index==allCount ){
            form.unmask();
            grid.resumeEvents();
            grid.getSelectionModel().resumeEvents();
          }
        };
        Ext.Ajax.request({
          method : 'POST',
          url : Routing.generate('carmen_ws_directimport', {
            layertype : couche.getLayerType().toUpperCase(), 
            uuid : couche.getUUID() 
          }),
          timeout :  24*60*60*1000,
          jsonData : data,
          success : function(response){
          	try {
              response = Ext.decode(response.responseText);
          	} catch(error){endFunction();return;}
            if ( !(response instanceof Object) ) {endFunction();return;}
            if ( response.status=="SUCCESS" ){
              couche.set('color', 'green');
            } else if ( response.status=="FAILURE" ){
                couche.set('color', 'red');
                if(response.import_result && response.import_result == "Fail, table name already exists") {
                    Ext.Msg.alert(response.import_status);
                    Ext.Ajax.abortAll();
                }
                couche.set('details', response.data);
            } else {
              couche.set('color', '');
            }

            couche.set(response.result);
            couche.commit();
            endFunction();
          },
          failure : function(){
            endFunction();
          }
        });
      });
    };
    
    grid.expandAll(function(){
      if ( importDataView.formGrid.getLayer() ){
        Ext.Msg.confirm("Donnée en cours d'édition", "Une donnée est en cours d'édition. Voulez-vous valider sa configuration actuelle ?"
        , function(btn){
            if ( btn=="yes" ){
              if ( !CarmenImportData.library.saveLayerConfiguration.apply(importDataView.formGrid) ) return;
            } else {
              CarmenImportData.library.cancelLayerConfiguration.apply(importDataView.formGrid)
            }
            doSave();
          }, importDataView)
      } else {
        doSave();
      }
    });
  },
  
  deferImport : function(){
    var importDataView = this;
    var store = Ext.StoreMgr.lookup('CoucheDonneesStore');
    if ( !store ) return;
    var grid = importDataView.formGrid.getGrid();
    var form = importDataView.formGrid.getForm().up();
    grid.getSelectionModel().deselectAll();
    var doSave = function(){
      grid.suspendEvents(false);
      grid.getSelectionModel().suspendEvents(false);
      var index = 0;
      var submit = [];
      store.each(function(couche){
        //if ( !couche.get('file_source') ) return;
        submit.push(couche.getData({persist : true, serialize : true}));
      });
    
      var endFunction = function(){
        grid.resumeEvents();
        grid.getSelectionModel().resumeEvents();
      };
        
      Ext.Ajax.request({
        method : 'POST',
        url : Routing.generate('carmen_ws_deferimport', {
          layertype : importDataView.down('[name=metadata_layertype]').getValue().toUpperCase(), 
          uuid : Carmen.metadata.uuid 
        }),
        timeout :  24*60*60*1000,
        jsonData : {couches : submit},
        success : function(response){
          try {
            response = Ext.decode(response.responseText);
          } catch(error){endFunction();return;}
          if ( !(response instanceof Object) ) {endFunction();return;}
          store.each(function(couche, index){
            var result = response.results[index];
            if ( !Ext.isDefined(result) || !(result instanceof Object) ) return;
            couche.set(result);
            couche.commit();
          });
          endFunction();
        },
        failure : function(){
          endFunction();
        }
      });
    };
    
            
    if ( importDataView.formGrid.getLayer() ){
      Ext.Msg.confirm("Donnée en cours d'édition", "Une donnée est en cours d'édition. Voulez-vous valider sa configuration actuelle ?"
      , function(btn){
          if ( btn=="yes" ){
            if ( !CarmenImportData.library.saveLayerConfiguration.apply(importDataView.formGrid) ) return;
          } else {
            CarmenImportData.library.cancelLayerConfiguration.apply(importDataView.formGrid)
          }
          doSave();
        }, importDataView)
    } else {
      doSave();
    }
  },
  
  confirmBeforeClose : function(){
  	return ("Voulez-vous fermer la fenêtre et perdre toutes les modifications apportées ?");
  },
  
  cancelImport : function(){
  	window.close();
  },
  
  editMap : function(){
    var formGrid = this;
    if ( !Carmen.metadata ) return;
    window.open(Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/geosource/layerAddToMap/"+ Carmen.metadata.uuid);
  },
  resetMap : function(btn){
    var formGrid = this;
    if ( !Carmen.metadata ) return;
    
    btn.up('splitbutton').mask();
    Ext.Ajax.request({
      cors: true,
      crossDomain: true,
      withCredentials : true,
      url : Carmen.Defaults.CATALOGUE_URL.replace(/\/$/g, "") + "/geosource/mapDelete/"+ Carmen.metadata.uuid+'?casuser='+Carmen.user.username,
      success : function(oResUserRights) {
      	btn.up('splitbutton').unmask();
        //Ext.Msg.alert("La représentation par défaut a été réinitialisée");
        Ext.Msg.alert('Représentation par défaut', "La représentation par défaut a été réinitialisée", function(){});
      },
      failure : function() {
        btn.up('splitbutton').unmask();
        Ext.Msg.alert('Représentation par défaut', "La représentation par défaut n'a pas été réinitialisée", function(){}).setIcon(Ext.Msg.ERROR);
      }
    });
  },
  
  changeTableName : function(button, evt, additionnalMsg){
    var formGrid = this;
    if ( !additionnalMsg ) additionnalMsg = "";
    
    var record = formGrid.getLayer();
    if ( !record ) return;
    var old_value = record.get('couchd_emplacement_stockage');
    var prompt = Ext.Msg.prompt('Renommage de la table de stockage', additionnalMsg+
      '<b>Attention</b> : Cette action est immédiate sur les données.' +
      '<br/><br/>' +
      'Saisissez le nouveau nom de table : ',
      function(btn, value){
        if ( btn!="ok" ) return;
        if ( value == old_value ){
          CarmenImportData.library.changeTableName.call(formGrid, null, null, "<b>Indiquez un nouveau nom de table.</b><br/><br/>");
          return;
        }
        
        prompt.hide();
        formGrid.mask('Renommage en cours...');
        Ext.defer(function(){
          Ext.Ajax.request({
            url : Routing.generate('carmen_ws_changetablename', {
              pk_couche_donnees : Ext.Number.from(formGrid.getLayer().get('pk_couche_donnees'), -1), 
              new_tablename : value , 
              old_tablename : old_value 
            }),
            type : 'json',
            failure : function(){
              formGrid.unmask();
            },
            success : function(response){
              formGrid.unmask();
              response = Ext.decode(response.responseText);
              if ( !response.success ){
                Ext.Msg.alert("Changement du nom de la table de stockage", response.message).setIcon(Ext.Msg.ERROR);
                return;
              }
              Ext.Msg.alert("Changement du nom de la table de stockage", "Renommage réussi").setIcon(Ext.Msg.INFO);
              formGrid.getLayer().set('couchd_emplacement_stockage', response.tablename, {commit : true});
              formGrid.down('[name=couchd_emplacement_stockage]').setValue(response.tablename);
            }
          });
        }, 1000)
      },
      formGrid, 
      false,
      old_value
    );
      
  },
  
  verify_unicity_emplacement : function(textfield){
    if ( !textfield || !textfield.isReadOnly || textfield.isReadOnly() ) return;
    var formGrid = this;
    var old_value = textfield.originalValue;
    var new_value = textfield.getValue();
    if ( !new_value ) return;
    var radios = Ext.ComponentQuery.query('radio[name=import_action][importaction]');

    var onSuccess = function(unique, table_exists){
      if ( !unique ){
        textfield.existing = true;
        textfield.markInvalid ( textfield.getErrors(new_value) );
        Ext.each(radios
        , function(radio){
            radio.setDisabled(true);
            radio.setValue(false);
          }
        );
        return;
      } 

      var importaction = "created";
      if ( !table_exists ) importaction = "new";
      Ext.each(radios
      , function(radio){
          radio.checkImportAction(importaction);
        }
      );
      if ( importaction=="new" ){
        Ext.getCmp('import_action_create').setValue(true);
      } else if ( !Ext.getCmp('import_action_replace').getValue() ){
        Ext.getCmp('import_action_update').setValue(true);
      }
    };
    if ( new_value && new_value!=textfield.old_value ){
      
      //reinit config
      textfield.existing = false;
      var errors = textfield.getErrors(new_value);
      if ( errors.length ) textfield.markInvalid(errors)
      else                 textfield.clearInvalid();
      Ext.each(radios
      , function(radio){
          radio.setDisabled(false);
        }
      );
      
      
      Ext.Ajax.request({
        async : false,
        url : Routing.generate('carmen_ws_uniquestorage', {
          pk_couche_donnees : Ext.Number.from(formGrid.getLayer().get('pk_couche_donnees'), -1), 
          couchd_emplacement_stockage : new_value 
        }),
        type : 'json',
        timeout :  24*60*60*1000,
        failure : function(){onSuccess(true, formGrid.getLayer().get('import_action'));},
        success : function(response){
          response = Ext.decode(response.responseText);
          onSuccess(response.unique, response.table_exists);
        }
      });
      textfield.old_value = new_value;
    }
  },
    
  fields_actions : {
    getCoucheEmplacement : function(formGrid, layertype){
      return {
        couchd_emplacement_stockage : {
          checkImportAction : function(importaction){
            var field = this;
            field.setReadOnly(field.importaction!=importaction);
            field.allowBlank = !field.isVisible() || (field.importaction!=importaction);
          },
          listeners : {
            beforerender : function add_specific_error(){
              var field = this;
              Ext.override(field, {
                isValid: function(new_value){
                  return field.callParent(arguments) && !field.existing;
                },
                getErrors: function(new_value){
                  var error = 'Ce nom de table est déjà utilisé par une autre donnée.';
                  var errors = this.callParent(arguments) || [];
                  if ( field.existing ){
                    errors.push(error);
                  } else {
                    Ext.Array.remove(errors, error);
                  }
                  return errors;
                }
              });
            },
            
            blur         : {
              fn : CarmenImportData.library.verify_unicity_emplacement,
              scope : formGrid
            }
          }
        },
        action_change_tablename : {
          handler : CarmenImportData.library.changeTableName,
          scope : formGrid
        }
        
//      , action_show_database : {
//        	hidden : true,
//          handler : function(){
//            CarmenImportData.library.showData.call(formGrid, 'database');
//          },
//          scope : formGrid
//        }
      };
    },
    
    getCoucheImportAction : function(formGrid, layertype){
      return {
        create : {
          checkLayerType : function(layertype){
            var label = (this.labels[layertype.toLowerCase()] || this.labels.others);
            this.setBoxLabel(label);
          },
          checkImportAction : function(importaction){
            var field = this;
            var old_disabled = this.isDisabled();
            this.setDisabled(this.importaction!=importaction);
            if ( old_disabled!=this.isDisabled() && !this.isDisabled() )this.setValue(true);
          }
        },
        
        update : {
          fieldcontainer : {
            checkImportAction : function(importaction){
              var field = this;
              var disabled = this.importaction!=importaction || this.down('combo').getStore().getCount()==0;
              Ext.each(this.query('field'), function(){
                var old_disabled = this.isDisabled();
                this.setDisabled(disabled);
                if ( old_disabled!=this.isDisabled() && !this.isDisabled() && this.is('radio') ){
                  this.setValue(true);
                }
              })
            },
          
            isFormField : true,
//            resetOriginalValue: function() {
//                this.originalValue = this.getValue();
//                this.validate();
//            },
            reset : function(){
              var field = this;
              this.down('combo').getStore().removeAll();
              //this.down('combo').reset();
            },
            isDirty : function(){
              var field = this;
              return this.down('combo').isDirty();
            },
            getValue : function(){ 
              var field = this;
              return this.down('combo').getStore().getRange();
            },
            getModelData : function(){
              var field = this;
              return {};{unique_fields : this.getValue()};
            },
            getSubmitData : function(){
              var field = this;
              return {};{unique_fields : this.getValue()};
            },
            setValue : function(value){ 
              var field = this;
              var combo = this.down('combo');
              var comboValue = combo.getValue();
              combo.setValue(null);
              var store = combo.getStore();
              store.removeAll();
              if ( value instanceof Array )
                store.loadRawData(value);
              else if ( value instanceof Ext.data.Store ){
                store.loadData(value.getRange());
              }
              combo.setValue(comboValue);
              this.checkImportAction('created');
            },
            isValid : function(){var field = this; return !this.down('radio').getValue() || this.down('combo').isValid() },
            validate : function(){var field = this; return this.isValid();}
          },
          radio : {
            listeners : {
              change : function(radio, checked){
                var field = this;
                radio.nextSibling('combo').allowBlank = !checked;
                radio.nextSibling('combo').setDisabled(!checked);
              },
              afterrender: function(me) {
                 // Register the new tip with an element's ID
                 Ext.tip.QuickTipManager.register({
                   target: me.getId(), // Target button's ID
                   title : me.boxLabel, 
                   text  : "La couche doit posséder la même structure (même nom des champs, même type de champs) que la table dans PostGIS." +
                           "<br/>Cette opération ajoute les lignes qui ne sont pas déjà dans la table en se basant sur la clé."
                 });
              }
            }
          },
          combo : {}
        },
        
        replace : {
          checkLayerType : function(layertype){
            var label = (this.labels[layertype.toLowerCase()] || this.labels.others);
            this.setBoxLabel(label);
          },
          checkImportAction : function(importaction){
            var field = this;
            this.setDisabled(this.importaction!=importaction);
          },
          listeners : {
            afterrender: function(me) {
               // Register the new tip with an element's ID
               Ext.tip.QuickTipManager.register({
                 target: me.getId(), // Target button's ID
                 title : me.boxLabel, 
                 text  : "La couche doit posséder la même structure (même nom des champs, même type de champs) que la table dans PostGIS." +
                         "<br/>Tous les enregistrements sont supprimés puis l'import est fait à partir de la couche."
               });
            }
          }
        }
      };
    },//getCoucheImportAction
    
    getCoucheVisibility : function(formGrid, layertype, type){
      return {
        active_webservice : {
          setValue : function(value){
            var field = this;
            if ( this.rendered ) Ext.form.field.Checkbox.prototype.setValue.call(this, !value); 
            Ext.form.field.Checkbox.prototype.setValue.call(this, value)
          },
          listeners : {
            change : function(checkbox, checked){
              var field = this;
              formGrid.down('[name=config_'+type+']').setDisabled(!checked);
            }
          }
        },
        configure_webservice : function(checkbox, rowIndex){
          var btn = this;
          var couche = Ext.StoreMgr.lookup('CoucheDonneesStore').getAt(rowIndex);
          if ( !couche ) return;
          if ( !(couche instanceof CoucheDonnees) ) return;
          var pk_couche_donnees = Ext.Number.from(couche.get('pk_couche_donnees'), -1);
          var url = Carmen.Defaults.AddToWebServices;
          if ( pk_couche_donnees==-1 ){
          	Ext.Msg.alert('Traitement impossible', "Cette couche n'est pas encore enregistrée et ne peut faire l'objet d'un paramétrage de la diffusion sur le serveur "+type.toUpperCase()+".").setIcon(Ext.Msg.ERROR);
          	checkbox.setValue(false);
          	return;
          }
          var params = {
            ACTION : type,
            uuid : couche.getUUID(),
            couchd_id : pk_couche_donnees,
            //couche_nom : couche.get('couchd_nom'),
            DATA : couche.get('couchd_emplacement_stockage'),
            TYPE_STOCKAGE : Carmen.Defaults.DATATYPE_TO_STOCKAGE[couche.get('couchd_type_stockage')] || couche.get('couchd_type_stockage'),
            casuser : Carmen.user.username,
            OPENER : encodeURIComponent(window.location.origin+Routing.generate('carmen_ws_refreshdiffusion', {
              pk_couche_donnees : pk_couche_donnees,
              callback : 'this.opener && this.opener.afterLayerAddToWebService'
            }))
          };
          var win = window.open(url+'?'+Ext.Object.toQueryString(params), type);
          return false;
        }
      };
    }//getCoucheVisibility
    
    
  }
  
//, showData : function(origin){
//    var formGrid = this;
//    var layerfields = formGrid.down('[name=layerfields]').getValue()
//        , fields = [], columns = [];
//        
//    var layertype = formGrid.up().down('[name=metadata_layertype]').getValue().toUpperCase();
//    var relativePath = formGrid.down('[name=file_source]').getValue();
//    if ( origin=='file' && (relativePath=="" || relativePath==null) ) return;
//    
//    var fileSheet = (!formGrid.down('[name=file_sheet]').isDisabled() ? formGrid.down('[name=file_sheet]').getValue() || null : null);
//    var data = formGrid.getLayer().getData({persist : true, serialize : true});
//    
//    Ext.each(layerfields, function(field){
//      var field_name = field.get('field_name');
//      fields.push(field_name);
//      columns.push({header : field_name, dataIndex : field_name, shrinkWrap :1});
//    });
//    
//    var args = {}, dataOrigin;
//    if ( origin=="file" ){
//    	dataOrigin = " du fichier "+formGrid.down('[name=file_name]').getValue();
//    	args = {
//        pk_couche_donnees : data.pk_couche_donnees,
//        couchd_type_stockage : data.couchd_type_stockage,
//        layertype : layertype, 
//        layerfile : encodeURIComponent(relativePath),
//        layersheet : (fileSheet=="" ? null : fileSheet)
//      };
//    } else {
//      args = {
//        couchd_emplacement_stockage : formGrid.down('[name=couchd_emplacement_stockage]').getValue()
//      };
//      dataOrigin = " de la table "+args.couchd_emplacement_stockage;
//    }
//
//    var store = Ext.create('Ext.data.JsonStore', {
//      autoLoad : true,
//      fields : fields, 
//      pageSize: 20,
//      listeners: {
//        'metachange': function(store, meta) {
//          Ext.getCmp('showData').down('grid').reconfigure(store, meta.columns);
//        }
//      },
//      proxy : {
//        type : 'ajax',
//        enablePaging: true,
//        reader: {
//          rootProperty: 'data',
//          totalProperty: 'total',
//          metaProperty : 'metaData'
//        },
//        url : Routing.generate('carmen_ws_data_'+origin, args)
//      }
//    });
//    
//    Ext.create('Ext.window.Window', {
//      closeAction : 'destroy',
//      id : 'showData',
//      autoShow : true,
//      width : 800,
//      title : 'Visualisation des données'+dataOrigin,
//      layout : 'fit',
//      items : [{
//        columnLines : true,
//        xtype : 'grid',
//        reserveScrollbar : true,
//        height : 500,
//        width : '100%',
//        viewConfig : {stripeRows : true, emptyText : 'Aucune donnée'},
//        columns : columns,
//        bbar: {
//          xtype: 'pagingtoolbar',
//          pageSize: 10,
//          store: store,
//          displayInfo: true
//        },
//        store : store,
//        listener : {
//          afterRender: function(){
//              this.getStore().load();
//          }
//        }
//      }]
//    }).show();
//  }    
});

function positionMarkViews(){/*Marqueur de position pour le code global*/}
/**
 * This Form demonstrates the fact that by virtue of inheriting from the Ext.Container
 * class, an Ext.form.Panel can contain any Ext.Component. This includes all the
 * subclasses of Ext.Panel, including the GridPanel.
 *
 * The Grid demonstrates the use of creation of derived fields in a Record created using a
 * custom `convert` function, and the use of column renderers.
 *
 * The Form demonstrates the use of radio buttons grouped by name being set by the value
 * of the derived rating field.
 */
Ext.define('CarmenImportData.view.form.FormGrid', {
    extend: 'Ext.panel.Panel',
    xtype: 'form-grid',
    
    layertype : CarmenImportData.Datatypes.enums.vector,
        
    
    initGrid : function(config){
      var formGrid = this;
      var layertype = this.layertype;
      var grid = {
        reserveScrollbar : true,
        forceFit : true,
        rootVisible: false,
        rowLines : true,   
          //region : 'center',
        id : 'importdatagrid',
        tbar : {
          defaultButtonUI : 'default',
          items : [{
          	xtype :'tbtext',
            text : (Ext.getCmp("metadata_layertype").getValue()!="multi" ? "Ajout d'une nouvelle donnée SIG " : "Création de métadonnées enfants et de leurs données SIG")+" : "
          }, {
            checkLayerType : function(layertype){
              this.setVisible( Ext.getCmp("metadata_layertype").getValue()!="multi");
            },
            text : 'Téléverser et Sélectionner',
            tooltip : 'Téléverser un fichier (<256 Mo) depuis son poste local vers le serveur',
            handler : function(){CarmenImportData.library.createLayer.call(formGrid, null, OPEN_CLIENT_BROWSER)},
            scope : formGrid
          }, {
            checkLayerType : function(layertype){
              this.setVisible( Ext.getCmp("metadata_layertype").getValue()!="multi");
            },
            text : 'Sélectionner depuis le serveur',
            tooltip : "Sélectionner un fichier déjà déposé sur le serveur",
            handler : function(){CarmenImportData.library.createLayer.call(formGrid, null, OPEN_SERVER_BROWSER)},
            scope : formGrid
          }, {
            text : "Ajout multiple à partir d'une archive (ZIP)",
            tooltip : "Transférer un fichier ZIP (>256Mo) d'un serveur distant vers le serveur",
            scope : formGrid,
            handler : CarmenImportData.library.importZIP
          }]
        },
        xtype: 'treepanel',
        viewConfig : {
          emptyText : 'Aucune donnée', 
          deferEmptyText : false,
          getRowClass : function( record , index , rowParams , store ){
          	if ( record instanceof Metadata )return 'row-metadata';
          },
          listeners : {
          	cellclick : function(view, td, cellIndex, record, tr, rowIndex, evt){
          		if ( evt.target.tagName=="IMG" ) return;
          		if( record instanceof Metadata ) (record.isExpanded() ? record.collapse() : record.expand());
          	}
          }
        },
        store: new CarmenImportData.data.TreeStore({model:'CarmenImportData.model.Metadata', id:'CoucheDonneesStore'}),
        overflow:'y',
        height: 350,
        title : 'Données SIG',
        border : true,
        columnLines : true,
        
        columns: [
        {
          xtype: 'treecolumn',
          including_layertype : ['multi'],
          width:170,
          dataIndex : 'mtype',
          renderer : function(value, meta, record){
          	var data = record.getTypePath() || [];
          	data = Ext.Array.remove(data, null);
          	data = Ext.Array.remove(data, undefined);
          	return data[data.length-1];
          }
        },
        {
          text: 'Donnée',
          cellWrap : true,
          flex: 0.55,
          sortable: true,
          dataIndex: 'nom',
          renderer : function(value, meta, record){
            var tpl = '{nom}';
            if ( record.get('description') ) tpl = '{nom}<br/><i>{description}</i>';
            return '<div style="display:inline-block">'+new Ext.XTemplate(tpl).apply(record.data)+'</div>';
          }
        }, {
          including_layertype : ['raster'],
          text: 'Fichier source',
          flex: 0.52,
          sortable: true,
          dataIndex: 'file_name'
        }, {
          exclude_layertype : 'raster',
          text: 'Table de stockage',
          flex: 0.52,
          sortable: true,
          dataIndex: 'stockage'
        }]
        .concat(this.getCoucheVisibility(formGrid, layertype))
        .concat([{
          text: "Statut",
          cellWrap : true,
          align : 'left',
          flex: 0.47,
          sortable: true,
          dataIndex: 'status',
          renderer : function(value, meta, record){
            meta.tdCls = "import-data-status "+(record.get('color') || "");
            return value;
          }
        }, {
          xtype : 'actioncolumn',
          flex: 0.1,
          text : 'Actions',
          align : 'center',
          items : [{
          	getClass : function(v, meta, record){
          		if ( record instanceof  CarmenImportData.model.CoucheDonnees ) return "x-hidden";
          		return "";
          	},
            icon : '/images/add.png',
            tooltip : 'Ajouter une donnée SIG à la métadonnée',
            scope: formGrid,
            handler : CarmenImportData.library.createLayerOnMetadata
          },{
            getClass : function(v, meta, record){
              if ( !(record instanceof  CarmenImportData.model.CoucheDonnees) ) return "x-hidden";
              return "";
            },
            tooltip : 'Supprimer la donnée SIG et les données importées associées',
            icon : '/images/delete.png',
            scope: formGrid,
            handler : CarmenImportData.library.deleteLayer
          }]
        }]),
        listeners: {
          scope: formGrid,
          itemclick: CarmenImportData.library.onSelectionChange
        }
      };
      var tmp =[];
      var layertype = Ext.getCmp('metadata_layertype').getValue();
      grid.columns.forEach(function(column){
      	if ( (column.exclude_layertype ? column.exclude_layertype==layertype : false) 
      	  || (column.including_layertype ? column.including_layertype.indexOf(layertype)==-1 : false) ) return;
      	tmp.push(column);
      })
      grid.columns = tmp;
      return Ext.apply(grid, config);
    },
    
    getCoucheDescription: function(formGrid, layertype)
    {
      return [{
        xtype : 'textfield',
        fieldLabel: 'Nom de la couche',
        name : 'couchd_nom'
      },{
        xtype : 'textarea',
        fieldLabel: 'Description',
        name : 'couchd_description',
        allowBlank : true
      }];
    },
    
    getCoucheEmplacement : function(formGrid, layertype)
    {
      var functions = CarmenImportData.library.fields_actions.getCoucheEmplacement(formGrid, layertype);
      // //////////////////////////////////
      return [{
        xtype : 'fieldcontainer',
        name : 'saut_de_ligne',
        value : '',
        style : 'min-height:10px !important'
      }, {
        labelWidth : 150,
        fieldLabel : 'Table POSTGIS',
        exclude_layertype : 'raster',
        xtype : 'cid-fieldcontainer',
        layout : {
          type : 'table',
          tableAttrs : {
            width : '100%'
          }
        },
        items : [
          Ext.apply(functions.couchd_emplacement_stockage, {
            exclude_layertype : 'raster',
            allowBlank : false,
            width : '100%',
            xtype : 'textfield',
            vtype : 'identifier',
            name : 'couchd_emplacement_stockage',
    
            importaction : 'new',
            margin : '0 10 0 0'
              // end Table
          }),
  
          Ext.apply(functions.action_change_tablename, {
            width : 90,
            xtype : 'button',
            disabled : true,
            importaction : 'created',
            text : 'Renommer',
            scope : formGrid
          })
  
//        , Ext.apply(functions.action_show_database, {
//            xtype : 'button',
//            disabled : true,
//            margin : '0 0 0 10',
//            // importaction : 'created',
//            //            exclude_layertype : 'raster',
//            text : 'Afficher les valeurs',
//            scope : formGrid
//          })
        ]
      }]
    },//getCoucheEmplacement
    
    getCoucheImportAction : function(formGrid, layertype)
    {
      var labelVector;
      var functions = CarmenImportData.library.fields_actions.getCoucheImportAction(formGrid, layertype);
      ////////////////////////////////////
      return [{
        exclude_layertype : 'raster',
        xtype : 'radiogroup',
        allowBlank : false,
        fieldLabel : "Action d'import"+UNSAVED_MARK,
        vertical : true,
        columns : 1,
        items : [
          Ext.apply(functions.create, {
            exclude_layertype : 'raster',
            xtype : 'radio',
            name : 'import_action',
            id : 'import_action_create',
            inputValue : 'create',
            importaction : 'new',
            labels : {
              others : (labelVector = 'Création de la table POSTGIS'),
              raster : 'Création du raster'
            },
            boxLabel : labelVector,
            labelWidth : 0
          // end Create table
          }), 
          Ext.apply(functions.update.fieldcontainer, {
            xtype : 'fieldcontainer',
            name : 'unique_fields',
            layout: {
              type: 'hbox',
            type: 'column',
              pack: 'start',
              align: 'stretch'
            },
            importaction : 'created',
            items : [
              Ext.apply(functions.update.radio, {
                exclude_layertype : 'raster',
                xtype : 'radio',
                name : 'import_action',
                id : 'import_action_update',
                inputValue : 'update',
                fieldAlign : 'top',
                boxLabel : 'Mise à jour de la table POSTGIS par rapport à la clé&nbsp;',
                labelWidth : 0
              // end Create table
              }),
              Ext.apply(functions.update.combo, {
                exclude_layertype : 'raster',
                xtype : 'combo',
                queryMode : 'local',
                store :  Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenImportData.model.ChampDonnee'}),
                name : 'field_key', 
                emptyText : 'Aucune clé',
                displayField : 'field_name',
                valueField : 'field_name'
              // end Create table
              })
            ]
          }), 
          Ext.apply(functions.replace, {
            exclude_layertype : 'raster',
            xtype : 'radio',
            name : 'import_action',
            id : 'import_action_replace',
            inputValue : 'replace',
            labels : {
              others : (labelVector = 'Annulation et remplacement de la table POSTGIS'),
              raster : 'Annulation et remplacement du raster'
            },
            boxLabel : labelVector,
            importaction : 'created',
            labelWidth : 0
          // end Create table
          })
        ]
      }]
    },
    
    getCoucheVisibility : function(formGrid, layertype)
    {
      return []
      .concat(formGrid.getCoucheVisibilityByType(formGrid, layertype, 'wms', '(ouverture des droits en consultation)'))
      .concat(formGrid.getCoucheVisibilityByType(formGrid, layertype, 'wfs', '(ouverture des droits en téléchargement)'));
    },
    
    getCoucheVisibilityByType : function(formGrid, layertype, type, legend)
    {
      var TYPE = type.toUpperCase();
      var functions = CarmenImportData.library.fields_actions.getCoucheVisibility(formGrid, layertype, type);
      ////////////////////////////////////
      return [{
        xtype : 'checkcolumn', 
        flex : 0.1,
        text : TYPE,
        dataIndex : 'couchd_'+type,
        exclude_layertype : 'raster',
        listeners : {beforecheckchange : functions.configure_webservice},
        renderer: function (value, metaData, record) {
        	if ( record instanceof CoucheDonnees ) return  Ext.grid.column.Check.prototype.defaultRenderer.apply(this, arguments);
          return "";
        }
      }];
    },
    
    initForm : function(config){
    
      var layertype = this.layertype;
      var formGrid = this;
      
      var columnLeft = [].concat(
        this.getCoucheDescription(formGrid, layertype)
      , this.getCoucheEmplacement(formGrid, layertype)
      , this.getCoucheImportAction(formGrid, layertype)
      );
      
      
      Ext.each(columnLeft, function(field, index){
        columnLeft[index] = Ext.ComponentManager.create(field, 'textfield');
      });
//      Ext.each(columnRight, function(field, index){
//        columnRight[index] = Ext.ComponentManager.create(field, 'textfield');
//      });
      
      // configuration of fields visibility according to the state of record and the layertype of the metadata
      
      
      
      // FORM
      var form = {
          //region : 'south',
        width: '100%',
        title:DEFAULT_TITLE,
        defaults : {
          margin : "0 10 0 10"
        },
        xtype : 'panel',
        disabled : true,
        border : true,
        items : [
       //   {html : "<span class='div-info'>(*) Signale les champs de configuration non enregistrés sauf dans le cas de l'import différé des données.</span>"}, 
          {
            xtype: 'form',
            //trackResetOnLoad : true,
            id : 'importdataform',
            layout: 'column',
            buttonAlign : 'center',
            buttons : [{
              text : 'Valider',
              handler : CarmenImportData.library.saveLayerConfiguration,
              scope : formGrid
            }, {
              text : 'Annuler',
              handler : CarmenImportData.library.cancelLayerConfiguration,
              scope : formGrid
            }],
            
            defaults: {
              layout: 'form',
              defaultType: 'textfield',
              columnWidth : 0.5,
              margin : 10
            },
            
            items: [{
              columnWidth : 0.5, 
              fieldDefaults : {allowBlank : false},
              defaults : {allowBlank : false},
              id : 'form-column-left', 
              items : columnLeft
            } , {
              id : 'form-column-right', 
              xtype: 'carmenimportconfigurator',
              formGrid : formGrid
            }]
          }]
      };
      return Ext.apply(form, config);
    },
    
    // In this example, configuration is applied at initComponent time
    // because it depends on themeInfo object that is generated for the
    // FormGrid instance.
    initComponent: function() {
      var formGrid = this;
      var buttons = this.buttons || [];
      delete this.buttons;
      Ext.apply(this, {
        width: '100%',
        fieldDefaults: {
          labelAlign: 'left',
          labelWidth: 150,
          anchor: '100%',
          msgTarget: 'side'
          
        },
        frame: true,
        bodyPadding: 10,
        defaults : {padding: 10,border : true},
        
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [
          this.initGrid({
            bbar : {
              defaultButtonUI : 'default',
              items : ['->',
              {
                xtype: "tbtext",
                text : "<u>Sur l'ensemble des couches configurées </u>"
              },'-'].concat(buttons.slice(0,2)).concat([{
                xtype:'tbseparator',
                checkLayerType : function(layertype){
                  this.setVisible( Ext.getCmp("metadata_layertype").getValue()!="multi");
                }
              },{
                checkLayerType : function(layertype){
                  this.setVisible( Ext.getCmp("metadata_layertype").getValue()!="multi");
                },
              	text : "Paramétrer la représentation par défaut",
              	handler: CarmenImportData.library.editMap, scope : formGrid,
                xtype : 'splitbutton',
                menu: new Ext.menu.Menu({
                    items: [
                        // these will render as dropdown menu items when the arrow is clicked:
                        {text: 'Paramétrer la représentation par défaut', handler: CarmenImportData.library.editMap, scope : formGrid},
                        {text: 'Réinitialiser la représentation par défaut', handler: CarmenImportData.library.resetMap, scope : formGrid}
                    ]
                })
              }, '-'
              ]).concat(buttons.slice(2))
            }
          }), 
          this.initForm({})
        ],
        buttonAlign : 'center'
      });
      this.callParent();
      this.on('recordloaded', CarmenImportData.library.onLayerLoaded, this);
    },
    
    
    getFileBrowserOptions : function(layertype, forBrowser){
    	var store = null;
    	switch ( layertype ){
        case "VECTOR" : 
          store = Ext.create('Ext.data.JsonStore', {
          	proxy : {type:'memory', reader : {type:'json'}},
          	fields : ['valueField', 'displayField', 'extensions'],
            data : [{
              valueField : false,
              displayField : 'Données vectorielles',
              extensions: FILETYPES[layertype]
            }, {
              valueField : true,
              displayField : 'Données tabulaires avec coordonnées',
              extensions: FILETYPES["GEOREFERENCED"]
            }]
          });
        break;
    		case "RASTER" : 
          store = Ext.create('Ext.data.JsonStore', {
            proxy : {type:'memory', reader : {type:'json'}},
            fields : ['valueField', 'displayField', 'extensions'],
            data : [{
              valueField : false,
              displayField : 'Fichier raster',
              extensions: FILETYPES[layertype]
            }, {
              valueField : true,
              displayField : 'Tuilage raster',
              extensions: FILETYPES["TILEINDEX"]
            }]
          });
        break;
    	}
    	if ( !store ) return null;
    	var labels = {
        vector : 'Données tabulaires<br/>avec coordonnées'+UNSAVED_MARK,
        raster : 'Tuilage raster'+UNSAVED_MARK
      }
      var cmp =  {
      	labelWidth : 300,
      	readOnly : !forBrowser,
        including_layertype : ['raster', 'vector'],
        exclude_layertype : '@see CarmenImportData.library.fields_actions.getCoucheDataSource.file_georeferenced.checkLayertype function ',//included_layertype
        labels : labels,
        fieldLabel: 'Type de données',
        xtype: 'combo',  
        queryMode : 'local',
        store : store,
        value : false,
        displayField : 'displayField',
        valueField : 'valueField',
        name : 'file_georeferenced'
      };
      if ( forBrowser ){
      	cmp = {
        labelWidth : 300,
      		padding : '0 10 0 10',
      		xtype : 'panel',
      		layout : 'form',
      		items : [cmp]
      	};
      }
      return cmp;
    },
    
    getGrid : function(){return this.down('treepanel#importdatagrid');},
    getForm : function(){return this.down('form#importdataform');},
    getLayer : function(){return this.getForm().getRecord();},
    loadStore : function(){
      var grid = this.getGrid(),
          store = grid.getStore();
      store.load();
    },
    setStore : function(data){
      var grid = this.getGrid(),
          store = grid.getStore();
      if ( data instanceof Ext.data.Store ){
        store.loadData(data.getRange(), false);
      }
      else {
        store.loadRawData(data, false);
      }
      store.fireEvent('load', store, store.getRange());
      store.each(function(record){record.set('couchd_type_stockage', this.layertype)}, this);
    }
});


Ext.define('CarmenImportData.view.form.ImportData', {
    extend: 'Ext.panel.Panel',
    xtype : 'form-importdata',
    scrollable : 'y',
    initComponent : function(){
      var importDataView = this;
      Ext.apply(this, {
      	//height : (Ext.dom.Element.getViewportHeight()),
        items : [{
          title : 'Importation des données',
          //region : 'north',
          layout: {
              type: 'vbox',
              pack: 'start',
              align: 'center'
          },
          items : [{
            width: '50%',
            xtype : 'form',
            id : 'metadata-form',
            fieldDefaults: {
              labelAlign: 'left',
              msgTarget: 'side'
            },
            defaults : {
              labelWidth: 150,
              width : 600
            },
            items : [{
              xtype : 'displayfield',
              fieldLabel : 'Identifiant',
              name : 'uuid',
              value : (Carmen.metadata ? Carmen.metadata.uuid : null)
            }, {
              xtype : 'displayfield',
              fieldLabel : 'Nom de la métadonnée',
              name : 'metadata_title',
              value : (Carmen.metadata ? Carmen.metadata.metadata_title : null)
            }, {
              xtype : 'combo',
              fieldLabel : 'Type de données',
              name : 'metadata_layertype',
              id : 'metadata_layertype',
              readOnlyCls : 'x-form-readonly-noborder',
              queryMode : 'local',
              store : CarmenImportData.Datatypes.getStore('valueField', 'displayField'),
              valueField : 'valueField', 
              displayField : 'displayField',
    
              value : (Carmen.metadata ? Carmen.metadata.metadata_datatype : null) || 'vector',
              importaction : 'new',
              changeState : function(enable){
                this.setDisabled(!enable);
              },
              listeners : {
              	change : function(combo){
              		var isJoin = combo.getValue()=="join";
                  importDataView.down('[name=view_name]').setVisible(isJoin);
              		importDataView.down('[name=view_name]').allowBlank = !(isJoin);
              		importDataView.down('button#initialiser').setText(isJoin ? "Paramétrer la jointure" : "Initialiser");
              	}
              }
            }, {
              readOnlyCls : 'x-form-readonly-noborder',
            	hidden : (Carmen.metadata ? Carmen.metadata.metadata_datatype : null)!="join",
              xtype : 'textfield',
              fieldLabel : 'Nom de la vue',
              name : 'view_name',
    
              value : (Carmen.metadata ? Carmen.metadata.view_name : null)
            }],buttons : [{
              text : 'Initialiser',
              id : 'initialiser',
              importaction : 'new',
              changeState : function(enable){
                this.setVisible(enable);
              },
              confirm : true,
              handler : CarmenImportData.library.initialiserImportData(importDataView)
            }]
          }]
        }]
      })
      this.callParent();
      if ( !(Carmen.metadata && Carmen.metadata.id) ) this.hide();
    }
    
});

function positionMarkReadyFunction(){/*Marqueur de position pour le code global*/}
var DEV = true;
UNSAVED_MARK = "";
Ext.onReady(function() {
  Ext.tip.QuickTipManager.init();
  var importDataView = Ext.create('Ext.container.Viewport', {
  	layout : 'fit',
    renderTo : 'main',
    items:[{xtype:'carmenimportconfiguratorform'}]
  });
  
});

function executeImport(callback, scope){
	//console.log(Ext.getCmp('formImportLocalData').getForm().getFieldValues());
	callback.call(scope);
}