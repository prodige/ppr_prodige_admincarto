/* global Ext */

Ext.define('CarmenAddToWebService.model.CoucheWebService', {
  extend : 'CarmenImportData.model.CoucheDonnees',
  idProperty : 'pk_couche_donnees',
  entityName: 'CoucheWebService',
  fields: [
    {name : 'wxs_type', persist : false, defaultValue : 'WMS'}, 
    {name : 'wxs_title', convert : function(value, record){return value || record.get('couchd_nom');}}, 
    {name : 'wxs_source', convert : function(value, record){return value || record.get('couchd_emplacement_stockage').replace(/\/|\./g, "_");}}, 
    'wxs_identifiant', 
    {name:'wxs_fields', mapping : function(data){return data.wxs_fields || data.fieldnames;}, convert : function(value, record){return value || record.get('fieldnames');}, defaultValue:[]},
    {name:'wxs_domaine', mapping : function(data){return data.wxs_domaine || data.couchd_wms;}, convert : function(value, record){return value || record.get('couchd_wms');}},
    'wxs_keywords',
    {name : 'wxs_resume', convert : function(value, record){return value || record.get('couchd_description');}},
    'wxs_projection',
    {name : 'wxs_status', calculate : function(data){
        return (data.wxs_type=='WMS' 
        ? data.couchd_wms!=0
        : data.couchd_wfs);
     }, serialize : function(){return null;}}
  ]
});
CarmenAddToWebService.model.CoucheWebService.getField('layerfields').persist = false;
CarmenAddToWebService.model.CoucheWebService.getField('unique_fields').persist = false;
Ext.define('CarmenAddToWebService.model.Metadata', {
  extend : 'Ext.data.Model',
  idProperty : 'uuid',
  entityName: 'MetadataWebService',
  fields: [
    'id', 
    {name : 'uuid', critical:true}, 
    {name : 'metadata_title', critical:true}, 
    {name : 'wms_domaines', defaultValue : [], critical:true}, 
    {name : 'wms_global_group', critical:true}, 
    {name : 'wms_on_global', type : 'boolean', calculate : function(data){return !!data.wms_global_group;}, critical:true}, 
    {name : 'wms_on_domaines', type : 'boolean', calculate : function(data){return data.wms_domaines.length>0;}, critical:true}, 
    {
      name:'wms_couches', critical:true, 
      defaultValue : Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenAddToWebService.model.CoucheWebService', data :[]}),
      convert : function(value){
        if ( value instanceof Array ) {
          value = Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenAddToWebService.model.CoucheWebService', data :value});
          value && value.each && value.each(function(item){
            item.set('wxs_type', 'WMS', {commit:true});
          });
          return value;
        }
        return value;
      },
      serialize : function(value){
        value = value || [];
        var serialize = [];
        if ( !(value instanceof Array) ){
          value = value.getRange();
        }
        Ext.each(value, function(coucheWebService){
          serialize.push(coucheWebService.getData({persist:true, serialize:true}));
        });
        return serialize;
        
      }
    },
    {
      name:'wfs_couches', critical:true, 
      defaultValue : Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenAddToWebService.model.CoucheWebService', data :[]}),
      convert : function(value){
        if ( value instanceof Array ) {
          value = Ext.create('CarmenImportData.data.JsonMemoryStore', {model : 'CarmenAddToWebService.model.CoucheWebService', data :value});
          value && value.each && value.each(function(item){
            item.set('wxs_type', 'WFS', {commit:true});
          });
          return value;
        }
        
        return value;
      },
      serialize : function(value){
        value = value || [];
        var serialize = [];
        if ( !(value instanceof Array) ){
          value = value.getRange();
        }
        Ext.each(value, function(coucheWebService){
          serialize.push(coucheWebService.getData({persist:true, serialize:true}));
        });
        return serialize;
        
      }
    }
  ]
});
Ext.define('CarmenAddToWebService',{
  extend : 'Ext.window.Window',
  config : {
    type : null,
    metadata : null,
    store : null,
    coucheStore : null
  },
  scrollable : 'y',
  constraintHeader : true,
  layout : 'fit',
  width : '70%',
  y : 10,
  //height : '90%',
  padding : 10,
  
  
  viewModel : true,
  
  pageReady : false,
  
  initComponent : function(){
    var original_metadata = Ext.apply({}, this.metadata);
    var selected_route = 'catalogue_geosource_wxs_'+this.type.toLowerCase()+'_to_dataset';
    Ext.Ajax.request({
      cors : true,
      withCredentials : true,
      async : false,
      asynchronous : false,
      url : Routing.generate(selected_route, {uuid:this.metadata.uuid}),
      scope : this,
      success : function(response){
        var fjoin = function(a){return a.join();};
        var fsplit = function(a){return [a];};
        var data = Ext.decode(response.responseText, true);
        if ( !data ) return;
        var fcouches = this.type.toLowerCase()+'_couches';
        var metadata = Ext.apply({}, this.metadata);
        metadata[fcouches] = [];
        Ext.each(this.coucheStore.getRange(), function(store_couche){
          var pk_couche_donnees = store_couche.get('pk_couche_donnees');
          var couche = ((data[fcouches] || {})[pk_couche_donnees]) || {};

          store_couche = Ext.apply({}, store_couche.getData());          
          couche.wxs_fields = Ext.Array.map(
                  Ext.Array.intersect(Ext.Array.map(couche.wxs_fields || store_couche.fieldnames, fjoin), 
                  Ext.Array.map(store_couche.fieldnames, fjoin)), 
                  fsplit);

            store_couche = Ext.apply(store_couche, couche);
          metadata[fcouches].push(store_couche);
        }, this);
        delete data[fcouches];
        
        metadata = Ext.apply(metadata, data);
        this.metadata = Ext.create('CarmenAddToWebService.model.Metadata', metadata);
      },
      failure : function(response){
        console.log('get failure', response);
      }
    });
    
    if ( Ext.ClassManager.getName(this.metadata) != 'CarmenAddToWebService.model.Metadata' ){
      this.metadata = Ext.create('CarmenAddToWebService.model.Metadata', this.metadata);
    }

    this.items = this.getPage();
    this.title = new Ext.XTemplate("Diffusion {diffusion_type} des données - {uuid} - {metadata_title}").apply(
            Ext.apply({diffusion_type : this.type.toUpperCase()}, original_metadata)
    );
    this.buttons = (this.getPageButtons()||[]).concat([{
      text : 'Diffuser les données',
      scope : this,
      handler : this.diffusionAction
    }, {
      text : 'Supprimer la diffusion',
      scope : this,
      handler : this.suppressionAction
    }, '->', {
      text : 'Fermer',
      scope : this,
      handler : function(){this.close();}
    }]);
    this.callParent(arguments);
    
  },
  
  diffusionAction : function(){
    var form = this.down('#mainform');
    if ( !form ) return;
    var isValid = true;
    switch(this.type.toUpperCase()){
      case "WMS" :
        isValid = this.validateWMS();
        break;
      case "WFS" :
        isValid = this.validateWFS();
        break;
    }
    if ( !isValid ) return;
    
    if ( !form.isValid() ) {
      form.getForm().getFields().each(function(field){
        if ( !field.isValid() ){
          field.up('tabpanel').setActiveItem(field.up('tabpanel > panel'));
          field.focus();
          return false;
        }
      });
      return false;
    }
    
    var record = form.getRecord();
    Ext.each(form.query('form'), function(subform){
      subform.updateRecord();
    });
    var buttons = this.query("button") || [];
    Ext.each(buttons, function(button){button.setDisabled(true)});
    //Ext.data.Connection.setTimeout(120000);
    record.save({
      cors : true,
      withCredentials : true,
      timeout : 120000,
      url : Routing.generate('catalogue_geosource_wxs_dataset_to_'+this.type.toLowerCase()),
      method : 'POST',
      scope : this,
      success : function(metadata){
        Ext.each(buttons, function(button){button.setDisabled(false)});
        var store = this.metadata.get(this.type.toLowerCase()+'_couches');
        if ( !store ) return;
        this.coucheStore.each(function(item){
          var layer = store.findRecord('pk_couche_donnees', item.get('pk_couche_donnees'));
          if ( !layer ) return;
          switch (this.type.toLowerCase()){
            case "wms" :
              item.set('couchd_wms', layer.get('couchd_wms'));
            break;
            case "wfs" :
              item.set('couchd_wfs', layer.get('couchd_wfs'));
            break;
          }
        }, this);

        Ext.Msg.alert('Diffusion '+this.type.toUpperCase()+' des données', 'La diffusion '+this.type.toUpperCase()+' des couches est opérationnelle', function(){
          this.pageReady = false;
          this.onPageReady();
        }, this);

      },
      failure : function(response){
        console.log('failure', arguments);
        Ext.each(buttons, function(button){button.setDisabled(false)});
        console.log('failure', response);
      }
    });
  },
  
  validateWMS : function(){
    var form = this.down('#mainform');
    if ( !form ) return false;
    var wms_domaines = [];
    var wms_global_group = null;
    var wms_on_domaines = form.down('#wms_on_domaines');
    var wms_on_global = form.down('#wms_on_global');
    
    if ( wms_on_domaines.checked ){
      var selection = form.down('#wms_domaines').getSelection();
      wms_domaines = Ext.Array.pluck(Ext.Array.pluck(selection, 'data'), 'id');
      if ( !wms_domaines.length ){
        Ext.Msg.alert('Enregistrement de la diffusion', '<b>'+wms_on_domaines.boxLabel+'</b><br>ERREUR : Veuillez sélectionner au moins un service WMS de publication');
        return false;
      }
    }
    if ( wms_on_global.checked ){
      var selection = form.down('#wms_global_group').getSelection();
      selection = (selection ? [selection] : []);
      wms_global_group = Ext.Array.pluck(Ext.Array.pluck(selection, 'data'), 'id');
      if ( !wms_global_group.length ){
        Ext.Msg.alert('Enregistrement de la diffusion', '<b>'+wms_on_global.boxLabel+'</b><br>ERREUR : Veuillez sélectionner le groupe de publication');
        return false;
      }
      wms_global_group = wms_global_group[0];
    }
    
    if ( !wms_on_domaines.checked && !wms_on_global.checked ){
      Ext.Msg.alert('Diffusion WMS', 'Aucune destination de diffusion définie');
      return false;
    }
    
    var record = form.getRecord();
    record.set({
      wms_domaines : wms_domaines,
      wms_global_group : wms_global_group
    });
    
    return true;
  },
  
  validateWFS : function(){
    return true;
  },
  
  suppressionAction : function(){
    Ext.Msg.confirm(
      'Diffusion '+this.type.toUpperCase()+' des données', 
      'Confirmez-vous le retrait des couches des flux '+this.type.toUpperCase()+' ?',
      function(btn){
        if ( btn!='yes' ) return;

        var buttons = this.query("button") || [];
        Ext.each(buttons, function(button){button.setDisabled(true)});
        Ext.Ajax.request({
          cors : true,
          withCredentials : true,

          url : Routing.generate('catalogue_geosource_wxs_remove_dataset_from_'+this.type.toLowerCase(), {uuid : this.metadata.get('uuid')}),
          scope : this,
          success : function(response){
            Ext.each(buttons, function(button){button.setDisabled(false)});
            var data = Ext.decode(response.responseText, true);
            if ( !data ) return;
            var layers = data[this.type.toLowerCase()+'_couches'];
            delete data[this.type.toLowerCase()+'_couches'];
            this.metadata.set(data, {commit:true});
            var store = this.metadata.get(this.type.toLowerCase()+'_couches');
            if ( !store || !(store instanceof Ext.data.Store) ) return;
            Ext.iterate(layers, function(pk_couche_donnees, data){
              var layer;

              layer = this.coucheStore.findRecord('pk_couche_donnees', pk_couche_donnees);
              if ( layer ) layer.set(data, {commit:true});

              layer = store.findRecord('pk_couche_donnees', pk_couche_donnees);
              if ( layer ) layer.set(data, {commit:true});
            }, this);
            Ext.Msg.alert('Diffusion '+this.type.toUpperCase()+' des données', 'SUCCES : Les données ne sont plus diffusées en '+this.type.toUpperCase()+'', function(){
              this.pageReady = false;
              this.onPageReady();
            }, this);
          },
          failure : function(response){
            Ext.each(buttons, function(button){button.setDisabled(false)});
            console.log('failure', response);
          }
        });

      },
      this
    );
  },
  onPageReady : function(){
    var form = this.down('#mainform');
    if ( form && !this.pageReady){
      form.loadRecord(this.metadata);
      this.pageReady = true;
    }
  },
  
  editMap : function(){
    window.open(Routing.generate('catalogue_geosource_wxs_'+this.type.toLowerCase()+'_map', {uuid : this.metadata.get('uuid')}), this.type.toLowerCase()+'_map');
  },
  resetMap : function(btn){
    btn.up('splitbutton').mask();
    Ext.Ajax.request({
      cors: true,
      crossDomain: true,
      withCredentials : true,
      url : Routing.generate('catalogue_geosource_wxs_wms_map_delete', {uuid : this.metadata.get('uuid')}),
      success : function(response) {
        btn.up('splitbutton').unmask();
        if ( !response || !response.responseText ) return;
        response = Ext.decode(response.responseText);
        //Ext.Msg.alert("La représentation par défaut a été réinitialisée");
        Ext.Msg.alert('Représentation par défaut', "La représentation par défaut "+(response.success ? "a été" : "n'a pas été")+" réinitialisée", function(){});
      },
      failure : function() {
        btn.up('splitbutton').unmask();
        Ext.Msg.alert('Représentation par défaut', "La représentation par défaut n'a pas été réinitialisée", function(){}).setIcon(Ext.Msg.ERROR);
      }
    });
  },
  getPageButtons : function(){
    if ( this.type.toUpperCase()=='WMS' ) {
      return [{
        xtype : 'button',
        text : 'Représentation cartographique',
        scope : this,
        handler : this.editMap, 
        xtype : 'splitbutton',
        menu: new Ext.menu.Menu({
          items: [
            // these will render as dropdown menu items when the arrow is clicked:
            {text: 'Paramétrer la représentation par défaut', handler: this.editMap, scope : this},
            {text: 'Réinitialiser la représentation par défaut', handler: this.resetMap, scope : this}
          ]
        })
      }, '->'];
    } else {
      return ['->'];
    }
  },
  getPage : function(){
    if ( this.type.toUpperCase()=='WMS' ) {
      
      this.treeStoreWithWMS = Ext.create('Ext.data.JsonStore', {
        fields : ['id', 'text', 'type', 'ssdom_service_wms'],
        proxy : {type : 'memory'}
      });
      this.storeSousDomaines = Ext.create('Ext.data.JsonStore', {
        fields : ['id', 'text', 'type', 'ssdom_service_wms', 'rubric_id', 'domaine_id'],
        proxy : {type : 'memory'}
      });
      this.storeAllDomaines = Ext.create('Ext.data.TreeStore', {
        autoLoad : true,
        fields : ['id', 'text', 'type', 'ssdom_service_wms', 'rubric_id', 'domaine_id'],
        proxy : {
          type : 'ajax',
          reader: {
              type: 'json'
          },
          url : Routing.generate('carmen_webservice_domaines', {fmeta_id : this.metadata.get('id')})
        },
        listeners : {
          scope : this,
          load : function(store){
            this.onPageReady();
          }
        }
      });
      return this.getPageWMS();
    }
    if ( this.type.toUpperCase()=='WFS' ) {
      return this.getPageWFS();
    }
  },
  
  getPageWMS : function(){
    
    var wms_domaines = {
      bind : {
        disabled : '{!wms_on_domaines.checked}'
      },
      //xtype : 'wms_domaines',
      xtype : 'grid',
      border : true,
      reserveScrollbar: true,
      height : 'auto',
      reference : 'wms_domaines',
      itemId : 'wms_domaines',
      selModel: {
        selType: 'checkboxmodel',
        mode : 'MULTI'
        //injectCheckbox : 'last'
      },
      columns : {
        items : [{
          align : 'left',
          flex: 0.8,
          dataIndex : 'text',
          header : 'Sous-domaine(s) WMS de publication'
        }],
        defaults: {
          align : 'center',
          flex: 0.03
        }
      },
      viewConfig : {
        emptyText : 'Aucun sous-domaine associé à un service WMS'
      },
      store : {type : 'chained', source : this.storeAllDomaines, filters : [{property : 'ssdom_service_wms', value:true}]}
    };
    
    var form = Ext.override(Ext.create('Ext.form.Panel', {   
      viewModel : true,
      width : '100%',
      itemId : 'mainform',
      items : [{          
        xtype : 'panel',
        width : '100%',
        margin : '0 0 10 0',
        title : 'Diffuser la/les donnée(s) dans',
        layout : {
        width : '100%',
          type : 'column',
          align : 'left',
          pack : 'stretch'
        },
        defaults : {columnWidth : 0.5, height : 200, width : '100%'},
        items : [{
          xtype : 'fieldset',
          layout : {
            type : 'vbox',
            align : 'center',
            pack : 'stretchmax'
          },
          defaults : {width:'100%'},
          /*{
            type : 'vbox',
            align : 'left',
            pack : 'stretch'
          },*/
          margin : '0 10 0 0',
          items : [{
            xtype : 'checkbox',
            boxLabel : 'Services WMS des sous-domaines',
            name : 'wms_on_domaines',
            reference : 'wms_on_domaines',
            itemId : 'wms_on_domaines'
          },    
            wms_domaines
          ]
        }, {
          xtype : 'fieldset',
          layout : {
            type : 'vbox',
            align : 'center',
            pack : 'stretchmax'
          },
          defaults : {width:'100%'},
          items : [{
            xtype : 'checkbox',
            boxLabel : 'Service WMS de la plateforme',
            name : 'wms_on_global',
            reference : 'wms_on_global',
            itemId : 'wms_on_global'
          }, {
            bind : {
              disabled : '{!wms_on_global.checked}'
            },
            name : 'wms_global_group',
            reference : 'wms_global_group',
            itemId : 'wms_global_group',
            xtype : 'combo',
            queryMode : 'local',
            fieldLabel : 'Groupe de publication',
            labelAlign : 'top',
            store : {type : 'chained', source : this.storeAllDomaines},
            displayField : 'text',
            valueField : 'id',
            triggers : {
              reset : {
                extraCls: 'fa fa-times',
                cls: 'x-form-close-trigger',
                weight: 1,
                handler: function(combo) {
                  combo.reset && combo.reset();
                }
              }
            }
          }]
        }]
      }, this.getCouchesFormSheets()]
    }), {
      loadRecord : this.loadRecord
    });
    
    return [form];
  },
  
  getPageWFS : function(){
    return [ Ext.override(Ext.create('Ext.form.Panel', {
      itemId : 'mainform',
      items : [this.getCouchesFormSheets()],
      listeners : {
        scope : this,
        afterrender : function(){
          this.onPageReady();
        }
      }
    }), {
      loadRecord : this.loadRecord
    }) ];
  },
  
  loadRecord : function(record){
    // /!\ this==form
    var wms_domaines = this.down('#wms_domaines');
    if ( wms_domaines ){
      var setSelection = function(){
        var selectionIds = record.get(wms_domaines.itemId) || [];
        var selection = wms_domaines.getStore().queryBy(function(item){
          return selectionIds.indexOf(item.getId())!=-1;
        });
        wms_domaines.setSelection(selection.getRange());
      };
      if ( wms_domaines.store.getCount()>0 ){
        setSelection();
      } else {
        wms_domaines.store.on('load', setSelection, this, {single:true});
      }
    }
    
    Ext.each(record.get('wms_couches').getRange(), function(couche){
      var form = this.down('#form_wms_'+couche.get('pk_couche_donnees'));
      if ( form ) {
        form.loadRecord(couche);
      }
    }, this);
    Ext.each(record.get('wfs_couches').getRange(), function(couche){
      var form = this.down('#form_wfs_'+couche.get('pk_couche_donnees'));
      if ( form ) {
        form.loadRecord(couche);
      }
    }, this);
    
    this.callParent(arguments);
  },
  
  getCouchesFormSheets : function(){
    var sheets = [];
    Ext.each(this.metadata.get(this.type.toLowerCase()+'_couches').getRange(), function(record){
      if(record.get('couchd_type_stockage')==0
      || record.get('couchd_type_stockage')==1
      || record.get('couchd_type_stockage')==-4){
        sheets.push({
          padding : 10,
          title : record.get('wxs_title'),
          xtype : 'form',
          trackResetOnLoad : true,
          itemId : 'form_'+this.type.toLowerCase()+'_'+record.get('pk_couche_donnees'),
          layout : {
            type : 'hbox',
            align : 'left',
            pack : 'stretch'
          },
          defaults : {flex:.5, xtype : 'panel'},
          items : [{
            defaults : {xtype : 'textfield', flex:1, labelWidth : 120, labelSeparator:'', width:'100%', allowBlank : false},
            items : [{
              name : 'wxs_title',
              fieldLabel : 'Titre'
            }, {
              name : 'wxs_source',
              fieldLabel : 'Nom de la couche',
              readOnly : true
            }, {
              name : 'wxs_keywords',
              fieldLabel : 'Mots-clés',
              allowBlank : true
            }, {
              xtype : 'textarea',
              name : 'wxs_resume',
              fieldLabel : 'Résumé',
              allowBlank : true
            }/*, {
              xtype : 'displayfield',
              fieldLabel : 'Statut de la diffusion',
              name : 'wxs_status',
              scope : this,
              renderer : function(value){
                var current_record = this.metadata.get(this.type.toLowerCase()+'_couches').findRecord('pk_couche_donnees', record.get('pk_couche_donnees')) || record;
                if ( !current_record ) return "";
                if ( value===null ) return "";
                var flux = "";
                if ( current_record.get('wxs_type')=='WMS' ){
                  flux = this.storeSousDomaines.queryRecords('id', current_record.get('couchd_wms'));
                  if ( flux.length ){
                    flux = this.storeDomaines.queryRecords('id', flux[0].get('domaine_id'));
                  }
                  if ( flux.length ){
                    flux = 'le flux WMS du domaine '+flux[0].get('text');
                  } else {
                    flux = 'le flux WMS de la platforme';
                  }
                } else {
                  flux = 'les flux WFS';
                }
                return (value ? 'Donnée diffusée sur '+flux : 'Donnée non diffusée');
              },
              listeners : {
                scope : this,
                afterrender : function(field){
                  this.storeSousDomaines.on('datachanged', function(store){
                    var value = field.getValue();
                    field.setValue(null);
                    field.setValue(value);
                  });
                  this.storeSousDomaines.fireEvent('datachanged', this.storeSousDomaines);
                },
                redraw : function(field){
                  var value = field.getValue();
                  field.setValue(null);
                  field.setValue(value);
                }
              }
            }*/]
          }, {
            defaults : {xtype : 'textfield', flex:1, labelWidth : 180, labelSeparator:'', width:'100%', allowBlank : false},
            margin : '0 0 0 10',
            items : [/*{
              hidden : !hasDomaine,
              allowBlank : !hasDomaine,
              name : (hasDomaine ? 'wxs_domaine' : null),
              fieldLabel : 'Groupe de publication WMS',
              xtype : 'combo',
              queryMode : 'local',
              store : this.storeSousDomaines,
              valueField : 'id', displayField : 'text',
              listConfig : {emptyText : 'Aucun domaine de publication WMS sélectionné', deferEmptyText:false},
              listeners : {
                scope : this,
                afterrender : function(combo){
                  combo.getStore().on('datachanged', function(store){
                    var indexValue = store.find(combo.valueField, combo.getValue());
                    var indexInitial = store.find(combo.valueField, combo.originalValue);
                    if (indexValue==-1 && indexInitial==-1) {
                      combo.setValue(null);
                    } else {
                      var index = (indexValue==-1 ? indexInitial : indexValue);
                      combo.setSelection(store.getAt(index));
                    }
                  }, this);
                  combo.getStore().fireEvent('datachanged', combo.getStore());
                }
              }
            }, 
   */
            {
              name : 'wxs_identifiant',
              fieldLabel : 'Champ identifiant',
              xtype : 'combo',
              queryMode : 'local',
              allowBlank : (Ext.getCmp("metadata_layertype").getValue()==="raster" ? true : false),
              valueField : 'fieldname', displayField : 'fieldname',
              store : Ext.create('Ext.data.ArrayStore', {
                fields : ['fieldname'],
                data : record.get('fieldnames')
              })
            }, {
              name : 'wxs_fields',
              fieldLabel : 'Champs mis à disposition',
              xtype : 'tagfield',
              allowBlank : (Ext.getCmp("metadata_layertype").getValue()==="raster" ? true : false),
              queryMode : 'local',
              filterPickList : true,
              stacked : true,
              growMax : 100,
              valueField : 'fieldname', displayField : 'fieldname',
              store : Ext.create('Ext.data.ArrayStore', {
                fields : ['fieldname'],
                data : record.get('fieldnames')
              })
            }, {
              name : 'wxs_projection',
              xtype : 'tagfield',
              fieldLabel : 'Projection(s)',
              store : Carmen.Dictionaries.PROJECTIONS,
              filterPickList : true,
              stacked : true,
              growMax : 100,
              valueField : 'valueField', displayField : 'displayField'
            }]
          }]
        });
      }
    }, this);
    
    return {
      defaults : {scrollable : 'y'},
      xtype : 'tabpanel',
      maxHeight : 300, 
      deferredRender : false,
      border : true,
      items : sheets,
      hidden : sheets.length==0
    };
  }
});
