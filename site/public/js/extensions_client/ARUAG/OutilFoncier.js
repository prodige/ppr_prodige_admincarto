/**
 * @client ARUAG - GéoGuyane
 * @extends /carmenweb/js/CarmenJS/Carmen/Ext/Map/MapTools.js
 * 
 * Ajout d'une case à cocher dans le paramétrage des outils de la carte pour activer l'outil de demande de foncier de géoguyane
 */
Ext.onReady(function(){
  var configTool = Ext.Object.toQueryString({
  	toolTitle   : 'Export PDF pour demande de foncier',
  	modele      : '../demandedefoncier.tpl',
    title       : 'Outil du Foncier Agricole de Guyane',
    logo        : 'logoFEADER.jpg',
    'scales[0]' : '36100',
    'scales[1]' : '18100'
  });

  if ( Carmen && Carmen.__CMN_MAPTOOLS__ ) { 
    var modelFields = [].concat(Carmen.__CMN_MAPTOOLS__.modelFields);
    var formItems = Carmen.__CMN_MAPTOOLS__.formItems;
    
    /* 1. Ajout d'un field dans le model de Carmen.Model.MapTools */
    var index = Ext.Array.pluck(modelFields, 'name').indexOf('exportMultiple');
    if ( index!=-1 ){
      var field = Ext.apply({}, modelFields[index]);
      field.name = 'fixedScalesExportPDF';
      field.type = 'string';
      field.defaultValue = null;
      field.convert = function(value){
        if ( value===true || value=="on" || value==configTool ) 
          return configTool; 
        return null;
      };
      field.serialize = function(value){
        if ( value===true || value=="on" || value==configTool ) 
          return configTool; 
        return null;
      };
      modelFields[index+1] = field;
    }
    modelFields.push({name:'mapId'});
    Carmen.Model.MapTools.replaceFields(modelFields, true);
    Carmen.Model.MapTools.prototype.fieldOrdinals = Carmen.Model.MapTools.fieldOrdinals;
    Carmen.Model.MapTools.prototype.fieldsMap = Carmen.Model.MapTools.fieldsMap;
    Carmen.Model.MapTools.prototype.fields = Carmen.Model.MapTools.fields;
    
    /* 2. Ajout d'un field dans le formulaire mapTools */
    var search = function(data, parent){
      var result = {item : null, parent:parent};
      if ( Ext.isArray(data) ){
        Ext.each(data, function(item, index){
          var find = search(item, item);
          if ( find.item ){
            result = find;
          }
        });
        if ( data.items ){
          Ext.each(data.items, function(item, index){
            var find = search(item, data);
            if ( find.item ){
              result = find;
            }
          });
        }
      }
      if ( Ext.isObject(data) ){
        if ( data.name=="exportMultiple" ){
          result.item = data;
          return result;
        }
        if ( data.items ){
          Ext.each(data.items, function(item, index){
            var find = search(item, data);
            if ( find.item ){
              result = find;
            }
          });
        }
      }
      return result;
    };
    
    var find = search(formItems, null);
    if ( find.item && find.parent ){
      var index = (find.parent.items ? Ext.Array.pluck(find.parent.items, 'name').indexOf('annotation') : -1);
      if ( index!=-1 ){
        var field = Ext.apply({}, find.parent.items[index]);
        field.id = 'maptools_fixedScalesExportPDF';
        field.name = 'fixedScalesExportPDF';
        field.beforeBoxLabelTextTpl = '<i class="fa fa-fw fa-file-pdf-o"></i> ';
        field.boxLabel = "Edition d'une carte de demande de foncier";
        field.inputValue = configTool;
        find.parent.items[index+1] = field;
      }
    }
  }
})