/**
 * Class: Descartes.Button.ContentTask
 * Classe "abstraite" d�finissant un bouton permettant la modification du contenu d'une carte.
 * 
 * :
 * Les saisies n�cessaires aux traitements associ�s au bouton sont assur�es par une instance de <Descartes.ModalDialog>.
 * 
 * :
 * Le bouton doit �tre instanci� par un gestionnaire de contenu (voir <Descartes.action.MapContentManager>) � qui il transmet les valeurs saisies pour modification du contenu de la carte.
 * 
 * H�rite de:
 *  - <Descartes.Button>
 * 
 * Classes d�riv�es:
 *  - <Descartes.Button.ContentTask.AbstractEditGroup>
 *  - <Descartes.Button.ContentTask.AbstractEditLayer>
 *  - <Descartes.Button.ContentTask.RemoveGroup>
 *  - <Descartes.Button.ContentTask.RemoveLayer>
 *  - <Descartes.Button.ContentTask.ChooseWmsLayers>
 * 
 * Ev�nements d�clench�s:
 * done - Fermeture de la boite de dialogue avec validation des saisies.
 * 
 * Ecouteurs mis en place:
 *  - l'�v�nement 'done' de la classe <Descartes.ModalDialog> d�clenche la m�thode <sendDatas>.
 *  - l'�v�nement 'cancelled' de la classe <Descartes.ModalDialog> d�clenche la m�thode <deleteDialog>.
 */
Descartes.Button.ContentTaskOverloaded = {

    EVENT_TYPES: ["done", "activate", "deactivate"]

};
OpenLayers.Util.extend(Descartes.Button.ContentTask.prototype, Descartes.Button.ContentTaskOverloaded);
