/**
 * Class: Descartes.Tool.Informationcircle
 * Classe définissant un bouton permettant d'interroger la
 * carte suite à une sélection circulaire
 * 
 * Hérite de:
 *  - <Descartes.Tool.Information>
 */
Descartes.Tool.InformationCircle = OpenLayers.Class(Descartes.Tool.Information, {
    
    /**
     * Constructeur: Descartes.Tool.InformationCircle
     * Constructeur d'instances
     */
    initialize: function(options) {
        Descartes.Tool.Information.prototype.initialize.apply(this,arguments);
        
        // Handler Circle pour selection circulaire
        this.querytype = "circle";
        var handlerOptions = OpenLayers.Util.extend({}, this.defaultHandlerOptions);
        //keymask du control Openlayers
        this.handler = new Descartes.Handler.Circle(this,{done: this.onSelection}, {keyMask: this.keyMask} );
    },
    
    /**
     * Methode: onSelection
     * Effectue l'interrogation en fonction du handler
     * 
     * Paramètre:
     * evt - {<OpenLayers.Event>} événement déclenché par le handler associé .
     */
    onSelection: function(evt) {
        //hasGFI permet de savoir si le getFeatureInfo renvoie des résultats ou pas
    	hasGFI = false;
        this.action.performQuery(evt, null);
    },

    CLASS_NAME: "Descartes.Tool.InformationCircle",

    VERSION : "3.1"
});