/*
Copyright MEEDDM
Contributeur : Ga�lle Barris, Denis Chabrier

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.Tool.MiniMapImage
 * Classe d�finissant une mini-carte de navigation li�e � la carte principlae.
 * 
 * H�rite de:
 *  - <Descartes.I18N>
 */
Descartes.Tool.MiniMapImage = OpenLayers.Class(Descartes.I18N, {
	
    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML de la mini-carte.
     * 
     * :
     * globalClassName - Racine des noms de classes CSS pour les �l�ments cr��s par OpenLayers.Control.OverviewMap ("DescartesMiniMap" par d�faut).
     */
	defaultDisplayClasses: {
		globalClassName: "cmnControlOverviewMap"
	},

    /**
     * Propriete: defaultOverviewParams
     * {Object} Objet JSON stockant les param�tres optionnels de configuration par d�faut de la mini-carte.
     * 
     * :
     * size - Taille de la mini-carte ("OpenLayers.Size(150,90)" par d�faut).
     */
	defaultOverviewParams: {
		size: new OpenLayers.Size(150,90)
	},
	
	/**
	 * Propriete: size
	 * {OpenLayers.Size} Taille de la mini-carte
	 */
	size: null,
	
	/**
	 * Propriete: open
	 * {Boolean} Indique si la mini-carte est "ouverte" lors de l'affichage initial de la carte.
	 */
	open: true,
	
	/**
	 * Constructeur: Descartes.Tool.MiniMapImage
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * OL_map - {OpenLayers.Map} Carte OpenLayers li�e � la mini-carte.
	 * resourceUrl - {String} URL de la ressource WMS alimentant la mini-carte
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
	 * 
	 * Options de construction propres � la classe:
	 * open - {Boolean} Indique si la mini-carte est "ouverte" lors de l'affichage initial de la carte.
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la mini-carte.
	 * messages - {Object} Objet JSON stockant les messages selon la forme : {use:"Instructions", maximize:"Info-bulle pour ouvrir la mini-carte", minimize:"Info-bulle pour fermer la mini-carte"}
	 * size - {OpenLayers.Size} Taille de la mini-carte
	 */
	initialize: function(OL_map, resourceUrl, options) {
		Descartes.I18N.prototype.initialize.call(this);
    		
		var displayClasses = OpenLayers.Util.extend({}, this.defaultDisplayClasses);
    	var messages = {
    		use: this.getMessage("TITLE_USE_MINIMAP"),
    		maximize: this.getMessage("TITLE_MAXIMIZE"),
    		minimize: this.getMessage("TITLE_MINIMIZE")
    	};
    	
		if((options !== undefined)){
			OpenLayers.Util.extend(displayClasses, options.displayClasses);
			delete options.displayClasses;
			OpenLayers.Util.extend(messages, options.messages);
			delete options.messages;
		}
		OpenLayers.Util.extend(this, this.defaultOverviewParams);
		OpenLayers.Util.extend(this, options);
		
		var maxExtent = this.maxExtent || OL_map.maxExtent;
		var projection = this.projection || OL_map.projection;
		var units = this.units || OL_map.units;
		var div = this.div ? Descartes.Utils.getDiv(this.div) : null;
		
		var overview_OL =  new OpenLayers.Layer.Image(
		    "MiniMapImage",
		    resourceUrl,
		    maxExtent,
		    this.size, 
		    {
          maxResolution: maxExtent.getWidth()/this.size.w,
		      projection : projection,
		      units : units
		    }
		);
		 
		var olOptions = {
			displayClass: displayClasses.globalClassName,
			size: this.size,
			layers : [overview_OL],
			minRectSize : 15,
			title: messages.use,
			minRatio: 1,
			maxRatio: 1000000000,
			autoPan: false,
			mapOptions : {
				numZoomLevels: 1,
				maxExtent : maxExtent,
				projection : projection,
				units : units
				,theme: Descartes.getThemeLocationForOL()
			} 
		};
		
		if (div) {
			olOptions.div = div;
			olOptions.outsideViewport = true;
		}
		
		var overview = new OpenLayers.Control.OverviewMap(olOptions);
		
		//overview.displayClass = this.CLASS_NAME.replace("Carmen.", "cmn").replace(/\./g, "");
		
		OL_map.addControl(overview);
		
		if (!div) {
			overview.maximizeDiv.title = messages.maximize;
			overview.minimizeDiv.title = messages.minimize;
			if(this.open) {
				overview.maximizeControl();
			}
		}
	},
	
	CLASS_NAME: "Descartes.Tool.MiniMapImage"
});