/**
 * Class: Descartes.Tool.Informationrect
 * Classe définissant un bouton permettant d'interroger la
 * carte suite à une sélection rectangulaire
 * 
 * Hérite de:
 *  - <Descartes.Tool.Information>
 */
Descartes.Tool.InformationUrl = OpenLayers.Class(Descartes.Tool.Information, {
    
    /**
     * Constructeur: Descartes.Tool.InformationRect
     * Constructeur d'instances
     */
    initialize: function(options) {
        Descartes.Tool.Information.prototype.initialize.apply(this,arguments);
        this.queryurl = 'URL';
        
        // Handler.Box pour selection rectangulaire
        var handlerOptions = OpenLayers.Util.extend({}, this.defaultHandlerOptions);        
        this.handler = new OpenLayers.Handler.Box( this,
                          {done: this.onSelection}, handlerOptions );
    },
    
    /**
     * Methode: onSelection
     * Effectue l'interrogation en fonction du handler
     * 
     * Paramètre:
     * evt - {<OpenLayers.Event>} événement déclenché par le handler associé .
     */
    onSelection: function(evt) {
		//hasGFI permet de savoir si le getFeatureInfo renvoie des résultats ou pas
    	hasGFI = false;
        this.action.performQuery(evt, 'URL');
    },

    CLASS_NAME: "Descartes.Tool.InformationUrl",

    VERSION : "3.1"
});