/**
 * Class: Descartes.Tool.Information
 * Classe définissant un bouton permettant d'interroger la carte suite à un clic du pointeur sur celle-ci.
 * 
 * Hérite de:
 *  - <Descartes.Tool>
 */
Descartes.Tool.InformationPoint = OpenLayers.Class(Descartes.Tool.Information, {

	/**
	 * Constructeur: Descartes.Tool.Information
	 * Constructeur d'instances
	 */
	initialize: function(options) {
        Descartes.Tool.Information.prototype.initialize.apply(this,arguments);
				
        // Handler.Click
		var handlerOptions = OpenLayers.Util.extend({}, this.defaultHandlerOptions);
		this.handler = new OpenLayers.Handler.Click(this, {'click': this.onSelection}, handlerOptions);
	},
	
    /**
     * Methode: onSelection
     * Effectue l'interrogation en fonction du handler
     * 
     * Paramètre:
     * evt - {<OpenLayers.Event>} évènement déclenché par le handler associé .
     */
	onSelection: function(evt) {
		//hasGFI permet de savoir si le getFeatureInfo renvoie des résultats ou pas
		hasGFI = false;
		
		if (this.mapContent) {
			var layers = [];
			for (var iLayer=0 ; iLayer<this.mapContent.layers.length ; iLayer++) {
				if (this.mapContent.layers[iLayer].configuration) {
					if (this.mapContent.layers[iLayer].configuration.jsonLayerDesc.Extension.INTERROGEABLE == "ON"){
						layers.push(this.mapContent.layers[iLayer]);
						this.mapContent.item.items[iLayer].activeToQuery = true;
					}
				}
			} 
		}
		
		if (layers.length > 0 ){
			var myCallback = function(reponse) {

				hasGFI = true;
				this.action.performQuery(evt.xy, null, reponse);

			};

			this.action.performGetFeatureInfo(evt, OpenLayers.Function.bindAsEventListener(myCallback,this));
			
		}else{
			this.action.performQuery(evt.xy, null, null);
		}
  
	},

	CLASS_NAME: "Descartes.Tool.InformationPoint",

	VERSION : "3.1"
});