/**
 * Class: Descartes.Tool.Information
 * Classe d�finissant un bouton permettant d'interroger la carte suite � un clic du pointeur sur celle-ci.
 * 
 * H�rite de:
 *  - <Descartes.Tool>
 */
Descartes.Tool.Information = OpenLayers.Class(Descartes.Tool, {

	/**
	 * Propriete: defaultHandlerOptions
	 * {Object} Objet JSON stockant les propri�t�s par d�faut du OpenLayer.Handler.Click associ�.
     * 
     * :
     * single - true
     * double - false
     * pixelTolerance - 0
     * stopSingle - false
     * stopDouble - false
	 */
	defaultHandlerOptions: {
		  'single': true,
		  'double': false,
		  'pixelTolerance': 0,
		  'stopSingle': false,
		  'stopDouble': false
	},
	action: null,
	querytype: null,
	
	urlquery: null,
    
    /**
     * Methode: setMapContent
     * R�cup�re les param�tres de mapContent
     * 
     * Param�tres:
     * mapContent objet contenant la majorit� de la config
     *   
     */
    setMapContent: function(mapContent){
        this.mapContent = mapContent;
        this.initAction();
    },

    /**
     * Methode: setMap
     * R�cup�re les param�tres de map
     * 
     * Param�tres:
     * map - objet OpenLayers
     */
    setMap: function(map){
        Descartes.Tool.prototype.setMap.call(this, map);
        this.initAction();
    },
	
	initAction : function() {
		if (this.map && this.mapContent) {
			this.action = new Descartes.Action.CarmenRequest(this.mapContent, this.mapfile, this.map, this.querytype);
		}
	},
	
	/**
	 * Constructeur: Descartes.Tool.Information
	 * Constructeur d'instances
	 */
	initialize: function(options) {	
        Descartes.Tool.prototype.initialize.apply(this,arguments);
	},
	
    /**
     * Methode: activate
     * Rend le bouton actif parmi l'ensemble des boutons de type <Descartes.Tool> pr�sents dans la barre d'outils � laquelle il appartient.
     */
    activate: function() {
        OpenLayers.Control.prototype.activate.apply(this,arguments);
    },

    /**
     * Methode: deactivate
     * Rend le bouton actif parmi l'ensemble des boutons de type <Descartes.Tool> pr�sents dans la barre d'outils � laquelle il appartient.
     */
    deactivate: function(){
        Descartes.Tool.prototype.deactivate.apply(this,arguments);

        // Cacher le panel Ext propre � l'outil de selection sinon
        // quand on switche entre les interrogations avec une
        // fen�tre d�j� ouverte, les deux sont affich�es.
        if (this.action && this.action.renderer)
          this.action.renderer.hide();
    },

	CLASS_NAME: "Descartes.Tool.Information",

	VERSION : "3.1"
});