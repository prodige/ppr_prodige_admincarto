/**
 * Descartes.ProgressBar : Manages a progress bar for long treatments
 * 3 possibilities : 
 * 
 * - The request is sent via a OpenLayers.Request.POST (or any simple ajax request) 
 *      -> use OpenLayers.Request.POST_Descartes
 * 		the callback is overloaded to manage the stopping of the progress
 * 
 * - A form is used for the request and a IFrame "load" callback can be used
 * 		-> use attachProgressToIFrame
 * 		the progress is stopped on the "load" event of the frame or the "close" event of window.
 * 
 * - For downloads of files, load event is never fired and the server has to give the information of the end of the treatment.
 * 		-> use startProgress(true)
 * 		-> on server side use service/progressBar/progress.php stopProgress method to notify the end of the treatment
 * 		getStatus.php counts the number of ended treatments
 */ 
 Descartes.ProgressBar =  Descartes.ProgressBar || {
					delayedDisplay : null,	// current timeOut id for displaying the progressBar
					sentRequest : 0,		// how many requests have been sent since progressBar has started
					pendingRequest : 0,  	// how many pending request 
					startTimeout : 1500,	// delay to wait until displaying the bar
					checkInterval : 1000,	// interval between 2 serveur status check
					
					// start a progressBar
					// @param
					// 		serverStatusCheck : boolean
					startProgress: function(serverStatusCheck){ 	
						Descartes.ProgressBar.sentRequest ++;
						Descartes.ProgressBar.pendingRequest ++;
						if (serverStatusCheck){
							window.setTimeout(Descartes.ProgressBar.getstatus,  Descartes.ProgressBar.startTimeout - 200);
						}
						Descartes.ProgressBar.delayedDisplay = window.setTimeout(Descartes.ProgressBar.displayWaitMessage,  Descartes.ProgressBar.startTimeout, null, serverStatusCheck);
												
					},
					getstatus :  function(){ 						// request the server status of current requests
						var success = function(data){     
							var rep = Ext.util.JSON.decode(data.responseText);
							Descartes.ProgressBar.displayWaitMessage(rep, true);
						};
						
						Ext.Ajax.request({
					    	  url: "/services/progressBar/getStatus.php",
					          success: success
					      	});
					},
					// displays the bar and manage progression 
					// @param
					//		rep 				: {stoppedRequest : int}
					// 		serverStatusCheck 	: boolean
					displayWaitMessage : function(rep, serverStatusCheck){
						if ((rep != null && rep.stoppedRequest == Descartes.ProgressBar.pendingRequest) || Descartes.ProgressBar.pendingRequest <= 0) {
							Descartes.ProgressBar.sentRequest = 0;
							Descartes.ProgressBar.pendingRequest = 0;
							// fin des traitements en cours
							if (Ext.Msg.isVisible()){
								Ext.Msg.hide();
							}
							if (Descartes.ProgressBar.delayedDisplay){
								window.clearTimeout(Descartes.ProgressBar.delayedDisplay);
								Descartes.ProgressBar.delayedDisplay = null;
							}
						}else {
							if (rep != null && rep.stoppedRequest != undefined ){
								Descartes.ProgressBar.pendingRequest -= rep.stoppedRequest;
							}
								
							// les traitements sont en cours.
							var value  = (Descartes.ProgressBar.sentRequest - Descartes.ProgressBar.pendingRequest) / Descartes.ProgressBar.sentRequest;
							var text = Descartes.ProgressBar.pendingRequest + " traitement(s) en cours ...";
							
							// needs to update progress
							if (Ext.Msg.isVisible()){
								Ext.Msg.updateProgress(value, text);
							}else{
								Ext.Msg.show({
					                title : "Chargement en cours",
					                buttons: false,
					                progress:true,
					                closable:true,
					                modal:false,
					                minWidth: Ext.Msg.minProgressWidth,
					                progressText: text
					            });
							}
								
							if (serverStatusCheck){
						        Descartes.ProgressBar.delayedDisplay = window.setTimeout(Descartes.ProgressBar.getstatus, Descartes.ProgressBar.checkInterval);
							}
						}
					},
					// attach progressBar with IFrame load
					// @param : 
					//			config 		: {load : string, close : string} -> les ids des composants Ext auxquels les events doivent être attachés.  
					//														  ! Ext.ux.IFrameComponent pour le load !
					attachProgressToIFrame : function(config){ 
						if (!config || !config.load){
							return;
						}
						var idIFrameLoad = config.load;
						
						// on affiche la barre de progression						
						Descartes.ProgressBar.startProgress(false);
						
						// on masque la barre au chargement de la fenetre (ou a l'echec)
						Ext.getCmp(idIFrameLoad).el.on("load", Descartes.ProgressBar.progress)
						Ext.getCmp(idIFrameLoad).el.on("error", Descartes.ProgressBar.progress)
						
						// si la fenetre est fermée avant que la frame soit chargée, 
						// il faut aussi masquer la barre de progression
						var idIFrameClose = config.close;
						if (idIFrameClose){
							Ext.getCmp(idIFrameClose).on("close", Descartes.ProgressBar.progress);
							
							// pour ne PAS faire avancer 2 fois la barre : 
							// au chargement de la fenetre puis à sa fermeture, 
							// on supprime le listener sur la fermeture
							var unbindClose = function(){
								Ext.getCmp(idIFrameClose).un("close", Descartes.ProgressBar.progress);
							}
							Ext.getCmp(idIFrameLoad).el.on("load", unbindClose);
							Ext.getCmp(idIFrameLoad).el.on("error", unbindClose);
						}
					},
					progress :function(){
						Descartes.ProgressBar.displayWaitMessage({stoppedRequest:1},false);
					}					
				};
 
 /**
  * Surcharge du post OpenLayers pour gérer 
  * une barre de chargement en fonction 
  * du nombre de requêtes en cours de traitement.
  */
OpenLayers.Request.POST_Descartes = function(config) {
		
		Descartes.ProgressBar.startProgress();
		
		var cb= config.callback;
		var newCb = function(res){
			Descartes.ProgressBar.progress();
			if (cb && typeof(cb) == "function"){
				cb(res);
			}	
		};
		config.callback = newCb;

		return OpenLayers.Request.POST(config);						
	};
	
