
/**
 * Class: Descartes.Action.ProdigeBuffer 
 * 
 * Inherits from: - <Descartes.Action>
 */
Descartes.Action.ProdigeBuffer = new OpenLayers.Class(Descartes.Action,{
    
    action: null,
    
    map: null,
    
    //url ajax
    serviceUrl: null,
     
    mapfile: null,
    
    mapContentManager: null,
    
    //OL
    style : {},
    
        
    /**
     * Constructeur: Descartes.Action.ProdigeBuffer
     * Constructeur d'instances
     * 
     * Param�tres:
     * div - {DOMElement|String} El�ment DOM de la page accueillant l'interface associ�e ou identifiant de cet �lement.
     * OL_map - {OpenLayers.Map} Carte OpenLayers sur laquelle intervient le contr�leur.
     * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
     * 
     * Options de construction propres � la classe:
     * params -  {Object} Objet JSON stockant les param�tres de mise en page initiaux (�tendus par <defaultParams>), structur� comme le mod�le associ�.
     * infos - {Object} Objet JSON stockant les informations compl�mentaires � ins�rer dans le fichier PDF.
     */
    initialize: function (mapContent, OL_map, options){
    	this.model = {};
    	this.model.mapfile = window.mapfile;
    	this.serviceUrl = '/services/Buffer/index.php';
    	this.model.mode = options.mode;
   	
        Descartes.Action.prototype.initialize.call(this, this.div, OL_map, Descartes.ViewManager.getView(Descartes.ViewManager.PRODIGE_BUFFER_IN_PLACE), {});
        this.renderer.events.register('bufferFromParameters', this, this.bufferFromParameters);
//        this.renderer.events.register('bufferFromSelection', this, this.bufferFromSelection);
    },
 
    bufferFromParameters : function() {
    	if(this.model.queryParams){
    		this.performAjax(this.model.queryParams);
    	}
        //console.log('bufferFromSelection');    
    },    

    performAjax : function(queryParams) {
    	//modif dds
    	this.myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Génération de la zone tampon,Patientez svp..."});
    	
    	this.myMask.show();
        if (queryParams!=null) { 
            // launching request... 
            Ext.Ajax.request({
            url: this.serviceUrl,
            success: this.drawFeatures,
            failure: function (response) {
            	this.myMask.hide();
            	Descartes.UtilsOverloaded.handleAjaxFailure(response, "Erreur", true)
            },
            scope : this,
            params: queryParams
        });
        }
    },

    performQuery: function(options){
    	var units = Descartes.Utils.getProjectionUnits(this.OL_map.projection);
        options.units = units;
        // mapContentManager pouvant venir de annotation
        if ((this.mapContentManager == null && options.mapContentManager != null)) {
        	this.mapContentManager = options.mapContentManager;
        }
    	this.renderer.draw(options);
    },    
    
  // draw buffer geometries returned by buffer service... 
    drawFeatures : function (response) {
    	//modif dds
    	this.myMask.hide();
	    var layerName = this.model.selectedLayer;
	    this.style = this.model.selectedStyle;

	    var data = {"title": layerName,
	        "type": Descartes.Layer.TYPE_ANNOTATION,
	        "layersDefinition": [{"style": this.style, "projection": this.OL_map.projection}],
	        "options": {"queryable": false,"legend":[]}
	    };
	    
	    // Assign mapContenManager to this.mapContentManager
	    this.mapContentManager = app.map.mapContentManager;
	    
	   //Return a Descartes.Layers.Annotation
	    var bufferLayer = this.mapContentManager.addBufferLayers(data);
	    //Retrieve the layer
	    bufferLayer = bufferLayer.OL_layers[0];
	    
        // drawing features into layer
        response = Ext.decode(response.responseText);
        for (var i=0; i<response.length; i++) {
			var attributes = {fid : response[i].fid}
			// Modif SWT [P2-5] 7590 : Saisie de plusieurs attributs pour les annotations
			// on ajoute les noms des champs qui contiennent les attributs de l'annotation
			// ainsi que les valeurs des attributs
			var annotationAttributes = null;
			if(this.model.ctx && this.model.ctx.mdataTool.attribut_value_annotation != ''){
				annotationAttributes = this.model.ctx.getAnnotationAttributes();
			}
			if(this.model.ctxIhm && this.model.ctxIhm.mdataTool.attribut_value_annotation != ''){
				annotationAttributes = this.model.ctxIhm.getAnnotationAttributes();
			}
			if (annotationAttributes != null) {
				for ( var k = 0; k < annotationAttributes.length; k++) {
					var attributAnnotation = annotationAttributes[k];
					attributes[attributAnnotation] = response[i][attributAnnotation];
				}
	        }
			
            var f = new OpenLayers.Feature.Vector(
                OpenLayers.Geometry.fromWKT(response[i].geom), attributes);
            var f_simples = Descartes.Utils.simplifyFeature(f);
            bufferLayer.addFeatures(f_simples);
        }
        
    },
    // displaying results
    showWindow : function() {
    	
    },

    CLASS_NAME: "Descartes.Action.ProdigeBuffer",

    VERSION : "3.1"
});

//
Descartes.Action.ProdigeBuffer.ACTIVATED = 0;
//Selection peut �tre activ�e
Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED = 0;
//Param�tres pour le partage avec Informations 
Descartes.Action.ProdigeBuffer.COMBO_LAYERS = 0;
Descartes.Action.ProdigeBuffer.CONFIG_LAYERS = 0;

Descartes.Action.ProdigeBuffer.bufferCount = 1;