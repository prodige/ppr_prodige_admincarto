
/**
 * Class: Descartes.Layer.geoJson
 * Objet "m�tier" correspondant � une couche pour le contenu de la carte, bas�e sur des *fichiers geoJson*.
 * 
 * H�rite de:
 * - <Descartes.Layer.Vector>
 */
Descartes.Layer.GeoJson = OpenLayers.Class(Descartes.Layer.Vector, {

	/**
	 * Propriete: keepInternalStyles
	 * {Boolean} Indique si les styles embarqu�s dans le geoJson doivent �tre conserv�s.
	 */
  keepInternalStyles: true,

  epsg: null,
  
  clusterOptions: null,

	/**
	 * Constructeur: Descartes.Layer.geoJson
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * title - {String} Intitul� de la couche.
	 * layersDefinition - {Array(<Descartes.ResourceLayer>)} D�finition des couches geoJson constituant la couche.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * keepInternalStyles - {Boolean} Indique si les styles embarqu�s dans le geoJson doivent �tre conserv�s.
	 */
  initialize: function (title, layersDefinition, options) {
    Descartes.Layer.TYPE_GEOJSON = 1000;
    this.type = Descartes.Layer.TYPE_GEOJSON;

    this.symbolizers = OpenLayers.Util.extend({}, Descartes_Symbolizers_WFS);

    this.epsg = options.epsg;

    console.log(options, this.clusterOptions ,  options.clusterOptions);

    
    Descartes.Layer.Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
  },
  
  setClusterOptions: function(clusterOptions){
    this.clusterOptions = clusterOptions;
  },
	/**
	 * Methode: createOL_layer
	 * Cr�e une couche OpenLayers Vector repr�sentant la couche geoJson.
	 * 
	 * Retour:
	 * {OpenLayers.Layer.Vector} La couche OpenLayers cr��e. 
	 */
  createOL_layer: function (layerDefinition) {
    let self = this;
    let sizeFix = this.clusterOptions ? this.clusterOptions.fixedSize : false;
    console.log(50, layerDefinition, this.clusterOptions);
    
    var map =  null;
    function display(event) {
      console.log('display loop');
      console.log(event.type);
      var f = event.feature;
      
    }

    function getColor(feature, fromStroke){
      let size  = feature.cluster && feature.cluster.length ? feature.cluster.length : 1;
      let color = size > 25 ? '248, 128, 0' : size > 8 ? '248, 192, 0' : '128, 192, 64';

      
      color =  'rgba(' + color + ',' + (fromStroke ? 0.3 : 0.6) +')' ;
      if(self.clusterOptions.singleColor && self.clusterOptions.color){
        color = '#' + self.clusterOptions.color;
      }
    
      return color;
    }

   
    var style = new OpenLayers.Style({
      pointRadius: "${radius}",
      fillColor: "${fillColor}",//"#ffcc66",
      fillOpacity: 0.8,
      strokeColor: "${strokeColor}" ,//"#cc6633",
      strokeWidth: 2,
      strokeOpacity: 0.8,
      label: "${text}"
    }, {
      context: {
        fillColor: function(feature){
          return getColor(feature, false);
        },
        strokeColor: function(feature){
          return getColor(feature, true);
        },
        radius: function (feature) {

          return sizeFix ? self.clusterOptions.size : Math.min(feature.attributes.count, 7) + 3;
        },
        text: function (feature) {
          if (feature.cluster && feature.cluster.length > 1 && self.clusterOptions && self.clusterOptions.withText) {
            return feature.cluster.length;
          }

          return '';
        }
      }
    });

    var params = this.getCommonParams();
    console.log(this.epsg, layerDefinition.serverUrl)
    params.projection = new OpenLayers.Projection(this.epsg);
    params.strategies = [new OpenLayers.Strategy.Fixed(), new OpenLayers.Strategy.Cluster({ distance: (self.clusterOptions.distance ? self.clusterOptions.distance : 50) })];
    params.protocol =  new OpenLayers.Protocol.Script({
      url: layerDefinition.serverUrl,
      format: new OpenLayers.Format.GeoJSON()
    })

    params.styleMap = new OpenLayers.StyleMap({
      "default": style,
      "select": {
        fillColor: "#8aeeef",
        strokeColor: "#32a8a9",
        label: "Test"
      }
    })

    var vector = new OpenLayers.Layer.Vector("geoJson", params);

    var highlightCtrl = new OpenLayers.Control.SelectFeature(
      vector, {
      hover: true,
      highlightOnly: true,
      renderIntent: "temporary",
      eventListeners: {
        beforefeaturehighlighted: display,
        featureunhighlighted: display
      }
    });

    vector.events.register("added", vector, () => {
      setTimeout(() => {
        console.log(145, vector.map);
        $.ajax({
          xhrFields: { withCredentials: true },
          async: false,
          url: layerDefinition.serverUrl
        })
        .done(result => {
       
            let geojsonFormat = new OpenLayers.Format.GeoJSON();
            vector.addFeatures(geojsonFormat.read(result));
            console.log(vector)
            let map = vector.map;
      
            map.addControl(highlightCtrl);
            
      
            highlightCtrl.activate();
      
       
        }).fail(function(){
          console.log('fail',arguments); 
        }).always(function(){
          console.log('always',arguments); 
        }).abort(function(){
          console.log('abort',arguments); 
        });
      }, 1000)
     
    });

  
   

    return vector;
  },

	/**
	 * Methode: toJSON
	 * Fournit une repr�sentation simplifi� de la couche sous forme d'objet JSON.
	 * 
	 * Retour:
	 * {Object} Objet JSON.
	 * 
	 * :
	 * L'objet JSON est de la forme suivante :
	 * (start code)
	 * {
	 * 		title: <Intitul�>,
	 * 		type: <Type de couche>,
	 * 		layersDefinition: {
	 * 				layerName: <Nom de la couche ou de l'objet OWS>
	 * 				serverUrl: <URL du serveur OWS>
	 * 			} [],
	 * 		options: {
	 * 				format: <Type MIME pour l'affichage des couches OpenLayers>,
	 * 				legend: <Adresses d'acc�s aux l�gendes>,
	 * 				metadataURL: <Adresse d'acc�s aux informations compl�mentaires>,
	 * 				attribution: <Texte de copyright>,
	 * 				queryable: <Interrogation potentielle>,
	 * 				sheetable: <Affichage des propri�t�s sous forme de tableau>,
	 * 				maxScale: <D�nominateur de l'�chelle maximale>,
	 * 				minScale: <D�nominateur de l'�chelle minimale>
	 * 			}
	 * }
	 * (end) 
	 */
  toJSON: function () {
    var json = {};
    json.title = this.title;
    json.type = this.type;
    json.layersDefinition = [{
      serverUrl: this.OL_layers[0].protocol.url
    }];
    json.options = {
      format: this.format,
      legend: (this.legend !== null) ? this.legend[0] : "",
      metadataURL: (this.metadataURL !== null) ? this.metadataURL : "",
      attribution: (this.attribution !== null) ? this.attribution : "",
      queryable: this.queryable,
      sheetable: this.sheetable,
      maxScale: (this.maxScale !== null) ? this.maxScale : "",
      minScale: (this.minScale !== null) ? this.minScale : ""
    };
    return json;
  },

	/**
	 * Methode: serialize
	 * Fournit une repr�sentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
	 * 
	 * Retour:
	 * {Object} Objet JSON.
	 */


  CLASS_NAME: "Descartes.Layer.geoJson",

  VERSION: "3.1"
});
