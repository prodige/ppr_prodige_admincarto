/**
 * Class: Descartes.MapContent
 * Objet "métier" correspondant au contenu de la carte en terme de groupes et couches.
 * 
 * :
 * Les éléments constituant le contenu de la carte peuvent être des instances de <Descartes.Group> et/ou <Descartes.Layer>.
 * 
 * Evènements déclenchés:
 * changed - La liste des éléments du contenu de la carte a changé.
 * itemPropertyChanged - Les propriétés des éléments du contenu de la carte ont changé (visibilités, opacités).
 * layerRemoved - Une couche a été supprimée.
 */
Descartes.MapContentOverloaded = {
  
  EVENT_TYPES: ["changed", "itemPropertyChanged", "layerRemoved", "toolTipsChanged"],
  
  /**
   * Methode: addItem
   * Ajoute un �l�ment au contenu de la carte.
   * 
   * Param�tres:
   * item - {<Descartes.Group> | <Descartes.Layer>} El�ment � ajouter.
   * parentItem - {<Descartes.Group>} Groupe o� ajouter l'�l�ment. S'il est nul, l'�l�ment est ajout� � la racine.
   * 
   * Retour:
   * {<Descartes.Group> | <Descartes.Layer>} El�ment ajout�.
   */
  addItem: function(item, parentItem) {
    if (item instanceof Descartes.Layer || item instanceof Descartes.Group) {
      if(parentItem && parentItem.items){
        parentItem.items.push(item);
      }else{
        this.item.items.push(item);
      }
      
      this.refresh(item);
    }
    return item;
  },
  old_refresh : Descartes.MapContent.prototype.refresh,
  refresh : function(){
  	this.getLayers();
      var copyLayers = [];
      this.layers.forEach(function(l){ copyLayers.push(OpenLayers.Util.extend({}, l)); });
      this.copyLayers = copyLayers;
    this.old_refresh.apply(this, arguments);
  },
  
  /**
   * Methode: removeItemByIndex
   * Supprime un élément du contenu de la carte, et déclenche l'évènement 'layerRemoved'.
   * 
   * Paramètres:
   * index - {String} Index hiérarchique de l'élément à supprimer.
   * isTriggerActive _ {Boolean} si le trigger layerRemoved doit être lancé (facultatif)
   */
  removeItemByIndex: function(index, isTriggerActive) {
    var indexes = index.split("#");
    if (indexes.length != 1) {
      index = indexes[indexes.length-1];
      indexes.splice(indexes.length-1,1);
      this.getItemByIndex(indexes.join("#")).items.splice(index, 1);
    } else {
      this.item.items.splice(index, 1);
    }
     if (isTriggerActive !== false) {
      this.refresh();
      this.events.triggerEvent("layerRemoved");
     }
  },
  
  /**
   * Methode: removeItemByIndex
   * Supprime un élément du contenu de la carte, et déclenche l'évènement 'layerRemoved'.
   * 
   * Paramètres:
   * index - {String} Index hiérarchique de l'élément à supprimer.
   * isTriggerActive _ {Boolean} si le trigger layerRemoved doit être lancé (facultatif)
   */
  removeItemById: function(itemId, isTriggerActive) {
    var item = this.getItemById(itemId, true);
    var parentItem = item.parent;
    var searchItem = item.search;
    if ( !(parentItem && searchItem) ) return;
    
    var itemIndex = parentItem.items.indexOf(searchItem);
    if ( itemIndex!=-1 ){
      parentItem.items.splice(itemIndex, 1);
    }

    if (isTriggerActive !== false) {
      this.refresh();
      this.events.triggerEvent("layerRemoved");
    }
    return item;
  },
  
  /**
   * Methode: reaffectItem
   * Déplace un élément au contenu de la carte, avant ou après un autre élément.
   * 
   * Paramètres:
   * movedItemIndex - {String} Index hiérarchique de l'élément à déplacer.
   * destItemIndex - {String} Index hiérarchique de l'élément de référence.
   * position - {"Top" | "Bottom"} Destination de l'élément par rapport à l'élément de référence.
   * 
   * Retour:
   * {<Descartes.Group> | <Descartes.Layer>} Elément déplacé.
   */
  reaffectItem: function(movedItemIndex, destItemIndex, position, activeRefresh) {
    var item = this.getItemByIndex(movedItemIndex);
    var destItem = this.getItemByIndex(destItemIndex);
    if (position === 'Top') {
      this.insertItemBefore(item, destItem, activeRefresh);
    } else {
      this.insertItemAfter(item, destItem, activeRefresh);
    }
    
    this.events.triggerEvent("toolTipsChanged");
  },
  
  /**
   * Methode: insertItemBefore
   * Ins�re un �l�ment au contenu de la carte, avant un autre �l�ment.
   * 
   * Param�tres:
   * item - {<Descartes.Group> | <Descartes.Layer>} El�ment � ajouter.
   * refItem - {<Descartes.Group> | <Descartes.Layer>}} El�ment avant lequel ins�rer le nouvel �l�ment.
   * 
   * Retour:
   * {<Descartes.Group> | <Descartes.Layer>} El�ment ins�r�.
   */
  insertItemBefore: function(item, refItem, activeRefresh) {
    if (item instanceof Descartes.Layer || item instanceof Descartes.Group) {
      var parentRefItem = this.getParentItem(refItem);
      this.removeItemById(item.id, false);
      parentRefItem.items.splice(OpenLayers.Util.indexOf(parentRefItem.items, refItem), 0, item);
      if (activeRefresh !== false) {
        this.refresh(item);
      }
    }
    return item;
  },
  
  /**
   * Methode: insertItemAfter
   * Ins�re un �l�ment au contenu de la carte, apr�s un autre �l�ment.
   * 
   * Param�tres:
   * item - {<Descartes.Group> | <Descartes.Layer>} El�ment � ajouter.
   * refItem - {<Descartes.Group> | <Descartes.Layer>}} El�ment apr�s lequel ins�rer le nouvel �l�ment.
   * 
   * Retour:
   * {<Descartes.Group> | <Descartes.Layer>} El�ment ins�r�.
   */
  insertItemAfter: function(item, refItem, activeRefresh) {
    if (item instanceof Descartes.Layer || item instanceof Descartes.Group) {
      var parentRefItem = this.getParentItem(refItem);
      this.removeItemById(item.id, false);
      parentRefItem.items.splice(OpenLayers.Util.indexOf(parentRefItem.items, refItem) +1, 0, item);
      if (activeRefresh !== false) {
        this.refresh(item);
      }
    }
    return item;
  },

  /**
   * Methode: getItemByIndex
   * Fournit un élément du contenu de la carte.
   * 
   * Paramètres:
   * id - {String} Id de l'élément recherché.
   * 
   * Retour:
   * {<Descartes.Group> | <Descartes.Layer>} Elément trouvé.
   */
  getItemById: function(id, asObject) {
    var item = this._getItemById(this.item, id);
    if ( !item ) return null;
    if ( !item.parent )
      item.parent = this.item;
    return (asObject ? item : item.search);
  },
  
  /*
   * PRIVATE
   */
  _getItemById: function(item, id) {
    if (item.id == id) {
      return {search : item};
    }
    
    var itemRetour = null;
    if (item.items && item.items.length) {
      for(var i=0; i<item.items.length && itemRetour==null; i++) {
        itemRetour = this._getItemById(item.items[i], id);
      }
      if ( itemRetour && !itemRetour.parent ) itemRetour.parent = item; 
    }
    return itemRetour;
  }
};

OpenLayers.Util.extend(Descartes.MapContent.prototype, Descartes.MapContentOverloaded);
