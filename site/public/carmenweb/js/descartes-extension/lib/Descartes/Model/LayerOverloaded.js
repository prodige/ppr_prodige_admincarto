/**
 * Class: Descartes.Layer
 * Objet "m�tier" correspondant � une couche pour le contenu de la carte.
 * 
 * :
 * Une couche peut comporter plusieurs couches OpenLayers. Ces derni�res sont alors manipul�es globalement.
 * 
 * :
 * C'est en fait un agr�gat de couches OpenLayers.
 * 
 * :
 * Par exemple, rendre la couche invisible masque simultan�ment toutes les couches OpenLayers qu'elle comporte.
 * 
 * Classes d�riv�es:
 * - <Descartes.Layer.WMS>
 * - <Descartes.Layer.Tiled>
 * - <Descartes.Layer.Vector>
 */
Descartes.LayerOverloaded = {
	id:null,
	
    initializeOld : Descartes.Layer.prototype.initialize,
	
	/**
	 * Constructeur: Descartes.Layer
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * title - {String} Intitul� de la couche.
	 * layersDefinition - {Array(<Descartes.ResourceLayer>)} D�finition des couches OWS constituant la couche.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * alwaysVisible -  {Boolean} Indique si la couche est toujours visible.
	 * visible -  {Boolean} Indique si la couche est visible.
	 * queryable - {Boolean} Indique si la couche est potientiellement interrogeable.
	 * activeToQuery - {Boolean} Indique si la couche est active pour les fonctionnalit�s d'interrogation.
	 * sheetable - {Boolean} Indique si les propri�tes de l'ensemble des objets de la couche peuvent �tre affich�es sous forme de tableau.
	 * opacity - {Integer} Opacit� initiale de la couche.
	 * opacityMax - {Integer} Opacit� maximale de la couche.
	 * legend - {Array(URL)} Adresses d'acc�s aux l�gendes.
	 * metadataURL - {URL} Adresse d'acc�s � des informations compl�mentaires.
	 * attribution - {String} Texte de copyright de la couche (�ventuellement lien vers une image).
	 * format - {String} Type MIME pour l'affichage des couches OpenLayers associ�es.
	 * displayOrder - {Integer} Ordre de superposition de la couche dans la carte.
	 */
	initialize: function(title, layersDefinition, options) {
		this.initializeOld.apply(this, arguments);
		
        if (this.id == null) {
            this.id = OpenLayers.Util.createUniqueID(this.CLASS_NAME + "_");
        }
	},
	
    _createOL_layersOld : Descartes.Layer.prototype._createOL_layers,
	
	/**
	 * Private
	 */
	_createOL_layers: function(layersDefinition) {
		this._createOL_layersOld.apply(this, arguments);
		
		for(var i=0; i<this.OL_layers.length; i++) {
			this.OL_layers[i].isDescartesLayer = true;
			var names = ['getUrl', 'getURL'];
			for (var j=0; j<names.length; j++){
        var oldFn = this.OL_layers[i][names[j]];
        if ( oldFn ){
        	this.OL_layers[i][names[j]] = function(){
        		var url = oldFn.apply(this, arguments);
        		var sep = (url.indexOf('?')==-1 ? '?' : '&');
        	  return url+sep+'time='+(new Date().getTime()); }
        }
			}
		}
	}
};
OpenLayers.Util.extend(Descartes.Layer.prototype, Descartes.LayerOverloaded);
Descartes.Layer.WMTS.prototype._createOL_layers = Descartes.Layer.prototype._createOL_layersOld;