/**
 * Class: Descartes.Group
 * Objet "métier" correspondant à un groupe d'éléments pour le contenu de la carte.
 * 
 * : Les éléments du groupe peuvent étre des instances de <Descartes.Group> ou <Descartes.Layer>.
 * 
 * : Aucune limite de profondeur "groupe / sous-groupe / sous-sous-groupe / ..." n'existe.
 */
Descartes.Group = OpenLayers.Class(Descartes.Group, {
  id:null,
  
  initializeOld : Descartes.Group.prototype.initialize,

  /**
   * Constructeur: Descartes.Group
   * Constructeur d'instances
   * 
   * Paramètres:
   * title - {String} Intitulé du groupe.
   * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
   * 
   * Options de construction propres à la classe:
   * opened - {Boolean} Indique si le groupe est déplié ou non dans la vue affichant l'arborescence du contenu de la carte.
   */
  initialize: function(title, options) {
    this.initializeOld.apply(this, arguments);
    
    if (this.id == null) {
      this.id = OpenLayers.Util.createUniqueID(this.CLASS_NAME + "_");
    }
  }
});
