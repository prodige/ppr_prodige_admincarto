/**
 * Class: Descartes.Button
 * Classe "abstraite" d�finissant un bouton pouvant �tre disponible ou non selon le contexte.
 * 
 * :
 * Le bouton doit �tre plac� dans une barre d'outils d�finie par la classe <Descartes.ToolBar>.
 * 
 * H�rite de:
 *  - <Descartes.I18N>
 *  - OpenLayers.Control.Button
 * 
 * Classes d�riv�es:
 *  - <Descartes.Button.CenterToCoordinates>
 *  - <Descartes.Button.ContentTask>
 *  - <Descartes.Button.DirectionalPan>
 *  - <Descartes.Button.ExportPDF>
 *  - <Descartes.Button.ExportPNG>
 *  - <Descartes.Button.ZoomToInitialExtent>
 *  - <Descartes.Button.ZoomToMaximalExtent>
 */
Descartes.ButtonOverloaded = {
    
    /**
	 * Methode: deactivate
	 * Surcharge le comportement de Decartes pour remettre celui d'OpenLayers.
     */
    deactivate: function() {
    	OpenLayers.Control.Button.prototype.deactivate.apply(this, arguments)
    },
    
	/**
	 * Methode: enable
	 * Rend le bouton disponible
	 */
	enable: function() {
		this.enabled = true;
		this.activate();
	},
	
	/**
	 * Methode: disable
	 * Rend le bouton indisponible
	 */
	disable: function() {
		this.enabled = false;
		this.deactivate();
	},
	
	/**
	 * Methode: trigger
	 * Déclenche l'exécution du traitement si le bouton est disponible.
	 */
	trigger: function() {
		if (this.enabled) {
			this.execute();
		}
	}
};
OpenLayers.Util.extend(Descartes.Button.prototype, Descartes.ButtonOverloaded);
