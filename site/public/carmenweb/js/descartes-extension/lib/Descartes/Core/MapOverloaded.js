/**
 * Class: Descartes.Map
 * Classe abstraite *principale* de la librairie Descartes permettant d'associer une carte OpenLayers à un ensemble d'outils, de gestionnaires, d'informations.
 * 
 * Classes dérivées:
 * - <Descartes.Map.ContinuousScalesMap>
 * - <Descartes.Map.DiscreteScalesMap>
 * 
 * Description générale:
 * Pour la librairie Descartes, une carte est constituée :
 * - d'un *contenu*, défini par la classe <Descartes.MapContent>.
 * - d'une *carte OpenLayers*, alimentée par des couches OpenLayers fournies par le contenu.
 * - de *contrôles* permettant de piloter la carte selon les principes propres à OpenLayers.
 * - d'*actions* permettant de piloter la carte suite à des saisies effectuées hors de la carte. Elles sont définies par des classes dérivées de la classe <Descartes.Action>.
 * - de *zones informatives* automatiquement actualisées selon le contexte courant de la carte. Elles sont définies par des classes dérivées de la classe <Descartes.Info>.
 * - d'*outils* permettant de piloter la carte suite à des interactions sur celle-ci. Ils sont définis par des classes dérivées des classe <Descartes.Button> et <Descartes.Tool>.
 * 
 * :
 * Le fonctionnement de la plupart des contrôles, actions, zones informatives et outils est relativement simple, tant pour l'utilisateur final que pour le développeur.
 * 
 * :
 * Il est en conséquence aisé de les associer à la carte, grâce à des méthodes génériques :
 * - <addOpenLayersNavigationControl> et <addOpenLayersNavigationControls> pour les contrôles
 * - <addAction> et <addActions> pour les actions
 * - <addInfo> et <addInfos> pour les zones informatives
 * - <addToolInToolBar> et <addToolsInToolBar> pour les outils
 * 
 * :
 * Des listes prédéfinies d'éléments disponibles pour ces méthodes facilitent leur utilisation.
 * 
 * :
 * D'autres éléments, dont la manipulation est plus complexe, bénéficient de méthodes dédiées pour les associer à la carte :
 * 
 * :
 * Barre d'outils - Instance de la classe <Descartes.ToolBar> ajout�e gr�ce � la m�thode <addToolBar>
 * Gestionnaire de contenu - Instance de la classe <Descartes.Action.MapContentManager> ajout� gr�ce � la m�thode <addContentManager>
 * Mini-carte de navigation - Instance de la classe <Descartes.Tool.MiniMap> ajout�e gr�ce � la m�thode <addMiniMap>
 * "Rose des vents" de navigation - Instance de la classe <Descartes.Button.DirectionalPanPanel> ajout�e gr�ce � la m�thode <addDirectionalPanPanel>
 * Gestionnaire d'info-bulles - Instance de la classe <Descartes.Info.ToolTip> ajout� gr�ce � la m�thode <addToolTip>
 * Gestionnaire de requ�tes attributaires - Instance de la classe <Descartes.ToolBar> ajout� gr�ce � la m�thode <addRequestManager>
 * Gestionnaire de localisation rapide - Instance de la classe <Descartes.Action.Gazetteer> ajout� gr�ce � la m�thode <addGazetteer>
 * Gestionnaire de localisation rapide administratif - Instance de la classe <Descartes.Action.DefaultGazetteer> ajout� gr�ce � la m�thode <addDefaultGazetteer>
 * Gestionnaire de vues personnalis�es - Instance de la classe <Descartes.Action.BookmarksManager> ajout� gr�ce � la m�thode <addBookmarksManager>
 * 
 * Contenu de la carte:
 * Il s'agit de couches �ventuellement hi�rarchis�es en groupes, sous-groupes, sous-sous-groupes, etc.
 * - une couche est d�finie par la classe <Descartes.Layer>
 * - un groupe est d�fini par la classe <Descartes.Group>
 * 
 * Contr�les OpenLayers disponibles (voir <OL_CONTROLS_NAME>) par d�faut:
 * - OpenLayers.Control.DragPan pour le type <Descartes.Map.OL_DRAG_PAN>
 * - OpenLayers.Control.ZoomBox pour le type <Descartes.Map.OL_ZOOM_BOX>
 * - OpenLayers.Control.CenterTo pour le type <Descartes.Map.OL_CENTER_TO>
 * - OpenLayers.Control.Navigation pour le type <Descartes.Map.OL_NAVIGATION>
 * - OpenLayers.Control.PanPanel pour le type <Descartes.Map.OL_PAN_PANEL>
 * - OpenLayers.Control.ZoomPanel pour le type <Descartes.Map.OL_ZOOM_PANEL>
 * - OpenLayers.Control.PanZoom pour le type <Descartes.Map.OL_PAN_ZOOM>
 * - OpenLayers.Control.PanZoomBar pour le type <Descartes.Map.OL_PAN_ZOOM_BAR>
 * 
 * Actions disponibles (voir <ACTIONS_NAME>) par d�faut:
 * - <Descartes.Action.CoordinatesInput> pour le type <Descartes.Map.COORDS_INPUT_ACTION>
 * - <Descartes.Action.ScaleChooser> pour le type <Descartes.Map.SCALE_CHOOSER_ACTION>
 * - <Descartes.Action.ScaleSelector> pour le type <Descartes.Map.SCALE_SELECTOR_ACTION>
 * - <Descartes.Action.SizeSelector> pour le type <Descartes.Map.SCALE_SELECTOR_ACTION>
 * - <Descartes.Action.PrinterParamsManager> pour le type <Descartes.Map.PRINTER_SETUP_ACTION>
 * 
 * Zones informatives disponibles (voir <INFOS_NAME>) par d�faut:
 * - <Descartes.Info.GraphicScale> pour le type <Descartes.Map.GRAPHIC_SCALE_INFO>
 * - <Descartes.Info.MetricScale> pour le type <Descartes.Map.METRIC_SCALE_INFO>
 * - <Descartes.Info.MapDimensions> pour le type <Descartes.Map.MAP_DIMENSIONS_INFO>
 * - <Descartes.Info.LocalizedMousePosition> pour le type <Descartes.Map.MOUSE_POSITION_INFO>
 * - <Descartes.Info.Legend> pour le type <Descartes.Map.LEGEND_INFO>
 * 
 * Outils disponibles (voir <TOOLS_NAME>) par d�faut:
 * - <Descartes.Button.ZoomToInitialExtent> pour le type <Descartes.Map.MAXIMAL_EXTENT>
 * - <Descartes.Button.ZoomToMaximalExtent> pour le type <Descartes.Map.INITIAL_EXTENT>
 * - <Descartes.Tool.DragPan> pour le type <Descartes.Map.DRAG_PAN>
 * - <Descartes.Tool.ZoomIn> pour le type <Descartes.Map.ZOOM_IN>
 * - <Descartes.Tool.ZoomOut> pour le type <Descartes.Map.ZOOM_OUT>
 * - <Descartes.Tool.NavigationHistory> pour le type <Descartes.Map.NAV_HISTORY>
 * - <Descartes.Tool.CenterMap> pour le type <Descartes.Map.CENTER_MAP>
 * - <Descartes.Button.CenterToCoordinates> pour le type <Descartes.Map.COORDS_CENTER>
 * - <Descartes.Tool.Measure.MeasureDistance> pour le type <Descartes.Map.DISTANCE_MEASURE>
 * - <Descartes.Tool.Measure.MeasureArea> pour le type <Descartes.Map.AREA_MEASURE>
 * - <Descartes.Button.ExportPDF> pour le type <Descartes.Map.PDF_EXPORT>
 * - <Descartes.Button.ExportPNG> pour le type <Descartes.Map.PNG_EXPORT>
 * - <Descartes.Tool.Selection.PointSelection> pour le type <Descartes.Map.POINT_SELECTION>
 * - <Descartes.Tool.Selection.PointRadiusSelection> pour le type <Descartes.Map.POINT_RADIUS_SELECTION>
 * - <Descartes.Tool.Selection.CircleSelection> pour le type <Descartes.Map.CIRCLE_SELECTION>
 * - <Descartes.Tool.Selection.PolygonSelection> pour le type <Descartes.Map.POLYGON_SELECTION>
 * - <Descartes.Tool.Selection.RectangleSelection> pour le type <Descartes.Map.RECTANGLE_SELECTION>
 * 
 * Ecouteurs mis en place:
 *  - l'�v�nement 'changed' de la classe <Descartes.MapContent> d�clenche la m�thode <refresh>.
 *  - l'�v�nement 'moveend' de la classe OpenLayers.Map d�clenche la m�thode <refreshContentManager>.
 */
Descartes.MapOverloaded = {
	
		TOOLS_WITH_MAPCONTENT : [
		"Descartes.Tool.Selection.PointSelection",
		"Descartes.Tool.Selection.PointRadiusSelection",
		"Descartes.Tool.Selection.PolygonSelection",
		"Descartes.Tool.Selection.CircleSelection",
		"Descartes.Tool.Selection.RectangleSelection",
		"Descartes.Button.ExportPNG",
		"Descartes.Button.ExportPDF",
		"Descartes.Info.Legend",
		"Descartes.Info.LegendExtJS",
		"Descartes.Info.LegendJQueryUI",
		"Descartes.Action.PrinterParamsManager",
		"Descartes.Button.Print",
		"Descartes.Action.LocalisationAdresse",
		"Descartes.Action.GazetteerPlugin"
	],
	
		/**
	 * Constructeur: Descartes.Map
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant la carte OpenLayers ou identifiant de cet �lement.
	 * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
	 * 
	 * Options de construction de la classe:
	 * initExtent - {OpenLayers.Bounds} Emprise initiale de visualisation de la carte.
	 * maxExtent - {OpenLayers.Bounds} Emprise maximale de visualisation de la carte.
	 * projection - {String} Code de la projection de la carte.
	 * units - {String} Unit� de mesure associ�e au syst�me de projection de la carte.
	 * size - {OpenLayers.Size} Taille initiale de la carte.
	 */
	initialize: function(div, mapContent, options) {
		
		this.defaultInfos = {
			GraphicScale: Descartes.Info.GraphicScale,
			MetricScale: Descartes.Info.MetricScale,
			MapDimensions: Descartes.Info.MapDimensions,
			LocalizedMousePosition: Descartes.Info.LocalizedMousePosition,
			Legend: Descartes.ViewManager.getView(Descartes.ViewManager.LEGEND),
			Attribution: Descartes.Info.Attribution
		};
		
		this.defaultActions = {
			CoordinatesInput: Descartes.Action.CoordinatesInput,
			ScaleChooser: Descartes.Action.ScaleChooser,
			ScaleSelector: Descartes.Action.ScaleSelector,
			SizeSelector: Descartes.Action.SizeSelector,
			PrinterParamsManager:Descartes.Action.PrinterParamsManager
		};
		
		this.defaultTools = {
			ZoomToInitialExtent: Descartes.Button.ZoomToInitialExtent,
			ZoomToMaximalExtent: Descartes.Button.ZoomToMaximalExtent,
			DragPan: Descartes.Tool.DragPan,
			ZoomIn: Descartes.Tool.ZoomIn,
			ZoomOut: Descartes.Tool.ZoomOut,
			NavigationHistory: Descartes.Tool.NavigationHistory,
			CenterMap: Descartes.Tool.CenterMap,
			CenterToCoordinates: Descartes.Button.CenterToCoordinates,
			MeasureDistance: Descartes.Tool.Measure.MeasureDistance,
			MeasureArea: Descartes.Tool.Measure.MeasureAreaGeoide,
			ExportPDF: Descartes.Button.ExportPDF,
			ExportPNG: Descartes.Button.ExportPNG,
			PointSelection: Descartes.Tool.Selection.PointSelection,
			PointRadiusSelection: Descartes.Tool.Selection.PointRadiusSelection,
			CircleSelection: Descartes.Tool.Selection.CircleSelection,
			PolygonSelection: Descartes.Tool.Selection.PolygonSelection,
			RectangleSelection: Descartes.Tool.Selection.RectangleSelection,
			PrintCarmen: Descartes.Button.Print
		};
		
		this.defaultOlNavigationControls = {
			DragPan: OpenLayers.Control.DragPan,
			ZoomBox: OpenLayers.Control.ZoomBox,
			CenterTo: OpenLayers.Control.CenterTo,
			Navigation: OpenLayers.Control.Navigation,
			PanPanel: OpenLayers.Control.PanPanel,
			ZoomPanel: OpenLayers.Control.ZoomPanel,
			PanZoom: OpenLayers.Control.PanZoom,
			PanZoomBar: OpenLayers.Control.PanZoomBar
		};
	
		this.mapContent = mapContent;
		
		if (options !== undefined) {
			OpenLayers.Util.extend(this, options);
		}
		// Modif SWT [P2-13] 6185 Améliorer le support des couches WMS-C
		// La ligne suivante n'est pas expliquée/commentée :
		//
		// this.maxExtent = Descartes.Utils.extendBounds(this.maxExtent, this.size);
		//
		// Je suppose que l'idée c'etait de trouver une valeur correcte pour
		// initExtent :
		//
		// this.initExtent = (this.initExtent !== null) ? this.initExtent : this.maxExtent;
		//
		// Le problème, c'est que dans le cas de WMS-C (voir Application.js)
		// on veut que le maxExtent soit aligné sur la grille WMS.
		// Ce changement de maxExtent est donc problématique !
		// J'ajoute donc un contournement qui va éviter de changer l'implémentation
		// dans le cas général (je ne connais pas bien l'impact du changement) et
		// qui permet, dans le cas de WMS-C, de ne pas toucher à maxExtent.
		var tempExtent = this.maxExtent;
		if (this.fixedMaxExtent !== true && this.maxExtent) {
			tempExtent = Descartes.Utils.extendBounds(this.maxExtent, this.size);
		}
		this.initExtent = (this.initExtent !== null) ? this.initExtent : tempExtent;
		
		var oldMaxExtent = this.maxExtent;
		if ( oldMaxExtent === null ){
			this.maxExtent = this.initExtent;
		}
		this.createOlMap();
    if ( oldMaxExtent === null ){
      this.maxExtent = oldMaxExtent;
    }
    //this.OL_map.setOptions({maxExtent: this.maxExtent});
    //this.OL_map.setOptions({restrictedExtent: this.restrictedExtent});
		this.OL_map.render(div);

		this.mapContent.events.register( 'changed', this, this.refresh);
		this.OL_map.events.register( 'zoomend', this, this.refreshContentManager);
	},
	
	/**
	 * Methode: addDirectionalPanPanel
	 * Ajoute une une "rose des vents" de navigation <Descartes.Button.DirectionalPanPanel> � la carte.
	 * 
	 * Retour:
	 * {<Descartes.Button.DirectionalPanPanel>} La "rose des vents" cr��e.
	 */
	addDirectionalPanPanel : function() {
		var panel = new Descartes.Button.DirectionalPanPanelExtension();
		this.OL_map.addControl(panel);
		return panel;
	},
	
	/**
	 * Methode: addToolInToolBar
	 * Ajoute un outil (<Descartes.Button> ou <Descartes.Tool>) à la barre d'outils principale.
     * 
     * Paramètres:
     * tool - {Object} Objet JSON décrivant l'outil.
     * 
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,			// parmi les valeurs de Descartes.Map.TOOLS_NAME
     * 											// ou classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 
     * 		args: <arguments>					// arguments du constructeur de l'outil
     * }
     * (end)
	 */
	addToolInToolBar: function(tool) {
		var tools = [];
		var toolInstance = null;
		var toolClassName = null;
		if (typeof tool.type === "string") {
			if (Descartes.Map.TOOLS_NAME.indexOf(tool.type) !== -1) {
				toolClassName = this.defaultTools[tool.type];
			}
		} else if (typeof tool.type === "function"){
			toolClassName = tool.type;
		}
		if (toolClassName !== null) {
			// ************************************************** //
			// **********BUG INSTANCIATION TOOL****************** //
			// ************************************************** //
			toolInstance = new toolClassName(OpenLayers.Class.isPrototype);
			var args;
			if (tool.args !== null){
				if ( !(tool.args instanceof Array)) {
					args = [tool.args];
				} else {
					args = tool.args;
				}
			} else {
				args = [];
			}
			toolClassName.prototype.initialize.apply(toolInstance, args);
		}
		if (toolInstance !== null) {
			//Si setMapContent définie alors on l'appelle
			if (toolInstance.setMapContent !== undefined) {
				toolInstance.setMapContent(this.mapContent);
			}
					
			if (toolInstance.CLASS_NAME === "Descartes.Tool.NavigationHistory") {
				var navigationHistoryTool = toolInstance;
				this.OL_map.addControl(navigationHistoryTool.globalControl);	
				tools.push(navigationHistoryTool.nextControl);
				tools.push(navigationHistoryTool.previousControl);
			} else {
				tools.push(toolInstance);
			}
			
			this.mainToolBar.addControls(tools);
		}
		return toolInstance;
	},
	
	
	/**
	 * Methode: addAction
	 * Ajoute une action <Descartes.Action> & la carte.
     * 
     * Paramètres:
     * action - {Object} Objet JSON décrivant l'action.
     * 
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'action>,			// parmi les valeurs de Descartes.Map.ACTIONS_NAME
     * 											// ou classe personnalisée remplaçant la classe par défaut associée au type de l'action
     * 
     * 		div: <zone d'affichage de la vue>,	// élément DOM de la page accueillant la vue associée ou identifiant de cet élément.
     * 		options: <options>					// options du constructeur de l'action
     * }
     * (end)
	 */
	addAction: function(action) {
		var actionClassName = null;
		if (typeof action.type === "string") {
			if (Descartes.Map.ACTIONS_NAME.indexOf(action.type) !== -1) {
				actionClassName = this.defaultActions[action.type];
			}
		} else if (typeof action.type === "function"){
			actionClassName = action.type;
		}
		if (actionClassName !== null) {
			var actionInstance;
			if (action.options !== null){
				actionInstance = new actionClassName(action.div, this.OL_map, action.options);
			} else {
				actionInstance = new actionClassName(action.div, this.OL_map);
			}
			//Si setMapContent définie alors on l'appelle
			if (actionInstance.setMapContent !== undefined) {
				actionInstance.setMapContent(this.mapContent);
			}
			if (actionInstance.CLASS_NAME === "Descartes.Action.SizeSelector") {
				this.sizeSelector = actionInstance;
			}
			return actionInstance;
		}
		return null;
	},
	
	/**
	 * Methode: addContentManager
	 * Ajoute un gestionnaire de contenu <Descartes.Action.MapContentManager> � la carte.
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant la barre d'outils sp�cifique pour les outils du gestionnaire de contenu ou identifiant de cet �lement.
	 * toolsType - {Array(Object)} Liste des types d'outils � inclure dans le gestionnaire. Voir la m�thode <Descartes.Action.MapContentManager.addTool>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de la m�thode:
	 * toolBarDiv - {DOMElement|String} El�ment DOM de la page accueillant l'arborescence associ�e au gestionnaire ou identifiant de cet �lement.
	 * Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Action.MapContentManager.Descartes.Action.MapContentManager>.
	 * 
	 * Retour:
	 * {<Descartes.Action.MapContentManager>} Le gestionnaire de contenu cr��.
	 */
	addContentManager: function(div, toolsType, options) {
		this.mapContentManager = new Descartes.Action.MapContentManager(div, this.mapContent, options);

		if(this.mapContent.editable && toolsType !== undefined && toolsType !== null){
			var singleToolBar = true;
			if (options !== undefined && options.toolBarDiv !== undefined) {
				singleToolBar = false;
				var rendererClass = Descartes.ViewManager.getView(Descartes.ViewManager.TOOL_BAR);
				if (options.toolBarTitle !== undefined && options.toolBarTitle !== null) {
					this.contentManagerToolBar = new rendererClass(options.toolBarDiv, this.OL_map, {title: options.toolBarTitle});
				} else {
					this.contentManagerToolBar = new rendererClass(options.toolBarDiv, this.OL_map);
				}
				//this.contentManagerToolBar = new rendererClass(Descartes.Utils.getDiv(options.toolBarDiv), this.OL_map, {title:options.toolBarTitle});
			}
			if ( !(toolsType instanceof Array)) {
				toolsType = [toolsType];
			}
			var contentTools = [];
			for (var i=0, len=toolsType.length ; i<len ; i++) {
				this.mapContentManager.addTool(toolsType[i]);
			}
			
			if (this.mapContentManager.getTools().length !== 0) {
				if (singleToolBar === true) {
					if (this.mainToolBar !== null) {
						this.mainToolBar.addControls(this.mapContentManager.getTools());
					} else {
						throw "Aucune barre d'outils n'est d�finie : les outils de gestion de contenu ne peuvent �tre ajout�s.";
					}
				} else {
					this.contentManagerToolBar.addControls(this.mapContentManager.getTools());
				}
			}
			this.mapContentManager.activeInitialTools();

		}
		return this.mapContentManager;
	},
	
	addLocalizedMousePosition : function(div, options) {
		var typeClass = Descartes.ViewManager.getView(Descartes.ViewManager.LOCALIZED_MOUSE_POSITION);
		this.addInfo({type : typeClass, div : div, options : options});
	},
	
	/**
	 * Methode: addToolTip
	 * Ajoute un gestionnaire d'info-bulles <Descartes.Info.ToolTip> � la carte.
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant l'info-bulles ou identifiant de cet �lement.
	 * toolTipLayers - {Array(Object)} Tableau d'objets JSON repr�sentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
	 * options - {Object} Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Info.ToolTip.Descartes.Info.ToolTip>.
	 * 
	 * Retour:
	 * {<Descartes.Info.ToolTip>} Le gestionnaire d'info-bulles cr��.
	 */
	addToolTip: function(div, toolTipLayers, options) {
		if (!this.hasOlNavigationControls) {
			var typeClass = Descartes.ViewManager.getView(Descartes.ViewManager.TOOLTIP);
			this.toolTip = new typeClass(div, this.OL_map, toolTipLayers, options);
			if (this.mapContent !== null) {
				this.mapContent.events.register( 'layerRemoved', this, this.refreshToolTip);
				this.mapContent.events.register( 'toolTipsChanged', this, this.refreshAfterChange);
			}
			this.OL_map.addControl(this.toolTip);
			return this.toolTip;
		} else {
			throw "Descartes.Info.ToolTip ne peut �tre ajout� � la carte qu'AVANT les contr�les de navigation OpenLayers";
		}
	},
	
		/**
	 * Methode: refreshAfterChange
	 * Tri la liste des des layers des tool tip apr�s un changement d'ordre dans la map
	 * 
	 */
	refreshAfterChange: function() {
		if (this.toolTip !== null) {
			
			var mapLayers = this.mapContent.getLayers();
			var toolTips = this.toolTip.toolTipLayers;
			// tableau des layers tri�e 
			var toolTipLayersSorted = [];
			var sindex = 0;
			
			// pour chaque layer de la map
			for (var iMapLayer=0, lenMapLayers=mapLayers.length; iMapLayer<lenMapLayers ; iMapLayer++) {
				var mapLayer = mapLayers[iMapLayer];

				// on cherche le laye tooltip �quivalent
				for (var iToolTip=0, lenToolTips=toolTips.length; iToolTip<lenToolTips ; iToolTip++) {
					
					var toolTipLayer = toolTips[iToolTip].layer;
					if (toolTipLayer === mapLayer) {
						// et on l'injecte dans la nouvelle liste au bon indice
						toolTipLayersSorted[sindex] = toolTips[iToolTip];
						sindex++;
						break;
					}
				}
			}
		// maj des tooltips avec le tableau tri�
		this.toolTip.toolTipLayers = toolTipLayersSorted;
		
		}
	},
    
    /**
     * Private
     */
    _removeAllLayers: function() {
        var len = this.OL_map.layers.length;
        for (var i=(len-1) ; i>=0 ; i--) {
        	
        	//Ajout de isDescartesLayer pour ne pas supprimer les layers uniquement déclarées dans OpenLayers
        	
            if (!this.OL_map.layers[i].isBaseLayer && this.OL_map.layers[i].isDescartesLayer === true) {
                this.OL_map.removeLayer(this.OL_map.layers[i]);
            }
        }
    }
};

	
OpenLayers.Util.extend(Descartes.Map.prototype, Descartes.MapOverloaded);




/**
 * Constante: PRINT
 * Code de l'outil pour imprimer la carte courante (impression carmen).
 */
Descartes.Map.PRINT = "PrintCarmen";
Descartes.Map.TOOLS_NAME.push(Descartes.Map.PRINT);

