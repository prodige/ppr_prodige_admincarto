/**
 * Class: Descartes.Tool
 * Classe "abstraite" d�finissant un bouton d'interaction avec la carte.
 * 
 * :
 * Le bouton doit �tre plac� dans une barre d'outils d�finie par la classe <Descartes.ToolBar>.
 * 
 * Un seul bouton de ce type peut �tre accessible � chaque instant parmi l'ensemble des boutons d'interaction de la barre d'outils.
 * 
 * H�rite de:
 *  - <Descartes.I18N>
 *  - OpenLayers.Control
 * 
 * Classes d�riv�es:
 *  - <Descartes.Tool.CenterMap>
 *  - <Descartes.Tool.DragPan>
 *  - <Descartes.Tool.Selection>
 *  - <Descartes.Tool.ZoomIn>
 *  - <Descartes.Tool.ZoomOut>
 *  - <Descartes.Tool.Measure>
 */
Descartes.ToolOverloaded = {
	
	/**
	 * Methode: activate
	 * Active le bouton et d�sactive les autres boutons d'interaction de la barre d'outils.
	 */
	activate: function() {
		//Ne rien faire
	}
};
OpenLayers.Util.extend(Descartes.Tool.prototype, Descartes.ToolOverloaded);
