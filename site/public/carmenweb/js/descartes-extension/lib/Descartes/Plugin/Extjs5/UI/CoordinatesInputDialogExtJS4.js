/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/** 
 * Class: Descartes.UI.CoordinatesInputDialogExtJS4
 * Classe proposant, sous forme de boite de dialogue, la saisie d'un couple de coordonn�es pour recentrer la carte.
 * 
 * H�rite de:
 *  - <Descartes.UI>
 * 
 * 
 * Ev�nements d�clench�s:
 * recentrage - Les coordonn�es sont saisies et valides.
 * 
 * Ecouteurs mis en place:
 *  - l'�v�nement 'done' de la classe <Descartes.ModalDialog> d�clenche la m�thode <sendDatas>.
 *  - l'�v�nement 'cancelled' de la classe <Descartes.ModalDialog> d�clenche la m�thode <deleteDialog>.
 */
 Descartes.UI.CoordinatesInputDialogExtJS4 = OpenLayers.Class(Descartes.UI, {

    EVENT_TYPES: ["recentrage"],
    
	/**
	 * Propriete: msgTarget
	 * {String} Mode d'affichage des messages d'erreurs.
	 */
    msgTarget:'side',

    /**
	 * Constructeur: Descartes.UI.CoordinatesInputDialogExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - null.
	 * model - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.CoordinatesInput>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * msgTarget - {String} Mode d'affichage des messages d'erreurs.	 
	 */
	initialize: function(div, model, options) {
		Descartes.UI.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
	},
	
	/**
	 * Methode: draw
	 * Ouvre la boite de dialogue modale pour la saisie des coordonn�es.
	 */
	draw: function() {
		var self = this;
		
		this.formPanel =  new Ext.FormPanel({
	        xtype        : 'form',
	        autoScroll   : true,
	        id           : 'dialog-form',
	        frame        : true,
	        pollForChanges: true,
	        items        : [
	            {
	                fieldLabel : this.getMessage("INVITE1_MESSAGE"),
			        xtype      : 'numberfield',
	                id         : 'xdialog',
	                minValue   : 0,
	                allowBlank : false,
	                msgTarget  : this.msgTarget,
            		blankText  : this.getMessage("X_ERROR"),
            		nanText    : this.getMessage("X_ERROR"),
            		minText    : this.getMessage("X_ERROR")
	            },
	            {
	                fieldLabel : this.getMessage("INVITE2_MESSAGE"),
			        xtype      : 'numberfield',
	                id         : 'ydialog',
	                minValue   : 0,
	                allowBlank : false,
	                msgTarget  : this.msgTarget,
            		blankText  : this.getMessage("Y_ERROR"),
            		nanText    : this.getMessage("Y_ERROR"),
            		minText    : this.getMessage("Y_ERROR")
	            }
	        ],
		    buttons     : [{
		        text     : this.getMessage("OK_BUTTON"),
		        formBind : true,
		        handler  : function() {
	        		self.done();	        		
		        }
		    },{
	        	text     : this.getMessage("CANCEL_BUTTON"),
	        	handler  : function(){Ext.getCmp('popup').close();}
	        }],
	        buttonAlign : 'center'
	    });
	
		var w = new Ext.Window({
            id          : 'popup',
            modal       : true,
            items       : [this.formPanel],
            title        : this.getMessage("DIALOG_TITLE"),
            closable    : false,
            renderTo    : document.body,
            width: 360
        });
		
		w.show();
	},
	
	/**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'recentrage'.
     */
	done: function() {
		this.model.x = this.formPanel.getForm().findField('xdialog').getValue();
		this.model.y = this.formPanel.getForm().findField('ydialog').getValue();
		this.events.triggerEvent("recentrage");
		Ext.getCmp('popup').close();
	},
	
	CLASS_NAME: "Descartes.UI.CoordinatesInputDialogExtJS4",

	VERSION : "3.2"
});
