/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.Button.ContentTask.ChooseWmsLayersExtJS4
 * Classe d�finissant un bouton permettant l'ajout de couches dans le contenu d'une carte, apr�s d�couverte des capacit�s d'un serveur WMS.
 * 
 * H�rite de:
 *  - <Descartes.Button.ContentTask>
 */
Descartes.Button.ContentTask.ChooseWmsLayersExtJS4 = OpenLayers.Class(Descartes.Button.ContentTask, {
	
	/**
	 * Propriete: layers
	 * {Array(Object)} Liste des couches d�couvertes disponibles dans la projection de la carte (selon le format d�fini par la classe OpenLayers.Format.WMSCapabilities).
	 */
	layers: null,
	
	/**
	 * Propriete: wmsVersion
	 * {String} Version du serveur WMS.
	 */
	wmsVersion:null,
	
	/**
	 * Constructeur: Descartes.Button.ContentTask.ChooseWmsLayersExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
     */
	initialize: function(options) {
		Descartes.Button.ContentTask.prototype.initialize.apply(this,[options]);
	},

	/**
	 * Methode: execute
	 * Lance l'interrogation GetCapabilities d'un serveur WMS.
	 */
	execute: function() {
	
    	var self = this;
		Ext.MessageBox.prompt('', this.getMessage("PROMPT_URL_SERVER"),
				function() {
					var args = arguments;
					//if (args.length >= 3 && args[2].length >=2 && args[2][0] === "ok" && args[2][1].length !==0) {
                    //    self._throwRequest(args[2][1]);
                    //}
                    if (args.length >= 3 && args[0] === "ok" && args[1].length !==0) {
                        self._throwRequest(args[1]);
                    }
				}
		);
    },
	
	_throwRequest: function(urlServer) {
    	var self = this;
		var url = new Descartes.Url(urlServer);
		url.alterOrAddParam("REQUEST", "GetCapabilities");
		url.alterOrAddParam("SERVICE", "WMS");

		var urlProxy = new Descartes.Url(Descartes.PROXY_SERVER);
		var urlProxyString = urlProxy.getUrlString();
		if (urlProxyString.indexOf("?") === -1){
			urlProxyString += "?";
		}
		Ext.Msg.wait('Interrogation en cours, merci de patienter...','', {animate:true});
		var capabilitiesRequest = OpenLayers.Request.GET({
			url: urlProxyString + url.getUrlString(),
			success: function(response) {self.showLayers(response);},
			failure: function(response) {Ext.Msg.hide(); alert(Descartes_GET_CAPABILITIES_FAILURE);}
		});
	},
    
    /**
     * Methode: showLayers
     * Affiche la liste des couches WMS d�couvertes et propose la s�lection multiple des couches � ajouter � la carte.
     * 
     * Param�tres:
     * ajaxResponse - {Object} R�ponse AJAX � l'appel GetCapabilities.
     */
    showLayers: function(ajaxResponse) {
		Ext.Msg.hide();
		var wmsParser = new OpenLayers.Format.WMSCapabilities();

		//IE parser ActiveXObject:
		//si utilisation de responseText, il faut que l encodage soit UTF-8 
		//pour que responseXML ne soit pas null, le content-type doit etre text/xml
		
		//var wmsCapabilities = wmsParser.read(ajaxResponse.responseText);
		var data = ajaxResponse.responseText;
		if(ajaxResponse.responseXML !== null && ajaxResponse.responseXML.documentElement !== null){
			 data = ajaxResponse.responseXML;
		}	
		var wmsCapabilities = wmsParser.read(data);
		
		if(wmsCapabilities.version !== undefined && wmsCapabilities.version !== null){
			this.wmsVersion = wmsCapabilities.version;
		}
		
		this.layers = [];
		if(wmsCapabilities.capability!==undefined && wmsCapabilities.capability !== null){
			for(var iLayers=0, lenLayers=wmsCapabilities.capability.layers.length; iLayers<lenLayers; ++iLayers) {
				this.layers.push(new Descartes.WmsLayer(wmsCapabilities.capability.layers[iLayers], wmsCapabilities.capability.request.getmap.href)); 
				//this.layers.push(new Descartes.WmsLayer(wmsCapabilities.capability.layers[iLayers], urlServer));               
			}
				
			var mapProjection = this.map.projection;
			this.layers = this.layers.filter(function(element) {return (element.isValid(mapProjection));});
			if (this.layers.length !== 0) {
				var self = this;
				this.elements = [];
				var data = [];
				// ExtJS ne cr�e qu'une seule combobox pour tout le tableau 
				//(ceci emp�che donc d'afficher compl�tement la combobox lorsque les champs ne sont pas en �dition) 
				/*var combo = new Ext.form.ComboBox({
					   xtype:        "combo",
					   id:'combo',
					   typeAhead:    true,
		               triggerAction : 'all',
					   //autoSelect    : true,
		               //lazyRender    : true,
		              //mode          : 'local',
					   queryMode: 'local',
					   editable      : false,
					   store         : new Ext.data.SimpleStore({
						   			   		fields: ['id', 'displayValue']
									   }
					   ),
					   displayField  : 'displayValue',
					   valueField    : 'id'
		            });*/
				
				// Politique de s�l�ction de ligne (S�lection par checkbox)
				//var checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel({
				var checkboxSelectionModel = new Ext.selection.CheckboxModel({
					 //singleSelect : false,
	                mode:'MULTI',
			        sortable     : false,
			        checkOnly    : true
			    });
				
				// Cr�ation de la structure des colonnes du tableau ainsi que leurs propri�t�s associ�es
				//var columnModel = new Ext.grid.ColumnModel([
				var columnModel = [
				    {dataIndex : 'idChooseWmsLayer', hideable  : true},
				    //checkboxSelectionModel,
				    {dataIndex : 'checkbox', hideable  : true},
				    {header : self.getMessage("TITLE_COL_NAME"),dataIndex : self.getMessage("TITLE_COL_NAME"),sortable : true,width:250,autoScroll: true},
				    //{header : self.getMessage("FORMAT_COL_NAME"), dataIndex : self.getMessage("FORMAT_COL_NAME"),sortable : true, editor : combo},
				    {header : self.getMessage("FORMAT_COL_NAME"),dataIndex : self.getMessage("FORMAT_COL_NAME"),width:80, editor : {
				         xtype:        "combo",
				         id:'combo',
				         typeAhead:    true,
				         triggerAction:"all",
				         editable      : false,
				         listConfig:{cls: "x-combo-list-small"},
				         store:[]
				      }},
				    
				    {header : self.getMessage("MINSCALE_COL_NAME"), dataIndex : self.getMessage("MINSCALE_COL_NAME"), sortable : true,width:80},
				    {header : self.getMessage("MAXSCALE_COL_NAME"), dataIndex : self.getMessage("MAXSCALE_COL_NAME"), sortable : true,width:80},
				    {header : self.getMessage("ABSTRACT_COL_NAME"), dataIndex : self.getMessage("ABSTRACT_COL_NAME"), sortable : true,width:200,autoScroll: true},
				    {header : self.getMessage("STYLE_COL_NAME"),dataIndex : self.getMessage("STYLE_COL_NAME"),width:80, editor : {
				         xtype:        "combo",
				         id:'comboStyle',
				         typeAhead:    true,
				         triggerAction:"all",
				         editable      : false,
				         listConfig:{cls: "x-combo-list-small"},
				         store:[]
				      }},
				    {header : self.getMessage("KEYWORDS_COL_NAME"), dataIndex : self.getMessage("KEYWORDS_COL_NAME"), sortable : true,width:100},
				    {header : self.getMessage("METADATA_COL_NAME"), dataIndex : self.getMessage("METADATA_COL_NAME"), sortable : true, width:100,
				    	renderer : function(value, p, record){return '<a href=\''+value+'\' target=\'_blank\'>'+value+'</a>'} // Affiche la valeur sous forme d'un lien
				    }
			    ];
				//]);
				
				// Stockage des donn�es � afficher au format ExtJS
				for (var i=0, len=this.layers.length ; i<len; i++) {
					
					var style = "";
					if(this.layers[i].styles.length !==0){
						style = this.layers[i].styles[0].title;
					}
					
					data.push([
					            i,
					            false,
					           	this.layers[i].title,
					           	this.layers[i].formats[0],
					        	//this.layers[i].formats,
					           	(this.layers[i].minScale !== 0) ? Descartes.Utils.readableScale(this.layers[i].minScale) : "-",
					           	(this.layers[i].maxScale !== 0) ? Descartes.Utils.readableScale(this.layers[i].maxScale) : "-",
			           			this.layers[i].description,
			           			style,
			           			this.layers[i].keywordList,
			           			this.layers[i].metadataURL
					          ]);
				}
				
				// Cr�ation du model de donn�es � afficher (Titre des colonnes et contenu)
			    var store = new Ext.data.ArrayStore({
			        fields : [
			                    'idChooseWmsLayer', // Colonne stockant l'index de la couche dans le mod�le
			                    'checkedChooseWmsLayer', // Permet la s�lection d'une seul ligne et/ou toutes les lignes du tableau
			                  	self.getMessage("TITLE_COL_NAME"),
			                  	self.getMessage("FORMAT_COL_NAME"),
			                  	self.getMessage("MINSCALE_COL_NAME"),
			                  	self.getMessage("MAXSCALE_COL_NAME"),
			                  	self.getMessage("ABSTRACT_COL_NAME"),
			                  	self.getMessage("STYLE_COL_NAME"),
			                  	self.getMessage("KEYWORDS_COL_NAME"),
			                  	self.getMessage("METADATA_COL_NAME")
			                 ], 
			        data   : data
			    });
			    
			    // Cr�ation du div contenant le tableau affichant les couches disponibles
			    var container = document.createElement("div");
				container.id = 'containerChooseWmsLayers';
				document.body.appendChild(container);
				
				// Cr�ation du tableau affichant les couches disponibles
			    //var grid = new Ext.grid.EditorGridPanel({
				var grid = new Ext.grid.GridPanel({
			    	id           : 'gridChooseWmsLayersDescartes',
			    	viewConfig   : { //Fait en sorte que la somme des largeurs de colonnes soit �gale � la taille de la grille
			        	forceFit : true,
			        	autoFill : true,
			        	stripeRows   : true
			        },
			        renderTo     : 'containerChooseWmsLayers',
					height      : getWindowHeight()/2,
					width       : getWindowWidth()/2,
			        frame        : true,
			        clicksToEdit : 1,
			        //stripeRows   : true,
				    enableHdMenu : false,
				    autoScroll   : true,
			        //cm           : columnModel,
				    columns           : columnModel,
				    //sm           : checkboxSelectionModel, // Politique de s�l�ction de ligne (S�lection par checkbox)
				    selModel           : checkboxSelectionModel, // Politique de s�l�ction de ligne (S�lection par checkbox)
				    store        : store,
				   // forceFit: true,
			        plugins: [
	                          Ext.create('Ext.grid.plugin.CellEditing', {
	                        	  	clicksToMoveEditor: 1,
	                                autoCancel: false
	                          })
	                          ],
					listeners    : {
						beforeedit : function(e){
							var dataComboLayer = [];
							if(e.field === self.getMessage("FORMAT_COL_NAME")){
								for (var iFormat=0, lenFormats=self.layers[e.record.data.idChooseWmsLayer].formats.length ; iFormat<lenFormats ; iFormat++) {
									var format = self.layers[e.record.data.idChooseWmsLayer].formats[iFormat];
									dataComboLayer.push([format, format]);
								}
								
								//combo.store.loadData(dataComboLayer);
								Ext.getCmp('combo').store.loadData(dataComboLayer);
							}
							
							if(e.field === self.getMessage("STYLE_COL_NAME")){
								for (var iStyle=0, lenStyles=self.layers[e.record.data.idChooseWmsLayer].styles.length ; iStyle<lenStyles ; iStyle++) {
									var style = self.layers[e.record.data.idChooseWmsLayer].styles[iStyle];
									dataComboLayer.push([style.title, style.name]);
								}
								
								//combo.store.loadData(dataComboLayer);
								Ext.getCmp('comboStyle').store.loadData(dataComboLayer);
							}
	
							
						}
					}
				});
			    
				//grid.getColumnModel().setHidden(0, true);
	            grid.headerCt.getComponent(1).hide();
	            grid.headerCt.getComponent(2).hide();
			    			
				this.elements.push(container);
				
				Descartes.Button.ContentTask.prototype.execute.call(this);
				
				Ext.getCmp('gridChooseWmsLayersDescartes').getView().refresh(true);
			} else {
				alert(Descartes_GET_CAPABILITIES_NO_LAYERS);
			}
		}else{
			alert(Descartes_GET_CAPABILITIES_FAILURE);
		}
    },
    
    /**
     * Methode: sendDatas
     * Envoie les couches s�lectionn�es pour ajout et leurs propri�t�s au gestionnaire de contenu.
     */
    sendDatas: function() {
    	this.datas = [];
    	//var selectionArray = Ext.getCmp('gridChooseWmsLayersDescartes').getSelectionModel().getSelections();
    	var selectionArray = Ext.getCmp('gridChooseWmsLayersDescartes').getSelectionModel().getSelection();
    	
    	for (var i=0, len=selectionArray.length ; i<len ; i++) {
    		var dataLayerSelected = selectionArray[i].data;
    		
    		if(this.wmsVersion!==undefined && this.wmsVersion!==null){
				this.layers[dataLayerSelected.idChooseWmsLayer].serverVersion=this.wmsVersion;
			}
    		
			this.layers[dataLayerSelected.idChooseWmsLayer].selectedFormat = dataLayerSelected.Format;
			
			if(dataLayerSelected.Style !== ""){
				var styleName = this._getStyleName(this.layers[dataLayerSelected.idChooseWmsLayer].styles,dataLayerSelected.Style);
				this.layers[dataLayerSelected.idChooseWmsLayer].selectedStyle = styleName;
				//this.layers[dataLayerSelected.idChooseWmsLayer].selectedStyle = dataLayerSelected.Style;
			}
			
			this.datas.push(this.layers[dataLayerSelected.idChooseWmsLayer].toJSON());
    	}
    	this.deleteDialog();
    	if (this.datas.length !== 0) {
   			this.events.triggerEvent("done");
    	}
    },
    
    /*
	* private
	*/
    _getStyleName: function (styles,selectedStyle){
    	var styleName="";
    	for (var i=0, len=styles.length ; i<len ; i++) {
			if(styles[i].title === selectedStyle){
				styleName = styles[i].name;
				break;
			}
    	}
    	return styleName;
    },
    
	CLASS_NAME: "Descartes.Button.ContentTask.ChooseWmsLayersExtJS4",

	VERSION : "3.2"
});
