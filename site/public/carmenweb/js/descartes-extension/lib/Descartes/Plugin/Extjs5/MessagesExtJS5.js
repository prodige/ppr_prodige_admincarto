/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
   Luc Boyer
   Damien Despres
   David Marquet
   Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

Descartes_Messages_UI_CoordinatesInputDialogExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_CoordinatesInputDialog, {
                                        X_ERROR : 'La valeur de X doit être un nombre positif',
                                        Y_ERROR : 'La valeur de Y doit être un nombre positif'
                                      });
Descartes_Messages_UI_CoordinatesInputInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_CoordinatesInputInPlace, {
                                        X_ERROR : 'La valeur de X doit être un nombre positif',
                                        Y_ERROR : 'La valeur de Y doit être un nombre positif'
                                      });
Descartes_Messages_UI_TabbedDataGridsExtJS5 = {
    DIALOG_TITLE : "Résultats de la recherche",
    NO_RESULT : "Aucun résultat"
  };
Descartes_Messages_UI_DataGridExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_DataGrid, {});
Descartes_Messages_UI_LayersTreeExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_LayersTree, {});
Descartes_Messages_UI_PrinterSetupInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_PrinterSetupInPlace, {
                                        PAPER_INPUT_ERROR : 'Sélectionnez un format de feuille'
                                      });
Descartes_Messages_UI_PrinterSetupDialogExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_PrinterSetupDialog, {
                                        PAPER_INPUT_ERROR : 'Sélectionnez un format de feuille'
                                      });
Descartes_Messages_UI_ScaleChooserInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_ScaleChooserInPlace, {});
Descartes_Messages_UI_ScaleSelectorInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_ScaleSelectorInPlace, {});

Descartes_Messages_Info_LegendExtJS5 = OpenLayers.Util.extend(Descartes_Messages_Info_Legend, {});
Descartes_Messages_UI_BookmarksInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_BookmarksInPlace, {});
Descartes_Messages_UI_RequestManagerInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_RequestManagerInPlace, {});
Descartes_Messages_UI_GazetteerInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_GazetteerInPlace, {});
Descartes_Messages_UI_ToolTipContainerExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_ToolTipContainer, {});
Descartes_Messages_Button_ContentTask_ChooseWmsLayersExtJS5 = OpenLayers.Util.extend(Descartes_Messages_Button_ContentTask_ChooseWmsLayers, {});
Descartes_Messages_UI_SizeSelectorInPlaceExtJS5 = OpenLayers.Util.extend(Descartes_Messages_UI_SizeSelectorInPlace, {});
Descartes_Messages_UI_InformationInPlaceExtJS5 = {
	DIALOG_TITLE : "Information",
	RESULTAT : 'résultat',
	RESULTATS : 'résultats',
	CLOSE_BUTTON : "Fermer",
	INTERROGATION : 'Interrogation des couches...',
	BTN_EXPORTER : "Exporter",
	BTN_IMPRIMER : "Imprimer",
	LBL_LIBELLE : "Libellé",
	LBL_DONNEE : "Donnée"
	
};