/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
   Luc Boyer
   Damien Despres
   David Marquet
   Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.AbstractInPlaceExtJS5
 * Classe "abstraite" d�finissant les options des panels ExtJS5.
 * 
 * 
 * H�rite de:
 *  - <Descartes.UI>
 * 
 * Classes d�riv�es:
 *  - <Descartes.UI.BookmarksInPlaceExtJS5>
 *  - <Descartes.UI.CoordinatesInputInPlaceExtJS5>
 *  - <Descartes.UI.GazetteerInPlaceExtJS5>
 *  - <Descartes.UI.PrinterSetupInPlaceExtJS5>
 *  - <Descartes.UI.RequestManagerInPlaceExtJS5>
 *  - <Descartes.UI.ScaleChooserInPlaceExtJS5>
 *  - <Descartes.UI.ScaleSelectorInPlaceExtJS5>
 *  - <Descartes.UI.SizeSelectorInPlaceExtJS5>
 */
Descartes.UI.AbstractInPlaceExtJS5 = OpenLayers.Class(Descartes.UI, {
  
  /**
   * Propriete: label
   * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone.
   */
  label: true,
  
  /**
   * Propriete: defaultOptionsPanel
   * {Objet} Options par d�faut du panel.
   * 
   * :
   * collapsible - ("false" par defaut)
   * collapsed - ("false" par defaut)
   * tools - ("null" par d�faut)
   */
  defaultOptionsPanel:{
    collapsible:false,
    collapsed:false,
    tools:null
  },
  
  /**
   * Propriete: optionsPanel
   * {Objet} Options du panel.
   */
  optionsPanel:{},
  
  /**
     * Constructeur: Descartes.UI.AbstractInPlaceExtJS5
   * Constructeur d'instances
   * 
   * Param�tres:
   * div - {DOMElement|String} El�ment DOM de la page accueillant l'interface associ�e ou identifiant de cet �lement.
   * model - {Object} Donn�es manipul�es par la vue et retourn�es au contr�leur.
   * eventTypes - {Array(String)} Types d'�v�nements potentiellement d�clench�s par la vue.
   * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
   * 
   * Options de construction propres � la classe:
   * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de legende.
   * optionsPanel - {Objet} Options du panel.
   */
  initialize: function(div, model, eventTypes, options) {
    this.optionsPanel = OpenLayers.Util.extend({}, this.defaultOptionsPanel);
    
    if((options !== undefined && options.optionsPanel !== undefined)){
      this.optionsPanel = OpenLayers.Util.extend(this.optionsPanel, options.optionsPanel);
      delete options.optionsPanel;
    }
    
    Descartes.UI.prototype.initialize.apply(this, [div, model, eventTypes, options]);
    
    this.initOptionsPanel();
    
  },
  
  /**
   * Methode: initOptionsPanel
   * Permet d'assurer la coh�rence d'affichage de la vue.
   */
  initOptionsPanel: function () {
    if (!this.label){
      this.optionsPanel.collapsible=false;
      this.optionsPanel.collapsed=false;
      this.optionsPanel.tools=null;
    } else if (!this.optionsPanel.collapsible) {
      this.optionsPanel.collapsed=false;
    } else {
      this.optionsPanel.tools=null;
    }
  },
  
  
  CLASS_NAME: "Descartes.UI.AbstractInPlaceExtJS5",

  VERSION : "3.2"
});

