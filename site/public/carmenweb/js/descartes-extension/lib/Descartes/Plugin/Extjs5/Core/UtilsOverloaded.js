// ERROR CODES (not used in final version or ?)
Descartes.CARMEN_ERROR = 256;
Descartes.CARMEN_ERROR_SESSION_EXPIRED = 257;
/*
// iframe extjs component taken from
// https://yui-ext.com/forum/showthread.php?p=298080
Ext.ux.IFrameComponent = function(config) {
  this.addEvents(
        'load'
  );
  Ext.ux.IFrameComponent.superclass.constructor.call(this, config);
};
Ext.extend(Ext.ux.IFrameComponent, Ext.BoxComponent, {
    
    onRender: function(ct, position){
        this.el = ct.createChild({
            tag: 'iframe',
            id: 'iframe-' + Ext.id(),
            name : this.name,
            width : '100%',
            height: '100%',
            frameBorder: 0,
            src: this.url
        });
    this.el.on('load', function() {
        this.fireEvent("load", this);
      }, this);
    }
});
*/
Descartes.UtilsOverloaded = {

  /** 
   * Various Constants
   */
  
  INFO_TOOL_XY_TOLERANCE : 5,
  
  /**
   * 
   * Fonction Descartes.Utils.positionToStr
   * permet de transformer l'objet interrogé en chaine de caractères selon son type (bounds, lonlat ou pixel)
   * 
   * Paramètres:
   *  position {<OpenLayers.Bounds}
   * 
   */
  
  positionToStr : function(position) {
    var str = "";
    if (position instanceof OpenLayers.Bounds) {
      str = '(' + position.left + ',' + position.bottom + ',' + position.right + ',' + position.top + ')';  
    }
    else if (position instanceof OpenLayers.LonLat) {
      str = '(' + position.lon + ',' + position.lat + ')';
    }
    else if (position instanceof OpenLayers.Pixel) {
      str = '(' + position.x + ',' + position.y + ')';
    }
    return str; 
  },
  
  strExtentToOlBounds : function(extentStr, punctualBufferRadius) {
  	if ( !extentStr ) return null;
    var tabExtent = extentStr.substr(1,extentStr.length-1).split(",");
    for (var i=0; i<4; i++) {
       tabExtent[i] = parseFloat(tabExtent[i]);
    }
    if (punctualBufferRadius) {
      // in case of extent limited to a point, we extend the extent
      if (tabExtent[1]==tabExtent[3]) {
        tabExtent[1] = tabExtent[1] - punctualBufferRadius;
        tabExtent[3] = tabExtent[3] + punctualBufferRadius;
      }
      if (tabExtent[2]==tabExtent[4]) {
         tabExtent[2] = tabExtent[2] - punctualBufferRadius;
         tabExtent[4] = tabExtent[4] + punctualBufferRadius;
      }
    }
      return new OpenLayers.Bounds(tabExtent[0], tabExtent[1], tabExtent[2], tabExtent[3]);
  },
  
  
  zoomToSuitableExtent : function(OL_map, bounds, minScale,maxScale) {
      var center = bounds.getCenterLonLat();
      var zoom = OL_map.getZoomForExtent(bounds);
      if (minScale && maxScale) {
          var resolution = OL_map.getResolutionForZoom(zoom);
          var scale = OpenLayers.Util.getScaleFromResolution(resolution, OL_map.units);
          //console.log(minScale + ' ' + scale + ' ' + maxScale);
          var newScale = scale<minScale ? minScale :
          (scale>maxScale ? maxScale : scale);
          if (newScale != scale) {
              resolution = OpenLayers.Util.getResolutionFromScale(newScale, OL_map.units);
              zoom = OL_map.getZoomForResolution(resolution);
          }
      }
      OL_map.setCenter(center, zoom);
  },
  
  /**
   * Fonction Descartes.Utils.positionToBounds
   * Renvoie la position et la transforme en bounds
   * 
   *  Paramètres:
   *  position {Object} - 
   *  mapobjet {<OpenLayers.Map} 
   * 
   */
  positionToBounds : function(position, mapobject) {
    var bounds = null;
    // Pour gérer l'interogation par sélection rectangulaire  
    if (position instanceof OpenLayers.Bounds) {
      var minXY = mapobject.getLonLatFromPixel(
      new OpenLayers.Pixel(position.left, position.bottom));
      var maxXY = mapobject.getLonLatFromPixel(
      new OpenLayers.Pixel(position.right, position.top));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat, maxXY.lon, maxXY.lat);
    }
    else {
      // Pour gérer l'interrogation par cercle
      if (position instanceof OpenLayers.Geometry.Polygon ){      
              bounds = position.getBounds();      
      }
      else {
        // it's a pixel
            // if no tolerance
            // this.showInfoWindow(this.map.getLonLatFromPixel(position));
            // with tolerance
            var tol = this.INFO_TOOL_XY_TOLERANCE;
            var minXY = mapobject.getLonLatFromPixel(
            new OpenLayers.Pixel(position.x-tol, position.y-tol));
            var maxXY = mapobject.getLonLatFromPixel(
            new OpenLayers.Pixel(position.x+tol, position.y+tol));
            bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
            maxXY.lon, maxXY.lat);      
      }
    }
    return bounds;
  },
  
  circleToStr : function(circle) {
    var str = "";
    // test if we have a circle
    if (circle.radius!=undefined && circle.origin!=undefined) {
    str = '(' + circle.origin.x + ',' + circle.origin.y + ',' + circle.radius + ')';
    }
    return str;
  },
  
  simplifyFeature : function(feature) {
    var newFeatures = [];
    var res = [];
    var geom = feature.geometry;
    if (Descartes.Utils.isComplexGeometry(geom)) {
      //console.log(geom.CLASS_NAME +" found !")
      for (var j=0; j<geom.components.length; j++) {
        var f = feature.clone();
        f.geometry = f.geometry.components[j];
        newFeatures.push(f);
      }
      res = res.concat([], newFeatures);
    }
    else {
          res = [feature];
    }
    
    if (newFeatures.length>0) {
      //alert("before relaunching simplification...");
      for (var i =0; i<newFeatures.length; i++ ) {
        var newFeatures2 = Descartes.Utils.simplifyFeature(newFeatures[i]);
        if (newFeatures2.length>0){
          res = res.concat(newFeatures2);
        }
      }
    }
    return res;
  },  

  isComplexGeometry : function(geom) {
      return (geom.CLASS_NAME=="OpenLayers.Geometry.Collection" ||
          geom.CLASS_NAME=="OpenLayers.Geometry.MultiLineString" ||
          geom.CLASS_NAME=="OpenLayers.Geometry.MultiPolygon" ||
          geom.CLASS_NAME=="OpenLayers.Geometry.MultiPoint");
  },
  
  getProjectionUnits : function(cs) {
        var prj = null;
        if (typeof cs == "string"){
            prj = new OpenLayers.Projection(cs);
        }
        else if (cs instanceof OpenLayers.Projection){
            prj = cs;
        }
        return (prj==null) ? 'dd' : prj.getUnits()== null ? 'dd' : prj.getUnits();
    },

  /**
   * @brief convert a boolean value to its corresponding string representation
   * @param bool : a boolean value
   * @return "1" for true, "0" for false or undefined 
   */
  bool2IntStr : function(bool) {
    return bool ? "1" : "0";
  },

  /**
   * Fonction pour supprimer le separateur de fin de ligne et
   * découper en fonction du séparateur  *  
   * 
   * Paramètres:
   * str {<string>} - Chaine à traiter
   * sep {<string>} - Separateur pour le découpage avec split 
   * 
   */

  chompAndSplit : function(str, sep) {
    // chomp
    //console.log('str in cas : ' + str);
    if (str.length>0 && str.substr(str.length-1,1)==sep) {
     str = str.substr(0,str.length-1);
    }
    // and split
    return str.split(sep);
  },

  decodeDescartesFieldDesc : function(fieldDesc, baseURL) {
      var arrFieldDesc = Descartes.Utils.chompAndSplit(fieldDesc, '|');
      if (!baseURL)
        baseURL = '';
      return arrFieldDesc.length>=3 ? {
           name : arrFieldDesc[0],
           alias : arrFieldDesc[1],
           type : arrFieldDesc[2],
           // if no URL template (arrFieldDesc[4]) given,
           // putting entire field (arrFieldDesc[0]) in it 
           url : arrFieldDesc[2] != 'URL' ? null :
             (arrFieldDesc[3]!== "" ? baseURL + arrFieldDesc[3] : baseURL + '<'+ arrFieldDesc[0] +'>'),
            // on ajoute un champs title si le champs est un id
           title : arrFieldDesc[5] != null ? true : null
          } : null;
  },
    
  /**
   * 
   * 
   * 
   */

  // Miscellaneous functions
  // array_filter : return an array composed of 
  // elements of aray satisfying filter test
  array_filter : function (aray, filter) {
    var res = [];
    for(var i=0; i<aray.length;i++) {
      if (filter(aray[i])) {
        res.push(aray[i]);
      }
    }
    return res;
  },
  
  
  openWindowIframe : function(title, url, autosize) {
//    url = decodeURIComponent(url);
    dec_url = decodeURIComponent(url);    

    var iFrame = new Ext.ux.IFrameComponent();
    var win = new Ext.Window({
          layout : 'fit',
        width: autosize ? 'auto' : 550,
        height: autosize ? 'auto' : 500,
        plain : true,
        title : title,
        header : (title!=null) && (title!=''),
          modal : false,
          autoDestroy : true,
          resizable : true,
                  maximizable : true,        
          items : [ iFrame ]        
        });
    win.on(
      'maximize',
      function (winElt) {
        //alert('here' + url + '  ' + title);
        winElt.close();
        window.open(dec_url);
      },
      win
    );
  
    var success = function(rep){
      if (rep && rep.responseText){
        if(SEN_CONTEXT == "true"){      
          if (rep.responseText != 404 && rep.responseText != 408){
            iFrame.url = dec_url;
            win.show();
          }else{
            if (rep.responseText == 404){
              // pb d'authentifications
              Ext.MessageBox.alert("Erreur", "Le document demandé est introuvable.");
            }else if (rep.responseText == 408){
              // Le serveur a interrompu la connexion car le client n'a pas
              // réussi à envoyer une requête complète dans le délai qui lui
              // était imparti.
              Ext.MessageBox.alert("Erreur", "Le délai d'envoi dépassé.");
            }
          }
        }else{
          if (rep.responseText >= 100 && rep.responseText < 400){
            iFrame.url = dec_url;
            win.show();
          }else{
            if (rep.responseText < 100){
              // domaine de l'URL invalide
              Ext.MessageBox.alert("Erreur", "Impossible de charger la page.");
            }else if (rep.responseText >= 500){
              // Erreur serveur
              Ext.MessageBox.alert("Erreur", "Une erreur serveur est survenue lors du chargement de la page.");
            }else if (rep.responseText == 403){
              // pb d'authentifications
              Ext.MessageBox.alert("Erreur" , "Délai d'inactivité atteint ; rechargement de la carte initiale ", function(){window.location.reload(); });
            }else if (rep.responseText == 404){
              // pb d'authentifications
              Ext.MessageBox.alert("Erreur", "Le document demandé est introuvable.");
            }else {
              // erreurs 400-499
              Ext.MessageBox.alert("Erreur", "Impossible de charger la page.");
            }
          }
        } 
      }     
    };
    
    urlCheckStatus = "/descartes-extension/php/services/getHttpStatus.php?url="+ url;
    
    Ext.Ajax.request({
            url: urlCheckStatus,
            success: success
    });
  
  },
  
        descartesTypeToExtRenderer : function(descartesStr) {
          var res = Descartes.Utils.textRenderer;

          switch(descartesStr) {
            case 'AAAAMMJJ':
              res = Ext.util.Format.dateRenderer('d/m/Y');
              break;
            case 'AAAAJJMM':
              res = Ext.util.Format.dateRenderer('d/m/Y');
              break;
            case 'MMJJAAAA':
              res = Ext.util.Format.dateRenderer('d/m/Y');
              break;
            case 'JJMMAAAA':
              res = Ext.util.Format.dateRenderer('d/m/Y');
              break;
            case 'URL' :
              res = Descartes.Utils.URLRenderer;
              break;
            case 'IMG' :
              res = Descartes.Utils.imgRenderer;
              break;
            default :
              break;
          }
          return res;
        },
  
  descartesDateTypeToExtDateFormat : function(descartesStr) {
    var res = 'd-m-Y';
    switch(descartesStr) {
      case 'AAAAMMJJ' :
        res = 'Y-m-d';
        break;
      case 'AAAAJJMM' :
        res = 'Y-d-m';
        break;        
      case 'MMJJAAAA' :
        res = 'm-d-Y';
        break;
      case 'JJMMAAAA' :
        res = 'd-m-Y';
          break;                
        default :
          break;         
      }
      return res;
  },
  
  descartesTypeToExtType : function(descartesStr) {
    var res = 'auto';
    switch(descartesStr) {
      case 'TXT','IMG' :
        res = 'string';
        break;
      case 'URL','BOUTON' : 
        res= 'auto';
        break;  
      case 'AAAAMMJJ', 'AAAAJJMM', 'MMJJAAAA','JJMMAAAA' :
        res = 'date';
          break;
        default :
          break;         
      }
      return res;
  },
  
  textRenderer : function(v) {
    var html =  '<div><span>' + v + '</span></div>';
      return html;
  },
  isExtensionTrueReturnImgIcon: function(string){
      var extensions = {  'pdf' : 'Pdf',
                          'txt' : 'Txt',
                          'odt' : 'Odt',
                          'ods' : 'Ods',
                          'doc' : 'Doc',
                          'docx' : 'Doc',
                          'png' : 'Png',
                          'jpg' : 'Jpg',
                          'jpeg' : 'Jpg',
                          'gif' : 'Jpg',
                          'xls' : 'Xls',
                          'xlsx' : 'Xls',
                          'kml' : 'Kml'
                         };
      var isPresent = null;
      for (extension in extensions){
          var pattern = new RegExp('\\.'+ extension +'$', 'i');
          if(pattern.test(string)===true){
            isPresent = extensions[extension];
          }
      }
      return isPresent;
  },  
  
  URLRenderer : function(v) {
  	var href = decodeURIComponent(v.href);
  	href = (href.indexOf('http')==0 ? href : 'http://'+href);
    var html =  '<div class="cmnLayerTreeLayerNodeMetadata"><a target="Informations d&eacute;taill&eacute;es" href="' + decodeURIComponent(href) + '" >' + v.text + '</a></div>';
    var img = Descartes.Utils.isExtensionTrueReturnImgIcon(v.href)
    if (img !== null){
      var html =  '<div class="DescartesIconDocument' + img + '" onclick="javascript:window.open(\'' + v.href + '\', \'Informations d&eacute;taill&eacute;es\');">' +
      		'<img class="DescartesImageTooltipDocumentDim" src="'+ Ext.BLANK_IMAGE_URL +'" title="' + v.href + '"/></div>';
    }
    return html;
        
        
    v.href = v.href.replace('&', '%26');
        v.href = v.href.replace('=', '%3D');
        // [P2-18] 6200 : Affichage URL dans la barre d'état
        // J'ajoute l'URL, mais j'ai constaté que la partie "préfixe global" était URLEncodée
        // alors que la partie URL au niveau du champ ne l'est pas.
        // Or on concatène les deux ; et le code ci-dessus laisse penser qu'on voudrait tout
        // avoir en encodé pour le passer à la fonction "openWindowIframe".
        // Donc deux choses :
        // 1°) Ce serait bien de changer le backoffice pour stocker la valeur URL du champ
        // en encodé, plutôt que de faire un simili-encodage ci-dessus (qui est incomplet, il
        // manque par exemple les "/")
        // 2°) Je décode pour positionner l'URL en "href" dans une balise anchor (<a>) plutôt
        // qu'un span, de manière à satisfaire la demande d'affichage dans la barre d'état.
        // On notera qu'il faut que le "onClick" fasse un "return false" pour que le href ne
        // soit pas exécuté.
        var html =  '<div class="cmnLayerTreeLayerNodeMetadata"><a onclick="javascript:Descartes.Utils.openWindowIframe(' +  
      '\'Informations d&eacute;taill&eacute;es\', \'' + 
      v.href + '\'); return false;" href="' + decodeURIComponent(v.href) + '" >' + v.text + '</a></div>';
      var img = Descartes.Utils.isExtensionTrueReturnImgIcon(v.href)
        if (img !== null){
          var html =  '<div class="DescartesIconDocument' + img + '" onclick="javascript:Descartes.Utils.openWindowIframe(' +  
          '\'Informations d&eacute;taill&eacute;es\', \'' + 
          v.href + '\');"><img class="DescartesImageTooltipDocumentDim" src="'+ Ext.BLANK_IMAGE_URL +'" title="' + v.href + '"/></div>';
        }
      return html;
      
  },
  
  imgRenderer : function(src) {
    var html =  '<div><span>Aucune image associ&eacute;e</span></div>'
    if (src!=null && src!='')
      html = '<div class="cmnImgRenderer"><img src="'+ 
        src + '" onclick="javascript:Descartes.Utils.openWindowIframe(' +  
        '\'\', \'' + 
        src + '\', true);" /></div>';
      //console.log(html);
      return html;
  },
  valueInArray : function (a){
    var o = {};
    for(var i=0;i<a.length;i++){
      o[a[i]]='';
    }
    return o;
  },   
    /**
     * APIMethod: degToDMS
     * Convert decimal degrees number into sexagecimal degrees string.
     *
     * Parameters:
     * dec - {Number} decimal degrees
     * locals - {Array} the axis direction (N, S) or (E, W).
     *      If undefined, null or empty, the leading minus will prefix the
     *      decimal degrees string.
     *
     * Returns:
     * {String} the sexagecimal value whose syntax conforms with
     *      <Geoportal.Util.dmsToDeg>() function.
     */
    degToDMS: function(dec, locals) {
        var positive_degrees= Math.abs(dec);
        var degrees= Math.round(positive_degrees + 0.5) - 1;
        var decimal_part= 60*(positive_degrees - degrees);
        var minutes= Math.round(decimal_part + 0.5) - 1;
        decimal_part= 60*(decimal_part - minutes);
        var seconds= Math.round(decimal_part + 0.5) - 1;
        var remains= 10 * (decimal_part - seconds);
        remains= remains.toFixed(this.numDigits);
        if (remains>=10) {
            seconds= seconds+1;
            remains= 0;
        }
        if (seconds==60) {
            minutes= minutes+1;
            seconds= 0;
        }
        if (minutes==60) {
            degrees= degrees+1;
            minutes= 0;
        }
        var sig= '';
        var dir= '';
        if (locals && (locals instanceof Array) && locals.length==2) {
            dir= ' ' + (dec > 0 ? locals[0] : locals[1]);
        } else {
            sig= (dec >= 0 ? '' : '-');
        }

        var s= sig +
               (degrees < 10 ? "0" + degrees : degrees) + "&deg; " +
               (minutes < 10 ? "0" + minutes : minutes) + "' " +
               (seconds < 10 ? "0" + seconds : seconds) + "." +
               remains + "\"" +
               dir;
        return s;
    },
  ExtJS : {
  },
  
  unhtmlspecialchars : function(str) {
    str = str.replace(/&amp;/g,'&');
    str = str.replace(/&quot;/g, '"');
    str = str.replace(/&gt;/g, '>');
    str = str.replace(/&lt;/g, '<');
    return str;
  },
  
  htmlspecialchars : function(str) {
    str = str.replace(/&/g,'&amp;');
    str = str.replace(/"/g,'&quot;');
    str = str.replace(/>/g,'&gt;');
    str = str.replace(/</g,'&lt;');
    return str;
  },
  

  json2xml : function(o, tab, header) {
    var toXml = function(v, name, ind) {
      var xml = "";
      if (v instanceof Array) {
        for ( var i = 0, n = v.length; i < n; i++)
          xml += ind + toXml(v[i], name, ind + "\t") + "\n";
      } else if (typeof (v) == "object") {
        var hasChild = false;
        xml += ind + "<" + name;
        for ( var m in v) {
          if (m.charAt(0) == "@")
            xml += " " + m.substr(1) + "=\"" + Descartes.Utils.htmlspecialchars(v[m].toString()) + "\"";
          else if (m=="attributes") {
            for (n in v[m]) {
              xml += " " + n.toString() + '="' + Descartes.Utils.htmlspecialchars(v[m][n].toString()) + '"';  
            }
          } else
            hasChild = true;
        }
        xml += hasChild ? ">" : "/>";
        if (hasChild) {
          for ( var m in v) {
            if (m == "#text")
              xml += v[m];
            else if (m == "#cdata")
              xml += "<![CDATA[" + v[m] + "]]>";
            else if (m.charAt(0) != "@" && m != "attributes")
              xml += toXml(v[m], m, ind + "\t");
          }
          xml += (xml.charAt(xml.length - 1) == "\n" ? ind : "") + "</"
              + name + ">";
        }
      } else {
        xml += ind + "<" + name + ">" + Descartes.Utils.htmlspecialchars(v.toString()) + "</" + name + ">";
      }
      return xml;
    };
    var xml = header ? header : "";
    
    for ( var m in o)
      xml += toXml(o[m], m, "");
    return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
  },
  
  loadPage : function(URL) {
    if (URL)
      top.window.location.replace(URL);
    else {
      top.window.location.reload();
    }
  },
  
  handleAjaxFailure : function(response, callerName, skipExceptSession) {
    callerName = Ext.valueFrom(callerName, "Descartes", false);
    skipExceptSession = Ext.valueFrom(skipExceptSession, false, false);
    
    if (response && response.status==500) {
    	if (!skipExceptSession) Ext.Msg.alert(callerName, 'Service indisponible');
    	return;
    }
    if (response && response.status!=404) {
    	  try {
        var response = Ext.decode(response.responseText);
    	  } catch (error){response = {};}
        if (response.errmsg) {
            var failureType = Ext.valueFrom(response.failureType, 0, false);  
            // special handling for SESSION_EXPIRED error 
            if (failureType == Descartes.CARMEN_ERROR_SESSION_EXPIRED)
              Ext.Msg.alert(callerName, response.errmsg, 
                function() {
                  top.window.location.reload();
                });
            else if (!skipExceptSession) 
              Ext.Msg.alert(callerName, response.errmsg + " (" + response.failureType + ")");
        }
        else if (!skipExceptSession)
            Ext.Msg.alert(callerName, 'Service indisponible');
      }
      else if (!skipExceptSession)
        Ext.Msg.alert(callerName, 'Service indisponible');
  },
  
    clone : function(obj) {
        var cloned = {};
        for (a in obj) {
            cloned[a] = obj[a];
        } 
        return cloned;
        // return eval(uneval(obj));
    },
    removeUndefinedFromObject: function(objectInput){
      tmpArray = [];
      var j = 0;
      for (i in objectInput){
          if (typeof(objectInput[i])!=='undefined'){
            if (typeof(objectInput[i])!=='function'){
                  //console.log(typeof(objectInput[i]));
                  tmpArray.push(objectInput[i]);
            }
          }
      }
      return tmpArray;
    },
    
    isEmpty : function(ob){
        for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
        return true;
    },
  in_array : function (needle, haystack, argStrict) {
      // http://kevin.vanzonneveld.net
      // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // +   improved by: vlado houba
      // +   input by: Billy
      // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
      // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
      // *     returns 1: true
      // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
      // *     returns 2: false
      // *     example 3: in_array(1, ['1', '2', '3']);
      // *     returns 3: true
      // *     example 3: in_array(1, ['1', '2', '3'], false);
      // *     returns 3: true
      // *     example 4: in_array(1, ['1', '2', '3'], true);
      // *     returns 4: false
      var key = '',
          strict = !! argStrict;
  
      if (strict) {
          for (key in haystack) {
              if (haystack[key] === needle) {
                  return true;
              }
          }
      } else {
          for (key in haystack) {
              if (haystack[key] == needle) {
                  return true;
              }
          }
      }
  
      return false;
  }
  
  
};
OpenLayers.Util.extend(Descartes.Utils, Descartes.UtilsOverloaded);

//Descartes.Utils.ExtJS.emptyStore = new Ext.data.SimpleStore({fields: [], data: []});
//Descartes.Utils.ExtJS.emptyColumnModel = new Ext.grid.ColumnModel({columns: []});
