/*
Copyright MEEDDM
Contributeur : Ga�lle Barris, Denis Chabrier

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.ScaleSelectorInPlaceExtJSExtension
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'une �chelle dans une liste pour recadrer la carte.
 * 
 * :
 * Le formulaire de saisie est constitu� d'une seule liste d�roulante.
 * 
 * H�rite de:
 *  - <Descartes.UI>
 * 
 * 
 * Ev�nements d�clench�s:
 * selected - Une �chelle est s�lectionn�e dans la liste.
 */
Descartes.UI.ScaleSelectorInPlaceExtJS5Extension = OpenLayers.Class(Descartes.UI, {

  /**
   * Propriete: label
   * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de choix.
   */
  label: true,
  
  /**
   * Propriete: ascending
   * {Boolean} Indicateur pour l'ordre d'affichage des �chelles disponibles.
   */
  ascending: false,
  
    EVENT_TYPES: ["selected"],

  /**
   * Constructeur: Descartes.UI.ScaleSelectorInPlaceExtJSExtension
   * Constructeur d'instances
   * 
   * Param�tres:
   * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
   * scaleModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.ScaleSelector>.
   * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
   * 
   * Options de construction propres � la classe:
   * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
   * ascending - {Boolean} Indicateur pour l'ordre d'affichage des �chelles disponibles.
   */
  initialize: function(div, scaleModel, options) {
    Descartes.UI.prototype.initialize.apply(this, [div, scaleModel, this.EVENT_TYPES,options]);
  },

  /**
   * Methode: draw
   * Construit la zone de la page HTML pour le choix de l'�chelle.
   * 
   * Param�tres:
   * enabledScales - {Array(Float)} Liste des d�nominateurs d'�chelle propos�s.
   */
  draw: function(enabledScales) {
    if (enabledScales.length !== 0) {
      if (this.ascending) {
        enabledScales.sort(function(a, b) {return b - a;});
      } else {
        enabledScales.sort(function(a, b) {return a - b;});
      }
      var self = this;

      var optionsSelect = [];
      //optionsSelect.push(["", this.getMessage("DEFAULT_OPTION_MESSAGE")]);
      for(var i=0; i<enabledScales.length; i++) {
        optionsSelect.push([enabledScales[i].toString(), Descartes.Utils.readableScale(enabledScales[i])]);
      }
        
      var store = new Ext.data.SimpleStore({
          fields: ['value', 'text'],
          data : optionsSelect
      });
      
      this.comboBox = Ext.create('Ext.form.ComboBox', {
        id : 'selectScaleSelector_' + (Descartes.UI.ScaleSelectorInPlaceExtJS5Extension.index++),
        hideLabel : this.toolBar ? false : true,
        fieldLabel: this.toolBar && this.label ? this.getMessage("TITLE_MESSAGE") : null,
        store: store,
        valueField:'value',
        displayField:'text',
        typeAhead: false,
        mode: 'local',
        forceSelection: true,
        triggerAction: 'all',
        editable : false,
        fieldStyle:  "text-align:right;",
        emptyText: this.getMessage("DEFAULT_OPTION_MESSAGE"),
        selectOnFocus: false,
        listConfig: {
          style:  "text-align:right;",
        },
        listeners : {
          select : function() { 
            self.done(); 
          }
        }
      });
      
      this.comboBox.addListener('blur', function() {
        //this.comboBox.clearValue();
      }, this);
      
      if (this.toolBar) {
        if (this.toolBarPos!=null && (this.toolBarPos<this.toolBar.items.length)) {
          this.toolBar.insert(this.toolBarPos, this.comboBox);
        }
        else {
          this.toolBar.add(this.comboBox);
        }
      } 
      else {
        this.formPanel = Ext.create('Ext.form.Panel',{
          renderTo: this.div.id,
          autoScroll: true,
          id: 'ScaleSelector_formPanel_' + (Descartes.UI.ScaleSelectorInPlaceExtJSExtension.index++),
          frame: true,
          title: this.label ? this.getMessage("TITLE_MESSAGE") : null,
          items: [this.comboBox]
        });
        this.formPanel.show();
      }
    }
  },
  
  /**
   * Methode: done
   * Transmet le modèle au contrôleur, en déclenchant l'évènement 'selected'.
   * 
   */
  done: function() {
    var value = parseFloat(this.comboBox.getValue());
    if (value !== "") {
      this.model.scale = value;
      this.events.triggerEvent("selected");
    }
  },
  
  /**
   * Methode: destroy
   * Nettoie la zone de la page HTML pour le choix de l'échelle.
   * 
   */  
  destroy: function() {
    if (this.toolBar) {
      this.toolBar.remove(this.comboBox, true);
    }
  },
  
  CLASS_NAME: "Descartes.UI.ScaleSelectorInPlaceExtJS5Extension"
});
Descartes.UI.ScaleSelectorInPlaceExtJS5Extension.index=0;
Descartes_Messages_UI_ScaleSelectorInPlaceExtJS5Extension = OpenLayers.Util.extend(Descartes_Messages_UI_ScaleSelectorInPlace, { "TITLE_MESSAGE" : "Echelle" });