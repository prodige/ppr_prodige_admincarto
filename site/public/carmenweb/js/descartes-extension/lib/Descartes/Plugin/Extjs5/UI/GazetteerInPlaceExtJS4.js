/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.GazetteerInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'un objet de r�f�rence pour recadrer la carte.
 * 
 * :
 * Le choix de l'objet est effectu� par s�lection de celui-ci dans un ensemble de listes d�roulantes imbriqu�es, puis par activation d'un bouton HTML. 
 * 
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS4>
 * 
 * 
 * Ev�nements d�clench�s:
 * localize - Un objet de r�f�rence a �t� choisi.
 */
Descartes.UI.GazetteerInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {

    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML du panneau de l�gendes.
     * 
     * :
	 * globalClassName - ("DescartesUI" par d�faut)
	 * textClassName - ("DescartesUIText" par d�faut)
	 * selectClassName - ("DescartesGazetteerList" par d�faut)
	 * buttonClassName - ("DescartesUIButton" par d�faut)
     */
	defaultDisplayClasses: {
		globalClassName: "DescartesUI",
		textClassName: "DescartesUIText",
		selectClassName: "DescartesGazetteerList",
		buttonClassName: "DescartesUIButton"
	},
	
	/**
	 * Propriete: proj
	 * {String} Code de la projection de la carte.
	 */
	proj: null,
	
	/**
	 * Propriete: initValue
	 * {String} Code de la valeur de l'objet "p�re" des objets du plus haut niveau de localisation (i.e. d'index 0).
	 */
	initValue: null,
	
	/**
	 * Propriete: service
	 * {<Descartes.GazetteerService>} Service c�t� serveur pour la fourniture des objets de r�f�rence.
	 */
	service: null,
	
	/**
	 * Propriete: location
	 * {String} R�pertoire de localisation des fichiers statiques contenant les objets de r�f�rence ("descartes/gazetteer/" par d�faut).
	 */
	location: Descartes._getScriptLocation() + "../gazetteer/",
	
	/**
	 * Propriete: levels
	 * {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
	 */
	levels: null,
	
	levelToActualize: null,
	jsonTool: new OpenLayers.Format.JSON(),
	
	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de localisation.
	 */
	label: true,
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
	height:100,
	
	/**
	 * Propriete: widthComboBox
	 * {Integer} Taille des comboBox.
	 */
	widthComboBox: 250,
	
	EVENT_TYPES: ["localize"],
	
	/**
	 * Constructeur: Descartes.UI.GazetteerInPlaceExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
	 * gazetteerModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.Gazetteer>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * levels - {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
	 * proj - {String} Code de la projection de la carte.
	 * initValue - {String} Code de la valeur de l'objet "p�re" des objets du plus haut niveau de localisation (i.e. d'index 0).
	 * service - {<Descartes.GazetteerService>} Service c�t� serveur pour la fourniture des objets de r�f�rence.
	 * location - {String} R�pertoire de localisation des fichiers statiques contenant les objets de r�f�rence ("descartes/gazetteer/" par d�faut).
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de localisation.
	 * widthComboBox - {Integer} Taille des comboBox.
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
	 */
	initialize: function (div, gazetteerModel, options) {
		this.levels = [];
		this.selectElements = [];
		
		if (options !== undefined) {
			this.service = options.service || null;
			delete options.service;
			if (options.levels !== undefined) {
				for (var iLevel=0 ; iLevel<options.levels.length ; iLevel ++) {
					var level = options.levels[iLevel];
					if ( !(level instanceof Descartes.GazetteerLevel)) {
						level = new Descartes.GazetteerLevel(level.message, level.error,level.name);
					}
					level.index = iLevel;
					this.levels.push(level);
				}
				delete options.levels;
			}       
		}
		Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, gazetteerModel, this.EVENT_TYPES, options]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone de la page HTML pour le choix d'un objet de r�f�rence.
	 */
 	draw: function() {
 		var gazetteerInstance = this;
 		var displayGlobal = this.displayClasses.globalClassName;
 		var displaySelect = this.displayClasses.selectClassName;
 		
 		var items = [];
 		
 		this.levels.forEach(
 				function(level) { 					
					var item = {
							displayField  : 'nom',
			                valueField    : 'code',
							fieldLabel    : "",
							labelSeparator : "",
			                typeAhead     : true,
			                //editable      : false, //true pour s'aider du clavier dans les listes deroulantes
			                forceSelection: true, //pour s'aider du clavier dans les listes deroulantes
			                selectOnFocus:true, //pour s'aider du clavier dans les listes deroulantes
			                queryMode: 'local',
			                triggerAction : 'all',
					        xtype         : 'combo',
					        //hidden		  : true,
					        width: gazetteerInstance.widthComboBox,
			                id            : gazetteerInstance.div.id + '_selectGazetteer_' + level.index,
			                store         : new Ext.data.SimpleStore({
				                                fields : ['code', 'nom']
				                            }),
                            listeners     : {
                                   select: function(combo, record){
                                       /*
                                       //avant utilisation du clavier
                                        var index = combo.store.indexOf(record[0]);
                                       gazetteerInstance.model.activeZone= gazetteerInstance.levels[level.index].datasList[index];
                                       gazetteerInstance.showLevel(level.index + 1, record[0].data.code);*/
                                	   
                                	   var code = record[0].data.code;
                                	   var dataList = gazetteerInstance.levels[level.index].datasList;
                                	   for (var i=0, len=dataList.length ; i<len ; i ++) {
                                		   if(dataList[i].code===code){
                                			   gazetteerInstance.model.activeZone= dataList[i];
                                    		   gazetteerInstance.showLevel(level.index + 1, code);
                                    		   break;
                                		   }
                                		   
                                	   }  
                                   },
	                                change: function(combo, record){
	                                	//pour utilisation du clavier
	                                	for (var idxLevel=0, len=gazetteerInstance.levels.length; idxLevel<len ; idxLevel ++) {
	                        				if (gazetteerInstance.levels[idxLevel].index > level.index) {
	                        					Ext.getCmp(gazetteerInstance.div.id + '_selectGazetteer_' + gazetteerInstance.levels[idxLevel].index).hide();
	                        				}
	                        			}
	                               },
	                               blur: function(combo){
		                            	//pour utilisation du clavier
		                            	 var code = combo.lastValue;
		                          	   var dataList = gazetteerInstance.levels[level.index].datasList;
		                          	   for (var i=0, len=dataList.length ; i<len ; i ++) {
		                          		   if(dataList[i].code===code){
		                          			   gazetteerInstance.model.activeZone= dataList[i];
		                              		   gazetteerInstance.showLevel(level.index + 1, code);
		                              		   break;
		                          		   }
		                          		   
		                          	   }
                                
	                               }
                            }
					};
					items.push(item);
 				}
 		);	
 		
 		 var title = '';
         
         if(this.label){
             title = this.getMessage("TITLE_MESSAGE");
         }
 		
         var optsPanel = {
 	        xtype        : 'panel',
	        title		 : title,
	        renderTo     : this.div.id,
	        autoScroll   : true,
	        id           : this.div.id + '_panelGazetteer',
	        frame        : true,
	        items        : items,
		    buttons      : [{
		        text     : this.getMessage("BUTTON_MESSAGE"),
		        handler  : function() {
		        	gazetteerInstance.events.triggerEvent("localize");	        		
		        }
		    }],
		    buttonAlign  : 'center'
	    };
         
		this.panel =  new Ext.Panel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));
	    this.panel.show();
		
	    this.showLevel(0, this.initValue);
	    
	    if(!(this.label && this.panel.collapsible && this.panel.collapsed)){
        	this.modifyStyle();
        }
 	},
 	
 	modifyStyle: function() {
 		var h = this.height;
 		if(!this.label){
 			h -= 25;
 		}
 		this.panel.getEl().dom.style.height=h+"px";
	},
 	
	/**
	 * Methode: showLevel
	 * Demande l'actualisation d'une des liste d'objets de r�f�rence.
	 * 
	 * :
	 * Cette actualisation est automatiquement demande par le changement d'un objet dans la liste imm�diatement sup�rieure.
	 * 
	 * Param�tres:
	 * index - {Integer} Index du niveau (<Descartes.GazetteerLevel>) � actualiser.
	 * value - {String} Code de la valeur de l'objet "p�re".
	 */
	showLevel: function(index, value) {
		var existingLevel = false;
		var len = this.levels.length;
		for (var iLevel=0 ; iLevel<len ; iLevel ++) {
			if (this.levels[iLevel].index == index) {
				Ext.getCmp(this.div.id + '_selectGazetteer_' + index).show();
				existingLevel =true;
			}
		}
		if (existingLevel) {
			for (var idxLevel=0 ; idxLevel<len ; idxLevel ++) {
				if (this.levels[idxLevel].index > index) {
					Ext.getCmp(this.div.id + '_selectGazetteer_' + this.levels[idxLevel].index).hide();
				}
			}
			this.levelToActualize = index;
			this.ajaxCall(value);
		}
	},
	
	/**
	 * Methode: ajaxCall
	 * Lance une requ�te AJAX permettant de r�cup�rer un ensemble d'objets de r�f�rence de m�me niveau.
	 * 
	 * Param�tres:
	 * value - {String} Code de la valeur de l'objet "p�re".
	 */
	ajaxCall: function (value) {
		var gazetterInstance = this;
		var actionURL;
		if (this.service === null) {
			actionURL = this.location + this.proj + "/" + this.proj + this.levels[this.levelToActualize].name + value + ".json";
		} else {
			this.service.url.alterOrAddParam(this.service.levelParam, this.levels[this.levelToActualize].index);
			this.service.url.alterOrAddParam(this.service.projectionParam, this.proj);
			if (value !== "") {
				this.service.url.alterOrAddParam(this.service.codeParam, value);
			}
			actionURL = this.service.url.getUrlString();
		}
		var request = new OpenLayers.Request.GET({
			url:actionURL,
			callback:function(ajaxResponse) {gazetterInstance.actualize(ajaxResponse);} });
	},
	
	/**
	 * Methode: actualize
	 * Actualise le contenu d'une liste d�roulante avec les objets de r�f�rence ad-hoc
	 * 
	 * Param�tres:
     * ajaxResponse - {Object} R�ponse AJAX contenant les objets de r�f�rence.
	 */
	actualize: function(ajaxResponse) {
		var list = this.jsonTool.read(ajaxResponse.responseText);
   		var level = this.levels[this.levelToActualize];
   		level.datasList = list;
   		var msg = (list ? level.message : level.error);
   		var component = Ext.getCmp(this.div.id + '_selectGazetteer_' + this.levelToActualize); 
   		if (msg){
   			component.emptyText = msg;
   			component.reset();
   		}
   		
		if (list !== null) {
			var data = [];
	   		for (var itemIndex=0 ; itemIndex<list.length ; itemIndex++) {
	   			data.push([list[itemIndex].code, list[itemIndex].nom]);
	   		}
	   		component.store.loadData(data);
		}	
	},
	
	CLASS_NAME: "Descartes.UI.GazetteerInPlaceExtJS4",

	VERSION : "3.2"
});