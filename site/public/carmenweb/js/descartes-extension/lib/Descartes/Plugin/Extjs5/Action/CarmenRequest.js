/**
 * Class: Descartes.Action.AjaxSelection
 * Classe technique assurant les communications entre les services d'interrogation c�t� serveur et les vues accueillant les r�sultats.
 * 
 * H�rite de:
 * - <Descartes.Action>
 * 
 * Ecouteurs mis en place:
 *  - l'�v�nement 'gotoFeatureBounds' de la classe d�finissant la vue MVC associ�e, quand elle est de type <Descartes.UI.TYPE_WIDGET>, d�clenche la m�thode <gotoFeatureBounds>.
 */
Descartes.Action.CarmenRequest = OpenLayers.Class(Descartes.Action, {

    /**
     * Propriete: showGraphicalSelection
     * 
     */
    showGraphicalSelection: true,

    layerTreeParams: null,

    ajaxParameters: null,

    jsonobject: null,
    
    mapfile: null,
    
    querytype: null,
    
    action: null,
    
    vectorLayer: null,
    
    urlquery: null,

    /**
     * Constructeur: Descartes.Action.CarmenRequest
     * Constructeur d'instances
     * 
     * Param�tres:
     * resultUiParams - {Object} Objet li� � l'interogation g�ographique.
     * mapContent - {Object} Objet stockant les Param�tres n�cessaires pour la g�n�ration de la requ�te au service d'interrogation.
     * OL_map - {OpenLayers.Map} Carte OpenLayers sur laquelle intervient le contr�leur.
     * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
     * 
     * Propri�t�s n�cessaires pour la g�n�ration de la vue:
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour l'affichage des r�sultats.
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * div - {DOMElement|String} : El�ment DOM de la page accueillant le r�sultat de la s�lection si le type est <Descartes.UI.TYPE_WIDGET> ou <Descartes.UI.TYPE_INPLACEDIV>.
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhait�.
     * withReturn - {Boolean} : Indique si le r�sultat doit proposer une localisation rapide sur les objets retourn�s.
     * withCsvExport - {Boolean} : Indique si le r�sultat doit proposer une exportation CSV pour les objets retourn�s.
     */
    initialize: function (mapContent, mapfile, OL_map, querytype) {
        this.model = {};
        this.mapContent = mapContent;
        this.mapfile = mapfile;
        this.OL_map = OL_map;
        this.querytype = querytype;

        Descartes.Action.prototype.initialize.call(this, this.div, OL_map, Descartes.ViewManager.getView(Descartes.ViewManager.INFORMATION_IN_PLACE), {});
        this.renderer.events.register('highlightSelectedFeatures', this, this.highlightSelectedFeatures);
        this.renderer.events.register('closed', this, function() {
            if (Ext.getCmp('window_ProdigeBufferInPlaceExtJS0') && (Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED == 1)){
                Ext.getCmp('window_ProdigeBufferInPlaceExtJS0').setVisible(false);
            }
        	this.OL_map.removeLayer(this.vectorLayer);
    		this.vectorLayer.destroyFeatures();
    		//Desactivate buffer button
    		Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED = 0;
        });

		this.vectorLayer = new OpenLayers.Layer.Vector("CarmenRequest_vectorLayer");
// TODO Serialization KML
//		this.vectorLayer = new OpenLayers.Layer.Annotation("Vector Layer", 
//				{
//					fixedPosition : 0,
//					useStyleMap : true
//				});
				
        
        olMapTest = this.OL_map;
        vectorLayerTest = this.vectorLayer;

    },
    
    filterAjaxParamsUrlLayer : function(){
        var ajaxParamsUrl = this.ajaxParameters;
        var includeLayers = [];
        var inputparams = Ext.decode(ajaxParamsUrl.fields);
        var extarray = [];
        // Boucle pour avoir le nom des couches contenant une URL
        for (var i = 0; i < inputparams.length; i++) {                    
            var intarray = [];
            for (var j = 0; j < inputparams[i].length; j++) {
                if (inputparams[i][j].type == 'URL'){
                    intarray.push(inputparams[i][j]);
                }
            }
            if (intarray.length > 0){
                extarray.push(intarray);
                includeLayers.push(Ext.decode(ajaxParamsUrl.layers)[i]);
            }
        }
        return includeLayers;
    },
    
    filterAjaxParamsUrl : function(){
        var ajaxParamsUrl = this.ajaxParameters;
        var includeLayers = this.filterAjaxParamsUrlLayer();
  
                
                
        var layersCopy = [];
        var fieldsCopy = [];
        var briefFieldsCopy = [];
        var layersIncludedFields = [];
//        var layersIncludedBriefFields = [];
                
        for (var i = 0; i < includeLayers.length; i++) {
                    
            var layerPosition = Ext.decode(ajaxParamsUrl.layers).indexOf(includeLayers[i]);
            layersCopy.push(Ext.decode(ajaxParamsUrl.layers)[layerPosition]);
            var includedFields =[];
            var includedBriefFields =[];
            for (var j = 0; j < Ext.decode(ajaxParamsUrl.fields)[layerPosition].length; j++) {
                if (Ext.decode(ajaxParamsUrl.fields)[layerPosition][j].type == 'URL'){
                    includedFields.push(Ext.decode(ajaxParamsUrl.fields)[layerPosition][j]);
//                    includedBriefFields.push(Ext.decode(ajaxParamsUrl.briefFields)[layerPosition][j]);
                }
            }
            layersIncludedFields.push(includedFields);
//            layersIncludedBriefFields.push(includedBriefFields);
                    
                    
            briefFieldsCopy.push(Ext.decode(ajaxParamsUrl.briefFields)[layerPosition]);
        }
                
//         console.log(Ext.encode(layersIncludedFields));
//         console.log(Ext.encode(layersCopy));
//         console.log(Ext.encode(briefFieldsCopy));
        ajaxParamsUrl.layers = Ext.encode(layersCopy);
        ajaxParamsUrl.fields = Ext.encode(layersIncludedFields);
        ajaxParamsUrl.briefFields = Ext.encode(briefFieldsCopy);
        ajaxParamsUrl.layerInfo = Ext.encode(ajaxParamsUrl.layerInfo);
        return ajaxParamsUrl;
    },
    
    performGetFeatureInfo: function (evt, callback) {
    	
    	var pixel = new OpenLayers.Pixel(evt.xy.x, evt.xy.y);
    	var fluxPixel = "#X=" + pixel.x.toString() + "#Y=" + pixel.y.toString(); 
    	
    	if (this.mapContent) {

    		var layers = [];
    		for (var iLayer=0 ; iLayer<this.mapContent.layers.length ; iLayer++) {
    			if (this.mapContent.layers[iLayer].configuration) {
    				if (this.mapContent.layers[iLayer].configuration.jsonLayerDesc.Extension.INTERROGEABLE == "ON"){
    					layers.push(this.mapContent.layers[iLayer]);
    				}
    			}
    		} 
    		var fluxInfos = Descartes.ExternalCallsUtils.writeQueryPostBody(Descartes.ExternalCallsUtils.WMS_SERVICE, layers,this.OL_map);
    		if (fluxInfos !== null) {
    			var requestParams = {distantService:Descartes.FEATUREINFO_SERVER, infos: fluxInfos, pixelMask:fluxPixel};
    			pixel.x = evt.clientX;
    			pixel.y = evt.clientY;

    			this.format = "JSON";

    			var postBody = "infos=" + escape(requestParams.infos);
    			if (requestParams.gmlMask !== undefined) {
    				postBody += "&gmlMask=" + requestParams.gmlMask;
    			}
    			if (requestParams.dataMask !== undefined) {
    				postBody += "&dataMask=" + requestParams.dataMask;
    			}
    			if (requestParams.pixelMask !== undefined) {
    				postBody += "&pixelMask=" + requestParams.pixelMask;
    			}
    			if (this.withReturn) {
    				postBody += "&withReturn=true";
    			}
    			if (this.withCsvExport) {
    				if (options === undefined) {
    					options = {};
    				}
    				options.withCsvExport = true;
    				postBody += "&withCsvExport=true";
    			}
    			postBody +="&format=" + this.format; 
    			
    			var self = this;
    			var request = new OpenLayers.Request.POST(
    					{ url:requestParams.distantService,
    						headers: {"Content-Type": "application/x-www-form-urlencoded"},
    						data:postBody,
//    						success:function(myAjaxRequest) { myAjaxRequest.responseText;},
    						success: callback,
    						failure:function() {alert('probleme lors du getfeatureinfo');}
    					}
    			); 
//    			var action = new Descartes.Action.AjaxSelection(this.resultUiParams, requestParams, this.OL_map,{pixel:pixel,toolTipLayers: this.toolTipLayers});
    		}
    		
    	}
    },
    
    performQuery: function (obj, urlquery, getFInfoRep) {
    	var me = this;
    	
    	this.getFInfoRep = getFInfoRep;
    	this.urlquery = urlquery;
    	
    	this.vectorLayer.destroyFeatures();
    	
    	var layers = this.mapContent.getQueryableLayers();//getLayers();
    	
    	var layers2 = [];
    	
    	for (var i = 0; i < layers.length; i++){
    		if (this.mapContent.layers[i].configuration){
    			if (this.mapContent.layers[i].configuration.jsonLayerDesc.Extension.INTERROGEABLE != "ON"){
            		layers2.push(layers[i]);
            	}
    		}
    	}
    	
    	layers = layers2;
        //R�cup�re params depuis mapContent pour la requete ajax
        this.ajaxParameters = this.buildParams(layers, obj);
        
        
        if (this.urlquery == 'URL'){
        	if (this.filterAjaxParamsUrlLayer().length > 0){
        		this.ajaxParameters = this.filterAjaxParamsUrl();
        	}
        	else {
        		this.renderer.urlInfoNoURL();
        		return;
        	}            
        }
            
        

        //Recup�re params depuis mapContent pour pouvoir construire le noeud de
        // listing des couches avec leur libell�
        this.layerTreeParams = this.getLayersNameTitle(layers);


        //Preparation pour requete ajax
        var url = Routing.generate('carmen_ws_helpers_getinformation', {
                    mapfile  : Carmen.user.map.get('real_mapfile')
                  });
        
        if( layers.length != 0 ){
          this.waitingWindow = Ext.Msg.wait('Interrogation en cours');
        	 var request = OpenLayers.Request.POST({
          	timeout : 1000*1000,
                 method: "POST",
                 url: url,
                 success: OpenLayers.Function.bind(this.onsuccess, this),
                 failure: function (response) {
              me.waitingWindow && me.waitingWindow.hide();
                 	Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED = 0;
              Descartes.UtilsOverloaded.handleAjaxFailure(response, "La requête ajax a échouée", false);
                 },
                 headers: {
                     "Content-Type": "application/x-www-form-urlencoded"
                 },
                 data: OpenLayers.Util.getParameterString(this.ajaxParameters)
             });
        }else{
        	if( getFInfoRep != null ) {
        		this.jsonobject = null;
            	this.getGFIResAndDraw();
        	}else{
        		this.renderer.drawAlert("noQueryableLayer",null, null);
        	}
        	
        }
       
    },
    /**
     * Methode: buildParams
     * R�cup�re les Param�tres n�cessaires pour l'envoi de la requ�te au service d'interrogation de Carmen
     * 
     * Param�tres:
     * nodes - {Object} objet contenant la description des couches (issu de mapContent)
     * obj {Objet} objet li� � l'interrogation graphique
     * fieldsFilter {<string>} filtre Accepte null
     * 
     */

    buildParams: function (nodes, obj) {
        var layerNames = [];
        var layerInfo = {};
        var fieldsList = [];
        var briefFieldsList = [];
        var baseQueryURLList = [];

        for (var i = 0; i < nodes.length; i++) {
        	if (nodes[i].configuration){
        		if (nodes[i].configuration.jsonLayerDesc.Extension.layerSettings_infoFields) {
        			var l_info = { 'title' : decodeURIComponent(nodes[i].configuration.jsonLayerDesc.Title)};
              layerInfo[nodes[i].configuration.jsonLayerDesc.Extension.LAYER_ID] = l_info;
              layerNames.push(nodes[i].configuration.jsonLayerDesc.Extension.LAYER_ID);
        			fieldsList.push(decodeURIComponent((nodes[i].configuration.jsonLayerDesc.Extension.layerSettings_infoFields).replace(/\+/g, " ")));
        			briefFieldsList.push(decodeURIComponent((nodes[i].configuration.jsonLayerDesc.Extension.layerSettings_briefFields).replace(/\+/g, " ")));
        			baseQueryURLList.push(nodes[i].configuration.jsonLayerDesc.Extension.layerSettings_queryURL);
        		}
        	}
        };
        var fieldsDesc = new Array();
        for (var i = 0; i < fieldsList.length; i++) {
            var list = Descartes.Utils.chompAndSplit(fieldsList[i], ';');
            fieldsDesc[i] = new Array();
            for (var j = 0; j < list.length; j++) {
                var desc = Descartes.Utils.decodeDescartesFieldDesc(list[j], baseQueryURLList[i]);
                fieldsDesc[i][j] = desc;
            }
        }
        var briefFieldsDesc = new Array();
        for (var i = 0; i < briefFieldsList.length; i++) {
            var list = Descartes.Utils.chompAndSplit(briefFieldsList[i], ';');
            briefFieldsDesc[i] = new Array();
            for (var j = 0; j < list.length; j++) {
                var desc = Descartes.Utils.decodeDescartesFieldDesc(list[j], baseQueryURLList[i]);
                briefFieldsDesc[i][j] = desc;
            }
        }
        // linking kept info fields and brief fields
        // to the corresponding layer node in the tree
        for (var i = 0; i < nodes.length; i++) {
        	if(nodes[i].configuration){
        		if(nodes[i].configuration.attributes.infoFieldsDesc && nodes[i].configuration.attributes.briefFieldsDesc){
        			nodes[i].configuration.attributes.infoFieldsDesc = fieldsDesc[i];
        			nodes[i].configuration.attributes.briefFieldsDesc = briefFieldsDesc[i];
        		}
        	}
        };
        var shape ="";
        var params = {
            layers: Ext.util.JSON.encode(layerNames),
            layerInfo: Ext.util.JSON.encode(layerInfo),
            fields: Ext.util.JSON.encode(fieldsDesc),
            briefFields: Ext.util.JSON.encode(briefFieldsDesc),
            shape: shape,
            map: this.mapfile,
            showGraphicalSelection: this.showGraphicalSelection
        };
        
        if (this.querytype!=null){
        	params.shape = Descartes.Utils.circleToStr(obj);
            params.shapeType = "circle";
        }
        else {
        	params.shape = Descartes.Utils.positionToStr(Descartes.Utils.positionToBounds(obj, this.OL_map));
        }
        return params;
    },

    getLayersNameTitle: function (nodes) {
        var arrayLayerNamesTitle = [];

        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].configuration.jsonLayerDesc.Extension.layerSettings_infoFields) {
                arrayLayerNamesTitle[nodes[i].configuration.jsonLayerDesc.Title] = decodeURI(nodes[i].configuration.jsonLayerDesc.Extension.LAYER_TITLE);
            }
        };
        return arrayLayerNamesTitle;
    },

    fieldFilter_URL: function (field) {
        return field.type == 'URL';
    },

    fieldFilter_TXT: function (field) {
        return field.type == 'TXT';
    },

    highlightSelectedFeatures: function () {
    	if ( !this.OL_map.getLayersByName(this.vectorLayer.name).length ){
        this.OL_map.addLayer(this.vectorLayer);
        this.OL_map.setLayerIndex(this.vectorLayer, 10000);
    	}
    	var bounds = this.model.bounds;
    	if (bounds) {
        	Descartes.Utils.zoomToSuitableExtent(this.OL_map, bounds);
    	}
    	var geometries = this.model.geometries;
    	this.vectorLayer.destroyFeatures();
    	if (geometries && geometries.length) {
    		var wktFormat = new OpenLayers.Format.WKT();
    		for(var i=0; i<geometries.length; i++) {
    			var wkt = geometries[i];
    			var features = wktFormat.read(wkt);
	    		if(features) {
	                if(features.constructor != Array) {
	                    features = [features];
	                }
	                this.vectorLayer.addFeatures(features);
	            } else {
//	                console.log('Bad WKT');
	            }
    		}
//    		console.log({"highlightSelectedFeatures non implémenté":geometries});
    	}
    },

    getGFIResAndDraw: function (){
    	if(hasGFI){
         	this.jsonobject2 = Ext.util.JSON.decode(this.getFInfoRep.responseText);
        }
		this.renderer.draw(this.jsonobject, this.ajaxParameters, this.OL_map, this.jsonobject2);
    },
    
    onsuccess: function (json) {
    	this.waitingWindow && this.waitingWindow.hide();
        this.jsonobject = Ext.util.JSON.decode(json.responseText); //Retour ajax transform� en objet js
        
        //Debut diff avec brgm
		if(this.jsonobject.iQueryMaxRepReached){
			this.renderer.drawAlert("iQueryMaxRepReached",this.jsonobject.iQueryMaxRepReached, this.jsonobject.I_QUERY_MAX_REP);
		}else{//fin diff avec brgm
			
		var totalCount = this.jsonobject.totalCount;
            // ajout du nom de la couche en plus du nom syst�me (n�cessaire pour l'affichage)
            var keysValuesTitleName = this.layerTreeParams; //Cl�s/valeurs pour rajouter le titre.
            for (key in this.jsonobject.results) {
                for (keyTn in keysValuesTitleName) {
                    if (keyTn === key) {
                        this.jsonobject.results[key].title = keysValuesTitleName[keyTn];
                    }
                };
            };
            
           this.getGFIResAndDraw();
            
        //Ne s'occuper de remplir les �lements pour le buffer que si activ�
        if (Descartes.Action.ProdigeBuffer.ACTIVATED == 1){
            if (totalCount > 0){
                //Parametre partag� pour g�rer l'activation du buffer
                Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED = 1;
                
                //Filtrer Descartes.Action.ProdigeBuffer.COMBO_LAYERS :
                // on peut interroger une carte mais si le retour est absent pour une des couches, erreur
                var resultLayers = [];
                for (layer in this.jsonobject.results){
                    if (/^layer/.test(layer)){
                        resultLayers.push(layer);
                    }
                }
                var keysValuesTitleNameFiltered = [];
                for (key in keysValuesTitleName){
                    if (typeof(keysValuesTitleName[key])=='string' && Descartes.Utils.in_array(key, resultLayers)){
                        keysValuesTitleNameFiltered[key] = keysValuesTitleName[key];                
                    }
                }
                Descartes.Action.ProdigeBuffer.COMBO_LAYERS = keysValuesTitleNameFiltered;
                var gConfigBufferLayers ={};
                var newParams;
                var j = 0;
    
                for (key in Descartes.Action.ProdigeBuffer.COMBO_LAYERS){
                    if (typeof(Descartes.Action.ProdigeBuffer.COMBO_LAYERS[key])=='string'){
                        var layerIndex = Ext.decode(this.ajaxParameters.layers).indexOf(key);
                        newParams = this.renderer.buildGridConfig(this.ajaxParameters.fields, this.jsonobject, layerIndex, key ,null, 0);
                        
                        //Clean extent column
                        for (var i = 0; i < newParams.columnModel.columns.length; i++){
                           if (/^extent_/.test(newParams.columnModel.columns[i].id)){
                               delete newParams.columnModel.columns[i];
                               delete newParams.columnModel.config[i];
                           }
                        }
                                            
                        var tmpArrayCmCol = Descartes.Utils.removeUndefinedFromObject(newParams.columnModel.columns);
                        var tmpArrayCmConfig = Descartes.Utils.removeUndefinedFromObject(newParams.columnModel.config);
    
                        newParams.columnModel.columns = tmpArrayCmCol;
                        newParams.columnModel.config = tmpArrayCmConfig;
                                                
                        gConfigBufferLayers[key] = newParams;
                        //j++;
                    }
                }
                Descartes.Action.ProdigeBuffer.CONFIG_LAYERS = gConfigBufferLayers;

                //Empty store from buffer
                if (Ext.getCmp('window_ProdigeBufferInPlaceExtJS0')){
                    var combo = Ext.getCmp('comboLayer_ProdigeBufferInPlaceExtJS0');
                    combo.setValue(null);
                    var grid = Ext.getCmp('gridSelection_ProdigeBufferInPlaceExtJS0');
                    grid.disable();
                    grid.hide();
                    Ext.getCmp('window_ProdigeBufferInPlaceExtJS0').setVisible(false);
              }

            }
            else {
                Descartes.Action.ProdigeBuffer.SELECTION_ACTIVATED = 0;
            }
        }
    }//A supprimer car diff avec brgm
	}
});

// Constant used to distinguish request type
Descartes.Action.CarmenRequest.SRC_MS = 0;
Descartes.Action.CarmenRequest.SRC_WMS = 1;
Descartes.Action.CarmenRequest.PUNCTUAL_RADIUS = 100.0;
Descartes.Action.CarmenRequest.noResultsResponse = {
    "totalCount": 0,
    "results": []
};

var olMapTest;
var vectorLayerTest;