/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.ModalDialogExtJS4
 * Classe simulant une boite de dialogue modale.
 * 
 * Ev�nements d�clench�s:
 * done - Fermeture de la boite de dialogue avec validation des saisies.
 * cancelled - Fermeture de la boite de dialogue avec annulation des saisies.
 */
Descartes.ModalDialogExtJS4 = OpenLayers.Class({
	
    /**
     * Propriete: defaultLabels
     * {Object} Objet JSON stockant les libell�s "communs" par d�faut.
     * 
     * :
     * title - Titre de la boite de dialogue (null par d�faut).
     * ok - Libell� du bouton fermant la boite de dialogue en validant les saisies ("Oui" par d�faut).
     * cancel - Libell� du bouton fermant la boite de dialogue en annulant les saisies ("Non" par d�faut). 
     */
    defaultLabels: {
    	title: null,
    	ok: "Oui",
    	cancel: "Non"
    },
    
    /**
     * Propriete: labels
     * {Object} Objet JSON stockant les libell�s "communs".
     * 
     * :
     * Construit � partir de defaultLabels, puis surcharg� par le constructeur.
     */
    labels: null,
    
    /**
     * Propriete : elementContent
     * {DOMElement} El�ment DOM contenant le corps de la boite de dialogue.
     */
    elementContent: null,
    
    EVENT_TYPES: ["done", "cancelled"],

	/**
	 * Constructeur: Descartes.ModalDialogExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * elementContent - {DOMElement} El�ment DOM contenant le corps de la boite de dialogue.
	 * labels - {Object} Objet JSON stockant les libell�s "communs".
	 */
	initialize: function(elementContent, labels) {
		this.elementContent = elementContent;
		this.labels = OpenLayers.Util.extend({}, this.defaultLabels);
		if((labels !== undefined)){
			OpenLayers.Util.extend(this.labels, labels);
		}
		
		var self = this;
		this.events = new OpenLayers.Events(this, null, this.EVENT_TYPES);
	},
	
	/**
	 * Methode: open
	 * Ouvre la boite de dialogue.
	 */
	open: function() {
		var self = this;

		this.title = '';
		if (this.labels.title !== null) {
			this.title = this.labels.title;
		}
		
		try{
			var w = new Ext.Window({
				id     		: 'content-popup',
				autoScroll  : true,
		        modal  		: true,
		        draggable   : false,
				resizable	: false,
		        title       : self.title,
		        contentEl   : self.elementContent,
			    buttons     : [{
			        text    : self.labels.ok,
			        handler : function() {
		        		self.done();	        		
			        }
			    },{
		        	text    : self.labels.cancel,
		        	handler : function(){
		        		self.events.triggerEvent("cancelled");}
		        }],
		        buttonAlign : 'center',
		        closable 	: false,
		        renderTo	: document.body
			});
			
			if(Ext.getCmp('content-popup').height){
				Ext.getCmp('content-popup').height = undefined;
				Ext.getCmp('content-popup').setHeight(Ext.getCmp('content-popup').height);
			}
			if(Ext.getCmp('content-popup').width){
				Ext.getCmp('content-popup').width = undefined;
				Ext.getCmp('content-popup').setWidth(Ext.getCmp('content-popup').width);
			}

			w.show();
			w.center();
			
		}catch(err) {
			  txt="Une erreur a �t� rencontr�e.\n\n";
			  txt+="Description: " + err + "\n\n";
			  alert(txt);
		}
	},
	
	/**
	 * Methode: done
	 * D�clenche l'�v�nement 'done'.
	 */
	done: function() {
		this.events.triggerEvent("done");
	},
	
	/**
	 * Methode: destroy
	 * D�truit la boite de dialogue.
	 */
    destroy: function() {
    	try{
    		Ext.getCmp('content-popup').destroy();
    	}catch(err) {
  		  txt="Une erreur a �t� rencontr�e.\n\n";
  		  txt+="Description: " + err + "\n\n";
  		  alert(txt);
  		}
    },
    
	CLASS_NAME: "Descartes.ModalDialogExtJS4",

	VERSION : "3.2"
});
