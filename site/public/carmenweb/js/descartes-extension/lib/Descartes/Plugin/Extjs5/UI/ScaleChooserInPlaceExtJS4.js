/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.ScaleChooserInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, la saisie d'une �chelle pour recadrer la carte.
 * 
 * :
 * Le formulaire de saisie est constitu� d'une seule zone de texte.
 * 
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS4>
 * 
 * 
 * Ev�nements d�clench�s:
 * choosed - L'�chelle est saisie et valide.
 */
Descartes.UI.ScaleChooserInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {

	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 */
	label: true,
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
	height:105,
	
	/**
	 * Propriete: msgTarget
	 * {String} Mode d'affichage des messages de erreurs.
	 */
	msgTarget:'side',
		
	/**
	 * Propriete: minScaleDenominator
	 * {Float} D�nominateur de l'�chelle minimale autoris�e.
	 */
	minScaleDenominator: null,

	/**
	 * Propriete: maxScaleDenominator
	 * {Float} D�nominateur de l'�chelle maximale autoris�e.
	 */
	maxScaleDenominator: null,

    EVENT_TYPES: ["choosed"],

	/**
	 * Constructeur: Descartes.UI.ScaleChooserInPlaceExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
	 * scaleModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.ScaleChooser>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 * msgTarget - {String} Mode d'affichage des messages d'erreurs.	 
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
	 */
	initialize: function(div, scaleModel, options) {
		Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, scaleModel, this.EVENT_TYPES, options]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone de la page HTML pour la saisie de l'�chelle.
	 * 
	 * Param�tres:
	 * minScaleDenominator - {Float} D�nominateur de l'�chelle minimale autoris�e.
	 * maxScaleDenominator - {Float} D�nominateur de l'�chelle maximale autoris�e.
	 */
	draw: function(minScaleDenominator, maxScaleDenominator) {
		this.minScaleDenominator = minScaleDenominator;
		this.maxScaleDenominator = maxScaleDenominator;
		var self = this;
		
		var title = '';
		
		if (this.label){
			title = this.getMessage("TITLE_MESSAGE");
		}
		
		var optsPanel = {
	        xtype        : 'form',
	        renderTo     : this.div.id,
	        autoScroll   : true,
	        id           : 'formPanelScaleChooser',
	        frame        : true,
	        title        : title,
	        pollForChanges: true,
	        items        : [
	            {
	                fieldLabel : this.getMessage("INVITE_MESSAGE"),
			        xtype      : 'numberfield',
	                id         : 'choosedScale',
	                minValue   : 1,
	                allowBlank : false,
	                msgTarget  : this.msgTarget,
            		blankText  : this.getMessage("FORMAT_MESSAGE"),
            		nanText    : this.getMessage("FORMAT_MESSAGE"),
            		minText    : this.getMessage("FORMAT_MESSAGE")
	            }
	        ],
		    buttons      : [{
		        text     : this.getMessage("BUTTON_MESSAGE"),
		        formBind : true,
		        handler  : function() {
	        		self.done();	        		
		        }
		    }],
		    buttonAlign  : 'center'
	    };
		
		this.formPanel =  new Ext.FormPanel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));

	    this.formPanel.show();
	    
	    if(!(this.label && this.formPanel.collapsible && this.formPanel.collapsed)){
        	this.modifyStyle();
        }
	},
	
 	modifyStyle: function() {
 		var h = this.height;
 		if(!this.label){
 			h -= 25;
 		}
 		this.formPanel.getEl().dom.style.height=h+"px";
	},
	
    /**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'choosed', ou affiche les messages d'erreur rencontr�s.
     */
	done: function() {
		var newScaleDenominator = parseFloat(this.formPanel.getForm().findField('choosedScale').getValue());
		if ( newScaleDenominator >= this.minScaleDenominator && newScaleDenominator <= this.maxScaleDenominator) {
			this.model.scale = newScaleDenominator;
			this.events.triggerEvent("choosed");
		} else {
			alert(this.getMessage("RANGE_PART_ONE_MESSAGE") + Descartes.Utils.readableScale(this.minScaleDenominator) + this.getMessage("RANGE_PART_TWO_MESSAGE") + Descartes.Utils.readableScale(this.maxScaleDenominator));
		}
	},
	
	CLASS_NAME: "Descartes.UI.ScaleChooserInPlaceExtJS4",

	VERSION : "3.2"
});
