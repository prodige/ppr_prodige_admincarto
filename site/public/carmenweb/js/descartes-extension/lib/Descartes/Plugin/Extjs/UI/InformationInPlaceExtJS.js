/**
 * Class: Descartes.UI.InformationInPlaceExtJS
 * Classe proposant
 * 
 * 
 * 
 * 
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * selected - Une échelle est sélectionnée dans la liste.
 */
Descartes.UI.InformationInPlaceExtJS = OpenLayers.Class(Descartes.UI, {

    defaultDisplayClasses: {
        textClassName: "DescartesUI",
        selectClassName: "DescartesUI"
    },

    /**
     * Propriete: label
     * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de choix.
     */
    label: true,
    
    jsonobject: null,
    
    indexui: null,

    /**
     * Propriete: ascending
     * {Boolean} Indicateur pour l'ordre d'affichage des �chelles disponibles.
     */
    ascending: false,

    EVENT_TYPES: ["highlightSelectedFeatures", "closed"],

    /**
     * Constructeur: Descartes.UI.ScaleSelectorInPlace
     * Constructeur d'instances
     * 
     * Paramètres:
     * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
     * informationModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.ScaleSelector>.
     * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
     * 
     * Options de construction propres � la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     * ascending - {Boolean} Indicateur pour l'ordre d'affichage des �chelles disponibles.
     */
    initialize: function (div, informationModel, options) {
    	Descartes.UI.InformationInPlaceExtJS.index++;
    	this.indexui = Descartes.UI.InformationInPlaceExtJS.index;
    	
        Descartes.UI.prototype.initialize.apply(this, [div, informationModel, this.EVENT_TYPES, options]);
    },
    
    highlightSelectedFeatures : function(bounds, geometries) {
    	this.model.geometries = geometries;
    	this.model.bounds = bounds;
    	this.events.triggerEvent("highlightSelectedFeatures");
    },
    
    /**
	 * drawAlert permet de renvoyer un message d'erreur si le nombre de resultat est trop eleve.
	 */
	drawAlert: function(errorContext, totalCount, limit){
		var msg = "";

		switch (errorContext) 
		{ 
		case "iQueryMaxRepReached": 
			msg = 'La requête renvoie trop de résultats. <br/> La limite est de ' + limit + ' résultats.<br/> Veuillez intérroger une plus petite surface.';
			break; 
		case "noQueryableLayer": 
			msg = 'Aucune couche interrogeable';
			break; 
		default: 

			break; 
		}	
	
	this.win_error = new Ext.Window({
	            id: 'InformationInPlaceExtJS_window_alert_' + this.indexui,
	            layout: 'anchor',
	            width: 400,
	            height: 150,
	            plain: false,
	            title: this.getMessage('DIALOG_TITLE'),
	            modal: false,
	            autoDestroy: true,
	            resizable: true,
	            collapsible : true,
	            closeAction: 'hide',
	            shadow: false,
				html: msg,
	            buttons : [{
					id: 'InformationInPlaceExtJS_window_btnFermer_alert_' + this.indexui,
					text: this.getMessage("CLOSE_BUTTON"),
					listeners : {
						'click' : {
							fn : function() {
								this.win_error.hide();
							},
							scope : this
						} 
					}
				}]
	           
	        });
	this.win_error.doLayout();
    this.win_error.show();
	},
	
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour le choix de l'�chelle.
     * 
     * Paramètres:
     * jsonobject - Json retourné suite à la requête ajax
     * ajaxparams - params ajax envoyés par l'action
     * OL_map - objet map Openlayers
     */
    draw: function (jsonobject, ajaxparams, OL_map, jsonobject2) {
        this.initializeUIComponents();
        this.showInfoWindow(jsonobject, ajaxparams, OL_map, jsonobject2);
        
    	this.highlightSelectedFeatures(null, this.allGeometries);
        
    	if( jsonobject != null ){
    		var currentExtent = Descartes.Utils.strExtentToOlBounds(jsonobject.extent, Descartes.Action.CarmenRequest.PUNCTUAL_RADIUS);
            this.fullExtent = currentExtent;
    	}else{
    		this.fullExtent = null;
    	}

    },
    urlInfoNoURL : function() {
	    Ext.Msg.show({
		    title:'Pas de champs avec des URL',
		    msg: 'Il n\'existe aucun champ avec une URL informative',
		    buttons: Ext.Msg.OK
	    });
    },
    
    hide : function() {
    	if (this.win) {
    		this.win.hide();
    	}
    },
    
    //Initialisation des variables pour l'affichage des informations attributaires
    win : null,
    win_2 : null,
	win_error : null,//variable utilisee lors de l'affichage du message d'erreur dans drawAlert()
    headerPanel : null,
    layerTreeInfoPanel : null,
    gridInfoPanel : null,
    gridInfoPanel_2 : null,
    windowTitle : 'Information',
    currentLayer : "",
    premierAffichage : true,

    /**
     * Methode: initializeUIComponents
     * Permet de cr�er les objets ext n�cessaires pour afficher les informations attributaires
     * 
     * Paramètres:
     * 
     */
    
    initializeUIComponents: function () {
    	
    	if (!this.win) {
    	
    		// fenêtre information globale
	        this.win = new Ext.Window({
	            id: 'InformationInPlaceExtJS_window_' + this.indexui,
	            layout: 'anchor',
	            width: 400,
	            height: 400,
	            plain: true,
	            title: this.getMessage('DIALOG_TITLE'),
	            modal: false,
	            autoDestroy: true,
	            resizable: true,
	            collapsible : true,
	            closeAction: 'hide',
	            shadow: false,
	            buttons : [{
					id: 'InformationInPlaceExtJS_window_btnExporter_' + this.indexui,
					text: this.getMessage("BTN_EXPORTER"),
					disabled : true,
					listeners : {
						'click' : {
							fn : function() {								
								Ext.ux.GridExporter.exporter(this.gridInfoPanel, this.gridInfoFormPanel.getForm(), "CSV");
							},
							scope : this
						} 
					}
	            },{
					id: 'InformationInPlaceExtJS_window_btnImprimer_' + this.indexui,
					text: this.getMessage("BTN_IMPRIMER"),
					disabled : true,
					listeners : {
						'click' : {
							fn : function() {
								// pour le titre de l'impression
								this.gridInfoPanel.name = this.currentLayer;
								Ext.ux.GridPrinter.print(this.gridInfoPanel);
							},
							scope : this
						} 
					}
				},{
					id: 'InformationInPlaceExtJS_window_btnFermer_' + this.indexui,
					text: this.getMessage("CLOSE_BUTTON"),
					listeners : {
						'click' : {
							fn : function() {
								this.win.hide();
							},
							scope : this
						} 
					}
				}],
	           listeners: {
					'hide' : { 
						fn : function () {
							this.events.triggerEvent("closed");
							// on r�initilise le current layer
							this.currentLayer = "";
							// la fermeture de la fenêtre 1 entraine celle de la fenêtre 2
							this.win_2.hide();
						},
						scope : this
					} 
				}
	        });
	
			// fenêtre information globale --> partie haute
	        this.headerPanel = new Ext.Panel({
	            id: 'InformationInPlaceExtJS_headerPanel_' + this.indexui,
	            autoScroll: true,
	            fitToFrame: true,
	            iconCls: 'cmnInfoGridZoomIcon',
	            header: true,
	            title: this.getMessage('INTERROGATION'),
	            anchor: '100% 50%',
	            listeners: {
					'afterrender' : { 
						fn : function () {
							 this.headerPanel.header.on('click',
				                function() {
				                	this.highlightSelectedFeatures( this.fullExtent, this.allGeometries);
				                },
				                this
				            );
						},
						scope : this
					} 
				}
	        });
            
            // fenêtre information globale --> arbre
	        this.layerTreeInfoPanel = new Ext.tree.TreePanel({
	            id: 'InformationInPlaceExtJS_layerTreeInfoPanel_'+ this.indexui,
	            margins: '5 0 5 5',
	            useArrows: false,
	            animate: false,
	            border: false,
	            fitToFrame: true,
	            rootVisible: false,
	            loader: new Ext.tree.TreeLoader(),
	            root: {
	                text: 'root'
	            },
	            viewConfig: {
	                autoFill: true,
	                forceFit: false
	            },
	            // listener 
	            listeners: {
	            	// affiche la fenetre d'information detaille � partir d'un clique sur un icone du tree
            		click: OpenLayers.Function.bind(function(n) {
	            			if (Ext.getDom(arguments[1].getTarget()).hasClassName('x-tree-node-icon') == true) {

	            				var grid = this.gridInfoPanel;
	            				var rec = grid.store.getAt(n.attributes.index);
	            				// Si on est sur un seul objet
								if (rec) {
								    this.showDetails(rec, grid.getColumnModel());
								} else {
								//Si on cherche à zoomer sur une sélection d'une couche								    
								    if (this.model.geometries.length > 0) {
								    	//Recupération de l'étendue
									    var geoms = new OpenLayers.Geometry();
									    var allGeoms = this.model.geometries;
									    var collection = [];
									    for (var i =0, length = allGeoms.length; i < length; i++) {
	                                        collection.push(OpenLayers.Geometry.fromWKT(allGeoms[i]));
										}
									    var olCollection = new OpenLayers.Geometry.Collection(collection);
									    var bounds = olCollection.getBounds();

                                        
								    	this.highlightSelectedFeatures(bounds, this.model.geometries);
								    }
								}
	            			}
            			}, this)
            	}
	        });
	        
	        // fenêtre information globale --> grid résultat
	        this.gridInfoPanel = new Ext.grid.GridPanel({
	            id: 'InformationInPlaceExtJS_gridInfoPanel_' + this.indexui,
	            store: new Ext.data.SimpleStore({
	                fields: [],
	                data: []
	            }),
	            columns: [],
	            title: 'R&eacute;sultats s&eacute;lectionn&eacute;s',
	            hidden: true,
	            autoScroll: true,
	            collapsible: true,
	            border: false,
	            anchor: '100% 50%'
	        });
	        
	        // fenêtre information détaillé
	        this.win_2 = new Ext.Window({
	            id: 'InformationInPlaceExtJS_window_detail_' + this.indexui,
	            layout: 'anchor',
	            width: 400,
	            height: 400,
	            plain: true,
	            title: "A DEFINIR",
	            modal: false,
	            autoDestroy: true,
	            resizable: true,
	            collapsible : true,
	            expandOnShow : true,
	            closeAction: 'hide',
	            shadow: false,
	            // BOUTONS
	            buttons : [{
					id: 'InformationInPlaceExtJS_window2_btnExporter_' + this.indexui,
					text: this.getMessage("BTN_EXPORTER"),
					listeners : {
						'click' : {
							fn : function() {
								Ext.ux.GridExporter.exporter(this.gridInfoPanel_2, this.gridInfoFormPanel_2.getForm(), "CSV");
							},
							scope : this
						} 
					}
				},{
					id: 'InformationInPlaceExtJS_window2_btnImprimer_' + this.indexui,
					text: this.getMessage("BTN_IMPRIMER"),
					listeners : {
						'click' : {
							fn : function() {
								Ext.ux.GridPrinter.print(this.gridInfoPanel_2);
							},
							scope : this
						} 
					}
				},{
					id: 'InformationInPlaceExtJS_window2_btnFermerDetail_' + this.indexui,
					text: this.getMessage("CLOSE_BUTTON"),
					listeners : {
						'click' : {
							fn : function() {
								this.win_2.hide();
							},
							scope : this
						} 
					}
				}]
	        });
	        
	       	// fenêtre information détaillé  --> grid résultat
	        this.gridInfoPanel_2 = new Ext.grid.GridPanel({
	            id: 'InformationInPlaceExtJS_gridInfoPanel_detail_' + this.indexui,
	            store: new Ext.data.SimpleStore({
	                fields: [],
	                data: []
	            }),
	            columns: [],
	            hidden: false,
	            autoScroll: true,

	            border: false,
	            anchor: '100% 100%',
	            view : new Ext.grid.GridView({forceFit : true})
	        });
     	
	     	// fenetre information global  --> form panel
	     	this.gridInfoFormPanel = new Ext.form.FormPanel({
				id: 'InformationInPlaceExtJS_gridInfoFormPanel_' + this.indexui,
				hidden : true,
				fileUpload : true,
				waitTitle : "Traitement en cours..."
			});
			
			// fenetre information detaillee  --> form panel
	     	this.gridInfoFormPanel_2 = new Ext.form.FormPanel({
				id: 'InformationInPlaceExtJS_gridInfoFormPanel_detail_' + this.indexui,
				hidden : true,
				fileUpload : true,
				waitTitle : "Traitement en cours..."
			});
			
			//ajoute l'interactivite sur le grid
	        this.gridInfoPanel.on('cellclick',
				function(grid, rowIndex, columnIndex, e){
					
					var rec = grid.store.getAt(rowIndex);
					var extentStr = rec.get('extent');
					var bounds = Descartes.Utils.strExtentToOlBounds(extentStr, Descartes.Action.CarmenRequest.PUNCTUAL_RADIUS);
					var geometryWKT = rec.get('geometryWKT');
					
					// mise en évidence si on clique sur la loupe (columnIndex à 1)
					if (columnIndex === 1) {
						this.highlightSelectedFeatures(bounds, [geometryWKT]);
					}					

/*		Fonctionnalit� active sur le tree et non la grid, ref CCTP			
					// si clique sur l'icone loupe, affichage fenetre detail
					if (columnIndex == 1) {
						this.showDetails(rec, grid.getColumnModel());
					}
*/

				}, this);
        }
    },

    layerTree: null,

    /**
     * Methode: assignModelAndData
     * Crée les objets ext nécessaires pour remplir le grid lorsqu'on clique sur le TreePanel
     * (Ré)Assigne les informations et affiche
     * 
     * Paramètres:
     * layerName - Nom de la couche
     * panel - Nom du panel
     * ajaxparams - Param�tres envoy�s au json suite � l'action
     * jsonobject - Retour json suite � l'action
     * layerIndex - Index de la couche dans la liste de couches
     * queryType - Type de requete (préxistant, non utilisé pour le moment, défini à 0 lorsque demandé)
     * indexNode - index du noeud (pour selectionner la ligne dans le grid
     * OL_map - Objet map OpenLayers
     * layerbounds - Etendue de la couche pour la layer
     * 
     */
    assignModelAndData: function (layerName, panel, ajaxparams, jsonobject, layerIndex, queryType, nodeIndex, OL_map, layerbounds, geometries, gridTitle, layerTitle) {
                     
        // Si l'utilisateur à cliquer dans le même layer, on ne fait rien 
        if (this.currentLayer != layerTitle) {
        	
        	this.currentLayer = layerTitle;
        	
        	//Correction layerIndex qui ne matche plus quand on interroge deux
        	// couches mais qu'une seule contient des r�sultats (sinon d�calage entre
        	// nom colonnes et donn�es contenues) 
        	var layerslist = JSON.parse(ajaxparams.layers);
        	layerIndex = layerslist.indexOf(layerName);
        	
        	
	        //assigner à gConfig l'objet issu de buildGridConfig qui permet de remplir
	        //  les informations du cm (column model) et du store
	        var gConfig = this.buildGridConfig(ajaxparams.fields, jsonobject, layerIndex, layerName, null, queryType);
	
	        //Reassigner au panel le store et le cm
	        panel.reconfigure(gConfig.store, gConfig.columnModel);
	        
	        //Gère l'exception si on est sur la racine du tree
	        if (nodeIndex != 'n'){
	        	//permet de synchroniser le tree avec le grid sur la sélection des objets
		        this.gridInfoPanel.getView().focusRow(nodeIndex);
		        this.gridInfoPanel.getSelectionModel().selectRow(nodeIndex);
	        }
	        //this.highlightSelectedFeatures(layerbounds, geometries);
	        
	        this.gridInfoPanel.setTitle(gridTitle);
	        
	        //Afficher le panel si jamais activé
	        if (!panel.isVisible()) {
	            panel.show();
	            this.win.setSize(this.win.getSize().width, this.win.getSize().height + 1);
	        }
	        
	        // afficher les boutons d'export et d'impression
	        Ext.getCmp('InformationInPlaceExtJS_window_btnExporter_' + this.indexui).enable();
	        Ext.getCmp('InformationInPlaceExtJS_window_btnImprimer_' + this.indexui).enable();
        }
        this.highlightSelectedFeatures(null, geometries);
    },
    
    nodeIndex: null,
    fullExtent : null,

    /**
     * Methode: showInfoWindow
     * Affiche les informations apr�s le clic. Fonction principal apr�s initialisation des composants ExtJS
     * 
     * jsonobject - param�tres envoy�s pour la requ�te ajax suite � l'action 
     * ajaxparams - objet json contenant les infos envoy�es par ajax servant � l'action
     */     

    showInfoWindow: function (jsonobject, ajaxparams, OL_map, jsonobject2) {
                
        //Construction d'un objet arraylayers contenant l'arborescence des couches et la liste des objets s�lectionn�s
        var arraylayers = [];

        var queryType = 0;
        var k = 0;
        this.allGeometries = [];
        
        // pour chaque layer (ex : France
        if(jsonobject != null) {
        	
        
        for (var layerName in jsonobject.results) {
            var layerIndex = k;
            var gPanel = this.gridInfoPanel;

            //Fill the pan with details info
			var geometries = [];
            var subarray = [];
            if (jsonobject.results[layerName].data) {
                var idposition = jsonobject.results[layerName].briefFields.indexOf('fid');
                var extentposition = jsonobject.results[layerName].briefFields.indexOf('extent');
                var geometryWKTposition = jsonobject.results[layerName].briefFields.indexOf('geometryWKT');
                
                var layerBounds = Descartes.Utils.strExtentToOlBounds(jsonobject.results[layerName].extent, Descartes.Action.CarmenRequest.PUNCTUAL_RADIUS);
                
                var layerText = jsonobject.results[layerName].title + ' (' + this.getResulatText(jsonobject.results[layerName].briefData.length) + ')';
                var layerTitle = jsonobject.results[layerName].title;
                
                for (var j = 0; j < jsonobject.results[layerName].briefData.length; j++) {
                    var geometryWKT = jsonobject.results[layerName].briefData[j][geometryWKTposition];
                    geometries.push(geometryWKT);
                    this.allGeometries.push(geometryWKT);
                    
                    this.nodeIndex = j;
                    
					var rootResults = jsonobject.results[layerName];
					var briefFieldsLen = rootResults.briefFields.length;
					
					var textNodeObject = "";
					// Il y a plus d'1 identifiant
					if (briefFieldsLen > 4) {
						for (var i = 0; i < briefFieldsLen; i++) {
							if (!Descartes.Utils.in_array(rootResults.briefFields[i], ['fid', 'extent', 'geometryWKT'])){
								//Debut titre arbre sup
								if (i === 0) {
									//Si type url, afficher l'url
									if (rootResults.briefData[j][i].text !== undefined) {
										textNodeObject = decodeURIComponent(rootResults.briefData[j][i].text) + " (";
									}
									else {
										textNodeObject = rootResults.briefData[j][i] + " (";
									}
									
									
								}
								else {
									//Si type url, afficher l'url
									if (rootResults.briefData[j][i].text !== undefined) {
										textNodeObject = textNodeObject + rootResults.briefFields[i]+ ': ' + decodeURIComponent(rootResults.briefData[j][i].text) + ', ';
									}
									else {
										textNodeObject = textNodeObject + rootResults.briefFields[i]+ ': ' + rootResults.briefData[j][i] + ', ';
									}									
								}
						    }
						}
						textNodeObject = textNodeObject.substr(0, textNodeObject.length - 2) + ')';
					//	console.log(textNodeObject);
					}
					else {
						//Si type url, afficher l'url
						if (rootResults.briefData[j][0].text !== undefined){
	                         textNodeObject = decodeURIComponent(rootResults.briefData[j][0].text);
                        }
                        else {
                        	textNodeObject = rootResults.briefData[j][0];
                        }
						
					//	console.log(textNodeObject);
					}
                    
                    // construction de la liste des fils
                    subarray.push({
                        'text': textNodeObject,
                        'index': j,
                        'cls': 'file',
                        'leaf': true,
                        'children': [],
                        listeners: {
                            click: OpenLayers.Function.bind(this.assignModelAndData, this, layerName, gPanel, ajaxparams, jsonobject, layerIndex, queryType, this.nodeIndex, OL_map, layerBounds, [geometryWKT], layerText, layerTitle)
                        }
                    });
                };
                
                //Gère l'affichage des libellés en fonction du nombre
//                var pluriel;
//                (jsonobject.results[layerName].briefData.length > 1) ? pluriel = 's' : pluriel = '';

				var resulText = jsonobject.results[layerName].briefData.length > 1 ? this.getMessage('RESULTATS') : this.getMessage('RESULTAT');

                arraylayers.push({
                    'text': layerText,
                    'cls': 'folder',
                    'iconCls': 'cmnInfoGridZoomIcon',
                    'leaf': false,
                    'children': subarray,
                    listeners: {
                        click: OpenLayers.Function.bind(this.assignModelAndData, this, layerName, gPanel, ajaxparams, jsonobject, layerIndex, queryType, 'n', OL_map, layerBounds, geometries, layerText, layerTitle)
                    }
                });
            };

            k = k + 1;
        }
        };
        
        if(hasGFI){
        if(!gPanel){
        	var gPanel = this.gridInfoPanel;
        }
//      	TODO inserer les données du getFeatureInfo
        	for(var idx = 0; idx < jsonobject2.length; idx++){
        		var textResult = jsonobject2[idx].layerDatas.length > 1 ? this.getMessage('RESULTATS') : this.getMessage('RESULTAT');
        		var infoText = jsonobject2[idx].layerTitle+" ("+jsonobject2[idx].layerDatas.length+" "+textResult+")"
        		arraylayers.push({
        			'text': infoText,
//      			'cls': 'folder',
        			'iconCls': 'cmnInfoGridZoomIcon',
        			'leaf': true,
        			'childen': [],
        			listeners :{
        				click: OpenLayers.Function.bind(this.showGetFInfoRes, this, gPanel, jsonobject2, idx)
        			}

        		});
        	}
        }
        
        //Création d'un Ext.tree.AsyncTreeNode nécessaire pour remplir le
        // Ext.tree.TreePanel layerTreeInfoPanel
        var informationRoot = new Ext.tree.AsyncTreeNode({
            text: 'root',
            //texte du noeud
            draggable: false,
            // Désactiver le Drag and drop sur ce noeud
            id: '1_' + this.indexui,
            // identifiant du noeud
            children: arraylayers //Objet JSON contenant la structure de l�arbre
        });
        
        if (!this.win.rendered) {
        	
        	// ajout des composants du la fenêtre détaillé
            this.headerPanel.add(this.layerTreeInfoPanel);
            this.win.add(this.headerPanel);
            this.layerTreeInfoPanel.setRootNode(informationRoot);
            this.win.add(this.gridInfoPanel);
            this.win.add(this.gridInfoFormPanel);

			// recalcul du layout
            this.win.doLayout();
            this.win.show();

        } else {
        	//this.win.hide();
            this.gridInfoPanel.hide();
            // add the new one and render it            
            this.layerTreeInfoPanel.setRootNode(informationRoot);
            this.layerTreeInfoPanel.render();
        }
        
        // set title
//      ajouter le nombre de resultats pour le getfeatureinfo
        var jsonObjTotalCount = 0;
    	if(jsonobject != null ){
    		jsonObjTotalCount = jsonobject.totalCount
    	}
    	
    	var newTotalCount = 0;
        if(hasGFI){
        	newTotalCount = parseInt(jsonObjTotalCount)+jsonobject2.length;
        }else{
        	newTotalCount = jsonObjTotalCount;
        }
        
        
        this.headerPanel.setTitle(this.getResulatText(newTotalCount.toString()));
        
        if (!this.win.isVisible()) {
        	//this.win.doLayout();
            this.win.show();
        }
        
    	// cache les boutons d'export et d'impression
        Ext.getCmp('InformationInPlaceExtJS_window_btnExporter_' + this.indexui).disable();
        Ext.getCmp('InformationInPlaceExtJS_window_btnImprimer_' + this.indexui).disable();
    },
    
    /*
     * Remplit et affiche la fenêtre avec les infos du getfeatureinfo
     * jsonobject2 : resultats du getfeatureinfo
     * index : index du lien cliqué
     */
    showGetFInfoRes : function (panel, jsonobject2, idx){
    	if (this.currentLayer != jsonobject2[idx].layerTitle){
    		this.currentLayer = jsonobject2[idx].layerTitle;
    		
    		
    		var fields = new Array();
    		var columns = new Array();
    		columns.push(new Ext.grid.RowNumberer()); 
    		
    		columns.push({
    			id: 'extent_' + this.indexui,
    			header: '',
    			width: 20,
    			resizable: true,
    			renderer: this.extentRenderer,
    			dataIndex: 'extent'
    			}); 
    		
    		var fDesc = {
    				name: "Resultat" + i.toString(),
    				type: Descartes.Utils.descartesTypeToExtType("TXT")
    		};
    		
    		fields.push(fDesc); 
    				
    		var colDesc = {
    				id: "Resultat" + i.toString() + '_' + this.indexui,
    				header: "Resultat",
    				width: 200,
    				sortable: true,
    				resizable: true,
    				renderer: Descartes.Utils.descartesTypeToExtRenderer("TXT"),
    				dataIndex: "Resultat" + i.toString()
    				};
    		
    		columns.push(colDesc); 
    		
    		if ( jsonobject2[idx].layerDatas.length > 0 ) {
    			var resString = jsonobject2[idx].layerDatas.toJSON();
        		resString = resString.replace(/\[{/g,'');
        		resString = resString.replace(/\}]/g,'');
        		resString = resString.replace(/\"/g,'');
        		
        		var tab1 = new Array()
        		tab1.push(resString);
        		
        		var resultGFI = new Array();
        		resultGFI.push(tab1);
        		
        		var config2 = {
        				store: new Ext.data.SimpleStore({
        					fields: fields,
        					data: resultGFI
        				}),
        				columnModel: new Ext.grid.ColumnModel({
        					columns: columns
        				})
        		}; 
        		panel.reconfigure(config2.store, config2.columnModel);
        		this.gridInfoPanel.setTitle(jsonobject2[idx].layerTitle);
        		
        		if (!panel.isVisible()) {
        			panel.show();
        		}
        		Ext.getCmp('InformationInPlaceExtJS_window_btnExporter_' + this.indexui).enable();
    	        Ext.getCmp('InformationInPlaceExtJS_window_btnImprimer_' + this.indexui).enable();
    	        
    	        
    		}else{
    			panel.hide();
    			Ext.getCmp('InformationInPlaceExtJS_window_btnExporter_' + this.indexui).disable();
    	        Ext.getCmp('InformationInPlaceExtJS_window_btnImprimer_' + this.indexui).disable();
    		}
    		
    		
    	}
    	
    },
    
    /*
     * Remplit et affiche la fenêtre d'info détail: Information
     * - rec : Record de l'index sélectionné
     * - clmModel :ColumnModel du grid "parent"
     */
    showDetails : function(rec, clmModel) {
    	
    	// titre de la fenêtre
    	this.win_2.setTitle(this.currentLayer);
                		
		// au premier affichage, on positionne la fenêtre détail par rapport à la première fenêtre
		if (this.premierAffichage) {
    		var tabPos = this.win.getPosition(true);
    		this.win_2.setPosition(tabPos[0] + 100, tabPos[1] + 50);
    		this.premierAffichage = false;
		}
		
		// construction du store - data
		var data = new Array();

		for (var i = 0; i < clmModel.getColumnCount(); i++) {
			if (clmModel.getColumnHeader(i)) {
				var value = rec.get(clmModel.getDataIndex(i));
				// application du renderer
				value = clmModel.getRenderer(i) ? clmModel.getRenderer(i)(value) : value;
    			data.push([ clmModel.getColumnHeader(i),value]);
			}
		}
		
        var store = new Ext.data.ArrayStore({
                fields: ['libelle', 'donnee'],
                data: data
            });
        
        // construction du store - columnModel 
        var columns = new Array(); 
        var colDesc1 = {
            id: 'libelle_' + this.indexui,
            header: this.getMessage('LBL_LIBELLE'),
            sortable: true,
            resizable: true,
            dataIndex: 'libelle'
            };
            
		columns.push(colDesc1);
		
		var colDesc2 = {
            id: 'donnee_' + this.indexui,
            header: this.getMessage('LBL_DONNEE'),
            sortable: true,
            resizable: true,
            dataIndex: 'donnee'
            };
            
		columns.push(colDesc2);

		var columnModel = new Ext.grid.ColumnModel({
				columns: columns
		});
	        
		// construction du grid 2
		this.gridInfoPanel_2.reconfigure(store, columnModel);
		// pour le titre de l'impression
		this.gridInfoPanel_2.name = this.currentLayer;
		 
		// affichage de la fenêtre 2    
		this.win_2.add(this.gridInfoPanel_2);
		this.win_2.add(this.gridInfoFormPanel_2);	
		this.win_2.doLayout();
		this.win_2.expand();
		this.win_2.show();
    	
    },
    
    getResulatText : function(size) {
    	return size + ' ' + (size > 1 ? this.getMessage('RESULTATS') : this.getMessage('RESULTAT'));
    },

    buildGridConfig: function (fieldsDesc, result, layerIndex, layerName, olLayer, queryType) {
        var fields = new Array();
        var columns = new Array();
        columns.push(new Ext.grid.RowNumberer());
        if (queryType == Descartes.Action.CarmenRequest.SRC_MS) {
            columns.push({
                id: 'extent_' + this.indexui,
                header: '',
                width: 20,
                resizable: true,
                renderer: this.extentRenderer,
                dataIndex: 'extent'
            });
        };
        // Adaptation de la variable de Carmen pour Descartes
        var fieldsDesc = Ext.util.JSON.decode(fieldsDesc)[layerIndex];

        for (var i = 0; i < fieldsDesc.length; i++) {
            var desc = fieldsDesc[i];
            var fDesc = {
                name: desc.name + i.toString(),
                type: Descartes.Utils.descartesTypeToExtType(desc.type)
            };
            switch (desc.type) {
            case 'date':
                fDesc['dateFormat'] = Descartes.Utils.descartesDateTypeToExtDateFormat(desc.type);
                break;
            default:
                break;
            }
            fields.push(fDesc);
            var colDesc = {
                id: desc.name + i.toString() + '_' + this.indexui,
                header: desc.alias,
                width: 200,
                sortable: true,
                resizable: true,
                renderer: Descartes.Utils.descartesTypeToExtRenderer(desc.type),
                dataIndex: desc.name + i.toString()
            };
            columns.push(colDesc);
        }
        if (queryType == Descartes.Action.CarmenRequest.SRC_MS) {
            fields.push({
                name: 'extent'
            });
            fields.push({
                name: 'fid'
            });
            fields.push({
                name: 'geometryWKT'
            });
        }
        fields.push({
            name: 'layerName'
        });

        // Adaptation de la variable de Carmen pour Descartes
        var result = result.results[layerName];
        for (var i = 0; i < result.data.length; i++) {
            result.data[i] = result.data[i].concat([layerName]);
        }
        var config = {
            store: new Ext.data.SimpleStore({
                fields: fields,
                data: result.data
            }),
            columnModel: new Ext.grid.ColumnModel({
                columns: columns
            })
        };
        return config;
    },
    extentRenderer: function (v) {
        return '<div class="cmnInfoGridZoomIcon"> </div>';
    },

    CLASS_NAME: "Descartes.UI.InformationInPlaceExtJS",

    VERSION: "3.1"
});

Descartes.UI.InformationInPlaceExtJS.index = 0;