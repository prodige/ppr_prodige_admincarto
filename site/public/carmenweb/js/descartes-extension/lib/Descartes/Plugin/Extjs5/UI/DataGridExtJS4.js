/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.DataGridExtJS4
 * Classe permettant d'afficher, sous forme de tableau, les propri�t�s d'un ensemble d'objets g�ographiques.
 *
 * H�rite de:
 * - <Descartes.UI>
 *
 * Ev�nements d�clench�s:
 * gotoFeatureBounds - La localisation rapide sur un objet est demand�e.
 */
Descartes.UI.DataGridExtJS4 = OpenLayers.Class(Descartes.UI, {
   
    /**
     * Private
     */
    colNames: [],

    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML du panneau de l�gendes.
     * 
     * :
	 * localizeHeaderClassName - ("DescartesDataGridLocalizeHeader" par d�faut)
     */
    defaultDisplayClasses: {
        localizeHeaderClassName: "DescartesDataGridLocalizeHeader"
    },
   
    /**
     * Propriete: acceptLocalization
     * {Boolean} Indicateur pour la pr�sence de bouton de localisation rapide sur chaque objet.
     */
    acceptLocalization: false,
   
    /**
     * Propriete : tabPanel
     * {Ext.TabPanel} Le conteneur des onglets.
     */
    tabPanel: null,
   
    /**
     * Private
     */
    EVENT_TYPES: ["gotoFeatureBounds"],

    /**
     * Constructeur: Descartes.UI.DataGridExtJS4
     * Constructeur d'instances
     *
     * Param�tres:
     * tabPanel - {Ext.TabPanel} Le conteneur des onglets.
     * div - {DOMElement|String} El�ment DOM de la page accueillant le tableau.
     * datasModel - {Object} Objet JSON contenant les propri�t�s des objets g�ographiques, de la forme {prop1:"val1", prop2:"val2", ...}[].
	 * 
	 * Options de construction propres � la classe:
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
	 * acceptLocalization - {Boolean} Indicateur pour la pr�sence de bouton de localisation rapide sur chaque objet.
     */
    initialize: function(tabPanel, div, datasModel) {
        this.tabPanel = tabPanel;
        this.colNames = [];
        if (datasModel[0] !== undefined) {
            for (var colName in datasModel[0]) {
                if (colName !== undefined) {
                    if (colName === "bounds" && datasModel[0].bounds instanceof Array) {
                        this.acceptLocalization = true;
                    } else {
                        this.colNames.push(colName);
                    }
                }
            }
        }
        Descartes.UI.prototype.initialize.apply(this, [div, datasModel, this.EVENT_TYPES]);
        this.draw();
    },
   
    /**
     * Methode: draw
     * Construit et affiche le tableau (ou le met � jour en cas de tri sur une colonne).
     */
    draw: function() {
       
        var self = this;
        var colModel = [];
        var columnNames = [];
       
        for (var iRow=0, lenRows=this.colNames.length ; iRow<lenRows ; iRow++) {           
            columnNames.push({name:this.colNames[iRow]});
            colModel.push(
                    {header    :this.colNames[iRow],
                     sortable  : true,
                     dataIndex :this.colNames[iRow],
                     renderer  : function(currentCellValue, metadata, record, rowIndex, colIndex, doctorOrderStore) {
                                    var formatedCellValue = currentCellValue;
                                    if (currentCellValue.indexOf("http://") === 0) {
                                        var message = self.getMessage("TITLE_OPEN_LINK");
                                        if (currentCellValue.indexOf(".") != -1) {
                                            var extension = currentCellValue.split(".")[currentCellValue.split(".").length -1].toLowerCase();
                                            if (extension == "jpg" || extension == "jpeg" || extension == "png" || extension == "gif") {
                                                message = "<img src=\"" + currentCellValue + "\" width=\"100\" />";
                                            }
                                            if (extension == "zip" || extension == "pdf" || extension == "odt" || extension == "ods" || extension == "odp") {
                                                message = self.getMessage("TITLE_DOWNLOAD_FILE") + " " + extension.toUpperCase();
                                            }
                                        }
                                        formatedCellValue = "<a href=\"" + currentCellValue + "\" target=\"_blank\">" + message + "</a>";
                                    } else if (currentCellValue.indexOf("mailto:") === 0) {
                                        formatedCellValue = "<a href=\"" + currentCellValue + "\" >" + self.getMessage("TITLE_MAIL_LINK") + "</a>";
                                    }
                                    return formatedCellValue;
                                 }
                    });
        }
       
        var data = [];
        OpenLayers.Util.extend(data, this.model);
       
        if(this.acceptLocalization){
            columnNames.unshift({name :"Localize"});
            colModel.unshift({header:"<span class='"+this.displayClasses.localizeHeaderClassName+"'></span>",sortable: false,
                                width: 30, fixed: true,
                              renderer: function(currentCellValue, metadata, record, rowIndex, colIndex, doctorOrderStore) {
                                            return '<span class="DescartesDataGridLocalizeCell"></span>';
                                         }
                             });
        }   
       
        var store = new Ext.data.SimpleStore({
	   		fields:columnNames
        });
        
        store.loadData(data);
       
        var index = this.tabPanel.id.replace("tabPanel_", "");
        var dataGridDescartesId= 'dataGridDescartes_'+index;
        try{
        	Ext.getCmp(dataGridDescartesId).destroy();
        } catch (e) {
        }
        
        var grid = new Ext.grid.GridPanel({
            id            : dataGridDescartesId,
            store        : store,
            columns           : colModel,
            selModel           : new Ext.selection.RowModel({ mode:'SINGLE' }),
            viewConfig: {stripeRows: true},
            enableHdMenu : false,
            autoScroll   : true,
            columnLines: true,
            listeners    : {
                cellclick  : function(grid, rowIndex, colIndex) {
                    if(self.acceptLocalization && colIndex === 0){
                        var selectionArray = Ext.getCmp(dataGridDescartesId).getSelectionModel().getSelection();
                        self.gotoBounds(selectionArray[0].data);   
                    }
                }
            }
        });
       
        this.tabPanel.getActiveTab().removeAll();
        this.tabPanel.getActiveTab().add(grid);
        this.tabPanel.getActiveTab().doLayout();
    },
   
    /**
     * Methode: gotoBounds
     * Demande la localisation rapide sur un objet, en d�clenchant l'�v�nement 'gotoFeatureBounds'.
     *
     * Param�tres:
     * data - {ObjectS} Donn�es de la ligne s�l�ctionn�e.
     */
    gotoBounds: function(data) {
        var bounds = data.bounds;
        this.events.triggerEvent("gotoFeatureBounds", bounds);
    },
   
    CLASS_NAME: "Descartes.UI.DataGridExtJS4",

    VERSION : "3.2"
});
