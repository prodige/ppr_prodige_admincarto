/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.Info.LegendExtJS4
 * Classe d�finissant une zone informative affichant un panneau de l�gendes.
 *
 * H�rite de:
 *  - <Descartes.Info>
 *
 * Ecouteurs mis en place:
 *  - l'�v�nement 'changed' de la classe <Descartes.MapContent> d�clenche la m�thode <update>.
 *  - l'�v�nement 'itemPropertyChanged' de la classe <Descartes.MapContent> d�clenche la m�thode <update>.
 */
Descartes.Info.LegendExtJS4 = OpenLayers.Class(Descartes.Info, {
       
    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML du panneau de l�gendes.
     *
     * :
     * separateurClassName - ("DescartesInfos" par d�faut)
     */
    defaultDisplayClasses: {
        separateurClassName: "DescartesInfos"
    },
   
    /**
     * Propriete: separateur
     * {Boolean} Indicateur pour la pr�sence d'une ligne s�paratrice entre chaque l�gende (true par d�faut).
     */
    separateur:true,
   
    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent:null,
   
	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de legende.
	 */
	label: true,
	
	/**
	 * Propriete: defaultOptionsPanel
	 * {Objet} Options par d�faut du panel.
	 * 
	 * :
	 * collapsible - ("false" par defaut)
	 * collapsed - ("false" par defaut)
	 * tools - ("null" par d�faut)
	 */
	defaultOptionsPanel:{
		collapsible:false,
		collapsed:false,
		tools:null
	},
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
   
    height:300,

    /**
     * Constructeur: Descartes.Info.LegendExtJS4
     * Constructeur d'instances
     *
     * Param�tres:
     * div - {DOMElement|String} El�ment DOM de la page accueillant la zone informative ou identifiant de cet �lement.
     * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
     *
     * Options de construction propres � la classe:
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
     * separateur - {Boolean} Indicateur pour la pr�sence d'une ligne s�paratrice entre chaque l�gende.
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de localisation.
     * optionsPanel - {Objet} Options du panel.

     */
    initialize: function(div, options) {
    	this.optionsPanel = OpenLayers.Util.extend({}, this.defaultOptionsPanel);
		
		if((options !== undefined && options.optionsPanel !== undefined)){
			this.optionsPanel = OpenLayers.Util.extend(this.optionsPanel, options.optionsPanel);
			delete options.optionsPanel;
		}
		
    	Descartes.Info.prototype.initialize.apply(this, [div, options]);
        this.initOptions();
	},
	
	/**
	 * Methode: initOptions
	 * Initialise les param�tres pour garder la coh�rence d'affichage
	 */
	initOptions:function(){
		if (!this.label){
			this.optionsPanel.collapsible=false;
			this.optionsPanel.collapsed=false;
			this.optionsPanel.tools=null;
		} else if (!this.optionsPanel.collapsible) {
			this.optionsPanel.collapsed=false;
		} else {
			//Probleme d'affiche quand utilisation de collapsible et tools en meme temps
			this.optionsPanel.tools=null;
		}
	},
   
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au panneau de l�gendes.
     *
     * Param�tres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function(mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register( 'changed', this, this.update);
        this.mapContent.events.register( 'itemPropertyChanged', this, this.update);
    },
   
     /**
     * Methode: update
     * Actualise le panneau de l�gendes en fonction du contexte
     */
    update: function() {
        var self = this;
        this.layersVisible = OpenLayers.Util.extend([],this.mapContent.getVisibleLayers());

        var divLegend = document.createElement('div');
        divLegend.id = 'legendeDescartes';
       
        var leseparateur = this.separateur;
       
        var className = this.displayClasses.separateurClassName;
       
        this.layersVisible.forEach(
            function (layers){
                if (layers.legend!==null){
                    layers.legend.forEach(
                            function (legend){
                                var element = document.createElement('div');
                                var image = document.createElement('img');
                                image.src = legend.replace(/&amp;/g,"&");
                                element.appendChild(image);
                                divLegend.appendChild(element);

                                if (leseparateur){
                                    var sep = document.createElement('hr');
                                    sep.className = className;
                                    divLegend.appendChild(sep);
                                }
                            }
                    );
                }
            }
        );   
       
        var title = '';
       
        if(this.label){
            title = this.getMessage("TITLE_MESSAGE");
        }
        
        if(Ext.getCmp('dPanelLegend')){
            Ext.getCmp('dPanelLegend').destroy();      
        }   
        
        var optsPanel = {
            xtype        : 'panel',
            id: 'dPanelLegend',
            renderTo     : this.div.id,
            frame        : true,
            title        : title,
            autoScroll   :true,
            contentEl     : divLegend
        };
        
        var panel =  new Ext.Panel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));

        if(!(this.label && panel.collapsible && panel.collapsed)){
        	this.modifyStyle();
        }

    },
    
 	modifyStyle: function() {
 		
        if(Ext.getCmp('dPanelLegend').getHeight()){
        	Ext.getCmp('dPanelLegend').height = this.height;
        	Ext.getCmp('dPanelLegend').setHeight(Ext.getCmp('dPanelLegend').height);
        }
        
        if(Ext.getCmp('dPanelLegend').getWidth()){
			Ext.getCmp('dPanelLegend').width = Ext.getCmp('dPanelLegend').width;
			Ext.getCmp('dPanelLegend').setWidth(Ext.getCmp('dPanelLegend').width);
		}
 		
	},
 
    CLASS_NAME: "Descartes.Info.LegendExtJS4",

    VERSION : "3.2"
});