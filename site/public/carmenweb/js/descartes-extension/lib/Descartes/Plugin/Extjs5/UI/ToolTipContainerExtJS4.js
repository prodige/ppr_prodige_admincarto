/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.ToolTipContainerExtJS4
 * Classe affichant dans une info-bulle les attributs des objets survol�s par le pointeur.
 * 
 * H�rite de:
 * - <Descartes.UI>
 */
Descartes.UI.ToolTipContainerExtJS4 = OpenLayers.Class(Descartes.UI, {

    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML du panneau de l�gendes.
     * 
     * :
	 * lienClassName - ("DescartesToolTipLien" par d�faut)
     */
	defaultDisplayClasses: {
		lienClassName: "DescartesToolTipLien"
	},
	
	EVENT_TYPES: [],
	
	/**
	 * Constructeur: Descartes.UI.ToolTipContainerExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant l'info-bulle.
	 * toolTipModel - null.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
	 */
	initialize: function(div, toolTipModel, options) {
		Descartes.UI.prototype.initialize.apply(this, [div, toolTipModel, this.EVENT_TYPES, options]);
		this.div.innerHTML = "";
		this.div.style.display='none';
	},

	/**
	 * Methode: draw
	 * Construit et affiche l'info-bulle.
	 * 
	 * Param�tres:
	 * jsonResult - {Object} Objet JSON contenant les objets survol�s avec leurs attributs.
	 */
	draw: function(jsonResult) {
		this.div.innerHTML="";
		var fieldsToolTip = [];

		if (jsonResult !== "") {
			this.div.style.display='block';
			
			var items = [];
			
			var display = false;
			
			for(var i=0, len=jsonResult.length; i<len;i++){
				
				var itemsField = [];
				for(var index=0, tlen=this.toolTipLayers.length; index<tlen;index++){
					if(this.toolTipLayers[index].layer.title === jsonResult[i].layerTitle){
						fieldsToolTip = this.toolTipLayers[index].fields;
						
						for(var fi=0, ftlen=fieldsToolTip.length; fi<ftlen;fi++){
							fieldsToolTip[fi] = fieldsToolTip[fi].replace(/_/g," "); 
						}
						
						for(var findex=0, flen=fieldsToolTip.length; findex<flen;findex++){
							try {
								var property = fieldsToolTip[findex];
								var value = jsonResult[i].layerDatas[0][property];

								display = true;
								itemsField.push({xtype : 'label', html : property+' : '+this.formatCell(value)})
								
							} catch (e) {
								// propriete absente ignoree
							}
						}
					}
				}
				if (itemsField.length > 0) {
					var item = {
								xtype : 'panel',
								layout:'table',
								layoutConfig: {
								    columns: 1
								},
								title : jsonResult[i].layerTitle,
								items : itemsField
					};
					items.push(item);
				}
			}

			if(display){
				var toolTipContainer = new Ext.Window({
				        draggable : false,
				        closable : false,
				        resizable : false,
				        width:150,
				        renderTo : this.div.id,
				        items  : items,
						x : (window.scrollX ? window.scrollX : 0) + this.pixel.x,
						y : (window.scrollY ? window.scrollY : 0) + this.pixel.y
				    });
				toolTipContainer.show();
			}
			var self = this;
			setTimeout(function() {self.div.style.display='none';}, 5000);
		} else {
			this.div.style.display='none';
		}
	}, 
	
	/**
	 * Methode: formatCell
	 * Formate une valeur d'attribut correspondant � une ressource HTTP.
	 * 
	 * : Dans le cas d'un lien correspondant � une image, outre l'insertion d'une balise <A>, l'image est directement affich�e.
	 * 
	 * Param�tres:
	 * cellValue - {String} La valeur de l'attribut.
	 * 
	 * Retour:
	 * {String} Une cha�ne comportant l'�ventuel lien HTTP (par d�faut, la cha�ne initiale est retourn�e).
	 */
	formatCell: function(cellValue) {
		var formatedCellValue = cellValue;
		if (cellValue.indexOf("http://") === 0) {
			var message = this.getMessage("TITLE_OPEN_LINK");
			if (cellValue.indexOf(".") != -1) {
				var extension = cellValue.split(".")[cellValue.split(".").length -1].toLowerCase();
				if (extension == "jpg" || extension == "jpeg" || extension == "png" || extension == "gif") {
					message = "<img src=\"" + cellValue + "\" width=\"100\" />";
				}
				if (extension == "zip" || extension == "pdf" || extension == "odt" || extension == "ods" || extension == "odp") {
					message = this.getMessage("TITLE_DOWNLOAD_FILE") + " " + extension.toUpperCase();
				}
			}
			formatedCellValue = "<a href=\"" + cellValue + "\" target=\"_blank\" class=" + this.displayClasses.lienClassName + ">" + message + "</a>"; 
		} else if (cellValue.indexOf("mailto:") === 0) {
			formatedCellValue = "<a href=\"" + cellValue + "\" class=" + this.displayClasses.lienClassName + ">" + this.getMessage("TITLE_MAIL_LINK") + "</a>"; 
		}
		return formatedCellValue;
	},

	CLASS_NAME: "Descartes.UI.ToolTipContainerExtJS4",

	VERSION : "3.2"
	
});
