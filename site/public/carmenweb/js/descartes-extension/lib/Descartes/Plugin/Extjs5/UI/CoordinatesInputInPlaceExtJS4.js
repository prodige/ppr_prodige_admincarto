/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.CoordinatesInputInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, la saisie d'un couple de coordonn�es pour recentrer la carte.
 * 
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS4>
 * 
 * 
 * Ev�nements d�clench�s:
 * recentrage - Les coordonn�es sont saisies et valides.
 */
Descartes.UI.CoordinatesInputInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {
	
	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 */
	label: true,
	
	height:120,
	
	/**
	 * Propriete: msgTarget
	 * {String} Mode d'affichage des messages de erreurs.
	 */
	msgTarget:'side',
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
    EVENT_TYPES: ["recentrage"],

    /**
	 * Constructeur: Descartes.UI.CoordinatesInputInPlaceExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
	 * model - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.CoordinatesInput>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 * msgTarget - {String} Mode d'affichage des messages d'erreurs.	 
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
	 */
	initialize: function(div, model, options) {
		Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone de la page HTML pour la saisie des coordonn�es. 
	 */
	draw: function() {
		var self = this;
		var title = '';
		
		if(this.label){
			title = this.getMessage("TITLE_MESSAGE");
		}
		
		var optsPanel = {
	        xtype        : 'form',
	        renderTo     : this.div.id,
	        autoScroll   : true,
	        id           : 'formPanel',
	        frame        : true,
	        title        : title,
	        pollForChanges: true,
	        items        : [
	            {
	                fieldLabel : this.getMessage("INVITE1_MESSAGE"),
			        xtype      : 'numberfield',
	                id         : 'x',
	                minValue   : 0,
	                allowBlank : false,
	                msgTarget  : this.msgTarget,
            		blankText  : this.getMessage("X_ERROR"),
            		nanText    : this.getMessage("X_ERROR"),
            		minText    : this.getMessage("X_ERROR")
	            },
	            {
	                fieldLabel : this.getMessage("INVITE2_MESSAGE"),
			        xtype      : 'numberfield',
	                id         : 'y',
	                minValue   : 0,
	                allowBlank : false,
	                msgTarget  : this.msgTarget,
            		blankText  : this.getMessage("Y_ERROR"),
            		nanText    : this.getMessage("Y_ERROR"),
            		minText    : this.getMessage("Y_ERROR")
	            }
	        ],
		    buttons      : [{
		        text     : this.getMessage("BUTTON_MESSAGE"),
		        formBind : true,
		        handler  : function() {
	        		self.done();	        		
		        }
		    }],
		    buttonAlign  : 'center'
	    };
		
		this.formPanel =  new Ext.FormPanel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));
	    this.formPanel.show();
	    
	    if(!(this.label && this.formPanel.collapsible && this.formPanel.collapsed)){
        	this.modifyStyle();
        }
	    
	},
	
 	modifyStyle: function() {
 		var h = this.height;
 		if(!this.label){
 			h -= 25;
 		}
 		this.formPanel.getEl().dom.style.height=h+"px";
	},
	
	/**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'recentrage'.
     */
	done: function() {
		this.model.x = this.formPanel.getForm().findField('x').getValue();
		this.model.y = this.formPanel.getForm().findField('y').getValue();
		this.events.triggerEvent("recentrage");
	},
	
	CLASS_NAME: "Descartes.UI.CoordinatesInputInPlaceExtJS4",

	VERSION : "3.2"
});
