/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.PrinterSetupInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, la saisie des param�tres de mise en page pour l'exportation PDF.
 *  
 *  H�rite de:
 * - <Descartes.UI.AbstractInPlaceExtJS4>
 * 
 * Ev�nements d�clench�s:
 * choosed - Les param�tres sont saisis et valides.
 */
Descartes.UI.PrinterSetupInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {

	/**
	 * Propriete: errors
	 * {Array(String)} Liste des erreurs rencontr�s lors des contr�les de surface.
	 */
	errors: null,
	
	/**
	 * Propriete: msgTarget
	 * {String} Mode d'affichage des messages d'erreurs.
	 */
	msgTarget:'side',
	
	/**
	 * Propriete: papers
	 * {Array(Object)} Objets JSON stockant les propri�t�s des "papiers" disponibles en coh�rence avec la taille de la carte.
	 */
	papers:[],
	
	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des param�tres d'impression.
	 */
	label: true,
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
	height:245,
	
	EVENT_TYPES: ["choosed"],

	/**
	 * Constructeur: Descartes.UI.PrinterSetupInPlaceExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
	 * paramsModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.PrinterParamsManager>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 *
	 * Options de construction propres � la classe:
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des param�tres d'impression.
	 * msgTarget - {String} Mode d'affichage des messages d'erreurs.	 
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
	 */
	initialize: function(div, paramsModel, options) {
		Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone de la page HTML pour la saisie des param�tres de mise en page.
	 */
	draw: function() {
		this.setPapers();
		var self = this;
		
		var title = '';
        
        if(this.label){
            title = this.getMessage("TITLE_MESSAGE");
        }
		
        var optsPanel = {
	        xtype        : 'form',
	        title		 : title,
	        renderTo     : this.div.id,
	        id           : 'formPanelPdf',
	        frame        : true,
	        pollForChanges:true,
	        items        : [
	            {
	                fieldLabel    : this.getMessage("PAPER_INPUT"),
	                displayField  : 'text',
	                valueField    : 'value',
	                typeAhead     : true,
	                queryMode	  : 'local',
	                triggerAction : 'all',
			        xtype         : 'combo',
					editable	  : false,
	                id            : 'paper',
            		allowBlank    : false,
            		msgTarget 	  : this.msgTarget,
            		value         : this.model.paper,
            		blankText     : this.getMessage("PAPER_INPUT_ERROR"),		                
	                store         : new Ext.data.SimpleStore({
				                         fields :['value', 'text', 'paperWidth', 'paperHeight'],
				                         data   : self.papers
			                        })
	            },
	            {
	            	xtype : 'fieldset',
	            	title : this.getMessage("MARGINS_GROUP_INPUT"),
	            	items : [{
	            		xtype      : 'numberfield',
	            		fieldLabel : this.getMessage("MARGIN_TOP_INPUT"),
	            		id         : 'marginTop',
	            		value      : this.model.marginTop,
	            		minValue   : 0,
	            		allowBlank : false,
	            		msgTarget 	  : this.msgTarget,
	            		nanText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		minText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		blankText  : this.getMessage("MARGIN_TOP_ERROR")
	            	},{
	            		xtype      : 'numberfield',
	            		fieldLabel : this.getMessage("MARGIN_BOTTOM_INPUT"),
	            		id         : 'marginBottom',
	            		value      : this.model.marginBottom,
	            		minValue   : 0,
	            		allowBlank : false,
	            		msgTarget 	  : this.msgTarget,
	            		nanText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		minText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		blankText  : this.getMessage("MARGIN_BOTTOM_ERROR")
	            	},{
	            		xtype      : 'numberfield',
	            		fieldLabel : this.getMessage("MARGIN_LEFT_INPUT"),
	            		id         : 'marginLeft',
	            		value      : this.model.marginLeft,
	            		minValue   : 0,
	            		allowBlank : false,
	            		msgTarget 	  : this.msgTarget,
	            		nanText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		minText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		blankText  : this.getMessage("MARGIN_LEFT_ERROR")
	            	},{
	            		xtype      : 'numberfield',
	            		fieldLabel : this.getMessage("MARGIN_RIGHT_INPUT"),
	            		id         : 'marginRight',
	            		value      : this.model.marginRight,
	            		minValue   : 0,
	            		allowBlank : false,
	            		msgTarget 	  : this.msgTarget,
	            		nanText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		minText    : this.getMessage("MARGIN_TOP_ERROR"),
	            		blankText  : this.getMessage("MARGIN_RIGHT_ERROR")
	            	}]
	            }
	        ],
		    buttons: [{
		        text     : this.getMessage("BUTTON_MESSAGE"),
		        formBind : true,
		        handler  : function() {
	        		self.done();	        		
		        }
		    }],
		    buttonAlign  : 'center'
	    };
        
		this.formPanel =  new Ext.FormPanel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));

	    this.formPanel.show();		
	    
	    if(!(this.label && this.formPanel.collapsible && this.formPanel.collapsed)){
        	this.modifyStyle();
        }
	},
	
	modifyStyle: function() {
		var h = this.height;
 		if(!this.label){
 			h -= 25;
 		}
 		this.formPanel.getEl().dom.style.height=h+"px";
	},
	
	/**
	 * Methode: redraw
	 * Reconstruit la zone de la page HTML pour la saisie des param�tres de mise en page en cas de changement de taille de la carte.
	 * 
	 * Param�tres:
	 * paramsModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.PrinterParamsManager>.
	 */
	redraw: function(paramsModel) {
		this.model = paramsModel;
		this.clearDiv();
		this.draw();
	},
	
    /**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'choosed', ou affiche les messages d'erreur rencontr�s.
     */
    done: function() {
    	if (this.validateDatas()) {
    		this.events.triggerEvent("choosed");
    	} else {
    		this.showErrors();
    	}
    },
    
    /**
	 * Methode: setPapers
	 * Construit la liste des "papiers" disponibles � partir du mod�le transmis par le contr�leur.
	 */
	setPapers: function() {
		this.papers = [];
		for (var i=0, len=this.model.enabledFormats.length ; i<len ; i++) {
			format = this.model.enabledFormats[i];
			var text = format.code.substring(0, format.code.length -1);
			text += (format.code.substring(format.code.length -1) === "L") ? this.getMessage("LANDSCAPE_OPTION") : this.getMessage("PORTRAIT_OPTION");
			this.papers.push([format.code, text, format.width, format.height]);
		}
	},
	
    /**
     * Methode: validateDatas
     * Effectue les contr�les de surface apr�s la saisie.
     * 
     * :
     * Met � jour le mod�le si ceux-ci sont assur�s.
     * 
     * :
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function() {
		this.errors = [];

    	this.model.paper = this.formPanel.getForm().findField('paper').getValue();
    	
		this.model.marginTop = parseInt(this.formPanel.getForm().findField('marginTop').getValue(), 10);
		this.model.marginBottom = parseInt(this.formPanel.getForm().findField('marginBottom').getValue(), 10);
		this.model.marginLeft = parseInt(this.formPanel.getForm().findField('marginLeft').getValue(), 10);
		this.model.marginRight = parseInt(this.formPanel.getForm().findField('marginRight').getValue(), 10);
		
		var lMap = parseFloat(Math.round(this.model.mapWidth * 25.4 / 96));
		var hMap = parseFloat(Math.round(this.model.mapHeight * 25.4 / 96));
		
		var lPage;
		var hPage;
		
		for (var i=0, len=this.papers.length ; i<len ; i++) {
			var paper = this.papers[i];
						
			if (paper[0] === this.model.paper) {
				lPage = paper[2];
				hPage = paper[3];
			}
		}

		var deltaL = (lMap + this.model.marginLeft + this.model.marginRight) - lPage;
		var deltaH = (hMap + this.model.marginTop + this.model.titleHeight + this.model.marginBottom) - hPage;
		
		if (deltaL > 0) {
			this.errors.push(this.getMessage("MARGIN_WIDTH_ERROR") + deltaL.toString() + " mm" + this.getMessage("MARGINS_TIP"));
		}
		if (deltaH > 0) {
			this.errors.push(this.getMessage("MARGIN_HEIGHT_ERROR") + deltaH.toString() + " mm" + this.getMessage("MARGINS_TIP"));
		}
		
		return (this.errors.length === 0);
    },
    
    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontr�s lors des contr�les de surface.
     */
    showErrors: function() {
    	if (this.errors.length !== 0) {
    		var errorsMessage = this.getMessage("ERRORS_LIST");
    		for (var i=0, len=this.errors.length ; i<len ; i++) {
    			errorsMessage += "\n\t- " + this.errors[i];
    		}
    		alert(errorsMessage);
    	}
    },
    
	CLASS_NAME: "Descartes.UI.PrinterSetupInPlaceExtJS4",

	VERSION : "3.2"
});
