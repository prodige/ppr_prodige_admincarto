/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.Info.MeasurePopupExtJS4
 * Classe pour l'affichage des mesures graphiques effectu�es sur la carte
 * 
 * H�rite de:
 *  - OpenLayers.Control
 */
Descartes.Info.MeasurePopupExtJS4 = OpenLayers.Class(OpenLayers.Control,{
	
    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML de la zone d'affichage des mesures.
     * 
     * :
     * messageClassName - ("DescartesMeasurePopup" par d�faut)
     */
	defaultDisplayClasses:{
		messageClassName: "DescartesMeasurePopup"
	},
	
    /**
     * Propriete: displayClasses
     * {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
     * 
     * :
     * Construit � partir de defaultDisplayClasses, puis surcharg� en pr�sence d'option compl�mentaire.
     */
	displayClasses: null,
	
    messageDiv: null,
    closeDiv: null,
    
    width:140,

	/**
	 * Constructeur: Descartes.Info.MeasurePopupExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
	 */
	initialize: function(options) {
		this.displayClasses = OpenLayers.Util.extend({}, this.defaultDisplayClasses);
		if((options !== undefined)){
			OpenLayers.Util.extend(this.displayClasses, options.displayClasses);
			delete options.displayClasses;
		}
	
		var olOptions = {};
		olOptions.displayClass = this.displayClasses.messageClassName;
		OpenLayers.Control.prototype.initialize.apply(this,[olOptions]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone d'affichage des mesures dans la page HTML.
	 */
	draw: function () {
		var self = this;
		OpenLayers.Control.prototype.draw.apply(this, arguments);

	    this.element = document.createElement('div');
	    this.element.className = this.displayClasses.messageClassName;
	    
        this.messageDiv = document.createElement('div');
        this.messageDiv.className = this.displayClasses.messageClassName;
        this.element.appendChild(this.messageDiv);
        
        this.div.id = 'divMeasure';
        
        return this.div;
	},
	
	/**
	 * Methode: refresh
	 * Actualise le contenu la zone d'affichage des mesures dans la page HTML.
	 * 
	 * Param�tres:
	 * out - {String} Texte pour l'affichage des mesures.
	 */
    refresh: function(out) {

    	this.messageDiv.innerHTML = out;

    	if(Ext.getCmp('measure-popup')){	    	
    		Ext.getCmp('measure-popup').destroy();
    	}
    	
    	new Ext.Window({
			id     		: 'measure-popup',
	        renderTo    : this.map.div.id,
			constrain	: true,
			autoScroll  : true,
	        modal  		: false,
	        draggable   : false,
	        shadow      : false,
	        contentEl    : this.element,
	        minWidth	:this.width,
	        minHeight	:30,
	        resizable	:false,
	        closeAction : 'destroy',
	        closable 	: true
		}).show();
    	 	    	
    	this.modifyStyle();
    	
    	this.registerEvents();
    },
    
	modifyStyle: function() {
		
    	if(!Ext.getCmp('measure-popup')){	    	
	    	 
	    	Ext.getCmp('measure-popup').width = this.width;
	    	Ext.getCmp('measure-popup').getEl().anchorTo(this.map.div.id, "tr-br", [0, -Ext.getDom('measure-popup').clientHeight]);

    	}else{

    		if(Ext.getCmp('measure-popup').width){
				Ext.getCmp('measure-popup').width = this.width;
				Ext.getCmp('measure-popup').height = undefined;
				Ext.getCmp('measure-popup').setHeight(Ext.getCmp('measure-popup').height);
			}
			
			Ext.getCmp('measure-popup').getEl().anchorTo(this.map.div.id, "tr-br", [0, -Ext.getDom('measure-popup').clientHeight]);

    	}
	},
	
    registerEvents:function() {
        this.events = new OpenLayers.Events(this, Ext.getDom('measure-popup'), null, true);
        
        this.events.on({
            "mousedown": this.onmousedown,
            "mousemove": this.onmousemove,
            "mouseup": this.onmouseup,
            "click": this.onclick,
            "mouseout": this.onmouseout,
            "dblclick": this.ondblclick,
            scope: this
        });
        
     },

    onmousedown: function (evt) {
        this.mousedown = true;
        OpenLayers.Event.stop(evt, true);
    },

    onmousemove: function (evt) {
        if (this.mousedown) {
            OpenLayers.Event.stop(evt, true);
        }
    },

    onmouseup: function (evt) {
        if (this.mousedown) {
            this.mousedown = false;
            OpenLayers.Event.stop(evt, true);
        }
    },

    onclick: function (evt) {
        OpenLayers.Event.stop(evt, true);
    },

    onmouseout: function (evt) {
        this.mousedown = false;
    },
    
    ondblclick: function (evt) {
        OpenLayers.Event.stop(evt, true);
    },
	
	CLASS_NAME: "Descartes.Info.MeasurePopupExtJS4",

	VERSION : "3.2"
});