/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
   Luc Boyer
   Damien Despres
   David Marquet
   Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.ScaleSelectorInPlaceExtJS5
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'une �chelle dans une liste pour recadrer la carte.
 * 
 * :
 * Le formulaire de saisie est constitu� d'une seule liste d�roulante.
 * 
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS5>
 * 
 * 
 * Ev�nements d�clench�s:
 * selected - Une �chelle est s�lectionn�e dans la liste.
 */
Descartes.UI.ScaleSelectorInPlaceExtJS5 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS5, {

  /**
   * Propriete: label
   * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de choix.
   */
  label: true,
  
  /**
   * Propriete: optionsPanel
   * {Objet} Options du panel.
   */
  optionsPanel:{},
  
  height:60,
  
  /**
   * Propriete: widthComboBox
   * {Integer} Taille des comboBox.
   */
  widthComboBox: 200,
  
  /**
   * Propriete: ascending
   * {Boolean} Indicateur pour l'ordre d'affichage des �chelles disponibles.
   */
  ascending: false,
  
    EVENT_TYPES: ["selected"],

  /**
   * Constructeur: Descartes.UI.ScaleSelectorInPlaceExtJS4
   * Constructeur d'instances
   * 
   * Param�tres:
   * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
   * scaleModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.ScaleSelector>.
   * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
   * 
   * Options de construction propres � la classe:
   * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
   * ascending - {Boolean} Indicateur pour l'ordre d'affichage des �chelles disponibles.
   * widthComboBox - {Integer} Taille des comboBox.
   * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
   */
  initialize: function(div, scaleModel,options) {
    Descartes.UI.AbstractInPlaceExtJS5.prototype.initialize.apply(this, [div, scaleModel, this.EVENT_TYPES,options]);
  },

  /**
   * Methode: draw
   * Construit la zone de la page HTML pour le choix de l'�chelle.
   * 
   * Param�tres:
   * enabledScales - {Array(Float)} Liste des d�nominateurs d'�chelle propos�s.
   */
  draw: function(enabledScales) {
      if (enabledScales.length !== 0) {
        if (this.ascending) {
          enabledScales.sort(function(a, b) {return b - a;});
        } else {
          enabledScales.sort(function(a, b) {return a - b;});
        }
        var self = this;

        var title = '';
        if(this.label){
          title = this.getMessage("TITLE_MESSAGE");
        }
        
        var optionsSelect = [];
        enabledScales.forEach(
          function(scale) {             
            var option = [scale.toString(), Descartes.Utils.readableScale(scale)];
            optionsSelect.push(option);
          }
        );

        var optsPanel = {
            xtype        : 'panel',
            renderTo     : this.div.id,
            autoScroll   : true,
            id           : 'panelScaleSelector',
            frame        : true,
            title        : title,
            collapsible  : this.collapsible,
            collapsed    : this.collapsed,
            tools    : this.tools,
            items        : [
                          {
                            displayField  : 'text',
                            valueField    : 'value',
                            typeAhead     : true,
                            queryMode     : 'local',
                            triggerAction : 'all',
                            xtype         : 'combo',
                            width         : this.widthComboBox,
                            editable      : true,
                            id            : 'selectScaleSelector',
                            emptyText     : this.getMessage("DEFAULT_OPTION_MESSAGE"),                    
                            store         : new Ext.data.SimpleStore({
                                             fields :['value', 'text'],
                                             data   : optionsSelect
                                          }),
                            listeners     : {
                               select: function(combo, record, eOpt){
                                 self.done(-1, record.data.value);
                               }
                            }
                          }
          ]
        };
        
      this.panel =  new Ext.create('Ext.panel.Panel', OpenLayers.Util.extend(optsPanel,this.optionsPanel));

        this.panel.show();
        
        if(!(this.label && this.panel.collapsible && this.panel.collapsed)){
            this.modifyStyle();
          }
      }
  },
  
  modifyStyle: function() {
    var h = this.height;
    if(!this.label){
      h -= 25;
    }
    this.panel.getEl().dom.style.height=h+"px";
  },
  
    /**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'selected'.
     * 
     * Param�tres:
     * control - {DOMElement} Liste d�roulante de choix de l'�chelle dans le formulaire.
     */
  done: function(index, value) {
    this.model.scale = value;
    this.events.triggerEvent("selected");
  },
  
  CLASS_NAME: "Descartes.UI.ScaleSelectorInPlaceExtJS5",

  VERSION : "3.3"
});
