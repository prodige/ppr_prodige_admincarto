/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
   Luc Boyer
   Damien Despres
   David Marquet
   Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Chargement automatique du plugin ExtJS5
**/

Descartes.ViewManager.addPlugin(
  "ExtJS5",
  {
    0 : Descartes.UI.CoordinatesInputInPlaceExtJS4,
    1 : Descartes.UI.CoordinatesInputDialogExtJS4,
    2 : Descartes.ModalDialogExtJS4,
    3 : Descartes.UI.TabbedDataGridsExtJS4,
    4 : Descartes.UI.LayersTreeExtJS4,
    5 : Descartes.UI.PrinterSetupInPlaceExtJS4,
    6 : Descartes.UI.PrinterSetupDialogExtJS4,
    7 : Descartes.UI.ScaleChooserInPlaceExtJS4,
    8 : Descartes.UI.ScaleSelectorInPlaceExtJS5,
    9 : Descartes.Info.LegendExtJS4,
    10 : Descartes.UI.BookmarksInPlaceExtJS4,
    11 : Descartes.UI.RequestManagerInPlaceExtJS4,
    12 : Descartes.UI.GazetteerInPlaceExtJS4,
    13 : Descartes.UI.ToolTipContainerExtJS4,
    14 : Descartes.Info.MeasurePopupExtJS4,
    15 : Descartes.Button.ContentTask.ChooseWmsLayersExtJS4,
    16 : Descartes.ToolBarExtJS4,
    17 : Descartes.UI.SizeSelectorInPlaceExtJS5
  },
  function() {  
    Descartes.Action.AjaxSelection.prototype.showWaitingMessage = function() {
      Ext.Msg.wait('Recherche en cours, merci de patienter...','', {animate:true});
    };
    Descartes.Action.AjaxSelection.prototype.closeWaitingMessage = function() {
      Ext.Msg.hide();
    };
  }
);

Descartes.ViewManager.EXTJS5 = "ExtJS5";
