/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

if (typeof Ext !== "undefined") {
/**
 * Surcharge de Ext.tree.View.
 */
Ext.override(Ext.tree.View, {
	onCheckChange : function(record){
        var checked = record.get('checked');     
        checked = !checked;
        record.set('checked', checked);
        this.fireEvent('checkchange', record, checked);
        
    },
    toggle        : function(record) {
    	var self = this;
        this[record.isExpanded() ? 'collapse' : 'expand'](record, false, function(){
        		self.panel.fireEvent('afterrender', self.panel);
        	}
        );
    }
});

/**
 * Surcharge de Ext.tree.column.
 */
Ext.override(Ext.tree.Column, {
	initComponent : function() {
        var origRenderer = this.renderer || this.defaultRenderer,
            origScope    = this.scope || window;

        this.renderer = function(value, metaData, record, rowIdx, colIdx, store, view) {
            var buf   = [],
                format = Ext.String.format,
                depth = record.getDepth(),
                treePrefix  = Ext.baseCSSPrefix + 'tree-',
                elbowPrefix = treePrefix + 'elbow-',
                expanderCls = treePrefix + 'expander',
                imgText     = '<img src="{1}" class="{0}" />',
                checkboxText= '<input type="button" role="checkbox" class="{0}" {1}/>',
                formattedValue = origRenderer.apply(origScope, arguments),
                href = record.get('href'),
                target = record.get('hrefTarget'),
                cls = record.get('cls');

            while (record) {
                if (!record.isRoot() || (record.isRoot() && view.rootVisible)) {
                    if (record.getDepth() === depth ) {
                    	if(cls === "folder"){
	                        buf.unshift(format(imgText,
	                            treePrefix + 'icon ' + 
	                            treePrefix + 'icon' + (record.get('icon') ? '-inline ' : (record.isLeaf() ? '-leaf ' : '-parent ')) +
	                            (record.get('iconCls') || ''),
	                            record.get('icon') || Ext.BLANK_IMAGE_URL
	                        ));
	                        if (record.get('checked') !== null) {
	                            buf.unshift(format(
	                                checkboxText,
	                                (treePrefix + 'checkbox') + (record.get('checked') ? ' ' + treePrefix + 'checkbox-checked' : ''),
	                                record.get('checked') ? 'aria-checked="true"' : ''
	                            ));
	                            if (record.get('checked')) {
	                                metaData.tdCls += (' ' + treePrefix + 'checked');
	                            }
	                        } else if(!record.parentNode.isRoot()){
	                        	buf.unshift(format(
	                                    checkboxText,
	                                    (treePrefix + 'checkbox') + ' ' + treePrefix + 'node-grayed', ''
	                            ));
	                            if (record.get('checked') === null) {
	                                metaData.tdCls += (' ' + treePrefix + 'node-grayed');
	                            }
	                        }
                    	}
                        if (record.isLast()) {
                        	if(cls === "folder"){
                                  buf.unshift(format(imgText, (elbowPrefix + 'plus ' + expanderCls), Ext.BLANK_IMAGE_URL));       
                        	}else{
                        		if (record.isExpandable()) {
                                    buf.unshift(format(imgText, (elbowPrefix + 'end-plus ' + expanderCls), Ext.BLANK_IMAGE_URL));
                                } else {
                                    buf.unshift(format(imgText, (elbowPrefix + 'end'), Ext.BLANK_IMAGE_URL));
                                }
                        	}
                        } else {
                        	if(cls === "folder"){
                                buf.unshift(format(imgText, (elbowPrefix + 'plus ' + expanderCls), Ext.BLANK_IMAGE_URL));       
	                      	}else{
	                      		if (record.isExpandable()) {
	                                buf.unshift(format(imgText, (elbowPrefix + 'plus ' + expanderCls), Ext.BLANK_IMAGE_URL));
	                            } else {
	                                buf.unshift(format(imgText, (treePrefix + 'elbow'), Ext.BLANK_IMAGE_URL));
	                            }
	                      	}
                        } 
                    } else {
                        if (record.isLast() || record.getDepth() === 0) {
                            buf.unshift(format(imgText, (elbowPrefix + 'empty'), Ext.BLANK_IMAGE_URL));
                        } else if (record.getDepth() !== 0) {
                            buf.unshift(format(imgText, (elbowPrefix + 'line'), Ext.BLANK_IMAGE_URL));
                        }                      
                    }
                }
                record = record.parentNode;
            }
            if (href) {
                buf.push('<a href="', href, '" target="', target, '">', formattedValue, '</a>');
            } else {
                buf.push(formattedValue);
            }
            if (cls) {
                metaData.tdCls += ' ' + cls;
            }
            return buf.join('');
        };
        this.callParent(arguments);
    }
});
}
