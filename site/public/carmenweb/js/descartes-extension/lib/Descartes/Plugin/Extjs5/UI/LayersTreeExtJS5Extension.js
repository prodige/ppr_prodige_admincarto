/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
   Luc Boyer
   Damien Despres
   David Marquet
   Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.LayersTreeExtJS5
 * Classe affichant une arborescence du contenu de la carte et offrant des interactions sur celui-ci.
 * 
 * H�rite de:
 * - <Descartes.UI>
 * 
 * Ev�nements d�clench�s:
 * layerSelected - Une couche a �t� s�lectionn�e.
 * groupSelected - Un groupe a �t� s�lectionn�.
 * nodeUnselect - Une couche ou une couche a �t� d�s�lectionn�.
 * rootSelected - La racine a �t� s�lectionn�e.
 */
Descartes.UI.LayersTreeExtJS5 = OpenLayers.Class(Descartes.UI, {

    EVENT_TYPES: ["layerSelected", "groupSelected", "nodeUnselect", "rootSelected"],
        
  defaultDisplayClasses: {
    treeRoot: "DescartesLayersTreeRoot",
    layerVisibilityOn: "DescartesLayersTreeLayerVisible",
    layerVisibilityOff: "DescartesLayersTreeLayerHidden",
    layerVisibilityNeedZoomOut: "DescartesLayersTreeLayerZoomOut",
    layerVisibilityNeedZoomIn: "DescartesLayersTreeLayerZoomIn",
    layerInfoEnabled: "DescartesLayersTreeLayerInfoEnabled",
    layerInfoDisabled: "DescartesLayersTreeLayerInfoDisabled",
    layerInfoForbidden: "DescartesLayersTreeLayerInfoForbidden",
    layerSheetActive: "DescartesLayersTreeLayerSheetActive",
    layerSheetInactive: "DescartesLayersTreeLayerSheetInactive",
    blankBlocQuote: "DescartesLayersTreeBlockQuote",
    layerLienMetadata: "DescartesLayersTreeLayerLienMetadata",
    treePanel:"DescartesLayersTreePanel"
    },
    
  /**
   * Propriete: defaultResultUiParams
   * {Object} Objet JSON stockant les propri�t�s par d�faut n�cessaires � la g�n�ration du tableau des propri�tes de l'ensemble des objets d'une couche.
     * 
     * :
     * type - <Descartes.UI.TYPE_WIDGET>
     * view - <Descartes.UI.ToolTipContainer>
     * div - null
     * format - "JSON"
     * withReturn - false
     * withCsvExport - false
   */
  defaultResultUiParams: {
    type: Descartes.UI.TYPE_WIDGET,
    view: null,
    div: null,
    format: "JSON",
    withReturn: false,
    withCsvExport: false
  },
  
  /**
   * Propriete: resultUiParams
   * {Object} Objet JSON stockant les propri�t�s n�cessaires � la g�n�ration du tableau des propri�tes de l'ensemble des objets d'une couche gr�ce � la classe <Descartes.Action.AjaxSelection>.
     * 
     * :
     * Construit � partir de defaultResultUiParams, puis surcharg� en pr�sence d'options compl�mentaires.
     * 
     * :
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour le tableau
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * div - {DOMElement|String} El�ment DOM de la page accueillant le tableau ou identifiant de cet �lement si le type est <Descartes.UI.TYPE_WIDGET> ou <Descartes.UI.TYPE_INPLACEDIV>.
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhait�.
     * withReturn - {Boolean} Indicateur pour la pr�sence de bouton de localisation rapide sur chaque objet.
     * withCsvExport - {Boolean} Indicateur pour la possibilit� d'exporter le tableau sous forme de fichier CSV.
   */
  resultUiParams: null,
    
    /**
     * Propriete: selectedItem
     * {String} Index hi�rarchique du groupe ou de la couche s�lectionn� dans l'arborescence.
     */
    selectedItem: "",
    
    /**
     * Propriete: tree
     * {Ext.tree.TreePanel} Le treeview ExtJS5.
     */
    tree: null,
    
  /**
   * Constructeur: Descartes.UI.LayersTreeExtJS5
   * Constructeur d'instances
   * 
   * Param�tres:
   * div - {DOMElement|String} El�ment DOM de la page accueillant l'arborescence.
   * layersModel - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
   * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
   * 
   * Options de construction propres � la classe:
   * resultUiParams - {Object} Objet JSON stockant les propri�t�s n�cessaires � la g�n�ration du tableau des propri�tes de l'ensemble des objets d'une couche.
   */
  initialize: function(div, layersModel, options) {
    this.resultUiParams = OpenLayers.Util.extend({}, this.defaultResultUiParams);

    if((options !== undefined)){
      OpenLayers.Util.extend(this.resultUiParams, options.resultUiParams);
      delete options.resultUiParams;
    }
    Descartes.UI.prototype.initialize.apply(this, [div, layersModel, this.EVENT_TYPES]);
  },
  
  /**
   * Methode: draw
   * Construit et affiche l'arborescence de gestion du contenu de la carte.
   */
   draw: function() {
    var self = this;
    this.clearDiv();
    
    var json = [];
    
    this._getStructure(this.model.item, json);
    
    var store = new Ext.data.TreeStore({
          root: {
          expanded : true,
        text: 'Fake',
        draggable:false,
        id:'fake',
        children: json
          }
      });
    
    this.tree = new Ext.tree.Panel({
          store           : store,
          id        : 'dTreePanel',
          hideHeaders     : true,
          rootVisible     : false,
        border          : false,
        autoScroll      : false,
        containerScroll : false,
          viewConfig      : {
              plugins: [{ptype : 'treeviewdragdrop', 
                     enableDrag: self.model.editable, 
                     enableDrop: self.model.editable}],
              baseCls: self.displayClasses.treePanel,
          },
          autoHeight      : true,
          autoWidth       : true,
          renderTo        : self.div.id,
          collapsible     : false,
      listeners       : {
         afterrender    : function(){
             self._setToolTip();
             if(self.model.fctVisibility){
               self._addToolsVisibility(this.getRootNode().childNodes[0]);
             }
             if(self.model.fctQueryable){
               self._addToolsQueryable(this.getRootNode().childNodes[0]);
             }
             if (self.model.fctOpacity){
               self._addToolsSlider(this.getRootNode().childNodes[0]);
             }
             self._addToolsMetadataUrl(this.getRootNode().childNodes[0]);
             
             self.resetHeight(this);
           },
         beforeitemmove : function(node, oldParent, newParent, index, eOpts){
              self.onDropItem(node, newParent, index, eOpts);
           },
         beforeitemappend : function(newParent, node, eOpts){
            self.onDropAppendItem(newParent, node, eOpts);           
         },
         beforeitemmousedown : function(self,record,item,index,e,eOpts){
            self.plugins[0].dragZone.dragText=record.raw.text;
         },
         itemclick      : function(view, record, item, index, e, eOpts){
           if(self.model.editable && e.target.className !== self.displayClasses.layerLienMetadata){
             self.selectItem(record);
           }
           return false;
         }

        }
      });
    
    this._setToolTip();
    
    this.tree.on("beforeitemexpand", function(node, eventOpts){
         self.openCloseGroup(node.raw.id.substring(self.CLASS_NAME.replace(/\./g, "").length), 'expanded');
         self._setToolTip();
       });
    this.tree.on("scrollershow", function(node, eventOpts){
       self.resetHeight(self.tree);
       });
    
    this.tree.on("beforeitemcollapse", function(node, eventOpts){
         self.openCloseGroup(node.raw.id.substring(self.CLASS_NAME.replace(/\./g, "").length), 'collapsed');
         self._setToolTip();
         self.resetHeight(self.tree);
       });
    this.tree.on("checkchange", function(node, checked, eventOpts){
      if(node.childNodes.length > 0){
        node.cascadeBy(function(child){
            child.set("checked",checked);
            self._addToolsVisibility(child,checked);
            self._addToolsQueryable(child,checked);
            self._addToolsSlider(child);
            self._addToolsMetadataUrl(child);
            self.toggleLayerVisibilityByCheckBoxChanged(child, checked);
          });   
      } else {
        self._updateParents(node);
      }
      self._addToolsVisibility(node,checked);
      self._addToolsQueryable(node,checked);
      self._addToolsSlider(node);
      self._addToolsMetadataUrl(node);
      self.toggleLayerVisibilityByCheckBoxChanged(node, checked);
      self._setToolTip();
    });
  },
  
  resetHeight: function(cmp){   
            cmp.setHeight(cmp.getEl().down('table.x-grid-table').getHeight()+1);
  },
  
  /**
   * Private
   * 
   * Mise a jour de l'etat des checkbox des groupes parents.
   */
  _updateParents: function(node){
    var parentNode = node.parentNode;
    if((parentNode !== undefined || parentNode !== null) && parentNode.raw.id.replace(this.CLASS_NAME.replace(/\./g, ""), "") !== ""){
      var checked;
      parentNode.eachChild(function (n) {
        if(checked === undefined){
          checked = n.data.checked;
        }else if (checked !== n.data.checked) {
          checked = null;
          return false;
        }
            });
      parentNode.set('checked', checked);
      this._updateParents(parentNode);
    }
  },
  
  /**
   * Private
   * 
   * Affichage de l'info bulle pour l'affichage/masquage des groupes.
   */
  _setToolTip: function(){
    var iconCollapsed = Ext.DomQuery.jsSelect('.x-tree-lines .x-tree-elbow-plus');
    var iconExpanded = Ext.DomQuery.jsSelect('.x-tree-lines .x-grid-tree-node-expanded .x-tree-elbow-plus');

    for (var i=0, len=iconCollapsed.length ; i<len ; i++) {
      iconCollapsed[i].title = this.getMessage("SHOW_GROUP");
    }
    
    for (var i=0, len=iconExpanded.length ; i<len ; i++) {
      iconExpanded[i].title = this.getMessage("HIDE_GROUP");
    }
  },
  
  /**
   * Private
   * 
   * R�cup�ration des groupes et couches � afficher dans le tree .
   */
  _getStructure: function(node, groupItems){
    var id = this.CLASS_NAME.replace(/\./g, "") + node.index.toString();
    var text = node.index === "" ? "Contenu de la carte" : node.title;
    var iconCls = node.index === "" ? this.displayClasses.treeRoot : 'no-icon';
    var checked = node.visible;
    var item;
    
    if(node.items){
      var expanded = node.index === "" ? true : node.opened;
      var ssGroupItems = [];
      
      for (var i=0, len=node.items.length ; i<len ; i++) {
        this._getStructure(node.items[i], ssGroupItems);
      }
      if(node.index === "" || !this.model.fctVisibility){
        item = {id: id, text: text, leaf: false, cls: 'folder', iconCls: iconCls, expanded: expanded, children: ssGroupItems};
      }else{
        item = {id: id, text: text, leaf: false, cls: 'folder', iconCls: iconCls, expanded: expanded, checked: checked, children: ssGroupItems};
      }
      groupItems.push(item);
    } else {
      //item = {id: id, text: '<span id="'+id+'Visibility"></span><span id="'+id+'Queryable"></span><span id="'+id+'Slider" class="DescartesLayersTreeSlider"></span><span title="'+text+'">'+text+'</span><span id="'+id+'MetadataUrl"></span>', leaf: true, cls: 'file', iconCls: iconCls, checked: checked};
      item = {id: id, text: '<span id="'+id+'Visibility"></span><span id="'+id+'Queryable"></span><span id="'+id+'Slider" class="DescartesLayersTreeSlider"></span>'+'<span id="'+id+'MetadataUrl"></span><span title="'+text+'">'+text+'</span>', leaf: true, cls: 'file', iconCls: iconCls, checked: checked};
      groupItems.push(item);
    }
  },
  
  /**
   * Private
   * 
   * Mise � jour des groupes.
   */
  _updateStateGroups: function(item){
    if(item.items){
      for (var i=0, len=item.items.length ; i<len ; i++) {
        this._updateStateGroups(item.items[i])
      }
      if(!item.opened){
        this.tree.store.getNodeById(this.CLASS_NAME.replace(/\./g, "") + item.index.toString()).collapse();
      }     
    }
  },
  
  /**
   * Private
   * 
   * Ajout de l'item permettant de modifier la visibilit� d'une couche.
   */
  _addToolsVisibility: function(node,checked){
    var self = this;
    
    var descartesNodeIndex = node.raw.id.substring(this.CLASS_NAME.replace(/\./g, "").length);
    var itemDescartes = this.model.getItemByIndex(descartesNodeIndex);
    
    if(descartesNodeIndex === "" || itemDescartes instanceof Descartes.Group){
      for (var i=0, len=node.childNodes.length ; i<len ; i++) {
        this._addToolsVisibility(node.childNodes[i], checked);
      }
    }else {
      var uiNodeParent = document.getElementById(node.raw.id+'Visibility');
      if(uiNodeParent != null && uiNodeParent.childNodes.length === 0){
        var isChecked = checked ? checked : itemDescartes.visible;
        
        var uiNodeVisibility = document.createElement('span');
        
        var className;
        if (!itemDescartes.acceptMaxScaleRange()) {
            className = this.displayClasses.layerVisibilityNeedZoomOut;
          } else if (!itemDescartes.acceptMinScaleRange()) {
            className = this.displayClasses.layerVisibilityNeedZoomIn;
          } else {
            className = (isChecked ? this.displayClasses.layerVisibilityOn : this.displayClasses.layerVisibilityOff);
            uiNodeVisibility.onclick=function(){self.toggleLayerVisibilityByDescartesIconChanged(node, descartesNodeIndex);};
          }
        
        var message;
        
        if (itemDescartes.isInScalesRange()) {
            message = (isChecked ? this.getMessage("HIDE_LAYER") : this.getMessage("SHOW_LAYER"));
          } else {
            if (itemDescartes.minScale !== null && itemDescartes.maxScale !== null) {
              message = this.getMessage("MIN_SCALE") + Descartes.Utils.readableScale(itemDescartes.minScale) + this.getMessage("MAX_SCALE") + Descartes.Utils.readableScale(itemDescartes.maxScale);
            } else {
              message = (itemDescartes.minScale !== null ? this.getMessage("MIN_SCALE_ONLY") + Descartes.Utils.readableScale(itemDescartes.minScale) : this.getMessage("MIN_SCALE_ONLY") + Descartes.Utils.readableScale(itemDescartes.maxScale));
            }
          }
        
        uiNodeVisibility.className=className;
        uiNodeVisibility.title = message;
        
        uiNodeParent.appendChild(uiNodeVisibility);
      }
    }
  },
  
  /**
   * Private
   * 
   * Ajout de l'item permettant de modifier l'interrogation d'une couche et l'affichage des donn�es associ�es.
   */
  _addToolsQueryable: function(node,checked){
    var self = this;
    
    if(node.childNodes.length !== 0){
      for (var i=0, len=node.childNodes.length ; i<len ; i++) {
        this._addToolsQueryable(node.childNodes[i],checked);
      }
    }else { 
      var uiNodeQueryable = document.getElementById(node.raw.id+'Queryable');
      if(uiNodeQueryable != null && uiNodeQueryable.childNodes.length === 0){
      
        var descartesNodeIndex = node.raw.id.substring(this.CLASS_NAME.replace(/\./g, "").length);
        var itemDescartes = this.model.getItemByIndex(descartesNodeIndex);
        
        var isChecked = checked ? checked : itemDescartes.visible;
        
        var uiNodeInfo = document.createElement('span');
        
        if (itemDescartes.queryable) {
          if (isChecked && itemDescartes.isInScalesRange()) {
            className = (itemDescartes.activeToQuery ? this.displayClasses.layerInfoEnabled : this.displayClasses.layerInfoDisabled);
            message = (itemDescartes.activeToQuery ? this.getMessage("DISABLE_QUERY") : this.getMessage("ENABLE_QUERY"));
            uiNodeInfo.onclick = function() {self.toggleLayerQueryInfo(descartesNodeIndex);};
          } else {
            className = this.displayClasses.layerInfoForbidden;
            message = this.getMessage("FORBIDDEN_QUERY");
          }
        } else {
            className = this.displayClasses.blankBlocQuote;
            message = this.getMessage("NO_QUERY");
        }
        uiNodeInfo.className = className;
        uiNodeInfo.title = message;
        uiNodeQueryable.appendChild(uiNodeInfo);
  
        var uiNodeSheet = document.createElement('span');
        if (itemDescartes.sheetable) {
          if (isChecked && itemDescartes.isInScalesRange()) {
            className = this.displayClasses.layerSheetActive;
            message = this.getMessage("ACTIVE_SHEET");
            uiNodeSheet.onclick = function() {self.showDatasLayer(descartesNodeIndex);};
          } else {
            className = this.displayClasses.layerSheetInactive;
            message = this.getMessage("INACTIVE_SHEET");
          }
        } else {
            className = this.displayClasses.blankBlocQuote;
            message = this.getMessage("NO_QUERY");
        }
        uiNodeSheet.className = className;
        uiNodeSheet.title = message;
        
        uiNodeQueryable.appendChild(uiNodeSheet);
      }
    }
  },
  
  /**
   * Private
   * 
   * Ajout de l'item permettant de modifier l'opacit� d'une couche (slider).
   */
  _addToolsSlider: function(node){
    var self = this;
    
    if(node.childNodes.length !== 0){
      for (var i=0, len=node.childNodes.length ; i<len ; i++) {
        this._addToolsSlider(node.childNodes[i]);
      }
    }else { 
      var uiNodeSlider = document.getElementById(node.raw.id+'Slider');
      if(uiNodeSlider !== null && uiNodeSlider.childNodes.length === 0){
        var nodeId = node.raw.id;
        var descartesNodeIndex = nodeId.substring(this.CLASS_NAME.replace(/\./g, "").length);
        var itemDescartes = this.model.getItemByIndex(descartesNodeIndex);
                
        new Ext.Slider({
          renderTo  : node.raw.id+'Slider',
          width     : 50,
          height    : 18,
          ctCls     : 'ie-class',
          value     : itemDescartes.opacity,
          increment : 1,
          minValue  : 0,
          maxValue  : itemDescartes.opacityMax,
          listeners : {
            drag : function(slider, event){
              var sliderContainerId = slider.container.id;
              var item = self.model.getItemByIndex(sliderContainerId.substring(self.CLASS_NAME.replace(/\./g, "").length, sliderContainerId.indexOf('Slider')));
              self.changeOpacity(item.index, slider.getValue());
            },
            changecomplete : function(slider, newValue){
              var sliderContainerId = slider.container.id;
              var item = self.model.getItemByIndex(sliderContainerId.substring(self.CLASS_NAME.replace(/\./g, "").length, sliderContainerId.indexOf('Slider')));
              self.changeOpacity(item.index, newValue);
            }
          }
        });
        Ext.DomQuery.jsSelect('span[id*="'+nodeId+'Slider"]')[0].children[0].style.display='inline-block';
        var aHrefs = Ext.DomQuery.jsSelect('a[class*="x-tree-node-anchor"]');
        for (var i=0, len=aHrefs.length ; i<len ; i++) {
          aHrefs[i].href = '#';
        }
      }
    }
  },
  
  /**
   * Private
   * 
   * Ajout de l'item permettant d'afficher le lien de m�tadonn�e d'une couche.
   */
  _addToolsMetadataUrl: function(node){
    var self = this;
    var nodeId = node.raw.id;
    
    var uiNodeMetadataUrl = document.getElementById(nodeId+'MetadataUrl');
    
    var descartesNodeIndex = nodeId.substring(this.CLASS_NAME.replace(/\./g, "").length);
    
    var itemDescartes = this.model.getItemByIndex(descartesNodeIndex);
    
    if(node.childNodes.length !== 0){
      for (var i=0, len=node.childNodes.length ; i<len ; i++) {
        this._addToolsMetadataUrl(node.childNodes[i]);
      }
    }else if(uiNodeMetadataUrl !== null && uiNodeMetadataUrl.childNodes.length === 0 && itemDescartes.metadataURL !== null){
    
      var a = document.createElement('a');
      a.href = itemDescartes.metadataURL.replace(/&amp;/g,"&");
      a.target = "_blank";
      
      var uiNodeLien = document.createElement('span');
      uiNodeLien.className = this.displayClasses.layerLienMetadata;
      uiNodeLien.title = this.getMessage("LIEN_METADATA");
      a.appendChild(uiNodeLien);
      
      uiNodeMetadataUrl.appendChild(a);
    } 
  },
  
  /**
   * Private
   * 
   * D�placement d'un noeud.
   */
  onDropItem: function(node, newParent, index, event) {
    var dropNodeIndex = node.raw.id.replace(this.CLASS_NAME.replace(/\./g, ""), "");
    
    if(newParent.raw !== undefined){
      var newParentId = newParent.raw.id.replace(this.CLASS_NAME.replace(/\./g, ""), "");
      
      var movedItem = this.model.getItemByIndex(dropNodeIndex);
      var newParentItem = this.model.getItemByIndex(newParentId);
      
      var targetId;
      
      if(newParentId === ""){
        targetId = ""+index;
      }else{
        targetId = newParentId+"#"+index;
      }
      
      if(newParentId === "" && index === 0){
        this.model.reaffectItem(dropNodeIndex,targetId , "Top");
      }else if(newParentId === "" && index === this.model.item.items.length ){
          targetId = this.model.item.items.length-1;
          targetId = ""+ targetId;
          this.model.reaffectItem(dropNodeIndex,targetId , "Bottom");
      }else if (newParentId === ""){
        this.model.reaffectItem(dropNodeIndex, targetId, "Top");
      }else if (index === newParentItem.items.length){
        targetId = newParentItem.items.length-1;
        targetId = newParentId+"#"+ targetId;
        this.model.reaffectItem(dropNodeIndex,targetId , "Bottom");
      }else{
        this.model.reaffectItem(dropNodeIndex, targetId, "Top");
      }
    }else{
      this.model.reaffectItem(dropNodeIndex,"0" , "Top");
    }
  },
  
  /**
   * Private
   * 
   * D�placement d'un noeud: ajout dans un groupe.
   */
  onDropAppendItem: function(newParent, node, eOpts) {
    var dropZoneId = this.tree.viewConfig.plugins[0].cmp.plugins[0].dropZone.overRecord.raw.id.replace(this.CLASS_NAME.replace(/\./g, ""), "");
    
    var dropNodeIndex = node.raw.id.replace(this.CLASS_NAME.replace(/\./g, ""), "");
    var newParentId = newParent.raw.id.replace(this.CLASS_NAME.replace(/\./g, ""), "");
    
    if(dropZoneId === newParentId){
      this.tree.suspendEvents();
      
      
      var movedItem = this.model.getItemByIndex(dropNodeIndex);
      var groupItem;
      
      if(newParentId !== ""){
        groupItem = this.model.getItemByIndex(newParentId);
      
        var titleTarget = groupItem.title;
        var itemsTarget = groupItem.items.slice(0);
        
        this.model.removeItemByIndex(dropNodeIndex);
        groupItem = this._getGroupItemTarget(this.model.item.items, titleTarget, groupItem);
        if(groupItem.items.length === 0){
          this.model.addItem(movedItem, groupItem);
        }else{
          this.model.insertItemBefore(movedItem, groupItem.items[0]);
        }
      }else{
        this.model.removeItemByIndex(dropNodeIndex);
        this.model.insertItemBefore(movedItem, this.model.item.items[0]);
      }
      this.tree.resumeEvents();
    }
  },
  
  /**
   * Private
   * 
   * R�cup�ration du groupe cible lors du d�placement d'un noeud.
   */
  _getGroupItemTarget: function(items, titleTarget, oldGroupItem){
    for (var i=0, len=items.length ; i<len; i++) {
      if(items[i].title === titleTarget && items[i].CLASS_NAME === "Descartes.Group" && items[i].items.length === oldGroupItem.items.length){
        var groupTargetOk = true;
        for (var j=0, len=items[i].items.length ; j<len; j++) {
          if(items[i].items[j].title !== oldGroupItem.items[j].title){
            groupTargetOk = false;
            break;
          }
        }
        if(groupTargetOk){
          return items[i];
        }
      }else if(items[i].CLASS_NAME === "Descartes.Group"){
        var newGroup = this._getGroupItemTarget(items[i].items, titleTarget, oldGroupItem);
        if(newGroup !== undefined){
          return newGroup;
        }
      }
    }
  },
  
  /**
   * Methode: openCloseGroup
   * Inverse la pr�sentation du groupe entre les affichages "pli�" et "d�pli�".
   * 
   * Param�tres:
   * nodeIndex - {String} Index hi�rarchique du groupe dans l'arborescence.
   * state - {String} Etat du groupe.
   */
  openCloseGroup: function(nodeIndex, state) {
    var item = this.model.getItemByIndex(nodeIndex);
    if(item != undefined){
      if(state === 'expanded'){
        item.opened = true;
      }else{
        item.opened = false;
      }
    }
  },
  
  /**
   * Methode: toggleLayerVisibility
   * Inverse la visibilit� du groupe ou de la couche, avec �ventuelle propagation ascendante et descendante.
   * 
   * Param�tres:
   * node - {DOMElement} Element DOM concern�.
   */
  toggleLayerVisibilityByCheckBoxChanged: function(node, checked) {
    var nodeId = node.raw.id;
    
    var spanParentVisibility = Ext.DomQuery.jsSelect('span[id*="'+nodeId+'Visibility"]');
    var item = this.model.getItemByIndex(nodeId.substring(this.CLASS_NAME.replace(/\./g, "").length));
    item.setVisibility(checked);
    
    if(spanParentVisibility.length != 0 
        && (spanParentVisibility[0].children[0].className === this.displayClasses.layerVisibilityOn 
            || spanParentVisibility[0].children[0].className === this.displayClasses.layerVisibilityOff)){
      
      var className = (item.visible ? this.displayClasses.layerVisibilityOn : this.displayClasses.layerVisibilityOff);
      spanParentVisibility[0].children[0].className = className;
      
          var self = this;
          var descartesNodeIndex = node.raw.id.substring(this.CLASS_NAME.replace(/\./g, "").length);
            var nextSpan = Descartes.Utils.getNextSiblings(spanParentVisibility[0]);
         
          if(nextSpan.length !== 0 && nextSpan[0].children !== 0
                  && (nextSpan[0].children[0].className===this.displayClasses.layerInfoDisabled
                          || nextSpan[0].children[0].className===this.displayClasses.layerInfoEnabled
                          || nextSpan[0].children[0].className===this.displayClasses.layerInfoForbidden)){
             
              var spanInfo = nextSpan[0].children[0];
              spanInfo.onclick = function() {};
             
              var queryableClassName=  this.displayClasses.layerInfoForbidden;
              if(item.visible && item.isInScalesRange() && item.queryable){
                  if (item.activeToQuery){
                      queryableClassName=  this.displayClasses.layerInfoEnabled;
                      spanInfo.onclick = function() {self.toggleLayerQueryInfo(descartesNodeIndex);};
                  }else {
                      queryableClassName=  this.displayClasses.layerInfoDisabled;       
                      spanInfo.onclick = function() {self.toggleLayerQueryInfo(descartesNodeIndex);};
                  }
              }
              var spanInfo = nextSpan[0].children[0].className = queryableClassName ;
          }
         
          if(nextSpan.length !== 0 && nextSpan[0].children !== 0
                  && (nextSpan[0].children[1].className===this.displayClasses.layerSheetActive
                          || nextSpan[0].children[1].className===this.displayClasses.layerSheetInactive)){
             
              var spanSheetable = nextSpan[0].children[1];
              spanSheetable.onclick = function() {};
             
              var queryableClassName=  this.displayClasses.layerSheetInactive;
              if(item.visible && item.isInScalesRange()){
                  if (item.sheetable){
                      queryableClassName=  this.displayClasses.layerSheetActive;
                     
                     
                      spanSheetable.onclick = function() {self.showDatasLayer(descartesNodeIndex);};
                  }
              }
              spanSheetable.className = queryableClassName ;
          }
    }
    
        this.model.events.triggerEvent("itemPropertyChanged");
  },
  
  /**
   * Methode: toggleLayerVisibility
   * Inverse la visibilit� du groupe ou de la couche, avec �ventuelle propagation ascendante et descendante.
   * 
   * Param�tres:
   * node - {DOMElement} Element DOM concern�.
   * nodeDescartesIndex - {String} Index hi�rarchique du groupe ou de la couche dans l'arborescence.
   */
  toggleLayerVisibilityByDescartesIconChanged: function(node, nodeDescartesIndex) {
    var item = this.model.getItemByIndex(nodeDescartesIndex);
    item.setVisibility(!item.visible);
    this.tree.getView().onCheckChange(node, !node.get('checked'));
  },
    
  /**
   * Methode: toggleLayerQueryInfo
   * Inverse la capacit� � �tre interrog�e de la couche.
   * 
   * Param�tres:
   * nodeIndex - {String} Index hi�rarchique de la couche dans l'arborescence.
   */
  toggleLayerQueryInfo: function(nodeIndex) {
    var item = this.model.getItemByIndex(nodeIndex);
    item.activeToQuery = !item.activeToQuery;
    this.draw();
  },
  
  /**
   * Methode: changeOpacity
   * Modifie l'opacit� de la couche.
   * 
   * Param�tres:
   * nodeIndex - {String} Index hi�rarchique de la couche dans l'arborescence.
   * value - {Integer} Valeur de l'opacit�.
   */
  changeOpacity: function(nodeIndex, value) {
    var item = this.model.getItemByIndex(nodeIndex);
    this.model.changeOpacity(item, value);
  },
  
  /**
   * Methode: showDatasLayer
   * Affiche les propri�tes de l'ensemble des objets d'une couche.
   * 
   * :
   * Lance l'appel au service d'interrogation gr�ce � la classe <Descartes.Action.AjaxSelection>.
   * 
   * Param�tres:
   * nodeIndex - {String} Index hi�rarchique de la couche dans l'arborescence.
   */
  showDatasLayer: function(nodeIndex) {
    var item = this.model.getItemByIndex(nodeIndex);

    var bbox = item.OL_layers[0].map.maxExtent.toArray();
    var maxBBOX = "<Box srsName=\"" + item.OL_layers[0].map.projection + "\"><coordinates>" + bbox[0] + ',' + bbox[1] + ' ' + bbox[2] + ',' + bbox[3] + "</coordinates></Box>";

    var dataMask = "<BBOX><PropertyName>msGeometry</PropertyName>" + maxBBOX + "</BBOX>";

    var fluxInfos = Descartes.ExternalCallsUtils.writeQueryPostBody(Descartes.ExternalCallsUtils.WFS_SERVICE, [item], item.OL_layers[0].map);

    if (fluxInfos !== null) {
      var requestParams = {distantService:Descartes.FEATURE_SERVER, infos:fluxInfos, dataMask:dataMask, withReturn:this.withReturn, withCsvExport:this.withCsvExport};
      
      var action = new Descartes.Action.AjaxSelection(this.resultUiParams, requestParams, item.OL_layers[0].map);
    }
  },
  
  /**
   * Methode: selectItem
   * S�lectionne un groupe ou une couche pour servir de base aux fonctions de modification de l'arborescence.
   * 
   * Param�tres:
   * node - {DOMElement} El�ment DOM portant le titre du groupe ou de la couche.
   */
  selectItem: function(node) {
    if (node) {
      var descartesNodeIndex = node.raw.id.substring(this.CLASS_NAME.replace(/\./g, "").length);

      if (this.selectedItem != descartesNodeIndex) {
        this.selectedItem = descartesNodeIndex;
        
        if(this.selectedItem !== ""){
          if (this.model.getItemByIndex(this.selectedItem).CLASS_NAME === "Descartes.Group") {
            this.events.triggerEvent("groupSelected");
          } else {
            this.events.triggerEvent("layerSelected");
          }
        }else{
          this.events.triggerEvent("rootSelected");
        }
        
        this.tree.getSelectionModel().select(node);
      } else {
        this.selectedItem = "";
        this.tree.getSelectionModel().deselect(node);
        this.events.triggerEvent("nodeUnselect");
      }
    } else {
      this.selectedItem = "";
      this.events.triggerEvent("nodeUnselect");
    }
  },
  
  CLASS_NAME: "Descartes.UI.LayersTreeExtJS5",

  VERSION : "3.3"
});
