/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.SizeSelectorInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'une taille de carte dans une liste.
 * 
 * :
 * Le formulaire de saisie est constitu� d'une seule liste d�roulante.
 * 
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS4>
 * 
 * 
 * Ev�nements d�clench�s:
 * changeSize - La taille s�lectionn�e dans la liste a chang�.
 */
Descartes.UI.SizeSelectorInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {

	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de choix.
	 */
	label: true,
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
	height:60,
	
	/**
	 * Propriete: widthComboBox
	 * {Integer} Taille des comboBox.
	 */
	widthComboBox: 200,
	
    EVENT_TYPES: ["changeSize"],
	
	/**
	 * Constructeur: Descartes.UI.SizeSelectorInPlaceExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
	 * sizeSelectorModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.SizeSelector>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 * widthComboBox - {Integer} Taille des comboBox.
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
	 */
	initialize: function(div, sizeSelectorModel,options) {
		Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, sizeSelectorModel, this.EVENT_TYPES,options]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone de la page HTML pour le choix de la taille.
	 * 
	 * Param�tres:
	 * sizeList - {Array(OpenLayers.Size)} Liste des tailles propos�es.
	 * defaultSize - {Integer} Index de la taille courante de la carte dans la liste des tailles.
	 */
	draw: function(sizeList, defaultSize) {
		var self = this;
		
		var title = '';
		if(this.label){
			title = this.getMessage("TITLE_MESSAGE");
		}
		
		var optionsSelect = [];
		for (var indexSize=0, len=sizeList.length ; indexSize < len ; indexSize++) {
			var mapSize = sizeList[indexSize].w + "x" + sizeList[indexSize].h;
			var option = [indexSize, mapSize];
			optionsSelect.push(option);
		}
		
		 var optsPanel = {
	        xtype        : 'panel',
	        renderTo     : this.div.id,
	        autoScroll   : true,
	        id           : 'DescartesSelectSize',
	        frame        : true,
	        title        : title,
	        items        : [
		   			            {
		   			                displayField  : 'text',
		   			                valueField    : 'value',
		   			                value 		  : defaultSize,
		   			                typeAhead     : true,
		   			                queryMode          : 'local',
		   			                triggerAction : 'all',
		   					        xtype         : 'combo',
		   					        width: this.widthComboBox,
		   			                id            : 'selectSizeSelector',
		   			                editable      : false,
		   		            		emptyText     : this.getMessage("DEFAULT_OPTION_MESSAGE"),		                
		   			                store         : new Ext.data.SimpleStore({
		   						                         fields :['value', 'text'],
		   						                         data   : optionsSelect
		   					                        }),
					   			        listeners     : {
										   select: function(combo, record, index){
											   self.done(record[0].data.text);
										   }
										}
		   			            }
		   	]
	    };
		
		this.panel =  new Ext.Panel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));

	    this.panel.show();
	    
	    if(!(this.label && this.panel.collapsible && this.panel.collapsed)){
        	this.modifyStyle();
        }
	},
	
	modifyStyle: function() {
 		var h = this.height;
 		if(!this.label){
 			h -= 25;
 		}
 		this.panel.getEl().dom.style.height=h+"px";
	},
	
    /**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'changeSize'.
     * 
     * Param�tres:
     * control - {DOMElement} Liste d�roulante de choix de la taille dans le formulaire.
     */
	done: function(value) {
		var dims = value.split("x");
 		this.model.size = new OpenLayers.Size(dims[0],dims[1]);
 		this.events.triggerEvent("changeSize");	
	},
	
	/**
	 * Methode: selectSize
	 * S�lectionne une taille parmi celles disponibles et encha�ne avec la m�thode <done>.
	 * 
	 * :
	 * Cette m�thode a vocation � �tre utilis�e pour d�clencher un changement de taille demand� par un autre moyen que la s�lection gr�ce � la vue (� partir de la restauration d'un contexte de consultation par exemple).
     * 
     * Param�tres:
     * width - {Integer} Largeur souhait�e de la carte.
     * height - {Integer} Hauteur souhait�e de la carte.
	 */
	selectSize: function(width, height) {
		var control = document.getElementById("DescartesSelectSize").selectSizeSelector;
		var options = control.options;
		var newSizeIndex = -1;
		for (var i=0, len= options.length ; i<len ; i++) {
			var mapSize = width + "x" + height;
			if (options[i].text === mapSize) {
				newSizeIndex = i;
				break;
			}
		}
		if (newSizeIndex != -1) {
			this.done(mapSize);
		}
	},

	CLASS_NAME: "Descartes.UI.SizeSelectorInPlaceExtJS4",

	VERSION : "3.2"
});
