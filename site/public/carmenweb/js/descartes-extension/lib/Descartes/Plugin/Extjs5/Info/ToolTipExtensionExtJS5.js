/**
 * Class: Descartes.Info.ToolTipExtensionExtJS5
 * Classe d�finissant une info-bulle s'affichant en cas d'inactivit� du pointeur.
 * 
 * H�rite de:
 *  - OpenLayers.Control
 */
Descartes.Info.ToolTipExtensionExtJS5 = OpenLayers.Class(Descartes.Info.ToolTip, {
	
	/**
	 * Propriete: defaultHandlerOptions
	 * {Object} Objet JSON stockant les propri�t�s par d�faut du OpenLayer.Handler.Hover associ�.
     * 
     * :
     * delay - 2000
     * pixelTolerance - null
     * stopMove - true
	 */
	defaultHandlerOptions: {
		'delay': 2000,
		'pixelTolerance': 1,
		'stopMove': true
	},

	/**
	 * Constructeur: Descartes.Info.ToolTipExtensionExtJS5
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant l'info-bulle.
	 * OL_map - {OpenLayers.Map} Carte OpenLayers concern�e.
	 * toolTipLayers - {Array(Object)} Tableau d'objets JSON repr�sentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
	 * 
	 * Options de construction propres � la classe:
	 * handlerOptions - {Object} Objet JSON stockant les propri�t�s du OpenLayer.Handler.Hover associ�.
	 * resultUiParams - {Object} Objet JSON stockant les propri�t�s n�cessaires � la g�n�ration de l'info-bulle.
	 */
	initialize: function(div, OL_map, toolTipLayers, options) {
		Descartes.Info.ToolTip.prototype.initialize.apply(this, arguments);
/*
		var mapControls = this.map.controls;
		var mousePositioneClass = Descartes.ViewManager.getView(Descartes.ViewManager.LOCALIZED_MOUSE_POSITION);
		for (var i=0 ; i<mapControls.length ; i++) {
			if (mapControls[i] instanceof mousePositioneClass) {
				this.mousePosition = mapControls[i];
				break;
			}
		}
*/
	},

	
	/**
	 * Methode: onPause
	 * Lance l'appel au service d'interrogation gr�ce � la classe <Descartes.Action.AjaxSelection>, si le pointeur est suffisamment longtemps inactif.
	 * 
	 * Param�tre:
	 * evt - {<OpenLayers.Event>} �v�nement d�clench� par le handler associ� .
	 */
	onPause: function(evt) {
		if (app.toolTipsEnabled) {
			var pixel = new OpenLayers.Pixel(evt.xy.x, evt.xy.y);
			var fluxPixel = "#X=" + pixel.x.toString() + "#Y=" + pixel.y.toString();
	
			if (this.map !== null && this.toolTipLayers.length !== 0) {
				var layers = [];
				for (var iLayer=0 ; iLayer<this.toolTipLayers.length ; iLayer++) {
					if(this.toolTipLayers[iLayer].layer.isVisible() && this.toolTipLayers[iLayer].layer.activeToToolTip === true){
						layers.push(this.toolTipLayers[iLayer].layer);
					}
				}
					
				var fluxInfos = Descartes.ExternalCallsUtils.writeQueryPostBody(Descartes.ExternalCallsUtils.WMS_SERVICE, layers,this.map);
				if (fluxInfos !== null) {
          var requestParams = {distantService:Descartes.FEATUREINFO_SERVER, infos: fluxInfos, pixelMask:fluxPixel};
	
					pixel.x = evt.clientX;
					pixel.y = evt.clientY;
				
					var action = new Descartes.Action.AjaxSelection(this.resultUiParams, requestParams, this.map,{pixel:pixel,toolTipLayers: this.toolTipLayers});
				}
			}
		}
	},
	
	CLASS_NAME: "Descartes.Info.ToolTipExtensionExtJS5",

	VERSION : "3.1"
});