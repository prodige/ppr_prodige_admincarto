/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.BookmarksInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, un ensemble de pictogrammes permettant de g�rer des vues personnalis�es.
 * 
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS4>
 * 
 * 
 * Ev�nements d�clench�s:
 * sauvegarder - La cr�ation d'une nouvelle vue est demand�e.
 * supprimer - La suppression d'une vue est demand�e.
 * recharger - Le chargement d'une vue est demand�.
 */
Descartes.UI.BookmarksInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {

    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML du panneau de l�gendes.
     * 
     * :
     * globalClassName - ("DescartesBookmarks" par d�faut)
	 * labelClassName - ("DescartesBookmarksLabel" par d�faut)
	 * imgLoadClassName - ("DescartesBookmarksLoad" par d�faut)
	 * imgOpenClassName - ("DescartesBookmarksOpen" par d�faut)
	 * imgRemoveClassName - ("DescartesBookmarksRemove" par d�faut)
     */
	defaultDisplayClasses: {
		globalClassName: "DescartesBookmarks",
		labelClassName: "DescartesBookmarksLabel",
		imgLoadClassName: "DescartesBookmarksLoad",
		imgOpenClassName: "DescartesBookmarksOpen",
		imgRemoveClassName: "DescartesBookmarksRemove"
	},
	
	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 */
	label: true,
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
	height:65,
	
	EVENT_TYPES: ["sauvegarder","supprimer","recharger"],
	
	/**
	 * Constructeur: Descartes.UI.BookmarksInPlaceExtJS4
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
	 * bookmarksModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.BookmarksManager>.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 * 
	 * Options de construction propres � la classe:
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
	 */
	initialize: function(div, bookmarksModel, options) {
		Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, bookmarksModel, this.EVENT_TYPES, options]);
	},
	
	/**
	 * Methode: draw
	 * Construit la zone de la page HTML pour le gestionnaire de vues.
	 */
 	draw: function() {
		this.clearDiv();
		var self = this;
		var title = '';
		var items = [];
		
		var divContent = document.createElement('div');
		
		if (this.label) {
				title = this.getMessage("TITLE_MESSAGE");
		} 
		
		if (this.model.vues.length !== 0) {
			
			for (var iVue=0 ; iVue<this.model.vues.length; iVue++) {
				var vueDiv =  document.createElement('div');
				vueDiv.id = "DescartesUIVue"+iVue;
				vueDiv.className = this.displayClasses.globalClassName;
				
				var vueTitle = Descartes.DomUtils.createLabelSpan(this.model.vues[iVue].nom,this.displayClasses);
				if (this.model.vues[iVue].maxDate !== undefined && this.model.vues[iVue].maxDate !== null) {
					vueTitle.title = this.getMessage("MAX_DAY_PREFIX") + this.model.vues[iVue].maxDate;
				}
				vueDiv.appendChild(vueTitle);
				
				var imgLoad = document.createElement('div');
				imgLoad.className = this.displayClasses.imgLoadClassName;
				imgLoad.title = this.getMessage("RELOAD_VIEW");
				imgLoad.onclick = function() {self.doneRecharger(this);};
				vueDiv.appendChild(imgLoad);
				
				if (this.model.vues[iVue].url !== null) {
					var a = document.createElement('a');
					a.href = this.model.vues[iVue].url;
					a.target = "_blank";
					
					var imgOpen = document.createElement('div');
					imgOpen.className = this.displayClasses.imgOpenClassName;
					imgOpen.title = this.getMessage("OPEN_VIEW");
					imgOpen.border = "0";
					a.appendChild(imgOpen);
					vueDiv.appendChild(a);
				}
				
				var imgRemove = document.createElement('div');
				imgRemove.className = this.displayClasses.imgRemoveClassName;
				imgRemove.title = this.getMessage("DELETE_VIEW");
				imgRemove.onclick = function() {self.doneSupprimer(this);};
				vueDiv.appendChild(imgRemove);
				
				divContent.appendChild(vueDiv);
			}
 		}
		
		var optsPanel = {
	        xtype        : 'panel',
	        renderTo     : this.div.id,
	        autoScroll   : true,
	        id           : 'panelBookmarks',
	        frame        : true,
	        title        : title,
	        contentEl    : divContent,
		    buttons      : [{
		        text     : this.getMessage("BUTTON_MESSAGE"),
		        id       : "descartesBookmarksButton",
		        handler  : function() {
		        	self.doneSauvegarder(this);	        		
		        }
		    }],
		    buttonAlign  : 'center'
	    };
		
		var panel =  new Ext.Panel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));
		
		panel.show();		
		 
		 if(!(this.label && panel.collapsible && panel.collapsed)){
        	this.modifyStyle();
         }
 	},
 	
 	modifyStyle: function() {
		if (this.model.vues.length === 0) {
			var h = this.height;
	 		if(!this.label){
	 			h -= 25;
	 		}
			Ext.getCmp('panelBookmarks').getEl().dom.style.height=h+"px";
		}
	},
 	
    /**
     * Methode: doneSauvegarder
     * Demande la cr�ation d'une vue.
     * 
     * :
     * Cette m�thode est appel�e suite � un clic sur le bouton "Enregistrer la vue courante".
     * 
     * :
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'sauvegarder'.
     */
 	doneSauvegarder: function(){
 		this.events.triggerEvent("sauvegarder");
 	},
 	
    /**
     * Methode: doneRecharger
     * Demande le rechargement d'une vue.
     * 
     * :
     * Cette m�thode est appel�e suite � un clic sur le pictogramme "Recharger" correspondant � la vue souhait�e.
     * 
     * :
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'recharger'.
     * 
     * Param�tres:
     * elt - {DOMElement} Pictogramme "Recharger" correspondant � la vue souhait�e.
     */
 	doneRecharger: function(elt){
		var num = elt.parentNode.id;
		this.model.numVue = num.replace("DescartesUIVue","");
 		this.events.triggerEvent("recharger");
 	},
 	
    /**
     * Methode: doneSupprimer
     * Demande la suppression d'une vue.
     * 
     * :
     * Cette m�thode est appel�e suite � un clic sur le pictogramme "Supprimer" correspondant � la vue souhait�e.
     * 
     * :
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'supprimer'.
     * 
     * Param�tres:
     * elt - {DOMElement} Pictogramme "Supprimer" correspondant � la vue souhait�e.
     */
 	doneSupprimer: function(elt){
		var num = elt.parentNode.id;
		this.model.numVue = num.replace("DescartesUIVue","");
 		this.events.triggerEvent("supprimer");
 	},
 	
 	CLASS_NAME: "Descartes.UI.BookmarksInPlaceExtJS4",

	VERSION : "3.2"
});

