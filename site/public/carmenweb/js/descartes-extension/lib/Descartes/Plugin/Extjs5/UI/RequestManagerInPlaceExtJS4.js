/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.RequestManagerInPlaceExtJS4
 * Classe proposant, dans une zone fixe de la page HTML, la saisie de crit�res de requ�tes.
 *
 * H�rite de:
 *  - <Descartes.UI.AbstractInPlaceExtJS4>
 *
 * Ev�nements d�clench�s:
 * sendRequest - Les crit�res d'une requ�te sont saisis.
 * unselect - Demande l'effacement de la s�lection pr�c�dente.
 */
Descartes.UI.RequestManagerInPlaceExtJS4 = OpenLayers.Class(Descartes.UI.AbstractInPlaceExtJS4, {
   
    EVENT_TYPES: ["sendRequest","unselect"],
   
	/**
	 * Propriete: label
	 * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des requ�tes.
	 */
	label: true,
	
	/**
	 * Propriete: optionsPanel
	 * {Objet} Options du panel.
	 */
	optionsPanel:{},
	
	heightMember: 30,
	heightForm:70,
	heightPanel: 65,
    
    /**
     * Constructeur: Descartes.UI.RequestManagerInPlaceExtJS4
     * Constructeur d'instances
     *
     * Param�tres:
     * div - {DOMElement|String} El�ment DOM de la page accueillant le formulaire de saisie.
     * requestManagerModel - {Object} Mod�le de la vue pass� par r�f�rence par le contr�leur <Descartes.Action.RequestManager>.
     * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 *
	 * Options de construction propres � la classe:
	 * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des requ�tes.
	 * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceExtJS4>).
     */
    initialize: function(div, requestManagerModel, options) {
        Descartes.UI.AbstractInPlaceExtJS4.prototype.initialize.apply(this, [div, requestManagerModel, this.EVENT_TYPES, options]);
	},
	
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des crit�res de requ�te.
     *
     * Param�tres:
     * requests - {Array(<Descartes.Request>)} Liste des requ�tes propos�es.
     */
    draw: function(requests) {
        this.clearDiv();

        if (requests.length !==0) {
            var self = this;
            var mdl = this.model;    
            var items = [];  
            var formsHeight=[];
            for (var indexRequest=0 ; indexRequest<requests.length ; indexRequest++) {               
                var request = requests[indexRequest];
                var itemsForm = [];
                var formHeightTotal=this.heightForm;
                for (var indexMember=0,len=request.members.length ; indexMember<len ; indexMember++) {	
                    var member = request.members[indexMember];
                    if (member.visible) {
                    	formHeightTotal+=this.heightMember;
                        var itemForm = null;
                        if (member.list) {
                        	formHeightTotal+=5;
                            var optionsSelect = [];
                            var values = member.value;
                            values.forEach(
                                    function(value) {                      
                                        var option = [value.toString(),value];
                                        optionsSelect.push(option);
                                    }
                               );
                              
                            itemForm = {
                                    fieldLabel    : member.title,
                                    //labelSeparator : " :",
                                    displayField  : 'text',
                                    valueField    : 'value',
                                    typeAhead     : true,
                                    queryMode          : 'local',
                                    triggerAction : 'all',
                                    height		  : this.heightMember,
                                    xtype         : 'combo',
                                       //
                                       name          :'selectRequest'+indexRequest+'_'+indexMember,
                                       //
                                    editable      : false,
                                    id            : 'selectRequest'+indexRequest+'_'+indexMember,
                                    emptyText     : this.getMessage("INACTIVE_SELECT_TITLE"),                       
                                    store         : new Ext.data.SimpleStore({
                                                        fields :['value', 'text'],
                                                        data   : optionsSelect
                                                   })
                            };
                        } else {
                            itemForm = {
                                    fieldLabel    : member.title,
                                    height		  : this.heightMember,
                                    xtype         : 'field',
                                    id            : 'selectRequest'+indexRequest+'_'+indexMember
                            };
                        }
                        itemsForm.push(itemForm);
                    }
                }

                var form = {
                    xtype        : 'form',
                    id           : 'DescartesUIRequest'+indexRequest,
                    frame        : true,
                    renderTo     : this.div.id,
                    title        : request.title,
                    height		 : formHeightTotal,
                    width		 : 290,
                    items        : itemsForm,
                    buttons      : [{
                        text     : this.getMessage("BUTTON_MESSAGE_RECHERCHE"),
                        handler  : function() {

                            self.done(this);                   
                        }
                    }],
                    buttonAlign  : 'center'
                };
                
                items.push(form);
                formsHeight.push(formHeightTotal);
                
            }
           
            var title = '';
            
            if(this.label){
                title = this.getMessage("TITLE_MESSAGE");
            }
            
            var optsPanel = {
                renderTo     : this.div.id,
                title        : title,
                autoScroll   : true,
                id           : 'panelRequests',
                frame        : true,
                items        : items,
                buttons      : [{
                    text     : this.getMessage("BUTTON_MESSAGE_UNSELECT"),
                    handler  : function() {
                        self.events.triggerEvent("unselect");              
                    }
                }],
                buttonAlign  : 'center'
            };
            
            var panel =  new Ext.Panel(OpenLayers.Util.extend(optsPanel,this.optionsPanel));

            panel.show();
            
            if(!(this.label && panel.collapsible && panel.collapsed)){
            	this.modifyStyle(panel,formsHeight);
            }
        }
    },
    
    modifyStyle: function(panel,formsHeight) {
    	
    	var panelHeightTotal=this.heightPanel;
    	if(!this.label){
    		panelHeightTotal -= 25;
 		}
        
        for (var i=0,len=panel.items.items.length ; i<len ; i++) {
        	panelHeightTotal+=formsHeight[i];
        	panel.items.items[i].getEl().dom.style.height=formsHeight[i]+"px";
        }
        
        panel.getEl().dom.style.height=panelHeightTotal+"px";
    	
    },
   
    /**
     * Methode: done
     * Transmet le mod�le au contr�leur, en d�clenchant l'�v�nement 'sendRequest'.
     *
     * Param�tres:
     * elt - {DOMElement} Bouton de recherche activ� dans le formulaire.
     */
    done: function(elt) {
        var form = elt.up('form').getForm();
        var fieldValues = form.getFieldValues();
       
        var values = [];
        var selectValue;
        for (var i=0,len=elt.findParentByType('form').getForm().items.length ; i< len; i++) {
            values[i] = fieldValues[elt.findParentByType('form').getForm().items[i].name];
        }
       
        this.model.values = values;
       
        var num = elt.findParentByType('form').id;
        this.model.numRequest = num.replace("DescartesUIRequest","");
       
        this.events.triggerEvent("sendRequest");
    },
   
   
    CLASS_NAME: "Descartes.UI.RequestManagerInPlaceExtJS4",

    VERSION : "3.2"
});