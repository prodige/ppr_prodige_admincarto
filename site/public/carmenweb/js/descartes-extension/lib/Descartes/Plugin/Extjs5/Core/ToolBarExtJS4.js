/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.ToolBarExtJS4
 * Classe d�finissant une barre d'outils, pouvant accueillir des boutons de type <Descartes.Button> et/ou <Descartes.Tool>.
 */
Descartes.ToolBarExtJS4 = OpenLayers.Class({

	/**
	 * Propriete: OL_map
	 * {OpenLayers.Map} Carte OpenLayers r�agissant aux boutons de la barre d'outils.
	 */
	OL_map: null,

	/**
	 * Propriete: OL_panel
	 * {OpenLayers.Control.Panel} Panneau OpenLayers construit par la barre d'outils.
	 */
	OL_panel: null,
	
	/**
	 * Propriete: toolBarName
	 * {String} Le nom de la barre d'outils.
	 */
	toolBarName:"",
	
	toolBarOpener:null,
	
	tools:[],
	
	visible: true,
	
	/**
	 * Propriete: title
	 * {String} Titre de la barre d'outils.
	 */
	title: "",
	
	closable: false,
	
	window:null,
	
	height:60,
	
	/**
	 * Propriete: width
	 * {Integer} Longueur de la barre d'outils.
	 */
	width:350,
	
	events: null,
	EVENT_TYPES: ["closed"],

	/**
	 * Constructeur: Descartes.ToolBar
	 * Constructeur d'instances
	 * 
	 * Param�tres:
	 * div - {DOMElement|String} El�ment DOM de la page accueillant la barre d'outils.
	 * OL_map - {OpenLayers.Map} Carte OpenLayers r�agissant aux boutons de la barre d'outils.
	 * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance
	 *
	 * Options de construction propres � la classe:
	 * title - {String} Titre de la barre d'outils.
	 */
	initialize: function(div, OL_map, options) {
		var self = this;
   		this.OL_map = OL_map;

   		this.toolBarName=Descartes.ToolBarExtJS4.TEMPLATE_NAME+Descartes.ToolBarExtJS4.ID;
   		Descartes.ToolBarExtJS4.ID++; 		
   		
		if (options !== undefined) {
			OpenLayers.Util.extend(this, options);
		}
		this.OL_panel = new OpenLayers.Control.Panel();
		var divToolBar = document.createElement('div');
		divToolBar.className = this.CLASS_NAME.replace(".", "").replace("ExtJS4","");
		
		this.OL_panel.div = divToolBar;
		if (OL_map !== null) {
			this.OL_map.addControl(this.OL_panel);
		}
		
		if ( div === undefined || div === null) {
			this.window = new Ext.Window({
				cls         : this.CLASS_NAME.replace(".", ""),
				autoScroll  : true,
				minWidth	: 42,
				minHeight	: 42,
				width	: this.width,
				height	: this.height,
				modal  		: false,
				resizable   : true,
				draggable   : true,
				contentEl	: this.OL_panel.div,
				title 		: this.title,
				closable 	: this.closable,
				constrain	: true,
				shadow		: false
			});
			
			this.window.show();
			if (this.closable) {
				this.window.on("close", this.removeControls, this);
			}

			this.window.alignTo(this.OL_map.div, "tl-tl?");

			this.modifyStyle(divToolBar);
			
			this.events = new OpenLayers.Events(this, null, this.EVENT_TYPES);

		} else {
			div = Descartes.Utils.getDiv(div);
			div.appendChild(divToolBar);
			var divClr = document.createElement('div');
			divClr.className = "clr";
			div.appendChild(divClr);
		}
	},
	
	modifyStyle: function(div) {
		if(div.ancestors !== undefined && div.ancestors !== null){
			div.ancestors()[0].style.width= "";
			div.ancestors()[0].style.height= "";
			div.ancestors()[1].style.width= this.width+"px";
			div.ancestors()[1].style.height= this.height+"px";
			div.ancestors()[1].firstDescendant().style.width= this.width+"px";
		} else {
			this.window.getEl().dom.style.height=this.height+"px";
			this.window.getEl().dom.style.width=this.width+"px";
			this.window.getEl().dom.children[0].style.width=this.width+"px";
		}
	},
	
	/**
	 * Methode: addControls
	 * Ajoute plusieurs boutons � la barre d'outils.
	 * 
	 * Param�tres:
	 * control - {Array(Descartes.Button|Descartes.Tool)} Boutons � ajouter.
	 */
	addControls: function(controls) {
		if ( !(controls instanceof Array)) {
			controls = [controls];
		}
		for (var i=0 ; i<controls.length ; i++) {
			this.addControl(controls[i]);
		}
	},
	
	/**
	 * Methode: addControl
	 * Ajoute un bouton � la barre d'outils.
	 * 
	 * Param�tres:
	 * control - {Descartes.Button|Descartes.Tool} Bouton � ajouter.
	 */
	addControl: function(control) {
		this.OL_panel.addControls(control);
		control.parentPanel = this.OL_panel;
		for (var i=0, len=this.OL_panel.controls.length ; i<len ; i++) {
			var aControl = this.OL_panel.controls[i];
			if (aControl.enabled) {
				aControl.enable();
			}
			if (control instanceof Descartes.Button) {
				this.OL_map.events.register('moveend', control, control.refresh);
			}
		}
	},
	
	/**
	 * Methode: removeControls
	 * Supprime plusieurs boutons � la barre d'outils.
	 */
	removeControls: function() {
		if (this.OL_panel.controls !== null) {
			for (var i=0, len=this.OL_panel.controls.length ; i<len ; i++) {
				this.OL_panel.controls[i].deactivate();
				this.OL_map.removeControl(this.OL_panel.controls[i]);
			}
		}
		this.OL_map.removeControl(this.OL_panel);
		this.visible=false;
		this.events.triggerEvent("closed");
	},
	
	alignTo: function(anchorDiv) {
		this.window.alignTo(anchorDiv);
	},
	
	/**
	 * Methode: destroy
	 * Supprime la barre d'outils.
	 */
	destroy: function() {
		this.window.close();
	},
	
	CLASS_NAME: "Descartes.ToolBarExtJS4",

	VERSION : "3.3"
});

Descartes.ToolBarExtJS4.TEMPLATE_NAME="DescartesToolBar";
Descartes.ToolBarExtJS4.ID=1;