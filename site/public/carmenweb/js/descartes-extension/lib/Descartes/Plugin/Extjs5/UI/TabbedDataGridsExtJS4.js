/*
Copyright MEDDE - MLETR
Contributeurs :
     Ga�lle Barris
     Denis Chabrier
     Christophe Bocquet
     Marc Regnault
     David Berger
     Thibaud Bioulac
	 Luc Boyer
	 Damien Despres
	 David Marquet
	 Thierry Baco

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: Descartes.UI.TabbedDataGridsExtJS4
 * Classe permettant d'afficher, sous forme d'onglets, un ensemble de tableaux de propri�t�s d'objets g�ographiques.
 *
 * H�rite de:
 * - <Descartes.UI>
 *
 * Ev�nements d�clench�s:
 * gotoFeatureBounds - La localisation rapide sur un objet est demand�e.
 *
 * Ecouteurs mis en place:
 *  - l'�v�nement 'gotoFeatureBounds' de la classe <Descartes.UI.DataGrid> d�clenche la m�thode <gotoBounds>.
 */
Descartes.UI.TabbedDataGridsExtJS4 = OpenLayers.Class(Descartes.UI, {
   
    /**
     * Propriete: defaultDisplayClasses
     * {Object} Objet JSON stockant les noms des classes CSS par d�faut des �l�ments HTML du panneau de l�gendes.
     * 
     * :
	 * panelHeaderExportClassName - ("DescartesPanelExportCsvButton" par d�faut)
     */
    defaultDisplayClasses: {
        panelHeaderExportClassName: "DescartesPanelExportCsvButton"
    },

    /**
     * Propriete: activePanel
     * {Integer} Index du tableau actif.
     */
    activePanel: null,

    /**
     * Propriete: withCsvExport
     * {Boolean} Indicateur pour la possibilit� d'exporter les tableaux sous forme de fichier CSV.
     */
    withCsvExport: false,
   
    /**
     * Private
     */
    formCsvExport: null,
   
    /**
     * Propriete: divPanelsContent
     * {Array(DOMElement)} El�ments DOM de la page contenant les tableaux de propri�t�s.
     */
    divPanelsContent: [],
   
    index: -1,
   
    EVENT_TYPES: ["gotoFeatureBounds","close"],

    /**
     * Constructeur: Descartes.UI.TabbedDataGridsExtJS4
     * Constructeur d'instances
     *
     * Param�tres:
     * div - {DOMElement|String} El�ment DOM de la page accueillant les onglets.
     * model - null.
     * options - {Object} Objet optionnel contenant les propri�t�s � renseigner dans l'instance.
     *
     * Options de construction propres � la classe:
	 * displayClasses - {Object} Objet JSON stockant les noms des classes CSS des �l�ments HTML de la zone d'affichage des mesures.
     * withCsvExport - {Boolean} Indicateur pour la possibilit� d'exporter les tableaux sous forme de fichier CSV.
     */
    initialize: function(div, model, options) {
        Descartes.UI.prototype.initialize.apply(this, [div, null, this.EVENT_TYPES, options]);
        Descartes.UI.TabbedDataGridsExtJS4.INDEX ++;
        this.index = Descartes.UI.TabbedDataGridsExtJS4.INDEX;
    },
   
    /**
     * Methode: draw
     * Construit et affiche les onglets et le premier tableau.
     *
     * Param�tres:
     * datasModel - {Object} Objet JSON contenant les propri�t�s des tableaux, de la forme
     * (start code)
     * {
     *         layerTitle:"Titre",
     *         layerMinScale:echelleMini,
     *         layerMaxScale:echellMaxi,
     *         layersDatas:{prop1:val1, prop2:val2, ...}[]
     * }[]
     * (end)
     */
    draw: function(datasModel) {
        this.model = datasModel;
        var self = this;
       
        var items = [];
		for (var i=0, len=this.model.length ; i<len ; i++) {
			var title = '';
			if (this.withCsvExport) {
				title = '<span class="' + this.displayClasses.panelHeaderExportClassName + '" id="export_' + this.index + '_' + i + '"></span>';
			}
			title += this.model[i].layerTitle + " (" + this.model[i].layerDatas.length + ")";
				
			if (this.model[i].layerDatas.length != 0) {
				items.push({
						id       : "tabs_" + this.index + "_" + i,
						xtype    : 'panel',
						closable : false,
						title    : title,
						layout: 'fit'
				});
			}
		}
        
        var tabPanel = new Ext.TabPanel({
        	id:"tabPanel_" + this.index,
            autoScroll : true,
            enableTabScroll : true,
            activeTab  : 0,
            items      : items,
            listeners  : {
                'tabchange' : function(tabPanel, tab){
                    self.showPanel(tabPanel, tab);
                },
                render: function() {
                    if (self.withCsvExport) {
                        this.items.each(function(i){
                             i.tab.on('click', function(){
                                var indexTab = i.id.replace("tabs_" + self.index + "_", "");
                                var indexTabActive = tabPanel.getActiveTab().id.replace("tabs_" + self.index + "_", "");
                                if(indexTab===indexTabActive){
                                    self.exportPanel(indexTab);
                                }
                                
                             });        
                        });
                    }
                }


            }
        });

        this.showPanel(tabPanel, tabPanel.getActiveTab());
       
        if(items.length>0){
        	
        	 var w = new Ext.Window({
                 id          : 'resultPopup_' + this.index,
                 modal       : false,
                 layout: 'fit',
                 height      : getWindowHeight()/2,
                 width       : getWindowWidth()/2,
                 items       : [tabPanel],
                 title        : this.getMessage("DIALOG_TITLE"),
                 closable    : true
             });

             w.on('beforeClose', function(panel, o) {
                 self.events.triggerEvent("close");
             });
             
             w.show();       
            
             if (this.withCsvExport && document.getElementById("exportCsvSubmit") === null) {
                 this.formCsvExport = document.createElement('form');
                 this.formCsvExport.id = "exportCsvSubmit";
                 this.formCsvExport.action = Descartes.EXPORT_CSV_SERVER;
                 this.formCsvExport.method = "POST";
                 var hiddenSubmit = document.createElement('input');
                 hiddenSubmit.type= "hidden";
                 hiddenSubmit.id = "CSVcontent";
                 hiddenSubmit.name = "CSVcontent";
                 this.formCsvExport.appendChild(hiddenSubmit);
                
                 document.body.appendChild(this.formCsvExport);
             }
        	
        }else{
	    	this.showAlert(this.getMessage("NO_RESULT"));
	    }
        
       
    },
   
    /**
     * Methode: showPanel
     * Affiche le tableau de l'onglet activ�.
     *
     * Param�tres:
     * tabPanel - {Ext.TabPanel} Le conteneur des onglets.
     * tab - {Ext.Panel} L'onglet s�lectionn�.
     */
    showPanel: function(tabPanel, tab) {
        
    	if(tab.id!==null && tab.id!==undefined){
    		var activeTabIndex = tab.id.replace("tabs_" + this.index + "_", "");
            
            this.activePanel = activeTabIndex;
           
            if(this.model[activeTabIndex].layerDatas.length>0){
                var dataGrid = new Descartes.UI.DataGridExtJS4(tabPanel, tab.contentEl, this.model[activeTabIndex].layerDatas);
                dataGrid.events.register('gotoFeatureBounds', this, this.gotoBounds);
            }
    	}
    	
    },
   
    /**
     * Methode: exportPanel
     * Exporte au format CSV les donn�es d'un tableau.
     *
     * Param�tres:
     * tabNum - {Integer} Index de l'onglet s�lectionn�.
     */
    exportPanel: function(tabNum) {
        var datas = this.model[tabNum].layerDatas;

        var csvStream = "";
        var headerRow = "";
        for (var colHeaderName in datas[0]) {
            if (colHeaderName !== undefined) {
                if ( !(colHeaderName === "bounds" && datas[0].bounds instanceof Array) ) {
                    headerRow += "\"" + colHeaderName + "\";";
                }
            }
        }
        csvStream += headerRow + "\r\n";

        for (var iRow=0, lenRows=datas.length ; iRow<lenRows ; iRow++) {
            var row = "";
            for (var colName in datas[iRow]) {
                if (colName !== undefined) {
                    if ( !(colName === "bounds" && datas[0].bounds instanceof Array) ) {
                        row += "\"" + datas[iRow][colName].replace(/"/g,'""') + "\";";
                    }
                }
            }
            csvStream += row + "\r\n";
        }
		document.getElementById("exportCsvSubmit").CSVcontent.value = csvStream;
		document.getElementById("exportCsvSubmit").submit();
    },
   
    /**
     * Methode: gotoBounds
     * Demande la localisation rapide sur un objet, en d�clenchant l'�v�nement 'gotoFeatureBounds'.
     *
     * Param�tres:
     * bounds - {Array(Float)} Coordonn�es de l'emprise de l'objet sous la forme [xMin, yMin, xMax, yMax].
     */
    gotoBounds: function(bounds) {
        this.events.triggerEvent("gotoFeatureBounds", [bounds, this.model[this.activePanel].layerMinScale, this.model[this.activePanel].layerMaxScale]);
    },
    
	/**
	 * Methode: showAlert
     * Affichage message.
     * 
     * Param�tres:
     * message - {String} le message a affich�.
	 */
	showAlert: function(message) {
		alert(message);
	},
   
    CLASS_NAME: "Descartes.UI.TabbedDataGridsExtJS4",

    VERSION : "3.2"
});

Descartes.UI.TabbedDataGridsExtJS4.INDEX = -1;
