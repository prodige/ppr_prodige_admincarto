var DYNAMIC_MAP = '0';
var STATIC_MAP = '1';
/**
 * 
 */
Ext.define('Prodige.InitMapPanel', {
  extend : 'Ext.form.Panel',
  uuid : null,
  
  standardSubmit : true,
  
  initComponent : function(){
  	console.log(13);
    var me = this;
    me.fieldDefaults = {width : '100%',padding : '10 10 0 10', labelAlign : 'right', labelWidth : 170, labelSeparator : ''};
    me.items = [{
      xtype : 'combo',
      name : 'cartp_format',
      store : {
        xtype: 'jsonstore',
        fields : ['displayField', 'valueField'],
        data : [{displayField : 'Carte interactive', valueField : DYNAMIC_MAP}, {displayField : 'Carte statique', valueField : STATIC_MAP}]
      },
      queryMode : 'local',
      displayField : 'displayField',
      valueField : 'valueField',
      editable : false,
      allowBlank : false,
      fieldLabel : 'Type de carte',
      readOnly : !!Carmen.carte,
      
      listeners : {
        change : function(combo, value){
        	var dynamic = value==DYNAMIC_MAP;
          me.down('[name=map_file]').setVisible( dynamic ).setDisabled( !dynamic ).allowBlank = !dynamic;
          me.down('[name=static_file]').setVisible( !dynamic ).setDisabled( dynamic );
          me.down('[name=static_file] > textfield').allowBlank = dynamic;
        }
      }
    }, {
      xtype : 'textfield',
      name : 'map_file',
      fieldLabel : 'Nom du fichier',
      vtype : 'identifier',
      allowBlank : false
    }, {
      name : 'static_file',
      xtype: 'fieldcontainer',
      width : '100%',padding : '10 10 0 10',
      fieldLabel: 'Fichier représentant la carte',
      layout: 'hbox',
      fieldDefaults : {padding:0},
      defaults : {margin:'0 0 0 5'},
      disabled : true,
      hidden : true,
      listeners : {
      	disabled : function(){this.cascade(function(item){item.setDisabled(true);})},
      	enabled : function(){this.cascade(function(item){item.setDisabled(false);})}
      },
      items: [{
        margin:0,
        name : 'data_file',
        xtype: 'textfield',
        allowBlank: true,
        maxLength: 512,
        enforceMaxLength: true,
        readOnly : true,
        flex: 11
      }, {
        xtype: 'button',
        text: 'Parcourir le serveur',
        flex: 2,
        name : 'browser',
        handler: function(){
          var browser = me.getBrowserWindow();
          browser.show();
        }
      }, {
        xtype: 'button',
        text: 'Téléverser et sélectionner',
        flex: 2,
        name : 'uploader',
        handler: function(){
          var browser = me.getBrowserWindow();
          var uploader = browser.fileBrowser.openUploader();
        }
      }]
    }];
    me.bodyPadding = 20;
    me.buttons = [{
      text : 'Valider',
      handler : function(){
        if ( !me.isValid() ) return;
        me.standardSubmit = true;
        me.submit({
        	url : Routing.generate('carmen_initialize_map', {uuid : me.uuid}),
        	method : 'POST'
        });
      }
    }, {
      text : 'Fermer',
      handler : function(){
      	window.close();
      }
    }]
    
    this.callParent(arguments);
    
    //LOAD RECORD
    var record = (Carmen.carte || {
      cartp_format : DYNAMIC_MAP,
      stkcard_path : ''
    });
    record.map_file = record.stkcard_path.replace(/\.map$/, '');
    record.data_file = record.stkcard_path;
    
    record.getData = function(key){return Ext.apply({}, this);};
    record.get = function(key){return this[key];};
    record.set = function(key, value){this[key] = value; return this;};
    
    this.loadRecord(record);
  },
  
  getBrowserWindow : function(){
  	var me = this;
    var windowClass = "Ext.ux.FileBrowserWindow";
    var browser = Ext.getCmp('browser') 
    || Ext.create(windowClass, {
      id: 'browser',
      selectExtension: '*.pdf,*.jpeg,*.jpg,*.png,*.gif,*.tif,*.tiff',
      uploadExtension: '.pdf,.jpeg,.jpg,.png,.gif,.tif,.tiff',
      dataUrl: Routing.generate('prodige_staticmap_browser', {routing:'noroute', uuid : me.uuid}),
      defaultPath: '/Root',
      relativeRoot: '/Root/Publication/CartesStatiques',
      listeners: {  
        selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
          var ctrlTo = me.down('[name=data_file]');
          ctrlTo.setValue(fullPathName);
          wind.close();
        }
      }
    });
    
    return browser;
  }
})


function InitMap(uuid){
  new Prodige.InitMapPanel({uuid : uuid, id : 'initmappanel', renderTo : 'main', width : '100%', title : 'Paramétrage de la carte'});
}
