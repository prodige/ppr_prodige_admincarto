//Carmen.Util.INFO_TOOL_XY_TOLERANCE = 5;
// i18next initialisation
i18n.init({
  lng: 'fr',
  fallbackLng: 'fr',
  preload: ['fr'],
  resGetPath: '/carmenweb/locales/__lng__/translation.json',
  debug: false,
  getAsync: false
}, function () {
  //init Carmen after loading i1_n  
  _t = i18n.t;
  requirejs(["build/carmen_carto"], function (util) {
    // launch init when ext is ready
    Ext.onReady(function () {
      init();
    });
  });
}
);