
/**
 * @_requires OpenLayers/Control/ZoomHistory.js
 */

/**
 * Class: Carmen.Control.ZoomHistory
 
 * Inherits from:
 *  - <OpenLayers.Control.NavigationHistory>
 */

Carmen.Control.ZoomHistory = {};

Carmen.Control.ZoomHistory = new OpenLayers.Class(OpenLayers.Control.NavigationHistory, {
    
    // Static constants defining 
    // the number of buttons to display
    PREV_BTN : 1,
    NEXT_BTN : 2,
    BOTH_BTN : 3,
    
    displayConfig : 3,

    btnPrevious: Ext.create('Ext.button.Button', {
      xtype : 'button',
      tooltip: 'Zoom Précédent',      
      tooltipType: 'qtip',
      cls: 'x-btn-icon', 
      enableToggle: false,
      disabled: true, // zoom prev btn is not enabled just after init
      text : '<i class="fa falk-last-zoom fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
    }),
  
    btnNext: Ext.create('Ext.button.Button', {
      xtype : 'button',
      tooltip: 'Zoom Suivant',
      tooltipType: 'qtip',
      cls: 'x-btn-icon',
      enableToggle: false,
      disabled: true, // idem for zoom next
      text : '<i class="fa falk-prev-zoom fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
    }),
  
  
  initialize: function(options) {
    OpenLayers.Control.NavigationHistory.prototype.initialize.apply(this, arguments);
    
    // extending previous zoom control to enable/disable ext btn    
    var btnP = this.btnPrevious;
    var olControlP = this.previous;
    this.previous = OpenLayers.Util.extend(olControlP, {
      activate : function () {
        OpenLayers.Control.prototype.activate.apply(this, arguments);
        btnP.setDisabled(!olControlP.active);
      }
    });
    // idem for the next zoom control 
    var btnN = this.btnNext;
    var olControlN = this.next;
    this.next = OpenLayers.Util.extend(olControlN, {
      activate : function () {
        OpenLayers.Control.prototype.activate.apply(this, arguments);
        btnN.setDisabled(!olControlN.active);
      }
    });
  },
  
  setMap: function(map) {
      OpenLayers.Control.NavigationHistory.prototype.setMap.apply(this, arguments);
      
      if ((this.displayConfig & this.PREV_BTN) > 0) {
        this.btnPrevious.addListener('click', Carmen.Util.buildExt2olHandlerButton(this.previous));
      }
      if ((this.displayConfig & this.NEXT_BTN) > 0) {
        this.btnNext.addListener('click', Carmen.Util.buildExt2olHandlerButton(this.next));
      }
      this.activate();
  },
    
  getButtons: function() {
    var res = [];
    if ((this.displayConfig & this.PREV_BTN) > 0) {
      res.push(this.btnPrevious);
    }
    if ((this.displayConfig & this.NEXT_BTN) > 0) {
      res.push(this.btnNext);
    }
    return res;
  }, 
    
  CLASS_NAME: "Carmen.Control.ZoomHistory"
});

Carmen.Control.ZoomHistory.PREV_BTN = 1;
Carmen.Control.ZoomHistory.NEXT_BTN = 2;
Carmen.Control.ZoomHistory.BOTH_BTN = 3;
