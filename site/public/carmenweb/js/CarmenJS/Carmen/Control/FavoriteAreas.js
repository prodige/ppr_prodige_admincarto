/**
 * @_requires OpenLayers/Control.js
 */
_T_FAVORITE_AREAS = function(key){
	if ( typeof window._t != "undefined" ){
		var root = "FavoriteAreas.";
		var translation = _t(root + key);
    return (translation==(root + key) ? key : translation);
	}
	return eval('Carmen.Control.FavoriteAreas.translations.'+key) || key;
}
/**
 * Class: Carmen.Control.FavoriteAreas
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.FavoriteAreas = new OpenLayers.Class(OpenLayers.Control, {

	cbShowEditor : null,
  cbOnSelectItem : null,
  cbOnEndEdit : null,
	cbOnDeleteItem : null,
	
	tagItemCloseSelector : '.favoritearea-item-close',
  initialize : function(contextInit,options) {
  	var me = this;
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
    
    this.areaStore = Ext.create('Ext.data.JsonStore', {
      fields: ['areaName', 'value'],
      data : [//['Zones Favorites',''],
                ]
    });  
    this.areaBox = Ext.create('Ext.form.ComboBox', {
      autoLoad: false,
      queryMode: 'local',
      triggerAction: 'all',
      emptyText: _T_FAVORITE_AREAS("emptySelection"),
      // allowBlank : false,
      id : 'favoriteAreasCombo',
      displayField: 'areaName',
      valueField: 'value',
      listConfig : { 
      	listeners : {
      		beforeitemclick : function(list, record, listEl, index, evt, eOpts ){
      			if ( evt.getTarget(me.tagItemCloseSelector) ){
      				Ext.Msg.confirm(
      				  _T_FAVORITE_AREAS("delete.title"), 
      				  new Ext.XTemplate(_T_FAVORITE_AREAS("delete.text")).apply(record.data),
      				  function(btn){
      				    if ( btn=="yes" ){
      				      (me.cbOnDeleteItem || me.onDeleteItem).apply(me, [record, index]);
      				    }
      				  }
      				)
      				return false;
      			}
      		}
      	},
        loadingText: _T_FAVORITE_AREAS("loadingText"), 
        itemTpl: [ '<div data-qtip="{areaName}" class="favoriteAreaSelectIcon"><span>{areaName}</span><span class="'+me.tagItemCloseSelector.substring(1)+'"><i class="fa fa-times"></i></span></div>' ]
      },
      editable : false,
      disableKeyFilter : false,
      valueNotFoundText : Carmen.Control.FavoriteAreas.emptyFavoriteAreaTxt,
      maxWidth: 155,
      //flex: 70,
      store : this.areaStore 
    });
    
    this.favoriteBtn = Ext.create('Ext.button.Button', {
        id: 'gearButton',
        text: '<i class="fa fa-star fa-1x"></i>',
        scale: 'small',
        //flex: 20,
        maxWidth: 25,
        height: 25,
        margin: '0 5 0 5',
        padding: 0,
        tooltip: _T_FAVORITE_AREAS("add.title")
    });
    
    this.favoriteBtn.setHandler(
      function(event, toolEl, panelHeader) {
        var func = ( me.cbShowEditor || me.showWindow )
        func.apply(me);
      },
      me
    );
    
    this.areaBox.addListener('select',
      function(store, record, rowIndex) {
      	var r
    	  if ( me.cbOnSelectItem ){ 
    	    r = me.cbOnSelectItem.apply(me, arguments);
    	  } else {
    	  	r = me.onSelectItem(store, record, rowIndex);
    	  }
    	  me.areaBox.setValue(null);
    	  return r;
      }, me);
      
    if (contextInit!=null)
      this.initFromContext(contextInit);     
      
    // obedel sample carmen 2015
    //this.initFromContext([{name : 'zone favorite 1', BoundingBox: {attributes : {minx:0, miny:0, maxx:0, maxy:0}} }]);
    
    this.component = Ext.create('Ext.form.FieldContainer', 
    {
      layout: 'hbox',
      width : '100%',
      disabled: true,
      items : [this.areaBox,
               this.favoriteBtn]
    });
      
  },

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    
    var map = this.map;
    var control = this;
    
    this.configureStore(map)
    // enabling component when added to the map
    this.component.enable();
    
  },
  
  configureStore : function(map) {
  	//nothing to do
  },
  
  getComponent : function() {
    return this.component;
  },
  
  showWindow : function(){
    var control = this;
    Ext.Msg.prompt(Carmen.Control.FavoriteAreas.newFavoriteAreaTxt, _T_FAVORITE_AREAS("add.field"), (control.cbOnEndEdit || control.onEndEdit), control);
  },
  
  onSelectItem : function(store, record, rowIndex) {
    if (record.data.areaName == Carmen.Control.FavoriteAreas.newFavoriteAreaTxt) {
      ( this.cbShowEditor || this.showWindow ) ();
    }
    else if (record.data.areaName != Carmen.Control.FavoriteAreas.emptyFavoriteAreaTxt && record.data.areaName !='') {
      this.map.zoomToExtent(record.data.value);
    }
  },
  
  onEndEdit : function(btn, value) {
    var map = this.map;
    if (btn=='ok') {
      var gbmName = value.length>0 ? value : 
        Carmen.Control.FavoriteAreas.defaultName + 
        Carmen.Control.FavoriteAreas.getUniqueId().toString();
      this.areaBox.getStore().loadData([[gbmName, map.getExtent()]], true);
    }
  },
  
  onDeleteItem : function(record) {
    this.areaBox.getStore().remove(record);
  },
  
  serializeForContext : function() {
    var bookmarks = [];
    var store = this.areaBox.getStore();

    for (var i=0; i<store.getCount(); i++) {
      var rec = store.getAt(i);
      var extent = rec.get('value');

      var bookmark = {
        name : rec.get('areaName'),
        BoundingBox : {
          attributes : {
            minx : extent.left.toString(),
            maxx : extent.right.toString(),
            miny : extent.bottom.toString(),
            maxy : extent.top.toString()
          }
        }
      }
      bookmarks.push(bookmark);
    }
    return {GeoBookmark : bookmarks} ;
  },

  initFromContext : function(bookmarks) {
  	return;
    var store = this.areaBox.getStore();
    // cleaning the store
    for (var i=1; i<store.getTotalCount();i++) {
      store.removeAt(i);
    }
    // loading new bookmarks
    if (!(bookmarks instanceof Array))
      bookmarks = [bookmarks];    
    //console.log(bookmarks);
    Ext.define('Bookmark', {
      extend: 'Ext.data.Model',
      fields: [
        {name: 'areaName'},
        {name: 'value'}
      ]
    });
      
    for (var i=0; i<bookmarks.length;i++) {
      var rec = Ext.create('Bookmark', {
        areaName: bookmarks[i].name,
        value: Carmen.Util.convertContextBBtoolBounds(bookmarks[i].BoundingBox)
      });
      store.add(rec);
    }
  },

  CLASS_NAME: "Carmen.Control.FavoriteAreas"

});

Carmen.Control.FavoriteAreas.translations = {
   "loadingText" : "Chargement...",
   "emptySelection" : "Zones favorites",
   "delete" : {
     "title" : "Suppression d'une zone favorite",
     "text" : "Confirmez-vous la suppression définitive de la zone favorite {areaName} ?"
   },
   "add" : {
     "title" : "Ajouter la zone actuelle à vos favoris",
     "field" : "Nom de la zone à créer"
   },
   "msg" : {
     "cannot_create" : "Echec de création de la zone favorite."
   }
};

Carmen.Control.FavoriteAreas.defaultName = 'zone_';
Carmen.Control.FavoriteAreas.uniqueId = 0;
Carmen.Control.FavoriteAreas.getUniqueId = function() {
   Carmen.Control.FavoriteAreas.uniqueId++;
   return (Carmen.Control.FavoriteAreas.uniqueId - 1);
};
// misc constants
Carmen.Control.FavoriteAreas.emptyFavoriteAreaTxt = _T_FAVORITE_AREAS("emptySelection");
Carmen.Control.FavoriteAreas.newFavoriteAreaTxt = _T_FAVORITE_AREAS("add.title");
