  /**
 * @_requires OpenLayers/Control.OverviewMap.js
 */

/**
 * Class: Carmen.Control.ReferenceMap
 
 * Inherits from:
 *  - <OpenLayers.Control.OverviewMap>
 */

Carmen.Control.ReferenceMap = new OpenLayers.Class(OpenLayers.Control.OverviewMap, {

  referencePanel : new Ext.create('Ext.Panel', {
    id: 'referencePanel',
    border: true,
    //collapsible: true,
    margins: '5 5 5 5',
    width: 220,
    height: 110,
    border: true,
    style: {
      bottom: '40px',
      right: '20px'
    },
     layout: {
        type: 'absolute'
    },
    items: [{
      xtype: 'button',
      id: 'referencePanelMinButton',
      text: '<i class="fa falk-angle-down fa-1x"></i>',
      cls: 'x-toolbar-item',
      style: {
        bottom: '0px',
        right: '0px',
        'z-index': 10000
      },
      enableToggle : true
    }]
    /*
    headerPosition: 'right',
    title: '<i class="fa fa-globe fa-1x"></i>',
    titleAlign: 'center',
    titleRotation: 0,
    
    header : {
      width: 30,
      layout: 'hbox',
      titlePosition: 1
    }*/
    
  }),



  initialize: function(layerRef, options) {
  // todo : change the way the default opt are handled, bcos mapOptions is overiden and not extended....
/*    
    options = OpenLayers.Util.extend(options,
      layerRef==null ? 
        { mapOptions : {numZoomLevels: 1} } :
        {
          mapOptions : {numZoomLevels: 1}, 
          layers : [layerRef]
        } 
    );
  */  
    
    this.layerRef = layerRef;
    OpenLayers.Control.OverviewMap.prototype.initialize.apply(this, [options]);
    this.displayClass = this.CLASS_NAME.replace("Carmen.", "cmn").replace(/\./g, "");
  },

  setMap: function(map) {
    this.map = map;
    var centerPanel = this.map.app.ui.getCenterPanel();
    centerPanel.add(this.referencePanel);
    
    var referencePanelButton = Ext.getCmp('referencePanelMinButton');
    referencePanelButton.on('toggle', 
      function(btn, state) {
        console.log(state);
        if (state==true) { // pressed for reduce
          //console.log('reducing');  
          this.referencePanel.setSize(29,24);
          btn.setText('<i class="fa falk-angle-up fa-1x"></i>');
        }
        else {
          //console.log('enlarging');  
          this.referencePanel.setSize(220,110);
          btn.setText('<i class="fa falk-angle-down fa-1x"></i>');
        }
      }, this);
    
    this.map.app.ui.doLayout();
    var id_div = this.referencePanel.body.id.replace('-body','-innerCt');
    this.div = Ext.get(id_div);
    this.outsideViewport = true;
    OpenLayers.Control.OverviewMap.prototype.setMap.apply(this, arguments);
     
  },

 
  draw: function() {
    OpenLayers.Control.OverviewMap.prototype.draw.apply(this, arguments);
    
    // adjusting heigth of the div
    var h = Math.min(this.size.h + 10,140);
    this.mapDiv.style.height = h.toString() + 'px';
    
    // and adding specific css
    var eltMapDiv = Ext.get(this.mapDiv);
    eltMapDiv.replaceCls('olMap','cmnControlOverviewMap');
 
    // overloading css
/*    var reference = Ext.get(this.referencePanel.body.id);
    var referenceMap = reference.child('.CarmenControlOverviewMapElement');
    referenceMap.addClass('cmnReferenceMapElement');
    var referenceMapRectangle = referenceMap.child('.CarmenControlOverviewMapExtentRectangle');
    referenceMapRectangle.addClass('cmnReferenceMapExtentRectangle');
*/    
    return this.div;
  },

  update : function() {
    OpenLayers.Control.OverviewMap.prototype.update.apply(this, arguments);

    // css overriding
    var eltMapDiv = Ext.get(this.mapDiv);
    eltMapDiv.replaceCls('olMap','cmnControlOverviewMap');
  },

  CLASS_NAME: "Carmen.Control.OverviewMap"

});
  
  
