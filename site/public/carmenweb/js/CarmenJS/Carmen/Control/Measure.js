/**
 * @_requires OpenLayers/Control/Measure.js
 */

/**
 * Class: Carmen.Control.Measure
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.Measure = new OpenLayers.Class(OpenLayers.Control, {

  measureTool : Ext.create('Ext.button.Selector', {
    id:'grpBtn-'+'measure',
    "class":'grpBtn',
    menuAlign: 'tl-bl?',
    arrowVisible: true,
    arrowCls: 'selector',
    arrowAlign: 'bottom', 
    text : '',
    tooltip : 'Mesures',     
    tooltipType: 'qtip',
    showText: true,
    width: ctrlWidth,
    height: ctrlHeight,
    enableToggle: false,
    toggleGroup: 'mainMapControl',
    //allowDepress: false,
    menu: {
    	cls : 'x-panel-body-default',
    	baseCls : 'x-toolbar',
      id: 'grpBtn-menu-'+'measure',
      style: { top: '-5px' },
      layout: { 
        type : 'hbox',
        align: 'stretch'
      },
      plain: true,
      minWidth: 30,
      items : []
    }
  }),
  
  msrLength : null,
  msrArea : null,

  
  btnLength: Ext.create('Ext.button.Button',{
    xtype : 'button',
    tooltip: 'Mesure de distance',      
    tooltipType: 'title',
    cls: 'x-toolbar-item x-btn-icon',
    enableToggle: true,
    //id : 'measure_distance',
    toggleGroup: 'mainMapControlMeasure',
    text: '<i class="fa falk-rule"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  }),

  btnArea: Ext.create('Ext.button.Button',{
    xtype : 'button',
    tooltip: 'Mesure de surface',      
    tooltipType: 'title',
    cls: 'x-toolbar-item x-btn-icon',
    enableToggle: true,
    //id : 'measure_surface',
    toggleGroup: 'mainMapControlMeasure',
    text: '<i class="fa falk-square fa-1x"></i><i class="fa falk-texture fa-stack-1x text-dark"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  }),
  
  
  
	buttonAdded : false,
  // Static constant flags defining 
  // which buttons to display
  LENGTH_BTN : 1,
  AREA_BTN : 2,
  BOTH_BTN : 3,
    
  displayConfig : 3,
  
  win : Ext.create('Ext.Window',{
    id : 'measure' +'_' +'window',
    width: 200,
    constrain : true,
    closeAction: 'hide',
    plain: true,
    title: '',
    hidden:true
  }),
  
  sketchSymbolizers : {
    "Point": {
      pointRadius: 4,
      graphicName: "square",
      fillColor: "white",
      fillOpacity: 1,
      strokeWidth: 1,
      strokeOpacity: 1,
      strokeColor: "#333333"
    },
    "Line": {
      strokeWidth: 3,
      strokeOpacity: 1,
      strokeColor: "#666666",
      strokeDashstyle: "dash"
    },
    "Polygon": {
      strokeWidth: 2,
      strokeOpacity: 1,
      strokeColor: "#666666",
      fillColor: "white",
      fillOpacity: 0.3
    }
  },
  initialize: function(options) {
    var control = this;       
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
    
    this.measureTool.disable();
     
    var style = new OpenLayers.Style();
    style.addRules([
      new OpenLayers.Rule({
        symbolizer: this.sketchSymbolizers
      })
    ]);
    
    var styleMap = new OpenLayers.StyleMap({
      "default": style
      }
    );
    var msrOptions = {
      handlerOptions: {
        style: "default",
        // this forces default render intent
        layerOptions: {
          styleMap: styleMap
        },
        persist: true
      }
    };
    
    if ((this.displayConfig & this.LENGTH_BTN) > 0) {
      this.msrLength = new OpenLayers.Control.Measure(OpenLayers.Handler.Path, msrOptions);
//        ui.addToCycleButtonGroup(this.toolbarId || this.map.app.context.getToolbarTarget('measure'), 'Mesure', 'measure', this.btnLength, 0, true);
      this.btnLength.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(control.msrLength));
      this.msrLength.events.on({ 
        "activate": function(evt) {//console.log(this.CLASS_NAME + ' length activated');
        },
        "measure": control.measureHandler,
        "measurepartial": control.measureHandler,
        "deactivate": function(evt) {/* console.log(this.CLASS_NAME + ' length deactivated');*/ control.win.hide(); },
        scope : control 
      });
        
    }
    
    if ((this.displayConfig & this.AREA_BTN) > 0) {
      this.msrArea = new OpenLayers.Control.Measure(OpenLayers.Handler.Polygon, msrOptions);
//        ui.addToCycleButtonGroup(this.toolbarId || this.map.app.context.getToolbarTarget('measure'), 'Mesure', 'measure', this.btnArea, 1, false);
      this.btnArea.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(control.msrArea));
      this.msrArea.events.on({ 
        "activate": function(evt) {/*console.log(this.CLASS_NAME + ' area activated')*/},
        "measurepartial": control.measureHandler,
        "measure": control.measureHandler,
        "deactivate": function(evt) {/*console.log(this.CLASS_NAME + ' area deactivated'); */control.win.hide(); },
        scope : control
      });
    }
  },
  
  deactivate : function() {
    var state = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    this.msrLength.deactivate();
    this.msrArea.deactivate();
    this.measureTool.toggle(false);
    this.measureTool.pressed = false;
  },
    
  getButton: function() {
    this.measureTool.menu.removeAll(false);
    if ((this.displayConfig & this.LENGTH_BTN) > 0) {
      this.measureTool.menu.add(this.btnLength);
    }
    
    if ((this.displayConfig & this.AREA_BTN) > 0) {
      this.measureTool.menu.add(this.btnArea);
    }
    this.measureTool.setActiveItem(this.measureTool.menu.items.get(0)); 
    return this.measureTool;
  }, 
  
  
  setMap: function(map) {
      OpenLayers.Control.prototype.setMap.apply(this, arguments);
      
    
      var ui = this.map.app.ui;
      var control = this;       
      if ((this.displayConfig & this.LENGTH_BTN) > 0) {
        this.map.addControl(this.msrLength);
      }
      if ((this.displayConfig & this.AREA_BTN) > 0) {
      	this.map.addControl(this.msrArea);
      }
      this.measureTool.enable();
  },
  
  measureFormatter : function(n) {
     //return (new Number(n)).toLocaleString();
     return (new Number(n)).toString();
  },
   
  measureHandler : function(event)  { 
      var geometry = event.geometry;
      var units = event.units;
      var order = event.order;
      var measure = event.measure;

      var out = "";
      var status = "";

      if (order == 1) {
        status = "Mesure de distance";
        out += "<b>&nbsp;Distance:&nbsp;" + this.measureFormatter(measure.toFixed(3)) + "&nbsp;" + units + "</b>";
      } 
      else {
        var a = this.map.getControlsByClass('Carmen.Control.Measure');
        var ctrl = a[0].msrArea;
        var length_m = ctrl.getLength(event.geometry, 'm');
        var area_m2 = ctrl.getArea(event.geometry, 'm'); 
      
        out += '<table height="auto" width="200" style="text-align: right; margin-right: 0px; ' +
             'height: 200px; width: 200px; overflow: auto;" ' +
             'border="0" cellpadding="1" cellspacing="1">' ;
        out += '<tbody>' ;
        out += '<tr>' ;
        out += '<td colspan="2" rowspan="1" style="width: 50px;text-align: left;">' + 
         'P&eacute;rim&egrave;tre</td>' ;
        out += '</tr>' ;
                   
        out += '<tr style="vertical-align:baseline;"><td style="width: 150px;"><b>' + this.measureFormatter(length_m.toFixed()) + '</b></td>' + 
          '<td style="text-align: left;"><b>&nbsp;&nbsp;m</b></td>' ;
        out += '<tr style="vertical-align:baseline;"><td style="width: 150px;"><b>' + this.measureFormatter((length_m /100.0).toFixed(2)) + '</b></td>' + 
          '<td style="text-align: left;"><b>&nbsp;&nbsp;hm</b></td>' ;
        out += '<tr style="vertical-align:baseline;"><td style="width: 150px;"><b>' + this.measureFormatter((length_m /1000.0).toFixed(3)) + '</b></td>' + 
          '<td style="text-align: left;"><b>&nbsp;&nbsp;km</b></td>' ;
   
        out += '<tr>' ;
        out +=  '<td colspan="2" rowspan="1" style="width: 50px;text-align: left;">' + 
            'Surface</td>' ;
        out += '</tr>' ;

        out += '<tr style="vertical-align:baseline;"><td style="width: 150px;"><b>' + this.measureFormatter(area_m2.toFixed()) + '</b></td>' + 
        '<td style="text-align: left;"><b>&nbsp;&nbsp;m²</b></td>' ;
        out += '<tr style="vertical-align:baseline;"><td style="width: 150px;"><b>' + this.measureFormatter((area_m2/10000.0).toFixed(2)) + '</b></td>' + 
        '<td style="text-align: left;"><b>&nbsp;&nbsp;ha </b></td>' ;
        out += '<tr style="vertical-align:baseline;"><td style="width: 150px;"><b>' + this.measureFormatter((area_m2/1000000.0).toFixed(3)) + '</b></td>' + 
        '<td style="text-align: left;"><b>&nbsp;&nbsp;km²</b></td>' ;
           
        out += '</tbody>' ;
        out += '</table>' ;
              
        status = "Mesure";      
      }
     
      this.win.setTitle(status);
      //this.win.body.dom.innerHTML = out;   
      this.win.update(out);
      this.win.show();

    },
    
  CLASS_NAME: "Carmen.Control.Measure"
});

Carmen.Control.Measure.LENGTH_BTN = 1;
Carmen.Control.Measure.AREA_BTN = 2;
Carmen.Control.Measure.BOTH_BTN = 3;
