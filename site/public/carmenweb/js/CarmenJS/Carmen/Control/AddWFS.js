/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.AddWFS
 * 
 * Inherits from: - <OpenLayers.Control>
 */

Carmen.Control.AddWFS = new OpenLayers.Class(OpenLayers.Control,
{
  // UI components
  btn :new Ext.Button( {
    //tooltip: 'Add WFS Layer',
    tooltip: 'Ajouter une couche WFS',
    tooltipType: 'title',
    cls: 'x-btn-icon cmn-tool-addwfs',
    enableToggle: true,
		toggleGroup: 'mainMapControl',
    disabled: false
  }),
  
  win : null,

  setMap: function(map) {
      OpenLayers.Control.prototype.setMap.apply(this, arguments);

      this.btn.addListener('toggle', 
	      Carmen.Util.buildExt2olHandlerToggle(this));    
      var tb = this.map.app.ui.getTopToolBar();
      //tb.addItem(this.btn);
      
      this.map.app.ui.doLayout();
  },
  
  activate: function() {
  	OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    this.showWindow();
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win && this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();
  },
    
  showWindow : function() {
    this.win = new Ext.Window({
      id : 'window_addWFSa',
      layout : 'fit',
      width : 550,
      constrain : true,
      plain : true,
      title: "Ajout de couches WFS",
      // shim :false,
      modal : false,
      x :150,
      y :100,
      autoDestroy : true,
      // monitorValid:true,
      resizable : true,
      autoLoad : {
        url : '/services/AddWFS/addWFS.php',
        loadingText: 'Chargement',
        params : {
          mapProjection : this.map.projection,
          extent : app.context.getMaxExtent().left.toString() + ','
          +app.context.getMaxExtent().bottom.toString() + ','
          +app.context.getMaxExtent().right.toString() + ','
          +app.context.getMaxExtent().top.toString()
          },
        method : 'GET',
        scripts : true
      }
    });
    
    // adding listeners to link window closing and control deactivation   
    var control = this;
    this.win.on(
      'hide',
      function(w) { control.btn.toggle(false); },
      this.win);
    
   this.win.show();
  },

  CLASS_NAME :"Carmen.Control.AddWFS"
});

Carmen.Control.AddWFS.addLayerToMap = function(lLayerName, lLayerMapfile, lLayerTitle, lLayerLegendUrl, 
 infoFields, briefFields, toolTipFields) {
  
  
  var ctx = app.context;
  var layer = new Carmen.Layer.MapServerGroup(
      lLayerName, 
      window.CARMEN_URL_SERVER_DATA + "/cgi-bin/mapserv?map=" + lLayerMapfile,
      {
        LAYERS : lLayerName,
        map_imagetype: 'png',
        map_transparent: true
      },
      {
        maxExtent: ctx.getMaxExtent(),
        projection: ctx.getProjection(),
        units: ctx.getProjectionUnits(),  
        isBaseLayer: false,
        singleTile: true,
        mapfile : lLayerMapfile
      });
  layer.setVisibility(true);
  
  // adding layer to the map 
  app.map.addLayer(layer);
  
  // adding mdata to context
	var parsedLegendURL = Carmen.Util.parseURL(lLayerLegendUrl);
	var parsedPathMapfile = Carmen.Util.parsePath(lLayerMapfile); 
	var parsedMapFilename = parsedPathMapfile ? Carmen.Util.parseFilename(parsedPathMapfile.end) : null ;
  var descs = Carmen.Util.OLWFSCarmenOWSDesc(
	 	layer,
	 	app.context.getNextLayerId(),
	 	{ 
	 	  projection : app.map.projection ,
	 	  server : parsedLegendURL ? parsedLegendURL.start : lLayerLegendUrl,
	 	  mapfile :  parsedMapFilename ? parsedMapFilename.basename : lLayerMapfile, 
	 	  layerName : lLayerName,
	 	  layerTitle : lLayerTitle,
	 	  infoFields : infoFields,
	 	  briefFields : briefFields,
	 	  toolTipFields : toolTipFields
	 	}
  );
  
    // adding listeners...
  var layerManager = app.map.getLayerTreeManager();
  
  var layerIdx = app.context.addLayerDescs(descs);
    
  // creating the node for the layerTreeManager
  
  var node = new Ext.tree.TreeNode({
    text : lLayerTitle,
    leaf : true,
    expanded : false,
    checked : true,
    icon : lLayerLegendUrl,
    iconCls : 'cmnLayerTreeLayerNodeIcon',
    rightIcon : '/IHM/images/delete.gif',
    rightIconCls : 'cmnTreeNodeRightIcon',
    iconDisplayStrategy : true,
//    uiProvider : Carmen.TreeNodeUI,
    listeners: {
        // drag'n drop
        'beforemove' : {
            fn: function(tree, node , oldParent, newParent, index) { 
                  this.moveLayerNode(tree, node , oldParent, newParent, index);
            },
            scope: layerManager,
            delay: 100
          }
      }
  });
  
  node.attributes.type = Carmen.Control.LayerTreeManager.NODE_LAYER;
  node.attributes.OlLayer = layer;
  node.attributes.visible = true;
  node.attributes.expanded = false;
  node.attributes.layerIdx = layerIdx;
  node.attributes.opacity = 1.0;
  node.attributes.forceOpacityControl = true;
  node.attributes.addedByUser = true;
  // mandatory setting for querying the layer
  node.attributes.infoFields = infoFields;
  node.attributes.briefFields = briefFields;
  node.attributes.toolTipFields = toolTipFields;
  node.attributes.baseQueryURL = "";
  node.attributes.layerType = "mapfile_wfs";
  
  // bottom elt for the slider...
  node.attributes.hasBottomElt = true; 
  

  //  node.addListener('checkchange', this.onNodeCheck, node);
  node.addListener('checkchange', 
      function(n, cb_state) {
        var layer = n.attributes.OlLayer;
        layer.setVisibility(n.attributes.checked);
      }, 
    node); 
  
  // inserting the node in the layer tree    
  var tree = layerManager.getLayerTree();
  if (tree.hasChildNodes()) {
    tree.insertBefore(node, tree.childNodes[0]);
  }
  else {
    tree.appendChild(node);
  }
  
  node.attributes.layerName = layer.params.LAYERS;
  node.attributes.visible = true;
  
  node.render();

  // adding tooltip
  rIconDom = node.getUI().getRightIcon();
  new Ext.ToolTip({
    target: rIconDom,
    html: 'Supprimer la couche WFS'
  });
  
  
  
  
  var rIconElt = Ext.fly(rIconDom);

  rIconElt.addListener('click',
    function(evt) {
       // removing the layer from the map
       var layer = node.attributes.OlLayer;
       var map = layer.map;
       map.removeLayer(layer);
       // removing the node from the tree
       node.remove();
       evt.stopPropagation();
       return false;       
     } 
  );
  
  Carmen.Control.LayerTreeManager.addTransparencySliderToNode(node);
};
