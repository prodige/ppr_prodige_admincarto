
/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: OpenLayers.Control.Inlay
 * Add inlay to the map
 *
 * Inherits from:
 *  - <OpenLayers.Control>
 */
 //TODO: put in Carmen namespace...
Carmen.Control.Inlay = new OpenLayers.Class(OpenLayers.Control, {
    
    /**
     * APIProperty: font property
     * 
     */
		// style property set at control initialisation
		// position is handled via css 
		font : null,
    fontFamily : 'tahoma,arial,helvetica,sans-serif',
    color : '#000000',
    fontSize : '12px',
    fontWeight : 'normal',
    fontStyle : 'normal',
    backgroundColor :  null,
    opacity : 1.0,
       

    /**
     * Constructor: OpenLayers.Control.Attribution 
     * 
     * Parameters:
     * options - {Object} Options for control.
     */

    initialize: function(options) {
        //console.log('here');
        OpenLayers.Control.prototype.initialize.apply(this, arguments);
        
        this.fontFamily = this.font ? this.font + ',' + this.fontFamily : this.fontFamily;   
        if (this.color && this.color.charAt(0)!='#') 
        	this.color = '#' + this.color;
        if (this.backgroundColor && this.backgroundColor.charAt(0)!='#')
        	this.backgroundColor = '#' + this.backgroundColor;
        // alpha in bgcolor desc ?
        if (this.backgroundColor && this.backgroundColor.length==9) {
        	this.backgroundColor = this.backgroundColor.substring(0,6);
        	this.opacity = parseInt(this.backgroundColor.substring(7,8),16) / 255;
        }
    },	

		setMap : function(map) {
		  //console.log('in setMap');
    	OpenLayers.Control.prototype.setMap.apply(this, arguments);
		},

    /**
     * Method: draw
     * Initialize control.
     * 
     * Returns: 
     * {DOMElement} A reference to the DIV DOMElement containing the control
     */    
    draw: function() {
			OpenLayers.Control.prototype.draw.apply(this, arguments);
			this.div.style.display = "block";
			this.div.style.position = "absolute";
			this.div.style.backgroundColor = this.backgroundColor;
			this.div.style.fontFamily = this.fontFamily;
			this.div.style.fontWeight = this.fontWeight;
			this.div.style.color = this.color;
			this.div.style.fontSize = this.fontSize;
			this.div.style.fontStyle = this.fontStyle;
			OpenLayers.Util.modifyDOMElement(this.div, null, null, null,
			                                 null, null, null, this.opacity);
			this.update();
			return this.div;    
		},

    /**
     * Method: update
     * 
     */
    update: function() {
    },

    CLASS_NAME: "Carmen.Control.Inlay"
});
