/**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.QueryAttributes
 * 
 * Inherits from: - <OpenLayers.Control>
 */

Carmen.Control.ExportImg = new OpenLayers.Class(OpenLayers.Control,{


      //TODO temporaire
      inGroup : true,
			// UI components
			btn : Ext.create('Ext.Button', {
			  tooltip: 'Export image',
		      tooltipType: 'title',
		      cls: 'x-btn-icon',/* cmn-tool-save*/
		      enableToggle: true,
		      toggleGroup: 'mainMapControlExport',
          checked: true,
          disabled: false,
          text : '<i class="fa fa-file-image-o fa-2x"></i>',
          width: ctrlWidth,
          height: ctrlHeight

			}),

			win :Ext.create('Ext.Window', {
				layout :'anchor',
				// layout:'fit',
				//width :400,
				//height :250,
        bodyPadding: 10,
        constrain : true,
				//plain :true,
				id : "winExport",
				title :'Export image',
				modal :false,
				autoDestroy :true,
				resizable :false,
				closeAction :'hide',
				//shadow :false
			}),
			printPanel : Ext.create('Ext.FormPanel', 
          {
            id :'ExportImg',
            //standardSubmit: true,
            autoScroll :true,
            //fitToFrame :true,           
            //bodyStyle :'padding:5px 5px 0; color: #000000;',
            //labelStyle :'color: #000000;',
            url :'/services/GetIMG/getIMG.php',
            
            items : [{
                  xtype: 'combo',
                    id: 'comboFormat',
                    name: 'format',
                    //labelWidth :75,
                    fieldLabel: 'Format',
                    editable: false,
                    listConfig : { loadingText: 'Chargement' },
                    emptyText: 'Sélectionnez un format',
                    triggerAction: 'all',
                    minChars: 1,
                    queryDelay: 250,     
                    queryMode: 'local',
                    store: new Ext.data.SimpleStore({
                        fields: ['qstring1', 'label'],
                        data : [[ 'Image PNG', 'Image PNG' ],
                          [ 'Image JPEG', 'Image JPEG' ],
                          [ 'Image PNG24', 'Image PNG24'  ]          
                        ]}
                    ),
                    validator: function(v) {
                      return (v!='Sélectionnez un format');
                    },
                    displayField: 'label',
                    allowBlank: false,
                    valueField: 'qstring1',
                   // width: 180
                },
                {
                    xtype: 'combo',
                    id: 'comboWidth',
                    name: 'size',
                   // labelWidth :75,
                    fieldLabel: 'Taille de l\'image',
                    editable: false,
                    listConfig: { loadingText: 'Chargement' },
                    emptyText: 'Sélectionnez une taille',
                    triggerAction: 'all',
                    minChars: 1,
                    queryDelay: 250,     
                    allowBlank: false,
                    queryMode: 'local',
                    store: new Ext.data.SimpleStore({
                      fields: ['qstring', 'label'],
                      data : [[ '300x200', '300x200' ],
                              [ '600x400', '600x400' ],
                              [ '1200x800', '1200x800'  ]          
                      ]}
                    ),
                    validator: function(v) {
                       return (v!='Sélectionnez une taille');
                    },
                    displayField: 'label',
                    valueField: 'qstring',
                   // width: 180
                  },
                {
                  xtype :'radiogroup',
                  columns :1,
                  //labelWidth :75,
                  fieldLabel :'Traitement de la carte',
                  name :'typeExport',
                  items : [ {
                    boxLabel :'Echelle conservée',
                    name :'typeExport',
                    inputValue :'0',
                    checked :true
                  }, {
                    boxLabel :'Emprise conservée',
                    name :'typeExport',
                    inputValue :'1'
                  } ]
                },{
                  xtype :"hidden",
                  name :"contextJs",
                  value :"",
                  hidden :true,
                  hiddenLabel :true
                },{
                  xtype :"hidden",
                  name :"mapWidth",
                  value : "",
                  hidden :true,
                  hiddenLabel :true
                }/*, new Ext.ux.IFrameComponent( {
                  url :"/blank.html",
                  name :"iFrameImg",
                  width:"0",
                  height:"0"
                    })*/
                
            ],
            buttons : [ {
              id :'submitImg',
              text :'Générer',
              handler: function() {
                // The getForm() method returns the Ext.form.Basic instance:
                var form = Ext.getCmp('ExportImg').getForm();
                if (form.isValid()) {
                    Ext.MessageBox.show({ 
                      msg: 'Génération en cours, merci de patienter...', 
                      progressText: 'Saving...', 
                      width:300, 
                      wait:true, 
                      waitConfig: {interval:200}
                    });
                    // Submit the Ajax request and handle the response
                    form.submit({
                      success: function(form, action) {
                          if(action.result.urlFile){
                              document.getElementById('donwloadIframe').src =  '/services/readfile.php?file='+action.result.urlFile
                          }
                          Ext.MessageBox.hide();
                      },
                      failure: function() {
                          Ext.MessageBox.alert("Echec de la procédure");
                          Ext.MessageBox.hide();
                      },
                    });
                }
            }
              
            } ]
          }),

   initialize: function(options) {
	   OpenLayers.Control.prototype.initialize.apply(this, arguments);
	  },

		setMap: function(map) {
	    OpenLayers.Control.prototype.setMap.apply(this, arguments);

	    this.btn.addListener('toggle', 
         Carmen.Util.buildExt2olHandlerToggle(this));
         
	    this.map.app.ui.addToCycleButtonGroup(this.map.app.context.getToolbarTarget('exportImg'), 'Exports',  'export', this.btn, 0);
	    
	    
	    // adding listeners to link window closing and control deactivation   
      var control = this;
      this.win.on(
         'hide',
         function(w) { control.btn.toggle(false); },
         this.win);
	    
	    this.map.app.ui.doLayout();
		},

		trigger : function() {
			this.showWindow();
		},

    activate: function() {
      	OpenLayers.Control.prototype.activate.apply(this, arguments);
        this.map.toolTipDeactivate();
        this.showWindow();
      },
    
      deactivate: function() {
      	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
      	if (this.win.rendered)
          this.win.hide();
        this.map.toolTipActivate();
      },

			// displaying results
			showWindow : function() {
				var win = this.win;
				
		 
				if (!this.win.rendered) {
					this.win.add(this.printPanel);
					// adding listeners to the form
/*			
      var submitBtn = Ext.getCmp('submitImg');
					submitBtn
							.on(
									'click',
									function(btn, evt) {
										var form = Ext.getCmp("ExportImg").getForm(); 
									//	form.getEl().dom.target = 'iFrameImg';
									//	form.getEl().dom.action = '/services/GetIMG/getIMG.php';
										if (!form.isValid())
										  Ext.Msg.alert('Export Image',
													'Demande non valide');
										else {
										  form.getEl().dom.submit();
										}
									}, submitBtn);
					
					this.win.doLayout();
*/
				}
        

				if (!this.win.isVisible()) {
				  var form =  this.printPanel.getForm();
				  this.win.show();
          var app = this.map.app;
          var ctx = app.context;
          form.setValues({
            "mapWidth": app.map.size.w,
            "contextJs":Ext.util.JSON.encode(app.updateContext(true))});
				}
			},

			CLASS_NAME :"Carmen.Control.ExportImg"

		});