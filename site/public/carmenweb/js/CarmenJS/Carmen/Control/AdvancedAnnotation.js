  /**
 * //@_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.AdvancedAnnotation
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.FeatureStyler = new OpenLayers.Class(OpenLayers.Control, {
   
   vectorLayer : null,
   
   // dimension specific styles
   pointStyle : null,
   lineStyle : null,
   polygonStyle : null,

   // default style for init
   defaultStyle : OpenLayers.Feature.Vector.style['default'],
   
   // style stores
   stylePointStore : {
     fields : ['styleDef', 'styleDesc', 'styleName', 'stylePreview'],
     data : [
       [ { graphicName : 'circle', ms_symbolName : 'ms_circle'}, 'disque plein', 'point_style_1', '/IHM/images/sym_annot_circle.png'],
       [ { graphicName : 'square', ms_symbolName : 'ms_square' }, 'carré plein', 'point_style_2', '/IHM/images/sym_annot_square.png'],
       [ { graphicName : 'triangle', ms_symbolName : 'ms_triangle'  }, 'triangle plein', 'point_style_3', '/IHM/images/sym_annot_triangle.png'],
       [ { graphicName : 'x', ms_symbolName : 'ms_cross' }, 'croix (multiplication)', 'point_style_4', '/IHM/images/sym_annot_x.png'],
       [ { graphicName : 'cross', ms_symbolName : 'ms_plus' }, 'croix (addition)', 'point_style_5', '/IHM/images/sym_annot_cross.png'],
       [ { graphicName : 'star', ms_symbolName : 'ms_star' }, 'étoile', 'point_style_6', '/IHM/images/sym_annot_star.png']
     ] },
   
   styleLineStore : {
     fields : ['styleDef', 'styleDesc', 'styleName', 'stylePreview'],
     data : [
       [ { strokeDashstyle : 'solid', ms_symbolName : 'ms_line_solid' }, 'trait plein', 'line_style_1', '/IHM/images/sym_annot_lineplain.png'],
       [ { strokeDashstyle : 'dot', ms_symbolName : 'ms_line_dot' }, 'pointillés', 'line_style_2', '/IHM/images/sym_annot_linedot.png'],
       [ { strokeDashstyle : 'longdash', ms_symbolName : 'ms_line_dash' }, 'tiretés', 'line_style_3', '/IHM/images/sym_annot_linedash.png'],
       [ { strokeDashstyle : 'dashdot', ms_symbolName : 'ms_line_dashdot' }, 'tiretés - pointillés', 'line_style_4', '/IHM/images/sym_annot_linedashdot.png']
     ] },

   stylePolygonStore : {
     fields : ['styleDef', 'styleDesc', 'styleName', 'stylePreview'],
     data : [
       [ { fillOpacity : 0.4, ms_symbolName : 'ms_area_fill' }, 'surface pleine', 'polygon_style_1', '/IHM/images/sym_annot_areaplain.png'],
       [ { fillOpacity : 0.0, ms_symbolName : 'ms_area_empty' }, 'contour', 'polygon_style_2', '/IHM/images/sym_annot_areaempty.png']
     ] },

   // createNewStyle : if set to true, work on a copy of dimension specific styles 
   // so that features previously drawn with these styles are not modified...
   createNewStyle : true, 
   
   styleRenderer : null,
   
 
   initialize: function(vectorLayer, options) {
     this.vectorLayer = vectorLayer;
     
     OpenLayers.Control.prototype.initialize.apply(this, [options]);
     
     this.pointStyle = OpenLayers.Util.extend({}, this.defaultStyle); 
     this.lineStyle = OpenLayers.Util.extend(
       OpenLayers.Util.extend({}, this.defaultStyle),
       { strokeWidth : 2 });
     this.polygonStyle = OpenLayers.Util.extend({}, this.defaultStyle);
     
     // built style renderer, depends on browser type (IE, gecko...)
     var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
     renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers; 
     this.styleRenderer = new OpenLayers.Class(OpenLayers.Renderer[renderer[0]], { 
       resolution : 1.0,
       left : 0.0,
       top : 10.0,
         getResolution : function() {
           return 1.0;
         }
      });
     this.initializeUIComponents();
   },
   
   
   initializeUIComponents : function() {
   
     Carmen.ComboStyle = Ext.extend(Ext.form.ComboBox, {
        mode: 'local',
        editable: false,
        typeAhead: true,
        listConfig : {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div id="" ext:qtip="{styleDesc}" class="x-combo-list-item"><img src="{stylePreview}" alt="{styleDesc}"/></div></tpl>';}
        },
        //displayTpl: Ext.create('Ext.XTemplate',
        //'<tpl for="."><div id="" ext:qtip="{styleDesc}" class="x-combo-list-item"><img src="{stylePreview}" alt="{styleDesc}"/></div></tpl>'),
        displayTpl: new Ext.XTemplate(
                '<tpl for=".">' +
                    '{[values["styleDesc"]]}' +
                '</tpl>'
            ),
        forceSelection: true,
        triggerAction: 'all',
        displayField: 'styleDesc',
        valueField: 'styleDef',
        lazyInit: false,
        width: 150,
        styleStoreCfg : null,
        styleRenderer : null,
        initComponent: function() {
			
	      this.doQueryTask = Ext.create('Ext.util.DelayedTask', this.doRawQuery, this);
          this.id = Ext.id(); 
          this.tpl = '<tpl for="."><div id="tpl_' + this.id + '" ext:qtip="{styleDesc}" class="x-boundlist-item"><img src="{stylePreview}" alt="{styleDesc}"/></div></tpl>';

          //Ext.form.ComboBox.superclass.initComponent.call(this);

          this.store = new Ext.data.ArrayStore(this.styleStoreCfg);

          // setting initial combo value as the first one in the store
          this.on('afterrender', function() {
            this.setValue(this.store.getAt(0).data.styleDef); 
          
          }, this);
          
          // on change updating the linked style by extending it with combo selected style
          this.on('select', 
            function(c,r,i) {
              
              nv = r[0].get('styleDef');
              OpenLayers.Util.extend(this.linkedStyle, nv);
              /*if (this.styleRenderer)
                this.updateStylePreview();*/
                
            }, this);
             
        },
        
        updateStylePreview : function (){
          var styleBox = Ext.get('tpl_' + this.id);
          renderer = new this.styleRenderer(styleBox.id);
          var point = new OpenLayers.Geometry.Point(1.0,1.0);
          renderer.drawGeometry(point, OpenLayers.Feature.Vector.style['default'], Ext.id(null, 'style_geom_'));         
        }
        
       });
	 
     //var comboStyle = new Carmen.ComboStyle();
     
     Carmen.ColorChooserStyle = Ext.extend(Ext.ux.ColorChooser, {
        linkedStyle : null,
        listeners : {
             //on select, update linkedStyle color
             'select' : {
               fn : function(cp, colorStr) {
                 OpenLayers.Util.extend(cp.linkedStyle, { strokeColor : '#' + colorStr, fillColor : '#' + colorStr});
               }
             }   
           }  
     });     
     
     //var colorChooserStyle = new Carmen.ColorChooserStyle(); 
     
     this.pointStyler = new Ext.form.FieldContainer({
       fieldLabel : 'Style',
       items : [new Carmen.ComboStyle({ 
         styleStoreCfg : this.stylePointStore, 
         linkedStyle : this.pointStyle,
         styleRenderer : this.styleRenderer })
         ] 
     });
     
     this.pointStylerColor = new Carmen.ColorChooserStyle({ linkedStyle : this.pointStyle });
     
     this.lineStyler = new Ext.form.FieldContainer({
       fieldLabel : 'Style',
       items : [new Carmen.ComboStyle({
         styleStoreCfg : this.styleLineStore, 
         linkedStyle : this.lineStyle,
         styleRenderer : this.styleRenderer })]
     });
     
     this.lineStylerColor = new Carmen.ColorChooserStyle({ linkedStyle : this.lineStyle });

     this.polygonStyler = new Ext.form.FieldContainer({
       fieldLabel : 'Style',
       items : [new Carmen.ComboStyle({
         styleStoreCfg : this.stylePolygonStore, 
         linkedStyle : this.polygonStyle,
         styleRenderer : this.styleRenderer })]
     });
     
     this.polygonStylerColor = new Carmen.ColorChooserStyle({ linkedStyle : this.polygonStyle });
  
     this.styleWindow = new Ext.Window({
        id : 'styleWindow_' + this.displayClassName, 
        layout: 'fit',
  	    width: 365,
  	    height: 350,
        constrain : true,
  	    plain: true,
  	    title: 'Styles des annotations',
  	    modal:false,
  	    autoDestroy :true,
  	    resizable:true,
  	    closeAction: 'hide',
  	    shadow : false,
  	    hidden: true,
  	    autoScroll: true,
  	    items: [{
  	       xtype : 'form',
           fitToFrame: true,
           bodyStyle:'padding:10px 10px 10px 10px; color: #000000;',
           labelStyle: 'color: #000000;',
           monitorValid : true,
  	       items : [
  	       {
			  xtype: 'fieldset',
			  id: 'fieldsetRequest_points',
			  title: 'Points',
			  items: [
				this.pointStyler,
				this.pointStylerColor
			  ]
		   },
		   {
			  xtype: 'fieldset',
			  id: 'fieldsetRequest_lines',
			  title: 'Lignes',
			  items: [
				this.lineStyler,
				this.lineStylerColor
			  ]
		   },
		   {
			  xtype: 'fieldset',
			  id: 'fieldsetRequest_polygons',
			  title: 'Polygones',
			  items: [
				this.polygonStyler,
				this.polygonStylerColor
			  ]
		   }],
  	       buttons : [{
            id: 'btnStyleValid_' + this.displayClassName,
            formBind : true,
            text: 'Valider',
            listeners : {
              'click' : {
                fn : function() {
                  this.vectorLayer.updateGeometryDefaultStyle(this.pointStyle, this.lineStyle, this.polygonStyle);
                  this.styleWindow.hide();
                },                    
                scope : this
              } 
            }
           }] 
  	     }],
  	   listeners : {
  	     'hide' : {
  	       fn : function() { 
  	         this.deactivate(); 
  	       },
  	       scope : this
  	     }
  	   }  
  	 });  
   },

  activate : function() {
    var state = OpenLayers.Control.prototype.activate.apply(this, arguments);
    if (state) {
      this.styleWindow.show();
    }
    return state;
  },
  
  deactivate : function() {
    var state = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (state)
      this.styleWindow.hide();
    return state;
  },
  displayClassName: "CmnControlFeatureStyler",
  CLASS_NAME: "Carmen.Control.FeatureStyler"
});




/*********************************
 * FeatureTextStyler
 */
Carmen.Control.FeatureTextStyler = new OpenLayers.Class(OpenLayers.Control, {
  
   // default style for init
   defaultStyle : {
        fillColor: "#ee9900",
        fillOpacity: 0.4, 
        hoverFillColor: "white",
        hoverFillOpacity: 0.8,
        strokeColor: "#ee9900",
        strokeOpacity: 1,
        strokeWidth: 1,
        strokeLinecap: "round",
        strokeDashstyle: "solid",
        hoverStrokeColor: "red",
        hoverStrokeOpacity: 1,
        hoverStrokeWidth: 0.2,
        pointRadius: 6,
        hoverPointRadius: 1,
        hoverPointUnit: "%",
        pointerEvents: "visiblePainted",
        cursor: "inherit",
        stroke : false,
        fill : false
    },

  selectControl : null,
  
  fontStore :  {
     fields : ['fontOL', 'fontCSS', 'fontName'],
     data : [
       [ { fontColor: '#000000',
           fontFamily: 'Times, serif',
           fontWeight: 'normal',
           fontSize: '12px',
           ms_fontName : 'times'   
         }, 
         'font-family: Times, serif; font-weight: normal; font-size: 12px;', 
         'Times'],
       [ { fontColor: '#000000',
           fontFamily: '"Courier New", monospace',
           fontWeight: 'normal',
           fontSize: '12px',
           ms_fontName : 'courier'   
         }, 
         'font-family: "Courier New", monospace; font-weight: normal; font-size: 12px;', 
         'Courier'],
       [ { fontColor: '#000000',
           fontFamily: 'Verdana, sans-serif',
           fontWeight: 'normal',
           fontSize: '12px',
           ms_fontName : 'verdana'  
         }, 
         'font-family: Verdana, sans-serif; font-weight: normal; font-size: 12px;', 
         'Verdana'],
       [ { fontColor: '#000000',
           fontFamily: 'Comic, cursive',
           fontWeight: 'normal',
           fontSize: '12px',
           ms_fontName : 'comic'
         }, 
         'font-family: Comic, cursive; font-weight: normal; font-size: 12px;', 
         'Comic'],
       [ { fontColor: '#000000',
           fontFamily: 'Arial',
           fontWeight: 'bold',
           fontSize: '12px',
           ms_fontName : 'arialb'  
         }, 
         'font-family: Arial, sans-serif; font-weight: bold; font-size: 12px;', 
         'Arial']
     ] }, 
  
  sizeStore :  {
    fields : ['value'],
    data : [[8], [10], [12], [14], [16], [20], [24], [28], [32], [64]]
  },
  
  initialize : function(layerVector, layerLabel, options) {
    OpenLayers.Control.prototype.initialize.apply(this, [options]);
    var selectOptions = {
      onSelect : this.onSelect,
      toggle : true,
      scope : this
    }
    this.selectControl = new OpenLayers.Control.SelectFeature([layerVector, layerLabel], 
      selectOptions);
    this.layer = layerVector;
    this.layerLabel = layerLabel;
    var labelDefaultStyle = this.layerLabel.styleMap.styles['default'].defaultStyle;
    labelDefaultStyle.fill = false;
    labelDefaultStyle.stroke = false;
    
    
    this.initializeLayerListeners();
    
    this.initializeUIComponents();     
  },
  
  initializeUIComponents : function() {
    
    this.comboFont = new Ext.form.ComboBox({
        fieldLabel: 'Police',
        queryMode: 'local',
        editable: true,
        typeAhead: true,
        listConfig: {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div style="{fontCSS}" ext:qtip="{fontName}" class="x-combo-list-item">{fontName}</div></tpl>';}
        },  
        forceSelection: true,
        triggerAction: 'all',
        displayField: 'fontName',
        valueField: 'fontName',
        value : 'Arial',
        //value: this.fontStore.data[0].fontOL,
        lazyInit: false,
        width: 150,
        
        store: new Ext.data.ArrayStore(this.fontStore)
     });
     
     this.comboSize = new Ext.form.ComboBox({
       fieldLabel: 'Taille',
       queryMode: 'local',
       editable: true,
       typeAhead: true,
       listConfig: {loadingText: 'Chargement' },
       forceSelection: true,
       triggerAction: 'all',
       displayField: 'value',
       valueField: 'value',
       value: 8,
       lazyInit: false,
       width: 40,
       store: new Ext.data.ArrayStore(this.sizeStore)
     });
     
     this.colorChooser = new Ext.ux.ColorChooser();
     
     this.textArea = new Ext.form.TextArea({
        fieldLabel: 'Texte',
        editable: true,
        emptyText: 'Saisissez le contenu de l\'annotation',
        width: 200
      });
   
     this.styleSet = new Ext.form.FieldSet({
       //title : 'Paramètre de l\'annotation textuelle',
       layout : 'form',
       items : [      
         this.comboFont,
  	     this.comboSize,
  	     this.colorChooser,
  	     this.textArea ]
     });
      
     this.activCheckBox =  new Ext.form.Checkbox({
       boxLabel : 'Associer une annotation textuelle',
       checked : true,
       listeners : {
         'change' : {
           fn : function (cb, newValue, oldValue, eOpts) {
			   
             this.styleSet.setDisabled(!newValue);
           },
           scope : this
         } 
       }
     });
  
     this.styleWindow = new Ext.Window({
        id : 'styleWindow_' + this.displayClassName, 
  	    width:370,
  	    height: 305,
        constrain : true,
  	    plain: true,
  	    title: 'Styles des annotations textuelles',
  	    modal:false,
  	    autoDestroy :true,
  	    resizable:true,
  	    closeAction: 'hide',
  	    shadow : false,
  	    hidden: true,
  	    autoScroll: true,
  	    items: [
  	    {
           fitToFrame: true,
           bodyStyle:'padding:10px 10px 10px 10px; color: #000000;',
           labelStyle: 'color: #000000;',
           monitorValid : true,
  	       items : [
  	         this.activCheckBox,
  	         this.styleSet 
   	       ],
  	       buttons : [{
            id: 'btnStyleValid_' + this.displayClassName,
            formBind : true,
            text: 'Valider',
            listeners : {
              'click' : {
                fn : function() {
                  //registering text style values
                  var atts = this.labelFeature.attributes;
                  atts.textActiv = this.activCheckBox.getValue();
                  atts.textConfig_fontName = this.comboFont.getValue();
                  var idx = this.comboFont.getStore().find('fontName', this.comboFont.getValue());
                  var fontDesc = this.comboFont.getStore().getAt(idx).get('fontOL');
                  atts.textConfig_ms_fontName = fontDesc.ms_fontName;
                  atts.textConfig_fontFamily = fontDesc.fontFamily;
                  atts.textConfig_fontWeight = fontDesc.fontWeight;
                  atts.textConfig_fontColor = '#' + this.colorChooser.getValue();
                  atts.textConfig_fontSize = this.comboSize.getValue() + 'px';
                  atts.textConfig_label = this.textArea.getValue();
                  atts.textConfig_labelAlign = "cb";
                  
                  // Updating feature style
                  var style =  atts.textActiv ?
                    {
                      label: atts.textConfig_label,
                      fontColor: atts.textConfig_fontColor,
                      fontSize: atts.textConfig_fontSize,
                      fontFamily: atts.textConfig_fontFamily,
                      fontWeight: atts.textConfig_fontWeight,
                      labelAlign: atts.textConfig_labelAlign,
                      fontName : atts.textConfig_fontName,
                      ms_fontName: atts.textConfig_ms_fontName,
                      stroke : false,
                      fill: false
                     } :
                    {
                      label : null,
                      fontName : null,
                      ms_fontName: null,
                      fontColor : null,
                      fontSize : null,
                      fontFamily : null,
                      fontWeight : null,
                      labelAlign : null,
                      stroke : false,
                      fill: false
                    };
                  
                  this.layerLabel.addFeatureStyle(this.labelFeature, style, true); 
                  this.layerLabel.redraw();
                  this.styleWindow.hide();
                },
                scope : this
              } 
            }
           }]
   	    }],
   	    listeners : {
          'hide' : {
            fn : function() {this.selectControl.unselectAll();},
            scope : this
          }
        }
  	 });  
    
  },
  
  //addLabelFeature : function() 
  initializeLayerListeners :  function() {
   var scope = { layerLabel: this.layerLabel }; 
   this.layer.events.register("featureadded",
     scope, this.addLabelFeature);
   
   this.layer.events.register("featuremodified",
     scope, this.updateLabelFeature);
     
   this.layer.events.register("featureremoved",
     scope, this.removeLabelFeature);
  },  
  
  addLabelFeature : function(evt) {
    var feature = evt.feature;
    var geom = feature.geometry;
    var geomLabel = null;
    if (geom) {
      if (geom.CLASS_NAME == "OpenLayers.Geometry.LineString") {
        geomLabel = Carmen.Util.computeLinearMidPoint(feature.geometry);
      }
      else
        geomLabel = geom.getCentroid(); 
      var fLabel = new OpenLayers.Feature.Vector(geomLabel, { fid : this.layerLabel.features.length });
      this.layerLabel.addFeatures(fLabel);
      feature.attributes.fidLabel = fLabel.attributes.fid;
    }
  },
  
  updateLabelFeature : function(evt) {
    var feature = evt.feature;
    if (feature.attributes.fidLabel!=null) {
      var fLabel = this.layerLabel.getFeatureByAttribute("fid", feature.attributes.fidLabel);
      if (fLabel) {
        var geom = feature.geometry;
        var geomLabel = fLabel.geometry;
        if (geom) {
          if (geom.CLASS_NAME == "OpenLayers.Geometry.LineString") {
            geomLabel = Carmen.Util.computeLinearMidPoint(feature.geometry);
          }
          else
            geomLabel = geom.getCentroid();
        }
        var newPos = new OpenLayers.LonLat.fromString(geomLabel.toShortString());
        fLabel.move(newPos); 
      }
    }
  },
  
  removeLabelFeature : function(evt) {
    var feature = evt.feature;
    if (feature.attributes.fidLabel!=null) {
      var fLabel = this.layerLabel.getFeatureByAttribute("fid", feature.attributes.fidLabel);
      if (fLabel) 
        this.layerLabel.removeFeatures(fLabel);
    }      
  },
  
  
  addTextSupport : function(feature) {
    feature.attributes.textActiv = false;
    feature.attributes.textConfig_fontName = 'Arial';
    feature.attributes.textConfig_fontFamily = 'Arial';  
    feature.attributes.textConfig_fontWeight = 'bold';
    feature.attributes.textConfig_fontColor = '#000000';
    feature.attributes.textConfig_fontSize = '12px';
    feature.attributes.textConfig_label = '' ;  
  },
  
  //private
  updateStyle : function(style, newStyle) {
    if (style == null)
      style = {
        'default' : this.defaultStyle,
        'temporary' : OpenLayers.Feature.Vector.style['temporary'],
        'select' : OpenLayers.Feature.Vector.style['select'],
        'delete' : OpenLayers.Feature.Vector.style['delete']
      };
    style['default'] = OpenLayers.Util.extend(style['default'], newStyle);
    return style['default'];
  },
  
  onSelect : function(feature) {
    this.feature = feature;
    this.labelFeature = this.layerLabel.getFeatureByAttribute("fid", feature.attributes.fidLabel);
    if (this.labelFeature && this.labelFeature.attributes.textActiv==null)
      this.addTextSupport(this.labelFeature);
    this.showWindow(this.labelFeature);
  },
  
  showWindow : function(feature) {
    this.activCheckBox.setValue(feature.attributes.textActiv);
    var atts = feature.attributes;
    this.comboFont.setValue(atts.textConfig_fontName);
    this.comboSize.setValue(parseInt(atts.textConfig_fontSize));
    this.colorChooser.setValue(atts.textConfig_fontColor);
    if (atts.textConfig_label!='')
      this.textArea.setValue(atts.textConfig_label);
    else
      this.textArea.reset();
    this.styleWindow.show();
  },
  
  activate : function() {
    var res = this.selectControl.activate();
    return res;
  },
  
  deactivate : function() {
    this.selectControl.unselectAll();
    return this.selectControl.deactivate();
  },
  
  setMap: function(map) {
    this.selectControl.setMap(map);
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
  },
  
  displayClassName: "CmnControlFeatureTextStyler", 
  CLASS_NAME: "Carmen.Control.FeatureTextStyler"
});


/*********************************
 * FeatureBuffer
 */
Carmen.Control.FeatureBuffer = new OpenLayers.Class(OpenLayers.Control, {

  selectControl : null,
  bufferControl : null,

  initialize : function(layer, layerLabel, options) {
    OpenLayers.Control.prototype.initialize.apply(this, [options]);
    var selectOptions = {
      onSelect : this.onSelect,
      toggle : true,
      scope : this
    }
    this.selectControl = new OpenLayers.Control.SelectFeature([layer, layerLabel], 
      selectOptions);
    
    this.bufferControl = new Carmen.Control.Buffer(
      options.bufferOptions.serviceUrl,
      options.bufferOptions.mapfile,
      { mode : Carmen.Control.Buffer.BUFFER_ON_FEATURE });
    
    this.layer = layer;    
  },
  
  onSelect : function(feature) {
    
    this.bufferControl.trigger(feature);
    // TODO should be done better way
    this.bufferControl.win.on('hide',
     function() {
       this.selectControl.unselectAll();
     }, this); 
    
  },
  
  activate : function() {
    var res = this.selectControl.activate();
      if (res)
        res = this.bufferControl.activate();
    return res;
  },
  
  deactivate : function() {
    this.selectControl.unselectAll();
    return this.selectControl.deactivate() && this.bufferControl.deactivate();
  },
  
  setMap: function(map) {
    this.selectControl.setMap(map);
    this.bufferControl.setMap(map);
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
  },
  
  displayClassName: "CmnControlFeatureBuffer", 
  CLASS_NAME: "Carmen.Control.FeatureBuffer"
});







Carmen.Control.AdvancedAnnotation = new OpenLayers.Class(OpenLayers.Control, {

  vectorLayer : null,
  
  controls : {},
  
  btns : {},
  
  displayConfig: ['style', 'point', 'polyline', 'polygon', 'move', 'modify', 'text', 'buffer'], 
  
  currentStyle: 0, 
  
  panel : Ext.create('Ext.panel.Panel', {
    id : 'annotationPanel',
    border: false,
    hideBorders: true,
    margins: '5 5 5 5',
    hidden: true,
    hideMode: 'offsets',
    x: 5,
    y: 5,
    style: { 'z-index': Ext.WindowMgr.zseed - 1 },
    xtype: 'panel'
  }),
  
  panelBtn: Ext.create('Ext.Button', {
    tooltip: 'Ajouter une annotation',
    tooltipType: 'title',
    cls: 'x-btn-icon cmn-tool-annotate',
    id : 'annot-add',
    enableToggle: true,
    toggleGroup: 'mainMapControl'
  }),

  initialize: function(contextInit,options) {
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
    
    // building annotations layer and label annotations layer    
    this.labelLayer = new Carmen.Layer.Vector("Label Layer",
      { fixedPosition : 0,
        useStyleMap : true });
        
    this.vectorLayer = new Carmen.Layer.Vector("Vector Layer",
      { fixedPosition : 1,
        useStyleMap : true });

    
    if (contextInit!=null)
      this.initFromContext(contextInit);    
    
    // building editing controls
    this.controls['point'] = new OpenLayers.Control.DrawFeature(this.vectorLayer, OpenLayers.Handler.Point);
    
    this.controls['polyline'] = new OpenLayers.Control.DrawFeature(this.vectorLayer, OpenLayers.Handler.Path);
    
    this.controls['polygon'] = new OpenLayers.Control.DrawFeature(this.vectorLayer, OpenLayers.Handler.Polygon);
    
            

    var modifyFeatureControl = {    
      // redefine handleKeypress to allow delelting of whole feature when del key is pressed
      // Tbd : could probably extends instead of rewritting
      
      handleKeypress: function(evt) {
        var code = evt.keyCode;
        // check for delete key
        if(this.feature &&
           OpenLayers.Util.indexOf(this.deleteCodes, code) != -1) {
            var vertex = this.dragControl.feature;
            if(vertex &&
               OpenLayers.Util.indexOf(this.vertices, vertex) != -1 &&
               !this.dragControl.handlers.drag.dragging &&
               vertex.geometry.parent) {
                // remove the vertex
                vertex.geometry.parent.removeComponent(vertex.geometry);
                this.layer.drawFeature(this.feature,
                                       this.selectControl.renderIntent);
                this.resetVertices();
                this.onModification(this.feature);
                this.layer.events.triggerEvent("featuremodified", 
                                               {feature: this.feature});
            }
            else if (this.mode == OpenLayers.Control.ModifyFeature.RESHAPE ||
               this.mode == OpenLayers.Control.ModifyFeature.DRAG ) {
            	this.layer.removeFeatures(this.feature);
            	this.feature = null;
            	this.resetVertices();
              //this.onModification(this.feature);
            }
        }
      }
    };
    
    this.controls['move'] = new OpenLayers.Control.ModifyFeature(this.vectorLayer, modifyFeatureControl);
    this.controls['modify'] = new OpenLayers.Control.ModifyFeature(this.vectorLayer, modifyFeatureControl);
    this.controls['style'] = new Carmen.Control.FeatureStyler(this.vectorLayer);
    this.controls['text'] = new Carmen.Control.FeatureTextStyler(this.vectorLayer, this.labelLayer);
    this.controls['buffer'] = new Carmen.Control.FeatureBuffer(
     this.vectorLayer, this.labelLayer, {bufferOptions : options.bufferOptions});

    // building btns      
    var btns = [{
      name : 'point',    title : 'Dessin de points',     cls : 'fa falk-point-thin fa-2x'
    },{
      name : 'polyline', title : 'Dessin de polylignes', cls : 'fa falk-polyline fa-2x'
    },{
      name : 'polygon',  title : 'Dessin de polygones',  cls : 'fa falk-polygon fa-2x'
    },{
      name : 'text',     title : 'Associer une annotation textuelle', cls : 'fa fa-file-text-o fa-2x'
    },{
      name : 'modify',   title : 'Modification d\'annotations',  cls : 'fa fa-pencil fa-2x'
    },{
      name : 'move',     title : 'Déplacement d\'annotations',   cls : 'fa fa-arrows fa-2x'
    },{
      name : 'style',    title : 'Modifier le style des annotations', cls : 'fa fa-paint-brush fa-2x'
    },{
      name : 'buffer',   title : 'Créer une zone tampon à partir de l\'annotation',  cls : 'fa falk-buffer fa-2x'
    }];
    Ext.each(btns, function(btn){
      this.btns[btn.name] = this.buildBtn(btn.name, 'cmn-tool-annotate_'+btn.name, btn.title, '<i class="'+btn.cls+'"></i>');
    }, this);


  },
  

  activate : function() {
    var state = OpenLayers.Control.prototype.activate.apply(this, arguments);
    
    
    
    var win = new Ext.Window({
      id : 'test',
      width : 100,
      height : 100
    });
    
    win.show();
    var domNode = win.body;
    
    var point = new OpenLayers.Geometry.Point(5.0,5.0);
   
    var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
    
    //to be extended...
    var rendererSVG = new OpenLayers.Class(OpenLayers.Renderer.SVG, { 
     resolution : 1.0,
     left : 0.0,
     top : 10.0,
       getResolution : function() {
         return 1.0;
       }
    });
    
    renderer = new rendererSVG(domNode.id);
    

    renderer.drawGeometry(point, OpenLayers.Feature.Vector.style['default'], 'point');
    
    
    
    
    
    if (state) {
      this.panel.show();
      this.panel.setPosition(5,5);
      if (this.map)
       this.map.toolTipDeactivate();
    }
    return state;
  },
  
  deactivate : function() {
    var state = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (state) {
      // removing listeners
      /*for(var i=0; i<this.displayConfig.length; i++) {
        var key = this.displayConfig[i];
        this.btns[key].toggle(false);
      }*/
      // updating gui
      this.panel.hide();
      if (this.map)
       this.map.toolTipActivate();
    }
    return state;
  },
  /**
   * Export features as array of kml string(Polygon, ligne, point)
   */
  exportFeatures : function(){
	  var res = null;
	  if (this.vectorLayer.features.length>0) {
      // extending kml writer to export attributes values
      // TODO make this more robust to handle non valid characters in att names
      // and non string values
     
  	  var strPolygonKml = this.vectorLayer.exportAsKML('Polygon');
  	  var strLineStringKml = this.vectorLayer.exportAsKML('LineString');
  	  var strPointKml = this.vectorLayer.exportAsKML('Point');
  	  var annotationStyle = this.vectorLayer._serializeStyleMap();
	  
	    res = (new Array(strPolygonKml, strLineStringKml, strPointKml, annotationStyle));
	  }
	  
	  return res;
  },

  exportLabels : function() {
    var res = null;
    if (this.labelLayer.features.length>0) {
      var serialized_label = this.labelLayer.serializeForContext(true);
      res = { data : serialized_label.kml, style : serialized_label.style };
    }
    return { Label : res };
  },


  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);

    // adding the vector layer to the map
    this.map.addLayer(this.labelLayer);
    this.map.addLayer(this.vectorLayer);

    // updating guy     
    var mainPanel = this.map.app.ui.getMainPanel();
    var ui = this.map.app.ui;
    mainPanel.add(this.panel);
    this.panel.setPosition(5,5);

    this.panelBtn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));    
    
    for(var i=0; i<this.displayConfig.length; i++) {
      var key = this.displayConfig[i];
      this.map.app.ui.addToCycleButtonGroup(this.map.app.context.getToolbarTarget('annotation'),'Annotations','annotation', this.btns[key], 0);
    }
    
    
    /*
    // adding buttons to the editing panel    
    for(var i=0; i<this.displayConfig.length; i++) {
      var key = this.displayConfig[i];
      this.panel.add(this.btns[key]);
      this.panel.doLayout();
    }
    // force a first rendreing of the panel
    this.panel.show();this.panel.hide();
    */

    // adding listeners and linking controls to the map
    for(var i=0; i<this.displayConfig.length; i++) {
      var key = this.displayConfig[i];
      this.btns[key].olControl = this.controls[key];

			// stoping default mousedown action when hover a toolbar button
			this.btns[key].on('mousedown', 
				function (evt) { evt.stopEvent(); } ,this.btns[key]);
      
      this.btns[key].defaultHandler = Carmen.Util.buildExt2olHandlerToggle(this.controls[key]);
      switch(key) {
        case 'modify':
          this.btns[key].addListener('toggle', 
            function(extBtn, state) {
                extBtn.olControl.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                extBtn.defaultHandler(extBtn, state);
            });
          break;
        case 'move':
          this.btns[key].addListener('toggle', 
            function(extBtn, state) { 
                extBtn.olControl.mode = OpenLayers.Control.ModifyFeature.DRAG;
                extBtn.defaultHandler(extBtn, state);
             });
          break;
        case 'style':
          //obedel TODO : to be placed in Carmen.Util.buildExt2olHandlerToggle, but be carefull to side effects
          this.controls[key].events.register('deactivate', this.btns[key],
            function(evt) { this.toggle(false); });
          this.btns[key].addListener('toggle', this.btns[key].defaultHandler);
          break;
        default:
          this.btns[key].addListener('toggle', this.btns[key].defaultHandler);
          break;
      }
      this.map.addControl(this.controls[key]);
    }
    
  },


  serializeForContext : function() {
    var serialized_vector = this.vectorLayer.serializeForContext(true);
    var serialized_label = this.labelLayer.serializeForContext(true);
    return { Annotation : { data : serialized_vector.kml, style : serialized_vector.style },
             Label : { data : serialized_label.kml, style : serialized_label.style }
           };
  },


  initFromContext : function(config) {
  	var a = config.Annotation;
  	var l = config.Label;
  	this.vectorLayer.initFromContext(a.data, a.style);
  	this.labelLayer.initFromContext(l.data, l.style);
  },
  
  buildBtn : function(suffix, iconCls, toolTip, strTxt) {
    // Todo, see why tooltips raise error
  	var btn = new Ext.Button({
      tooltip: toolTip,
      tooltipType: 'title',
      id: 'annot_' + suffix,
      //icon: iconPath,
      cls: 'x-btn-icon ', /*+iconCls,*/
      enableToggle: true,
      toggleGroup: 'annotationControl', 
      xtype: 'button',
      text: strTxt
  	});
  	return btn;
  },

  CLASS_NAME: "Carmen.Control.AdvancedAnnotation"
});
