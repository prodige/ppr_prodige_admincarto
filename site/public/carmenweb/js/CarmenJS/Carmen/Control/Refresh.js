  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Refresh
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.Refresh = new OpenLayers.Class(OpenLayers.Control, {

  // UI components
  btn: new Ext.Button({
      tooltip: 'Actualiser',      
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-reload',*/
      enableToggle: false,
      disabled: false,
      text : '<i class="fa fa-refresh fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
  }),
  
  // methods
  initialize: function(options) {
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
  },


  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);

    this.btn.addListener('click', Carmen.Util.buildExt2olHandlerButton(this));    
    var tb = this.map.app.ui.getTopToolBar();
    tb.add(this.btn);
    this.map.app.ui.doLayout();
  },
  
  trigger: function() {
  	console.log(this.map.legendControl.toHTML());
    Ext.MessageBox.confirm("Rechargement de la carte",
  	"Vous êtes sur le point de recharger la carte courante " +
  	"dans son contexte initial. Toutes les annotations et les " +
  	"configurations que vous n'aurez pas enregistrées dans un " +
  	"contexte seront définitivement perdues. Voulez-vous poursuivre ?",
  	this.handleConfirmResponse, this);
  },


	handleConfirmResponse : function(btnId, msg, obj) {
		if (btnId == 'yes')
			Carmen.Util.loadPage();		
	},
  
  CLASS_NAME: "Carmen.Control.Refresh"

});


