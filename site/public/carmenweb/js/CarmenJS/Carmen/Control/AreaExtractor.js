  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.AreaExtractor
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */


Carmen.Control.AreaExtractorValidator = new OpenLayers.Class(OpenLayers.Control, {
  goodGeometry :true,
  
  initialize: function(vlayer, btn, processResponseURL, processResponseCallback, options) {
    OpenLayers.Control.prototype.initialize.apply(this, [options]);
    this.vectorlayer = vlayer;
    this.btn = btn;
    this.processResponseURL = processResponseURL; 
    this.processResponseCallback = processResponseCallback; 
  },
  
  activate : function() {
    var res = OpenLayers.Control.prototype.activate.apply(this, arguments); 
    
    if (!this.goodGeometry) {
      Ext.Msg.alert('Validation de la zone', 'La zone saisie dépasse la limite autorisée. Veuillez diminuez la zone.');
      this.btn.toggle(false);
      res = false;
    }
    else {
      Ext.Msg.show({
         title: 'Validadition de la zone',
         msg: 'Cliquez sur "OK" pour valider la zone dessinée ou sur "Annuler" pour revenir à l\'interface de saisie cartographique.',
         buttons: Ext.Msg.OKCANCEL,
         fn: this.processResponse,
         scope: this,
         modal : true,
         icon: Ext.MessageBox.QUESTION
      });
    }
    return res;
  },
  
  processResponse : function(buttonId, text , opt) {
    if (buttonId=="ok") {
  		var geomStr = (this.vectorlayer.features.length>0 &&
  			this.vectorlayer.features[0].geometry) ?
  		this.vectorlayer.features[0].geometry.toString() : '';
  		
  		var url = " " + this.processResponseURL + "?callback=" + this.processResponseCallback + "&param=" + geomStr;
  		
  		var iframe = document.createElement("iframe");
  		iframe.src = url;
  		iframe.style.visibility = 'hidden';
  		document.body.appendChild(iframe);
		  this.btn.toggle(false);
    } 
    else {
		  this.btn.toggle(false);
    }
    
  } 
  
  
  
  
});




Carmen.Control.AreaExtractor = new OpenLayers.Class(OpenLayers.Control, {

  vectorLayer : null,
  
  controls : {},
  
  btns : {},
  
  displayConfig: ['rectangle', 'polygon', 'move', 'modify', 'validate'], 
  
  displayConfigRectOnly : ['rectangle', 'move', 'modify', 'validate'], 
  
  rectOnly : false,
  
  sizeLimit : -1,
  
  alertStyle : OpenLayers.Util.extend(
                  OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']),
                  {
                   strokeWidth: 2,
                   strokeColor: '#FF0000',
                   strokeOpacity: 1.0,
                   fillOpacity: 0.6,
                   fillColor: '#FF0000'
                  }),
  
  defaultStyle : OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']),

    
  currentStyle: 0, 
  
  panel : Ext.create('Ext.Window', {
    id : 'areaExtractorPanel',
    border: false,
    title :'Paramètres d\'extraction',
    hideBorders: true,
    margins: '5 5 5 5',
    hidden: true,
    hideMode: 'offsets',
    x: 5,
    y: 5,
    style: { 'z-index': Ext.WindowMgr.zseed - 1 },
    xtype: 'panel'
  }),
  
  panelBtn: new Ext.Button({
    tooltip: 'Définir une zone d\'extraction vectorielle',
    tooltipType: 'title',
    cls: 'x-btn-icon',
    text : '<i class="fa fa-edit fa-2x"></i>',
    enableToggle: true,
    toggleGroup: 'mainMapControl',
    width: ctrlWidth,
    height: ctrlHeight
    
  }),

  initialize: function(options) {
    OpenLayers.Control.prototype.initialize.apply(this, arguments);

    // tool control options
    if (this.rectOnly)
		  this.displayConfig = this.displayConfigRectOnly;
    
    
    //obedel debug
    //this.sizeLimit = 13157224580.77613;
    
    
    this.vectorLayer = new Carmen.Layer.Vector("Vector Layer",
      { fixedPosition : 0,
        useStyleMap : true });
    
    this.vectorLayer.events.register('sketchcomplete', this.vectorLayer, 
    	function(evt) { 
    	  if (this.features.length>0) {
            this.removeFeatures(this.features, {silent : true});  
          }
        });
        
     this.vectorLayer.events.register('featureadded', this,
        function(evt) {
          var limitExceeded = this.sizeLimit>0 && evt.feature.geometry.getArea()>this.sizeLimit;
          if (limitExceeded) {
            this.vectorLayer.drawFeature(evt.feature, this.alertStyle);
          }
        });

    this.vectorLayer.events.register('featuremodified', this,
        function(evt) {
            var limitExceeded = this.sizeLimit>0 && evt.feature.geometry.getArea()>this.sizeLimit;
            evt.feature.style = limitExceeded ? this.alertStyle : this.defaultStyle;
            if (limitExceeded)
              this.vectorLayer.drawFeature(evt.feature);
        });
    
    // building btns            
    this.btns['rectangle'] = this.buildBtn('rectangle',
      '<i class="fa fa-edit fa-2x"></i>','Zone rectangulaire');
      //'/IHM/images/Annotate_box_OFF.png','Zone rectangulaire');
    this.btns['polygon'] = this.buildBtn('polygon',
      '<i class="fa falk-polygon fa-2x"></i>','Zone polygonale');
    this.btns['modify'] = this.buildBtn('modify',
      '<i class="fa fa-pencil fa-2x"></i>','Modification des limites la zone');
    this.btns['move'] = this.buildBtn('move',
      '<i class="fa fa-arrows fa-2x"></i>','Déplacement de la zone');
    this.btns['validate'] = this.buildBtn('validate',
      '<i class="fa fa-check fa-2x"></i>','Valider la zone');
    
    
    // building controls  
    this.controls['rectangle'] = new OpenLayers.Control.DrawFeature(this.vectorLayer,
        OpenLayers.Handler.RegularPolygon, 
        { handlerOptions: {
            sides : 4,
            irregular : true,
            sizeLimit : this.sizeLimit,
            alertStyle : this.alertStyle,
            move : function(evt) {
              OpenLayers.Handler.RegularPolygon.prototype.move.apply(this, arguments);
              if (this.sizeLimit>0 && this.feature.geometry.getArea()>this.sizeLimit) {
                this.clear();
                this.layer.drawFeature(this.feature, this.alertStyle);
              }
            }
          },
          featureAdded : function(f) {
            f.attributes.type = Carmen.Control.AreaExtractor.BOX_TYPE;
          }
        });
    
    this.controls['polygon'] = new OpenLayers.Control.DrawFeature(this.vectorLayer,
            OpenLayers.Handler.Polygon);
  
    var modifyFeatureControl = {    
      alertStyle : this.alertStyle,
      sizeLimit : this.sizeLimit,

      selectFeature: function(feature) {
        // changing modification type (resize or reshape) according to
        // the feature shape : box or polygon...
        if (this.mode == OpenLayers.Control.ModifyFeature.RESHAPE) {
          if (feature.attributes.type && 
              feature.attributes.type==Carmen.Control.AreaExtractor.BOX_TYPE) 
            this.mode = this.mode | OpenLayers.Control.ModifyFeature.RESIZE;
          else
            this.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
        }
        //console.log(this.mode);
        OpenLayers.Control.ModifyFeature.prototype.selectFeature.apply(this, arguments);
      },
      
      dragVertex : function(vertex, pixel) { 
        OpenLayers.Control.ModifyFeature.prototype.dragVertex.apply(this, arguments);
        this.feature.style = this.sizeLimit>0 && this.feature.geometry.getArea()>this.sizeLimit ?
            this.alertStyle :  this.defaultStyle;
        if (this.sizeLimit>0 && this.feature.geometry.getArea()>this.sizeLimit) 
          this.layer.drawFeature(this.feature, this.alertStyle);
        else 
          this.layer.drawFeature(this.feature,this.selectControl.renderIntent);
      },
     
      // redefine handleKeypress to allow delelting of whole feature when del key is pressed
      // Tbd : could probably extends instead of rewritting
      handleKeypress: function(evt) {
        var code = evt.keyCode;
        // check for delete key
        if(this.feature &&
           OpenLayers.Util.indexOf(this.deleteCodes, code) != -1) {
            var vertex = this.dragControl.feature;
            if(vertex &&
               OpenLayers.Util.indexOf(this.vertices, vertex) != -1 &&
               !this.dragControl.handlers.drag.dragging &&
               vertex.geometry.parent) {
                // remove the vertex
                vertex.geometry.parent.removeComponent(vertex.geometry);
                this.layer.drawFeature(this.feature,
                                       this.selectControl.renderIntent);
                this.resetVertices();
                this.onModification(this.feature);
                this.layer.events.triggerEvent("featuremodified", 
                                               {feature: this.feature});
            }
            else if (this.mode == OpenLayers.Control.ModifyFeature.RESHAPE ||
               this.mode == OpenLayers.Control.ModifyFeature.DRAG ) {
            	this.layer.removeFeatures(this.feature);
            	this.feature = null;
            	this.resetVertices();
              //this.onModification(this.feature);
            }
        }
      }
    };
    
    this.controls['move'] = new OpenLayers.Control.ModifyFeature(this.vectorLayer, modifyFeatureControl);
    this.controls['modify'] = new OpenLayers.Control.ModifyFeature(this.vectorLayer, modifyFeatureControl);
    this.controls['validate'] = new Carmen.Control.AreaExtractorValidator(
		this.vectorLayer, 
		this.btns['validate'],
		options.processResponseURL,
		options.processResponseCallback);
      
  },
  
  activate : function() {
    var state = OpenLayers.Control.prototype.activate.apply(this, arguments);
  
    if (state) {
      //obedel test
      //this.panel.show();
      //this.panel.setPosition(80,20);
      if (this.map)
       this.map.toolTipDeactivate();
       
      // setting automatic enabling of validation button depending 
      // on the presence of a drawn area
      this.btns['validate'].setDisabled(this.vectorLayer.features.length==0);
      this.vectorLayer.events.register('featureadded', this,
        function (evt) { 
          this.btns['validate'].setDisabled(false);
          if (this.sizeLimit>0)
            this.controls['validate'].goodGeometry = !(evt.feature.geometry.getArea()>this.sizeLimit);
          else
            this.controls['validate'].activate();
         });
      this.vectorLayer.events.register('featuremodified', this,
        function (evt) { 
          if (this.sizeLimit>0)
            this.controls['validate'].goodGeometry = !(evt.feature.geometry.getArea()>this.sizeLimit); 
          else
            this.controls['validate'].activate();
         });
      this.vectorLayer.events.register('featureremoved', this,
        function () { 
          if (this.controls['validate'].active)
            this.controls['validate'].deactivate();
          this.btns['validate'].setDisabled(true);
         });
    }
    return state;
  },
  
  deactivate : function() {
    var state = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (state) {
      // removing listeners
      for(var i=0; i<this.displayConfig.length; i++) {
        var key = this.displayConfig[i];
        this.btns[key].toggle(false);
      }

      // updating gui
      //obedel test
      //this.panel.hide();
      if (this.map)
       this.map.toolTipActivate();
      
      // setting automatic enabling of validation button depending 
      // on the presence of a drawn area
      this.vectorLayer.events.unregister('featureadded', this,
        function () { 
          this.btns['validate'].setDisabled(false);
         });
      this.vectorLayer.events.unregister('featureremoved', this,
        function () { 
          if (this.controls['validate'].active)
            this.controls['validate'].deactivate();
          this.btns['validate'].setDisabled(true);
         });
    }
    return state;
  },
  
  
  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);

    // adding the vector layer to the map
    this.map.addLayer(this.vectorLayer);

    // updating guy     
    var mainPanel = this.map.app.ui.getMainPanel();
    var ui = this.map.app.ui;
    //obedel test
    //mainPanel.add(this.panel);
    //this.panel.setPosition(5,5);
    
    //obedel test
    //this.panelBtn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));    
    //this.map.app.ui.addToToolPanel(this.panelBtn, 'simple', 8);
    ui.doLayout();
    
     for(var i=0; i<this.displayConfig.length; i++) {
      var key = this.displayConfig[i];
      this.map.app.ui.addToCycleButtonGroup('simple','Zone d\'extraction','extraction', this.btns[key], i,(i==0),1);
    }
    
    //obedel test
    /*
    // adding buttons to the editing panel    
    for(var i=0; i<this.displayConfig.length; i++) {
      var key = this.displayConfig[i];
      this.panel.add(this.btns[key]);
      this.panel.doLayout();
    }
    // force a first rendreing of the panel
    this.panel.show();this.panel.hide();
    */
    
    // adding listeners and linking controls to the map
    for(var i=0; i<this.displayConfig.length; i++) {
      var key = this.displayConfig[i];
      this.btns[key].olControl = this.controls[key];
			/*
			// stoping default mousedown action when hover a toolbar button
			this.btns[key].getEl().on('mousedown', 
				function (evt) { evt.stopEvent(); } ,this.btns[key]);
      */
      this.btns[key].defaultHandler = Carmen.Util.buildExt2olHandlerToggle(this.controls[key]);
      switch(key) {
        case 'modify':
          this.btns[key].addListener('toggle', 
            function(extBtn, state) {
                extBtn.olControl.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                extBtn.defaultHandler(extBtn, state);
            });
          break;
        case 'move':
          this.btns[key].addListener('toggle', 
            function(extBtn, state) { 
                extBtn.olControl.mode = OpenLayers.Control.ModifyFeature.DRAG;
                extBtn.defaultHandler(extBtn, state);
             });
          break
        default:
          this.btns[key].addListener('toggle', this.btns[key].defaultHandler);
      }
      this.map.addControl(this.controls[key]);
    }
    
    if (this.activeAtStartup)
      this.activate();
      //this.panelBtn.toggle(true);
      //this.btns['rectangle'].toggle(true);
  },

  
  buildBtn : function(suffix, iconPath, toolTip) {
    // Todo, see why tooltips raise error
  	
    var btn = Ext.create('Ext.Button', {
	    tooltip: toolTip,
      tooltipType: 'title',
      id: 'areaExtractor_' + suffix,
      cls: 'x-btn-icon',
      enableToggle: true,
      toggleGroup: 'areExtractorControl',
      disabled: false,
      text : iconPath,
      width: ctrlWidth,
      height: ctrlHeight
    });
    
  	return btn;
  },

  CLASS_NAME: "Carmen.Control.AreaExtractor"
});

Carmen.Control.AreaExtractor.POLYGON_TYPE = 0;
Carmen.Control.AreaExtractor.BOX_TYPE = 1;

