/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.MapserverScale
 * Display a small line indicator representing the current map scale on the map.
 * 
 * Inherits from:
 *  - <OpenLayers.Control>
 *  
 
 */
// TODO: see why we nedd to keep openlayers namespace to make the control added on the map
// may be some filter in Openlayers.Map or OpenLayers.control
// do not forget to correctly set displayClass which is used for css assignment
OpenLayers.Control.MapserverScale = new OpenLayers.Class(OpenLayers.Control, {

	// properties
	serverUrl: '',

	mapfile: '',

	scaleImg: null,

	displayTxt: true,
	displayImg: true,

	// temp   
	font: null,
	fontFamily: 'tahoma,arial,helvetica,sans-serif',
	color: '#000000',
	fontSize: '12px',
	fontWeight: 'normal',
	fontStyle: 'normal',
	backgroundColor: null,
	opacity: 1.0,
	// temp end     
	// 		
	initialize: function (options) {
		//temp        
		OpenLayers.Control.prototype.initialize.apply(this, arguments);

		this.fontFamily = this.font ? this.font + ',' + this.fontFamily : this.fontFamily;
		if (this.fontSize && this.fontSize.search(/^\d+$/g) != -1)
			this.fontSize = this.fontSize + 'px';

		if (this.color && this.color.charAt(0) != '#')
			this.color = '#' + this.color;
		if (this.backgroundColor && this.backgroundColor.charAt(0) != '#')
			this.backgroundColor = '#' + this.backgroundColor;
		// alpha in bgcolor desc ?
		if (this.backgroundColor && this.backgroundColor.length == 9) {
			this.opacity = 1.0 - (parseInt(this.backgroundColor.substring(7, 9), 16) / 255);
			this.backgroundColor = this.backgroundColor.substring(0, 7);
		}
		// temp end  
	},

	setMap: function (map) {
		OpenLayers.Control.prototype.setMap.apply(this, arguments);
		var tbFill = Ext.create('Ext.toolbar.Fill', { id: 'scaleDiv' });
		this.map.app.ui.addToBottomToolbar(tbFill, 1);
		this.div = tbFill.getEl().dom;
	},

    /**
     * Method: draw
     * 
     * Returns:
     * {DOMElement}
     */
	draw: function () {
    console.log("DRAW");
		// temp
		OpenLayers.Control.prototype.draw.apply(this, arguments);

		this.div.style.display = "block";
		this.div.style.position = "absolute";
		this.div.style.backgroundColor = this.backgroundColor;
		this.div.style.fontFamily = this.fontFamily;
		this.div.style.fontWeight = this.fontWeight;
		this.div.style.color = this.color;
		this.div.style.fontSize = this.fontSize;
		this.div.style.fontStyle = this.fontStyle;
		OpenLayers.Util.modifyDOMElement(this.div, null, null, null, null, null, null, this.opacity);

		if (!this.scaleImg && this.displayImg) {
			this.scaleImg = document.createElement("img");
			this.scaleImg.style.verticalAlign = 'top';
			this.div.appendChild(this.scaleImg);
		}
		if (!this.scaleTxt && this.displayTxt) {
			this.scaleTxt = document.createElement("span");
			this.scaleTxt.style.verticalAlign = 'top';
			this.scaleTxt.style.marginLeft = '10px';
			this.div.appendChild(this.scaleTxt);
		}
		this.map.events.register('moveend', this, this.update);
		this.update();
		this.map.app.ui.getBottomToolBar().doLayout();
		return this.div;
	},

    /**
     * Method: update
     * Update the size of the bars, and the labels they contain.
     */

	update: function () {
		if (this.displayImg && this.scaleImg) {
			var params = {
				MAP: this.mapfile,
				MAP_IMAGETYPE: 'PNG',
				MODE: 'SCALEBAR',
				map_resolution: Carmen.Mapserver_DOTS_PER_INCH,
				SCALE: this.map.getScale()
			};

			this.scaleImg.src = this.serverUrl + '?' +
				OpenLayers.Util.getParameterString(params);
		}
		if (this.displayTxt && this.scaleTxt) {
			this.scaleTxt.innerHTML = this.map.getScale() ?
				'Echelle ' + Carmen.SpecialChars["around"] + ' ' +
				Carmen.Util.scaleTextFormater(
					Carmen.Util.roundIntToDigits(this.map.getScale())) :
				'';
		}

	},
	CLASS_NAME: "OpenLayers.Control.MapserverScale"
});

