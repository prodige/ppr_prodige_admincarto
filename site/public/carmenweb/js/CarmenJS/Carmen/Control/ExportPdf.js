/**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.QueryAttributes
 * 
 * Inherits from: - <OpenLayers.Control>
 */

Carmen.Control.ExportPdf = new OpenLayers.Class(OpenLayers.Control,{
  // default layouts models
  layoutModelsData : [
	  ['modele1.tpl', 'Légende et carte de situation à droite'],
	  ['modele2.tpl', 'Légende, carte de situation et copyright à gauche'],
	  ['modele3.tpl', 'Grandes légendes'],
	  ['modele4.tpl', 'Légende à gauche, carte de siuation au dessus']
	  ],

	// UI components
	btn : Ext.create('Ext.Button', {
		tooltip :'Export pdf',
		tooltipType :'title',
    cls :'x-btn-icon', /* cmn-tool-printpdf',*/
		enableToggle: true,
    toggleGroup: 'mainMapControlExport',
    disabled: false,
    text : '<i class="fa fa-file-pdf-o fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight

	}),
  //TODO temporaire
  inGroup : true,

	win : Ext.create('Ext.Window', {
		layout :'anchor',
		// layout:'fit',
    bodyPadding: 10,
		//width :400,
    constrain : true,
		//plain :true,
		id : "winPdf",
		title :'Export PDF',
		modal :false,
		autoDestroy :true,
		resizable :false,
		closeAction :'hide',
		//shadow :false
	}),
	
  pdfPanel : Ext.create('Ext.FormPanel',{
		id :'generatepdf',
		autoScroll :true,
		//fitToFrame :true,
		//bodyStyle :'padding:5px 5px 0; color: #000000;',
		//labelStyle :'color: #000000;',
		url :'/services/GetPDF/getPdf.php',
		items : [{
			xtype :"textfield",
			//labelWidth :75,
      fieldLabel :'Titre',
			name :'title',
			value :"",
			//width :'270'
		},{
			xtype :"textarea",
      //labelWidth :75,
			fieldLabel :'Commentaire',
			name :'comment',
			//width :'270'
		},{
			xtype :'radiogroup',
			columns :1,
      //labelWidth :75,
			fieldLabel :'Format',
			name :'format',
			items : [ {
        //labelWidth :75,
				boxLabel :'A4 (petit format)',
				name :'format',
				inputValue :'A4',
				checked :true
			 }, {
        //labelWidth :75,
				boxLabel :'A3 (grand format)',
				inputValue :'A3',
				name :'format'
			} ]
		},
    {
			xtype :'radiogroup',
			columns :1,
			//labelWidth :75,
      fieldLabel :'Traitement de la carte',
			name :'typeExport',
			items : [ {
				//labelWidth :75,
        boxLabel :'Echelle conservée',
				name :'typeExport',
				inputValue :'0',
				checked :true
			}, {
				//labelWidth :75,
        boxLabel :'Emprise conservée',
				name :'typeExport',
				inputValue :'1'
			} ]
		},	
    {
			xtype :"hidden",
			id:"quality",
			name :"quality",
			value :"high",
			hidden :true,
			hiddenLabel :true
		},    
	  {
			xtype :"hidden",
			id:"legend",
			name :"legend",
			value :"",
			hidden :true,
			hiddenLabel :true
		}, {
			xtype :"hidden",
			name :"contextJs",
			value :"",
			hidden :true,
			hiddenLabel :true
		},{
			xtype :"hidden",
			name :"mapWidth",
			value :"",
			hidden :true,
			hiddenLabel :true
		}
		],
		buttons : [ {
			id :'submitPdf',
			text :'Générer',
      handler : function() {
                  var form = Ext.getCmp('generatepdf').getForm();
                  if (form.isValid()) {
                      Ext.MessageBox.show({ 
                        msg: 'Génération en cours, merci de patienter...', 
                        progressText: 'Saving...', 
                        width:300, 
                        wait:true, 
                        waitConfig: {interval:200}
                      });
                      // Submit the Ajax request and handle the response
                      form.submit({
                        success: function(form, action) {
                          if(action.result.urlFile){
                              document.getElementById('donwloadIframe').src =  '/services/readfile.php?file='+action.result.urlFile
                          }
                          Ext.MessageBox.hide();
                        },
                        failure: function() {
                            Ext.MessageBox.alert("Echec de la procédure");
                            Ext.MessageBox.hide();
                        },
                      });
                  }
                }
		} ]
	}),

	initialize: function(options) {
	  OpenLayers.Control.prototype.initialize.apply(this, arguments);
	},
  
  initLayoutCombo : function(context) {
    // retrieving specific layouts from context if existing
	  if (context.mdataMap.layoutModels!="undefined" && context.mdataMap.layoutModels!="") {
      var tabModels = (Url.decode(context.mdataMap.layoutModels)).split(";");
      if (tabModels.length>0) {
        this.layoutModelsData = [];  
        for(var i=0; i<tabModels.length;i++) {
         var tabInfoModel = tabModels[i].split("@");
          this.layoutModelsData.push([tabInfoModel[0], tabInfoModel[1]]);
        }
      } 
    }
    var layoutCombo = Ext.create('Ext.form.ComboBox',{
      editable: false,
      queryMode: 'local',
      triggerAction: 'all',
      emptyText: 'Modèles de mise en page',
      //width: 280,
      displayField: 'desc',
      listConfig: {
        loadingText: 'Chargement',
        getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>'; }
      },
      valueField: 'modele',
      name: 'modeleLayout',
      hiddenName: 'modele',
      fieldLabel :'Modèle',
      store: 
        new Ext.data.SimpleStore({
          fields: ['modele', 'desc'],
          data : []}
        )   
    });
    
    this.pdfPanel.add(layoutCombo);
    layoutCombo.getStore().loadData(this.layoutModelsData);
    layoutCombo.setValue(this.layoutModelsData[0][0]);   
  }, 

	setMap: function(map) {
	  OpenLayers.Control.prototype.setMap.apply(this, arguments);
    // Init layout models list
    this.initLayoutCombo(this.map.app.context);
     
    this.btn.addListener('toggle', 
      Carmen.Util.buildExt2olHandlerToggle(this));    
    
    this.map.app.ui.addToCycleButtonGroup(this.map.app.context.getToolbarTarget('exportPdf'), 'Exports', 'export', this.btn, 0);
    // adding listeners to link window closing and control deactivation   
    var control = this;
    this.win.on('hide',
      function(w) { control.btn.toggle(false); },
      this.win);

    this.map.app.ui.doLayout();
	},
	  
	activate: function() {
  	OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    /*
     if (!this.win.rendered) {
      this.showWindow();
      new Ext.ToolTip({
        target: 'quality_high_pdf',
        html: 'Format PNG N\'est pas toujours compatible avec Firefox 2'
      });
      new Ext.ToolTip({
        target: 'quality_medium_pdf',
        html: 'Format JPEG '
      });
    }
    else*/
      this.showWindow();
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();
  },

 trigger : function() {
	 this.showWindow();
 },

	// displaying results
	showWindow : function() {
		if (!this.win.rendered) {
			this.win.add(this.pdfPanel);
			// adding listeners to the form
			this.win.doLayout();
		}

		if (!this.win.isVisible()) {
			var form =  this.pdfPanel.getForm();
			this.win.show();
			var app = this.map.app;
			var ctx = app.context;
			form.setValues({"title": (ctx.getTitle()),
				"mapWidth": app.map.size.w,
				"legend": app.map.getLegend().toHTML(true).getHtml(),
				"contextJs":Ext.util.JSON.encode(app.updateContext(true))});
		}
	},

	CLASS_NAME :"Carmen.Control.ExportPdf"

});
