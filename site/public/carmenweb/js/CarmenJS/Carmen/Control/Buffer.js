  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Info
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */
 



Carmen.Control.Buffer = new OpenLayers.Class(OpenLayers.Control, {

  BUFFER_ON_SELECTION : 0,
  BUFFER_ON_FEATURE : 1,

  // UI components
  btn: new Ext.Button({
      tooltip: 'Zones tampons',      
      tooltipType: 'title',
      cls: 'x-btn-icon',
      //enableToggle: true,
      //toggleGroup: 'mainMapControlMeasure',
      text: '<i class="fa icon-tampon fa-2x"></i>',
      disabled: true,
      width: ctrlWidth,
      height: ctrlHeight
  }), 
    
  // properties 
  /**
   * Property: type
   * {OpenLayers.Control.TYPE}
   */
  type: OpenLayers.Control.TYPE_TOOL,

	layerTreeManager : null,

  layerTree : null,

  mapfile : null,
  
  serviceUrl : null,

  // UI part..
  windowTitle : 'Zones tampons',
  
  win : null,
  
  gridSelection : null,
  
  comboLayer : null,
  
  radiusField : null,
  
  colorField : null,
  
  colorBtn : null,
  
  nameField : null,
  
  // Vector layer & co
  
  bgColor : 'FF9900',

  style : OpenLayers.Util.extend(
            OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']),
            {
             strokeWidth: 2,
             strokeColor: '#FF9900',
             strokeOpacity: 1.0,
             fillOpacity: 0.3,
             fillColor: '#FF9900'
            }),
  
  // methods
  initialize: function(serviceUrl, mapfile, options) {
 	this.mode = this.BUFFER_ON_SELECTION;
 	OpenLayers.Control.prototype.initialize.apply(this, [options]);    
    this.mapfile = mapfile;
    this.serviceUrl = serviceUrl;
  },

  initializeUIComponents : function() {
	   if (this.mode == this.BUFFER_ON_SELECTION) {
  	   this.gridSelection = new Ext.grid.GridPanel({
		   id : 'gridSelectionBuffer',
  	      title : 'Sélection d\'objets',
  	      //width : 'auto',
  	      height : 170,
  	      store: Carmen.Util.emptyStore,
  	      cm: Carmen.Util.emptyColumnModel,
  	      hidden: true,
  	      disabled: true,
          //selType: 'checkboxmodel',
          selModel: Ext.create('Ext.selection.CheckboxModel', {
            checkOnly: true,
            mode: 'SIMPLE'
          }),
  	      //sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
  	      autoScroll: true,
  	      style: {
              //width: '90%',
              marginBottom: '10px',
              marginTop: '10px'
          },
          header : {
            height : 30,
            padding:  2
          }          
  	    });
        this.gridSelectionHelp = new Ext.form.DisplayField({
          value: 'Veuillez utiliser les touches Ctrl ou Shift pour réaliser des sélections multiples.'
        });
  
       this.comboLayer = new Ext.form.ComboBox({
        fieldLabel: 'Couche',
        queryMode: 'local',
        editable: true,
        listConfig: {
          loadingText: 'Chargement',
          getInnerTpl: function() {return '<tpl for="."><div ext:qtip="{layerTitle}" class="x-combo-list-item">{layerTitle}</div></tpl>';}
        },
        emptyText: 'Sélectionnez une couche',
        typeAhead : true,
        forceSelection: true,
        triggerAction: 'all',
        minChars: 1,
        queryDelay: 250,     
        store: new Ext.data.ArrayStore({
           fields: ['layer', 'layerTitle'],
           data : [[null, 'Sélectionnez une couche']] 
        }),
        validator: function(v) {
           return (v!='Sélectionnez une couche');
        },   
        displayField: 'layerTitle',
        valueField: 'layer',
        //width: 240,
        listeners : {
          'select' : { 
            fn : function(cmb,records,eopts) {
                  var r =records[0].data.layer;
                  if (r==null) {
                  	// vidage gridSelection
                  	this.gridSelection.disable();
                  	this.gridSelection.hide();
                  } else {
					  
                  	var sl = r.layer.getSelection(r.name);

                  	/*this.gridSelection = new Ext.grid.GridPanel({
					  id : 'gridSelectionBuffer',
					  title : 'Sélection d\'objets',
					  //width : 'auto',
					  height : 170,
					  store: Carmen.Util.emptyStore,
					  cm: Carmen.Util.emptyColumnModel,
					  hidden: true,
					  disabled: true,
					  //selType: 'checkboxmodel',
					  selModel: Ext.create('Ext.selection.CheckboxModel', {
						checkOnly: true,
						mode: 'SIMPLE'
					  }),
					  //sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
					  autoScroll: true,
					  style: {
						  //width: '90%',
						  marginBottom: '10px',
						  marginTop: '10px'
					  },
					  header : {
						height : 30,
						padding:  2
					  }          
					});    */
					//Ext.getCmp('window_'+this.displayClassName+this.mode).items.items[0].remove(this.gridSelection);
                  	this.gridSelection.reconfigure(sl.store, sl.cm);
                  	this.gridSelection.enable();
                  	this.gridSelection.show();
                  	//Ext.getCmp('window_'+this.displayClassName+this.mode).items.items[0].add(this.gridSelection);
                  }	

                    var wndw = Ext.getCmp('window_'+this.displayClassName+this.mode);

                    wndw.setWidth(wndw.getWidth());
                    wndw.center();
                    
                },
              scope : this 
           }
        }     	
      });
	 } 
   this.radiusField = new Ext.form.NumberField({
    	fieldLabel: 'Rayon (' + Carmen.Util.getProjectionUnits(this.map.projection) + ')',
    	mode: 'local',
    	allowBlank: false,
    	//width: 240 
    });
    
    this.nameField = new Ext.form.TextField({
      fieldLabel: 'Nom',
    	mode: 'local',
    	allowBlank: false,
    	//width: 240
    });

   this.colorChooser = new Ext.ux.ColorChooser({
     color : this.bgColor,
     //fieldLabel : 'Couleur',
     tooltip: 'Modifier la couleur de la zone tampon',
     tooltipType: 'title',
     listeners : {
       'select' : {
         fn : function(cp, colorStr) {
           this.bgColor = colorStr;
           OpenLayers.Util.extend(this.style, {
              strokeColor: '#' + this.bgColor,
              fillColor: '#' + this.bgColor
            });
          },
          scope : this
       }}
   });
   
   



   this.win = new Ext.Window({
      id : 'window_' + this.displayClassName + this.mode, 
      layout: 'fit',
	    //width:275,
      constrain : true,
	    height: this.mode == this.BUFFER_ON_SELECTION ? 440 : 215,
	    plain: true,
	    title: 'Ajout de zone tampon',
	    modal:false,
	    autoDestroy :false,
        maxWidth: (Ext.getBody().getViewSize().width)*0.7,
	    resizable:true,
	    closeAction: 'hide',
	    shadow : false,
	    hidden: true,
	    autoScroll: true,
	    items: [{
	       xtype : 'form',
         fitToFrame: true,
         bodyStyle:'padding:10px 10px 10px 10px; color: #000000;',
         labelStyle: 'color: #000000;',
         monitorValid : true,
	       items : this.mode == this.BUFFER_ON_SELECTION ?
	       [
	         this.comboLayer, 
             //this.gridSelectionHelp,
	         this.gridSelection, 
             this.radiusField, 
	         this.nameField ,
             this.colorChooser
	       ] : 
	       [
	         this.radiusField, 
	         this.nameField ,
           this.colorChooser
	       ],
	       buttons : [{
          id: 'btnValid_' + this.displayClassName + this.mode,
          formBind : true,
          text: 'Ajoutez la zone tampon',
          listeners : {
            'click' : {
              fn : this.performQuery,                    
              scope : this
            } 
          }
         }] 
	     }]
	  });
	},


  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    
    this.initializeUIComponents();
    if (this.mode == this.BUFFER_ON_SELECTION) { 
      // linking control activation to selection events
      this.map.events.register('addSelection', this,
        function(evt) {
          if (!this.active)
            this.btn.setDisabled(false);
            // updating here layers with selection list...
            if (!this.win.hidden)
             this.updateWindow();  
        });
      
      this.map.events.register('emptySelection', this,
        function(evt) {
          this.deactivate(); 
          this.btn.setDisabled(true);
        });
    
      // TODO handle the addition of the ext button toolbar in a specific mode at the Carmen.Control level
      this.btn.addListener('click', Carmen.Util.buildExt2olHandlerButton(this));    
      //this.map.app.ui.addToCycleButtonGroup('advanced', 'measure', this.btn, 2);    
      this.map.app.ui.addToToolPanel(this.btn, this.map.app.context.getToolbarTarget('buffer'), 5);
    }
  },
  
  activate: function() {
    
    if (this.getLayerTreeManager()==null) {
      Ext.MessageBox.alert("Warning", "The control " + 
       this. CLASS_NAME + 
       " requires the LayerTreeManager control to work.");
      return null;
    }
    OpenLayers.Control.prototype.activate.apply(this, arguments);
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	this.win.hide();
  },
  
  trigger : function(feature) {
    if (this.win.hidden)
      this.win.show();
    this.feature = feature;
    if (this.mode == this.BUFFER_ON_SELECTION)
      this.updateWindow();  
    
  },


  performQuery : function() {
     var queryParams = null;
     if (this.mode == this.BUFFER_ON_SELECTION) {
       var fidList = [];
        var selection = this.gridSelection.getSelectionModel().getSelection();
        for (i=0; i<selection.length; i++) {
           var record = selection[i];
        	 fid = record.get('fid');
        	 if(typeof fid == 'undefined'){
        		 fid = record.get('gid');
        	 }
        	 if(typeof fid == 'undefined'){
        		 fid = record.json[record.json.length-1];
        	 }
        	 if(typeof fid != 'undefined'){
            	 fidList.push(fid); 
        	 }
         }
       if (fidList.length==0) {
         Ext.Msg.alert('Zones tampons', 'Vous devez sélectionner ' +
           'au moins un objet autour duquel ' +
           'sera construit la zone tampon');
        }
        else {
          var lname = this.comboLayer.getValue().name;
          queryParams = {
            mode : this.mode,
            ratio : this.radiusField.getValue(),
            map : this.mapfile,
            layer : lname,
            fidList : fidList.join(',')
          };
        }
     } 
     else {
       if (this.feature==null || this.feature.geometry==null) 
         Ext.Msg.alert('Zones tampons', 'La géométrie sélectionnée ne permet pas de réaliser de zone tampon.');
       else {
        queryParams = {
            mode : this.mode,
            ratio : this.radiusField.getValue(),
            map : this.mapfile,
            geom : this.feature.geometry.toString()
          };
       }
     }
     if (queryParams!=null) { 
      // launching request... 
      Ext.Ajax.request({
        url: this.serviceUrl,
        success: this.drawFeatures,
        failure: function (response) { 
           Carmen.Util.handleAjaxFailure(response, control.windowTitle, true); 
        },
        scope : this,
        params: queryParams
      });
     }
  },
  
  // draw buffer geometries returned by buffer service... 
  drawFeatures : function (response) {
    var layerName = this.nameField.getValue(); 
    //this.bgColor = this.colorChooser.value;
    
    /*this.style = OpenLayers.Util.extend(this.style, {
	  strokeColor: '#' + this.bgColor,
	  fillColor: '#' + this.bgColor
	});*/     
    
    // adding new buffer layer to the map... 
    var bufferLayer = new Carmen.Layer.Vector(layerName);

    bufferLayer.styleMap.styles['default'].defaultStyle = OpenLayers.Util.extend({},this.style);
    this.map.addLayer(bufferLayer);
    
    // drawing features into layer    
    response = Ext.decode(response.responseText);
  	for (var i=0; i<response.length; i++) {
   	  var f = new OpenLayers.Feature.Vector(
  	    OpenLayers.Geometry.fromWKT(response[i].geom), {fid : response[i].fid});
  	  var f_simples = Carmen.Util.simplifyFeature(f);
  	  bufferLayer.addFeatures(f_simples);
  	}
    
    // adding layer to context...
    var descs = Carmen.Util.OLVector2CarmenOWSDesc(
  	 	bufferLayer, this.map.app.context.getNextLayerId(),
  	 	{ projection : this.map.app.map.projection });
   var layerIdx = this.map.app.context.addLayerDescs(descs);
    
    
    // adding layer control to layerTreeManager...
    var rgb = Carmen.Util.colorStr2css(this.bgColor);
    var nodeConfigBuffer = { 
        text : layerName,
        iconCls : null
      };

    var nodeAttsBuffer = { 
        type: Carmen.Control.LayerTreeManager.NODE_LAYER,
        layerName : layerName,
        iconDisplayStrategy : true,
        OlLayer : bufferLayer,
        initialOpacity : 1.0,
        forceOpacityControl : true,
        addedByUser : true,
        layerIdx : layerIdx,
        iconStyle : {
            'background-color': 'rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')',
            opacity: 0.7, 
            border: '2px solid #' + this.bgColor,
            width : '16px',
            height : '6px',
            margin : '2px',
            'background-image' : 'none',
            'vertical-align' : 'middle'  
          }
      };
    var node = this.map.getLayerTreeManager().buildLayerNode(nodeConfigBuffer, nodeAttsBuffer);
    this.map.getLayerTreeManager().addLayerNode(node, true);

    // drawing layer on the map    
  	bufferLayer.redraw();
  	
 },






  updateWindow : function() {
      //this.layerTree = this.getLayerTreeManager().getLayerTree().clone();
      this.layerTree = this.getLayerTreeManager().cloneLayerTree();
      //console.log(this.layerTree);
      var control = this; 
      this.layerTree.filter(
       function (n) {
         var type = Ext.valueFrom(n.attributes.type, -1, false);
  		   var res = (type == Carmen.Control.LayerTreeManager.NODE_GROUP && n.hasChildNodes() && n.getDepth()>0) || (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
  			   n.attributes.OlLayer instanceof Carmen.Layer.MapServerGroup && 
  			   n.attributes.OlLayer.handleSelection(n.attributes.layerName) &&
  			   n.attributes.OlLayer.hasSelection(n.attributes.layerName));
  	     return res;
       });
     
      //console.log(this.layerTree); 
      var layerNodes = this.layerTree.collectChildNodes( 
       function(n) {
         var type = Ext.valueFrom(n.attributes.type, -1, false);
         var res = (type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
  			   n.attributes.OlLayer instanceof Carmen.Layer.MapServerGroup);
         return res;
       });
  
      var data = [[null,'Sélectionnez une couche']].concat(
       Carmen.Util.array_map(layerNodes,
       function (e) {
         return [ { layer : e.attributes.OlLayer, name : e.attributes.layerName}, e.data.text ]
       }));
    
    if (data.length<=1) {
      this.win.hide();
      Ext.Msg.alert('Zones tampons', 'Aucune sélection n\'est active');
    }
    else {
      this.gridSelection.hide();
      this.comboLayer.getStore().removeAll();
      this.comboLayer.getStore().loadData(data);
      this.comboLayer.reset();
    }
  },
   
   getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },
  displayClassName: "CmnControlBuffer",
  CLASS_NAME: "Carmen.Control.Buffer"
});

Carmen.Control.Buffer.BUFFER_ON_SELECTION = 0;
Carmen.Control.Buffer.BUFFER_ON_FEATURE = 1; 




  

