  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Legend
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.Legend = new OpenLayers.Class(OpenLayers.Control, {

  mainPanel : Ext.create('Ext.Panel', {
    id: 'mainPanelLegend',
    header : false,
    border: false,
    margins: '0 0 0 0',
    resizeEl: 'layerTreeLegendPanel',
    layout: 'fit',
    tabConfig: {
      title: 'Légende',
      tooltip: 'Légende de la carte'
    }
  }), 
  
  
  
  
  // Properties
  layerTreeManager : null,

  layerTree : null,

  visible : true,
  
  // methods
  initialize: function(options) {
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
  },
  
  // function when cloning tree to attach a pointer to the src node 
  // as the attribute srcNode of each cloned node
  linkLegendNode : function(n) {
    if (n.attributes)
      n.attributes.srcNode = this;
    if (this.attributes) {
      if (this.attributes.linkedNodes==null)
        this.attributes.linkedNodes = [n];
      else
        this.attributes.linkedNodes.push(n);
    }    
    return n;
  },

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    
    
    this.legendTreeStore = Ext.create('Ext.data.TreeStore', {
      id : 'legendTreeStore',
      root: {
        expanded: true,
        text : 'Legende'
      },
      model : 'cmn.layerTreeModel'
    });
    
    this.legendTreeStore.setRoot(
      this.getLayerTreeManager().cloneLayerTree(
        this.legendTreeStore.getRoot(), this.linkLegendNode
      )
    );
    
    
    
    
    this.layerTreeLegendPanel = Ext.create('Ext.tree.Panel', {
      id: 'layerTreeLegendPanel',
      margins: '5 0 5 5',
      useArrows: false,
      hideHeaders: true,
      animate: false,
      border: false,
      //maxHeight: 300,
      constrain: true,
      width: 280,
      autoScroll: true,
      rootVisible: false,
      viewConfig: {
        trackOver: false,
        /*
         rowTpl :   //'<tpl if="values.record.data.checked == true" >'+
                    '"<table id="{rowId}" '+
                    'data-boundView="{view.id}" data-recordId="{record.internalId}" data-recordIndex="{recordIndex}" '+
                    'class="{[values.itemClasses.join(" ")]}" cellPadding="0" cellSpacing="0" {ariaTableAttr} '+
                    'style="width:100%;{itemStyle}">'+
                    '{%this.nextTpl.applyOut(values, out, parent)%}'+
                    '</table>"' //+
                    //'</tpl>'
        */ 
        
        // fields that will trigger a change in the ui that aren't likely to be bound to a column
        uiFields: ['expanded', 'loaded', 'leaf', 'icon', 'iconCls', 'loading', 'qtip', 'qtitle'],
          
        // treeRowTpl which is inserted into the rowTpl chain before the base rowTpl. Sets tree-specific classes and attributes
        treeRowTpl: [
            //'<tpl if="values.record.data.checked == true" >',
            '{%',
                'this.processRowValues(values);',
                'this.nextTpl.applyOut(values, out, parent);',
            '%}',
            //'</tpl>', 
            {
                priority: 10,
                processRowValues: function(rowValues) {
                    var record = rowValues.record,
                        view = rowValues.view;
                    // We always need to set the qtip/qtitle, because they may have been
                    // emptied, which means we still need to flush that change to the DOM
                    // so the old values are overwritten
                    rowValues.rowAttr['data-qtip'] = record.get('qtip') || '';
                    rowValues.rowAttr['data-qtitle'] = record.get('qtitle') || '';
                    if (record.isExpanded()) {
                        rowValues.rowClasses.push(view.expandedCls);
                    }
                    if (record.isLeaf()) {
                        rowValues.rowClasses.push(view.leafCls);
                    }
                    if (record.isLoading()) {
                        rowValues.rowClasses.push(view.loadingCls);
                    }
                    /*if (!(rowValues.record.data.checked == true)) {
                        rowValues.rowClasses.push('rowHidden');
                    }*/
                }
            }
        ]
     
      },
      
      //"{%var dataRowCls = values.recordIndex === -1 ? "" : " x-grid-row";%}<tr class="{[values.rowClasses.join(" ")]} {[dataRowCls]}" {rowAttr:attributes} {ariaRowAttr}><tpl for="columns">{%parent.view.renderCell(values, parent.record, parent.recordIndex, parent.rowIndex, xindex - 1, out, parent)%}</tpl></tr>"
      //"<td class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\"" + Ext.id() + "\"" : ""]} style="width:{column.cellWidth}px;<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}"><div {unselectableAttr} class="x-grid-cell-inner {innerCls}" style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>{value}</div></td>"
      columns: [{
          hideMode: 'visibility',
          xtype: 'treecolumn',
          dataIndex: 'text',
          width: '100%',
          cellWrap: true,
          cellTpl : [
              '<div class="row0" style="width:280px;">',
                '<div class="row0_first" style="width:{[(values.record.getDepth()-1)*20]}px;" >',
                '<tpl for="lines">',
                    /*'<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
                    '{parent.elbowCls}-<tpl if=".">line<tpl else>empty</tpl>" role="presentation"/>',*/
                    '<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
                    '{parent.elbowCls}-empty" role="presentation"/>',
                '</tpl>',
              /*'<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}',
                  '<tpl if="isLast">-end</tpl>',
                  '" role="presentation"/>',*/
                '</div>',
                '<div class="row0_second">',
                  '<tpl if="!values.record.attributes.classLegend">',
                  '<tpl if="!values.record.attributes.iconDisplayStrategy"/>', 
                    '<img src="{blankUrl}" role="presentation" class="{childCls} {baseIconCls} ',
                        '{baseIconCls}-<tpl if="leaf">leaf<tpl else>parent</tpl> {iconCls}"',
                        '<tpl if="icon">style="background-image:url({icon})',
                          '<tpl if="values.record.attributes.iconStyle"> {values.record.attributes.iconStyle}</tpl>',
                        '"',  
                        '<tpl elseif="values.record.attributes.iconStyle"> style="{values.record.attributes.iconStyle}"',
                        '</tpl>/>',
                  '</tpl>',
                '</tpl>',
                '<tpl if="href">',
                    '<a href="{href}" role="link" target="{hrefTarget}" class="{textCls} {childCls}">{value}</a>',
                '<tpl else>',
                      '<span class="{textCls} {childCls}">{value}</span>',
                      '<tpl if="values.record.attributes.classLegend">', 
                        '<tpl for="values.record.attributes.classLegend">',
                          '<br/>',
                          '<img src="{parent.blankUrl}" role="presentation" class=" x-tree-elbow-img x-tree-elbow-empty',
                          '" ',
                          'style="" ></img>',
                          '<img src="{parent.blankUrl}" role="presentation" class=" x-tree-icon x-tree-icon-leaf cmnLayerTreeLayerNodeIcon',
                          ' " ',
                          'style="background-image:url({icon})" ></img>',
                          '<span class="x-tree-node-text" >{text}</span>',
                        '</tpl>',  
                      '<tpl elseif="values.record.attributes.iconDisplayStrategy"/>',
                        '<img id="legendNode-{record.internalId}_iconDisplay" src="<tpl if="icon">{icon}<tpl else>{blankUrl}</tpl>" role="presentation" class="{iconCls}"/>',
                      '</tpl>',
                '</tpl>',
                '</div>',
              '</div>']
            
          
        }
      ],
      //node.attributes.classLegend
      selType: 'cellmodel',      
      disableSelection: true,
      store: this.legendTreeStore
/*
      listeners: {
              'render' : function(col, eopts) {
                  var me=this;
                },
              scope : this
            }
  */    
    
    });

    
    
  
  /*
   
         var legendTreePanel = legendNode.getOwnerTree();
      if (legendTreePanel) {
        var legendView = legendTreePanel.getView();
        var el = Ext.fly(legendView.getNode(legendNode));
        if (el)
          el.setDisplayed(visibility);
   
   
   */ 
    
  
  
  
  
    
    this.mainPanel.add(this.layerTreeLegendPanel);
    /*this.btn.setHandler(
        function(event, toolEl, panelHeader) {
          this.showWindow();
        },
        this
    )*/
    /*
    this.map.app.ui.dataPanel.addTool(
    
      {
        qtip: 'Légende',
        xtype: 'tool',
        renderTpl: [
                  '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-bars  fa-1x"' +
                  '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                ],
        scope:this,
      handler : function(){this.showWindow()}
      }
    );
    */ 
    /*
    this.layerTreeLegendPanel.expandAll();
    if (this.visible) {
      //obedel : ext5 pb with layertree init
      this.initTree();
      this.map.app.ui.addToDataPanel(this.mainPanel,1);
    }*/

    
    
  },
   showWindow : function() {
    
    //On definit notre fenetre contenant notre panel d'onglet
        this.win = Ext.create('Ext.Window',{
          title: 'Légende',
          width: 320,
          closeAction:'hide',
          //height: 700,
          layout: 'fit',
          items: this.mainPanel,
          constrain: true,
          //plain: true,
          x: 480,
          y: 100,
          autoDestroy: true,
          resizable: true,
          hidden:true
        });
	     
        // adding listeners to link window closing and control deactivation   
        var control = this;
        /*this.win.on(
          'hide',
          function(w) { control.btn.toggle(false); },
          this.win);
        */
       this.win.show(); 
       
   },
   hideWindow : function() {
    
    this.win.hide();
       
   },
  


  // displaying results
  initTree : function() {
    //this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeLegend);
    //this.layerTreeLegendPanel.setRootNode(this.layerTree);
    // obedel : in case of update
    /*
    	// remove the old tree
    	this.layerTreeLegendPanel.getRootNode().destroy();
      // add the new one and render it
      this.layerTreeLegendPanel.setRootNode(this.layerTree);
      //this.layerTreeLegendPanel.getRootNode().render();
    */
    //this.layerTree.filter(this.filterNodeLayerVisible);
  },


  update: function() {
    //this.legendTreeStore.removeAll();
    this.legendTreeStore.setRoot(
      this.getLayerTreeManager().cloneLayerTree(
        this.legendTreeStore.getRoot(), this.linkLegendNode
      )
    );
  },
  



  getLayerTreeManager : function() {
    if (this.layerTreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },

  transformNodeLegend : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
    cloneAtts.checked = '',
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expanded = true;
  	cloneAtts.hasBottomElt = false;
  	cloneAtts.rightIcon = null;
//  	cloneAtts.uiProvider = (atts.text == Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL)  ? 
//  	  Carmen.RootTreeNodeUI : 
//  	  Carmen.TreeNodeUI;
  	
  	return cloneAtts;
  },
  
  filterNodeLayerVisible :  function(node) {
    var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && 
          ((node.attributes.OlLayer.hasSubLayers!=null && node.attributes.OlLayer.hasSubLayers() &&
        	  node.attributes.OlLayer.getSubLayerVisibility(node.attributes.layerName)) ||  
       	   (node.attributes.OlLayer.hasSubLayers!=null && !node.attributes.OlLayer.hasSubLayers() && 
       	    node.attributes.OlLayer.getVisibility()) ||
       	   (node.attributes.OlLayer.hasSubLayers==null && node.attributes.OlLayer.getVisibility()))	
       	  && node.attributes.visible == true) || 
        (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_CLASS));
    return res;
  },
  
  
  
  /**
   * @return the HTML node in which the legend is rendered
   * @param staticRendering : if true remove all dynamic 
   * properties in the HTML (anchor, expand/collapse icons, cursor...)
   */
  toHTML : function(staticRendering) {
  	// forcing rendering

   if (!this.layerTreeLegendPanel.rendered) {
      //var panelVisible = !this.map.app.ui.dataButton.pressed;
      //this.map.app.ui.dataButton.toggle(true);
      //var activTab =  this.map.app.ui.dataPanel.getActiveTab();
      //this.map.app.ui.dataPanel.setActiveTab(1);
      //this.map.app.ui.dataPanel.setActiveTab(activTab);
      //this.map.app.ui.dataButton.toggle(paneVisible);
      this.showWindow();
      this.hideWindow();
    }
    
    return this.layerTreeLegendPanel.getEl();
  },
  
  
  // private
  // change the rendering aspect of the node n
  // remove all dynamic content (expand/collapse icons, 
  // anchor, cursor special styles...)
  toStaticNodeUI : function(n) {
  	var ui = n.getUI();
		
		// replace expand/collapse dynamic icons
  	var elt = Ext.fly(ui.ecNode);
  	if (elt==null)
  		return true;
  	var cls = '';
  	var newClass = '';
  	if (elt.hasClass('x-tree-elbow-minus')) {
  		cls = 'x-tree-elbow-minus';
  		newClass = 'x-tree-elbow';
  	}
  	else if (elt.hasClass('x-tree-elbow-plus')) {
  	  cls = 'x-tree-elbow-plus';
  		newClass = 'x-tree-elbow';
  	} else if (elt.hasClass('x-tree-elbow-end-minus')) {
  		cls = 'x-tree-elbow-end-minus';
  		newClass = 'x-tree-elbow-end';
  	}
  	else if (elt.hasClass('x-tree-elbow-end-plus')) {
  	  cls = 'x-tree-elbow-end-plus';
  		newClass = 'x-tree-elbow-end';
  	};
		if (cls!='') {
	   	Ext.fly(ui.ecNode).replaceClass(cls, newClass);
  	 	ui.ecc = ui.ecNode.className;
		}
		
		// changing anchor to span		
		var elt = document.createElement('span');
		elt.className = "x-tree-node-anchor";
		Ext.fly(elt).insertFirst(ui.anchor.firstChild);
		Ext.fly(ui.anchor).replaceWith(elt);
		
		// replacing cursor style		
		ui.wrap.style.cursor = 'default';
		c = ui.wrap.firstChild;
		while (c!=null) {
			c.style.cursor = 'default';
			c = Ext.fly(c).next(null,true);
		}
  	return true;
  },
  
  renderHTMLto : function(elt) {
  	this.layerTree = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeLegend);
  	this.layerTreeLegendPanel.setRootNode(this.layerTree);
  	this.layerTree.filter(
  		function(node) {
  			var res = true;
		    res = ('type' in node.attributes) && 
		      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
		       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && 
		        node.attributes.initialChecked &&
		        node.attributes.visible == true) || 
		        (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_CLASS));    
		    return res;
  		});
  	this.layerTreeLegendPanel.render(elt);
  },
  
  addLegendNode : function(layerNode) {
    var me = this;
    var legendNode = layerNode.clone(me.linkLegendNode);
    var tree = this.layerTreeLegendPanel.getRootNode();
    if (tree.hasChildNodes()) 
      tree.insertBefore(legendNode, tree.childNodes[0]);
    else
      tree.appendChild(legendNode);
    /*if (legendNode.attributes.iconDisplayStrategy) {
      
      var elt_icon = this.layerTreeLegendPanel.getById('legendNode-'+legendNode.internalId+'_iconDisplay');
      console.log('el_icon',elt_icon);  
      if (elt_icon) {
        elt_icon.on('load', function(e) { 
          // put here the strategy for changing icon position...
          //if (this.domIcon.height>40) ...
          //console.log(this.domIcon, ' loaded');
        }, 
        { node : legendNode, domIcon : elt_icon},
        { single : true });
      }
      
    }*/
  },
  
  
  moveNode : function(n,index) {
    //obedel strategy : use parent.insertChild
    n.parentNode.insertChild(index+1,n);
  
  },
  
  CLASS_NAME: "Carmen.Control.Legend"

});


