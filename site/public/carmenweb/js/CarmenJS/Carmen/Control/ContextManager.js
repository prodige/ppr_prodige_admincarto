/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.ContextManager
 * 
 * Inherits from: - <OpenLayers.Control>
 */
var tabContextRadioElt =new Array();
for(var i=0; i< tabContextParam.length; i++){
  tabContextRadioElt.push( 
  {
    boxLabel :tabContextParam[i][1],
    name :'rb-time',
    checked : (i==0 ? true : false),
    inputValue :tabContextParam[i][0]
  });
}



Carmen.Control.ContextManager = new OpenLayers.Class(OpenLayers.Control, {

	// UI components
	btn : Ext.create('Ext.Button', {
		tooltip :'Sauvegarde et restauration de contextes de carte',
		tooltipType :'title',
		cls :'x-btn-icon', /* cmn-tool-context',*/
		enableToggle: true,
    toggleGroup: 'mainMapControl',
    disabled: false,
    text : '<i class="fa fa-save fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight
	}),

  mainPanel: new Ext.create('Ext.tab.Panel', {
    activeItem: 0
  }),

	//obedel debug extjs 2.2.1 -> 3.0.0
	//savePanel :new Ext.FormPanel( {
	savePanel : Ext.create('Ext.form.FormPanel', {
		id :'saveContextPanel',
		autoScroll :true,
		header :true,
    bodyPadding: 10,
		title :'Sauvegarde',
		items : [ {
			id :'rb-mode',
		  xtype :'radiogroup',
			fieldLabel :'Mode de sauvegarde',
			name : 'rb-mode',
			itemCls :'x-check-group-alt',
			columns :1,
			items : [ {
				boxLabel :'sur&nbsp;le&nbsp;serveur',
				name :'rb-mode',
				inputValue :0,
				checked :true
			}, {
				boxLabel :'sur&nbsp;votre&nbsp;machine',
				name :'rb-mode',
				inputValue :1
			} ]
		},
		{
		xtype :'radiogroup',
      fieldLabel :'Durée de sauvegarde',
      name : 'rb-time',
      id : 'rb-time',
      itemCls :'x-check-group-alt',
      columns :1,
      items :tabContextRadioElt
		}
		
		],
		buttons : [ {
			id :'saveContextButton',
			text :'Enregistrer le contexte'
		} ]
	}),
	
	loadPanel :Ext.create('Ext.FormPanel', {
		id :'loadContextPanel',
		autoScroll :true,
    	bodyPadding: 10,
		header :true,
		title :'Chargement',
		fileUpload :true,
		buttons : [{
			xtype :'button',
			labelStyle :'',
			id :'loadContextButton',
			text :'Charger le contexte'
		}]
	}),
	
	fileField : Ext.create('Ext.form.field.File', {
    id :'loadContextFileField',
    emptyText :'Sélectionner un fichier',
    fieldLabel :'Fichier OWS',
    allowBlank: false,
    name :'uploadedfile',
    buttonText: 'Parcourir',
    vtype : 'owsContext'
	}),
	
	win : Ext.create('Ext.Window', {
		id :'saveContextWindow',
		width :350,
    constrain : true,
		title :'Sauvegarde et chargement de contextes',
		modal : false,
		resizable :false,
		closeAction :'hide',
	}),

	// properties
	mapfile :null,
	serviceUrl :null,

	// methods
	initialize : function(serviceUrl, mapfile, options) {
	  OpenLayers.Control.prototype.initialize.apply(this, [options]);    
	   
		this.mapfile = mapfile;
		this.serviceurl = serviceUrl;
	},

	setMap : function(map) {
		OpenLayers.Control.prototype.setMap.apply(this, arguments);

	  this.btn.addListener('toggle', 
	      Carmen.Util.buildExt2olHandlerToggle(this));
				
	  this.map.app.ui.addToToolPanel(this.btn, this.toolbarTarget, this.toolbarPos);

    // adding listeners to link window closing and control deactivation   
    var control = this;
    this.win.on(
      'hide',
      function(w) { control.btn.toggle(false); },
      this.win);

		this.map.app.ui.doLayout();
	},

  activate: function() {
  	OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    this.showWindow();
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();
  },


	// displaying main window
	showWindow : function() {
		if (!this.win.rendered) {
			this.mainPanel.add(this.savePanel);
			this.loadPanel.add(this.fileField);
			this.mainPanel.add(this.loadPanel);
			this.win.add(this.mainPanel);
			//this.win.add(this.iframePanel);
		}

		this.win.doLayout();
		if (!this.win.isVisible()) {
			this.win.show();
		}
	  Ext.getCmp('rb-mode').on('change', function(){
      var form = Ext.getCmp("saveContextPanel").getForm();
      switch (parseInt(form.getValues()['rb-mode'])) {
        case 1:
          Ext.getCmp("rb-time").hide();
          break;
        default:
          Ext.getCmp("rb-time").show();
          break;
      };
      
    });
		var saveBtn = Ext.getCmp('saveContextButton');
		saveBtn.on('click', this.onSave, this);
		
		var loadBtn = Ext.getCmp('loadContextButton');
    loadBtn.on('click', this.onLoad, this);
	},

	onSave : function(btn, evt) {
		// building params
		var queryParams = this.buildParams();
		// performing queries
		var form = this.savePanel.getForm();
		switch (parseInt(form.getValues()['rb-mode'])) {
		case this.SAVE_SERVER:
			this.performQueryServer(queryParams);
			break;
		default:
			// this.SAVE_LOCAL
			this.performQueryLocal(queryParams);
			break;
		};
		// closing tool
		this.win.hide();
	},

  onLoad : function(btn, evt) {
    
    var filePath = this.fileField.getValue();
    var form =  Ext.getCmp("loadContextPanel").getForm(); 
    form.waitTitle = 'Chargement de contexte';
    if (form.isValid()){
      form.submit({
          url: '/services/LoadContext/index.php',
          waitMsg: 'Transmission du fichier contexte',
          success: function(fp, o){
            Ext.Msg.confirm("Chargement de contexte", 
              "Mettre à jour la carte courante avec le contexte " + filePath + ".",
              function(buttonId) {
                if (buttonId== 'yes') {
                  Carmen.Util.loadPage(Carmen.Util.unhtmlspecialchars(o.result.contextURL));
                }
              });
          },
          failure: function(form, action) {
          	var msg = action.result.errmsg ? action.result.errmsg : 'Problème lors de l\'accès au serveur.';
            switch(action.result.failureType) {
            	case Carmen.CARMEN_ERROR_SESSION_EXPIRED :
            	  Ext.Msg.alert("Echec", msg);
            	  Carmen.Util.loadPage();
            	  break;
            	default:
            	 Ext.Msg.alert("Echec", msg);
            	 break;
            }
          }
      });
	    // closing tool
	    this.win.hide();
    }
  },

	// build the parameters of the request
	buildParams : function() {
		var form = this.savePanel.getForm();
		var time = parseInt(form.getValues()['rb-mode']) == this.SAVE_SERVER ? 
		 form.getValues()['rb-time'] : 
		 tabContextParam[tabContextParam.length-1][0] ;
		var jsonContext = this.map.app.updateContext(false);
		// WFS handling should be done after context update and before context serialization 
		var linked_wfs_layers = this.handleWFSlayers(time);
   	var xmlContext = Carmen.Util.json2xml(jsonContext, this.XML_HEADER);
		var params = {
			owsContext : xmlContext,
			map : this.mapfile,
			time : time,
			linked_wfs_layers : Ext.encode(linked_wfs_layers) 
		};

		return params;
	},

	// launch the request to the server
	performQueryLocal : function(queryParams) {
		var control = this;
		var url = '/services/SaveContext/getBack.php';
	  var f = control.savePanel;
		Carmen.Util.doFormUpload(f, url, queryParams);
	},

	performQueryServer : function(queryParams) {
		var control = this;
		var received = function(response, opts) {
			response = Ext.decode(response.responseText);
			Ext.MessageBox.show({
				title : 'Sauvegarde de contexte sur le serveur',
				buttons : Ext.MessageBox.OK,
				message :
							"Vous pouvez revenir au contexte courant de cette carte, en saisissant cette URL dans la barre d\'adresse de votre navigateur."+
							"(durée du contexte : "+ response.contextTime + ")",
        multiline : true,
        defaultTextHeight: 40,
        value : response.contextURL
			});
		};
		Ext.Ajax.request( {
			url :'/services/SaveContext/index.php',
			success : function(response, opts) {
				received(response);
			},
			failure : function (response, opts) {
        Carmen.Util.handleAjaxFailure(response, "Sauvegarde de contexte"); 
	    },
			params :queryParams
		});
	},

  handleWFSlayers : function(newMapLocation) {
    var res = [];
    var context = this.map.app.context;
    var layer = context.layer;
    for (var i=0; i<layer.length; i++) {
      if (layer[i].Extension && layer[i].Extension.layerType &&
         layer[i].Extension.layerType == "mapfile_wfs") {
        var layerUrl = Carmen.Util.parseURL(layer[i].Server.OnlineResource.attributes.url);
        var mapfilePath = Carmen.Util.parsePath(layer[i].Extension.layerSettings_mapfile);
        if (layerUrl && mapfilePath) {
          // registering wfs mapfile names
          res.push(layerUrl.end);
          // changing mapfile location reference wrt with context time policy
          layer[i].Server.OnlineResource.attributes.url = 
            layerUrl.start + "../" + newMapLocation + "/" + layerUrl.end;
          layer[i].Extension.layerSettings_mapfile = 
            mapfilePath.start + "../" + newMapLocation + "/" + mapfilePath.end;
           
        }
      }  
    }
    return res;        
  },

	SAVE_SERVER_LIFE :'7 jours',
	SAVE_SERVER :0,
	SAVE_LOCAL :1,
	XML_HEADER :'<?xml version="1.0" encoding="UTF-8"?>',

	CLASS_NAME :"Carmen.Control.ContextManager"
});

Carmen.Control.ContextManager.SAVE_SERVER = 0;
Carmen.Control.ContextManager.SAVE_LOCAL = 1;
