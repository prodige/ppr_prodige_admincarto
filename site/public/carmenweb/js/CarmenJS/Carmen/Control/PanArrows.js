/**
 * @_requires OpenLayers/Control/PanZoom.js
 */

/**
 * Class: Carmen.Control.PanArrows
 * 
 * Inherits from:
 *  - <OpenLayers.Control.PanZoom>
 */
Carmen.Control.PanArrows = OpenLayers.Class(OpenLayers.Control, {

    /** 
     * APIProperty: slideFactor
     * {Integer} Number of pixels by which we'll pan the map in any direction 
     *     on clicking the arrow buttons. 
     */
    slideFactor: 50,

    /** 
     * Property: buttons
     * {Array(DOMElement)} Array of Button Divs 
     */
    buttons: null,

    /** 
     * Property: position
     * {<OpenLayers.Pixel>} 
     */
    position: null,

    /**
     * Constructor: OpenLayers.Control.PanZoom
     * 
     * Parameters:
     * options - {Object}
     */
    initialize: function(options) {
        this.position = new OpenLayers.Pixel(0,0);
        OpenLayers.Control.prototype.initialize.apply(this, arguments);
    },

    /**
     * APIMethod: destroy
     */
    destroy: function() {
        OpenLayers.Control.prototype.destroy.apply(this, arguments);
        while(this.buttons.length) {
            var btn = this.buttons.shift();
            btn.map = null;
            OpenLayers.Event.stopObservingElement(btn);
        }
        this.buttons = null;
        this.position = null;
    },

   setMap: function(map) {
    OpenLayers.Control.PanZoom.prototype.setMap.apply(this, arguments);

    this.map.app.ui.getMainPanel().on(
      'resize', 
      function() {
      	 this.draw(this.position);
      	 }, 
      this);
    
    
  },

    //


    /**
     * Method: draw
     *
     * Parameters:
     * px - {<OpenLayers.Pixel>} 
     * 
     * Returns:
     * {DOMElement} A reference to the container div for the PanZoom control.
     */
    draw: function(px) {
        // initialize our internal div
        OpenLayers.Control.prototype.draw.apply(this, arguments);
        px = this.position;

        // place the controls
        this.buttons = [];

        var sz = new OpenLayers.Size(9,9);
    
        var mp_height = this.map.app.ui.getMainPanel().getEl().getHeight(true);
        var mp_width = this.map.app.ui.getMainPanel().getEl().getWidth(true);

        px_north = new OpenLayers.Pixel((mp_width+sz.w)/2, 0);
        this._updateButton("panup", "/IHM/images/Pan_Nord_OFF.png", px_north, sz);
 
        px_south = new OpenLayers.Pixel((mp_width+sz.w)/2, mp_height-sz.h);
        this._updateButton("pandown", "/IHM/images/Pan_Sud_OFF.png", px_south, sz);
 
        px_east = new OpenLayers.Pixel(mp_width-sz.w, (mp_height-sz.h)/2);
        this._updateButton("panright", "/IHM/images/Pan_Est_OFF.png", px_east, sz);
 
        px_west = new OpenLayers.Pixel(0, (mp_height-sz.h)/2);
        this._updateButton("panleft", "/IHM/images/Pan_Ouest_OFF.png", px_west, sz);
        
        px_northeast = new OpenLayers.Pixel(mp_width-sz.w, 0);
        this._updateButton("panupright", "/IHM/images/Pan_NordEst_OFF.png", px_northeast, sz);
        
        px_sudeast = new OpenLayers.Pixel(mp_width-sz.w, mp_height-sz.h);
        this._updateButton("pandownright", "/IHM/images/Pan_SudEst_OFF.png", px_sudeast, sz);
        
        px_northwest = new OpenLayers.Pixel(0, 0);
        this._updateButton("panupleft", "/IHM/images/Pan_NordOuest_OFF.png", px_northwest, sz);
        
        px_sudwest = new OpenLayers.Pixel(0, mp_height-sz.h);
        this._updateButton("pandownleft", "/IHM/images/Pan_SudOuest_OFF.png", px_sudwest, sz);

        return this.div;
    },
    
    /**
     * Method: _updateButton
     * 
     * Parameters:
     * id - {String} 
     * img - {String} 
     * xy - {<OpenLayers.Pixel>} 
     * sz - {<OpenLayers.Size>} 
     * 
     * Returns:
     * {DOMElement} A Div (an alphaImageDiv, to be precise) that contains the
     *     image of the button, and has all the proper event handlers set.
     */
    _updateButton:function(id, img, xy, sz) {
        var full_id = this.id + '_' + id;
        var btn = Ext.getDom(full_id);
        if (btn==null) {
          var imgLocation = img;
          var btn = OpenLayers.Util.createAlphaImageDiv(full_id, 
                                    xy, sz, imgLocation, "absolute");

          //we want to add the outer div
          this.div.appendChild(btn);

          OpenLayers.Event.observe(btn, "mousedown", 
            OpenLayers.Function.bindAsEventListener(this.buttonDown, btn));
          OpenLayers.Event.observe(btn, "dblclick", 
            OpenLayers.Function.bindAsEventListener(this.doubleClick, btn));
          OpenLayers.Event.observe(btn, "click", 
              OpenLayers.Function.bindAsEventListener(this.doubleClick, btn));
          btn.action = id;
          btn.map = this.map;
          btn.slideFactor = this.slideFactor;

          //we want to remember/reference the outer div
          this.buttons.push(btn);
        }
        else
          OpenLayers.Util.modifyDOMElement(btn, full_id, xy, sz,"absolute");
        return btn;
    },
    
    /**
     * Method: doubleClick
     *
     * Parameters:
     * evt - {Event} 
     *
     * Returns:
     * {Boolean}
     */
    doubleClick: function (evt) {
        OpenLayers.Event.stop(evt);
        return false;
    },
    
    /**
     * Method: buttonDown
     *
     * Parameters:
     * evt - {Event} 
     */
    buttonDown: function (evt) {
        if (!OpenLayers.Event.isLeftClick(evt)) {
            return;
        }

        switch (this.action) {
            case "panup": 
                this.map.pan(0, -this.slideFactor);
                break;
            case "pandown": 
                this.map.pan(0, this.slideFactor);
                break;
            case "panleft": 
                this.map.pan(-this.slideFactor, 0);
                break;
            case "panright": 
                this.map.pan(this.slideFactor, 0);
                break;
            case "panupright":
                this.map.pan(this.slideFactor, -this.slideFactor);
                break;
            case "panupleft":
                this.map.pan(-this.slideFactor, -this.slideFactor);
                break;
            case "pandownright":
                this.map.pan(this.slideFactor, this.slideFactor);
                break;
            case "pandownleft":
                this.map.pan(-this.slideFactor, this.slideFactor);
                break;
            
        }

        OpenLayers.Event.stop(evt);
    },
    //to : see why openLayers is necessary in the CLASS_NAME to display the control
    // tips : look at displayClassName in OpenLayer.Control
    CLASS_NAME: "OpenLayers.Control.PanArrows"
});
