/**
 * @_requires OpenLayers/Control/MousePosition.js
 */

/**
 * Class: Carmen.Control.Coordinate
 
 * Inherits from:
 *  - <OpenLayers.Control.MousePosition>
 */
tabStoreProj = new Array();
for (var i=0; i< tabEPSG.length; i++){
  data = new Array("EPSG:"+tabEPSG[i][0], tabEPSG[i][1]);
  tabStoreProj.push(data);
}

Carmen.Control.Coordinate = new OpenLayers.Class(OpenLayers.Control.MousePosition, {

  // Todo add display config option to allow or not scale box display
  // displayConfig: 0,
 
  coordinateSysBox : Ext.create('Ext.form.ComboBox', {
    queryMode: 'local',
    triggerAction: 'all',
    emptyText: 'Sélectionnez un sytème de coordonnées',
    editable : false,
    width: 250,
    fieldLabel: 'SRS',
    labelAlign: 'left',
    labelWidth: 30,
    labelSeparator: '',
    valueField: 'projName',
    displayField: 'projPrettyName',
    store:
      new Ext.data.ArrayStore({
        fields: ['projName', 'projPrettyName'],
        data : tabStoreProj
        }
      )
  }),

	projName : 'unknown',

  coordinateLabel : Ext.create('Ext.form.Label', {
    width: 180,
    id: 'coordLabel'
    //flex : 1
  }),
  
  
  coordinateBtn : Ext.create('Ext.button.Button', {
    iconCls: '',/*'button_coord', */
    id: 'coordButton',
    text: '<i class="fa icon-projection1"></i>',
    tooltip : 'choix du système de projection des coordonnées affichées'
  }),
  
   
  initialize: function(options) {
    options = OpenLayers.Util.extend(options, 
      {
      	prefix: " X : ",
      	separator: ', Y :'
      });
    
    OpenLayers.Control.MousePosition.prototype.initialize.apply(this, [options]);
  },
  
  setMap: function(map) {
    this.map = map;
    
    var currentProjIdx = this.initializeCombo(this.map.projection);
    if (currentProjIdx!=-1) {
      this.map.app.ui.addToBottomToolbar(this.coordinateSysBox,3);
			this.coordinateSysBox.setValue(this.projName);
    	this.displayProjection = new OpenLayers.Projection(this.projName);
    	this.coordinateSysBox.on('select', this.comboSelect, this);
    }
    else {
      Ext.Msg.alert("Projection inconnue", "Le système de coordonnées de la carte n'est pas connu. " +
          "Les informations fournies par les outils de mesure et de lecture de " +
          "coordonnées ne sont pas garanties.");
    }
    
    this.map.app.ui.addToBottomToolbar(this.coordinateLabel,3);
    /*
    tb.add(this.coordinateBtn);
    tb.add({ xtype: 'tbfill' });
    tb.add(this.coordinateLabel);
    */ 
    
    //this.coordinateBtn.setHandler(this.showHideCoordBox,this);
    
    
    // add to init the div here, once the extPanel have been rendered  
    //obedel debug extjs 2.2.1 -> 3.0.0
    this.map.app.ui.doLayout();
    this.div = this.coordinateLabel.getEl().dom;
    // Call parent constructor then...
    OpenLayers.Control.MousePosition.prototype.activate.apply(this, arguments);
  },
 
 	comboSelect : function(combo, rec, idx) {
 	  var newProjName = combo.getValue();

 	  if (newProjName != this.projName) {
			this.projName = newProjName; 
			this.displayProjection = new OpenLayers.Projection(this.projName);
			this.redraw();
 	  }
 	},
 	
 	initializeCombo : function(strProj) {
		var projName = strProj.toLocaleUpperCase();
		var projIndex = this.coordinateSysBox.getStore().find('projName', projName);
		this.projName = projIndex==-1 ? 'unknown' : projName;
		return projIndex;
 	},
 
   /**
   * Method: formatOutput
   * Provides custom display output
   *
   * Parameters:
   * lonLat - {<OpenLayers.LonLat>} Location to display
   */
  formatOutput: function(lonLat) {
      var isGeoProj = (this.displayProjection.getUnits()==null);
      var digits = isGeoProj ? parseInt(this.numDigits) : 2;
      var newHtml =
          this.prefix +
          lonLat.lon.toFixed(digits) +
          this.separator + 
          lonLat.lat.toFixed(digits) +
          this.suffix;
      return newHtml;
   },
  
  showHideCoordBox : function(btn,e) {
    if (this.coordinateSysBox.isVisible()) {
      this.coordinateSysBox.hide();
    }
    else {
	  app.map.hideControls();
      var x = this.coordinateBtn.getX();
      var y = this.coordinateBtn.getY();
      this.coordinateSysBox.setX(x);
      this.coordinateSysBox.setY((y-35));
      this.coordinateSysBox.show();
    }
  },
  hide : function(btn,e) {
    //this.coordinateSysBox.hide();
  },
 
 
  CLASS_NAME: "Carmen.Control.Coordinate"
});
