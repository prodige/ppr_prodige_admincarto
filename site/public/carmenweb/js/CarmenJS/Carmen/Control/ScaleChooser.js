/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.ScaleChooser
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.ScaleChooser = new OpenLayers.Class(OpenLayers.Control, {

  scaleValue : [],

  scaleDenoms : ['2000000', '1000000', '500000', '250000', '200000', 
   '150000', '100000', '90000', '80000', '70000', '60000', '50000',
   '40000', '30000', '25000', '20000', '15000', '10000', '9000', 
   '8000', '7000', '6000', '5000', '4000', '3000', '2000', '1000'], 

  
  scaleBox : null,
  
  scaleBtn : Ext.create('Ext.button.Button', {
    iconCls: '',/*'button_scale', */
    id: 'scaleButton',
    text: '<i class="fa falk-scale"></i>',
    tooltip : 'choix de l\'échelle'
  }),
  
  initialize: function(options) {
	  OpenLayers.Control.prototype.initialize.apply(this, arguments);
    
    var store = new Ext.data.ArrayStore({
          fields: ['scale','value'],
          data : [['1','1'],['2','2']]
    });

    this.scaleBox =  Ext.create('Ext.form.ComboBox', {
      fieldLabel: 'Echelle',
      labelAlign: 'left',
      labelWidth: 50,
      typeAhead: true,
      queryMode: 'local',
      triggerAction: 'all',
      emptyText: 'Zoomer à l\'échelle',
      selectOnFocus: true,
      width: 185,
      displayField: 'value',
      listConfig : { loadingText: 'Chargement' },
      valueField: 'scale',
      editable : true,
      store: store 
    });
  },
  

  setMap: function(map) {
      OpenLayers.Control.prototype.setMap.apply(this, arguments);      
      this.updateScaleValues(this.scaleDenoms);
      this.scaleBox.getStore().loadData(this.scaleValue);
      
      // adding the box to the bottom toolbar
      this.map.app.ui.addToBottomToolbar(this.scaleBox,1);
      
      this.scaleBtn.setHandler(this.showHideScaleBox, this)
      
      
      this.scaleBox.addListener('select',
        function(combo, records, eopts) {
          if (records.length>0) {
            record=records[0];
            
            if (record.data.scale!='maxExtentScale') {
              map.zoomToScale(parseFloat(record.data.scale),true);
            }
            else 
              map.zoomToMaxExtent();
          }
        }); 
      this.map.app.ui.doLayout();
      this.map.events.register('zoomend', this, 
        this.update);      
  },
  
  // display the scale approximating the best the map scale
  update : function() {
  	var mapScale = this.map.getScale();
  	var i = 1;
  	var found = false;
  	while (i<this.scaleValue.length && !found) {
  		var scale = parseFloat(this.scaleValue[i][0]);
  		found = mapScale>scale;
  		i++;
  	}
  	i--;    
    if (found && (i>1))
    	 i = ((parseFloat(this.scaleValue[i-1][0])-mapScale)<
    	     mapScale-parseFloat(this.scaleValue[i][0])) ?
     	     i-1 : i;      
    this.scaleBox.setValue(this.scaleValue[i][0]);
  },
  
  updateScaleValues : function(scaleDenoms) {
    this.scaleValue = [ ['maxExtentScale', 'Emprise Maximale'] ];
    for (var i=0; i<scaleDenoms.length; i++) {
      this.scaleValue.push([scaleDenoms[i], Carmen.SpecialChars["around"] +
      Carmen.Util.scaleTextFormater(scaleDenoms[i])]);
    }
    this.scaleBox.getStore().removeAll();
    this.scaleBox.getStore().loadData(this.scaleValue);
  },
  
  showHideScaleBox : function(btn,e) {
    if (this.scaleBox.isVisible()) {
      this.scaleBox.hide();
    }
    else {
	  app.map.hideControls();
      var x = this.scaleBtn.getX();
      var y = this.scaleBtn.getY();
      this.scaleBox.setX(x);
      this.scaleBox.setY((y-35));
      this.scaleBox.show();
    }
  },
  
  hide : function(btn,e) {
    //this.scaleBox.hide();
  },
  
  CLASS_NAME: "Carmen.Control.ScaleChooser"
});
