/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.AddLocalData
 * 
 * Inherits from: - <OpenLayers.Control>
 */
Carmen.Control.AddLocalData = new OpenLayers.Class(OpenLayers.Control,{
  btn : new Ext.Button({
	tooltip 		: 'Visualiser un fichier cartographique local',
	tooltipType		: 'title',
	cls				: 'x-btn-icon cmn-tool-addwfs',
	enableToggle	: true,
	toggleGroup		: 'mainMapControl',
	disabled		: false
  }),
  win : null,
  
  initialize : function(options){
    OpenLayers.Control.prototype.initialize.apply(this, arguments);
  },
  
  setMap : function(map){
	OpenLayers.Control.prototype.setMap.apply(this, arguments);
	this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
	var tb = this.map.app.ui.getTopToolBar();
	tb.add(this.btn);
	this.map.app.ui.doLayout();
  },
  
  activate : function(){
	OpenLayers.Control.prototype.activate.apply(this, arguments);
	this.map.toolTipDeactivate();
	this.showWindow();
  },
  
  deactivate : function(){
	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
	if(this.win.rendered)
	  this.win.close();
	this.map.toolTipActivate();
  },
  
  showWindow : function(){
	var localDataPanel = new Ext.FormPanel({
	  id				: 'AddLocalData',
	  autoScroll		: true,
  	  fitToFrame		: true,
	  labelWidth		: 75,
	  height			: 415,
	  html				: '<p>Les donn&eacute;es cartographiques que vous allez utiliser ne seront accessibles que durant le temps de votre connexion et seulement par vous m&ecirc;me.</p>'+
		              	  '<p>Celles-ci ne seront pas sauvegard&eacute;es.</p><hr/>',
	  bodyStyle			: 'padding: 5px 0 0 5px; color: #000000;',
	  labelStyle		: 'color: #000000',
	  items				: [ new Ext.ux.IFrameComponent({
		url				: CARMEN_URL_ADMIN_CARTO+'/UploadLocalData/UploadPopup.php?path='+CARMEN_URL_PATH_DATA+'cartes/Publication/local_data/&EXT_THEME_COLOR='+this.map.app.context.mdataMap.EXT_THEME_COLOR,
		name			: 'iFrameChooseLocalFiles',
		id				: 'iFrameChooseLocalFiles',
		height			: 350
	  })]	
	});
	this.win = new Ext.Window({
	  layout 			: 'anchor',
	  width				: 800,
	  height			: 450,
	  contrain			: true,
	  plain				: true,
	  id				: 'winLocalData',
	  title				: 'Visualiser un fichier cartographique local',
	  modal				: false,
	  autoDestroy		: true,
	  resizable			: true,
	  closeAction		: 'close',
	  shadow			: false,
	  items				: [localDataPanel]
    });
	var control = this;
	this.win.on('hide', function(w) {control.btn.toggle(false);}, this.win);
	this.win.show();
  },
  
  CLASS_NAME :"Carmen.Control.AddLocalData"
});
