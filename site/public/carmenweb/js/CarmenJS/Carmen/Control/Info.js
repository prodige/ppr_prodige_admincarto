  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.Info
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.Info = new OpenLayers.Class(OpenLayers.Control, {

  // UI components
  btn: new Ext.Button({
      tooltip: 'Information',      
      tooltipType: 'qtip',
      cls: 'x-btn-icon', /* cmn-tool-info',*/
      enableToggle: true,
      toggleGroup: 'mainMapControl',
      disabled: true,
      text : '<i class="fa fa-info fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
  }),
  
  getButton : function(){return this.btn},
  
  win : null,
  headerPanel : null,
  //layerTreeInfoPanel : null,
  gridInfoPanel : null,

  //TODO temporaire
  inGroup : true,
  
  // properties 
  /**
   * Property: type
   * {OpenLayers.Control.TYPE}
   */
  type: OpenLayers.Control.TYPE_TOOL,

  toolbarTarget: 'advanced',
  
  toolbarPos: 5,
  
  btnGrpDefaultActiv : true,

  queryInProgress : false,
  
  layerTreeManager : null,

  layerTreeRoot : null,
  
  infoTreeStore : null,
  
  mapfile : null,
  
  serviceUrl : null,

  windowTitle : 'Information',
  
  showGraphicalSelection : true,
  
  // count of queries which response is still being waited
  queriesSent : 0,
  totalCount : 0,
  fullExtent : null,

  requestTimeout : 120000, //set to 120s for long requests

  WMSResponseFormats : {
  	'text/plain': new OpenLayers.Format.Text(),
  	'application/vnd.ogc.gml': new OpenLayers.Format.WMSGetFeatureInfo(),
  	'text/xml': new OpenLayers.Format.WMSGetFeatureInfo()
  	//'application/vnd.ogc.gml': new OpenLayers.Format.GML()
  },


  // methods
  initialize: function(serviceUrl, mapfile, options) {
	  OpenLayers.Control.prototype.initialize.apply(this, [options]);    
    this.mapfile = mapfile;
    this.serviceurl = serviceUrl;       
    this.btn.disable();
  },

  initializeUIComponents : function() {
	  this.win = new Ext.Window({
	    id : this.displayClass +'_' +'window',
	    layout:'anchor',
	    width:400,
	    height: 400,
      constrain : true,
	    plain: true,
	    title: '',
	    modal:false,
	    autoDestroy :true,
	    resizable:true,
	    closeAction: 'hide',
	    shadow : false,
      layout: 'accordion'
	  });
    
	  /*this.headerPanel = new Ext.Panel({
	    id: this.displayClass +'_' +'headerPanel',
	    autoScroll: true,
	    fitToFrame: true,
	    iconCls : 'cmnInfoGridZoomIcon',
	    header : true,
	    title : 'Interrogation des couches...',
	    anchor : '100% 100%'
	  }); */
	  /*
    this.layerTreeInfoPanel = new Ext.tree.TreePanel({
	    id: this.displayClass +'_' +'layerTreeInfoPanel',
	    margins: '5 0 5 5',
	    useArrows: false,
	    animate: false,
	    border: false,
	    fitToFrame: true,
	    rootVisible: false,
	    store: this.infoTreeStore,
	    viewConfig : {autoFill:true, forceFit: false}
	  });
    this.gridInfoPanel = new Ext.grid.GridPanel({
	    id: this.displayClass +'_' + 'gridInfoPanel',
	    store: new Ext.data.SimpleStore({
	      fields: [],
	      data: []
	    }),
	    columns: [],
	    title:'Résultats sélectionnés',
	    hidden: true,
	    autoScroll: true,
	    border: false,
	    //anchor : '100% 50%',
	    tools:[{
			id:'xls',
			qtip: 'Export de la sélection au format Excel (XLS)'
		},
		{
			id:'csv',
			qtip: 'Export de la sélection au format CSV'
		}]
    });
    */
/*
    this.gridInfoFormPanel = Ext.create('Ext.form.Panel', {
		title: "upload",
		id: this.displayClass +'_' + 'gridInfoFormPanel',
		hidden : true,
		fileUpload : true,
		waitTitle : "Traitement en cours..."
	});*/
  },

  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    
/*TODO REMOVE LINE*/return;

    this.infoTreeStore = Ext.create('Ext.data.TreeStore', {
      id : 'infoTreeStore',
      root: {
        expanded: true,
        text : 'Information'
      },
      model : 'cmn.layerTreeModel'
    });
    
    
    this.initializeUIComponents();

    this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));    
    
    if (this.toolbarTarget=='simple')
      this.map.app.ui.addToToolPanel(this.btn, this.toolbarTarget, this.toolbarPos);
    else
      this.map.app.ui.addToCycleButtonGroup('advanced', 'Interrogations' ,'info', this.btn, 0, this.btnGrpDefaultActiv);
    
    this.map.events.register('emptySelection', this,
    function(evt) {
      if (!this.queryInProgress) {
        this.clearWindow(); 
      }
    });     
    this.btn.enable();
  },
  
  /**
   * Method: draw
   */    
  draw: function() {
      this.handler = new OpenLayers.Handler.Box( this,
                          {done: this.info }, {keyMask: this.keyMask} );
  },

  activate: function() {
/*TODO REMOVE LINE*/return;
  	if (this.getLayerTreeManager==null) {
      Ext.MessageBox.alert("Warning", "The control " + 
       this. CLASS_NAME + 
       " requires the LayerTreeManager control to work.");
      return null;
    }
    
    OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
  },


  deactivate: function() {
/*TODO REMOVE LINE*/return;
  	// removing highlighting...
    this.map.clearGraphicalSelection();
  	this.map.clearSelection();
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();
  },


  /**
   * Method: info
   *
   * Parameters:
   * position - {<OpenLayers.Bounds>} or {<OpenLayers.Pixel>}
   */
  info : function (position) {
    this.showInfoWindow(position);
  },

  // displaying results
  showInfoWindow : function(position) {

    /*this.layerTreeRoot = this.getLayerTreeManager().cloneLayerTree(
      this.infoTreeStore.getRoot(),
      this.transformNodeInfo
    );*/
    
    this.layerTreeRoot = this.getLayerTreeManager().getLayerTree().clone(this.transformNodeInfo);

    if (!this.win.rendered) {
      //this.headerPanel.add(this.layerTreeInfoPanel);
      //this.win.add(this.headerPanel);
      for(var i = 0; i < this.layerTreeRoot.childNodes.length; i++){
    	var currentChild = this.layerTreeRoot.childNodes[i];
        if(currentChild.data.text.match(/<table.*<\/table>/)){
          currentChild.data.text = currentChild.data.text.replace(/<table.*<\/table>/, "");
        }
      }
      //this.layerTreeInfoPanel.setRootNode(this.layerTree);
      //this.win.add(this.gridInfoPanel);
      /*this.win.add(this.gridInfoFormPanel);
      this.win.setTitle(this.windowTitle);
      this.win.doLayout();
      this.win.show();*/
      /*
      this.gridInfoPanel.view.hmenu.add({
        itemId : 'stats',
        text: 'Statistiques',
        iconCls:'cmn-tool-stats'
        });*/
      } 
    else {
      //this.gridInfoPanel.hide();
      //obedel here
      /*
      // remove the old tree
      this.layerTreeInfoPanel.getRootNode().destroy();
      for(var i = 0; i < this.layerTree.childNodes.length; i++){
      	var currentChild = this.layerTree.childNodes[i];
       	if(currentChild.text.match(/<table.*<\/table>/)){
       	  currentChild.text = currentChild.text.replace(/<table.*<\/table>/, "");
       	}
      }
      // add the new one and render it
      this.layerTreeInfoPanel.setRootNode(this.layerTree);
      //this.layerTreeInfoPanel.getRootNode().render();
      */
    }

    if (!this.win.isVisible()) {
      //this.win.show();
    }
    
    // informing of server querying     
    //this.headerPanel.setTitle('Interrogation des couches...');
    if (this.showGraphicalSelection) 
      this.map.clearGraphicalSelection();
    this.queryInProgress = true;
    this.map.clearSelection();
    this.queryInProgress = false;
    this.performQueries(position);
  },

  clearWindow : function() {
	if (this.win.rendered) {
		//this.gridInfoPanel.hide();
    	//this.layerTreeInfoPanel.getRootNode().destroy();
    	//this.win.hide();
	}
  },

  getLayerTreeManager : function() {
    if (this.layertreeManager == null) {
      var controls = this.map.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length==0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },




  performQueries : function(position) {
    var control = this;
    
	  // filter the node
    this.layerTreeRoot.filter(this.filterNodeLayerQueryable);
//           
//     
    var isLayerNode = function(n) { 
    	return ('type' in n.attributes && (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER)); 
    }; 
    var nodes = this.layerTreeRoot.collectChildNodes(isLayerNode);
    
    if (nodes.length==0) {
    	//this.headerPanel.setTitle('Aucun résultat');
    	//this.gridInfoPanel.hide();
      Ext.Msg.alert("Outil d'information", "Aucune donnée n'est interrogeable avec cet outil");
    } else {
	    var ms_nodes = {};
  	  var wms_nodes = [];
    	var wms_layers = [];

	    for (var i=0; i<nodes.length; i++) {
	    	var layer = nodes[i].attributes.OlLayer;
		    if (layer instanceof Carmen.Layer.WMSGroup) { 
					wms_nodes.push(nodes[i]);
					Carmen.Util.set_add(wms_layers, layer);
		    } else {
				  var mapfile = nodes[i].attributes.OlLayer.mapfile;
				  if (mapfile) {
				   if (!ms_nodes[mapfile])
				     ms_nodes[mapfile] = [];
				   ms_nodes[mapfile].push(nodes[i]);  
				  }
   			}
	    }
	 	// cancel all pending requests
	 	Ext.Ajax.abort();
		this.queriesSent = 0;
		this.totalCount = 0;
		this.fullExtent = null;
		// launching requests
		
		this.map.app.ui.delAllInfoPanel();
		
		for (var m in ms_nodes) {
		if (ms_nodes[m].length>0) 
			this.performQuery(this.buildParams(ms_nodes[m], position, null, m));
		}
		if (position instanceof OpenLayers.Pixel) {
			for (var i=0; i<wms_layers.length; i++) { 
				this.performWMSQuery(wms_layers[i], position);
			}
		}
    }
  },
  
  registerResults : function(response, queryType) {


  
	  var control = this;
	  this.queriesSent--;
	  this.totalCount = this.totalCount + parseInt(response.totalCount);

    if(this.queriesSent == 0 && this.totalCount == 0)
      Ext.Msg.alert("Outil d'information", "Aucun résultat ne correspond à votre recherche.");
      /*if(Object.getOwnPropertyNames(results).length == 0){
      		Ext.Msg.alert("Outil d'information", "Aucun résultat ne correspond à votre recherche.");
    	}*/
      
	  // updating headerPanel
	  /*var headerPanel = Ext.getCmp(control.displayClass +'_' +'headerPanel');
	  headerPanel.setTitle(this.totalCount + ' résultats');*/
	  this.map.app.ui.infoPanel.setTitle(this.totalCount + ' résultats');
	  if (queryType==Carmen.Control.Info.SRC_MS) {
		  var currentExtent = Carmen.Util.strExtentToOlBounds(response.extent, Carmen.Control.Info.PUNCTUAL_RADIUS);
		  this.fullExtent = this.fullExtent == null ? currentExtent : this.fullExtent.extend(currentExtent); 
		  this.map.app.ui.infoPanel.bounds = this.fullExtent;
      
      // removing old listenr if defined
      /*if (control.headerClickListener)
        control.headerClickListener.destroy();
      control.headerClickListener = headerPanel.getHeader().on({
         destroyable: true,
         click: function() {
            control.map.zoomToSuitableExtent(control.fullExtent);
            // displaying graphical selection ? 
            if (control.showGraphicalSelection) {
              control.map.clearGraphicalSelection();
              for(lname in response.results)  {
                var node = control.layerTreeRoot.findChildByAttribute('layerName', lname);
                // obedel patch : no highlight mode for wfs mapfile layer 
                if (node!=null) {
                  node.attributes.OlLayer.setDisplayMode(
                      Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
                      node.attributes.layerName, null, 
                      response.queryfile);
                }
              }
            }
         }
      });*/
      
		  for(lname in response.results)  {
			  var node = control.layerTreeRoot.findChildByAttribute('layerName', lname);
			  if (node!=null) {
				  var olLayer = node.attributes.OlLayer; 
				  olLayer.setDisplayMode(
						  Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
						  node.attributes.layerName, null, 
						  response.queryfile);
          
          //obedel migration, pb registering selection...
          
				  // registering selection
				  if (olLayer.handleSelection(lname)) { 
					  this.map.addToSelectionLayers(olLayer);
					  olLayer.setSelectionStore(lname, 
							  new Ext.data.SimpleStore({
								  fields: response.results[lname].fields.concat(['extent', 'fid']),
								  data :  response.results[lname].data
							  }));
                 
				  }
			  }
		  }   
	  }
  //updating result tree      
  //control.attachResultsToTree(control.layerTreeRoot, response.results, queryType);

  control.buildResultPanels(control.layerTreeRoot, response.results, queryType, response.queryfile);
  
  /*
  if (this.queriesSent==0)
    control.layerTreeRoot.filter(control.filterNodeLayerWithResults);
 	*/
  },
  

  // traditionnal query on mapserver layer
  performQuery : function (queryParams) {
    
    // prepraring answer callback
    var received = function (response) {
      response = Ext.decode(response.responseText);
	  	this.registerResults(response, Carmen.Control.Info.SRC_MS); 	
    }
    
    // launching the request
    this.queriesSent++;
    
    var url = '/services/GetInformation/index.php';
    //console.log(url);
    //console.log(queryParams);
    Ext.Ajax.request({
	  
      url: url,
      success: received,
      failure: function (response) { 
         this.queriesSent--;
         Carmen.Util.handleAjaxFailure(response, this.windowTitle, true); 
      },
      headers: {
        'my-header': 'foo'
      },
      scope : this,
      params: queryParams
    });
  }, 


  buildParams : function(nodes, position, fieldsFilter, mapfile) {
    
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];
    
    for (var i=0; i<nodes.length; i++) {
		layerNames.push(nodes[i].attributes.layerName);
	    fieldsList.push(nodes[i].attributes.infoFields);
	    briefFieldsList.push(nodes[i].attributes.briefFields);
	    baseQueryURLList.push(nodes[i].attributes.baseQueryURL);
    }
    
    var fieldsDesc = new Array();
    for (var i=0; i<fieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
      fieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        fieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
        fieldsDesc[i] = Carmen.Util.array_filter(fieldsDesc[i], fieldsFilter);
    }

    var briefFieldsDesc = new Array();
    for (var i=0; i<briefFieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(briefFieldsList[i],';');
      briefFieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        briefFieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
		briefFieldsDesc[i] = Carmen.Util.array_filter(briefFieldsDesc[i], fieldsFilter);
    }

    // linking kept info fields and brief fields 
    // to the corresponding layer node in the tree
    for (var i=0; i<nodes.length; i++) {
   	  nodes[i].attributes.infoFieldsDesc = fieldsDesc[i];
   	  nodes[i].attributes.briefFieldsDesc = briefFieldsDesc[i];
    }

    var params = {
      layers : Ext.util.JSON.encode(layerNames),
      fields : Ext.util.JSON.encode(fieldsDesc),
      briefFields : Ext.util.JSON.encode(briefFieldsDesc),
      shape : Carmen.Util.positionToStr(this.positionToBounds(position)),
      map : mapfile,
      showGraphicalSelection : this.showGraphicalSelection }
    return params;    
  },

  logvar : function(value, name, tab) {
  	var logv = (value==null) ? "null" : value;
  	tab[name] = logv;
  },
  
  printlog : function(tab) {

	Ext.Ajax.request({
      url: '/services/GetInformation/log.php',
      method: 'POST',
  	  success : function (response) { 
      	 Ext.Msg.alert(control.windowTitle, response.msg ? response.msg : "Commande log enregistrée");
      },
  	  failure: function (response) { 
      	 Ext.Msg.alert(control.windowTitle, 'Problème dans l\'écriture du fichier Log');
      },
      params : { 
        "log_array" : Ext.encode(tab)
      } 
    });
  
  },
	
  performWMSQuery : function(layer, position) {
    var control = this;
   	var received = function(response) {
		 
      //var logs = {}; //obedel log
      var features = [];
        //control.logvar(response, 'response',logs); //obedel log
      var contentStr = response.getResponseHeader("Content-Type")
            || response.getResponseHeader("Content-type");
      
      //control.logvar(contentStr, 'contentStr',logs); //obedel log
      if (contentStr!=null) { 
          contentStr = (contentStr.split(";")[0]).trim();
          //control.logvar(contentStr, 'contentStrSplit',logs); //obedel log
          var fmt = control.WMSResponseFormats[contentStr];
          //control.logvar(fmt, 'fmt',logs); //obedel log
          if (fmt) { 
             var rep = response.responseXml || response.responseText;
             //control.logvar(rep, 'rep',logs); //obedel log
             //console.log(Url.decode(rep));
             //console.log(Url._utf8_encode(rep));
             //console.log(Url._utf8_decode(rep));
             //console.log(response.responseXml || response.responseText);
             features = fmt.read(response.responseXml || response.responseText);
             //control.logvar(features, 'features',logs);
             //console.log(features);
           }
        }
      //control.printlog(logs);//obedel log	

      var results = {};
      var extentStr = Carmen.Util.positionToStr(control.map.getExtent());
      for (var i=0; i<features.length; i++) {
    	 var layerName = layer.name;//features[i].type;
    	 /*  for(var j=0; j<control.layerTree.childNodes.length; j++){
    		  if(features[i].type == control.layerTree.childNodes[j].attributes.OlLayer.params.LAYERS){
    			  layerName = control.layerTree.childNodes[j].attributes.layerName;
    			  break;
    		  }
    	  }*/
        if (!(features[i].type in results)) {
          results[layerName] = {
            "fields" : [],
            "briefFields" : [],
            "data" : [],
            "briefData" : []
          }
          for (var f in features[i].attributes) {
            results[layerName].fields.push(f);				
            results[layerName].briefFields.push(f);
          }
        }
        var featureData = [];
        for (var f in features[i].data) 
          featureData.push(features[i].data[f]);	
        results[layerName].briefData.push(featureData);
        results[layerName].data.push(featureData);
      }

      var responseJS = {
        "totalCount" : features.length,
        "results" : results
      };
      control.registerResults(responseJS, Carmen.Control.Info.SRC_WMS);
    };
    
    
    this.queriesSent++;
    Ext.Ajax.request({
      url: '/services/GetInformation/proxy.php',
      method: 'GET',
  	  success : function (response) {
      	 received(response);
      },
  	  failure: function (response) {
      	 // uncomment to display wms querying errors
      	 //var msg = Ext.valueFrom(response.responseText, "Service WMS indisponible");
      	 //Ext.Msg.alert(control.windowTitle, msg);
      	 control.registerResults(
      	   Carmen.Control.Info.noResultsResponse,
      	   Carmen.Control.Info.SRC_WMS);
      },
      headers: {
        'my-header': 'foo'
      },
      params : { 
        "proxy_url" : layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()),
        "force_utf8_output" : 1 // needed because ExtJS waits for UTF8 encoded response...
      } 
    });
    //console.log(layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()));
  }, 

  buildResultPanels : function(tree, results, queryType, queryfile) {
	  
	  	//this.map.app.ui.delAllInfoPanel();

		// obedel migration
		for(lname in results)  {
		 	var node = tree.findChildByAttribute('layerName', lname);
      
		 	if (node==null)
		 		node = tree.findChildByAttribute('text', lname);
		 	if (node==null)
		 		node = tree.findChildByAttribute('layerAlias', lname);
			
      if (node!=null) {
			  
			  //var panelTitle = /*node.text +*/ '  (' + results[lname].data.length + ' résultat'+(results[lname].data.length > 1 ? 's' : '')+')'
        var panelTitle = node.data.text;
			  node.attributes.layerAlias= node.text!="" ? node.text : node.attributes.layerAlias;
			  node.text = node.data.text + '  (' + results[lname].data.length + ' résultat'+(results[lname].data.length > 1 ? 's' : '')+')';
			  node.attributes.infoData = results[lname];
			  node.attributes.queryType = queryType;
			  node.attributes.queryfile = results[lname].queryfile;
			  node.attributes.extent = results[lname].extent;
			  
        			   
			  // add click listener to the node  	 	   
			  var control = this;

			  var panel_id = this.displayClass +'_' + 'gridInfoPanel_' +  Ext.id();
        var gPanel =  Ext.create( 'Ext.grid.GridPanel', {
          id: panel_id,
          store: new Ext.data.SimpleStore({
            fields: [],
            data: []
          }),
          columns: [],
          title: panelTitle,
          hidden: false,
          autoScroll: true,
          border: true,
          stripeRows: true,
          bounds: null,
          viewConfig: {
            forceFit:true
        },
          tools:[{
            id: panel_id + '_zoom',
            qtip: 'Zoom sur les résultats',
            renderTpl: [
              '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-search fa-1x"' +
              '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
            ],
            handler: function(){
              // show help here
				var bounds = this.toolOwner.bounds;
				control.map.zoomToSuitableExtent(bounds);
			}
          },{
            id: panel_id + '_xls',
            qtip: 'Export de la sélection au format Excel (XLS)',
            renderTpl: [
              '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-excel-o fa-1x"' +
              '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
            ]
          },
          {
            id: panel_id + '_csv',
            qtip: 'Export de la sélection au format CSV',
            renderTpl: [
              '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-file-text-o fa-1x"' +
              '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
            ]
          }],
          listeners : {
            afterrender:function(){
              var menu = this.headerCt.getMenu();
              menu.addCls('menuBackgroundWhite');
              menu.addListener('afterrender',
                function(menu){
                  var subitems = menu.items.items;
                  for(var i = 0; i < subitems.length; i++){
                    if(typeof subitems[i].menu !== "undefined"){ 
                     subitems[i].menu.addCls('menuBackgroundWhite');
                    }
                  }
                }
              );
            },
			rowclick:function(grid, record, tr, rowIndex, e, eOpt) {

			  var rec = grid.store.getAt(rowIndex);
				//var extentStr = rec.get('extent');
				//var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
				//control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
				var fid = rec.get('fid');
				var layerName = rec.get('layerName');
				// retrieving the openLayer layer linked with the selected row
				var n = control.layerTreeRoot.findChildByAttribute('layerName', layerName);
				var olLayer = n.attributes.OlLayer;                  
				// changing symbology to highlight selection
			  olLayer.setDisplayMode(
			  Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
			  layerName, fid);
			},
			cellclick:function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				if (cellIndex==1) {
					var rec = grid.getStore().getAt(rowIndex); 
					var extentStr = rec.get('extent');
					var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
					control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
				 }
				if (cellIndex==2) {
					var rec = grid.getStore().getAt(rowIndex); 
					var columns = grid.ownerCt.columnManager.columns;
					control.buildInfoWindow(rec, columns, rowIndex);					
					
				 }
			},
			selectionchange: function( grid, selected, eOpts ){
				if (selected.length == 0){
					if (this.node.attributes.queryType==Carmen.Control.Info.SRC_MS) {
						var minScale = this.node.attributes.minScale;
						var maxScale = this.node.attributes.maxScale;
						control.map.clearGraphicalSelection();
						if (control.showGraphicalSelection) {
						  this.node.attributes.OlLayer.setDisplayMode(
							Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
							this.node.attributes.layerName, null, 
							queryfile);
						}
					} 
					else if (this.node.attributes.queryType==Carmen.Control.Info.SRC_WMS) {
						var infoFieldsDesc = [];
						for (var i=0; i<this.node.attributes.infoData.fields.length; i++) {
							var fld = this.node.attributes.infoData.fields[i];
							infoFieldsDesc.push({
									name : fld,
									alias : fld,
									type : 'TXT'
								});
						}
						this.node.attributes.infoFieldsDesc = infoFieldsDesc; 			    		
					}
				}
			}			
          },
        scope : this
        });

        

        this.map.app.ui.addToInfoPanel(gPanel);
        
        //control.headerPanel.add(gPanel);
			  //gPanel.addListener('click', 
			  //  function() {
        if (node.attributes.queryType==Carmen.Control.Info.SRC_MS) {
          // zoom on the layer selection extent
          //var extentStr = node.attributes.infoData.extent;
          var minScale = node.attributes.minScale;
          var maxScale = node.attributes.maxScale;
          //console.log(node.attributes.infoData); 
          //var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
          
          //obedle migration here
          // obedel patch : no highlight mode for wfs mapfile layer
          //control.map.clearGraphicalSelection();
          if (control.showGraphicalSelection) {
              /*node.attributes.OlLayer.setDisplayMode(
            Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
            node.attributes.layerName, null, 
            node.attributes.queryfile);*/
            }
            //control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
        }else if (node.attributes.queryType==Carmen.Control.Info.SRC_WMS) {
            // building default fieldsDesc from fields name
            var infoFieldsDesc = [];
            for (var i=0; i<node.attributes.infoData.fields.length; i++) {
              var fld = node.attributes.infoData.fields[i];
              infoFieldsDesc.push({
                  name : fld,
                  alias : fld,
                  type : 'TXT'
                });
            }
            node.attributes.infoFieldsDesc = infoFieldsDesc; 			    		
        }
          // configuring and showing the result table panel
						var gConfig = control.buildGridConfig(
						  node.attributes.infoFieldsDesc, 
						  node.attributes.infoData, 
						  node.attributes.layerName,
						  node.attributes.olLayer,
						  node.attributes.queryType
						);
						/*gPanel.reconfigure(gConfig.store, gConfig.columns);
						//gPanel.purgeListeners();
						gPanel.setTitle(node.text);
						gPanel.node = node;*/
						
            // configuring xls export...
           var xlsTool = gPanel.getHeader().getComponent(panel_id + '_xls');
						xlsTool.on('click',
						  function() {
							var url = '/services/GetXLS/index.php';
							var f = this.control.gridInfoFormPanel; 
							queryParams = { 
							  field : Ext.encode(this.node.attributes.infoFieldsDesc),
							  data : Ext.encode(this.node.attributes.infoData.data),
							  selectionName : this.node.attributes.layerAlias,
							  output : "XLS"
							};
							Carmen.Util.doFormUpload(f, url, queryParams);
						  }, 
						  { control : control, node : node }
						);
						
						var csvTool = gPanel.getHeader().getComponent(panel_id + '_csv');
						csvTool.on('click',
						  function() {
							var url = '/services/GetXLS/index.php';
							var f = this.control.gridInfoFormPanel; 
							queryParams = { 
							  field : Ext.encode(this.node.attributes.infoFieldsDesc),
							  data : Ext.encode(this.node.attributes.infoData.data),
							  selectionName : this.node.attributes.layerAlias,
							  output : "CSV"
							};
							Carmen.Util.doFormUpload(f, url, queryParams);
						  }, 
						  { control : control, node : node }
						);
						
						gPanel.reconfigure(gConfig.store, gConfig.columns);
						if(node.attributes.extent !== undefined){
						  var bounds = Carmen.Util.strExtentToOlBounds(node.attributes.extent, Carmen.Control.Info.PUNCTUAL_RADIUS);
						  gPanel.bounds = bounds;
						}
						gPanel.setTitle(node.text);
						gPanel.node = node;
						gPanel.queryfile = queryfile;
						/*
						// adding zooming and highlighting to row click
						if (node.attributes.queryType==Carmen.Control.Info.SRC_MS) {
							gPanel.on('rowclick', 
								function(grid, record, tr, rowIndex, e, eOpts ) {
                  var rec = grid.store.getAt(rowIndex);
									//var extentStr = rec.get('extent');
									//var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
									//control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
									var fid = rec.get('fid');
									var layerName = rec.get('layerName');
									// retrieving the openLayer layer linked with the selected row
									var n = control.layerTreeRoot.findChildByAttribute('layerName', layerName);
 									var olLayer = n.attributes.OlLayer;                  
									// changing symbology to highlight selection
							    olLayer.setDisplayMode(
							      Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
							      layerName, fid);
                });
							gPanel.on('cellclick', 
							   function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
									 if (cellIndex==1) {
										var rec = grid.getStore().getAt(rowIndex); 
										var extentStr = rec.get('extent');
										var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
										control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
									 }
								});
						//}*/
						
						

						/*
						// configuring stats menu...
						var statsBtn = gPanel.view.hmenu.items.get('stats');
						statsBtn.purgeListeners()
						statsBtn.on('click',
							function() {
								var layerName = this.node.attributes.layerName;
								var layerAlias = this.node.attributes.layerAlias;
								var colIndex = this.gridPanel.view.hdCtxIndex;
								var cm = this.gridPanel.getColumnModel();
								var colName = cm.getDataIndex(colIndex);
								var realColName = colName.replace(/_[0-9]+$/,'');
								var colAlias = cm.getColumnHeader(colIndex);
							
								var store = this.gridPanel.getStore();
								
								// collect is a function used to get 
								// column values in a array
								var collect = function(str, fieldname) {
									var res = [];
									var browse = function (r) {
										this.res.push(r.get(this.fieldName));
										return true;
									}
									str.each(browse, 
										{ res : res, fieldName : fieldname });
									return res;
								}
								var data = collect(store, colName);
								var stats = Carmen.Util.stats(data);
								control.queryStatsOnColumn(stats, layerAlias, colAlias);	
							},
							{ node : node, gridPanel : gPanel, control : control }
						);
            */ 
						// hack needed to force layout

						if (!gPanel.isVisible()) {
						  gPanel.show();
						 //control.win.setSize(control.win.getSize().width, control.win.getSize().height+1);
						}  

		    //}, node);
        
        
      // obedel migration
      // not needed anymore
      /*  
			// adding result nodes under the layer node  	 	   
			for (var i=0; i<data.length; i++) {
			  var child = this.buildResNode(results[lname].briefFields, results[lname].briefData[i], i, node.attributes.queryType, node.attributes.briefFieldsDesc);
			  node.appendChild(child);
			}
			// but do not show them
			if (node.rendered)
		 		node.collapse();
		 		
		 	nodesToUpdate.push(node);
      */ 
		 }
		}
		//obedel migration
    /*
		// force rendering of all nodes to add zoom icon control 
		tree.expand(true);
		// add zoom icon control
		for(var i=0; i< nodesToUpdate.length; i++)  {
		 	var node = nodesToUpdate[i];
			if (node!=null) {
				var iconEl = Ext.fly(node.ui.getIconEl());
				iconEl.addListener('click',
					function() {
						if (this.attributes.queryType==Carmen.Control.Info.SRC_MS) {
							// zoom on the layer selection extent
							var extentStr = this.attributes.infoData.extent;
							var minScale = this.attributes.minScale;
							var maxScale = this.attributes.maxScale;
							//console.log(this.attributes.infoData); 
							var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
							if (control.showGraphicalSelection) {
								control.map.clearGraphicalSelection();
								this.attributes.OlLayer.setDisplayMode(
									Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
									this.attributes.layerName, null, 
									this.attributes.queryfile);
							}
							control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
						}
					}, node);   	  
			}
		}
		// collapse back the tree
		tree.collapse(true);
    */
     if (!Ext.getCmp(this.displayClass +'_' + 'gridInfoFormPanel')) 
			//Ext.getCmp(this.displayClass +'_' + 'gridInfoFormPanel').destroy();
        
        this.gridInfoFormPanel = Ext.create('Ext.form.Panel', {
			title: "upload",
			id: this.displayClass +'_' + 'gridInfoFormPanel',
			hidden : true,
			fileUpload : true,
			waitTitle : "Traitement en cours..."
		});
	  this.map.app.ui.infoPanel.add(this.gridInfoFormPanel);
	},

  attachResultsToTree : function(tree, results, queryType) {
		var nodesToUpdate = [];
		for(lname in results)  {
		 	var node = tree.findChildByAttribute('layerName', lname);
		 	if (node==null)
		 		node = tree.findChildByAttribute('text', lname);
			if (node!=null) {
			  var data = results[lname].data;
			  node.attributes.layerAlias= node.text!="" ? node.text : node.attributes.layerAlias;
			  node.text = node.data.text + '  (' + results[lname].data.length + ' résultat'+(results[lname].data.length > 1 ? 's' : '')+')';
			  node.attributes.infoData = results[lname];
			  node.attributes.queryType = queryType;
			  node.attributes.queryfile = results[lname].queryfile;
			  			   
			  // add click listener to the node  	 	   
			  var control = this;
			  var gPanel = this.gridInfoPanel;
	   
			  node.addListener('click', 
			    function() {
					
			    	if (this.attributes.queryType==Carmen.Control.Info.SRC_MS) {
						// zoom on the layer selection extent
						//var extentStr = this.attributes.infoData.extent;
						var minScale = this.attributes.minScale;
						var maxScale = this.attributes.maxScale;
						//console.log(this.attributes.infoData); 
						//var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
						// obedel patch : no highlight mode for wfs mapfile layer
						control.map.clearGraphicalSelection();
						if (control.showGraphicalSelection) {
  						  this.attributes.OlLayer.setDisplayMode(
							Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
							this.attributes.layerName, null, 
							this.attributes.queryfile);
						  }
						  //control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
			    	} 
			    	else if (this.attributes.queryType==Carmen.Control.Info.SRC_WMS) {
			    		// building default fieldsDesc from fields name
			    		var infoFieldsDesc = [];
			    		for (var i=0; i<this.attributes.infoData.fields.length; i++) {
			    			var fld = this.attributes.infoData.fields[i];
			    			infoFieldsDesc.push({
				    				name : fld,
				    				alias : fld,
				    				type : 'TXT'
			    				});
			    		}
			    		this.attributes.infoFieldsDesc = infoFieldsDesc; 			    		
			    	}
						// configuring and showing the result table panel
						var gConfig = control.buildGridConfig(
						  this.attributes.infoFieldsDesc, 
						  this.attributes.infoData, 
						  this.attributes.layerName,
						  this.attributes.olLayer,
						  this.attributes.queryType
						);
						
						// configuring xls export...
						/*var xlsTool = gPanel.getTool('xls');
						xlsTool.removeAllListeners();
						xlsTool.on('click',
						  function() {
							var url = '/services/GetXLS/index.php';
							var f = this.control.gridInfoFormPanel.getForm(); 
							queryParams = { 
							  field : Ext.encode(this.node.attributes.infoFieldsDesc),
							  data : Ext.encode(this.node.attributes.infoData.data),
							  selectionName : this.node.attributes.layerAlias
							};
							Carmen.Util.doFormUpload(f, url, queryParams);
						  }, 
						  { control : control, node : this }
						);
						
						var csvTool = gPanel.getTool('csv');
						csvTool.removeAllListeners();
						csvTool.on('click',
						  function() {
							var url = '/services/GetXLS/index.php';
							var f = this.control.gridInfoFormPanel.getForm(); 
							queryParams = { 
							  field : Ext.encode(this.node.attributes.infoFieldsDesc),
							  data : Ext.encode(this.node.attributes.infoData.data),
							  selectionName : this.node.attributes.layerAlias,
							  output : "CSV"
							};
							Carmen.Util.doFormUpload(f, url, queryParams);
						  }, 
						  { control : control, node : this }
						);*/
							
						gPanel.reconfigure(gConfig.store, gConfig.columns)
						gPanel.purgeListeners();
						gPanel.node = node;
						// adding zooming and highlighting to row click
						if (this.attributes.queryType==Carmen.Control.Info.SRC_MS) {
							gPanel.on('rowclick', 
								function(grid, rowIndex, e){
									var rec = grid.store.getAt(rowIndex);
									//var extentStr = rec.get('extent');
									//var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
									//control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
									var fid = rec.get('fid');
									var layerName = rec.get('layerName');
									// retrieving the openLayer layer linked with the selected row
									var n = control.layerTreeRoot.findChildByAttribute('layerName', layerName);
 									var olLayer = n.attributes.OlLayer;                  
									// changing symbology to highlight selection
							    olLayer.setDisplayMode(
							      Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
							      layerName, fid);

								});
							gPanel.on('cellclick', 
							   function(grid, rowIndex, columnIndex, e) {
									 if (columnIndex==1) {
										var rec = grid.getStore().getAt(rowIndex); 
										var extentStr = rec.get('extent');
										var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
										control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
									 }
								});
						}
						
						// configuring stats menu...
						var statsBtn = gPanel.view.hmenu.items.get('stats');
						statsBtn.purgeListeners()
						statsBtn.on('click',
							function() {
								var layerName = this.node.attributes.layerName;
								var layerAlias = this.node.attributes.layerAlias;
								var colIndex = this.gridPanel.view.hdCtxIndex;
								var cm = this.gridPanel.getColumnModel();
								var colName = cm.getDataIndex(colIndex);
								var realColName = colName.replace(/_[0-9]+$/,'');
								var colAlias = cm.getColumnHeader(colIndex);
							
								var store = this.gridPanel.getStore();
								
								// collect is a function used to get 
								// column values in a array
								var collect = function(str, fieldname) {
									var res = [];
									var browse = function (r) {
										this.res.push(r.get(this.fieldName));
										return true;
									}
									str.each(browse, 
										{ res : res, fieldName : fieldname });
									return res;
								}
								var data = collect(store, colName);
								var stats = Carmen.Util.stats(data);
								control.queryStatsOnColumn(stats, layerAlias, colAlias);	
							},
							{ node : this, gridPanel : gPanel, control : control }
						);
						// hack needed to force layout
						if (!gPanel.isVisible()) {
						  gPanel.show();
						  control.win.setSize(control.win.getSize().width, control.win.getSize().height+1);
						}  
		    }, node);
			// adding result nodes under the layer node  	 	   
			for (var i=0; i<data.length; i++) {
			  var child = this.buildResNode(results[lname].briefFields, results[lname].briefData[i], i, node.attributes.queryType, node.attributes.briefFieldsDesc);
			  //node.appendChild(child);
			}
			// but do not show them
			if (node.rendered)
		 		node.collapse();
		 		
		 	nodesToUpdate.push(node);
		 }
		}
		
		// force rendering of all nodes to add zoom icon control 
		tree.expand(true);
		// add zoom icon control
		for(var i=0; i< nodesToUpdate.length; i++)  {
		 	var node = nodesToUpdate[i];
			if (node!=null) {
				var iconEl = Ext.fly(node.ui.getIconEl());
				iconEl.addListener('click',
					function() {
						if (this.attributes.queryType==Carmen.Control.Info.SRC_MS) {
							// zoom on the layer selection extent
							var extentStr = this.attributes.infoData.extent;
							var minScale = this.attributes.minScale;
							var maxScale = this.attributes.maxScale;
							//console.log(this.attributes.infoData); 
							var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
							if (control.showGraphicalSelection) {
								control.map.clearGraphicalSelection();
								this.attributes.OlLayer.setDisplayMode(
									Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
									this.attributes.layerName, null, 
									this.attributes.queryfile);
							}
							control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
						}
					}, node);  
			}
		}
		// collapse back the tree
		tree.collapse(true);
	},

  buildResNode : function(fields, data, nodeIndex, queryType, briefFieldDesc) {
	// building desc line
	var html_title = "";
	var fieldDesc = briefFieldDesc[0];
	var f_render = Carmen.Util.carmenTypeToExtRenderer(fieldDesc.type);
	var first = data.length>2 ? data[0] : data[data.length-1];
	html_title += (f_render== '') ? '<span>'  + first  + '</span>': f_render(first);
	if (briefFieldDesc.length>1) {
		html_title += '<span>(</span>'  ;
		for (var i=1; i<briefFieldDesc.length; i++) {
			fieldDesc = briefFieldDesc[i];
			f_render = Carmen.Util.carmenTypeToExtRenderer(fieldDesc.type);
			html_title += '<span>' + fieldDesc.alias + ' :</span>'  ;
			html_title += (f_render== '') ? '<span>' + data[i] + '</span>' : f_render(data[i]);
			html_title += '<span>,</span>'  ;
		}
		html_title = briefFieldDesc.length<=1 ? 
			html_title :
			html_title.substring(0, html_title.length - '<span>,</span>'.length);
		html_title += '<span>)</span>'  ;
	}
	var control = this;   

  },           

 
  extentRenderer : function(v) {
  	return '<i class="fa fa-fw fa-search fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },
  
  infoRenderer : function(v) {
  	return '<i class="fa fa-fw fa-info-circle fa-1x"' +
              '" style="color: #000; font-size: 16px;"></i>';
  },


  buildGridConfig: function (fieldsDesc, result, layerName, olLayer, queryType) {
    var fields = new Array();
    var columns = new Array();
    columns.push(new Ext.grid.RowNumberer());
    if (queryType==Carmen.Control.Info.SRC_MS) {
	    columns.push({
	      //id : 'extent',
	      header : '',
	      width : 40,
	      resizable: true,
	      renderer: this.extentRenderer,
	      dataIndex: 'extent'
	    });
	    columns.push({
	      //id : 'info',
	      header : '',
	      width : 40,
	      resizable: true,
	      renderer: this.infoRenderer,
	      dataIndex: 'info'
	    });
		}    
    for (var i=0; i<fieldsDesc.length; i++) {
    	var desc = fieldsDesc[i];
    	var fDesc = {name : desc.name + '_' + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
    	switch (desc.type) {
        case 'date' : 
    	    fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
    	    break;
    	  default:
    	    break;
    	}
    	fields.push(fDesc);
    	
    	var colDesc = {
    		id: Ext.id(), //desc.name + '_' + layerName + '_' + i.toString()),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name + '_' + i.toString()
    	};
    	columns.push(colDesc);
    }
		if (queryType==Carmen.Control.Info.SRC_MS) {
	    fields.push({name : 'extent'});
	    fields.push({name : 'fid'});
		}
    fields.push({name : 'layerName'});
  
    for (var i=0; i<result.data.length;i++) {
    	result.data[i] = result.data[i].concat([layerName]);
    }  

    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),
	    /*columnModel : new Ext.grid.ColumnModel({
	      columns: columns 
	    })*/
      columns : columns
	  };
	  return config;
  },

    queryStatsOnColumn : function(stats, layerAlias, colAlias) {
	var labelMapping = {
		"count" : "Effectif",
		"min" : "Minimum",
		"max" : "Maximum",
		"sum" : "Somme",
		"variance" : "Variance",
		"average" : "Moyenne",
		"stddeviation" : "Ecart type"
	};
	var statWin = new Ext.Window({
		id : this.displayClass +'_' +'StatsWindow' + Ext.id(),
		layout:'anchor',
		width:340,
		height: 232,
    constrain : true,
		plain: true,
		title: 'Statistiques pour le champ ' + colAlias + ' de la couche ' + layerAlias,
		modal:false,
		autoDestroy :true,
		resizable:false,
		autoScroll: true,
		closeAction: 'hide',
		shadow : false
	  });
	  var infoPanel = new Ext.form.FormPanel({
		id: this.displayClass +'_' +'StatsInfoPanel' + Ext.id(),
		autoScroll: true,
		fitToFrame: true,
		defaults : {
			labelStyle: 'padding:5px;',
			style: 'font-weight:bold; padding:5px;'
		}
		//labelWidth: 80,
		//labelPad: 3
	  });
	  for (var field in stats) {
		if (labelMapping[field]) {
			var infoField = new Ext.form.DisplayField({
				fieldLabel : labelMapping[field],
				value : stats[field]
			});
			infoPanel.add(infoField);
		}
	  }
	statWin.add(infoPanel);
	statWin.show();
  },


  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
  	var res = true;
    //console.log(node.attributes.text);
  	res = (node.attributes!=null) && 
      ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       && ((node.attributes.infoFields && node.attributes.infoFields.length>0) ||
       	   (node.attributes.OlLayer instanceof Carmen.Layer.WMSGroup)) 
      )); 	
    return res;
  },

  filterNodeLayerWithResults : function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.infoData!=null) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_FEATURE));
    return res;
  },

  transformNodeInfo : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
/*
  	if (cloneAtts.text!=Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL)
  	   cloneAtts.uiProvider = Carmen.TreeNodeInfoUI;
  	else
  	   cloneAtts.uiProvider = Carmen.RootTreeNodeInfoUI;  	   
*/
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expandable = true;
  	if (cloneAtts.type == Carmen.Control.LayerTreeManager.NODE_LAYER) {
  		cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
  		cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
  	}
    cloneAtts.text = atts.layerAlias!=null ? atts.layerAlias : atts.text;
  	return cloneAtts;
  },
  
  fieldFilter_URL : function(field) {
    return field.type=='URL';
  },
  
  fieldFilter_TXT : function(field) {
    return field.type=='TXT';
  },
  
  positionToBounds : function(position) {
		var bounds = null;
  	if (position instanceof OpenLayers.Bounds) {
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.left, position.bottom));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.right, position.top));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    } 
    else { // it's a pixel
      // if no tolerance
      // this.showInfoWindow(this.map.getLonLatFromPixel(position));
      // with tolerance
      var tol = Carmen.Util.INFO_TOOL_XY_TOLERANCE;
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x-tol, position.y-tol));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x+tol, position.y+tol));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    }
	return bounds;
  },
  
  buildInfoWindow : function(rec, columns, rowIndex){
	  
	    var data = [];
		for(var i=3;i<columns.length;i++){
		  var value = {};
		  value['key'] = columns[i].text;
		  value['value'] = columns[i].renderer.apply(null,[rec.get(columns[i].dataIndex)]);
		  data.push(value);
		}
		
		Ext.create('Ext.data.Store', {
			storeId:'infoStore',
			fields:['key', 'value'],
			data:{'items':
				data
			},
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					rootProperty: 'items'
				}
			}
		});

		var gridInfoPanel = Ext.create('Ext.grid.Panel', {
			store: Ext.data.StoreManager.lookup('infoStore'),
			columns: [
				{ text: 'Libellé',  dataIndex: 'key', flex: 1},
				{ text: 'Donnée', dataIndex: 'value', flex: 1}
			],
			height: 200,
			width: 400
		});
		this.win.setTitle(this.windowTitle);
		this.win.removeAll();
		this.win.add(gridInfoPanel);
		this.win.show();
  },
  

  
  
  CLASS_NAME: "Carmen.Control.Info"

});


// Constant used to distinguish request type
Carmen.Control.Info.SRC_MS = 0;
Carmen.Control.Info.SRC_WMS = 1;

Carmen.Control.Info.PUNCTUAL_RADIUS = 100.0;

Carmen.Control.Info.noResultsResponse = {
 "totalCount" : 0,
	"results" : []
};
  

