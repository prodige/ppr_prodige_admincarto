 
/**
 * Class: Carmen.Control.ZoomToPlace
 */

Carmen.Control.ZoomToPlaceFromDB = new OpenLayers.Class(OpenLayers.Control, {

  extentArea: "",

  zoomToPlacePanel : Ext.create('Ext.form.Panel', {
    id: 'zoomPlaceFromDBPanel',
    collapsible: true,
    title: 'Zones prédéfinies',
    itemCls: 'areasClass',
    width: '100%',
    buttons: [{
      id: 'btn_zoomToPlaceFromDB',
      text: 'Localiser'
    }],
    tabConfig: {
      title: 'Zones prédéfinies',
      tooltip: {
          text: 'Localisation par zones prédéfinies'
      },
      toFrontOnShow : false
    },
    border: true,
    toFrontOnShow : false
  }),

  combos : [],


  setMap: function(map) {
    OpenLayers.Control.prototype.setMap.apply(this, arguments);
    /* 
    for (var i=0; i<this.configs.length; i++) {
    	// building combo
    	var combo = Carmen.Control.ZoomToPlace.buildCombo(this.configs[i], i);
    	combo['config'] = this.configs[i];
      // adding listeners
      combo.addListener('change', function(cmb, newVal, oldVal) {});
      // select listeners depends if the combo is linked or not       
      if (this.configs[i].linkedCombo) {
        combo.addListener('select', 
          function(cmb, record, index) {
            this.extentArea = record.data.extent;
          }, 
          this);
        // add listener to to parent combo  
        this.combos[this.configs[i].linkedCombo-1].addListener('select', 
          Carmen.Control.ZoomToPlace.buildListenerLink(combo), 
          this);
          
      } else {
      	combo.addListener('select', 
          function(cmb, record, index) {
            this.extentArea = record.data.extent;
          }, 
          this);
      }

    	this.combos.push(combo);
      this.zoomToPlacePanel.add(combo);
    }

    var btn = this.zoomToPlacePanel.buttons[0];
    btn.addListener('click', 
      function () {
        var tabExtent = this.extentArea.split(",");
        this.map.zoomToExtent(
          new OpenLayers.Bounds(tabExtent[0],
            tabExtent[1],
            tabExtent[2],
            tabExtent[3]
          ));
      },
      this);
  
    var navPanel = this.map.app.ui.getNavigationPanel();
    navPanel.add(this.zoomToPlacePanel);
    this.map.app.ui.doLayout();

    // hack to resize combo length... cos couldn't find 
    // the way to force with iitial config
    for (var i=0; i<this.combos.length; i++) {
    	this.combos[i].getEl().dom.size=18;
    }
    */
    this.map.app.ui.addToLocatorPanel(this.zoomToPlacePanel, 0, true);
    this.buildPanel(this.config);
    Ext.getCmp("btn_zoomToPlaceFromDB").setHandler(
        function(event, toolEl, panelHeader) {
           if(this.extentArea==""){
             Ext.MessageBox.alert("Localiser", "Merci de sélectionner une zone prédéfinie");
           }else{
             
             var tabExtent = this.extentArea.split(",");
             this.map.zoomToExtent(
               new OpenLayers.Bounds(tabExtent[0],
                tabExtent[1],
                tabExtent[2],
                tabExtent[3]
              )
            );
          }
        },
        this
      );
    
    /*this.zoomToPlacePanel.buttons[0].setHandler
    (
        function(event, toolEl, panelHeader) {
           var tabExtent = this.extentArea.split(",");
          this.map.zoomToExtent(
          new OpenLayers.Bounds(tabExtent[0],
            tabExtent[1],
            tabExtent[2],
            tabExtent[3]
          ));
        },
        this
      )*/
    
  },
  
  
  
    
  
  
  buildPanel : function(config) {
    // building Ajax Request to get combo config    
    var params = OpenLayers.Util.extend(
      { queryMode: 'getFields' },
      config.providerBaseParams
      );
    Ext.Ajax.request({
      url: config.providerUrl,
      success: this.buildCombos,
      failure: function (response) { 
         Carmen.Util.handleAjaxFailure(response, control.windowTitle, true); 
      },
      params : params,
      scope : this
    });
    
  },
  
  buildCombos : function(response) {
    comboConfigs = Ext.decode(response.responseText);
	  
	  for (var i=0; i<comboConfigs.length; i++) {
	    var combo = this.buildCombo(comboConfigs[i], this.config);
	    this.combos.push(combo);
      this.zoomToPlacePanel.add(combo);    
	  }
	  this.map.app.ui.doLayout();
	  
	  for (var i=0; i<comboConfigs.length; i++) {
	    var joined = parseInt(comboConfigs[i].field_join); 
	    if (joined!=0 && joined <=comboConfigs.length) {
	      this.linkCombos(i, joined-1, comboConfigs, this.config);  
	    }
	  }
    /*
    var btn = this.zoomToPlacePanel.buttons[0];
    btn.addListener('click', 
      function () {
        var tabExtent = this.extentArea.split(",");
        this.map.zoomToExtent(
          new OpenLayers.Bounds(tabExtent[0],
            tabExtent[1],
            tabExtent[2],
            tabExtent[3]
          ));
      },
      this);
   */
   // loading combos...
   for (var i=0; i<this.combos.length; i++) {
	   var joined = parseInt(comboConfigs[i].field_join); 
	   if (!(joined!=0 && joined <=comboConfigs.length)) { 
	     this.combos[i].getStore().load();
	   }       
	  }   
  },
  
  buildCombo : function (comboConfig, controlConfig) {
    var combo = Ext.create('Ext.form.ComboBox',{ 
      listConfig: {
        loadingText: 'Chargement',
        getInnerTpl: function() { return '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>'; }
      },
      emptyText: comboConfig.field_name,
      displayField: 'name',
      valueField: 'name',
      typeAhead: true,
      typeAheadDelay: 250,
      forceSelection: true,
      queryMode: 'local',
      triggerAction: 'all',
      editable: true,
      minChars: 1,
      queryDelay: 250,
      width: '100%',
      xtype: 'combo',
      id: 'combo' + comboConfig.field_order,
      store: Carmen.Control.ZoomToPlaceFromDB.buildStore(comboConfig, controlConfig),
      listeners:{select:{fn:function(combo, value) {
        this.extentArea = value[0].data.extent;
                        },scope:this
      }
                    }
    });
    
    return combo;
  },
  
  linkCombos : function(linked_id, parent_id, comboConfigs, controlConfig) {
    this.combos[parent_id].addListener('select', 
      function(cmb, record) {
        var params = { queryParams_filterValue : record[0].data.id };  
        this.combos[linked_id].clearValue();
        this.combos[linked_id].getStore().load({params: params});
        //this.extentArea = record[0].data.extent;
        this.combos[linked_id].enable();
     }, this);
     
     this.combos[linked_id].disable();
  },
  
  
  

  CLASS_NAME: "Carmen.Control.ZoomToPlaceFromDB"

});


// Todo: Should be passed as a config object, at least the reader...
Carmen.Control.ZoomToPlaceFromDB.buildStore = function(comboConfig, controlConfig) {
  var params = OpenLayers.Util.extend(
   { queryMode: 'getData', 
     queryParams_pk: comboConfig.field_pk},
   controlConfig.providerBaseParams);
       
  var store = new Ext.data.Store({
    proxy: {
      type : 'ajax',
      url: controlConfig.providerUrl,
      extraParams: params,
      reader: {
        type : 'json',
        idproperty: 'id',
        totalProperty: 'totalCount',
        rootProperty: 'Names'
      },
    },
    fields : 
      [ { name: 'extent'},
        { name: 'id' },
        { name: 'name' }
      ]
  });
  //console.log(store.baseParams);
  return store;
};
