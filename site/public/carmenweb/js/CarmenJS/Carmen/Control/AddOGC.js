/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.AddOGC
 * 
 * Inherits from: - <OpenLayers.Control>
 */

Carmen.Control.AddOGC = new OpenLayers.Class(OpenLayers.Control,
{
  // UI components
  
  
  
  win : null,

  setMap: function(map) {
      OpenLayers.Control.prototype.setMap.apply(this, arguments);

      /*this.btn.addListener('toggle', 
	      Carmen.Util.buildExt2olHandlerToggle(this));    
      
      
      this.btn.on("click",
        function(event, toolEl, panelHeader) {
          this.showWindow();
        }
      )
      */
      /*
      this.map.app.ui.dataPanel.addTool({
        qtip: 'Ajouter une couche',
        xtype: 'tool',
        renderTpl: [
                  '<i id="{id}-toolEl" data-ref="toolEl" class="fa fa-plus-square  fa-1x"' +
                  '" style="color: #FFF; font-size: 16px;" role="presentation"/></i>'
                ],
        scope:this,          
        handler : function(){this.showWindow()}
      });
      */

  },
  
  activate: function() {
  	OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    this.showWindow();
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win && this.win.rendered)
      this.win.close();
    this.map.toolTipActivate();
  },
    
  addServiceList : function(str) {
  	var control = this;   
  	var queryParams = {
  	  	          serviceType : str,
  	  	        }  	
  	var serveurWXSURL = Ext.getCmp("combo"+str);
  	Ext.data.JsonP.request({
  		method: 'GET',
    		url: CARMEN_URL_CATALOGUE+'/catalogue/Services/getOgcInfo.php',
          success: function(response) { 
  
          	var Tab=new Array(); 
          	if (response.Count == 0) {
              control.clearGridPanel();
              Ext.Msg.alert("Ajouter un service", "Aucun serivce non présent dans la liste n'a été trouvé.");
            }else{               	
          		storeList = serveurWXSURL.getStore();// avat d'ajouter on verifie si  le flux n'est pas deja present
              for(id in response.results.data)  {       	    		  
          			if(storeList.find("URL", response.results.data[id][1])==-1){ // si pas d'index trouve         		
              		Tab.push([response.results.data[id][0],response.results.data[id][1]]);      
              	}
          		} 
              serveurWXSURL.getStore().add(Tab);	
          	}	
          },
          failure: function (response) {
          	
          	Ext.Msg.alert("Ajouter un service", "Echec de la récupération des services");     	
  
          },
          params: queryParams
      }); 	
  },
  
  
  showWindow : function() {
	  
        // Un tableau qui contiendra nos onglets.
        var tabPanels = new Array();
        // WMS
        var storeWMS =  new Ext.data.XmlStore({
                        autoLoad: true,
                        fields : 
                        [ { name: 'NOM', mapping: '@NOM'},
                          { name: 'URL', mapping: '@URL' }
                        ],
                        proxy: {
                          type : 'ajax',
                          url: '/services/AddWMS/getServeursXml.php',
                          extraParams : {"service" : "WMS"},
                          reader: {
                            type : 'xml',
                            rootProperty: 'SERVEURS',
                            record: 'SERVEUR'
                          },
                        }
                      });
        var panelWMS = Ext.create('Ext.form.Panel',{
            //layout : 'fit',
            bodyPadding: 10,
            id:"formWMS",
            //constrain : true,
            //plain : true,
            title: "WMS",
            // shim :false,
            autoDestroy : true,
            // monitorValid:true,
            resizable : true,

            items: [
              {
              xtype: 'fieldset',
              id: 'fieldsetListWMS',
              defaults : {width : '100%'},
              title: 'Choix du serveur WMS',
              items: [
                  

                  { xtype: 'fieldcontainer',
                    fieldLabel: 'Serveur WMS',
                    disabled :false,
                    id : 'valueFieldContainerWMS',
                    layout: 'hbox',
                    defaults: {
                        hideLabel: true
                    },
                    items: [
                   
                       {
                          xtype: 'combo',
                          id: 'comboWMS',
                          fieldLabel: 'Serveur WMS',
                          editable: false,
                          listConfig : { loadingText: 'Chargement' },
                          emptyText: 'Sélectionnez un serveur',
                          displayField: 'NOM',
                          width: '92%',
                          margin: '0 5 0 0',
                          valueField: 'URL', 
                          store: storeWMS,
                          validator: function(v) {
                            return (v!='Sélectionnez un serveur');
                          },
                          listeners: {
                            select:{fn:function(combo, value) {
                              Ext.getCmp("INPUT_URL_WMS").setValue(value[0].data.URL);
                            }}
                          }
                       }, {
                        xtype: 'button',
              	        tooltip: 'Ajouter les services référencés dans le catalogue à la liste',
              	        tooltipType: 'title',
              	        msgTarget : 'side',
              	        cls: 'x-btn-icon',
              	        text : '<i class="fa fa-mail-reply"></i>',
              	        listeners : {
            	          'click' : {
            	             fn : function(b,evt) {
            	               this.addServiceList('WMS');
            	              },
            	             scope : this
            	            }
              	        }
                    	}
                     ]
                   },
                {
                      xtype :"textfield",
                      fieldLabel :'URL du serveur',
                      id :'INPUT_URL_WMS',
                      name :'INPUT_URL_WMS',
                      value :"",
                 },{
                      xtype: 'combo',
                      id: 'SELECT_VERSION_WMS',
                      fieldLabel: 'Version du service WMS interrogée',
                      editable: false,
                      listConfig : { loadingText: 'Chargement' },
                      emptyText: 'Sélectionnez une version',
                      store: new Ext.data.SimpleStore({
                          fields: ['version', 'label'],
                          data : [[ '1.1.1', '1.1.1' ],
                                  [ '1.3.0', '1.3.0' ]
                          ]}
                      ),
                      validator: function(v) {
                        return (v!='Sélectionnez une version');
                      },
                      displayField: 'label',
                      allowBlank: false,
                      valueField: 'version'                 
                 }/*,
                 {
                   xtype: 'radiogroup',
                     fieldLabel : 'Filtre',
                     defaultType: 'radiofield',
                     items: [
                         {
                           boxLabel  : 'Afficher le service ou les couches autorisant la projection de la carte',
                           name      : 'filterProjMap',
                           checked   : true,
                           inputvalue:1
                         },
                         {
                           boxLabel  : 'Afficher les couches autorisant la projection de la carte',
                           name      : 'filterProjMap',
                           inputvalue:2
                       }
                     ]
                 }*/

                 
               ]
             }, 
             {
                      xtype : 'button',
                      id :'listcouchesWMS',
                      width : "150px",
                      
                      text :'Lister les couches',
                      handler : function() {
                        var form = Ext.getCmp('formWMS').getForm();
                        if (form.isValid()) {
                              Ext.MessageBox.show({ 
                                msg: 'Interrogation en cours, merci de patienter...', 
                                progressText: 'Interrogation...', 
                                width:300, 
                                wait:true, 
                                waitConfig: {interval:200}
                              });
                             this.AddWMS_ListerCoucheFromServeur();
                        }
                      }, scope : this
            } 
             ],
                
             buttons : []
            
        });
        
        // WFS
        var panelWFS = Ext.create('Ext.FormPanel',{
            bodyPadding: 10,
            id:"formWFS",
            //constrain : true,
            //plain : true,
            title: "WFS",
            // shim :false,
            autoDestroy : true,
            // monitorValid:true,
            resizable : true,
            items: [
              {
              xtype: 'fieldset',
              id: 'fieldsetListWFS',
              defaults : {width : '100%'},
              title: 'Choix du serveur WFS',
              items: [
                 { xtype: 'fieldcontainer',
                   fieldLabel: 'Serveur WFS',
                   disabled :false,
                   id : 'valueFieldContainerWFS',
                   layout: 'hbox',
                   defaults: {
                       hideLabel: true
                   },
                   
                   items : [
                   {
                      xtype: 'combo',
                      id: 'comboWFS',
                      fieldLabel: 'Serveur WFS',
                      editable: false,
                      listConfig : { loadingText: 'Chargement' },
                      emptyText: 'Sélectionnez un serveur',
                      displayField: 'NOM',
                      valueField: 'URL', 
                      width:'92%',
                      margin: '0 5 0 0',
                      store: new Ext.data.Store({
                        autoLoad: true,
                        fields : 
                        [ { name: 'NOM', mapping: '@NOM'},
                          { name: 'URL', mapping: '@URL' }
                        ],
                        proxy: {
                          type : 'ajax',
                          applyEncoding : '',
                          url: '/services/AddWMS/getServeursXml.php',
                          extraParams : {"service" : "WFS"},
                          reader: {
                            type : 'xml',
                            rootProperty: 'SERVEURS',
                            record: 'SERVEUR'
                          },
                        }
                      })
                      ,
                      
                      validator: function(v) {
                        return (v!='Sélectionnez un serveur');
                      },
                      listeners: {
                        select:{fn:function(combo, value) {
                          Ext.getCmp("INPUT_URL_WFS").setValue(value[0].data.URL);
                        }}
                      }
              
                },{
                  xtype: 'button',
                  tooltip: 'Ajouter les services référencés dans le catalogue à la liste',
                  tooltipType: 'title',
                  msgTarget : 'side',
                  cls: 'x-btn-icon',
                  text : '<i class="fa fa-mail-reply"></i>',
                  listeners : {
                  'click' : {
                     fn : function(b,evt) {
                       this.addServiceList('WFS');
                      },
                     scope : this
                    }
                  }
                }
                ]                
                 }
                 
                 
                 
                 ,
                
                {
                      xtype :"textfield",
                      fieldLabel :'URL du serveur',
                      id :'INPUT_URL_WFS',
                      name :'INPUT_URL_WFS',
                      value :"",
                 }
               ]
             },
             {
                      xtype : 'button',
                      id :'listcouchesWFS',
                      text :'Lister les couches',
                      handler : function() {
                        var form = Ext.getCmp('formWFS').getForm();
                        if (form.isValid()) {
                              Ext.MessageBox.show({ 
                                msg: 'Interrogation en cours, merci de patienter...', 
                                progressText: 'Interrogation...', 
                                width:300, 
                                wait:true, 
                                waitConfig: {interval:200}
                              });
                             
                             this.AddWFS_ListerCoucheFromServeur();
                        }
                      }, scope : this
            }
             ],
                
             buttons : [  ]
            
        });
        // WMTS
        var panelWMTS = Ext.create('Ext.form.Panel',{
            //layout : 'fit',
            bodyPadding: 10,
            id:"formWMTS",
            //constrain : true,
            //plain : true,
            title: "WMTS",
            // shim :false,
            autoDestroy : true,
            // monitorValid:true,
            resizable : true,
            items: [
              {
              xtype: 'fieldset',
              id: 'fieldsetListWMTS',
              defaults : {width : '100%'},
              title: 'Choix du serveur WMTS',
              items: [
                   {
                      xtype: 'combo',
                      id: 'comboWMTS',
                      fieldLabel: 'Serveur WMTS',
                      editable: false,
                      listConfig : { loadingText: 'Chargement' },
                      emptyText: 'Sélectionnez un serveur',
                      displayField: 'NOM',
                      valueField: 'URL',   
                      store: new Ext.data.Store({
                        autoLoad: true,
                        fields : 
                        [ { name: 'NOM', mapping: '@NOM'},
                          { name: 'URL', mapping: '@URL' }
                        ],
                        proxy: {
                          type : 'ajax',
                          applyEncoding : '',
                          url: '/services/AddWMS/getServeursXml.php',
                          extraParams : {"service" : "WMTS"},
                          reader: {
                            type : 'xml',
                            rootProperty: 'SERVEURS',
                            record: 'SERVEUR'
                          },
                        }
                      }),
                      validator: function(v) {
                        return (v!='Sélectionnez un serveur');
                      },
                      listeners: {
                        select:{fn:function(combo, value) {
                          Ext.getCmp("INPUT_URL_WMTS").setValue(value[0].data.URL);
                        }}
                      }
              
                },{
                      xtype :"textfield",
                      fieldLabel :'URL du serveur',
                      id :'INPUT_URL_WMTS',
                      name :'INPUT_URL_WMTS',
                      value :"",
                 }
               ]
             },
             {
                     xtype : 'button',
                     id :'listcouchesWMTS',
                      text :'Lister les couches',
                      handler : function() {
                        var form = Ext.getCmp('formWMTS').getForm();
                        if (form.isValid()) {
                              Ext.MessageBox.show({ 
                                msg: 'Interrogation en cours, merci de patienter...', 
                                progressText: 'Interrogation...', 
                                width:300, 
                                wait:true, 
                                waitConfig: {interval:200}
                              });
                             this.AddWMTS_ListerCoucheFromServeur();
                        }
                      }, scope : this
            }
             ],
                
             buttons : [  ]
            
        });

        // WMS-C
        var panelWMSC =  Ext.create('Ext.form.Panel',{
            //layout : 'fit',
            bodyPadding: 10,
            id:"formWMSC",
            //constrain : true,
            //plain : true,
            title: "WMS-C",
            // shim :false,
            autoDestroy : true,
            // monitorValid:true,
            resizable : true,
            items: [
              {
              xtype: 'fieldset',
              id: 'fieldsetListWMSC',
              defaults : {width : '100%'},
              title: 'Choix du serveur WMS-C',
              items: [
                   {
                      xtype: 'combo',
                      id: 'comboWMSC',
                      fieldLabel: 'Serveur WMS-C',
                      editable: false,
                      listConfig : { loadingText: 'Chargement' },
                      emptyText: 'Sélectionnez un serveur',
                      displayField: 'NOM',
                      valueField: 'URL',   
                      store: new Ext.data.Store({
                        autoLoad: true,
                        fields : 
                        [ { name: 'NOM', mapping: '@NOM'},
                          { name: 'URL', mapping: '@URL' }
                        ],
                        proxy: {
                          type : 'ajax',
                          applyEncoding : '',
                          url: '/services/AddWMS/getServeursXml.php',
                          extraParams : {"service" : "WMSC"},
                          reader: {
                            type : 'xml',
                            rootProperty: 'SERVEURS',
                            record: 'SERVEUR'
                          },
                        }
                      }),
                      validator: function(v) {
                        return (v!='Sélectionnez un serveur');
                      },
                      listeners: {
                        select:{fn:function(combo, value) {
                          Ext.getCmp("INPUT_URL_WMSC").setValue(value[0].data.URL);
                        }}
                      }
              
                },{
                      xtype :"textfield",
                      fieldLabel :'URL du serveur',
                      id :'INPUT_URL_WMSC',
                      name :'INPUT_URL_WMSC',
                      value :"",
                 }
               ]
             },
             {
                      xtype : 'button',
                      id :'listcouchesWMSC',
                      text :'Lister les couches',
                      handler : function() {
                        var form = Ext.getCmp('formWMSC').getForm();
                        if (form.isValid()) {
                              Ext.MessageBox.show({ 
                                msg: 'Interrogation en cours, merci de patienter...', 
                                progressText: 'Interrogation...', 
                                width:300, 
                                wait:true, 
                                waitConfig: {interval:200}
                              });
                             this.AddWMSC_ListerCoucheFromServeur();
                        }
                      }, scope : this
            }
             ],
                
             buttons : [  ]
            
        });
        var PanelLocalData = Ext.create('Ext.form.Panel',{
			layout: 'fit',
			bodyPadding: 10,
			id: "formlocaldata",
			title: "Données locales",
			autoDestroy: true,
			resizable :  true,
      height : 400,
              
			items: [{
				xtype : "fieldset",
				id : "fieldsetLocaldata",
				defaults : {width: '100%'},
				title : 'Choix des données à ajouter',
				layout: 'fit',
				height: '100%',
				items : [{
				    xtype: "uxiframe",
				    frameName : "iFrameLocalData",
				    id : "iFrameLocalData",
				    src : "/services/AddLocalData/UploadPopup.php?path="+CARMEN_URL_PATH_DATA+"cartes/Publication/local_data/&EXT_THEME_COLOR="+this.map.app.context.mdataMap.EXT_THEME_COLOR
			    }]
			}],
			buttons : [{
				id : "displayLocaldata",
				text : "Afficher sur la carte",
				handler : function(){
			      var iframe = document.getElementById("iFrameLocalData");
			      iframe     = Ext.get(iframe.getElementsByTagName("iframe")[0]);
			      btn_import = iframe.el.dom.contentDocument.getElementById("btn_layersimportationlocaldata");
			      btn_import.onclick();
				}
			}]
		});

        //On ajoute les onglets a notre tableau d'onglets
        tabPanels = [panelWMS,panelWFS,panelWMTS,panelWMSC,PanelLocalData];
        //On definit un panel d'onglet contenant notre tableau d'onglets
        var tabItems =  Ext.create('Ext.TabPanel',{
          region: 'center',
          xtype: 'tabpanel',
          activeTab: 0,
          constrain: true,
          plain: true,
          modal: false,
          items: tabPanels
        });
        //On definit notre fenetre contenant notre panel d'onglet
        this.win = Ext.create('Ext.Window',{
          title: 'Ajouter une couche',
          width: 550,
          //height: 700,
          layout: 'fit',
          items: tabItems,
          id: 'window_mainajout',
          constrain: true,
          plain: true,
          //modal: false,
          x: 480,
          y: 16,
          autoDestroy: true,
          resizable: true
        });
	     
        // adding listeners to link window closing and control deactivation   
        var control = this;
        this.win.on(
          'hide',
          function(w) { control.btn.toggle(false); },
          this.win);
        
       this.win.show();
  },
   
  
  /**
  * ajout de couches WMS
  **/
  AddWMS_ListerCoucheFromServeur : function() {
    
      var   version = '1.1.1';
      var mapSRS = this.map.projection
      var fieldsetData = Ext.getCmp("fieldsetDataWMS");
      var selectFormatWMS = Ext.getCmp("SELECT_FORMAT_WMS");
      var me = this;
      
      //suppresion des champs recréés
      if(fieldsetData && selectFormatWMS){
        Ext.getCmp("formWMS").remove(fieldsetData);
        Ext.getCmp("fieldsetListWMS").remove(selectFormatWMS);
      }
      var buildTreeWMS = function(dataTree) {
          var me = this;
          var tree = new Ext.tree.TreePanel({
              //loader: new Ext.tree.TreeLoader(),
              animate:true,
              autoScroll:true,
              maxHeight: ((Ext.getBody().getViewSize().height)-(Ext.getCmp("window_mainajout").getHeight()))*0.5,
              height: 400,
              hideHeaders : true,
              id : "WMS_TREE",
              root:{
                  expanded: true,
                  children: [dataTree]
              },
              columns: [{
                  xtype: 'treecolumn', //this is so we know which column will show the tree
                  text: 'Données',
                  width: 350,
                  sortable: false,
                  dataIndex: 'text'
              },
              {
                  text: 'Ajout',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Ajouter la couche dans la carte',
                  //icon: '/IHM/images/add.png',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw falk-addlayer fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                      var serverURL = record.data.serverURL;
                      // retrieving version number
                      var version = record.data.serverVersion;
                      var format = Ext.getCmp("SELECT_FORMAT_WMS").getValue();
                      if (serverURL != "" && format!="" && version!="") {
                        Carmen.Control.AddOGC.addLayerWMSToMap(record.data.text, serverURL, record.data.name, Ext.getCmp("SELECT_FORMAT_WMS").getValue(), app.map.projection, record.data.metadataUrl, version);
                      }
                      
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              },
              {
                  text: 'Aperçu',
                  xtype: 'actioncolumn',
                  dataIndex: 'text',
                  width: 30,
                  sortable: false,
                  tooltip: 'Visualiser la donnée',
                  //icon: '/IHM/images/image_add.png',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-eye fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                    var serverURL = record.data.serverURL;
                    var lparamGetMap = "";
                    // retrieving version number
                    var version = record.data.serverVersion;
                    var format = Ext.getCmp("SELECT_FORMAT_WMS").getValue();
                    
                    if (serverURL != "" && format!="" && version!="") {
                      
                        lparamGetMap = lparamGetMap + "&width=500";
                        lparamGetMap = lparamGetMap + "&height=500";
                        
                        lparamGetMap = lparamGetMap + "&format=" + Ext.getCmp("SELECT_FORMAT_WMS").getValue();
                        lparamGetMap = lparamGetMap + "&layers=" + record.data.name;
                        lparamGetMap = lparamGetMap + "&bbox=" +  app.map.getExtent().left.toString() + ','+
                                                              +app.map.getExtent().bottom.toString() + ','+
                                                              +app.map.getExtent().right.toString() + ','+
                                                              +app.map.getExtent().top.toString()
                        lparamGetMap = lparamGetMap + "&styles=";
                        lparamGetMap = lparamGetMap + "&version=" + version;
                        lparamGetMap = lparamGetMap +
                                                    (version== "1.3.0" ?
                                                        "&CRS=" +  app.map.projection :
                                                        "&SRS=" +  app.map.projection);
                        var lUrl = '';
                        lUrl = serverURL;
                        if (serverURL.indexOf('?') != -1)
                          lUrl += "&service=wms&request=getmap";
                        else
                          lUrl += "?service=wms&request=getmap";
                        lUrl += lparamGetMap;
                        
                        Carmen.Util.openWindowIframe("Prévisualisation", lUrl, false, 500, 500);
                        
                    } else
                      alert("");
                   
                  },
                  // Only leaf level tasks may be edited
                  
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
                   
              },
              {
                  text: 'Métadonnée',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Visualiser la métadonnée associée',
                  //icon: '/IHM/images/information.png',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                       if (record.data.metadataUrl){
                        Carmen.Util.openWindowIframe("Métadonnée", record.data.metadataUrl, false, Math.min(document.body.clientWidth-100, 1220), Math.min(document.body.clientHeight-100, 790));
                      }else{
                        Ext.Msg.alert('Ajout de couche WMS', 'Aucune métadonnée n\'a été définie');
                      }
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              }],
              
              rootVisible: false,
              //enableDD: true,
              containerScroll: true
          });
          return tree;
      }

      var parseWMSCapabilities = function(response) {
          var wmsCpblt = new OpenLayers.Format.WMSCapabilities();    
          var capabilities = wmsCpblt.read(response.responseText);
          Ext.MessageBox.hide();
          if (capabilities.capability && capabilities.capability.nestedLayers) {
              var jsonLayersDesc = capabilities.capability.nestedLayers;
              
              var layersDesc = jsonLayersDescToExtTreeWMS(jsonLayersDesc, mapSRS);
              if(layersDesc.length>0){
                var dataTree = {
                    text : capabilities.service.title,
                    children : layersDesc[0].children
                };
               var wmsLayerTree = buildTreeWMS(dataTree);
               var formatStore = [];
               for(var i=0; i< capabilities.capability.request.getmap.formats.length; i++){
                 var formatValue = capabilities.capability.request.getmap.formats[i];
                 formatStore.push([formatValue,formatValue]);
               }
               Ext.getCmp("formWMS").add(
                  {
                    xtype: 'fieldset',
                    id: 'fieldsetDataWMS',
                    title: 'Choix des données',
                    items : [ {
                      xtype: 'combo',
                      id: 'SELECT_FORMAT_WMS',
                      fieldLabel: 'Format de la couche',
                      editable: false,
                      listConfig : { loadingText: 'Chargement' },
                      emptyText: 'Sélectionnez un format',
                      triggerAction: 'all',
                      minChars: 1,
                      queryDelay: 250,     
                      queryMode: 'local',
                      store: new Ext.data.SimpleStore({
                          fields: ['format', 'label'],
                          data : formatStore
                        }
                      ),
                      validator: function(v) {
                        return (v!='Sélectionnez un format');
                      },
                      displayField: 'label',
                      allowBlank: false,
                      valueField: 'format'
                    }, wmsLayerTree]
                  }
                );
                
                Ext.getCmp('SELECT_FORMAT_WMS').setValue(formatStore[0][0]);
                
                // expand root node
                //wmsLayerTree.getRootNode().expand(false, false);
              }else{
                top.Ext.Msg.alert("Ajout de couches WMS",
                    "Le service n'est pas disponible dans la projection de la carte");
              }
          }
          else {
              top.Ext.Msg.alert("Ajout de couches WMS",
                  "Le service ne semble pas répondre correctement. " +
                  "Il n'est peut être pas compatible avec la version de service demandée" +
                  " (" + version +").");
          }
          //document.getElementById("listingWMSData").disabled = false;
      }
      
      var jsonLayersDescToExtTreeWMS = function(jsonLayersDesc, projectionFilter) {
          var res = [];
          
          for(var i=0; i<jsonLayersDesc.length; i++){
                  var jsonLayerDesc = jsonLayersDesc[i];
                  var layer = {};
                  layer.name = jsonLayerDesc.name;
                  layer.text = jsonLayerDesc.title;
                  //TODO à améliorer
                  layer.serverURL = Ext.getCmp("INPUT_URL_WMS").getValue();
                  layer.serverVersion = "1.1.1";
                  layer.metadataUrl = '';
                  if (jsonLayerDesc.metadataURLs instanceof Array &&
                      jsonLayerDesc.metadataURLs.length>0) {
                      layer.metadataUrl = jsonLayerDesc.metadataURLs[0].href;
                  }
                  layer.srs = [];
                  if (jsonLayerDesc.srs) {
                          if (jsonLayerDesc.srs instanceof Array &&
                              jsonLayerDesc.srs.length>0) {
                              for (var j=0; j<jsonLayerDesc.srs.length; j++) {
                                  layer.srs.push(jsonLayerDesc.srs[j]);
                              }

                          }
                          else {
                              for (srsName in jsonLayerDesc.srs) {
                                  if (jsonLayerDesc.srs[srsName]==true)
                                      layer.srs.push(srsName);
                              }
                          }
                  }
                  layer.styles = jsonLayerDesc.styles;
                  layer.formats = jsonLayerDesc.formats;
                  if(jsonLayerDesc.nestedLayers && jsonLayerDesc.nestedLayers.length>0)
                          layer.children = jsonLayersDescToExtTreeWMS(jsonLayerDesc.nestedLayers,projectionFilter);
                  else
                          layer.leaf = true;
                   // filtering layer that complies with map projection
                  filter = false; // should the layer be filtered
                  if (projectionFilter && projectionFilter!="") {
                      filter = true;
                      for (var j=0; j< layer.srs.length; j++) {
                          if (layer.srs[j]==projectionFilter) {
                              filter=false;
                              break;
                          }
                      }
                  }
                  if (!filter || (layer.children && layer.children.length>0))
                      res.push(layer);
          }
          return res;
      }
      
      // retrieving version number
      version = Ext.getCmp('SELECT_VERSION_WMS').getValue();

      // configuring proxy for XHR call
      OpenLayers.ProxyHost = "/services/GetInformation/proxy.php?proxy_url=";

     // frm = document.getElementById('WMS_CAPABILITIES'); ex. http://carto.sigloire.fr/cgi-bin/mapserv?
      var serveurWMSURL = Ext.getCmp("INPUT_URL_WMS");
      if (serveurWMSURL) {
          // retrieving serverBaseUrl
          serverUrl = serveurWMSURL.value;// unescape(serveurWMS.value);

          var lTabDesc = serverUrl.split("\|");
          var serverBaseUrl = lTabDesc[0];

          // launching getCapabilities request
          OpenLayers.Request.GET({
              url: serverBaseUrl,
              params: {
                  SERVICE: "WMS",
                  REQUEST: "GetCapabilities",
                  VERSION: version
              },
              success: parseWMSCapabilities,
              failure: function(r) {
                  Ext.Msg.alert('Ajout de couche WMS', 'Problème dans la communication avec le serveur.')
              }
          });
      }
    },
    
    
    AddWFS_ListerCoucheFromServeur : function () {
      
      //empty div WFS_TREE and put loading text
      var fieldsetData = Ext.getCmp("fieldsetDataWFS");
       
      //suppresion des champs recréés
      if(fieldsetData){
        Ext.getCmp("formWFS").remove(fieldsetData);
      }
      var parseWFSCapabilities = function(response) {
          var wmsCpblt = new OpenLayers.Format.WFSCapabilities();    
          var capabilities = wmsCpblt.read(response.responseText);
          //console.log(capabilities);
          Ext.MessageBox.hide();
          if (capabilities.featureTypeList && capabilities.featureTypeList.featureTypes) {
              var jsonLayersDesc = capabilities.featureTypeList.featureTypes;
              var layersDesc = jsonLayersDescToExtTreeWFS(jsonLayersDesc);
              //if(layersDesc.length>0){
              //console.log(layersDesc);
                var dataTree = {
                    text : capabilities.service.title,
                    children : layersDesc
                };
               var wfsLayerTree = buildTreeWFS(dataTree);
                Ext.getCmp("formWFS").add(
                  {
                    xtype: 'fieldset',
                    id: 'fieldsetDataWFS',
                    title: 'Choix des données',
                    items : [wfsLayerTree]
                  }
                );
                
                // expand root node
                //wmsLayerTree.getRootNode().expand(false, false);
             /* }else{
                top.Ext.Msg.alert("Ajout de couches WFS",
                    "Le service n'est pas disponible dans la projection de la carte");
              }*/
          }
          else {
              top.Ext.Msg.alert("Ajout de couches WFS",
                  "Le service ne semble pas répondre correctement. " +
                  "Il n'est peut être pas compatible avec la version de service demandée");
          }
          //document.getElementById("listingWMSData").disabled = false;
      }  
      
      
      var buildTreeWFS = function(dataTree) {
          var me = this;
          var tree = new Ext.tree.TreePanel({
              //loader: new Ext.tree.TreeLoader(),
              animate:true,
              autoScroll:true,
              maxHeight: ((Ext.getBody().getViewSize().height)-(Ext.getCmp("window_mainajout").getHeight()))*0.5,
              height: 400,
              hideHeaders : true,
              id : "WFS_TREE",
              root:{
                  expanded: true,
                  children: [dataTree]
              },
              columns: [{
                  xtype: 'treecolumn', //this is so we know which column will show the tree
                  text: 'Données',
                  width: 350,
                  sortable: false,
                  dataIndex: 'text'
              },
              {
                  text: 'Ajout',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Ajouter la couche dans la carte',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw falk-addlayer fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                      var serverURL = record.data.serverURL;
                      // retrieving version number
                      var version = record.data.serverVersion;
                      /*if (serverURL != "" && format!="" && version!="") {
                        Carmen.Control.AddOGC.addLayerWFSToMap(record.data.title, serverURL, record.data.name, Ext.getCmp("SELECT_FORMAT_WMS").getValue(), app.map.projection, record.data.metadataUrl, version);
                      }*/
                      
                      
                      var queryParams ={
                        "mapProjection" : app.map.projection , 
                        "connection" : serverURL,
                        "layerName" : record.data.name,
                        "layerTitle" : record.data.text,
                        "extent": app.map.getExtent().left.toString() + ','+
                                                            +app.map.getExtent().bottom.toString() + ','+
                                                            +app.map.getExtent().right.toString() + ','+
                                                            +app.map.getExtent().top.toString(),
                        "layerProjection" : record.data.srs
                      };

                      if (serverURL.indexOf('?') != -1)
                        var lUrl = serverURL+"service=wfs&version=1.0.0&typeName="+record.data.name.value+"&request=getFeature";
                      else 
                        var lUrl = serverURL+"?service=wfs&version=1.0.0&typeName="+record.data.name+"&request=getFeature";
                      var lLayerDownloadUrl = CARMEN_URL_SERVER_DOWNLOAD+ "/index.php?"+
                                    "mode=prodige"+
                                    "&data_type=url"+
                                    "&direct=MQ%3D%3D"+
                                    "&fileRedirect=1"+
                                    "&service_idx=1"+
                                    "&projection="+
                                    "&email="+
                                    "&format=Z21s" +//url
                                    "&data="+encodeValue(lUrl);
                      // adding loading mask   
                      Ext.MessageBox.show({ 
                                msg: 'Récupération des données WFS en cours...', 
                                progressText: 'Interrogation...', 
                                width:300, 
                                wait:true, 
                                waitConfig: {interval:200}
                              });
                      Ext.Ajax.timeout = 900000;    
                      Ext.Ajax.request( {
                        url : window.CARMEN_URL_SERVER_FRONT+"/services/AddWFS/addLayerToMap.php",
                        success : function(response, opts) {
                          // removing loading mask
                          Ext.MessageBox.hide();
                          eval(response.responseText);
                          if(typeof(tmpFile)!="undefined" && typeof(legendUrl)!="undefined" && typeof(layerName)!="undefined"){
                            Carmen.Control.AddOGC.addLayerWFSToMap(layerName, tmpFile, record.data.text, legendUrl,
                              infoFields, briefFields, toolTipFields, lLayerDownloadUrl,  "");
                          }
                          else {
                            alert("Impossible d'ajouter cette couche : \n "+errMsg);
                          }
                        },
                        failure : function (response, opts) {
                          // removing loading mask
                          Ext.MessageBox.hide();
                          Ext.Msg.alert('Ajout de couche WFS', 'Impossible d\'ajouter cette couche')
                          //Carmen.Util.handleAjaxFailure(response, "Ajout de couche WFS"); 
                        },
                        params :queryParams
                      });
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              },
              {
                  text: 'Aperçu',
                  xtype: 'actioncolumn',
                  width: 30,
                  sortable: false,
                  tooltip: 'Visualiser la donnée',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-eye fa-1x', 'color: #000; font-size: 16px;'), 
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                    var serverURL = record.data.serverURL;
                    var lparamGetMap = "";
                    // retrieving version number
                    var version = record.data.serverVersion;
                    if (serverURL != "" && version!="") {
                      var urlGetMap = window.CARMEN_URL_SERVER_FRONT+"/services/AddWFS/addLayerToMap.php?service=visu";
                        
                      urlGetMap+="&mapProjection="+app.map.projection;
                      urlGetMap+="&connection="+serverURL;
                      urlGetMap+="&layerName="+record.data.name;
                      urlGetMap+="&layerTitle="+record.data.text;
                      urlGetMap+="&extent="+ app.map.getExtent().left.toString() + ','+
                                                          +app.map.getExtent().bottom.toString() + ','+
                                                          +app.map.getExtent().right.toString() + ','+
                                                          +app.map.getExtent().top.toString();
                      urlGetMap+="&layerProjection=" +record.data.srs;
                      
                      Carmen.Util.openWindowIframe("Prévisualisation", urlGetMap, false, 500,500);
                   
                   }
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              },
              {
                  text: 'Métadonnée',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Visualiser la métadonnée associée',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                      if (record.data.metadataUrl){
                        
                        Carmen.Util.openWindowIframe("Métadonnée", record.data.metadataUrl, false, Math.min(document.body.clientWidth-100, 1220),Math.min(document.body.clientHeight-100, 790));
                        
                      }else{
                        Ext.Msg.alert('Ajout de couche WFS', 'Aucune métadonnée n\'a été définie');
                      }
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              }
              ],
              
              rootVisible: false,
              //enableDD: true,
              containerScroll: true
          });
          return tree;
      }
      
      
      var jsonLayersDescToExtTreeWFS = function(jsonLayersDesc) {
          var res = [];
          for(var i=0; i<jsonLayersDesc.length; i++){
                  var jsonLayerDesc = jsonLayersDesc[i];
                  var layer = {};
                  layer.name = jsonLayerDesc.name;
                  layer.text = jsonLayerDesc.title;
                  layer.leaf = true;
                  layer.serverURL = Ext.getCmp("INPUT_URL_WFS").getValue();
                  layer.serverVersion = "1.0.0";
                  layer.srs = jsonLayerDesc.srs;
                  layer.metadataUrl = jsonLayerDesc.MetadataURL;
                  //console.log(jsonLayerDesc);
                 
                  res.push(layer);
          }
          var sort_by = function(field, reverse, primer){

            var key = primer ? 
                function(x) {return primer(x[field])} : 
                function(x) {return x[field]};

            reverse = !reverse ? 1 : -1;

            return function (a, b) {
                return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
              } 
          }
          res.sort(sort_by('text', false, function(a){return a.toUpperCase()}));
          return res;
      }
      
      var   serveurWFS;
      var   serverUrl;
      var   url;
      
      var   liste;
      var   opt;
      
      // configuring proxy for XHR call
      OpenLayers.ProxyHost = "/services/GetInformation/proxy.php?proxy_url=";

      var serveurWFSURL = Ext.getCmp("INPUT_URL_WFS");
      if (serveurWFSURL)
      {
      
        // retrieving serverBaseUrl
        serverUrl = serveurWFSURL.value;// unescape(serveurWMS.value);
        var lTabDesc = serverUrl.split("\|");
        var serverBaseUrl = lTabDesc[0];

        // launching getCapabilities request
        OpenLayers.Request.GET({
            url: serverBaseUrl,
            params: {
                SERVICE: "WFS",
                REQUEST: "GetCapabilities",
                //TODO voir si possible de passer en 1.1.1 pour exploiter les métadonnées
                VERSION: "1.0.0"
            },
            success: parseWFSCapabilities,
            failure: function(r) {
                Ext.Msg.alert('Ajout de couche WFS', 'Problème dans la communication avec le serveur.')
            }
        });
   
      }

            //document.getElementById("SELECT_SRS_WFS").options.length = 0;
    },

    AddWMTS_ListerCoucheFromServeur: function() {

      var fieldsetData = Ext.getCmp("fieldsetDataWMTS");
           
          //suppresion des champs recréés
      if(fieldsetData){
        Ext.getCmp("formWMTS").remove(fieldsetData);
      }  
      
      var loadXML = function (layers,tileMatrixSets,mapSRS){
        var result = new Array();
        for(var i=0; i<layers.length; i++){
          var layer = new Object();
          layer.id = layers[i].identifier;
          layer.text = layers[i].title;
          layer.styles = layers[i].styles;
          layer.formats = layers[i].formats;
          layer.serverURL = Ext.getCmp("INPUT_URL_WMTS").getValue();
          layer.name = layers[i].title;
                            
          var matrixSets = new Array();
          for(var j=0; j<layers[i].tileMatrixSetLinks.length;j++){
            var matrixSet = tileMatrixSets[layers[i].tileMatrixSetLinks[j].tileMatrixSet];
            var matrixSetSRS = matrixSet.supportedCRS;
            mapSRSSplit = mapSRS.split(new RegExp("EPSG:|EPSG:.*:","g"));
            matrixSetSRSSplit = matrixSetSRS.split(new RegExp("EPSG:.*:|EPSG:","g"));
            if(mapSRSSplit[mapSRSSplit.length-1] == matrixSetSRSSplit[matrixSetSRSSplit.length-1]){
              matrixSets.push(matrixSet);
            }
          }
          layer.matrix = matrixSets;
          if(layers[i].layers.length>0) {
            layer.children = loadXML(layers[i].layers);
            result.push(layer);
          }
          else {
            layer.leaf = true;
            if (layer.matrix.length>0)
              result.push(layer);
          }
        }
        return result;
      }
      
      
      var buildTreeWMTS = function(dataTree) {
          var me = this;
          var tree = new Ext.tree.TreePanel({
              animate:true,
              autoScroll:true,
              maxHeight: ((Ext.getBody().getViewSize().height)-(Ext.getCmp("window_mainajout").getHeight()))*0.5,
              height: 400,
              hideHeaders : true,
              id : "WMTS_TREE",
              root:{
                  expanded: true,
                  children: [dataTree]
              },
              columns: [{
                  xtype: 'treecolumn', //this is so we know which column will show the tree
                  text: 'Données',
                  width: 350,
                  sortable: false,
                  dataIndex: 'text'
              },
              {
                  text: 'Ajout',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Ajouter la couche dans la carte',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw falk-addlayer fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
					  var serverURL = record.data.serverURL;
                      var lLayerFormat = record.data.formats[0];
                      var lLayerStyle = "default";
                      var lLayerTileSet = record.data.matrix[0].identifier;
                      var lLayerMatrixIds = record.data.matrix[0].matrixIds; //check si tableau
                      var lLayerName = record.data.id;
                      var lLayerTitle = (record.data.text && record.data.text != "" ? record.data.text : record.data.name);
                      
                      // if (lLayerSRS != "" && lLayerBbox != "" && lLayerFormat != ""
                      // && lLayerName != "")
                      if (lLayerFormat == "") {
                        alert("Vous devez choisir un format pour ajouter cette couche");
                      }
                      else if (lLayerFormat != "" && lLayerName != "") {
                        Carmen.Control.AddOGC.addLayerWMTSToMap(lLayerTitle, serverURL, lLayerName, lLayerFormat, lLayerStyle, lLayerTileSet,lLayerMatrixIds);
                      } else
                        alert("La définition (issue du serveur WMTS) de la couche est incomplète.\nL'opération est abandonnée.");
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              }
              
              ],
              
              rootVisible: false,
              //enableDD: true,
              containerScroll: true
          });
          return tree;
      }
      
      
      var serveurWMTSURL = Ext.getCmp("INPUT_URL_WMTS");
      if (serveurWMTSURL)
      {
      
        serverUrl = serveurWMTSURL.value;// unescape(serveurWMTS.value);
        var lTabDesc = serverUrl.split("\|");
      
        var format = new OpenLayers.Format.WMTSCapabilities({
            /**
             * This particular service is not in compliance with the WMTS spec and
             * is providing coordinates in y, x order regardless of the CRS.  To
             * work around this, we can provide the format a table of CRS URN that
             * should be considered y, x order.  These will extend the defaults on
             * the format.
             */
            yx: {
                "urn:ogc:def:crs:EPSG::900913": true
            }
        });
        OpenLayers.ProxyHost = "/services/GetInformation/proxy.php?proxy_url=";
        OpenLayers.Request.GET({
            url: unescape(lTabDesc[0]),
            params: {
                SERVICE: "WMTS",
                VERSION: "1.0.0",
                REQUEST: "GetCapabilities"
            },
            success: function(request) {
              Ext.MessageBox.hide();
              var doc = request.responseXML;
              if (!doc || !doc.documentElement) {
                  doc = request.responseText;
              }
              var capabilities = format.read(doc);
              var layersList = capabilities.contents.layers;
              tileMatrixSets = capabilities.contents.tileMatrixSets;
              var data = new Object();
              data.children = loadXML(layersList,tileMatrixSets,app.map.projection );
              data.text = capabilities.serviceIdentification.title;
              var wmtsLayerTree = buildTreeWMTS(data);
              Ext.getCmp("formWMTS").add(
                {
                  xtype: 'fieldset',
                  id: 'fieldsetDataWMTS',
                  title: 'Choix des données',
                  items : [wmtsLayerTree]
                }
              );
            },
            failure: function() {
              Ext.MessageBox.hide();
              Ext.msg.alert("Echec de la récupération des couches");
            }
        });
      }
      
    },

    
    AddWMSC_ListerCoucheFromServeur : function() {
        var me = this;
        OpenLayers.ProxyHost = "/services/GetInformation/proxy.php?proxy_url=";

        var   version = '1.1.1';
        var buildWMSCTree = function(dataTree) {
            var tree = new Ext.tree.TreePanel({
              //loader: new Ext.tree.TreeLoader(),
              animate:true,
              autoScroll:true,
              maxHeight: ((Ext.getBody().getViewSize().height)-(Ext.getCmp("window_mainajout").getHeight()))*0.5,
              height: 400,
              hideHeaders : true,
              id : "WMSC_TREE",
              root:{
                  expanded: true,
                  children: [dataTree]
              },
              columns: [{
                  xtype: 'treecolumn', //this is so we know which column will show the tree
                  text: 'Données',
                  width: 350,
                  sortable: false,
                  dataIndex: 'text'
              },
              {
                  text: 'Ajout',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Ajouter la couche dans la carte',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw falk-addlayer fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                      var serverURL = record.data.serverURL;
                      // retrieving version number
                      var version = record.data.serverVersion;
                      var format = Ext.getCmp("SELECT_FORMAT_WMSC").getValue();
                      if (serverURL != "" && format!="" && version!="") {
                        Carmen.Control.AddOGC.addLayerWMSToMap(record.data.text, serverURL, record.data.name, Ext.getCmp("SELECT_FORMAT_WMSC").getValue(), app.map.projection, record.data.metadataUrl, version);
                      }
                      
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              },
              {
                  text: 'Aperçu',
                  xtype: 'actioncolumn',
                  width: 30,
                  sortable: false,
                  tooltip: 'Visualiser la donnée',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                    var serverURL = record.data.serverURL;
                    var lparamGetMap = "";
                    // retrieving version number
                    var version = record.data.serverVersion;
                    var format = Ext.getCmp("SELECT_FORMAT_WMSC").getValue();
                    
                    if (serverURL != "" && format!="" && version!="") {
                      
                        lparamGetMap = lparamGetMap + "&width=500";
                        lparamGetMap = lparamGetMap + "&height=500";
                        
                        lparamGetMap = lparamGetMap + "&format=" + Ext.getCmp("SELECT_FORMAT_WMSC").getValue();
                        lparamGetMap = lparamGetMap + "&layers=" + record.data.name;
                        lparamGetMap = lparamGetMap + "&bbox=" +  app.map.getExtent().left.toString() + ','+
                                                              +app.map.getExtent().bottom.toString() + ','+
                                                              +app.map.getExtent().right.toString() + ','+
                                                              +app.map.getExtent().top.toString()
                        lparamGetMap = lparamGetMap + "&styles=";
                        lparamGetMap = lparamGetMap + "&version=" + version;
                        lparamGetMap = lparamGetMap +
                                                    (version== "1.3.0" ?
                                                        "&CRS=" +  app.map.projection :
                                                        "&SRS=" +  app.map.projection);
                        var lUrl = '';
                        lUrl = serverURL;
                        if (serverURL.indexOf('?') != -1)
                          lUrl += "&service=wms&request=getmap";
                        else
                          lUrl += "?service=wms&request=getmap";
                        lUrl += lparamGetMap;
                        Carmen.Util.openWindowIframe("Prévisualisation", lUrl, false, 500,500);
                        
                    } else
                      alert("");
                   
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              },
              {
                  text: 'Métadonnée',
                  xtype: 'actioncolumn',
                  sortable: false,
                  width: 30,
                  tooltip: 'Visualiser la métadonnée associée',
                  defaultRenderer: Carmen.Control.AddOGC.buildActionRender('fa fa-fw fa-file-o fa-1x', 'color: #000; font-size: 16px;'),
                  //icon: '../simple-tasks/resources/images/edit_task.png',
                  handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
                      if (record.data.metadataUrl){
                        Carmen.Util.openWindowIframe("Métadonnée", record.data.metadataUrl, false, Math.min(document.body.clientWidth-100, 1220),Math.min(document.body.clientHeight-100, 790));
                      }
                  },
                  // Only leaf level tasks may be edited
                  isDisabled: function(view, rowIdx, colIdx, item, record) {
                      return !record.data.leaf;
                  }
              }],
              
              rootVisible: false,
              //enableDD: true,
              containerScroll: true
          });
          
            
            return tree;
        }
        
        
        var jsonLayersDescToExtTreeWMSC = function(jsonLayersDesc, jsonTileSetsDesc, projectionFilter) {
            var res = [];
            for(var i=0; i<jsonLayersDesc.length; i++){
                    var jsonLayerDesc = jsonLayersDesc[i];
                    var jsonTileSetDesc = jsonTileSetsDesc[i];
                    if(jsonTileSetDesc){
                      var layer = {};
                      layer.name = jsonLayerDesc.name;
                      layer.text = jsonLayerDesc.name;
                      layer.serverURL = Ext.getCmp("INPUT_URL_WMSC").getValue();
                      
                      //console.log(jsonLayerDesc);
                      layer.metadataUrl = '';
                      if (jsonLayerDesc.metadataURLs instanceof Array &&
                          jsonLayerDesc.metadataURLs.length>0) {
                          layer.metadataUrl = jsonLayerDesc.metadataURLs[0].href;
                          //console.log(jsonLayerDesc.metadataURLs[0]);
                      }
                      layer.srs = [];
                      layer.bboxes = [];
                      var desc = jsonTileSetDesc;
                      if (desc.srs) {
                              if (desc.srs instanceof Array &&
                                  desc.srs.length>0) {
                                  for (var j=0; j<desc.srs.length; j++) {
                                      layer.srs.push(desc.srs[j]);
                                  }
                              }
                              else {
                                  for (srsName in desc.srs) {
                                      if (desc.srs[srsName]==true)
                                          layer.srs.push(srsName);
                                          layer.bboxes[srsName] = desc.bbox[srsName].bbox;
                                  }
                              }
                      }
                      layer.tile_resolutions = desc.resolutions;
                      layer.tile_width = desc.width;
                      layer.tile_height = desc.height;
          
                      layer.styles = jsonLayerDesc.styles;
                      layer.formats = [desc.format];
                      //if(jsonLayerDesc.layers && jsonLayerDesc.layers.length>0)
                      //        layer.children = jsonLayersDescToExtTreeWMSC(jsonLayerDesc.layers);
                      //else
                      layer.leaf = true;
                      //console.log(layer);
          
                      // filtering layer that complies with map projection
                      filter = false; // should the layer be filtered
                      if (projectionFilter && projectionFilter!="") {
                          filter = true;
                          for (var j=0; j< layer.srs.length; j++) {
                              if (layer.srs[j]==projectionFilter) {
                                  filter = false;
                                  layer.bbox = layer.bboxes[layer.srs[j]];
                                  break;
                              }
                          }
                      }
                      if (!filter)
                          res.push(layer);
                    }
            }
            return res;
        }

        var parseWMSCCapabilities = function(response) {
            var wmsCpblt = new OpenLayers.Format.WMSCapabilities.v1_1_1_WMSC();
            var capabilities = wmsCpblt.read(response.responseText);
             Ext.MessageBox.hide();
            if (capabilities.capability && capabilities.capability.layers) {
                var jsonLayersDesc = capabilities.capability.layers;
                var jsonTileSetsDesc = capabilities.capability.vendorSpecific.tileSets;
                var layersDesc = jsonLayersDescToExtTreeWMSC(jsonLayersDesc, jsonTileSetsDesc, app.map.projection);
                var dataTree = {
                    text : capabilities.service.title,
                    children : layersDesc
                };
                var wmsLayerTree = buildWMSCTree(dataTree);
                //console.log(layersDesc);
                Ext.getCmp("formWMSC").add(
                  {
                    xtype: 'fieldset',
                    id: 'fieldsetDataWMSC',
                    title: 'Choix des données',
                    items : [wmsLayerTree]
                  }
                );
                var formatStore = [];
                for(var i=0; i< capabilities.capability.request.getmap.formats.length; i++){
                  var formatValue = capabilities.capability.request.getmap.formats[i];
                  formatStore.push([formatValue,formatValue]);
                }
                Ext.getCmp('fieldsetListWMSC').add(
                {
                      xtype: 'combo',
                      id: 'SELECT_FORMAT_WMSC',
                      fieldLabel: 'Format de la couche',
                      editable: false,
                      listConfig : { loadingText: 'Chargement' },
                      emptyText: 'Sélectionnez un format',
                      triggerAction: 'all',
                      minChars: 1,
                      queryDelay: 250,     
                      queryMode: 'local',
                      store: new Ext.data.SimpleStore({
                          fields: ['format', 'label'],
                          data : formatStore
                        }
                      ),
                      validator: function(v) {
                        return (v!='Sélectionnez un format');
                      },
                      displayField: 'label',
                      allowBlank: false,
                      valueField: 'format'
                });
                
                Ext.getCmp('SELECT_FORMAT_WMSC').setValue(formatStore[0][0]);
                
            }
            else {
                Ext.MessageBox.hide();
                Ext.Msg.alert("Ajout de couches WMSC",
                    "Le service ne semble pas répondre correctement. " +
                    "Il n'est peut être pas compatible avec la version de service demandée" +
                    " (" + version +").");
            }
            
        }
        
        var serveurWMSCURL = Ext.getCmp("INPUT_URL_WMSC");
        if (serveurWMSCURL) {
            // retrieving serverBaseUrl
            serverUrl = serveurWMSCURL.value;// unescape(serveurWMSC.value);
            var lTabDesc = serverUrl.split("\|");
            var serverBaseUrl = lTabDesc[0];

            // launching getCapabilities request
            OpenLayers.Request.GET({
                url: serverBaseUrl,
                params: {
                    SERVICE: "WMS",
                    REQUEST: "GetCapabilities",
                    VERSION: version
                },
                success: parseWMSCCapabilities,
                failure: function(r) {
                    Ext.Msg.alert('Ajout de couche WMSC', 'Problème dans la communication avec le serveur.')
                }
            });
        }

    },
    buildActionRender : function(cls, style) {
      return function(v, cellValues, record, rowIdx, colIdx, store, view) {
          var me = this,
              prefix = Ext.baseCSSPrefix,
              scope = me.origScope || me,
              items = me.items,
              len = items.length,
              i = 0,
              item, ret, disabled, tooltip;

          // Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
          // Assign a new variable here, since if we modify "v" it will also modify the arguments collection, meaning
          // we will pass an incorrect value to getClass/getTip
          ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || '' : '';

          cellValues.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';
          for (; i < len; i++) {
              item = items[i];

              disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record) : false);
              tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(item.scope || scope, arguments) : null));

              // Only process the item action setup once.
              if (!item.hasActionConfiguration) {

                  // Apply our documented default to all items
                  item.stopSelection = me.stopSelection;
                  item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
                  item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
                  item.hasActionConfiguration = true;
              }

              ret += '<i role="button" alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
                  '" class="' + me.actionIconCls + ' ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
                  (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (cls || me.iconCls || '')) + '"' +
                  (tooltip ? ' data-qtip="' + tooltip + '"' : '') + (disabled ? '' : ' style="'+style+'"') +  ' />';
          }
          return ret;
      };
    }
,
  CLASS_NAME :"Carmen.Control.AddOGC"
});


Carmen.Control.AddOGC.buildActionRender = function(cls, style) {
      return function(v, cellValues, record, rowIdx, colIdx, store, view) {
          var me = this,
              prefix = Ext.baseCSSPrefix,
              scope = me.origScope || me,
              items = me.items,
              len = items.length,
              i = 0,
              item, ret, disabled, tooltip;

          // Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
          // Assign a new variable here, since if we modify "v" it will also modify the arguments collection, meaning
          // we will pass an incorrect value to getClass/getTip
          ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || '' : '';

          cellValues.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';
          for (; i < len; i++) {
              item = items[i];

              disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record) : false);
              tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(item.scope || scope, arguments) : null));

              // Only process the item action setup once.
              if (!item.hasActionConfiguration) {

                  // Apply our documented default to all items
                  item.stopSelection = me.stopSelection;
                  item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
                  item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
                  item.hasActionConfiguration = true;
              }

              ret += '<i role="button" alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
                  '" class="' + me.actionIconCls + ' ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
                  (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (cls || me.iconCls || '')) + '"' +
                  (tooltip ? ' data-qtip="' + tooltip + '"' : '') + (disabled ? '' : ' style="'+style+'"') +  ' />';
          }
          return ret;
      };
    };

/**
 * hack for Openlayers.Util.getResolutionFromScale since factor doesn't match
 * 
 */
Carmen.Control.AddOGC.getResolutionFromScale =  function (scale, units) {
    var resolution;
    if (scale) {
        if (units == null) {
            units = "degrees";
        }
        var normScale = OpenLayers.Util.normalizeScale(scale);
        resolution = 1 / (normScale * OpenLayers.INCHES_PER_UNIT[units]
                                        * OpenLayers.DOTS_PER_INCH);        
        //Hack based on experimentations
        resolution = (156543.0339 / 147923.76911893254) * resolution; 
        
        
    }
    return resolution;
}
	

Carmen.Control.AddOGC.addLayerWMSToMap = function(lLayerTitle, lUrlValue, lLayerName, lLayerFormat, lLayerSRS, lLayerMetadataUrl, lServiceVersion) {
    
    var layer = new Carmen.Layer.WMSGroup(lLayerTitle, lUrlValue, {
        layers : lLayerName,
        version : lServiceVersion,
        format : lLayerFormat,
        transparent: true
      }, {
        
        singleTile : true,
        alwaysInRange : true,
        isBaseLayer : false,
        maxResolution : 'auto',
        projection : lLayerSRS

      });
    layer.setVisibility(true);
  
    // adding layer to the map
    app.map.addLayer(layer);

    // adding mdata to context
    var descs = Carmen.Util.OLWMSGroup2CarmenOWSDesc(
            layer,
            app.context.getNextLayerId(),
            { projection : app.map.projection }
    );

    // adding listeners...
    var layerManager = app.map.getLayerTreeManager();

    var layerIdx = app.context.addLayerDescs(descs);
    var layerTitle = lLayerTitle=="" ? lLayerName : lLayerTitle;
    // creating the node for the layerTreeManager
    var nodeCfg = {
        text : layerTitle,
        leaf : true,
        expanded : false,
        checked : true,
        hidden: true,
        icon: layer.getLegendGraphicURL(lLayerName),
        iconCls: 'cmnLayerTreeLayerWMSNodeIcon'
    };
   
    // inserting the node in the layer tree
    var tree = layerManager.getLayerTree();
    var node = tree.createNode(nodeCfg);
    node.attributes = {};
    if(lLayerMetadataUrl !="")
        node.attributes.metadata_url = lLayerMetadataUrl;
    node.attributes.type = Carmen.Control.LayerTreeManager.NODE_LAYER;
    
    
    node.attributes.OlLayer = layer;
    node.attributes.visible = true;
    node.attributes.expanded = false;
    node.attributes.layerIdx = layerIdx;
    node.attributes.opacity = 1.0;
    node.attributes.forceOpacityControl = true;
    node.attributes.addedByUser = true;
    node.attributes.layerAlias = layerTitle;
    node.attributes.layerName = layer.params.LAYERS;
    node.attributes.removable = true;
    node.attributes.iconDisplayStrategy = true;
    
    layerManager.addLayerNode(node, true);
};

	
Carmen.Control.AddOGC.addLayerWMSCToMap = function(lLayerTitle, lUrlValue, lLayerName, lLayerFormat, lLayerSRS, lLayerMetadataUrl, lServiceVersion, extraParams) {
    var layer = new Carmen.Layer.WMSGroup(lLayerTitle, lUrlValue, {
        layers : lLayerName,
        version : lServiceVersion,
        format : lLayerFormat,
        transparent: true
      }, {
        tileSize : new OpenLayers.Size(extraParams.tile_width,extraParams.tile_height),
        singleTile : false,
        alwaysInRange : true,
        isBaseLayer : false,
        maxResolution : 'auto',
        serverResolutions : extraParams.resolutions,
        resolutions : extraParams.resolutions,
        maxExtent : extraParams.bbox,
        projection : lLayerSRS

      });
    layer.setVisibility(true);
    // adding layer to the map
    app.map.addLayer(layer);

    // adding mdata to context
    var descs = Carmen.Util.OLWMSGroup2CarmenOWSDesc(
            layer,
            app.context.getNextLayerId(),
            { projection : app.map.projection }
    );

    // adding listeners...
    var layerManager = app.map.getLayerTreeManager();

    var layerIdx = app.context.addLayerDescs(descs);
    var layerTitle = lLayerTitle=="" ? lLayerName : lLayerTitle;
    // creating the node for the layerTreeManager
    var nodeCfg = {
        text : layerTitle,
        leaf : true,
        expanded : false,
        checked : true,
        icon : layer.getLegendGraphicURL(),
        iconCls : 'cmnLayerTreeLayerNodeIcon',//'cmnLayerTreeLayerWMSNodeIcon',
        rightIcon : '/IHM/images/delete.gif',
        rightIconCls : 'cmnTreeNodeRightIcon',
        iconDisplayStrategy : true
    };
    
    // inserting the node in the layer tree
    var tree = layerManager.getLayerTree();
    var node = tree.createNode(nodeCfg);
    node.attributes = {};
    if(lLayerMetadataUrl !="")
        node.attributes.metadata_url = lLayerMetadataUrl;
    node.attributes.type = Carmen.Control.LayerTreeManager.NODE_LAYER;
    node.attributes.OlLayer = layer;
    node.attributes.visible = true;
    node.attributes.expanded = false;
    node.attributes.layerIdx = layerIdx;
    node.attributes.opacity = 1.0;
    node.attributes.forceOpacityControl = true;
    node.attributes.addedByUser = true;
    node.attributes.layerAlias = layerTitle;
    node.attributes.layerName = layer.params.LAYERS;
    node.attributes.visible = true;
    node.attributes.removable   = true;
    if (tree.hasChildNodes()) {
        tree.insertBefore(node, tree.childNodes[0]);
    }
    else {
        tree.appendChild(node);
    }
    layerManager.initNode(node);
};

Carmen.Control.AddOGC.addLayerLocalDataToMap = function(lLayerName, lLayerMapfile, lLayerTitle, 
 lLayerLegendUrl, infoFields, briefFields, toolTipFields){
  var ctx = app.context;
  var layer = new Carmen.Layer.MapServerGroup(
    lLayerName, 
    window.CARMEN_URL_SERVER_DATA + "/cgi-bin/mapserv?map=" + lLayerMapfile,
    {
      LAYERS			: lLayerName,
      map_imagetype		: 'png',
      map_transparent	: true,
      map_localData		: lLayerTitle
    },
    {
      maxExtent			: ctx.getMaxExtent(),
      projection		: ctx.getProjection(),
      units				: ctx.getProjectionUnits(),
      isBaseLayer		: false,
      singleTile		: true,
      mapfile			: lLayerMapfile
    });
  layer.setVisibility(true);
  
  //Adding layer to map
  app.map.addLayer(layer);
  
  //Adding metadata to context
  var parsedLegendURL   = Carmen.Util.parseURL(lLayerLegendUrl);
  var parsedPathMapfile = Carmen.Util.parsePath(lLayerMapfile);
  var parsedMapFilename = parsedPathMapfile ? Carmen.Util.parseFilename(parsedPathMapfile.end) : null;
  var descs = Carmen.Util.OLLOCALDATACarmenOWSDesc(
	layer,
	app.context.getNextLayerId(),
	{
	  projection	: app.map.projection,
	  server		: parsedLegendURL ? parsedLegendURL.start : lLayerLegendUrl,
	  mapfile		: parsedMapFilename ? parsedMapFilename.basename : lLayerMapfile,
	  layerName		: lLayerName,
	  layerTitle	: lLayerTitle,
	  infoFields	: infoFields,
	  briefFields	: briefFields,
	  tootlTipFields: toolTipFields
	}
  );
  
  //Adding listeners
  var layerManager = app.map.getLayerTreeManager();
  var layerIdx = app.context.addLayerDescs(descs);
  //Creating node for layerTreeManager
  var nodeCfg = {
    text				: lLayerTitle,
    leaf				: true,
    expanded			: false,
    checked				: true,
    icon				: "/IHM/IHM/cartes/LEGEND/"+lLayerName+"_legend.jpg",
    iconCls				: 'x-tree-node-inline-icon cmnLayerTreeLayerNodeIcon',
    rightIcon			: '/IHM/images/delete.gif',
    rightIconCls		: 'cmnTreeNodeRightIcon',
    iconDisplayStrategy	: true,
  };
  
  // inserting the node in the layer tree
  var tree = layerManager.getLayerTree();
  var node = tree.createNode(nodeCfg);
  node.attributes = {};
  node.attributes.isLocalData			= true;
  node.attributes.mapfilePath			= lLayerMapfile;
  node.attributes.type 			 		= Carmen.Control.LayerTreeManager.NODE_LAYER;
  node.attributes.OlLayer 		 		= layer;
  node.attributes.visible 		 		= true;
  node.attributes.expanded		 		= false;
  node.attributes.layerIdx		 		= layerIdx;
  node.attributes.opacity		 		= 1.0;
  node.attributes.forceOpacityControl	= true;
  node.attributes.addedByUser			= true;
  node.attributes.infoFields			= infoFields;
  node.attributes.briefFields			= briefFields;
  node.attributes.toolTipFields			= toolTipFields;
  node.attributes.baseQueryURL			= "";
  node.attributes.layerType				= "mapfile_localdata";
  node.attributes.layerName = layer.params.LAYERS;
  node.attributes.visible   = true;
  node.attributes.removable   = true;
  node.attributes.stylable   = true;
  if (tree.hasChildNodes()) {
      tree.insertBefore(node, tree.childNodes[0]);
  }
  else {
      tree.appendChild(node);
  }
  layerManager.initNode(node);
};

Carmen.Control.AddOGC.addLayerWFSToMap = function(lLayerName, lLayerMapfile, lLayerTitle, lLayerLegendUrl, 
 infoFields, briefFields, toolTipFields, lLayerDownloadUrl, lLayerMetadataUrl) {
  //console.log("add layer to map wfs");
  var ctx = app.context;
  var layer = new Carmen.Layer.MapServerGroup(
      lLayerName,
      window.CARMEN_URL_SERVER_DATA + "/cgi-bin/mapserv?map=" + lLayerMapfile,
      {
        LAYERS : lLayerName,
        map_imagetype: 'png',
        map_transparent: true
      },
      {
        maxExtent: ctx.getMaxExtent(),
        projection: ctx.getProjection(),
        units: ctx.getProjectionUnits(),
        isBaseLayer: false,
        singleTile: true,
        mapfile : lLayerMapfile
      });
  layer.setVisibility(true);

  // adding layer to the map
  app.map.addLayer(layer);

  // adding mdata to context
  var parsedLegendURL = Carmen.Util.parseURL(lLayerLegendUrl);
  var parsedPathMapfile = Carmen.Util.parsePath(lLayerMapfile);
  var parsedMapFilename = parsedPathMapfile ? Carmen.Util.parseFilename(parsedPathMapfile.end) : null ;
  var descs = Carmen.Util.OLWFSCarmenOWSDesc(
                layer,
                app.context.getNextLayerId(),
                {
                  projection : app.map.projection ,
                  server : parsedLegendURL ? parsedLegendURL.start : lLayerLegendUrl,
                  mapfile :  parsedMapFilename ? parsedMapFilename.basename : lLayerMapfile,
                  layerName : lLayerName,
                  layerTitle : lLayerTitle,
                  infoFields : infoFields,
                  briefFields : briefFields,
                  toolTipFields : toolTipFields
                }
  );

    // adding listeners...
  var layerManager = app.map.getLayerTreeManager();

  var layerIdx = app.context.addLayerDescs(descs);

  // creating the node for the layerTreeManager
  var nodeCfg = {
    text : lLayerTitle,
    leaf : true,
    expanded : false,
    checked : true,
    icon : lLayerLegendUrl,
    iconCls : 'cmnLayerTreeLayerNodeIcon',
    rightIcon : '/IHM/images/delete.gif',
    rightIconCls : 'cmnTreeNodeRightIcon',
    iconDisplayStrategy : true,
  };
  
  // inserting the node in the layer tree
  var tree = layerManager.getLayerTree();
  var node = tree.createNode(nodeCfg);
  node.attributes = {};
  if(lLayerDownloadUrl !="")
      node.attributes.download_url = lLayerDownloadUrl;
  if(lLayerMetadataUrl !="")
      node.attributes.metadata_url = lLayerMetadataUrl;
  node.attributes.type = Carmen.Control.LayerTreeManager.NODE_LAYER;
  node.attributes.OlLayer = layer;
  node.attributes.visible = true;
  node.attributes.expanded = false;
  node.attributes.layerIdx = layerIdx;
  node.attributes.opacity = 1.0;
  node.attributes.forceOpacityControl = true;
  node.attributes.addedByUser = true;
  // mandatory setting for querying the layer
  node.attributes.infoFields = infoFields;
  node.attributes.briefFields = briefFields;
  node.attributes.toolTipFields = toolTipFields;
  node.attributes.baseQueryURL = "";
  node.attributes.layerType = "mapfile_wfs";
  node.attributes.layerName = layer.params.LAYERS;
  node.attributes.visible = true;
  node.attributes.removable = true;

  if (tree.hasChildNodes()) {
      tree.insertBefore(node, tree.childNodes[0]);
  }
  else {
      tree.appendChild(node);
  }
  layerManager.initNode(node);
};
		
		
		
Carmen.Control.AddOGC.addLayerWMTSToMap = function(lLayerTitle, lUrlValue, lLayerName, lLayerFormat, lLayerStyle, lLayerTileSet,lLayerMatrixIds) {
  var resolutions = [];
  for(var i = 0; i < lLayerMatrixIds.length; i++) {
          resolutions.push(this.getResolutionFromScale(lLayerMatrixIds[i].scaleDenominator, 'm'));

  }
  var topLeftCorner = lLayerMatrixIds[0].topLeftCorner;
  var layer = new Carmen.Layer.WMTSGroup({
        name: lLayerTitle,
        url: lUrlValue,
        layer: lLayerName,
        matrixSet: lLayerTileSet,
        matrixIds: lLayerMatrixIds,
        format: lLayerFormat,
        style: lLayerStyle,
        tileOrigin: topLeftCorner,
        opacity: 1,
        //maxResolution : 'auto',
        isBaseLayer: false,
//try to get extent
        resolutions : resolutions,
        serverResolutions : resolutions
      });

  layer.setVisibility(true);
  app.map.updateSize();
  // adding layer to the map
  app.map.addLayer(layer);
  layer.redraw();
  app.map.updateSize();
  // adding mdata to context
        var descs = Carmen.Util.OLWMTSGroup2CarmenOWSDesc(
                layer,
                app.context.getNextLayerId(),
                { projection : app.map.projection }
  );

  // adding listeners...
  var layerManager = app.map.getLayerTreeManager();

  var layerIdx = app.context.addLayerDescs(descs);
  var layerTitle = lLayerTitle=="" ? lLayerName : lLayerTitle;
  // creating the node for the layerTreeManager
  var nodeCfg = {
    text : layerTitle,
    leaf : true,
    expanded : false,
    checked : true,
    icon : window.CARMEN_URL_SERVER_FRONT+'/IHM/images/NoTreeIcon.gif',
    iconCls : 'cmnLayerTreeLayerNodeIcon',
    iconDisplayStrategy : true,
  };
  
  // inserting the node in the layer tree
  var tree = layerManager.getLayerTree();
  var node = tree.createNode(nodeCfg);
  node.attributes = {};
  node.attributes.type = Carmen.Control.LayerTreeManager.NODE_LAYER;
  node.attributes.OlLayer = layer;
  node.attributes.visible = true;
  node.attributes.expanded = false;
  node.attributes.layerIdx = layerIdx;
  node.attributes.opacity = 1.0;
  node.attributes.forceOpacityControl = true;
  node.attributes.addedByUser = true;
  node.attributes.layerAlias = layerTitle;
  node.attributes.layerName = layer.params.LAYERS;
  node.attributes.visible = true;
  if (tree.hasChildNodes()) {
      tree.insertBefore(node, tree.childNodes[0]);
  }
  else {
      tree.appendChild(node);
  }
  layerManager.initNode(node);
};


/**
 * Encode la valeur en base64
 * @param str
 * @return
 */
function encodeValue(str){
  return encode64((str));
}

var keyStr = "ABCDEFGHIJKLMNOP" +
                "QRSTUVWXYZabcdef" +
                "ghijklmnopqrstuv" +
                "wxyz0123456789+/" +
                "=";
/**
 * base64_encode js equivalent 
 * @param input
 * @return
 */  
function encode64(input) {
  input = escape(input);
  var output = "";
  var chr1, chr2, chr3 = "";
  var enc1, enc2, enc3, enc4 = "";
  var i = 0;
  do {
     chr1 = input.charCodeAt(i++);
     chr2 = input.charCodeAt(i++);
     chr3 = input.charCodeAt(i++);
  
     enc1 = chr1 >> 2;
     enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
     enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
     enc4 = chr3 & 63;
  
     if (isNaN(chr2)) {
        enc3 = enc4 = 64;
     } else if (isNaN(chr3)) {
        enc4 = 64;
     }
  
     output = output +
        keyStr.charAt(enc1) +
        keyStr.charAt(enc2) +
        keyStr.charAt(enc3) +
        keyStr.charAt(enc4);
     chr1 = chr2 = chr3 = "";
     enc1 = enc2 = enc3 = enc4 = "";
 } while (i < input.length);
 return output;
}

