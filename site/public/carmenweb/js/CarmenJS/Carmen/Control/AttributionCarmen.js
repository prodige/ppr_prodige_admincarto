
/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: OpenLayers.Control.AttributionCarmen
 * Add attribution from layers to the map display. Uses 'attribution' property
 * of each layer.
 *
 * Inherits from:
 *  - <OpenLayers.Control>
 */
 //TODO: put in Carmen namespace...
OpenLayers.Control.AttributionCarmen = new OpenLayers.Class(OpenLayers.Control, {
    
    /**
     * APIProperty: seperator
     * {String} String used to seperate layers.
     */
    separator: ", ",
    displayLogo : true,
    logoPath : null,   
   
// temp   
		font : null,
    fontFamily : 'tahoma,arial,helvetica,sans-serif',
    color : '#000000',
    fontSize : '12px',
    fontWeight : 'normal',
    fontStyle : 'normal',
    backgroundColor :  null,
    opacity : 1.0,
// temp end       


    initialize: function(options) {
        //console.log('here a1');
        
        //Carmen.Control.Inlay.prototype.initialize.apply(this, arguments);
//temp        
        OpenLayers.Control.prototype.initialize.apply(this, arguments);
        
        this.fontFamily = this.font ? this.font + ',' + this.fontFamily : this.fontFamily;
        if (this.fontSize && this.fontSize.search(/^\d+$/g)!=-1)
        	this.fontSize = this.fontSize + 'px';   
        if (this.color && this.color.charAt(0)!='#') 
        	this.color = '#' + this.color;
        if (this.backgroundColor && this.backgroundColor.charAt(0)!='#')
        	this.backgroundColor = '#' + this.backgroundColor;
        // alpha in bgcolor desc ?
        if (this.backgroundColor && this.backgroundColor.length==9) {
        	this.opacity = 1.0 - (parseInt(this.backgroundColor.substring(7,9),16) / 255);
        	this.backgroundColor = this.backgroundColor.substring(0,7);
        }
// temp end        
		},
    
    
    /** 
     * Method: destroy
     * Destroy control.
     */
    destroy: function() {
        this.map.events.un({
            "removelayer": this.updateAttribution,
            "addlayer": this.updateAttribution,
            "changelayer": this.updateAttribution,
            "changebaselayer": this.updateAttribution,
            scope: this
        });
        
        Carmen.Control.Inlay.prototype.destroy.apply(this, arguments);
    },    
    

    /**
     * Method: draw
     * Initialize control.
     * 
     * Returns: 
     * {DOMElement} A reference to the DIV DOMElement containing the control
     */    
    draw: function() {
        //Carmen.Control.Inlay.prototype.draw.apply(this, arguments);
// temp
	OpenLayers.Control.prototype.draw.apply(this, arguments);
			this.div.style.display = "block";
			this.div.style.position = "absolute";
			this.div.style.backgroundColor = this.backgroundColor;
			this.div.style.fontFamily = this.fontFamily;
			this.div.style.fontWeight = this.fontWeight;
			this.div.style.color = this.color;
			this.div.style.fontSize = this.fontSize;
			this.div.style.fontStyle = this.fontStyle;
			this.div.style.textAlign = 'center';
      this.div.id = "AttributionCarmenElt";
			OpenLayers.Util.modifyDOMElement(this.div, null, null, null,
			                                 null, null, null, this.opacity);
//			this.update();
    
// temp end        
        if (!this.logo && this.displayLogo && this.logoPath) {
          this.logo = document.createElement("img");
					this.logo.style.verticalAlign = 'top';
					this.logo.src = this.logoPath;
          this.logo.onload = function (){
            document.getElementById("AttributionCarmenElt").style.width = this.clientWidth+30+"px";
          };
          this.div.appendChild(this.logo);
        }    
				if (!this.txt) {
          this.txt = document.createElement("span");
					this.txt.style.verticalAlign = 'bottom';
					this.txt.style.marginRight = '10px';
					this.txt.style.display = 'block';
          this.div.appendChild(this.txt);		      
        }
        
        this.map.events.on({
            'changebaselayer': this.update,
            'changelayer': this.update,
            'addlayer': this.update,
            'removelayer': this.update,
            scope: this
        });
        this.update();

        return this.div;    
    },

    /**
     * Method: updateAttribution
     * Update attribution string.
     */
    update: function() {
        
        var attributions = [];
        if (this.map && this.map.layers) {
            for(var i=0, len=this.map.layers.length; i<len; i++) {
                var layer = this.map.layers[i];
                if (layer.attribution && layer.getVisibility()) {
                    attributions.push( layer.attribution );
                }
            }  
            this.txt.innerHTML = attributions.join(this.separator);
        }
    },

    CLASS_NAME: "OpenLayers.Control.AttributionCarmen"
});
