  /**
 * @_requires OpenLayers/Controljs
 */

/**
 * Class: Carmen.Control.TootlTip
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */

Carmen.Control.ToolTip = new OpenLayers.Class(OpenLayers.Control, {
	btn : new Ext.Button({
    tooltip: 'Infobulles',      
    tooltipType: 'qtip',
    cls: 'x-btn-icon', 
    enableToggle: true,
    toggleGroup: 'mainMapControl',
    disabled: true,
    text : '<i class="fa fa-comment fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  }),
  
  toolTipPanel: new Ext.form.FormPanel({
	    id : 'cmnToolTipPanel',
	    border: true,
	    margins: '5 5 5 5',
			frame:true,
	    floating: true,
	    hidden: true
	  }),
  getButton : function(){return this.btn},

		timer : null,
		
		mapfile : null,
		serviceUrl : null,
		
		windowTitle : "Outil Infobulle",
		
		showGraphicalSelection : false,
    
    /** 
     * Property: element
     * {DOMElement} 
     */
    element: null,
    
    /** 
     * APIProperty: prefix
     * {String}
     */
    prefix: '',
    
    /** 
     * APIProperty: separator
     * {String}
     */
    separator: ', ',
    
    /** 
     * APIProperty: suffix
     * {String}
     */
    suffix: '',
    
    /** 
     * APIProperty: numDigits
     * {Integer}
     */
    numDigits: 5,
    
    /** 
     * APIProperty: granularity
     * {Integer} 
     */
    granularity: 10,
    
    /** 
     * Property: lastXy
     * {<OpenLayers.LonLat>}
     */
    lastXy: null,

    /**
     * APIProperty: displayProjection
     * {<OpenLayers.Projection>} A projection that the 
     * mousecontrol will display.
     */
    displayProjection: null, 
    
    /**
     * Property: onMap
     * {Boolean} Whether the mouse is on the map or not
     */
    onMap : false,
    
    
    /**
     * Property: pendingRequests
     * {Array of Ajax request id } Array of Ajax request ids 
     * that have been launched by the tooltip control
     */
    pendingRequests : [],
    
	initialize: function(serviceUrl, mapfile, options) {
		OpenLayers.Control.prototype.initialize.apply(this, arguments);
		this.mapfile = mapfile;
		this.serviceurl = serviceUrl;
	},

    /**
     * Method: destroy
     */
     destroy: function() {
         if (this.map) {
             this.map.events.unregister('mousemove', this, this.redraw);
         }
         OpenLayers.Control.prototype.destroy.apply(this, arguments);
     },

    /**
     * Method: draw
     * {DOMElement}
     */    
    draw: function() {
        OpenLayers.Control.prototype.draw.apply(this, arguments);

        if (!this.element) {
            this.div.left = "";
            this.div.top = "";
            this.element = this.div;
        }
        
        this.redraw();
        return this.div;
    },
   
    /**
     * Method: redraw  
     */
    redraw: function(evt) {
        this.toolTipPanel.hide();
        if (evt!=null) {
  	      //console.log(evt.xy);
  	      var control = this;
  	      if (this.timer)
  	      	clearTimeout(this.timer);
  	      this.timer = setTimeout(
  	      	function() {
  	      	  if (control.onMap) 
  	      		  control.updateToolTip(evt.xy)
  	      	}, 
  	      	Carmen.Control.ToolTip.DELAY);
  			}
	  },

   
   	updateToolTip : function(pos) {
   		this.toolTipPanel.setPosition(pos.x, pos.y);

			this.layerTree = this.map.getLayerTreeManager().getLayerTree().clone();
			this.layerTree.filter(this.filterToolTipNode);
			var isLayerNode = function(n) { 
				return ('type' in n.attributes && (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER)); 
				}; 
			var nodes = this.layerTree.collectChildNodes(isLayerNode); 

      var ms_nodes = {};
      for (var i=0;i<nodes.length; i++) {
        var mapfile = nodes[i].attributes.OlLayer.mapfile;
				  if (mapfile) {
				   if (!ms_nodes[mapfile])
				     ms_nodes[mapfile] = [];
				   ms_nodes[mapfile].push(nodes[i]);  
				  }
      }
      
      // cancel all pending requests
      for (var i=0; i< this.pendingRequests.length; i++) {
		Ext.Ajax.abort(this.pendingRequests[i]);
	  }
	  this.alias = []; this.data = [];
	  this.queriesSent = 0;

      // launching requests
	  for (var m in ms_nodes) {
  		if (ms_nodes[m].length>0) 
  			this.performQuery(this.buildParams(ms_nodes[m], pos, null, m));
	  }
   	},
  
  registerResults : function(alias, data) {
	  this.queriesSent--;
	  if (alias!=null && data!=null) {
	    this.alias.push(alias);
	    this.data.push(data);
	  }
    if (this.queriesSent<=0 && this.alias.length>0) {
      this.updateToolTipPanel(this.alias[0], this.data[0]);
      this.alias = []; this.data = []; 
    }
   },  
  
  updateToolTipPanel : function(fields, data) {
  	var bdy = this.toolTipPanel.body.dom; 
  	// Removing previous content
  	bdy.innerHTML = '';
  	bdy.style.overflow = "auto";
    // Resizing panel to max width 
  	var panelWidth = 250; 
  	if (this.map)
  	  panelWidth = this.map.app.ui.getMainPanel().getSize().width;
    this.toolTipPanel.setWidth(panelWidth);
  	
  	var anchor = document.createElement('a');
  	// building 
  	for (var i=0; i<data.length; i++) {
  		var list = document.createElement("ul"); 
  		
  		for (var j=0; j<fields.length; j++) {
  			var line = document.createElement("li");
  			var span_field = document.createElement("span");
  			// TODO : see why don't work with css
  			//span_field.className = 'cmnToolTipField';
  			span_field.style.fontWeight = 'bold';
  			var span_data = document.createElement("span");
  			span_data.className = 'cmnToolTipData';
  			span_field.innerHTML = fields[j] + ' : '; 
  			span_data.innerHTML = data[i][j];
  			line.appendChild(span_field);
  			line.appendChild(span_data);
  			list.appendChild(line);
  		}
  		bdy.appendChild(list);
  	}
		
		bdy.appendChild(anchor);
		this.toolTipPanel.render();
		
		// looking for line max width
		lineWidth = 0;
		for (var i=0; i<bdy.childNodes.length-1; i++) {
  		list = bdy.childNodes[i];
  		for (var j=0; j<list.childNodes.length; j++) {
  		  lineWidth = Math.max(lineWidth, 
  		    Ext.fly(list.childNodes[j].childNodes[0]).getWidth() + Ext.fly(list.childNodes[j].childNodes[1]).getWidth());
  		}
		}
		
		var width = lineWidth + 30; 
		var height = Ext.fly(list).getHeight() * data.length + 15; 
		
		this.toolTipPanel.setWidth(width);
		this.toolTipPanel.setHeight(height);
		var mapPanel = this.map.app.ui.getMainPanel();
		var mousePosX = this.toolTipPanel.getPosition(true)[0];
		var mousePosY = this.toolTipPanel.getPosition(true)[1];
		var posX = (mousePosX - width) < 0 ? mousePosX : mousePosX - width;
		var posY = (mousePosY - height) < 0 ? mousePosY : mousePosY - height;
		
		posX = Math.max(posX, 0);
		posY = Math.max(posY, 0);
		
    // re-adjusting height and width if tooltip going outside the panel
		var mapPanelHeight = mapPanel.getEl().getHeight(true);
		var mapPanelWidth = mapPanel.getEl().getWidth(true);
		var readjustH = height> (mapPanelHeight-posY);
		height = readjustH ? mapPanelHeight-posY : height;
		var readjustW = width> (mapPanelWidth-posX);
		width = readjustW ? mapPanelWidth-posX : width;
		
		this.toolTipPanel.setHeight(height);
		this.toolTipPanel.setWidth(width);
		
		this.toolTipPanel.setPosition(posX+5, posY-5);
		
		this.toolTipPanel.show();

		if (readjustH || readjustW) {
		  Ext.fly(bdy).focus();
		}
	}, 
   

	fieldFilter_TXT : function(field) {
    return field.type=='TXT';
  },

  buildParams : function(nodes, position, fieldsFilter, mapfile) {
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];
    
    for (var i=0; i<nodes.length; i++) {
		layerNames.push(nodes[i].attributes.layerName);
	    fieldsList.push(nodes[i].attributes.toolTipFields);
	    baseQueryURLList.push(nodes[i].attributes.baseQueryURL);
    }
    
    var fieldsDesc = new Array();
    for (var i=0; i<fieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
      fieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        // forcing field output to TXT in tooltip
        desc.type='TXT';
        fieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
        fieldsDesc[i] = Carmen.Util.array_filter(fieldsDesc[i], fieldsFilter);
    }
 
    // linking kept info fields and brief fields 
    // to the corresponding layer node in the tree
    for (var i=0; i<nodes.length; i++) {
   	  nodes[i].attributes.infoFieldsDesc = fieldsDesc[i];
    }

   var params = layerNames.length >0 ? {
      layers : Ext.util.JSON.encode(layerNames),
      fields : Ext.util.JSON.encode(fieldsDesc),
      briefFields : Ext.util.JSON.encode(fieldsDesc),
      shape : Carmen.Util.positionToStr(this.positionToBounds(position)),
      map : mapfile,
      showGraphicalSelection : this.showGraphicalSelection } : null;
    return params;    
  },

  // traditionnal query on mapserver layer
  performQuery : function (queryParams) {
    
    // prepraring answer callback
    var received = function (response) {
      response = Ext.decode(response.responseText);
	  	
	  	// retrieving info from top queried layer 
	  	var fields = null; var data = null;
	  	var lname = '';
	  	for (l in response.results) {
	  		fields = response.results[l].fields
	  		data = response.results[l].data
	  		lname = l;
	  		if (data.length>0)
	  		  break; // only taking results form first layer...
	  	}
	  	
	  	if (data && data.length>0) {
	  	  // retrieving alias from fieldnames...
	  	  var node = this.layerTree.findChildByAttribute('layerName', lname);
	  	  var alias = [].concat(fields);
	  	  var infoFieldsDesc = node.attributes.infoFieldsDesc;
	  	  for (var i=0; i<alias.length; i++) {
	  	    for (var j=0; j<infoFieldsDesc.length; j++) {
	  	      infoFieldsDesc[j]
  	  	    if (infoFieldsDesc[j].name == alias[i])
  	  	      alias[i] = infoFieldsDesc[j].alias;
	  	    }
	  	  }
	  	}
	  	this.registerResults(alias, data);
    }
    
    // launching the request
    this.queriesSent++;
    
    var url = '/services/GetInformation/index.php';
    //console.log(url);
    //console.log(queryParams);
    var requestId = Ext.Ajax.request({
      url: url,
      success: received,
      failure: function (response) { 
         Carmen.Util.handleAjaxFailure(response, this.windowTitle, true);
         this.registerResults(null, null); 
      },
      headers: {
        'my-header': 'foo'
      },
      scope : this,
      params: queryParams
    });
    this.pendingRequests.push(requestId);
  }, 

  positionToBounds : function(position) {
		var bounds = null;
  	if (position instanceof OpenLayers.Bounds) {
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.left, position.bottom));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.right, position.top));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    } 
    else { // it's a pixel
      // if no tolerance
      // this.showInfoWindow(this.map.getLonLatFromPixel(position));
  
      // with tolerance
      var tol = Carmen.Util.INFO_TOOL_XY_TOLERANCE;
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x-tol, position.y-tol));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x+tol, position.y+tol));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    }
	return bounds;
  },
   
   
   
    /**
     * Method: formatOutput
     * Overrides to provide custom display output
     *
     * Parameters:
     * lonLat - {<OpenLayers.LonLat>} Location to display
     */
    formatOutput: function(lonLat) {
        var digits = parseInt(this.numDigits);
        var newHtml =
            this.prefix +
            lonLat.lon.toFixed(digits) +
            this.separator + 
            lonLat.lat.toFixed(digits) +
            this.suffix;
        return newHtml;
     },


		filterToolTipNode : function(node) {
			var res =true;

	  	res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       && (node.attributes.toolTipFields && node.attributes.toolTipFields.length>0)  
      ));
			return res;
		},




    /** 
     * Method: setMap
     */
    setMap: function() {
		  OpenLayers.Control.prototype.setMap.apply(this, arguments);
		  
/*TODO REMOVE LINE*/return;

		  this.activate();
		  var mainPanel = this.map.app.ui.getMainPanel();
		  mainPanel.add(this.toolTipPanel);
		  
		  // deactivating tooltip display when outside the map
		  control = this;
		  mainPanel.body.on('mouseout', 
		    function() { control.onMap = false; });
      mainPanel.body.on('mouseover', 
        function() { control.onMap = true; });
          
		  this.map.app.ui.doLayout();
    },    

  /**
   * Method: activate 
   */
  activate : function() {
/*TODO REMOVE LINE*/return;
	  var state = OpenLayers.Control.prototype.activate.apply(this, arguments);
    if (state) {
      this.map.events.register( 'mousemove', this, this.redraw);
    }
    return state;
  },

  /**
   * Method: deactivate
   */  
  deactivate : function() {
/*TODO REMOVE LINE*/return;
    var state = OpenLayers.Control.prototype.deactivate.apply(this, arguments);
    if (state) {
      // removing visible panel
      if (this.toolTipPanel.rendered)
        this.toolTipPanel.hide();
      // removing listeners
      if (this.map) 
        this.map.events.unregister('mousemove', this, this.redraw);
    }
    return state;
  },
  
  CLASS_NAME: "Carmen.Control.ToolTip"

});

// ToolTip DELAY in ms
Carmen.Control.ToolTip.DELAY = 1000;
