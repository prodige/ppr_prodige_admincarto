/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control.AddWMS
 * 
 * Inherits from: - <OpenLayers.Control>
 */

Carmen.Control.AddWMS = new OpenLayers.Class(OpenLayers.Control,
{
  // UI components
  btn :new Ext.Button( {
    //tooltip: 'Add WMS Layer',
    tooltip: 'Ajouter une couche WMS',
    tooltipType: 'title',
    cls: 'x-btn-icon cmn-tool-addwms',
    enableToggle: true,
		toggleGroup: 'mainMapControl',
    disabled: false
  }),
  
  win : null,

  setMap: function(map) {
      OpenLayers.Control.prototype.setMap.apply(this, arguments);

      this.btn.addListener('toggle', 
	      Carmen.Util.buildExt2olHandlerToggle(this));    
      var tb = this.map.app.ui.getTopToolBar();
      //tb.addItem(this.btn);
      
      this.map.app.ui.doLayout();
  },
  
  activate: function() {
  	OpenLayers.Control.prototype.activate.apply(this, arguments);
    this.map.toolTipDeactivate();
    this.showWindow();
  },

  deactivate: function() {
  	OpenLayers.Control.prototype.deactivate.apply(this, arguments);
  	if (this.win && this.win.rendered)
      this.win.hide();
    this.map.toolTipActivate();
  },
    
  showWindow : function() {
    this.win = new Ext.Window({
      layout : 'fit',
      width:550,
      constrain : true,
      plain : true,
      title: "Ajout de couches WMS",
      // shim :false,
      modal : false,
      x :150,
      y :100,
      autoDestroy : true,
      // monitorValid:true,
      resizable : true,
      autoLoad : {
        url : '/services/AddWMS/addWMS.php',
        loadingText: 'Chargement',
        params : {
          mapProjection : this.map.projection,
          extent : app.map.getExtent().left.toString() + ','
          +app.map.getExtent().bottom.toString() + ','
          +app.map.getExtent().right.toString() + ','
          +app.map.getExtent().top.toString()
          },
        method : 'GET',
        scripts : true
      }
    });
    
    // adding listeners to link window closing and control deactivation   
    var control = this;
    this.win.on(
      'hide',
      function(w) { control.btn.toggle(false); },
      this.win);
    
   this.win.show();
  },

  CLASS_NAME :"Carmen.Control.AddWMS"
});

Carmen.Control.AddWMS.addLayerToMap = function(lLayerTitle, lUrlValue, lLayerName, lLayerFormat, lLayerSRS, lServiceVersion) {

  //lUrlValue = 'http://mapcarmen.brgm.fr/cgi-bin/mapserv?map=/applications/mapserver/mapFiles/scan.map';
  var layer = new Carmen.Layer.WMSGroup(lLayerTitle, lUrlValue, {
        layers : escape(lLayerName),
        version : "1.1.1",
        format : lLayerFormat,
        transparent: true
      }, {
        //tileSize : new OpenLayers.Size(1024,1024),
        singleTile : true,
        alwaysInRange : true,
        isBaseLayer : false,
        maxResolution : 'auto',
        projection : lLayerSRS

      });
  layer.setVisibility(true);
  
  // adding layer to the map 
  app.map.addLayer(layer);
  
  // adding mdata to context
	var descs = Carmen.Util.OLWMSGroup2CarmenOWSDesc(
	 	layer,
	 	app.context.getNextLayerId(),
	 	{ projection : app.map.projection }
  );
  
  // adding listeners...
  var layerManager = app.map.getLayerTreeManager();
  
  var layerIdx = app.context.addLayerDescs(descs);
  var layerTitle = lLayerTitle=="" ? lLayerName : lLayerTitle;
  // creating the node for the layerTreeManager
  var node = new Ext.tree.TreeNode({
    text : layerTitle,
    leaf : true,
    expanded : false,
    checked : true,
    icon : layer.getLegendGraphicURL(),
    iconCls : 'cmnLayerTreeLayerNodeIcon',//'cmnLayerTreeLayerWMSNodeIcon',
    rightIcon : '/IHM/images/delete.gif',
    rightIconCls : 'cmnTreeNodeRightIcon',
    iconDisplayStrategy : true,
//    uiProvider : Carmen.TreeNodeUI,
    listeners: {
        // drag'n drop
        'beforemove' : {
            fn: function(tree, node , oldParent, newParent, index) { 
                  this.moveLayerNode(tree, node , oldParent, newParent, index);
            },
            scope: layerManager,
            delay: 100
          }
      }
  });
  
  node.attributes.type = Carmen.Control.LayerTreeManager.NODE_LAYER;
  node.attributes.OlLayer = layer;
  node.attributes.visible = true;
  node.attributes.expanded = false;
  node.attributes.layerIdx = layerIdx;
  node.attributes.opacity = 1.0;
  node.attributes.forceOpacityControl = true;
  node.attributes.addedByUser = true;
  // bottom elt for the slider...
  node.attributes.hasBottomElt = true; 
  node.attributes.layerAlias = layerTitle;
  
 
  //  node.addListener('checkchange', this.onNodeCheck, node);
  node.addListener('checkchange', 
      function(n, cb_state) {
        var layer = n.attributes.OlLayer;
        layer.setVisibility(n.attributes.checked);
      }, 
    node); 
  
  // inserting the node in the layer tree    
  var tree = layerManager.getLayerTree();
  if (tree.hasChildNodes()) {
    tree.insertBefore(node, tree.childNodes[0]);
  }
  else {
    tree.appendChild(node);
  }
  
  node.attributes.layerName = layer.params.LAYERS;
  node.attributes.visible = true;
  
  node.render();

  // adding tooltip
  rIconDom = node.getUI().getRightIcon();
  new Ext.ToolTip({
    target: rIconDom,
    html: 'Supprimer la couche WMS'
  });
  
  
  
  
  var rIconElt = Ext.fly(rIconDom);

  rIconElt.addListener('click',
    function(evt) {
       // removing the layer from the map
       var layer = node.attributes.OlLayer;
       var map = layer.map;
       map.removeLayer(layer);
       // removing the node from the tree
       node.remove();
       evt.stopPropagation();
       return false;       
     } 
  );

  Carmen.Control.LayerTreeManager.addTransparencySliderToNode(node);
};
