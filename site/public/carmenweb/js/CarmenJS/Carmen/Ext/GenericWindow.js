/**
 * Carmen Form Window
 */
function adjustSizeGenericWindow(cmngenericwindow){
  var constrainTo = Ext.getBody();
  var vsize = constrainTo.getSize();
  //cmngenericwindow.setXY(constrainTo.getXY());
  cmngenericwindow.setMaxHeight(Ext.dom.Element.getViewportHeight()-20);
  cmngenericwindow.setMaxWidth(Ext.dom.Element.getViewportWidth()-20);
}
Ext.onReady(function(){
	var constrainTo = Ext.getBody();
	if ( !constrainTo ) return;
	constrainTo.on('resize', function(){
	  Ext.each(Ext.ComponentQuery.query('cmngenericwindow'), adjustSizeGenericWindow);
	})
})
Ext.define('Carmen.Generic.Window', {
	xtype : 'cmngenericwindow',
  extend: 'Ext.window.Window', 
  record : null,
  //constrainHeader : true,
  
  toFrontOnShow : true,
  width: Carmen.Util.WINDOW_WIDTH,
  minHeight : 300,
  scrollable: true,
  closeAction: 'destroy',
  autoDestroy : true,
  closable: true,
  layout: 'fit',
  initComponent : function(){
  	this.on('destroy',     function(){app && app.ui && app.ui.un('resize', this.keepSize, this)}, this);
  	this.on('afterrender', function(){app && app.ui && app.ui.on('resize', this.keepSize, this)}, this);
    var constrainTo = Ext.getCmp('mapPanel').getEl();
    var me = this;
    me.resizable = {
      listeners : {
        beforeresize : function(){
          constrainTo.mask().setStyle('backgroundColor', 'transparent');
        },
        resize : function(){
          constrainTo.unmask();
        },
        close : function(){
          constrainTo.unmask();
        }
      }
    };
    me.defaultButtons =  [{ 
      name: 'button_apply',
      id: 'btApply'+me.getId(),
      text: _t('app.btn.apply'),
      handler : me.saveActiveForm.bind(me)
    },
    { 
      name: 'button_save',
      text: _t('app.btn.save'),
      handler : function(){me.saveAllForms()}
    },
    { 
      name: 'button_cancel',
      text: _t('app.btn.cancel'),
      handler : function() {
        var win = this.up('window');
        win.close();
      }
    }];
    
    me.buttons = me.buttons || me.defaultButtons;
    me.constrain = true;

    this.callParent(arguments);
    if ( constrainTo ){
      me.on('show', function(_this_, width, height){
        var vsize = constrainTo.getSize();
      	me.setXY(constrainTo.getXY());
        me.setMaxHeight(vsize.height-20);
        me.setMaxWidth(vsize.width-20);
      });
    }
    me.on('move', function(){
      me.setMaxHeight(Ext.dom.Element.getViewportHeight()-20);
      me.setMaxWidth(Ext.dom.Element.getViewportWidth()-20);
    }, me);
    me.on('close', function(){
      Ext.Ajax.abortAll();
      constrainTo.unmask();
    });
  },
  
  keepSize : function(){
    this.isVisible() && adjustSizeGenericWindow(this)
  },
  
  edit : function(record){
    this.record = record;
    var button_apply = this.down('[name=button_apply]');
    if ( button_apply && record ){
      button_apply.setVisible(!record.phantom)
    }
    this.show();
  },
  
  /**
   * @return the form activ in the window
   */ 
  getActiveForm : function() { 
    // assumed "this" is the window cpt
    var activeForm = null;
    var forms = this.getForms();
    if (forms.length==0)
      return activeForm;
    var tabpanel = this.down('tabpanel');
    if ( tabpanel ){
      if ( tabpanel.getActiveTab().is('cmngenericform') ){
        activeForm = tabpanel.getActiveTab();
      }
      else {
         activeForm = tabpanel.getActiveTab().down('cmngenericform');
      }
    } else {
        activeForm = forms[0];
    }
    return activeForm;
  },
  
  /**
   * @return the forms contained in the window
   */
  getForms : function() { 
    // assumed "this" is the window cpt
    var forms = this.query('cmngenericform');
    return forms;
  },
  
  closeIfAllOk : function() {
    this.formInProgress--;
    if (this.formInProgress>0)
      return;
    else
      this.close();
  },
  
  /**
   * Save the active Generic form contained by the window
   * @scope This : the current window
   */
  saveActiveForm : function()
  {
    //saving the active form
    var win = this;
    var form = win.getActiveForm();
    if ( !form ) return;
    if ( form.isValid() ){
      var record = form.getRecord();
      if ( !record ) return;
      if ( record.phantom ){
        record.set(record.getIdProperty(), null);
      }
      form.updateRecord(record);
      record = this.getSubmittedRecord(record);
      if(record.data.tools !== undefined && record.data.tools.data.attribut_annotation !== undefined) {
          record.data.tools.data.attribut_value_annotation = Ext.ComponentQuery.query('[name=attribut_value_annotation_textfield]')[0].value; 
      }
      this.saveRecord(record);
    }
  },
  
  /**
   * Save all the Generic form contained in the window
   * @scope This : the current window
   */
  saveAllForms : function(callbackSucccess, callbackFailure)
  {
    var win = this;
    var forms = win.getForms();
    
    // iterating over forms to save 
    var fields = {};
    var isValid = true;
    Ext.each(forms, function(form){
      if ( form.isValid() ){
        var record = form.getRecord();
        if ( !record ) return;
        form.updateRecord(record);
        if ( record.phantom ){
          record.set(record.getIdProperty(), null);
        }
      } else {
        var tabpanel = win.down('tabpanel');
        if ( tabpanel ) tabpanel.setActiveItem(form);
        isValid = false;
        return false;
      }
    });
    if ( !isValid ){
      if ( callbackFailure ) callbackFailure.call(win);
      return;
    }
    
    var record = this.getSubmittedRecord();
    var attribut_component = Ext.ComponentQuery.query('[name=attribut_value_annotation_textfield]')[0]; 
    if(record.data.tools !== undefined && record.data.tools.data.attribut_annotation !== undefined && attribut_component !== undefined) {
        record.data.tools.data.attribut_value_annotation = attribut_component.value; 
    }
    this.saveRecord(record, callbackSucccess || win.close, callbackFailure);
    return;
  },
  
  /**
   * A appeler après form.updateRecord(record)
   * 
   * @param Ext.data.Model record
   * @return Ext.data.Model
   */
  getSubmittedRecord : function(record){
    /**
     * Si aucun record n'est fourni alors renvoit le record de la fenêtre
     */
    if ( !record ){
      record = this.record;
    }
    if ( !record ) return null;
    
    this.initialRecord = record;
    /**
     * Clone le record à soumettre pour qu'il soit mis en forme pour la soumission
     */
    record = record.clone();
    /**
     * Soumet l'ensemble des champs et pas uniquement ceux modifiés
     */
    record.modified = record.getData({persist:true, serialize:true});

    return record;
  },
  
  /**
   * Save the submitted record and call the differents callback in success/failure mode
   * @param Ext.data.Model record
   * @param Function|null  callback : function to optionaly callback after save (ex : this.close) 
   */
  saveRecord : function(record, callbackSuccess, callbackFailure)
  {
    var win = this;
    if ( record ){
      win.preSave();
      record.save({
      	params : win.getMoreParams(),
        success: function(returnedRecord, operation){
          win.onSaveSuccess(returnedRecord, operation);
          if ( callbackSuccess ){
            callbackSuccess.call(win);
          }
        },
        failure: function(response) {
          if ( Carmen.notAuthenticatedException(response) ) return;
          var title = this.getConfig().title ? this.getConfig().title : _t("form.msgFailureTitle");
          Ext.Msg.alert(title, _t("form.msgFailure"))
          win.postSaveFailure(response);
          if ( callbackFailure ){
            callbackFailure.call(win);
          }
        }
      })
    }
  },
  
  /**
   * Build the response to call with specific function postSaveSuccess and notify the user to success
   * @param Ext.data.Model     returnedRecord
   * @param Ext.data.Operation operation
   */
  onSaveSuccess : function(returnedRecord, operation)
  {
    var win = this;
    this.initialRecord.commit();
    var response;
    if ( operation instanceof Ext.data.Operation ){
      if ( Ext.isDefined(operation._response) ){
        response = operation._response;
      } else {
        Ext.log({stack:true}, 'For the response object of the class Ext.data.Operation, the private attribute "_response" does not more exist');
        response = {responseText : "null"};
      }
    } else {
      Ext.log({stack:true}, 'Unimplemented treatment for this response object. Check compatibility with this version of librairy ExtJs');
      return;
    }

    var jsonResponse = Ext.decode(response.responseText);
    
    if(returnedRecord.data !== undefined ) {
        if(returnedRecord.data.mapOnlineResources  !== undefined ) {
            var record_online_resources_response = returnedRecord.data.mapOnlineResources; 
            jsonResponse.map.mapOnlineResources = record_online_resources_response;             
        }
    }
    Carmen.Notification.msg(win.title, _t("form.msgApplied"), 2);
    
    win.postSaveSuccess(operation, jsonResponse);
  },
  
  /**
   * @abstract 
   * Unimplemented function to interpret the save Operation and its jsonResponse
   * 
   * @param Ext.data.Operation operation
   * @param Object             jsonResponse
   */
  preSave : Ext.emptyFn,
  
  /**
   * @abstract 
   * Unimplemented function to interpret the save Operation and its jsonResponse
   * 
   * @param Ext.data.Operation operation
   * @param Object             jsonResponse
   */
  postSaveSuccess : Ext.emptyFn,
  /**
   * @abstract 
   * Unimplemented function to interpret request failure response
   * 
   * @param Object             failureResponse
   */
  postSaveFailure : Ext.emptyFn,
  /**
   * @abstract 
   * Unimplemented function to interpret request failure response
   * 
   * @param Object             failureResponse
   */
  getMoreParams : Ext.emptyFn
  
});