Ext.ns("Carmen.Notification");
Carmen.Notification = function(){
	  var delayed;
    var msgCt;

    function createBox(t, s, config){
       // return ['<div class="msg">',
       //         '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
       //         '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>', t, '</h3>', s, '</div></div></div>',
       //         '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
       //         '</div>'].join('');
       return Ext.apply(config||{}, {html : '<div class="msg ' + Ext.baseCSSPrefix + 'border-box">'+(t ? '<h3>' + t + '</h3>' : '')+'<p>' + s + '</p></div>'});
    }
    return {
        msg : function(title, format, delay, config) {
        	var args = Ext.apply({shadow:'sides', animate:false, destroyAfterHide:true, cls:'msg ' + Ext.baseCSSPrefix + 'border-box', align:'t', closable :false, plain :true}, config||{});
        	if ( delay!==false ){
        		args.autoCloseDelay = delay*1000;
        	} else {
        		if ( delayed ){
        			return;
        		}
        		args.autoCloseDelay = 24*60*60*1000;
        	}
          if ( title ){
            args.title = title;
          }
        	if ( format ){
        		args.html = format;
        	}
        	if ( !args.title && !args.html ) return;
        	msgCt = Ext.toast(args); 
        	if ( delay!==false ){
        		delayed = msgCt;
        		msgCt.on('close', function(){delayed = null;})
        	}

        	return msgCt;
        	
        }
    };
}();


/**
 * Compose the Scale form editor for a map UI
 *  
 * @return Ext.form.FieldContainer
 */
Carmen.getScaleFields = function (radical, rootTranslate, fieldConfig)
{
  fieldConfig = fieldConfig || {};
  var defaultConfig1 = Ext.apply({}, Carmen.Util.defaultFormConfig);
  var defaultConfig2 = Ext.apply({}, Carmen.Util.defaultFormConfig);
  
  return Ext.apply(defaultConfig1, {
    xtype: 'fieldcontainer',
    fieldLabel : _t(rootTranslate+'extra.'+radical+'Scales'),
    layout: {
      type: 'table',
      columns: 2,
      tableAttrs : {
        width : '100%'
      },
      tdAttrs : {
        width : '40%',
        align : 'left'
      }
    },
    
    defaults : Ext.apply(defaultConfig2, {
      labelAlign: 'right',
      labelWidth: 'auto',
      padding : null,
      xtype : 'numberfield',
      allowDecimals : true,
      allowBlank : false
    }),
    
    items : [
      Ext.apply({
        fieldLabel : _t(rootTranslate+'fields.'+radical+'Minscale'),
        name : radical+'Minscale'
      }, fieldConfig), 
      Ext.apply({
        labelAlign: 'left',
        labelWidth : 100,
        fieldLabel : _t(rootTranslate+'fields.'+radical+'Maxscale'),
        name : radical+'Maxscale'
      }, fieldConfig)
    ]
  });
} 

Carmen._serializeMapserverRecordForSubmit = function(record){

  var serialize = {};
  Ext.each(record.getFields(), function(field){
    if ( !field.persist ) return;
    var fvalue = (field.serialize ? field.serialize(record.get(field.name), record) : record.get(field.name));
    var mapping = field.mappingSerialize || field.mapping || field.name; 
    if ( mapping ){
      if ( Ext.isString(mapping) ){
        var maps = mapping.split(".");
        var traitArray = function(item, root, defaultValue){
          var indices = [];
          while ( /\[([^\]]+)\]/.test(item) ){
            indices.push( item.replace(/^(\w+)\[([^\]]+)\]/, "$2") );
            item = item.replace(/^(\w+)\[([^\]]+)\]/, "$1")
          }
          var name = root+'.'+item, expr = [];
          for (var j=0; j<indices.length; j++){
            expr.push(name +' = '+name+' || [];');
            name += '['+indices[j]+']';
          }
          expr.push(name +' = '+name+' || '+defaultValue+';');
          return {name : name, expr : expr};
        };
        var root = 'serialize', expr = [];
        for (var i=0; i<maps.length; i++){
          var r = traitArray(maps[i], root, (i==maps.length-1 ? 'null' : '{}'));
          expr = expr.concat(r.expr);
          root = r.name;
        }
        expr.push( root +' = Carmen.getFirstDefined(fvalue, '+root+');');
        eval(expr.join(';'));
      }
      if ( Ext.isFunction(mapping) ){
        mapping(fvalue, record, serialize);
      }
    }
  })
  return serialize;
}

Carmen.serializeMapserverRecordForSubmit = function(value){
  if ( value instanceof Ext.data.Store ){
    var records = [];
    value.getData().each(function(record){
    	var serialize = Carmen._serializeMapserverRecordForSubmit(record);
      records.push(serialize);
    })
    return records;
  }
  else if ( value instanceof Ext.data.Model ){
  	return Carmen._serializeMapserverRecordForSubmit(value);
  }
  return value;
};

Carmen.serializeRecordForSubmit = function(value){
  if ( value instanceof Ext.data.Model ){
    return value.getData({persist:true, serialize:true});
  }
  if ( value instanceof Ext.data.Store ){
  	var records = [];
  	value.getData().each(function(record){
    	records.push(
    	  record.getData({persist:true, serialize:true})
    	);
  	})
    return records;
  }
  return value;
};


Carmen.getFirstDefined = function()
{
  var args = Ext.Array.toArray(arguments);
  var result = null;
  Ext.each(args, function(value){
    if ( Ext.isDefined(value) && value!==null ){
      result = value; 
      return false;
    }
  });
  return result;
}
