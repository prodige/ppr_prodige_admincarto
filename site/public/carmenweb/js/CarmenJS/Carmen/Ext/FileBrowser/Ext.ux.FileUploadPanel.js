var _T_FILEBROWSER_UPlOAD = function (key) {
  return _t("FileBrowser.upload." + key);
};

// Create namespace
Ext.namespace('Ext.ux');

Ext.define('Ext.ux.FileUploadPanel', {
  extend: "Ext.Panel",
  xtype: "ux-fileuploadpanel",
  
  initComponent: function() {
    var me = this;
    
    me.items = [{
      xtype: 'textfield',
      name: 'file',
      fieldLabel: _T_FILEBROWSER_UPlOAD('label'),
      allowBlank: false,
      maxLength: 255,
      enforceMaxLength: true
    }];
    
    this.callParent(arguments);
  }
});