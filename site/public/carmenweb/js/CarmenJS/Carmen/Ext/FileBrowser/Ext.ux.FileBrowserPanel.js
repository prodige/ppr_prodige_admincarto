var _T_FILEBROWSER_PANEL = function (key) {
  return _t("FileBrowser.panel." + key);
};

var oldFileSize = Ext.util.Format.fileSize;
Ext.util.Format.fileSizeFr = function(fileSize){
	var fileSizeFormat = oldFileSize(fileSize);
	fileSizeFormat = fileSizeFormat.replace(/byte/g, 'octet').replace(/([KGMT])B/g, '$1o');
	return fileSizeFormat;
}
Ext.util.Format.fileSize = Ext.util.Format.fileSizeFr;
	

// Create namespace
Ext.namespace('Ext.ux');
Ext.Loader.setConfig({
  disableCaching: false
});
Ext.Loader.setPath({
    'Ext.ux.upload' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload',
    'Ext.ux.upload.uploader' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload/uploader',
    'Ext.ux.upload.data' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload/data',
    'Ext.ux.upload.header' : String((window.CARMEN_URL_ADMIN_CARTO || window.CARMEN_URL_ADMINCARTO || '')).replace('/app_dev.php', '')+'/carmenweb/js/CarmenJS/Carmen/Ext/FileBrowser/lib/upload/header'
});

/**
 * File browser panel
 */
Ext.define("Ext.ux.FileBrowserPanel", {
  
  extend: "Ext.Panel",
  xtype: "ux-filebrowserpanel",
  
  requires : [
    'Ext.ux.upload.header.AbstractFilenameEncoder',
    'Ext.ux.upload.header.Base64FilenameEncoder',
    'Ext.ux.upload.Dialog',
    'Ext.ux.upload.Panel'
  ],

  dataSet : 'files',
  
  /**
   * The URL that is used to process data
   */
  dataUrl: '',
  
  /**
   * authorized extensions on select, separated by comma
   */
  selectExtension: '*.*',

  /**
   * authorized extensions on upload, separated by comma
   */
  uploadExtension: '',

  /**
   * default path
   */
  defaultPath: '/Root',

  /**
   * default path
   */
  relativeRoot: '/Root',
  
  /**
   * Called by Ext when instantiating
   *
   * @private
   */
  initComponent: function () {
    var config;
    var me = this;
    
    var current_path = (this.defaultPath+this.relativeRoot.replace(this.defaultPath, '')).replace(/\/Root\/?/g, '').replace(/\/\/|\.\//g, '/').replace(/\/$|\.$/g, '');
    // create a grid that displays files
    this.grid = new Ext.grid.Panel({ 
      border: false,
      hidemode: 'display',
      hidden: false,
      stripeRows: true,
      enableDragDrop: true,
      trackMouseOver: true,
      ddGroup: 'fileMoveDD',
      scrollable : 'y',
      bbar : [{
        ui : 'default-toolbar x-grid-empty',
        padding : '0 10 0 10',
        xtype: 'label',
        name : 'current_path',
        
        text : '/'+current_path
      }],
      
      viewConfig : {
        emptyText : _T_FILEBROWSER_PANEL('gridEmptyText'),
        deferEmptyText : false
      },
      columns: [
        {
          text: _T_FILEBROWSER_PANEL('gridColumnNameHeaderText'),
          id: 'name',
          width: '35%',
          align: 'left',
          dataIndex: 'name',
          sortable: true,
          editor: new Ext.form.TextField({vtype: 'filename'})
        }, {
          text: _T_FILEBROWSER_PANEL('gridColumnSizeHeaderText'),
          dataIndex: 'size',
          width: '15%',
          align: 'right',
          sortable: true,
          renderer: Ext.util.Format.bytesToSi
        }, {
          text: _T_FILEBROWSER_PANEL('gridColumnTypeHeaderText'),
          dataIndex: 'type',
          align: 'left',
          width: '25%',
          sortable: true
        }, {
          text: _T_FILEBROWSER_PANEL('gridColumnDateModifiedText'),
          dataIndex: 'date_modified',
          width: '25%',
          align: 'center',
          sortable: true,
          renderer: Ext.util.Format.dateRenderer(_T_FILEBROWSER_PANEL('displayDateFormat'))
        }
      ],
      store: new Ext.data.JsonStore({
        autoLoad: false,
        proxy: {
          type: 'ajax',
          url: this.dataUrl+"/get-"+this.dataSet
        },
        root: 'data',
        fields: [
          {name: 'name', type: 'string'},
          {name: 'size', type: 'int'},
          {name: 'type', type: 'string'},
          {name: 'full_path_name', type: 'string'},
          {name: 'route', type: 'string'},
          {name: 'date_modified', type: 'date', dateFormat: 'timestamp'}
        ]
      }),
      selModel: new Ext.selection.RowModel({
        getSelected : function(){return this.selected;},
        listeners: {
          selectionchange: {
            fn: this.onGridSelectionChange,
            scope: this
          }
        }
      }),
      listeners: {
        render: {
          fn: this.onGridRender
        },
        rowcontextmenu: {
          fn: this.onGridContextMenu,
          scope: this
        },
        itemdblclick : {
          fn : this.onItemDblClick,
          scope : this 
        }
      }
    }); //eo this.grid =
    
    this.tree = new Ext.tree.Panel({
      //title: _T_FILEBROWSER_PANEL('treePanelHeaderText'),
      border: false,
      useArrows: true,
      root: {
        text: current_path,
        full_path_name : this.relativeRoot
      },

      store: new Ext.data.TreeStore({
        model: 'Ext.data.TreeModel',
        autoLoad: true,
        proxy: {
          type: 'ajax',
          url: this.dataUrl
        },
        listeners: {
          beforeload: {
            fn: this.onTreeBeforeLoad,
            scope: this
          }
        }
      }),
      selModel: new Ext.selection.TreeModel({
        getSelected : function(){return this.selected;},
        listeners: {
          selectionchange: {
            fn: this.onTreeSelectionChange,
            scope: this
          }
        }
      })/*,
      listeners: {
        afterrender: {
          fn: this.onTreeAfterRender,
          scope: this,
          single: true
        }
      }*/
    }); // eo this.tree =
    
    // config
    config = Ext.apply(this.initialConfig, {
      layout: 'border',
      border: true,
      height: this.height || 400,
      minWidth:800,
      width:"100%",
      items: [
      Ext.override(this.tree, {
        region: 'west',
        width: 200,
        autoScroll: true,
        split: true,
        collapseMode: 'mini'
      }), {
        region: 'center',
        layout: 'fit',
        activeItem: 0,
        tbar : [{
        	ui : 'default'/*-toolbar'*/,
          xtype: 'button',
          text: _T_FILEBROWSER_PANEL('uploadText'),
          //cmd: 'upload',
          //iconCls: 'ux-filebrowser-icon-upload',
          scope: this,
          handler: this.openUploader
        },'->',{
          ui : 'default',
          xtype: 'button',
          text: _T_FILEBROWSER_PANEL('selectText'),
          cmd: 'select',
          //iconCls: 'ux-filebrowser-icon-select',
          canToggle: true,
          disabled: true,
          scope: this,
          handler: this.onGridToolbarClick
        }],
        items: [
          this.grid
        ]
      }]
    });

    // appy the config
    Ext.apply(this, config);

    this.callParent(arguments);
  },
  
  openUploader : function(){ //this.onGridToolbarClick
    var me = this;
    var uploadPanel = Ext.create('Ext.ux.upload.Panel', {
        uploaderOptions : {
          url : this.dataUrl+'/upload-file'
        },
        uploadExtraHeaders: {
          "X-Destination-Path": this.getCurrentPath(),
          "X-Destination-DefaultPath": '/Root',
          "X-Destination-RelativeRoot": this.relativeRoot
        },
        accept: this.uploadExtension,
        listeners : {uploaderselectfile : this.uploaderSelectFile, scope : this},
        filenameEncoder : 'Ext.ux.upload.header.Base64FilenameEncoder',
        synchronous : true,
        emptyText : _T_FILEBROWSER_PANEL('uploadEmptyText'),
        textOk : _T_FILEBROWSER_PANEL('uploadTextOk'),
        textUpload : _T_FILEBROWSER_PANEL('uploadTextUpload'),
        textBrowse : _T_FILEBROWSER_PANEL('uploadTextBrowse'),
        textAbort : _T_FILEBROWSER_PANEL('uploadTextAbort'),
        textSelect : _T_FILEBROWSER_PANEL('uploadTextSelect'),
        textRemoveSelected : _T_FILEBROWSER_PANEL('uploadTextRemoveSelected'),
        textRemoveAll : _T_FILEBROWSER_PANEL('uploadTextRemoveAll'),
        textFilename : _T_FILEBROWSER_PANEL('uploadTextFilename'),
        textSize : _T_FILEBROWSER_PANEL('uploadTextSize'),
        textType : _T_FILEBROWSER_PANEL('uploadTextType'),
        textStatus : _T_FILEBROWSER_PANEL('uploadTextStatus'),
        textProgress : _T_FILEBROWSER_PANEL('uploadTextProgress'),
        selectionMessageText : _T_FILEBROWSER_PANEL('uploadSelectionMessageText'),
        uploadMessageText : _T_FILEBROWSER_PANEL('uploadLoadMessageText'),
        buttonText: _T_FILEBROWSER_PANEL('uploadButtonBrowse'),
        maxSizeMsg: _T_FILEBROWSER_PANEL('uploadMaxSizeMsg')
    });
    this.uploadDialog = Ext.create('Ext.ux.upload.Dialog', {
        dialogTitle :  new Ext.XTemplate(_T_FILEBROWSER_PANEL('uploadTitle')).apply([this.getCurrentPath().replace('/Root/', '/')]),
        textClose: _T_FILEBROWSER_PANEL('uploadTextClose'),
        width: Carmen.Util.WINDOW_WIDTH-100,
        panel : uploadPanel,
        listeners: {
          beforeclose: function() {
            me.refreshListOfFiles();
          },
          close: function() {
            me.uploadDialog = null;
          }/*,
          uploadcomplete: function(uploadPanel, manager, items, errorCount) {
            //uploadPanel.uploadComplete(items);
            if (!errorCount) {
              if( items.length>0 ) {
                me.refreshListOfFiles();
              }
              this.close();
            }
          }*/
        }
    });
    this.uploadDialog.show();
    return this.uploadDialog;
  },

  setExtensions : function(extensions){
  	var uploadExtension, selectExtension;
  	if ( Ext.isObject(extensions) ){
      selectExtension = extensions.selectExtension || this.selectExtension;
  		uploadExtension = extensions.uploadExtension || this.uploadExtension;
  	}
  	else {
  		selectExtension = arguments[0] || this.selectExtension;
      uploadExtension = arguments[1] || this.uploadExtension;
  	}
  	this.selectExtension = selectExtension;
  	this.uploadExtension = uploadExtension;
  	if ( this.isVisible() ){
  		this.refreshListOfFiles();
  	}
  	if ( this.uploadDialog ){
  		this.uploadDialog.panel.accept = uploadExtension;
  	}
  },
  
  /**
   * Event handlers for when grid row is right-clicked
   * Shows context menu
   *
   * @private
   * @param Ext.view.Table  grid       Grid panel that was right-clicked
   * @param Ext.data.Model  data       data of the selected row
   * @param int             rowIndex   data of the selected row
   * @param Ext.event.Event evt        data of the selected row
   */
  onGridContextMenu: function (grid, data, tr, rowIndex, evt) {
    
    evt.stopEvent();
    //grid.getSelectionModel().selectRow(rowIndex);

    var contextMenu = this.getGridContextMenu();
    contextMenu.rowIndex = rowIndex;
    contextMenu.showAt(evt.getXY());
  },
  
  /**
   * Event handler for when grid-specific contentmenu item is clicked
   * Delegates actions for menu items to other methods
   *
   * @private
   * @param Ext.menu.Menu   menu    he context menu
   * @param Ext.menu.Item   menuItem  The menu item that was clicked
   * @param Ext.EventObject evt     Event object
   */
  onGridContextMenuClick: function (menu, menuItem, evt) {
    
    switch (menuItem.cmd) {
      case 'select':
        var record = this.grid.getSelectionModel().getSelected();
        this.selectFile(record);
        break;
        
      case 'download':
        var record = this.grid.getSelectionModel().getSelected();
        this.downloadFile(record);
        break;
    }
  },

  /**
   * Event handler for all button that are clicked in the grid toolbar
   *
   * @private
   * @param Ext.Button      button  The button that was clicked
   * @param Ext.EventObject evt     The click event
   */
  onGridToolbarClick: function (button, evt) {
    
    switch (button.cmd) {
      case 'select':
        var selModel = this.grid.getSelectionModel();
        var record = (selModel.getSelectionMode()=="MULTI" ? selModel.getSelection() : selModel.getSelected());
        this.selectFile(record);
        break;
      case 'download':
        var record = this.grid.getSelectionModel().getSelected();
        this.downloadFile(record);
        break;
      /*case 'upload':
        this.uploadFile();
        break;*/
    }
  },



  /**
   * Event handler when double click on an item. Do the "select" toolbar button action 
   *
   * @private
   * @param Ext.view.Table  grid       Grid panel that was right-clicked
   * @param Ext.data.Model  record       data of the selected row
   * @param HTMLElement     rowEl       data of the selected row
   * @param int             rowIndex   data of the selected row
   * @param Ext.event.Event evt        data of the selected row
   */
  onItemDblClick : function (grid, record, rowEl, rowIndex, evt){
    this.onGridToolbarClick({cmd : 'select'});
  },
  
  /**
   * Event handler for when the selection changes in the tree
   * Appends the folder path of the selected node to the request
   * Causes files to be loaded in the grid for selected node
   * Toggles displaying of 'minus' and 'gear' tools depending on selection
   *
   * @private
   * @param Ext.selection.TreeModel  selection  tree model selection
   * @param Ext.data.TreeModel       data       array of selected tree node
   */
  onTreeSelectionChange: function (selection, data) {
    var currentNode = data[0];
    currentNode.expand();
    if (currentNode.isLoaded()) {
      this.refreshListOfFiles(currentNode.getPath('text'));
    }
    this.down('[name=current_path]').setText(currentNode.getPath('text'))
//    var path = currentNode.getPath('text');
//    this.queryPath(selection.store, path);
  },

  /**
   * Event handler for when selection in the grid changes
   * En- or disables buttons in the toolbar depending on selection in the grid
   *
   * @private
   * @param Ext.selection.RowModel sm The selection model
   */
  onGridSelectionChange: function (sm) {
    
    if (sm.hasSelection()) {
      this.enableGridToolbarButtons();
    } else {
      this.disableGridToolbarButtons();
    }
  },

  /**
   * Event handler for when the tree is about to load new data
   * Appends the folder path of the selected node to the request
   *
   * @private
   * @param Ext.data.TreeStore        TreeStore   The Treeloader
   * @param Ext.data.operation.Read   reader      The selected node
   * @param Object                    Callback    function specified in the 'load' call from the treeloader
   */
  onTreeBeforeLoad: function (treeStore, operation) {
    
    var path = (operation.node || operation.config.node).getPath('text');
    this.queryPath(treeStore, path);
  },
  
  queryPath : function(treeStore, path){
    treeStore.getProxy().setExtraParam("action", "get-folders");
    treeStore.getProxy().setExtraParam("path", path);
    treeStore.getProxy().setExtraParam("defaultPath", this.defaultPath);
    treeStore.getProxy().setExtraParam("relativeRoot", this.relativeRoot);
    
    // update list of files  
    this.refreshListOfFiles(path);
  },
  
  /**
   * Event handler for when the tree is renderer
   * Selects the root node, causing the files in the root to be loaded
   *
   * @private
   * @param Ext.tree.Panel tree The tree panel
   *
  onTreeAfterRender: function (tree) {
    
    var root = tree.getStore().root;
    tree.getSelectionModel().select(root);
  },*/

  /**
   * Gets and lazy creates context menu for file grid
   *
   * @private
   * @returns Ext.menu.Menu Context menu
   */
  getGridContextMenu: function () {

    if (!this.gridContextMenu) {
      this.gridContextMenu = new Ext.menu.Menu({
        items: [{
          text: _T_FILEBROWSER_PANEL('selectText'),
          cmd: 'select'
          //iconCls: 'ux-filebrowser-icon-selectfile'
        }/*, {
          text: _T_FILEBROWSER_PANEL('downloadText'),
          cmd: 'download',
          //iconCls: 'ux-filebrowser-icon-downloadfile'
        }*/],
        listeners: {
          click: {
            fn: this.onGridContextMenuClick,
            scope: this
          }
        }
      });
    }

    return this.gridContextMenu;
  },

  /**
   * Disables buttons in the toolbar that are marked 'toggleable'
   * @private
   */
  disableGridToolbarButtons: function () {
    var toolbars = this.getLayout().centerRegion.getDockedItems('toolbar');
    
    var buttons = [];
    Ext.each(toolbars, function(toolbar){
      buttons = buttons.concat(toolbar.items.items);
    });
    Ext.each(buttons, function (button) {
      if( button.canToggle == true ) {
        button.disable();
      }
    });
  },

  /**
   * Enables buttons in the toolbar that are marked 'toggleable'
   * @private
   */
  enableGridToolbarButtons: function () {
    var toolbars = this.getLayout().centerRegion.getDockedItems('toolbar');
    
    var buttons = [];
    Ext.each(toolbars, function(toolbar){
    	buttons = buttons.concat(toolbar.items.items);
    });
    Ext.each(buttons, function (button) {
      if( button.canToggle == true ) {
        button.enable();
      }
    });
  },

  /**
   * Download a file from the server
   * Shamelessly stolen from Saki's FileTreePanel (Saki FTW! :p)
   * But it does exactly what i need it to do, and it does it very well..
   * @see http://filetree.extjs.eu/
   *
   * @private
   * @param Ext.data.Model record Record representing the file that needs to be downloaded
   */
  uploaderSelectFile: function (uploader, selection) {
    var expression = new RegExp('('+this.selectExtension.replace(/\./g, "\\.").replace(/\*/g, ".+").replace(/,/g, ")|(")+')', "i");
    
    var selected = null;
    var selecteds = [];
    let self = this;

    /** Filtre des données */
    selection.forEach(function(file){
      let isOk = expression.test(file.get('filename'));
      if (  isOk && !selected){
        selected = file;
      }
     
      if( isOk ){
        selecteds.push(file);
      }
      
    });


    if(!selected) {
      Ext.Msg.alert(_T_FILEBROWSER_PANEL("uploadSelectFailureTitle"), new Ext.XTemplate(_T_FILEBROWSER_PANEL("uploadSelectFailureMessage")).apply([this.selectExtension])).setIcon(Ext.Msg.ERROR);
      return false;
    }

    let currentPath = self.getCurrentPath();

    /** paramétrage des fichiers uploadés */
    selecteds.forEach( (selectFile, idx) => {
      selectFile.set({
        name : currentPath +'/'+selectFile.get('filename'),
        full_path_name :  selectFile.get('filename'),
        route :  currentPath
      }, {
        commit:true,
        silent:true
      });

      
    })    

    selected.set({
      name : currentPath +'/'+selected.get('filename'),
      full_path_name :  selected.get('filename'),
      route :  currentPath
    }, {
      commit:true,
      silent:true
    });

    /** Selon le nombre de fichier, différent type d'évènement seront déclenché */
    this.selectFile( (selecteds.length > 1 ) ? selecteds : selected);

    if ( this.uploadDialog ){
      this.uploadDialog.suspendEvent('beforeclose');
      this.uploadDialog.close();
    }
  },

  /**
   * Download a file from the server
   * Shamelessly stolen from Saki's FileTreePanel (Saki FTW! :p)
   * But it does exactly what i need it to do, and it does it very well..
   * @see http://filetree.extjs.eu/
   *
   * @private
   * @param Ext.data.Model record Record representing the file that needs to be downloaded
   */
  selectFile: function (record) {
    var file_georeferenced = this.down('[name=file_georeferenced]:not([disabled=true])');

    if(file_georeferenced && file_georeferenced.value == 2) {
      fileSize = record.getAt(0).data.size /(1024*1024); //On convertit la taille du fichier en Mo
      maxSize = 50*1024*1024; //50 Mo
      if(maxSize < fileSize){
        alert('La taille du fichier utilisé pour le géocodage ne doit pas dépasser 50Mo');
        return;
      }
    }

    /** multiple */
    if ( record instanceof Ext.util.Collection ){
      if ( !record.length ) return;
      record = record.getAt(0);
    }

    /** */
    if ( record instanceof Array ){
      this.fireEvent('multiselectedfile', this, record);
      return false;
    }

    /** */
    var path = record.get('full_path_name');
    var name = record.get('name');
    
    if( path != "" ) {
      this.fireEvent('selectedfile', this, path, name, record);
    }
  },

  /**
   * Returns current path based on current selected node
   * @return string
   */
  getCurrentPath: function(currentNode) {
    
    var record = this.tree.getSelectionModel().getSelected();
    if( record.length> 0 ) {
      record = record.getAt(0);
    } else {
      record = this.tree.getRootNode();
    }
    return (record ? record.getPath('text') : this.defaultPath);
  },

  /**
   * Refresh list of files
   * @param String|null path
   */
  refreshListOfFolders: function(path) {
  },

  /**
   * Refresh list of files
   * @param String|null path
   */
  refreshListOfFiles: function(path) {
    path = path || this.getCurrentPath();
    var gridStore = this.grid.getStore();
    var proxy = gridStore.getProxy();
    
    proxy.setUrl(proxy.getUrl().replace(/\/(get-(files|folders))/, '/get-'+this.dataSet));
    proxy.setExtraParam("action", 'get-'+this.dataSet);
    proxy.setExtraParam("path", path);
    proxy.setExtraParam("defaultPath", this.defaultPath);
    proxy.setExtraParam("relativeRoot", this.relativeRoot);
    proxy.setExtraParam("extension", this.selectExtension);
    
    gridStore.load();
  },

  /**
   * Download a file from the server
   * Shamelessly stolen from Saki's FileTreePanel (Saki FTW! :p)
   * But it does exactly what i need it to do, and it does it very well..
   * @see http://filetree.extjs.eu/
   *
   * @private
   * @param Ext.data.Model record Record representing the file that needs to be downloaded
   */
  downloadFile: function (record) {

  },
 
  setDataSet : function(dataSet, multiple){
  	this.dataSet = dataSet;
  	if ( Ext.isBoolean(multiple) ){
  		this.grid.getSelectionModel().setSelectionMode(multiple ? "MULTI" : "SINGLE");
  	}
  	this.refreshListOfFiles();
  }
  /**
   * Show upload file panel
   * @private
   *
  uploadFile: function() {
    this.grid.hide();
    this.upload.currentPath = this.getCurrentPath();
    this.upload.show();
  }*/


});

/**
 * Additional Format function(s) to use
 */
Ext.apply(Ext.util.Format, {
  /**
   * Format filesize to human readable format
   * @param  int      size   Filesize in bytes
   * @return string  Formatted filesize
   */
  bytesToSi: function (size) {
    var s, e, r;
    //s = ['b', 'Kb', 'Mb', 'Gb', 'Tb'];
    s = eval("("+_T_FILEBROWSER_PANEL('units')+")"); //['o', 'Ko', 'Mo', 'Go', 'To'];
    if (typeof size === 'number' && size > 0) {
      if( size>1024 ) {
        e = Math.floor(Math.log(size) / Math.log(1024));
        r = size / Math.pow(1024, e);
        if (Math.round(r.toFixed(1)) !== r.toFixed(1)) {
          r = r.toFixed(1);
        }
      } else {
        e = 0;
        r = size;
      }
      return r + ' ' + s[e];
    } else {
      return '0 '.s[0];
    }
  }
});
