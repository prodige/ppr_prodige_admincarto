String.prototype.endPath = function(){return this.replace(/\/$/, '')+'/';}
var _T_FILEBROWSER_WINDOW = function (key) {
  return _t("FileBrowser.window." + key);
};

Ext.namespace('Ext.ux');

/**
 * window of file browser
 */
Ext.define('Ext.ux.FileBrowserWindow', {
  // ext properties
  extend: 'Ext.window.Window',
  reference: 'window-filebrowser',
  
  title: _T_FILEBROWSER_WINDOW('FileBrowser'),
  layout: 'vbox',
  
  width: (Carmen.Util.WINDOW_WIDTH || 1500) -50,
  minHeight : 350,

  
  /**
	 * The URL that is used to process data
	 */
	dataUrl: '',
  
  /**
   * authorized extensions on select, separated by comma
   */
  selectExtension: '*.*',

  /**
   * authorized extensions on upload, separated by comma
   */
  uploadExtension: '',

  /**
   * default path
   */
  defaultPath: '/Root',

  /**
   * default path
   */
  relativeRoot: '/Root',

  /**
   * intialisation of component
   */
  initComponent: function () {
    var me = this;
   
    me.title =  _T_FILEBROWSER_WINDOW('FileBrowser') +
      ( me.selectExtension != '' ? ' ('+me.selectExtension+')' : '' );
   
    this.fileBrowser = Ext.create("Ext.ux.FileBrowserPanel", {border:true,
    	height : (this.height || this.minHeight)-32,
      selectExtension: me.selectExtension,
      uploadExtension: me.uploadExtension,
      dataUrl: me.dataUrl,
      defaultPath: me.defaultPath,
      relativeRoot: me.relativeRoot,
      listeners: {
        selectedfile: function(panel, filePath, fileName, fileRecord) {
          var relativePath = (String(me.relativeRoot).endPath()+filePath).replace(String(me.defaultPath).endPath(), '');
          var absolutePath = (String(me.relativeRoot).endPath()+filePath).replace(String('/Root').endPath(), '');
          me.fireEvent('selectedfile', me, fileRecord, fileName, filePath, relativePath, absolutePath);
        },
        multiselectedfile: function(panel, records) {
          me.fireEvent('multiselectedfile', me, records, me.relativeRoot, me.defaultPath);
        }
      }
    });
    
    me.items = [
       this.fileBrowser
    ];
    
    me.callParent(arguments);
  }
  
});
