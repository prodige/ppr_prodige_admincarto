Ext.define('Carmen.LayerTree', {});

Carmen.LayerTree.defaults = {
	OL_PATH : null,
  OL_ITEM : null,
  nodeId : null,
  allowDrag: true,
  allowDrop: true,
  checked: true,
  cls: "",
  disabled: false,
  expandable: true,
  expanded: true,
  forceOpacityControl: false,
  href: "",
  hrefTarget: "",
  iconDisplayStrategy: false,
  opacity: 1,
  open: true,
  visible: true,
  removable : true,
  attributes : {},
  constructor : function(config){
  	this.callParent(arguments);
  	Ext.apply(this, config);
  }
}
Carmen.LayerTree.defaultGroupNode = Ext.apply({
  icon: "/carmenweb/images/empty.gif",
  iconCls: "cmnLayerTreeGroupNodeIcon",
  leaf: false,
  iconCls: "cmnLayerTreeGroupNodeIcon",
  type: "group",
  hasMetadata: false,
  groupIdentifier : null,
  groupName : null,
  children: []
}, Carmen.LayerTree.defaults);

Carmen.LayerTree.defaultLayerNode = Ext.apply({
  icon: "bundles/carmenweb/images/legend_layer.png",
  iconCls: "cmnLayerTreeLayerNodeIcon",
  leaf: true,
  text: "Layer",
  type: "layer",
  hasMetadata: true,
  children: []
}, Carmen.LayerTree.defaults);

Ext.define('Carmen.LayerTree.NodeGroup', Ext.apply({
  extend : 'Ext.data.NodeInterface'
}, Carmen.LayerTree.defaultGroupNode));

Ext.define('Carmen.LayerTree.NodeLayer', Ext.apply({
  extend : 'Ext.data.NodeInterface'
}, Carmen.LayerTree.defaultLayerNode));

Carmen.LayerTree.noNode = {
  allowDrag: false,
  allowDrop: false,
  checked: null,
  cls: "",
  disabled: true,
  expandable: false,
  expanded: false,
  href: "",
  hrefTarget: "",
  icon: "/carmenweb/images/empty.gif",
  iconCls: "cmnLayerTreeGroupNodeIcon",
  leaf: true,
  qshowDelay: 0,
  qtip: "",
  qtitle: "",
  text: "Pas de contenu",
  visible: true,
  forceOpacityControl: false,
  iconCls: "cmnLayerTreeGroupNodeIcon",
  iconDisplayStrategy: false,
  opacity: 1,
  open: false,
  type: "layer",
  hasMetadata: false,
  removable : false,
  children: []
};

Carmen.LayerTree.defaultRootNode = Carmen.LayerTree.NodeGroup.create({
	nodeId : 'root',
  text: "Contenu de la carte",
  removable : false
});



Ext.define('Carmen.LayerTree.TreeStore', {
  extend: 'Ext.data.TreeStore', 
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  },
  root: Carmen.LayerTree.noNode
     
});


