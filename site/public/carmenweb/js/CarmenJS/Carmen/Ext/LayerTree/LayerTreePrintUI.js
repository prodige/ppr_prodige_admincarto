var _T_LAYERTREEPRINT = function (key) {
	var root = "app.LayerTreePrint.";
	var tradution = _t(root + key);
  return (tradution==root + key ? key : tradution);
};
Ext.define("Carmen.LayerTreePrint.GridView", {
  xtype : 'layertreeprintgridview',
  extend : "Carmen.LayerTree.GridView"
})

Ext.define("Carmen.LayerTreePrint.Grid", {
  bufferedRenderer : false,
	extend : 'Ext.tree.Panel',
  margins: '5 0 5 5',
  layout : 'fit',
  /*maxHeight: 300,*/
/*  height: '100%',*/
  width: 'auto',
  useArrows: true,
  border: false,
  hideHeaders: true,
  deferRowRender : true,
  //disableSelection: true,
  //containerScroll:true,
  //fitToFrame: true,
  rootVisible: true,//rootVisible: false,
  
  viewType: 'layertreeprintgridview',
  appUI : null,
  initComponent : function(){
  	var me = this;
  	var appUI = this.appUI;
  	var store = this.store;
  	if ( !appUI ) return;
  	
    //me.on('render', function(){me.setHeight('100%')});
    me.viewConfig = {
      ddGroup: 'layerTreePrintPanel'
    };
    
    
    me.on('afterlayout', function(){
      var container = Ext.query( '#'+me.getId()+' .'+Ext.baseCSSPrefix + 'grid-item-container', false)[0];
      if ( container ) 
        container.setWidth(me.up().getWidth()-18);
      me.getView().autoExpandDataColumn();
    });
    
    me.columns = [
    {
        hideMode: 'visibility',
        xtype: 'treecolumn',
        dataIndex: 'text',
        width: '90%',
        cellWrap: true,
        cellTpl : [
              '<div class="row0" style="width:250px;">',
              '<div class="row0_first" style="width:{[20+(values.record.getDepth()*20)]}px;" >',
              '<tpl for="lines">',
                  '<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
                  '{parent.elbowCls}-<tpl if=".">line<tpl else>empty</tpl>" role="presentation"/>',
              '</tpl>',
              '<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}',
                  '<tpl if="isLast">-end</tpl>',
                  '<tpl if="expandable">-plus {expanderCls}</tpl>',
                  '" role="presentation"/>',
              '</div>',
              '<div class="row0_second">',
              
              '<tpl if="href">',
                  '{value}',
              '<tpl else>',
                  '<span class="{textCls} {childCls} layerTreeNode',
                  '<tpl if="!leaf">',
                    ' cmnGroupNode">',
                  '<tpl else>',
                    ' cmnLayerNode">',
                  '</tpl>',
                    '{value}',
                  '</span>',
                  '<br/>',
               '</tpl>', 
              '</div>',
              '</div>', {
                noCacheIcon : function(values){
                  var record = values.record;
                  var url = record.get('icon');
                  if ( url && url.indexOf('?')==-1 ) url+="?";
                  return (url ? url+'&time='+(new Date().getTime()) : url);
                }
              }
              
        ],
        listeners : {
          /*render : function(col, eopts) {
            col.variableRowHeight = false;
            return true;
          }*/
        
        }
    }, {
      xtype: 'actioncolumn',
      dataIndex: 'text',
      width: '9%',
      items: [{
        icon: '/carmenweb/images/plus.png',
        tooltip : 'Ajouter une couche ou un groupe',
        handler: function(grid, rowIndex, colIndex) {
          var current_group = store.getAt(rowIndex);
          var newItems = [];
          me.storeLegend.data.each(function(node){
            if ( !me.store.findRecord('nodeId', node.get('nodeId')) ){
              newItems.push(node.data)
            }
          });
          if ( !newItems.length ) {
          	Ext.Msg.alert('Ajouter une couche ou un groupe', "Tous les noeuds de l'arbre de légende sont placés dans l'arbre de la légende d'impression.");
            return;
          }
          var list = Ext.create('Ext.form.field.ComboBox', {
            fieldLabel : 'Choisissez la couche ou le groupe à ajouter',
            emptyText : 'Choisissez',
            store : Ext.create('Ext.data.JsonStore', {
            	autoLoad : true,
            	proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty : 'data'
                }
              },
            	fields : me.storeLegend.model.fields,
              data : {data : newItems}
            }),
            valueField : 'id',
            allowBlank : false
          })
          var win = Ext.create('Ext.window.Window', {
            title : "Synchronisation avec l'arbre de légende",
            layout : 'form',
            labelPosition : 'top',
            items : [list],
            buttonsAlign : 'center',
            buttons : [{
              text : 'Ajouter tout', 
              handler : function(btn){
              	Ext.Msg.confirm('Ajout de tous les noeuds manquants', "Confirmez-vous l'ajout de tous les noeuds manquants ?", 
              	function(confBtn){
              		if ( confBtn=="yes" ){
                    Ext.each(list.store.getRange().reverse(), function(record){
                      me.store.getRoot().insertChild(0, record.data);
                    });
              		}
                  btn.up('window').close();
              	})
              }
            }, {
              text : 'Ajouter', 
              handler : function(btn){
                if ( list.isValid() ){
                  me.store.getRoot().insertChild(0, list.getSelection().data);
                  btn.up('window').close();
                }
              }
            }, {
            	text : 'Annuler',
            	handler : function(btn){btn.up('window').close();}
            }]
          }).show();
        },                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {          
          if(r.data.leaf || r.data.depth) {                                                                      
            return "x-hidden-display";
          }else{
            return "";
          }
        },
        scope : this
      }, {
        icon: '/carmenweb/images/delete.png',
        tooltip : 'Supprimer la donnée de la carte',
        handler: function(grid, rowIndex, colIndex) {
          var current_node = store.getAt(rowIndex);
          var text = (current_node.data.type=="layer" ? 'de la couche' : 'du groupe')+' "'+current_node.data.text+'"';
        	Ext.Msg.confirm('Suppression '+text, "Confirmez-vous la suppression "+text+" de la légende d'impression"
        	, function(buttonId){
        		if ( buttonId!="yes" ) return;
            current_node.remove();
        	})
          
        },                                   
        getClass: function(v, metadata, r, rowIndex, colIndex, store) {    
          if ( !r.data.depth || !r.data.removable ){
            return "x-hidden-display";
          }      
          return "";
        },
        scope : this
      }]
    }];
    
    
    this.callParent(arguments);
    this.attachEventsChange();
  },
  plugins: [{
    ptype: 'nodedisabled'
  }]  
});

