Ext.define('Carmen.Generic.Form', {
  extend: 'Ext.form.Panel', 
  alias: 'widget.cmngenericform',
  bodyStyle : {padding : "10px"},
  scrollable: true,
  initComponent : function(){
  	var me = this;
  	this.callParent(arguments);
  	this.on('afterrender', function(){
    	this.body.on('scroll', function(){
    	  //console.trace();
    	  //me.scrollTo(this.getScrollLeft(), this.getScrollTop());
    	})
  	})
  },
  successCallback: function(form,record) {},
  
  /**
   * Saving the form to the server
   */ 
  save : function(callback, scope) {
    record = this.getRecord(); 
    if (record && this.isValid()) { 
      var form = this;
      this.updateRecord(record); // update the record with the form data
      record.setId(this.up('window').recordId);
      record.save({ // save the record to the server
        success: function(r) {
          // call the successCallback function
          form.successCallback(form,r);
          // if a callback given, launching callback
          if (scope==null)
            scope = r;
          if (callback)
            callback.apply(scope);
        },
        failure: Carmen.readFormFailure
      });
    } else { // display error alert if the data is invalid
      Ext.Msg.alert(_t("form.msgInvalidTitle"), _t("form.msgInvalid"))
    }
  },
  
  getRecordType : function(){
  	var record = this.getRecord();
  	if ( record && record instanceof Ext.data.Model ){
  		return record.$className.split('.').pop();
  	}
  	return null;
  },
  getValues : function(){
  	var values = this.callParent(arguments);
  	var record = this.getRecord();
  	if ( record && !Ext.isDefined(values[record.getIdProperty()]) ){
  		values[record.getIdProperty()] = record.getId();
  	}
  	return values;
  },
  /**
   * change the state of each field provided in components
   * @param {"Carmen.Model.LayerXXX"}  record       
   * @param {array}                    components   array of name's components to be checked
   * @param {array}                    extensions   array of suffixes to complete the list of components
   * @returns {undefined}
   */
  setStateFields: function(record, components, extensions, action) {
    action = ( typeof(action)=="undefined" ? "readonly" : action );
    var editable = (record.phantom);
    Ext.each(components, function(componentName) {
      for (var i=0; i<extensions.length; i++){
      	var extension = extensions[i];
        var component = this.down('[name='+componentName+extension+']')
        if ( !component ) return;
        if( action=="hide" ) {
          if( !editable ) {
            component.hide();
            component.setDisabled(true);
          } else {
            component.show();
          }
        } else if( action=="show" ) {
          if( !editable ) {
            component.show();
          } else {
            component.hide();
            component.setDisabled(true);
          }
        } else {
          if ( component.isFormField ){
            component.setReadOnly(!editable);
          } else {
            component.setDisabled(!editable);
          }
        }
      }
    }, this);
  }
});
