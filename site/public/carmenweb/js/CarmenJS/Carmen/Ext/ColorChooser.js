// Alkante SAS

Ext.define( 'Ext.ux.ColorChooser', {
    re : /^#?([A-F0-9]{6})$/i,
    allowBlank : true,
    extend: 'Ext.form.FieldContainer',
    mixins: [
        'Ext.form.Labelable',
        'Ext.form.field.Field'
    ],
    xtype: 'colorchooser',
    baseCls : "colorchooser",
    cls : "colorchooser",
    height : 25,
    layout: 'hbox',
    // default label
    defaults: {
      fieldLabel: '&nbsp;',
      labelSeparator: '&nbsp;'
    }, 
    
    // private
    initComponent: function() {

      this.colorBtn = Ext.create('Ext.Button', {
        tooltip: 'Modifier la couleur',
        tooltipType: 'title',
        cls: 'x-btn-icon',
        text: '<i class="fa fa-paint-brush fa-1x"></i>',
        scale: 'small',
        //flex: 20,
        maxWidth: this.height,
        height: this.height,
        margin: '0 5 0 5',
        padding: 0,
        xtype: 'button',
        //height: 18,
        //width: 18,
        listeners : {
          'click' : {
             fn : function(b,evt) {
               this.colorPanel.show()
             },
             scope : this
          }
        }
    	});
       
      this.colorPanel = new Ext.Window({
  	    title: 'Couleur',
  	    modal:true,
  	    autoDestroy :true,
  	    resizable:true,
  	    closeAction: 'hide',
  	    shadow : false,
  	    hidden: true,
  	    constrain : true,
  	    width : 200,
  	    height : 200,
        items : [{ 
          xtype : 'colorpicker',
          layout : 'fit',
          fieldLabel: "Couleur",
          value: this.color,
          listeners : {
            'select' : { 
              fn : function(cp, colorStr) {
                if (!this.colorPanel.hidden) {
                  this.colorPanel.hide();
                  this.color = colorStr;
                  this.updateColorFieldValue();
                  this.fireEvent('select', this, this.color);  
                }
              },
              scope : this
            }
          }
        }]
      });
  
      var rgb = this.colorStr2css(this.color);
      this.colorField = new Ext.form.DisplayField({
      	mode: 'local',
      	value : '',
      	fieldLabel: '',
      	style: {
          margin: '2px 2px 5px 0px',
          height: Math.max(10, this.height-2)+'px',
          width: '30px'
        }
      });
 
      this.items = [
        this.colorField, 
        this.colorBtn
      ];

      // propagation of the colorPalette select event
      //this.addEvents('select');
      
      this.on('afterrender', function() { this.updateColorFieldValue(); }, this );
      Ext.ux.ColorChooser.superclass.initComponent.call(this);
    },
    
    setValue : function(v) {
    	if ( !v ){
    		this.color = v;
        this.updateColorFieldValue();
    	} else {
        var test = v.match(this.re)
        if (test!=null) {
          this.color = test[1];
          this.updateColorFieldValue();
        }
    	}
    },
    
    getValue : function() {
      return this.color;
    },
    
    isValid : function() {
      return (Ext.isEmpty(this.color) ? this.allowBlank : this.re.test(this.color) );
    },
    
    //private
    updateColorFieldValue : function() {
      if (this.colorField.getEl()) {
        var rgb = this.colorStr2css(this.color);
        this.colorField.getEl().applyStyles({ 
          'background-color' : (rgb ? 'rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')' : 'transparent'),
          opacity : '0.7',  
          border : '1px solid black'
        });
      }
    },
    
    //private
    colorStr2css : function(colorStr) {
      var r,g,b;
      if (!colorStr) return null;
      r = parseInt(colorStr.slice(0,2),16);
      g = parseInt(colorStr.slice(2,4),16);
      b = parseInt(colorStr.slice(4),16);
      return {r : r, g : g, b : b};
    }    
});
