Ext.define('Carmen.ResultPanel', {
  extend: 'Ext.Panel',
  displayClass : 'cmn_resultPanel',	

  headerPanel : null,
  layerTreeInfoPanel : null,
  gridInfoPanel : null,

  layerTree : null,
  
  mapfile : null,
  map : null,
  id : null, 
 
  // count of queries which response is still being waited
  totalCount : 0,
  fullExtent : null,


  initComponents : function() {
	  if (this.id==null)
		this.id = ext.id();
	  this.headerPanel = new Ext.Panel({
	    id: this.displayClass +'_' +'headerPanel' + this.id,
	    autoScroll: true,
	    fitToFrame: true,
	    iconCls : 'cmnInfoGridZoomIcon',
	    header : true,
	    title : 'Interrogation des couches...',
	    anchor : '100% 50%'
	  }); 
	  this.layerTreeInfoPanel = new Ext.tree.TreePanel({
	    id: this.displayClass +'_' +'layerTreeInfoPanel' + this.id,
	    margins: '5 0 5 5',
	    useArrows: false,
	    animate: false,
	    border: false,
	    fitToFrame: true,
	    rootVisible: false,
	    root: {text: 'root'},
	    viewConfig : {autoFill:true, forceFit: false}
	  });
    this.gridInfoPanel = new Ext.grid.GridPanel({
	    id: this.displayClass +'_' + 'gridInfoPanel' + this.id,
	    store: new Ext.data.SimpleStore({
	      fields: [],
	      data: []
	    }),
	    columns: [],
	    title:'Résultats sélectionnés',
	    hidden: true,
	    autoScroll: true,
	    border: false,
	    anchor : '100% 50%',
	    tools:[{
			id:'xls',
			qtip: 'Export de la sélection au format Excel (XLS)'
		},
		{
			id:'csv',
			qtip: 'Export de la sélection au format CSV'
		}]
    });
	
    this.gridInfoFormPanel = new Ext.form.FormPanel({
		id: this.displayClass +'_' + 'gridInfoFormPanel' + this.id,
		hidden : true,
		fileUpload : true,
		waitTitle : "Traitement en cours..."
	});
  },

  // displaying results
  show : function(layerTree) {
    this.layerTree = layerTree.clone(this.transformNodeInfo);
    if (!this.rendered) {
      this.headerPanel.add(this.layerTreeInfoPanel);
      this.win.add(this.headerPanel);
      this.layerTreeInfoPanel.setRootNode(this.layerTree);
      
      this.win.add(this.gridInfoPanel);
      this.win.add(this.gridInfoFormPanel);
      this.win.setTitle(this.windowTitle);
      this.win.doLayout();
      this.win.show();
      
      this.gridInfoPanel.view.hmenu.add({
		itemId : 'stats',
		text: 'Statistiques',
		iconCls:'cmn-tool-stats'
	  });
    } 
    else {
      this.gridInfoPanel.hide();
    	// remove the old tree
    	this.layerTreeInfoPanel.getRootNode().destroy();
      // add the new one and render it
      this.layerTreeInfoPanel.setRootNode(this.layerTree);
      //this.layerTreeInfoPanel.getRootNode().render();
    }

    if (!this.win.isVisible()) {
      this.win.show();
    }
    
    // informing of server querying     
    this.headerPanel.setTitle('Interrogation des couches...');
    if (this.showGraphicalSelection) 
      this.map.clearGraphicalSelection();
    this.map.clearSelection();
    this.performQueries(position);
  },

  
  registerResults : function(response, queryType) {
	  var control = this;
	  this.queriesSent--;
	  this.totalCount = this.totalCount + parseInt(response.totalCount);  

    // updating headerPanel
    var headerPanel = Ext.getCmp(control.displayClass +'_' +'headerPanel');
    headerPanel.setTitle(this.totalCount + ' résultats');
		if (queryType==Carmen.Control.Info.SRC_MS) {
		  var currentExtent = Carmen.Util.strExtentToOlBounds(response.extent, Carmen.Control.Info.PUNCTUAL_RADIUS);
		  this.fullExtent = this.fullExtent == null ? currentExtent : this.fullExtent.extend(currentExtent); 
	    headerPanel.header.removeAllListeners();
	    headerPanel.header.on('click', 
	      function() {
	        control.map.zoomToSuitableExtent(control.fullExtent);
          // displaying graphical selection ? 
	        if (control.showGraphicalSelection) {
            control.map.clearGraphicalSelection();
            for(lname in response.results)  {
        		 	var node = control.layerTree.findChildByAttribute('layerName', lname);
		          // obedel patch : no highlight mode for wfs mapfile layer 
		          if (node!=null) {
                node.attributes.OlLayer.setDisplayMode(
                 Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
                 node.attributes.layerName, null, 
                 response.queryfile);
		          }
            }
	        }
	      }
	    );
	    
	    for(lname in response.results)  {
  		 	var node = control.layerTree.findChildByAttribute('layerName', lname);
        if (node!=null) {
          var olLayer = node.attributes.OlLayer; 
          olLayer.setDisplayMode(
           Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
           node.attributes.layerName, null, 
           response.queryfile);
          
          // registering selection
          if (olLayer.handleSelection(lname)) { 
      	    this.map.addToSelectionLayers(olLayer);
      	    olLayer.setSelectionStore(lname, 
      	      new Ext.data.SimpleStore({
                fields: response.results[lname].fields,
                data :  response.results[lname].data
              }));
          }
        }
      }
	    
	   
	    
	    
		}
  //updating result tree      
  control.attachResultsToTree(control.layerTree, response.results, queryType);

  if (this.queriesSent==0)
    control.layerTree.filter(control.filterNodeLayerWithResults);
/*
    // initial selection
    // TODO : See how to factorize code by firing click on header panel 
    // or defining a select all function
    if (control.showGraphicalSelection) {
      control.map.clearSelection();
      var layerNodes = control.layerTree.collectChildNodes(
  			function (n) {
  				return ('type' in n.attributes && 
  					n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER &&
  					'queryType' in n.attributes && 
  					n.attributes.queryType==Carmen.Control.Info.SRC_MS);
  			});
      
      for (var i=0; i<layerNodes.length; i++)  {
        var node = layerNodes[i];
        node.attributes.OlLayer.setDisplayMode(
           Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
           node.attributes.layerName, null, 
           response.queryfile);
      }
    }
    */ 	
  },
  

  // traditionnal query on mapserver layer
  performQuery : function (queryParams) {
    
    
    // prepraring answer callback
    var received = function (response) {
      response = Ext.decode(response.responseText);
	  	this.registerResults(response, Carmen.Control.Info.SRC_MS); 	
    }
    
    // launching the request
    this.queriesSent++;
    
    var url = '/services/GetInformation/index.php';
    //console.log(url);
    //console.log(queryParams);
    Ext.Ajax.request({
      url: url,
      success: received,
      failure: function (response) { 
         this.queriesSent--;
         Carmen.Util.handleAjaxFailure(response, this.windowTitle, true); 
      },
      headers: {
        'my-header': 'foo'
      },
      scope : this,
      params: queryParams
    });
  }, 


  buildParams : function(nodes, position, fieldsFilter, mapfile) {
    
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];
    
    for (var i=0; i<nodes.length; i++) {
		layerNames.push(nodes[i].attributes.layerName);
	    fieldsList.push(nodes[i].attributes.infoFields);
	    briefFieldsList.push(nodes[i].attributes.briefFields);
	    baseQueryURLList.push(nodes[i].attributes.baseQueryURL);
    }
    
    var fieldsDesc = new Array();
    for (var i=0; i<fieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(fieldsList[i],';');
      fieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        fieldsDesc[i][j]=desc;   
      }
      if (fieldsFilter) 
        fieldsDesc[i] = Carmen.Util.array_filter(fieldsDesc[i], fieldsFilter);
    }

    var briefFieldsDesc = new Array();
    for (var i=0; i<briefFieldsList.length; i++) {
      var list = Carmen.Util.chompAndSplit(briefFieldsList[i],';');
      briefFieldsDesc[i] = new Array();
      for (var j=0; j<list.length; j++) {
        var desc  = Carmen.Util.decodeCarmenFieldDesc(list[j], baseQueryURLList[i]);
        briefFieldsDesc[i][j]=desc;   
      }
      briefFieldsDesc[i] = Carmen.Util.array_filter(briefFieldsDesc[i], this.fieldFilter_TXT);
    }

    // linking kept info fields and brief fields 
    // to the corresponding layer node in the tree
    for (var i=0; i<nodes.length; i++) {
   	  nodes[i].attributes.infoFieldsDesc = fieldsDesc[i];
   	  nodes[i].attributes.briefFieldsDesc = briefFieldsDesc[i];
    }

    var params = {
      layers : Ext.util.JSON.encode(layerNames),
      fields : Ext.util.JSON.encode(fieldsDesc),
      briefFields : Ext.util.JSON.encode(briefFieldsDesc),
      shape : Carmen.Util.positionToStr(this.positionToBounds(position)),
      map : mapfile,
      showGraphicalSelection : this.showGraphicalSelection }
    return params;    
  },

  logvar : function(value, name, tab) {
	var logv = (value==null) ? "null" : value;
	tab[name] = logv;
  },
  
  printlog : function(tab) {

	Ext.Ajax.request({
      url: '/services/GetInformation/log.php',
      method: 'POST',
  	  success : function (response) { 
      	 Ext.Msg.alert(control.windowTitle, response.msg ? response.msg : "Commande log enregistrée");
      },
  	  failure: function (response) { 
      	 Ext.Msg.alert(control.windowTitle, 'Problème dans l\'écriture du fichier Log');
      },
      params : { 
        "log_array" : Ext.encode(tab)
      } 
    });
  
  },
	
  performWMSQuery : function(layer, position) {
    
    var control = this;
   	var received = function(response) {
	  //var logs = {}; //obedel log
	  var features = [];
      //control.logvar(response, 'response',logs); //obedel log
	  var contentStr = response.getResponseHeader("Content-Type")
					|| response.getResponseHeader("Content-type");
	  
	  //control.logvar(contentStr, 'contentStr',logs); //obedel log
	   	
	  if (contentStr!=null) { 
	   	  contentStr = (contentStr.split(";")[0]).trim();
	   	  //control.logvar(contentStr, 'contentStrSplit',logs); //obedel log
	   	  var fmt = control.WMSResponseFormats[contentStr];
	   	  //control.logvar(fmt, 'fmt',logs); //obedel log
    	  if (fmt) { 
     	     var rep = response.responseXml || response.responseText;
     	     //control.logvar(rep, 'rep',logs); //obedel log
     	     //console.log(Url.decode(rep));
     	     //console.log(Url._utf8_encode(rep));
     	     //console.log(Url._utf8_decode(rep));
     	     //console.log(response.responseXml || response.responseText);
     	     features = fmt.read(response.responseXml || response.responseText);
     	     //control.logvar(features, 'features',logs);
     	     //console.log(features);
    	   }
	   	}
		//control.printlog(logs);//obedel log	
		var results = {};
		var extentStr = Carmen.Util.positionToStr(control.map.getExtent());

		for (var i=0; i<features.length; i++) {
			if (!(features[i].type in results)) {
				results[features[i].type] = {
					"fields" : [],
					"briefFields" : [],
					"data" : [],
					"briefData" : []
				}
				for (var f in features[i].attributes) {
					results[features[i].type].fields.push(f);				
					results[features[i].type].briefFields.push(f);
				}
			}
			var featureData = [];
			for (var f in features[i].data) 
				featureData.push(features[i].data[f]);	
			results[features[i].type].briefData.push(featureData);
			results[features[i].type].data.push(featureData);
		}
			
		var responseJS = {
			"totalCount" : features.length,
			"results" : results
		};
			
		control.registerResults(responseJS, Carmen.Control.Info.SRC_WMS);
    };
    
    
    this.queriesSent++;
    
    Ext.Ajax.request({
      url: '/services/GetInformation/proxy.php',
      method: 'GET',
  	  success : function (response) { 
      	 received(response);
      },
  	  failure: function (response) { 
      	 // uncomment to display wms querying errors
      	 //var msg = Ext.valueFrom(response.responseText, "Service WMS indisponible");
      	 //Ext.Msg.alert(control.windowTitle, msg);
      	 control.registerResults(
      	   Carmen.Control.Info.noResultsResponse,
      	   Carmen.Control.Info.SRC_WMS);
      },
      headers: {
        'my-header': 'foo'
      },
      params : { 
        "proxy_url" : layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()),
        "force_utf8_output" : 1 // needed because ExtJS waits for UTF8 encoded response...
      } 
    });
    //console.log(layer.getFeatureInfoURL('', position, this.map.size, this.map.getExtent()));
  }, 


  attachResultsToTree : function(tree, results, queryType) {
		for(lname in results)  {
		 	var node = tree.findChildByAttribute('layerName', lname);
			if (node!=null) { 
			  var data = results[lname].data;
			  node.attributes.layerAlias= node.text;
			  node.setText(node.text + '  (' + results[lname].data.length + ' résultats)');
			  node.attributes.infoData = results[lname];
			  node.attributes.queryType = queryType;
			  node.attributes.queryfile = results[lname].queryfile;
			  			   
			  // add click listener to the node  	 	   
			  var control = this;
			  var gPanel = this.gridInfoPanel;
	   
			  node.addListener('click', 
			    function() {
			    	if (this.attributes.queryType==Carmen.Control.Info.SRC_MS) {
						// zoom on the layer selection extent
						var extentStr = this.attributes.infoData.extent;
						var minScale = this.attributes.minScale;
						var maxScale = this.attributes.maxScale;
						//console.log(this.attributes.infoData); 
						var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
						// obedel patch : no highlight mode for wfs mapfile layer
						control.map.clearGraphicalSelection();
						if (control.showGraphicalSelection) {
  						  this.attributes.OlLayer.setDisplayMode(
							Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP, 
							this.attributes.layerName, null, 
							this.attributes.queryfile);
						  }
						  control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
			    	} 
			    	else if (this.attributes.queryType==Carmen.Control.Info.SRC_WMS) {
			    		// building default fieldsDesc from fields name
			    		var infoFieldsDesc = [];
			    		for (var i=0; i<this.attributes.infoData.fields.length; i++) {
			    			var fld = this.attributes.infoData.fields[i];
			    			infoFieldsDesc.push({
				    				name : fld,
				    				alias : fld,
				    				type : 'TXT'
			    				});
			    		}
			    		this.attributes.infoFieldsDesc = infoFieldsDesc; 			    		
			    	}
						// configuring and showing the result table panel
						var gConfig = control.buildGridConfig(
						  this.attributes.infoFieldsDesc, 
						  this.attributes.infoData, 
						  this.attributes.layerName,
						  this.attributes.olLayer,
						  this.attributes.queryType
						);
						
						// configuring xls export...
						var xlsTool = gPanel.getTool('xls');
						xlsTool.removeAllListeners();
						xlsTool.on('click',
						  function() {
							var url = '/services/GetXLS/index.php';
							var f = this.control.gridInfoFormPanel.getForm(); 
							queryParams = { 
							  field : Ext.encode(this.node.attributes.infoFieldsDesc),
							  data : Ext.encode(this.node.attributes.infoData.data),
							  selectionName : this.node.attributes.layerAlias
							};
							Carmen.Util.doFormUpload(f, url, queryParams);
						  }, 
						  { control : control, node : this }
						);
						
						var csvTool = gPanel.getTool('csv');
						csvTool.removeAllListeners();
						csvTool.on('click',
						  function() {
							var url = '/services/GetXLS/index.php';
							var f = this.control.gridInfoFormPanel.getForm(); 
							queryParams = { 
							  field : Ext.encode(this.node.attributes.infoFieldsDesc),
							  data : Ext.encode(this.node.attributes.infoData.data),
							  selectionName : this.node.attributes.layerAlias,
							  output : "CSV"
							};
							Carmen.Util.doFormUpload(f, url, queryParams);
						  }, 
						  { control : control, node : this }
						);
							
						gPanel.reconfigure(gConfig.store, gConfig.columnModel)
						gPanel.setTitle(this.text);
						gPanel.node = node;
						// adding zooming and highlighting to row click
						if (this.attributes.queryType==Carmen.Control.Info.SRC_MS) {
							gPanel.on('rowclick', 
								function(grid, rowIndex, e){
									var rec = grid.store.getAt(rowIndex);
									var extentStr = rec.get('extent');
									var bounds = Carmen.Util.strExtentToOlBounds(extentStr, Carmen.Control.Info.PUNCTUAL_RADIUS);
									control.map.zoomToSuitableExtent(bounds, minScale, maxScale);
									var fid = rec.get('fid');
									var layerName = rec.get('layerName');
									// retrieving the openLayer layer linked with the selected row
									var n = control.layerTree.findChildByAttribute('layerName', layerName);
 									var olLayer = n.attributes.OlLayer;                  
									// changing symbology to highlight selection
							    olLayer.setDisplayMode(
							      Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
							      layerName, fid);

								});
								
						}
						
						// configuring stats menu...
						var statsBtn = gPanel.view.hmenu.items.get('stats');
						statsBtn.purgeListeners()
						statsBtn.on('click',
							function() {
								var layerName = this.node.attributes.layerName;
								var colIndex = this.gridPanel.view.hdCtxIndex;
								var cm = this.gridPanel.getColumnModel();
								var colName = cm.getDataIndex(colIndex);
								colName = colName.replace(/[0-9]+$/,'');
								control.queryStatsOnColumn(layerName, colName);	
							},
							{ node : this, gridPanel : gPanel, control : control }
						);
						// hack needed to force layout
						if (!gPanel.isVisible()) {
						  gPanel.show();
						  control.win.setSize(control.win.getSize().width, control.win.getSize().height+1);
						}  
		    }, node);
			// adding result nodes under the layer node  	 	   
			for (var i=0; i<data.length; i++) {
			  var child = this.buildResNode(results[lname].briefFields, results[lname].briefData[i], i, node.attributes.queryType);
			  node.appendChild(child);
			}
			// but do not show them
			if (node.rendered)
		 		node.collapse();
		 	}
		}
	},

  buildResNode : function(fields, data, nodeIndex, queryType) {
	  var control = this;   
    // building desc name
    var name = data.length>2 ? data[0] : data[data.length-1];;
    if (fields.length>3) {
    	name = name + ' (';
      for (var i=1; i<fields.length-2; i++) {
        name = name + fields[i] + ' : ' + data[i] + ', ';
      }
      name = name.substr(0, name.length-2) + ')';
    }
    
    var node = new Ext.tree.TreeNode({
      text : name,
      leaf : true,
//      uiProvider : Carmen.TreeNodeInfoUI,
      listeners: {
        'click' :  
          {
            fn: function(n , evt) { 
              n.parentNode.fireEvent('click',n);
              var index = n.parentNode.indexOf(n);
              control.gridInfoPanel.getView().focusRow(index);
              control.gridInfoPanel.getSelectionModel().selectRow(index);
							if (queryType==Carmen.Control.Info.SRC_MS) {
	              // changing symbology to highlight selection
	              control.map.clearGraphicalSelection();
	              n.parentNode.attributes.OlLayer.setDisplayMode(
	                Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP, 
	                n.parentNode.attributes.layerName, n.attributes.fid);

	            }
            }
          } 
      }
    });
    
    node.attributes.type = Carmen.Control.LayerTreeManager.NODE_FEATURE;
    // registering the id of the feature linked to the node
    if (queryType==Carmen.Control.Info.SRC_MS)
    	node.attributes.fid = data[data.length-1];
    return node;
  },           

 
  extentRenderer : function(v) {
  	return '<div class="cmnInfoGridZoomIcon"> </div>';
  },


  buildGridConfig: function (fieldsDesc, result, layerName, olLayer, queryType) {
    var fields = new Array();
    var columns = new Array();
    columns.push(new Ext.grid.RowNumberer());
    if (queryType==Carmen.Control.Info.SRC_MS) {
	    columns.push({
	      id : 'extent',
	      header : '',
	      width : 20,
	      resizable: true,
	      renderer: this.extentRenderer,
	      dataIndex: 'extent'
	    });
		}    
    for (var i=0; i<fieldsDesc.length; i++) {
    	var desc = fieldsDesc[i];
    	var fDesc = {name : desc.name + i.toString(), type : Carmen.Util.carmenTypeToExtType(desc.type)};
    	switch (desc.type) {
        case 'date' : 
    	    fDesc['dateFormat'] = Carmen.Util.carmenDateTypeToExtDateFormat(desc.type);
    	    break;
    	  default:
    	    break;
    	}
    	fields.push(fDesc);
    	
    	var colDesc = {
    		id: Ext.id(), //desc.name + i.toString(),
    		header: desc.alias,
    		width: 200,
    		sortable: true,
    		resizable: true,
    		renderer: Carmen.Util.carmenTypeToExtRenderer(desc.type),
    		dataIndex: desc.name + i.toString()
    	};
    	columns.push(colDesc);
    }
		if (queryType==Carmen.Control.Info.SRC_MS) {
	    fields.push({name : 'extent'});
	    fields.push({name : 'fid'});
		}
    fields.push({name : 'layerName'});
  
    for (var i=0; i<result.data.length;i++) {
    	result.data[i] = result.data[i].concat([layerName]);
    }  

    var config = {
	  	store : new Ext.data.SimpleStore({
	        fields: fields,
	        data : result.data
	    }),
	    columnModel : new Ext.grid.ColumnModel({
	      columns: columns 
	    })
	  };
   
	  return config;
  },

  queryStatsOnColumn : function(layerName, colName) {
	var url = '/services/GetStats/index.php';
	queryParams = { 
		fieldName : colName,
		layer : layerName,
		map : this.mapfile
	};
	
	var received = function(response) {
		response = Ext.decode(response.responseText);
		
		var stats = response.tabStat;
		var labelMapping = {
			"count" : "Effectif",
			"min" : "Minimum",
			"max" : "Maximum",
			"sum" : "Somme",
			"variance" : "Variance",
			"average" : "Moyenne",
			"stddeviation" : "Ecart type"
		};
		var statWin = new Ext.Window({
			id : this.displayClass +'_' +'StatsWindow',
			layout:'anchor',
			width:320,
			height: 200,
      constrain : true,
			plain: true,
			title: 'Statistiques pour le champ ' + colName + ' de la couche ' + layerName,
			modal:false,
			autoDestroy :true,
			resizable:true,
			closeAction: 'hide',
			shadow : false
		  });
		  var infoPanel = new Ext.form.FormPanel({
			id: this.displayClass +'_' +'StatsInfoPanel',
			autoScroll: true,
			fitToFrame: true
			//labelWidth: 80,
			//labelPad: 3
		  });
		  for (var field in stats) {
			if (labelMapping[field]) {
				var infoField = new Ext.form.DisplayField({
					fieldLabel : labelMapping[field],
					value : stats[field]
				});
				infoPanel.add(infoField);
			}
		  }
		statWin.add(infoPanel);
		statWin.show();
	};
	Ext.Ajax.request({
      url: url,
      method: 'GET',
  	  success : function (response) { 
      	 received(response);
      },
  	  failure: function (response) { 
      	 var msg = Ext.valueFrom(response.responseText, "Service Statistisques indisponible");
      	 Ext.Msg.alert(control.windowTitle, msg);
      },
      params : queryParams,
      scope : this
    });
	
	
	alert("Statistiques pour la couche " + layerName + " sur la colonne " + colName);
  },


  // Filter and transform function used on tree
  filterNodeLayerQueryable :  function(node) {
  	var res = true;
    //console.log(node.attributes.text);
  	res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.OlLayer.getVisibility(node.attributes.layerName)
       && ((node.attributes.infoFields && node.attributes.infoFields.length>0) ||
       	   (node.attributes.OlLayer instanceof Carmen.Layer.WMSGroup)) 
      ));
    //console.log(res);  	
    return res;
  },

  filterNodeLayerWithResults : function(node) {
  	var res = true;
    res = ('type' in node.attributes) && 
      ((node.attributes.type == Carmen.Control.LayerTreeManager.NODE_GROUP && node.hasChildNodes() && node.getDepth()>0) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER && node.attributes.infoData!=null) ||
       (node.attributes.type == Carmen.Control.LayerTreeManager.NODE_FEATURE));
    return res;
  },

  transformNodeInfo : function(atts) {
  	var cloneAtts = Carmen.Util.clone(atts);
/*
  	if (cloneAtts.text!=Carmen.Control.LayerTreeManager.TREE_NODE_ROOT_LABEL)
  	   cloneAtts.uiProvider = Carmen.TreeNodeInfoUI;
  	else
  	   cloneAtts.uiProvider = Carmen.RootTreeNodeInfoUI;  	  
*/ 
  	cloneAtts.cls = 'none';
    cloneAtts.infoData = null;  	
  	cloneAtts.listeners = {};
  	cloneAtts.expandable = true;
  	if (cloneAtts.type == Carmen.Control.LayerTreeManager.NODE_LAYER) {
  		cloneAtts.icon = '/IHM/images/NoTreeIcon.gif';
  		cloneAtts.iconCls = 'cmnInfoGridZoomIcon';
  	}  	  
  	return cloneAtts;
  },
  
  fieldFilter_URL : function(field) {
    return field.type=='URL';
  },
  
  fieldFilter_TXT : function(field) {
    return field.type=='TXT';
  },
  
  positionToBounds : function(position) {
		var bounds = null;
  	if (position instanceof OpenLayers.Bounds) {
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.left, position.bottom));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.right, position.top));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    } 
    else { // it's a pixel
      // if no tolerance
      // this.showInfoWindow(this.map.getLonLatFromPixel(position));
      // with tolerance
      var tol = Carmen.Util.INFO_TOOL_XY_TOLERANCE;
      var minXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x-tol, position.y-tol));
      var maxXY = this.map.getLonLatFromPixel(
        new OpenLayers.Pixel(position.x+tol, position.y+tol));
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,
          maxXY.lon, maxXY.lat);
    }
	return bounds;
  }
  
});

Ext.reg('resultPanel', 'Carmen.ResultPanel');
