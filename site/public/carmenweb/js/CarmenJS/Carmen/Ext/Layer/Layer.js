

function splitPattern(msStylePattern, indice) {
  if (Ext.isNumber(msStylePattern)) return msStylePattern;
  if (Ext.isString(msStylePattern) && msStylePattern != "" && msStylePattern.split(' ').length > indice) {
    return msStylePattern.split(' ')[indice];
  }
  return null;
}

function getAlignValueFromNumber(value) {
  const number = value * 1;
  const alignStr = {
    1: 'left',
    2: 'center',
    3: 'right'
  }

  return alignStr[number] ? alignStr[number] : null;
}

/**
 * Carmen.Model.Field
 */
Ext.define('Carmen.Model.Field', {
  extend: 'Ext.data.Model',
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  },
  //idProperty : 'fieldId',

  fields: [{
    // Field Id (not saved)
    name: "fieldId",
    persist: true,
    type: "int",
    allowNull: false,
    defaultValue: 0
  },
  // layer record (not saved)
  {
    name: 'layer',
    persist: false,
    allowNull: false,
    serialize: function (layer) {
      return null;
    }
  },
  // database fields record 
  { name: "fieldType", type: "int", defaultValue: 1, convert: function (value) { if (Ext.isObject(value)) return value.fieldTypeId; return value; } },
  { name: "fieldRank", type: "int", defaultValue: 1 },
  { name: "fieldAlias", type: "string" },
  { name: "fieldName", type: "string" },
  { name: "fieldDataName", type: "string" },
  { name: "fieldUrl", type: "string" },
  { name: "fieldHeaders", type: "boolean", defaultValue: false },
  { name: "fieldOgc", type: "boolean", defaultValue: false },
  { name: "fieldQueries", type: "boolean", defaultValue: false },
  { name: "fieldTooltips", type: "boolean", defaultValue: false },
  { name: "fieldDuplicate", type: "boolean", defaultValue: false },
  {
    name: "fieldDatatype", type: "string", defaultValue: FIELD_DATATYPES.DEFAULT
    , convert: function (value) { if (Ext.isObject(value)) return value.fieldDatatypeCode; return value; }
  }
  ]
});


/**
 * Carmen.Model.GeojsonParams
 */
Ext.define('Carmen.Model.GeojsonParams', {
  extend: 'Ext.data.Model',
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  },

  geojsonParams: [
    // database fields record 
    { name: "geojsonParamsId", persist: true, type: "int", allowNull: false, defaultValue: -1 },
    { name: 'layer', persist: false, allowNull: false, serialize: function (layer) { return null; } },
    { name: "geojsonDistance", type: "int", serialize: function (value) { return value * 1; } },
    { name: "geojsonSingleColor", type: "boolean", persist: true, defaultValue: false, serialize: function (value) { return value ? 1 : 0; } },
    { name: "geojsonColor", type: "string" },
    { name: "geojsonFixedSize", type: "boolean" },
    { name: "geojsonSize", type: "int", serialize: function (value) { return value * 1; } },
    { name: "geojsonTransitionEffect", type: "boolean" },
    { name: "geojsonZoomOnClick", type: "boolean" }
  ]
});

/**
 * Carmen.Model.VectorTileParams
 */
Ext.define('Carmen.Model.VectorTileParams', {
  extend: 'Ext.data.Model',
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  },

  vectorTileParams: [
    // database fields record 
    { name: "vectorTileParamsId", persist: true, type: "int", allowNull: false, defaultValue: -1 },
    { name: 'layer', persist: false, allowNull: false, serialize: function (layer) { return null; } },
    { name: "url", type: "string" },
  ]
});

/**
 * Carmen.Model.MsLabel
 */
function convertToNumberOrNull(value) {
  if (value == null || !Ext.isDefined(value)) return null;
  return Ext.num(value, 0);
}
Ext.define('Carmen.Model.MsLabel', {
  extend: 'Ext.data.Model',
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  },

  getLabelMapping: (getLabelMapping = function (data) {
    if (Ext.isDefined(data.layer)) data = data.layer;
    if (Ext.isDefined(data.data)) data = data.data;
    if (Ext.isDefined(data.msLabel)) data = data.msLabel;
    return data;
  }),
  fields: [
    // layer record (not saved)
    {
      name: 'layer',
      persist: false,
      allowNull: false,
      serialize: function (layer) {
        return null;
      },
    },
    { name: "msLabelMinScale", type: "int", mapping: "minscaledenom", defaultValue: null, convert: convertToNumberOrNull() },
    { name: "msLabelMaxScale", type: "int", mapping: "maxscaledenom", defaultValue: null, convert: convertToNumberOrNull() },
    { name: "msLabelForce", type: "boolean", mapping: "force", defaultValue: true, serialize: function (value) { return value ? "ON" : "OFF"; } },
    { name: "msLabelFontColor", type: "string", mapping: "color", defaultValue: Carmen.Defaults.LAYER_LABELS.FONTCOLOR },
    { name: "msLabelTest", type: "string", mapping: "test", defaultValue: Carmen.Defaults.LAYER_LABELS.TEST },
    {
      name: "msLabelMultiLine", type: "boolean", mappingSerialize: "multiline",
      mapping: (data) => {
        return !!data.wrap;
      },
      defaultValue: false
    },
    {
      name: "msLabelWrapLine", type: "string", mappingSerialize: "wrap",
      mapping: (data) => {
        // conversion de char numerique en ascii/unicode

        // si c'est un caractère spéciale (ascii < 33) (espace, retour à la ligne, etc...)
        if (data.wrap < 33) {
          switch (data.wrap) {
            case 10: return '\n';
          }
        }

        // caractère 
        return data.wrap ? String.fromCharCode(data.wrap) : "";
      },
      defaultValue: null
    },
    {
      name: "msLabelalign", type: "string", mappingSerialize: "align",
      mapping: (data) => {
        const number = data.align * 1;
        if ((number >= 1) && (number <= 3)) {
          return getAlignValueFromNumber(number);
        }
        else if (data.align instanceof String) {
          return data.align;
        }

        return null;
      },
      defaultValue: null
    },
    {
      name: "msLabelContour", type: "boolean", mappingSerialize: "contour",
      mapping: (data) => {
        if (data.styles && data.styles instanceof Array) {
          return !!data.outlinecolor;
        }

        return false;
      },
      defaultValue: null
    },
    {
      name: "msLabelShadow", type: "boolean", mappingSerialize: "shadow",
      mapping: (data) => {
        if (data.styles && data.styles instanceof Array && data.styles.length > 0) {
          return !!data.styles[0].color;
        }

        return false;
      },
      defaultValue: null
    },
    { name: "msLabelRepeatDistantce", type: "string", mapping: "repeatdistance", defaultValue: 0 },
    { name: "msOutlineColor", type: "string", mapping: "outlinecolor", defaultValue: null },
    { name: "msoutlinewidth", type: "string", mapping: "outlinewidth", defaultValue: null },
    { name: "msBackgroundShadowColor", type: "string", mapping: "backgroundshadowcolor", defaultValue: null },
    {
      name: "msBackgroundShadowSize", type: "string", mappingSerialize: "backgroundshadowsize",
      mapping: (data) => {
        return data.shadowsizex;
      },
      defaultValue: null
    },
    { name: "msLabelFont", type: "string", mapping: "font", defaultValue: Carmen.Defaults.LAYER_LABELS.POLICE },
    { name: "msLabelFontSize", type: "int", mapping: "size", defaultValue: Carmen.Defaults.LAYER_LABELS.FONTSIZE, convert: convertToNumberOrNull() },
    { name: "msLabelPosition", type: "string", mapping: "position", defaultValue: LABELPOSITION.TYPES[LABELPOSITION.DEFAULT].value },
    {
      name: "msLabelOffsetX", type: "number", defaultValue: null, mappingSerialize: "offsetx"
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.offsetx == 0 && data.offsety == 0 ? this.defaultValue : data.offsetx);
      }
    },

    {
      name: "msLabelOffsetY", type: "number", defaultValue: null, mappingSerialize: "offsety"
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.offsetx == 0 && data.offsety == 0 ? this.defaultValue : data.offsety);
      }
    },
    {
      name: "msLabelOffsetActif", type: "boolean", defaultValue: false, comment: "", persist: false
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return !(data.offsetx == 0 && data.offsety == 0) || this.defaultValue;
      }
    },

    {
      name: "msLabelShadowColor", type: "string", mappingSerialize: "shadowcolor", mapping: function (data) {
        if (data.styles && data.styles instanceof Array && data.styles.length) {
          return data.styles[0].color;
        }
      }
    },

    {
      name: "msLabelShadowSizeX", type: "number", defaultValue: null, mappingSerialize: "shadowsizex"
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.shadowcolor !== null ? data.shadowsizex : this.defaultValue);
      }
    },
    {
      name: "msLabelShadowSizeY", type: "number", defaultValue: null, mappingSerialize: "shadowsizey"
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.shadowcolor !== null ? data.shadowsizey : this.defaultValue);
      }
    },
    {
      name: "msLabelShadowActif", type: "boolean", defaultValue: false, comment: "", persist: false
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.shadowcolor !== null);
      }
    },

    {
      name: "msLabelAngle", type: "string", mapping: "angle", defaultValue: null, comment: "",
      serialize: function (value, record) {
        var actif = record.get('msLabelAngleActif');
        var angleType = record.get('msLabelAngleType');
        if (actif) {
          if (angleType == 'degrees') {
            return record.get('msLabelAngleDegrees');
          } else if (angleType == 'fields') {
            return record.get('msLabelAngleField');
          }
        }
        return null;
      }
    },
    {
      name: "msLabelAngleActif", type: "boolean", defaultValue: false, comment: ""
      , persist: false
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return data.angle !== null && data.angle !== "" && data.angle !== 0;
      }
    },
    {
      name: "msLabelAngleDegrees", type: "number", defaultValue: null, comment: ""
      , persist: false
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.angle !== null && data.angle !== "" && Ext.isNumber(data.angle) ? data.angle : this.defaultValue);
      }
    },
    {
      name: "msLabelAngleField", type: "string", defaultValue: null, comment: ""
      , persist: false
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.angle !== null && data.angle !== "" && !Ext.isNumber(data.angle) ? data.angle : this.defaultValue);
      }
    },
    {
      name: "msLabelAngleType", type: "string", defaultValue: 'degrees', comment: ""
      , persist: false
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return this.defaultValue;
        return (data.angle !== null && data.angle !== "" ? (Ext.isNumber(data.angle) ? "degrees" : "fields") : this.defaultValue);
      }
    },
    {
      name: "msLabelAnchorSymbol", type: "boolean"
      , mappingSerialize: "geomtransform"
      , mapping: function (data) {
        data = getLabelMapping(data);
        if (data === null) return false;
        if (!data.styles) return false;
        if (!data.styles.length) return false;
        return data.styles[0].geomtransform == "labelpnt";
      }, serialize: function (value) { return (value ? "labelpnt" : null); }
    }
  ]
});


function returnValueIf(func, value, record) {
  if (func(value, record)) return value;
  return null;
}
function isHatchSymbol(value, record) {
  return record.get('msStyleSymbol') == Carmen.Dictionaries.MS_SYMBOLES.TYPES.HATCHSYMBOL;
}
function escapeExpression(text) {
  return text;
}

function getCodeAnalyse(analyseId) {
  var filters = [].concat(LEX_ANALYSE_TYPE.getFilters().items);
  LEX_ANALYSE_TYPE.clearFilter(true);
  var result = (analyseId ? LEX_ANALYSE_TYPE.findRecord("valueField", analyseId).get("codeField") : null);
  LEX_ANALYSE_TYPE.addFilter(filters);
  return result;
}
/**
 * Carmen.Model.MsClass
 */
Ext.define('Carmen.Model.MsClass', {
  extend: 'Ext.data.Model',
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  },

  fields: [{
    // Analyze Id (not saved)
    name: "msClassIndex",
    persist: false,
    type: "int",
    allowNull: false,
    defaultValue: 0,
    mapping: "index"
  },
  // layer record (not saved)
  {
    name: 'layer',
    persist: false,
    allowNull: false,
    serialize: function (layer) {
      return null;
    }
  },
  { name: "msClassLegendIcon", type: "string", persist: false, mapping: "icon.image" },
  {
    name: "msClassExpression", type: "string", mapping: "expression"
    , serialize: function (value, record) {
      var layer = record.get('layer'); if (!layer) return null;
      var type = record.get('layer').get('msGeometryType'); if (!type) return null;
      if (Ext.isDefined(Carmen.Dictionaries.RASTERTYPES.TYPES[type])) return null;
      var analyse = record.getAnalyse(); if (!analyse) return null;
      var msClassItem = record.get('layer').get('msClassItem');
      var msStyleSizeField = record.get('layer').get('msStyleSizeField');

      var msStyleMinSize = record.get('layer').get('msStyleMinSize');
      var msStyleMaxSize = record.get('layer').get('msStyleMaxSize');

      var expression = null
        , useFields = []
        , fieldIndexes = Ext.Array.toMap(record.getFields(), "name")
        , fields = record.getFields();
      switch (analyse) {
        case LEX_ANALYSE_TYPE.TYPES.UNIQSYMBOL:
          return null;
          break;
        case LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL:
          record.set('msStyleSizeField', "[" + msStyleSizeField + "]");
          record.set('msStyleMinSize', msStyleMinSize);
          record.set('msStyleMaxSize', msStyleMaxSize);
          record.commit();
          return null;
          break;
        case LEX_ANALYSE_TYPE.TYPES.UNIQVALUE:
          expression = new Ext.XTemplate(
            '<tpl if="data.msClassValue !== &quot;&quot;">' +
            '{[escapeExpression(values.data.msClassValue)]}' +
            '<tpl else>' +
            '(length("[{msClassItem}]") eq 0)' +
            '</tpl>', { escapeExpression: escapeExpression });
          break;
        case LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR:
        case LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL:
          expression = new Ext.XTemplate(
            '<tpl if="data.msClassLowerBound !== &quot;&quot; &amp;&amp; data.msClassUpperBound !== &quot;&quot;">' +
            '(([{msClassItem}] >= {data.msClassLowerBound}) AND ([{msClassItem}] < {data.msClassUpperBound}))' +
            '<tpl elseif="data.msClassLowerBound !== &quot;&quot;">' +
            '([{msClassItem}] >= {data.msClassLowerBound})' +
            '<tpl elseif="data.msClassUpperBound !== &quot;&quot;">' +
            '([{msClassItem}] < {data.msClassUpperBound})' +
            '</tpl>'
          );
          break;
      }
      if (expression) {
        return /*escapeExpression*/(expression.apply({ data: record.getData(), msClassItem: msClassItem }));
      }
      return null;
    }
  },
  {
    name: "msClassName", type: "string", allowNull: false, mapping: "name"
    , serialize: function (value, record) {
      var analyse = record.getAnalyse();
      if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) == -1 && !record.get('msClassLegend'))
        return null;
      return value || record.get('msClassTitle') || (record.get('layer').get('layerTitle'));
    }
  },
  {
    name: "msClassTitle", type: "string", allowNull: false, mapping: "title"
    , serialize: function (value, record) {
      var analyse = record.getAnalyse();
      if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) == -1 && !record.get('msClassLegend'))
        return null;
      return record.get('msClassName') || (record.get('layer').get('layerTitle'));
    }
  },
  {
    name: "msClassValue", type: "string", persist: false, mapping: "expression"
    , convert: function (msClassExpression) {
      return msClassExpression;
    }
  },
  {
    name: "msClassLowerBound", type: "string", persist: false, mapping: "expression"
    , convert: function (msClassExpression) {

      var regExp = /.*\[[^\]]+\]\s*>=\s*([0-9\.]+)\s?.*/;
      var matche = (regExp.test(msClassExpression) ? msClassExpression.replace(regExp, "$1") : (Ext.isNumber(parseFloat(msClassExpression)) ? msClassExpression : ""));
      return ((typeof msClassExpression) != "undefined"
        ? matche
        : "");
    }
  },
  {
    name: "msClassUpperBound", type: "string", persist: false, mapping: "expression"
    , convert: function (msClassExpression) {
      var regExp = /.*\[[^\]]+\]\s*<\s*([0-9\.]+)\s?.*/;
      var matche = (regExp.test(msClassExpression) ? msClassExpression.replace(regExp, "$1") : (Ext.isNumber(parseFloat(msClassExpression)) ? msClassExpression : ""));
      return ((typeof msClassExpression) != "undefined"
        ? matche
        : "");
    }
  },
  { name: "msClassVisible", type: "boolean", mapping: "status", convert: function (value) { return (Ext.isString(value) ? value == "ON" : value); }, serialize: function (value) { return (value ? "ON" : "OFF"); }, defaultValue: true, comment: "Visibility of this class in map" },
  {
    name: "msClassLegend", type: "boolean", defaultValue: true
    , convert: function (value, record) {
      return value && record.get('msClassName') != null && record.get('msClassName') != ""/* && !data.layer.get('layerLegendScale')*/;
    }
  },

  {
    name: "msStyleTypeSymbol", type: "string", defaultValue: Carmen.Dictionaries.MS_SYMBOLES.TYPES.SHOWLIBRARY, comment: "Type of symbol"
    , persist: false
    , calculate: function (data) {
      if (data.msStyleSymbol == Carmen.Dictionaries.MS_SYMBOLES.TYPES.HATCHSYMBOL)
        return Carmen.Dictionaries.MS_SYMBOLES.TYPES.HATCHSYMBOL;
      return Carmen.Dictionaries.MS_SYMBOLES.TYPES.SHOWLIBRARY;
    }
  },
  {
    name: "styles", persist: false
    , mapping: function (data) {
      if (Ext.isArray(data.styles)) {
        var oneStyle = {};
        if (data.styles.length > 0) {
          oneStyle = Ext.apply(oneStyle, data.styles[0]);
        }
        if (data.styles.length > 1) {
          var outline = {};
          if (Ext.isDefined(data.styles[1].color) && data.styles[1].color !== null) {
            outline.outlinecolor = data.styles[1].color;
          }
          else if (Ext.isDefined(data.styles[1].outlinecolor) && data.styles[1].outlinecolor !== null) {
            outline.outlinecolor = data.styles[1].outlinecolor;
          }
          if (Ext.isDefined(data.styles[1].width) && data.styles[1].outlinecolor !== null) {
            outline.outlinewidth = data.styles[1].width;
          }
          else if (Ext.isDefined(data.styles[1].outlinewidth) && data.styles[1].outlinewidth !== null) {
            outline.outlinewidth = data.styles[1].outlinewidth;
          }
          oneStyle = Ext.apply(oneStyle, outline);
        }
        data.styles = oneStyle;
      }
      return data.styles;
    }
  },
  { name: "msStyleColor", type: "string", mapping: "styles.color", defaultValue: Carmen.Defaults.CLASSCOLOR, comment: "Background color", convert: function (value) { return value || Carmen.Defaults.CLASSCOLOR; }, serialize: function (value) { return value || Carmen.Defaults.CLASSCOLOR; } },
  {
    name: "msStyleGeomtransform", type: "string", mapping: "styles.geomtransform", defaultValue: null, comment: "GeomTransform"
    , serialize: function (value, record) {
      if ([LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL].indexOf(record.getAnalyse()) != -1) {
        var msGeometryType = record.get('layer').get('msGeometryType');
        if (msGeometryType == "POLYGON") return "centroid";
      }
      return null;
    }
  },
  {
    name: "msStyleOutline", type: "boolean"
    , calculate: function (data) {
      var values = [/*data.msStyleOutlineColor, */data.msStyleOutlineWidth];
      values = values.filter(function (item) { return item && String(item).trim() !== '' && item !== null });
      return values.length != 0
    }
  },
  { name: "msStyleOutlineColor", type: "string", mapping: "styles.outlinecolor", defaultValue: null, comment: "Border color" },
  { name: "msStyleOutlineWidth", type: "number", mapping: "styles.outlinewidth", defaultValue: null, comment: "Border line width (pixels)" },
  {
    name: "msStyleSymbol", type: "string", mapping: "styles.symbol", defaultValue: null, comment: "Name/index/... of the symbol"
    , serialize: function (value, record) {
      if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(record.getAnalyse()) != -1) return 0;
      var msGeometryType = record.get('layer').get('msGeometryType');
      //always unset symbol for lines
      if (msGeometryType == "LINE") {
        return null;
      }
      if (value) {
        return value;
      } else {
        if (msGeometryType) {
          if (msGeometryType == "POINT") {
            return "Cercle";
          } else {//POLYGON
            return "Carre";
          }
        } else {
          return "Vide";
        }
      }
    }
  },
  {
    name: "msStyleWidth", type: "number", mapping: "styles.width", defaultValue: 1, comment: "Line width (pixels)",
    //since there is only one field (size) for both parameters width and size, return size value for width for lines
    serialize: function (value, record) {
      if (record.get('layer').get('msGeometryType') == "LINE") {
        return value;

      } else {
        //always null for other types
        return null;
      }
    }
  },
  {
    name: "msStyleSize", type: "string", defaultValue: '1', comment: "Symbol initial size (pixels)",
    mapping: "styles.size",
    // Fonction appelée lorsqu'on effectue un record.getData({persist : true, serialize : true}) pour un soumission vers le serveur
    serialize: function (value, record) {
      if (record.get('msStyleSizeField')) return record.get('msStyleSizeField');
      if (record.get('is_default_classe')) {
        switch (record.get('layer').get('msGeometryType')) {
          case "POINT": value = 8; break;
          case "LINE": value = null; break;
        }
      }
      //in all cases, return null for lines
      if (record.get('layer').get('msGeometryType') == "LINE") {
        return null;
      }
      return value;
    },
    // fonction appelée lorsqu'on effectue un record.set('msStyleSize', value). 
    // Action qui est faite en lecture de la base, en soumission du formulaire vers le record (Ext.form.Basic.updateRecord(record) ) ou en appel manuel 
    convert: function (value, record) {
      if (value && Ext.isString(value)) return null;
      //since there is only one field (size) for both parameters width and size, get size value from width for lines
      if (record.get('layer').get('msGeometryType') == "LINE") {
        value = Math.max(0, value) || record.get('msStyleWidth');
        record.set('msStyleWidth', value);
        return value;
      } else {
        return value;
      }

    }
  },
  {
    name: "msStyleSizeField", type: "string", defaultValue: '1', comment: "Symbol initial size (pixels)", mapping: "styles.size", persist: false
    , convert: function (value, record) {
      if (value && Ext.isString(value)) return value;
      var msStyleSize = record.get('msStyleSize');
      if (msStyleSize && Ext.isString(msStyleSize)) return msStyleSize;
      return null;
    }
  },
  {
    name: "msStyleMinSize", type: "string", mapping: "styles.minsize", defaultValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE, comment: "Symbol min size (pixels)",
    convert: function (value, record) {
      var n = parseFloat(value);
      value = (!isNaN(n) && n >= Carmen.Defaults.MIN_MS_SYMBOL_SIZE && n <= Carmen.Defaults.MAX_MS_SYMBOL_SIZE ? n : Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
      return Carmen.getFirstDefined(value, Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
    },
    serialize: function (value, record) {
      var analyse = record.getAnalyse(); if (!analyse) return null;
      if (analyse != LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL) {
        return null;
      } else {
        return value;
      }
    }
  },
  {
    name: "msStyleMaxSize", type: "string", mapping: "styles.maxsize", defaultValue: Carmen.Defaults.MAX_MS_SYMBOL_SIZE, comment: "Symbol max size (pixels)",
    convert: function (value, record) {
      var n = parseFloat(value);
      value = (!isNaN(n) && n >= Carmen.Defaults.MIN_MS_SYMBOL_SIZE && n <= Carmen.Defaults.MAX_MS_SYMBOL_SIZE ? n : Carmen.Defaults.MAX_MS_SYMBOL_SIZE);
      return Carmen.getFirstDefined(value, Carmen.Defaults.MAX_MS_SYMBOL_SIZE);
    },
    serialize: function (value, record) {
      var analyse = record.getAnalyse(); if (!analyse) return null;
      if (analyse != LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL) {
        return null;
      } else {
        return value;
      }
    }
  },
  { name: "isHatch", type: "boolean", defaultValue: null, comment: "Hatch pattern activation", persist: false, calculate: function (data) { return !!data.msStylePattern; } },
  { name: "msStyleHatchWidth", type: "number", mapping: "styles.width", comment: "Hatch pattern activation", convert: returnValueIf.bind(undefined, isHatchSymbol), serialize: returnValueIf.bind(undefined, isHatchSymbol) },
  { name: "msStyleHatchAngle", type: "number", mapping: "styles.angle", comment: "Hatch pattern activation", convert: returnValueIf.bind(undefined, isHatchSymbol), serialize: returnValueIf.bind(undefined, isHatchSymbol) },
  { name: "msStyleHatchSize", type: "number", mapping: "styles.size", comment: "Hatch pattern activation", convert: returnValueIf.bind(undefined, isHatchSymbol), serialize: returnValueIf.bind(undefined, isHatchSymbol) },
  {
    name: "msStylePattern", type: "string", mapping: "styles.pattern", defaultValue: null, comment: "Hatch pattern"
    , serialize: function (value, record) {
      if (String(value).split(' ').length == 4) return value;
      if (Ext.isEmpty(value)) return null;
      try {
        if (!eval(value)) return null;
      } catch (error) { }
      var parts = [record.get('msStylePatternVisible1'), record.get('msStylePatternHidden1'), record.get('msStylePatternVisible2'), record.get('msStylePatternHidden2')];
      var index;
      parts = parts.filter(function (item) { return item !== '' && item !== null; });
      if (parts.length == 4) {
        return parts.join(' ');
      }
      return null;
    }
  },
  {
    name: "msStylePatternVisible1", type: "int", defaultValue: null, comment: "Hatch pattern : first visible"
    , persist: false, depends: ['msStylePattern'], mapping: "styles.pattern", convert: function (msStylePattern) { return (msStylePattern && (Ext.isNumber(msStylePattern) || msStylePattern.indexOf(' ') == -1) ? msStylePattern : splitPattern(msStylePattern, 0)); }
  },
  {
    name: "msStylePatternHidden1", type: "int", defaultValue: null, comment: "Hatch pattern : first non-visible"
    , persist: false, depends: ['msStylePattern'], mapping: "styles.pattern", convert: function (msStylePattern) { return (msStylePattern && (Ext.isNumber(msStylePattern) || msStylePattern.indexOf(' ') == -1) ? msStylePattern : splitPattern(msStylePattern, 1)); }
  },
  {
    name: "msStylePatternVisible2", type: "int", defaultValue: null, comment: "Hatch pattern : second visible"
    , persist: false, depends: ['msStylePattern'], mapping: "styles.pattern", convert: function (msStylePattern) { return (msStylePattern && (Ext.isNumber(msStylePattern) || msStylePattern.indexOf(' ') == -1) ? msStylePattern : splitPattern(msStylePattern, 2)); }
  },
  {
    name: "msStylePatternHidden2", type: "int", defaultValue: null, comment: "Hatch pattern : second non-visible"
    , persist: false, depends: ['msStylePattern'], mapping: "styles.pattern", convert: function (msStylePattern) { return (msStylePattern && (Ext.isNumber(msStylePattern) || msStylePattern.indexOf(' ') == -1) ? msStylePattern : splitPattern(msStylePattern, 3)); }
  },
  { name: "is_default_classe", type: "boolean", defaultValue: false, persist: false }
  ],

  getAnalyse: function () {
    return getCodeAnalyse(this.get('layer').get('layerAnalyseType'));
  }
});

Carmen.Model.LayerAnalyse = [];
function addAnalyseField(value) { Carmen.Model.LayerAnalyse.push(value); return value; };

/**
 * Carmen.Model.Layer
 */
Ext.define('Carmen.Model.Layer', {
  inheritableStatics: {
    // layer type
    TYPES: {
      VECTOR: 'Vector',
      RASTER: 'Raster',
      WMS: 'WMS',
      WFS: 'WFS',
      WMSC: 'WMSC',
      WMTS: 'WMTS',
      DATA: 'Data',
      Map: 'Map',
      POSTGIS: 'Postgis',
      GEOJSON: 'Geojson',
      TILE_VECTOR: 'TileVector'
    },

    loadOneRawData: function (aLayer) {
      return this.getProxy().getReader().readRecords(aLayer).getRecords()[0];
    }
  },
  extend: 'Ext.data.Model',
  idProperty: 'layerId',

  getProxy: function (mapId) {
    return Ext.Factory.proxy({
      type: 'rest',
      model: (this instanceof Carmen.Model.Layer ? this.$className : 'Carmen.Model.Layer'),
      url: Routing.generate('carmen_ws_layer', {
        mapId: (Ext.isDefined(mapId)
          ? mapId
          : (Ext.isDefined(Carmen.user.map)
            ? Carmen.user.map.get('mapId')
            : (this instanceof Carmen.Model.Layer
              ? this.getMap().get('mapId')
              : '{mapId}')))
      }),
      timeout: 300000
    });
  },
  fields: [{
    // Layer Id (not saved)
    name: "layerId",
    persist: false,
    type: "int",
    allowNull: false,
    defaultValue: 0
  },
  // user preferences (not saved)
  {
    name: 'userPreferences',
    persist: false,
    defaultValue: Carmen.user.preferences,
    serialize: function (value) { return null; }
  },
  // map record (not saved)
  {
    name: 'map',
    reference: 'Carmen.Model.Map',
    persist: false,
    allowNull: false,
    serialize: function (map) {
      return (map.get ? map.get('mapId') : map);
    }
  },
  // database fields record 
  // layerType is declared in each subclasses with their type specific
  { name: addAnalyseField("layerAnalyseType"), type: "string", defaultValue: 1, convert: function (value) { if (Ext.isObject(value)) return value.analyseTypeId; return value; } },
  {
    name: "layerDisplayMode", type: "string", persist: false

  },

  { name: "layerTitle", type: "string" },
  { name: "layerName", type: "string" },
  { name: "_displayname_", type: "string", persist: false, calculate: function (data) { return '[' + data.layerName + '] ' + data.layerTitle } },
  { name: "layerFieldUrl", type: "string" },
  { name: "layerMetadataUuid", type: "string" },
  { name: "hrefLayerMetadataUuid", type: "string" },
  { name: "layerMetadataFile", type: "string" },
  { name: "hrefLayerMetadataFile", type: "string" },
  {
    name: "geosourceLayerMetadata",
    depends: 'layerMetadataUuid',
    reference: 'GeosourceLayerMetadata',
    convert: function (data, layer) {
      data = (data instanceof Carmen.Model.GeosourceLayerMetadata) ? data : Ext.create('Carmen.Model.GeosourceLayerMetadata', { layer: layer });
      return data;
    },
    serialize: function (record) {
      if (record) {
        record.set('needsUpdate', record.get('layer').get('geosourceLayerMetadataNeedsUpdate'));
        record.set('publish', record.get('layer').get('geosourceLayerMetadataPublish'));
        record.set('cswTitle', record.get('layer').get('geosourceLayerMetadataCswTitle'));
        record.set('cswAbstract', record.get('layer').get('geosourceLayerMetadataCswAbstract'));
        record.set('cswKeywords', record.get('layer').get('geosourceLayerMetadataCswKeywords'));
        record.set('cswInspiretheme', record.get('layer').get('geosourceLayerMetadataCswInspiretheme'));
        return record.getData({ persist: true, serialize: true });
      }
      return record;
    }
  },
  { name: 'geosourceLayerMetadataNeedsUpdate', mapping: 'geosourceLayerMetadata.needsUpdate', persist: false, defaultValue: false },
  { name: 'geosourceLayerMetadataPublish', mapping: 'geosourceLayerMetadata.publish', persist: false, defaultValue: false },
  { name: 'geosourceLayerMetadataCswTitle', mapping: 'geosourceLayerMetadata.cswTitle', persist: false, defaultValue: "" },
  { name: 'geosourceLayerMetadataCswAbstract', mapping: 'geosourceLayerMetadata.cswAbstract', persist: false, defaultValue: "" },
  { name: 'geosourceLayerMetadataCswKeywords', mapping: 'geosourceLayerMetadata.cswKeywords', persist: false, defaultValue: "" },
  { name: 'geosourceLayerMetadataCswInspiretheme', mapping: 'geosourceLayerMetadata.cswInspiretheme', persist: false, defaultValue: "" },
  { name: "layerServerUrl", type: "string" },
  { name: "layerServerVersion", type: "string" },
  { name: "layerWxsname", type: "string" },
  { name: "projection", type: "string", convert: function (value) { return (value ? String(value).replace(/\+init=epsg:|\+init=crs:/, '') : value); }, defaultValue: null },
  {
    name: "layerProjectionEpsg", type: "int", depends: ['userPreferences', 'projection']
    , convert: function (value, record) {
      value = (Ext.isObject(value) ? value.projectionEpsg : value);
      var preferences = record.get("userPreferences");
      return Carmen.getFirstDefined(value, record.get("projection"), preferences.get("preferenceSrs"), Carmen.Defaults.PROJECTION);
    }
  },
  { name: "layerOutputformat", type: "string" },

  {
    name: addAnalyseField("layerTransparency"), type: "number", depends: ["layerOpacity"], convert: function (value, record) {
      return (Ext.isDefined(value) && value !== null
        ? value
        : 100 - Ext.num(record.get('layerOpacity'), 0));
    }, persist: false
  },
  { name: "layerOpacity", type: "int", defaultValue: 100, serialize: function (value, record) { var opacity = (100 - record.get('layerTransparency')); if (opacity < 1) opacity = opacity * 100; return opacity } },
  {
    name: "layerMinscale", type: "number", depends: ['userPreferences'],
    convert: function (value, record) {
      var n = parseFloat(value);
      value = (!isNaN(n) && n >= Carmen.Defaults.MINSCALE && n <= Carmen.Defaults.MAXSCALE ? n : Carmen.Defaults.MINSCALE);
      return Carmen.getFirstDefined(value, record.get("userPreferences").get("preferenceMinscale"), Carmen.Defaults.MINSCALE);
    }
  },
  {
    name: "layerMaxscale", type: "number", depends: ['userPreferences'],
    convert: function (value, record) {
      var n = parseFloat(value);
      value = (!isNaN(n) && n >= Carmen.Defaults.MINSCALE && n <= Carmen.Defaults.MAXSCALE ? n : Carmen.Defaults.MAXSCALE);
      return Carmen.getFirstDefined(value, record.get("userPreferences").get("preferenceMaxscale"), Carmen.Defaults.MAXSCALE);
    }
  },

  {
    name: "layerLegend", type: "boolean", defaultValue: false
    , serialize: function (value, record) {
      var classes = record.get('msClasses');
      if (classes) return classes.find("msClassLegend", true) != -1;
      return false;
    }
  },
  { name: addAnalyseField("layerLegendScale"), type: "boolean", defaultValue: false },
  { name: "layerDownloadable", type: "boolean", defaultValue: false },
  { name: "layerWms", type: "boolean", defaultValue: false },
  { name: "layerWfs", type: "boolean", defaultValue: false },
  { name: "layerAtom", type: "boolean", defaultValue: false },
  { name: "layerVisible", type: "boolean", defaultValue: true },
  { name: "layerHaslabel", type: "boolean", defaultValue: false },
  { name: "layerWmsQuery", type: "boolean", defaultValue: false },

  { name: "layerDiffusion", persist: false, calculate: function (data) { return { layerWms: data.layerWms, layerWfs: data.layerWfs, layerAtom: data.layerAtom }; } },
  { name: "layerDataEncoding", type: 'string', defaultValue: null },
  {
    name: "fields",
    defaultValue: [],
    convert: function (fields, layer) {
      fields = fields || [];
      if (Ext.isArray(fields)) {
        Ext.each(fields, function (field) {
          field.layer = layer;
        });
        fields = Ext.create('Ext.data.JsonStore', {
          model: 'Carmen.Model.Field',
          data: fields
        });
      }
      return fields;
    },
    serialize: Carmen.serializeRecordForSubmit,
    depends: ['layerId']
  },
  {
    name: "msClasses",
    convert: function (msClasses, layer) {
      msClasses = msClasses || [];
      if (Ext.isArray(msClasses)) {
        if (!msClasses.length) {
          msClasses = [Ext.create('Carmen.Model.MsClass', {
            msClassIndex: 0,
            layer: layer,
            msClassName: layer.get('layerTitle'),
            msClassTitle: layer.get('layerTitle'),
            is_default_classe: true
          })];
        }
        Ext.each(msClasses, function (msClasse) {
          msClasse.layer = layer;
        });

        var store = Ext.create('Ext.data.Store', {
          model: 'Carmen.Model.MsClass'
        });
        if (msClasses[0] instanceof Carmen.Model.MsClass)
          store.add(msClasses);
        else {
          store.loadRawData(msClasses);
        }

        msClasses = store;
      }
      return msClasses;
    },
    serialize: function (msClasses, layer) {
      var defaultClass = Ext.create('Carmen.Model.MsClass', {
        msClassIndex: 0,
        layer: layer,
        msClassName: layer.get('layerTitle'),
        msClassTitle: layer.get('layerTitle'),
        is_default_classe: true
      });
      if (!msClasses) {
        var msClasses = Ext.create('Ext.data.Store', {
          model: 'Carmen.Model.MsClass'
        });
        msClasses.add(defaultClass);
      }
      if (msClasses instanceof Ext.data.Store && !msClasses.getCount()) {
        msClasses.add(defaultClass);
      }
      return Carmen.serializeMapserverRecordForSubmit(msClasses, layer);
    },
    depends: ['layerId']
  },
  { name: "layerIdentifier", type: "string" },  // mapfile data : NAME
  { name: "layerMsname", type: "string" },   // mapfile data : NAME, stored in layer table 

  { name: "msLayerExtent", type: "auto", persist: false },  // mapfile data : NAME

  {
    name: 'metadata',
    unique: true,
    convert: function (value, layer) {
      value = (value instanceof Carmen.Model.msMetadataLayer ? value : Ext.create('Carmen.Model.msMetadataLayer', Ext.merge({ layer: layer }, value)));
      return value;
    },
    /**@see CarmenJS/Carmen/Ext/Util.js*/
    serialize: Carmen.serializeRecordForSubmit
  },
  { name: 'msGeoide_wxs_srs', persist: false, depend: "metadata", convert: function (value, record) { return value || record.get('metadata').get("geoide_wfs_srs") } },
  { name: 'layerIsBackground', type: "boolean", defaultValue: false },
  { name: 'layerIsDefaultBackground', type: "boolean", defaultValue: false },
  { name: "layerBackgroundImage", type: "string" },   // mapfile data : NAME, stored in layer table 
  {
    name: "msAnalyseType", type: "string", mapping: function (data) {
      return data && data.layerAnalyseType && data.layerAnalyseType.analyseTypeCode;
    }
  }
  ],

  getAnalyse: function () {
    return getCodeAnalyse(this.get('layerAnalyseType'));
  }
});

/**
 * Carmen.Model.LayerVectorial : master class for layer typed Vector, Postgis, WFS
 */
Ext.define('Carmen.Model.LayerVectorial', {
  extend: 'Carmen.Model.Layer',
  fields: [
    { name: "msGeometryType", type: "string", defaultValue: 'POINT' },
    {
      name: "msLayerType", type: "string", defaultValue: 'POINT', serialize: function (value, record) {
        var analyse = record.getAnalyse();
        var geometry = record.get('msGeometryType');
        if (!analyse) return geometry;
        if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) {
          return 'CHART';
        } else {
          return geometry;
        }
      }
    },      // mapfile data : TYPE
    {
      name: addAnalyseField("msClassItem"), type: "string"
      , serialize: function (value, record) {
        var analyse = record.getAnalyse();
        if ([LEX_ANALYSE_TYPE.TYPES.UNIQVALUE, LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL].indexOf(analyse) == -1) {
          return null;
        }
        if (value == "") return null; return value;
      }
    },
    { name: addAnalyseField("msStyleSizeField"), type: "string", persist: false, mapping: 'msClasses[0].styles.size' },
    { name: addAnalyseField("msSymbolScaleDenomPixels"), type: "float" },
    {
      name: addAnalyseField("msStyleMinSize"), type: "float", persist: false, mapping: 'msClasses[0].styles.minsize',
      convert: function (value, record) {
        var n = parseFloat(value);
        value = (!isNaN(n) && n >= Carmen.Defaults.MIN_MS_SYMBOL_SIZE && n <= Carmen.Defaults.MAX_MS_SYMBOL_SIZE ? n : Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
        return Carmen.getFirstDefined(value, Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
      },
      serialize: function (value, record) {
        var analyse = record.getAnalyse(); if (!analyse) return null;
        if (analyse != LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL) {
          return null;
        } else {
          return value;
        }
      }
    },
    {
      name: addAnalyseField("msStyleMaxSize"), type: "float", persist: false, mapping: 'msClasses[0].styles.maxsize',
      convert: function (value, record) {
        var n = parseFloat(value);
        value = (!isNaN(n) && n >= Carmen.Defaults.MIN_MS_SYMBOL_SIZE && n <= Carmen.Defaults.MAX_MS_SYMBOL_SIZE ? n : Carmen.Defaults.MAX_MS_SYMBOL_SIZE);
        return Carmen.getFirstDefined(value, Carmen.Defaults.MAX_MS_SYMBOL_SIZE);
      },
      serialize: function (value, record) {
        var analyse = record.getAnalyse(); if (!analyse) return null;
        if (analyse != LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL) {
          return null;
        } else {
          return value;
        }
      }
    },
    {
      name: addAnalyseField("msStyleMaxValue"), type: "float", persist: false,
      convert: function (value, record) {
        return record.get('msStyleMaxSize') / (record.get('msSymbolScaleDenomPixels') || 1);
      }
    },
    { name: addAnalyseField("msClassDistribution"), type: "string", defaultValue: Carmen.Dictionaries.CLASSDISTRIBUTION.DEFAULT },
    {
      name: addAnalyseField("msClassCount"), type: "int", calculate: function (data) {
        if (data.msClasses instanceof Ext.data.Store) {
          return Math.min(Carmen.Defaults.NB_MAX_MS_CLASSES, Math.max(Carmen.Defaults.NB_MIN_MS_CLASSES, data.msClasses.getCount()));
        }
        return Carmen.Defaults.NB_MIN_MS_CLASSES;
      }
    },

    {
      name: addAnalyseField("msProcessingChartSizeRange"), type: "string", mapping: 'processing.CHART_SIZE_RANGE', defaultValue: null
      , serialize: function (value, record) {
        var analyse = record.getAnalyse();
        if ([LEX_ANALYSE_TYPE.TYPES.PIECHART].indexOf(analyse) == -1) return null;
        if (record.get('msProcessingChartSizeMode') != 'byRange') {
          return null;
        }
        var min = Carmen.getFirstDefined(record.get('msProcessingChartSizeRangeMin'), Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
        var max = Carmen.getFirstDefined(record.get('msProcessingChartSizeRangeMax'), Carmen.Defaults.MAX_MS_SYMBOL_SIZE);
        var parts = [record.get('msProcessingChartSizeRangeItem'), min, max, min, max];
        var keep = [];
        for (var i = 0; i < parts.length; i++) { if (parts[i] != "" && parts[i] !== null && typeof parts[i] != "undefined" && parts[i] !== Number.NaN) keep.push(parts[i]); }
        return keep.join(' ');
      }
    },
    {
      name: addAnalyseField("msProcessingChartSizeRangeItem"), type: 'string', mapping: 'processing.CHART_SIZE_RANGE', persist: false, defaultValue: null
      , convert: function (value, record) {
        var parts = String(value).split(' ');
        return (value === null ? null : parts[0]);
      }
    },
    {
      name: addAnalyseField("msProcessingChartSizeRangeMin"), type: "number", mapping: 'processing.CHART_SIZE_RANGE', persist: false, defaultValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE
      , convert: function (value, record) {
        if (Ext.isNumber(value)) return value;
        var parts = String(value).split(' ');
        return (parts.length > 1 ? parts[1] : null);
      }
    },
    {
      name: addAnalyseField("msProcessingChartSizeRangeMax"), type: "number", mapping: 'processing.CHART_SIZE_RANGE', persist: false, defaultValue: Carmen.Defaults.MAX_MS_SYMBOL_SIZE
      , convert: function (value, record) {
        if (Ext.isNumber(value)) return value;
        var parts = String(value).split(' ');
        return (parts.length > 2 ? parts[2] : null);
      }
    },
    {
      name: addAnalyseField("msProcessingChartSize"), type: "number", mapping: 'processing.CHART_SIZE'
      , defaultValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE
      , serialize: function (value, record) {
        var analyse = record.getAnalyse();
        if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) == -1) return null;
        if (!record.get('msProcessingChartSizeMode') && record.get('msProcessingChartSizeRangeItem') != null) {
          return null;
        }
        if (record.get('msProcessingChartSizeMode') != 'bySize' && [LEX_ANALYSE_TYPE.TYPES.PIECHART].indexOf(analyse) != -1) {
          return null;
        }
        return value;
      }
    },
    {
      name: addAnalyseField("msProcessingChartSizeMode"), type: "string", mapping: 'processing', persist: false, defaultValue: 'bySize'
      , depends: ['msProcessingChartSize', 'msProcessingChartSizeRange']
      , convert: function (value, record) {
        if (value && Ext.isString(value)) return value;
        if (record.get('msProcessingChartSizeRangeItem')) {
          return 'byRange';
        }
        if (record.get('msProcessingChartSize')) {
          return 'bySize';
        }
        return value || undefined;
      }
    },
    {
      name: ("msProcessingChartType"), type: "string", mapping: 'processing.CHART_TYPE', defaultValue: Carmen.Defaults.LAYER_CHART_TYPE
      , convert: function (value, record) {
        var analyse = getCodeAnalyse(record.get('layerAnalyseType'));
        if (analyse && [LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) {
          return analyse.replace('CHART', '');
        } else {
          return null;
        }
      }
    },

    { name: "msLabelItem", type: "string" },
    { name: "layerGeojsonUrl", type: "string", persist: false },
    {
      name: "msLabel"//, reference : "Carmen.Model.MsLabel"
      , serialize: function (value, layer) { if (layer.get('msLabelItem')) return Carmen.serializeMapserverRecordForSubmit(value, layer); return null; }
      , convert: function (value, layer) {
        value = value || {};
        if (value instanceof Carmen.Model.MsLabel) {
          value.set('layer', layer);
        } else if (Ext.isObject(value)) {
          var store = Ext.create('Ext.data.JsonStore', {
            autoLoad: false,
            model: "Carmen.Model.MsLabel",
            proxy: {
              type: 'memory',
              reader: {
                type: 'json'
              }
            }
          });
          store.loadRawData([Ext.apply({ layer: layer }, value)], false);
          value = store.getAt(0);
          /*
          value = Ext.create('Carmen.Model.MsLabel', Ext.merge({layer:layer}, value));
          */
        }
        return value;
      }
    }
  ]
});

/**
 * LayerRasterized : master class for layer typed Raster, WMS, WMS-C
 */
Ext.define('Carmen.Model.LayerRasterized', {
  extend: 'Carmen.Model.Layer',
  fields: [
    { name: "msLayerType", type: "string", defaultValue: 'RASTER' },
    { name: "msGeometryType", type: "string", defaultValue: 'RASTER' }      // mapfile data : TYPE
  ]
});

/**
 * Carmen.Model.Field
 */
Ext.define('Carmen.Model.GeosourceLayerMetadata', {
  extend: 'Ext.data.Model',

  //idProperty : 'uuid',
  fields: [
    // layer record (not saved)
    {
      name: 'layer',
      persist: false,
      allowNull: false,
      serialize: function (layer) {
        return null;
      }
    },
    { name: "needsUpdate", type: "boolean", defaultValue: false },
    { name: "publish", type: "boolean", defaultValue: false },
    { name: "uuid", type: "string" },
    { name: "cswTitle", type: "string" },
    { name: "cswAbstract", type: "string" },
    {
      name: "cswKeywords", type: "string",
      convert: function (value) {
        if (value && Ext.isString(value)) {
          var values = value.split(',');
          for (var i = 0; i < values.length; i++) {
            values[i] = values[i].trim();
          }
        }
        return value;
      },
      serialize: function (value, record) {
        if (value && Ext.isArray(value))
          return value.join(',');
        return value;
      }
    },
    { name: "cswInspiretheme", type: "string" }
  ]
});

/**
 * Carmen.Model.Map
 */
Ext.define('Carmen.Model.msMetadataLayer', {
  extend: 'Ext.data.Model',
  fields: [
    {
      name: 'layer',
      persist: false,
      serialize: function () { return null }
    }, {
      name: 'geoide_wms_srs', defaultValue: "",
      convert: function (value) {
        if (value) {
          value = value.trim();
          value = value.split(' ');
          value = value.map(function (v) { return v.replace(/EPSG:|CRS:/, ''); });
        }
        return value;
      },
      serialize: function (value, record) {
        var srs = record.get('layer').get('msGeoide_wxs_srs');
        if (Ext.isArray(srs)) srs = srs.map(function (v) {
          if (v != "")
            return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider') + ':' + v;
        }).join(' ');
        return srs;
      }
    }, {
      name: 'geoide_wfs_srs', defaultValue: "",
      convert: function (value) {
        if (value) {
          value = value.trim();
          value = value.split(' ');
          value = value.map(function (v) { return v.replace(/EPSG:|CRS:/, ''); });
        }
        return value;
      },
      serialize: function (value, record) {
        var srs = record.get('layer').get('msGeoide_wxs_srs');
        if (Ext.isArray(srs)) srs = srs.map(function (v) {
          if (v != "")
            return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider') + ':' + v;
        }).join(' ');
        return srs;
      }
    }
  ]
});