var _T_LAYERFIELDS = function (key) {
  return _t("Layer.forms.TabField." + key);
};

var TRANSLATE = _T_LAYERFIELDS;

///////////////////////////////////////////
// Layer Fields Grid
///////////////////////////////////////////
Ext.define('Carmen.grid.Fields', {
  extend: 'Ext.grid.Panel',
  xtype: 'cmnlayerfields',
  //columnLines: true,
  flex: 1,
  //height : 200,
  width: '100%',
  columnLines: true,
  forceFit: true,
  frame: true,

  TRANSLATE: _T_LAYERFIELDS,

  // this grid is considerd a formfield so it can post its data
  isFormField: true,
  isFieldLabelable: true,
  setValue: function (records) {
    if (this.store) {
      this.store.loadData(records || []);
    }
  },
  getValue: function () {
    var res = {};
    res['fields'] = this.store.getData().getValues('data');
    return res;
  },
  getSubmitData: function () {
    return this.getValue();
  },
  getModelData: function () {
    return this.getValue();
  },
  isDirty: function () {
    return (this.store.getModifiedRecords().length + this.store.getNewRecords().length) > 0;
  },
  isValid: function () { return true; },
  isFileUpload: function () { return false },
  validate: function () { return true },

  headerCheckboxTpl: '<span><span data-checked="unchecked" class="x-grid-row-checker" style="display:block;margin-right:auto;margin-left:auto;"></span></span>',

  headerCheckboxClick: function (elt, column) {
    var me = this;
    // if the element is the checkbox
    // change its status and update all records accordingly
    var extElt = Ext.get(elt);
    if (extElt.hasCls('x-grid-row-checker')) {
      if (elt.dataset.checked == 'unchecked') {
        // set checked
        elt.dataset.checked = 'checked';
        extElt.parent().addCls('x-grid-item-selected');
        me.store.each(function (record) {
          record.set(column.dataIndex, true);
        });
      } else {
        // set unchecked
        elt.dataset.checked = 'unchecked';
        extElt.parent().removeCls('x-grid-item-selected');
        me.store.each(function (record) {
          record.set(column.dataIndex, false);
        });
      }
    }
  },

  viewConfig: {
    /* set a custom css class based on record value */
    getRowClass: function (record) {
      return record.get('fieldDuplicate') ? 'field-duplicate-row' : 'field-normal-row';
    },
    plugins: {
      ptype: 'gridviewdragdrop',
      dragText: _T_LAYERFIELDS('extra.reorderTooltip'),
      /**
       * override onViewRender to get access to the dragZone object
       * then override getDragData of dragZone object to disabled drag under input elements
       */
      dragZone: {
        getDragData: function (e) {
          // do not enable drag under input elements
          if (Ext.get(e.getTarget()).is('input')) return;
          return Ext.view.DragZone.prototype.getDragData.apply(this, arguments);
        }
      }
    }
  },

  initComponent: function () {
    var me = this;
    var widthCheckColumn = 65;
    me.store = Ext.create('Ext.data.JsonStore', {
      autoLoad: false,
      model: 'Carmen.Model.Field',
      data: []
    });
    me.border = true;
    me.columns = {
      defaults: {
        align: 'center'
      },
      items: [{
        xtype: 'actioncolumn',
        sortable: false,
        menuDisabled: true,
        width: 30,
        items: [{
          width: 25,
          icon: '/carmenweb/images/plus.png',
          tooltip: me.TRANSLATE('extra.dupField'),
          handler: function (grid, rowIndex, colIndex) {
            var record = grid.getStore().getAt(rowIndex);
            if (record.get('fieldDuplicate') === false) {
              // duplicate this original field
              var rec = record.copy(Date.now()/*new id*/);
              rec.set('fieldId', null);
              rec.set('fieldDuplicate', true);
              me.store.add(rec);
            }
          }
        }],
        renderer: function (value, meta, record, row, column, store, view) {
          if (record.get('fieldDuplicate') === true) {
            meta.style = "display:none";
          }
        }
      },
      {
        xtype: 'actioncolumn',
        sortable: false,
        menuDisabled: true,
        width: 30,
        items: [{
          icon: '/carmenweb/images/delete.png',
          tooltip: me.TRANSLATE('extra.removeField'),
          handler: function (grid, rowIndex, colIndex) {
            var record = grid.getStore().getAt(rowIndex);
            if (record.get('fieldDuplicate') === true) {
              // remove this duplicated field
              me.store.remove(record);
            }
          },
          isDisabled: function (view, rowIndex, colIndex, item, record) {
            // Returns true if 'editable' is false (, null, or undefined)
            return record.get('fieldDuplicate') === false;
          }
        }]
      },
      {
        text: me.TRANSLATE('columns.fieldName'),
        dataIndex: 'fieldName',
        align: 'left',
        sortable: false,
        menuDisabled: true
      },
      {
        text: me.TRANSLATE('columns.fieldAlias'),
        xtype: 'widgetcolumn',
        dataIndex: 'fieldAlias',
        widget: {
          margin: '5 0 5 0',
          xtype: 'textfield',
          listeners: {
            change: function (widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            }
          }
        },
        sortable: false,
        menuDisabled: true
      },
      {
        text: me.TRANSLATE('columns.fieldType'),
        xtype: 'widgetcolumn',
        dataIndex: 'fieldType',
        widget: {
          xtype: 'combobox',
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.FIELDTYPES,
          editable: false,
          autoLoadOnValue: true,
          autoSelect: true,
          listeners: {
            change: function (widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            }
          }
        },
        sortable: false,
        menuDisabled: true
      },
      {
        text: me.TRANSLATE('columns.fieldHeaders') + me.headerCheckboxTpl,
        xtype: 'checkcolumn',
        dataIndex: 'fieldHeaders',
        width: widthCheckColumn,
        sortable: false,
        menuDisabled: true,
        listeners: {
          headerclick: function (ct, column, e, t, eOpts) {
            // t is the dom element that triggered the event
            me.headerCheckboxClick(t, column);
          }
        }
      },
      {
        text: me.TRANSLATE('columns.fieldQueries') + me.headerCheckboxTpl,
        xtype: 'checkcolumn',
        dataIndex: 'fieldQueries',
        width: widthCheckColumn,
        sortable: false,
        menuDisabled: true,
        listeners: {
          headerclick: function (ct, column, e, t, eOpts) {
            // t is the dom element that triggered the event
            me.headerCheckboxClick(t, column);
          }
        }
      },
      {
        text: me.TRANSLATE('columns.fieldTooltips') + me.headerCheckboxTpl,
        xtype: 'checkcolumn',
        dataIndex: 'fieldTooltips',
        width: widthCheckColumn,
        sortable: false,
        menuDisabled: true,
        listeners: {
          headerclick: function (ct, column, e, t, eOpts) {
            // t is the dom element that triggered the event
            me.headerCheckboxClick(t, column);
          }
        }
      },
      {
        text: me.TRANSLATE('columns.fieldOgc') + me.headerCheckboxTpl,
        xtype: 'checkcolumn',
        hidden: (!VISIBILITY_LAYER_FORMS_TABFIELD_COLUMNS_FIELDOGC ? true : false),
        dataIndex: 'fieldOgc',
        width: widthCheckColumn,
        sortable: false,
        menuDisabled: true,
        listeners: {
          headerclick: function (ct, column, e, t, eOpts) {
            // t is the dom element that triggered the event
            me.headerCheckboxClick(t, column);
          }
        }
      },
      {
        text: me.TRANSLATE('columns.fieldUrl'),
        xtype: 'widgetcolumn',
        dataIndex: 'fieldUrl',
        width: 150,
        widget: {
          xtype: 'textfield',
          listeners: {
            change: function (widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            }
          }
        },
        sortable: false,
        menuDisabled: true
      }]
    };

    this.callParent();
  }
});

/**
 * fields tab
 */
var CMP_NAME = 'Carmen.FormSheet.Layer.TabField';
Ext.define('Carmen.FormSheet.Layer.TabField', {
  extend: 'Carmen.Generic.Form',
  xtype: CMP_REFERENCE(CMP_NAME),

  title: TRANSLATE('title'),
  height: 'auto',
  layout: 'fit',
  items: [
    {
      xtype: 'cmngenericform',
      title: false,
      layout: 'anchor',
      items: [
        {
          xtype: 'textfield',
          fieldLabel: TRANSLATE('fields.urlPrefix'),
          name: 'layerFieldUrl',
          anchor: '100%',
          padding: '10 0 0 10',
          labelAlign: 'left',
          labelWidth: 100,
          labelSeparator: ' '
        }
        , {
          xtype: 'cmnlayerfields',
          name: 'layerFields'
        }
      ]
    }
  ],

  loadRecord: function (record) {
    this.callParent(arguments);

    if (record instanceof Carmen.Model.Layer) {
      var fields = record.get('fields'); // assume fields is already a store
      if (fields instanceof Ext.data.Store) {
        if (this.query('cmnlayerfields').length > 0) {
          var cmnlayerfields = this.query('cmnlayerfields')[0];
          var records = [];
          fields.each(function (rec) {
            records.push(rec);
          });
          cmnlayerfields.setValue(records);
        }
      }
    }
  }
});
