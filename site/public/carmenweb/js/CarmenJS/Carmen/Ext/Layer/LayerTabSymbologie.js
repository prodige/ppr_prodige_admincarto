var CMP_NAME = 'Carmen.FormSheet.Layer.TabSymbologie';

(function () {
  var _T_SYMBOL = function (key) {
    return _t("Layer.forms.TabSymbologie." + key);
  };

  Ext.define('Carmen.FormSheet.Layer.TabSymbologie', {
    extend: 'Carmen.Generic.Form',
    xtype: CMP_REFERENCE(CMP_NAME),
    layerRecord: null,
    title: _T_SYMBOL('title'),
    height: 'auto',
    layout: 'fit',
    items: [],
    // methods
    initComponent: function () {
      var self = this;

      // items on this panel
      self.items = [{
        xtype: 'fieldset',
        layout: 'form',
        padding: 0,
        margin: 0,
        border: false,
        items: [{
          id: 'geojsonDistance',
          xtype: 'numberfield',
          name: 'geojsonDistance',
          fieldLabel: _T_SYMBOL('fields.geojsonDistance'),
          allowBlank: false,
          minValue: 0,
          flex: 6
        },{
          id: 'geojsonSingleColor',
          xtype: 'checkboxfield',
          name: 'geojsonSingleColor',
          fieldLabel: _T_SYMBOL('fields.geojsonSingleColor'),
          allowBlank: true,
          flex: 6,
          listeners: {
            change: function(elt, newValue, oldValue){
              var geojsonColorField = Ext.getCmp('geojsonColor');

              if(geojsonColorField){
                geojsonColorField.setDisabled(!newValue);
              }
            },
            
          }
        },{
          id: 'geojsonColor',
          xtype: 'colorfield',
          name: 'geojsonColor',
          fieldLabel: _T_SYMBOL('fields.geojsonColor'),
          allowBlank: true,
          maxLength: 255,
          enforceMaxLength: true,
          disabled: true,
          flex: 6,
          listeners:{
            afterrender: function(elt){
              var cmp = Ext.getCmp('geojsonSingleColor');
            console.log(cmp, cmp.value);
              if(cmp){
                elt.setDisabled(!cmp.value);
              }
            }
          }
        },{
          id: 'geojsonFixedSize',
          xtype: 'checkboxfield',
          name: 'geojsonFixedSize',
          fieldLabel: _T_SYMBOL('fields.geojsonFixedSize'),
          allowBlank: false,
          flex: 6,
          listeners: {
            change: function(elt, newValue, oldValue){
              var geojsonSizeField = Ext.getCmp('geojsonSize');

              if(geojsonSizeField){
                geojsonSizeField.setDisabled(!newValue);
              }
            }
          }
        },{
          id: 'geojsonSize',
          xtype: 'numberfield',
          name: 'geojsonSize',
          fieldLabel: _T_SYMBOL('fields.geojsonSize'),
          allowBlank: true,
          minValue: 0,
          flex: 6,
          listeners:{
            afterrender: function(elt){
              var cmp = Ext.getCmp('geojsonFixedSize');
              if(cmp){
                elt.setDisabled(!cmp.value);
              }
            }
          }
        },{
          id: 'geojsonTransitionEffect',
          xtype: 'checkboxfield',
          name: 'geojsonTransitionEffect',
          fieldLabel: _T_SYMBOL('fields.geojsonTransitionEffect'),
          allowBlank: false,
          flex: 6
        },{
          id: 'geojsonWithText',
          xtype: 'checkboxfield',
          name: 'geojsonWithText',
          fieldLabel: _T_SYMBOL('fields.geojsonWithText'),
          allowBlank: false,
          flex: 6
        }]
      }];
      
      self.callParent(arguments);
    },

    updateRecord: function (paramsEdit) {
      this.callParent(arguments);
      var self = this;
      geojsonParams = self.layerRecord.get('geojsonParams');
      if (Ext.isArray(geojsonParams)) {
        geojsonParams.forEach(function (param, idx) {
          if (param.geojsonParamsId === paramsEdit.data.geojsonParamsId) {

            Object.entries(geojsonParams[idx]).forEach(([key,value]) => {
              var cmp = Ext.getCmp(key);
              console.log(key, cmp);
              if(cmp){
                geojsonParams[idx][key] = cmp.getValue();
              }
            })
          }
        })
        self.layerRecord.set('geojsonParams', geojsonParams);
      }
    },
    loadRecord: function (record) {
      this.layerRecord = record;
      geojsonParams = record.get('geojsonParams');
      geojsonParamsStore = null;
      if (Ext.isArray(geojsonParams)) {
        geojsonParamsStore = Ext.create('Ext.data.JsonStore', {
          model: 'Carmen.Model.GeojsonParams',
          data: geojsonParams
        });
      }
  
      if (geojsonParamsStore instanceof Ext.data.Store) {
        geojsonParamsStore.each(function (rec) {
          record = rec;
        });
      }
    
      this.callParent(arguments);
    }

  });
})();