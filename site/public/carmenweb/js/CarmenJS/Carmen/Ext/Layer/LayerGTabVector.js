var _T_LAYER_VECTOR = function (key) {
  return _t("Layer.forms.GTabVector." + key);
};

/**
 * Carmen.Model.LayerVector
 */
Ext.define('Carmen.Model.LayerVector', {
  extend: 'Carmen.Model.LayerVectorial',
  fields: [
    { name: "layerType", type: "string", defaultValue: 'VECTOR', convert: function (value) { if (Ext.isObject(value)) return value.layerTypeCode; return value; } },

    { name: "msLayerData", type: "string" }  // mapfile data : DATA
  ]
});

/**
 * Generic tab for type of vector layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabVector', {
  extend: 'Carmen.Generic.Form',
  name: 'GTabVector',
  title: _T_LAYER_VECTOR('title'),
  height: 'auto',
  layout: 'form',
  otherTabs: ['TabStyle', 'TabLabel', 'TabField', 'TabMeta', 'TabOGC'],
  modelName: 'Carmen.Model.LayerVector',

  // methods
  initComponent: function () {
    var me = this;

    console.log("GTabVector");

    // items on this panel
    this.items = [{
      xtype: 'textfield',
      id: 'layerTitle',
      name: 'layerTitle',
      fieldLabel: _T_LAYER_VECTOR('fields.title'),
      allowBlank: false,
      maxLength: 255,
      enforceMaxLength: true
    }, {
      xtype: 'fieldcontainer',
      fieldLabel: _T_LAYER_VECTOR('fields.name'),
      layout: 'hbox',
      width: Carmen.Util.WINDOW_WIDTH - 40,
      items: [registerWarningOnIdentifierLength({
        xtype: 'textfield',
        id: 'layerName',
        name: 'layerName',
        allowBlank: false,
        /*  recommended that the name not contain spaces, special characters, or begin with a number */
        vtype: 'identifier',
        flex: 5
      }), {
        xtype: 'button',
        name: 'layerName_button',
        text: _T_LAYER_VECTOR('fields.auto'),
        margin: '0 10px 0 0',
        /**
         * auto calculation of layerName field
         */
        handler: function () {
          var ctrlFrom = me.down('[name=layerTitle]');
          var ctrlTo = me.down('[name=layerName]');
          if (ctrlFrom && ctrlTo) {
            ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
          }
        },
        flex: 1
      }, {
        xtype: 'textfield',
        labelAlign: 'right',
        id: 'layerIdentifier',
        name: 'layerIdentifier',
        fieldLabel: _T_LAYER_VECTOR('fields.ident'),
        allowBlank: false,
        /*  recommended that the name not contain spaces, special characters, or begin with a number */
        vtype: 'identifier',
        flex: 5
      }, {
        xtype: 'button',
        name: 'layerIdentifier_button',
        text: _T_LAYER_VECTOR('fields.auto'),
        /**
         * auto calculation of layerName field
         */
        handler: function () {
          var ctrlFrom = me.down('[name=layerTitle]');
          var ctrlTo = me.down('[name=layerIdentifier]');
          if (ctrlFrom && ctrlTo) {
            ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
          }
        },
        flex: 1
      }]
    }, {
      xtype: 'fieldcontainer',
      fieldLabel: _T_LAYER_VECTOR('fields.path'),
      layout: 'hbox',
      width: Carmen.Util.WINDOW_WIDTH - 40,
      items: [{
        name: 'msLayerData',
        xtype: 'textfield',
        allowBlank: false,
        maxLength: 512,
        enforceMaxLength: true,
        flex: 11
      }, {
        xtype: 'button',
        text: _T_LAYER_VECTOR('fields.browse'),
        flex: 1,
        name: 'msLayerData_button',
        handler: function () {
          var windowClass = "Ext.ux.FileBrowserWindow";
          var window = Ext.getCmp(CMP_REFERENCE(windowClass))
            || Ext.create(windowClass, {
              id: CMP_REFERENCE(windowClass),
              selectExtension: '*.shp,*.mif,*.tab',
              uploadExtension: '.shp,.prj,.dbf,.shx,.qpj,.mid,.mif,.tab,.id,.map,.dat',
              dataUrl: Routing.generate('carmen_ws_filebrowser', { routing: 'noroute' }),
              defaultPath: '/Root',
              relativeRoot: '/Root/Publication',
              listeners: {
                selectedfile: function (wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                  var ctrlTo = me.down('[name=msLayerData]');
                  ctrlTo.setValue(fullPathName);
                  wind.close();

                  me.getMsGeometryTypeFromSelectedFile(fullPathName);
                }
              }
            });
          window.show();
        }
      }]
    }, {
      xtype: 'fieldcontainer',
      fieldLabel: _T_LAYER_VECTOR('fields.proj'),
      width: Carmen.Util.WINDOW_WIDTH - 40,
      layout: 'hbox',
      items: [{
        flex: 6,
        margin: '0 10px 0 0',
        xtype: 'comboboxprojections',
        name: 'layerProjectionEpsg',
        editable: false,
        value: Carmen.Defaults.PROJECTION
      }, { flex: 6 }]
    }, {
      xtype: 'fieldcontainer',
      fieldLabel: _T_LAYER_VECTOR('fields.type'),
      layout: 'hbox',
      items: [{
        flex: 9,
        margin: '0 10px 0 0',
        xtype: 'combobox',
        name: 'msGeometryType',
        queryMode: 'local',
        displayField: 'displayField',
        valueField: 'valueField',
        store: Carmen.Dictionaries.GEOMETRYTYPES,
        autoLoadOnValue: true,
        autoSelect: true,
        editable: false
      }, {
        flex: 3,
        xtype: 'hiddenfield',
        name: 'layerDataEncoding'
      }]
    }, {
      xtype: 'fieldcontainer',
      fieldLabel: _T_LAYER_VECTOR('fields.scales'),
      layout: 'hbox',
      width: Carmen.Util.WINDOW_WIDTH - 40,
      items: [{
        labelAlign: 'right',
        labelWidth: 60,
        xtype: 'numberfield',
        name: 'layerMinscale',
        fieldLabel: _T_LAYER_VECTOR('fields.min'),
        allowBlank: false,
        maxLength: 10,
        enforceMaxLength: true,
        minValue: Carmen.Defaults.MINSCALE,
        maxValue: Carmen.Defaults.MAXSCALE,
        hideTrigger: true,
        keyNavEnabled: false,
        mouseWheelEnabled: false,
        allowOnlyWhitespace: false,
        allowDecimals: false,
        allowExponential: false,
        flex: 6,
        margin: '0 10px 0 0'
      }, {
        labelAlign: 'right',
        xtype: 'numberfield',
        name: 'layerMaxscale',
        vtype: '',
        fieldLabel: _T_LAYER_VECTOR('fields.max'),
        allowBlank: false,
        maxLength: 10,
        enforceMaxLength: true,
        minValue: Carmen.Defaults.MINSCALE,
        maxValue: Carmen.Defaults.MAXSCALE,
        hideTrigger: true,
        keyNavEnabled: false,
        mouseWheelEnabled: false,
        allowOnlyWhitespace: false,
        allowDecimals: false,
        allowExponential: false,
        flex: 6
      }]
    }, {
      xtype: 'fieldset',
      title: _t("Layer.window.titleLayerBackground"), //"Paramétrages d'affichages de la couche",
      layout: 'form',
      items: [{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'checkbox',
          name: 'layerIsBackground',
          fieldLabel: _t("Layer.forms.fields.layerIsBackground"),
          flex: 6,
          labelWidth: '80%',
          listeners: [{
            change: function (me, value) {
              var show_component = false;
              var is_checked = value;
              if (is_checked) {
                show_component = true;
              }

              var backgroundDisplayImage = Ext.ComponentQuery.query('[name=backgroundDisplayImage]')[0];
              backgroundDisplayImage.setVisible(show_component);
            }
          }]
        }, {
          xtype: 'checkbox',
          name: 'layerIsDefaultBackground',
          fieldLabel: _t("Layer.forms.fields.layerIsDefaultBackground"),
          flex: 6,
          labelWidth: '80%',
          listeners: [{
            change: function (me, value) {

              var mainFrame = Ext.ComponentQuery.query('[name=GTabVector]')[0];
              var mainFrameRecord = mainFrame.form._record;
              var layerId = mainFrameRecord.data.layerId;
              var joinedStore = mainFrameRecord.joined;

              for (var i = 0; i < joinedStore.length; i++) {
                var current_store = joinedStore[i];
                if (current_store.config.model === "Carmen.Model.Layer") {
                  for (var i = 0; i < current_store.data.items.length; i++) {
                    var current_layer = current_store.data.items[i];
                    if (current_layer.id !== layerId) {
                      current_layer.data.layerIsDefaultBackground = false;
                    }
                  }
                }
              }
            }
          }]
        }]
      }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        hidden: true,
        name: "backgroundDisplayImage",
        items: [{

          fieldLabel: _t("Layer.forms.fields.layerBackgroundImage"),
          name: 'layerBackgroundImage',
          xtype: 'textfield',
          labelWidth: '40%',
          width: Carmen.Util.WINDOW_WIDTH - 90,
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.LOGOPATH_IMAGES,
          editable: false,
          autoLoadOnValue: true,
          autoSelect: true,
          flex: 8
        }, {
          xtype: 'button',
          text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
          name: 'uiLogopath_button',
          flex: 3,
          handler: function () {
            var windowClass = "Ext.ux.FileBrowserWindow";
            var window = Ext.getCmp(CMP_REFERENCE(windowClass))
              || Ext.create(windowClass, {
                id: CMP_REFERENCE(windowClass),
                selectExtension: '*.jpg,*.gif,*.png',
                uploadExtension: '.jpg,.gif,.png',
                dataUrl: Routing.generate('carmen_ws_filebrowser', { routing: 'noroute' }),
                defaultPath: '/Root',
                relativeRoot: '/Root/IHM',
                listeners: {
                  selectedfile: function (wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                    var ctrlTo = me.down('[name=layerBackgroundImage]');
                    ctrlTo.setValue(fullPathName);
                    wind.close();
                  }
                }
              });
            window.show();
          },

          listeners: {
            afterrender: function () {
              var check = me.down('[name=uiLogo]');
              if (check) { check.fireEvent('change', check); }
            }
          }
        }]
      }]
    }];

    this.callParent(arguments);
  },

  loadRecord: function (record) {
    var ret = this.callParent(arguments);

    var components = ['msLayerData'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);

    this.on('afterrender', function () {
      var msGeometryType = this.down('[name=msGeometryType]').getValue();
      var layerDataEncoding = this.down('[name=layerDataEncoding]').getValue();
      var msLayerData = this.down('[name=msLayerData]').getValue();
      if ((!msGeometryType || !layerDataEncoding) && msLayerData) {
        this.getMsGeometryTypeFromSelectedFile(msLayerData);
      }
    }, this);
    if (this.rendered) this.fireEvent('afterrender');
    return ret;
  },

  /**
   * get layer type from selected file
   * @param {string} selectedFile
   * @returns {undefined}
   */
  getMsGeometryTypeFromSelectedFile: function (selectedFile) {
    var msGeometryType = this.down('[name=msGeometryType]');
    var layerDataEncoding = this.down('[name=layerDataEncoding]');
    msGeometryType.mask();
    Ext.Ajax.request({
      url: Routing.generate('carmen_ws_helpers_geometrytype', {
        layertype: "VECTOR",
        layerfile: encodeURIComponent(selectedFile)
      }),
      method: 'GET',
      success: function (response) {
        msGeometryType.unmask();
        var oRes = eval("(" + response.responseText + ")");
        msGeometryType.setValue(oRes.msGeometryType);
        layerDataEncoding.setValue(oRes.layerDataEncoding);
      },
      failure: function (response) {
        if (Carmen.notAuthenticatedException(response)) return;
        msGeometryType.unmask();
        var oRes = eval("(" + response.responseText + ")");
        Carmen.Notification.msg(_t('form.msgFailureTitle'), _T_LAYER_VECTOR('msgFailure.canGetLayerTypeFromSelectedFile') + "\n" + oRes.description);
      }
    });
  }
});
