var _T_LAYER_UI = function (key) {
  return _t("Layer.window." + key);
};

/**
 * window of layer
 */
Ext.define('Carmen.Window.Layer', {
  statics: {
    openedWindow: null,
    // layer type
    TYPES: {
      VECTOR: 'Vector',
      RASTER: 'Raster',
      WMS: 'WMS',
      WFS: 'WFS',
      WMSC: 'WMSC',
      WMTS: 'WMTS',
      DATA: 'Data',
      MAP: 'Map',
      LOCAL_DATA: 'LocalData',
      POSTGIS: 'Postgis',
      TILE_VECTOR: 'TileVector'
    },

    classPrefix: 'Carmen.FormSheet.Layer.',

    /**
     * Return sheet component. Created if does not exist
     * @param string className   class name
     * @param string layerType   layer type
     * @param object classConfig config of component
     * @returns object
     */
    getSheet: function (className, layerType, classConfig) {
      classConfig = classConfig || {};
      console.log(className, layerType, classConfig, layerType, " ; ", Carmen.Window.Layer.getCmp(className, layerType))
      return Carmen.Window.Layer.getCmp(className, layerType)
        || Ext.create(
          Carmen.Window.Layer.classPrefix + className
          , Ext.apply(classConfig, {
            id: Carmen.Window.Layer.getStaticId(className, layerType)
          })
        );
    },

    /**
     * private static method, return sheet component or window component
     * @param string className
     * @param string layerType
     * @returns object
     */
    getCmp: function (className, layerType) {
      return Ext.getCmp(this.getStaticId(className, layerType));
    },

    /**
     * private static method, return id of sheet component or id of window component
     * @param string className  class name
     * @param string layerType  layer type
     * @returns string
     */
    getStaticId: function (className, layerType) {
      console.log(Carmen.Window.Layer.classPrefix, layerType, className);
      return CMP_REFERENCE(Carmen.Window.Layer.classPrefix + layerType + '.' + className);
    },

    /**
     * private static method, create window and show this
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     * @param string                  layerType (index of Carmen.Model.Layer.TYPES or value of Carmen.Model.Layer.TYPES)
     */
    editById: function (layerId) {
      var layerRecord;
      if (Ext.isObject(Carmen.user.map) && (Carmen.user.map instanceof Carmen.Model.Map)) {
        var layersStore = Carmen.user.map.get('layers');
        if (layersStore instanceof Ext.data.Store) {
          layerRecord = layersStore.findRecord('layerId', layerId);
        }
      }
      if (layerRecord) {
        return Carmen.Window.Layer.editWindow(layerRecord, layerRecord.get('layerType'))
      }
      return null;
    },

    /**
     * private static method, create window and show this
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     * @param string                  layerType (index of Carmen.Model.Layer.TYPES or value of Carmen.Model.Layer.TYPES)
     */
    editWindow: function (layerRecord, layerType) {
      if (!Ext.isObject(Carmen.user.map) || !(Carmen.user.map instanceof Carmen.Model.Map)) {
        Ext.Msg.alert(_t("form.msgFailureTitle"), _t('app.no_map_selected'));
        return;
      }

      if (Carmen.Window.Layer.openedWindow) {
        Carmen.Window.Layer.openedWindow.close();
        Carmen.Window.Layer.openedWindow = null;
      }

      var mapId = Carmen.user.map.get('mapId');
      layerType = (Ext.isDefined(Carmen.Model.Layer.TYPES[layerType]) ? Carmen.Model.Layer.TYPES[layerType] : layerType);

      // called when loading is finished
      var openLayerWindow = function (layerRecord) {
        var layerWindow = Carmen.Window.Layer.getCmp('Window', layerType)
          || Ext.create('Carmen.Window.Layer', { mapId: mapId, layerType: layerType, layerRecord: layerRecord });

        if (!layerWindow) {
          Ext.Msg.alert(_t("form.msgFailureTitle"), _T_LAYER_UI('msgCantInitializeLayerRecord'));
          return;
        }

        Carmen.Window.Layer.openedWindow = layerWindow;
        layerWindow.edit(layerRecord);
        return layerWindow;
      };

      if (!layerRecord) {
        // create a new layer with the model corresponding to layerType
        // TODO: eval à changer
        var klass = eval('Carmen.Model.Layer' + layerType);
        console.log(klass, layerType);
        if(klass){
          layerRecord = klass.loadOneRawData({ mapId: mapId, map: Carmen.user.map });
          layerRecord.phantom = true;
          if (!layerRecord.getMap()) {
            layerRecord.setMap(Carmen.user.map)
          }
        }
      }
      return openLayerWindow(layerRecord);
    },

    /**
     * public static method, called by left toolbar to open window of layer vector
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editVector: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.VECTOR);
    },

    /**
     * public static method, called by left toolbar to open window of layer raster
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editRaster: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.RASTER);
    },

    /**
     * public static method, called by left toolbar to open window of layer WMS
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editWMS: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.WMS);
    },
    
    /**
     * public static method, called by left toolbar to open window of layer WMS
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editTileVector: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.TILE_VECTOR);
    },

    /**
     * public static method, called by left toolbar to open window of layer WFS
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editWFS: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.WFS);
    },

    /**
     * public static method, called by left toolbar to open window of layer WMS-C
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editWMSC: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.WMSC);
    },
    /**
     * public static method, called by left toolbar to open window of layer postgis
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editPostgis: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.POSTGIS);
    },
    /**
     * public static method, called by left toolbar to open window of layer postgis
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editLocalData: function (layerRecord) {
      if (!layerRecord) {
        return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.LOCAL_DATA);
      }

      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.POSTGIS);
    },
    /**
     * public static method, called by left toolbar to open window of layer WMTS
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editWMTS: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.WMTS);
    },
    /**
     * public static method, called by left toolbar to open window of layer
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    editData: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.DATA);
    },
    /**
     * public static method, called by left toolbar to open window of add map
     * @param {Carmen.Model.LayerXXX} layerRecord of current layer if update mode, =null for adding mode
     */
    addMap: function (layerRecord) {
      return Carmen.Window.Layer.editWindow(layerRecord, Carmen.Model.Layer.TYPES.MAP);
    }
  }, //End statics

  autoDestroy: true,
  // ext properties
  extend: 'Carmen.Generic.Window',
  reference: 'window-layer',
  titleTpl: "",
  emptyLayerTitle: _T_LAYER_UI('emptyLayerTitle'),

  // id of current map
  mapId: 0,
  // record of current layer
  layerRecord: null,

  // layer type : Vector | Raster | Postgis | WMS | WMSC | WFS | WMTS | DATA
  layerType: Carmen.Model.Layer.TYPES.VECTOR,

  /**
   * return id of window component
   */
  getId: function () {
    return Carmen.Window.Layer.getStaticId('Window', this.layerType);
  },

  /**
   * intialisation of component
   */
  initComponent: function () {
    var me = this;

    var classPrefix = 'Carmen.FormSheet.Layer.';
    me.id = me.getId();

    // get generic tab
    var mainGTab = me.getSheet('GTab' + me.layerType);
    console.log(mainGTab);
    var otherTabs = mainGTab.otherTabs;
    var tabs = [], otherTabsObj = [];
    tabs.push(mainGTab);

    /* add all sheets - Hidden if layer is not in  update mode */
    Ext.each(otherTabs, function (className) {
      console.log(className)
      if (TABSFORDELETING.indexOf(className) == -1) {
        console.log(253, me.getSheet(className, { hidden: false, layerType: me.layerType }) )
        var sheet = me.getSheet(className, { hidden: false, layerType: me.layerType });
        tabs.push(sheet);
      }
    });
    otherTabsObj = [].concat(tabs);

    me.mainTab = otherTabsObj.shift();
    me.otherTabs = otherTabsObj;

    me.items = [{
      xtype: 'tabpanel',
      tabRotation: 0,
      items: tabs
    }];

    me.callParent();

    var btApply = Ext.getCmp('btApply' + me.getId());
    if (btApply) {
      btApply.setVisible(false);
    }

    me.on('show', function () {
      var updateMode = (!me.record.phantom);
      if (updateMode) return;
      var tabpanel = me.down('tabpanel');
      if (!tabpanel) return;
      //For each other tabs (= not the first tab) 
      Ext.each(me.otherTabs || [], function (oForm) {
        tabpanel.remove(oForm, true);
      });
    })
  },

  /**
   * called to show current window
   * @param {Carmen.Model.LayerXXX} layerRecord of current layer
   */
  edit: function (layerRecord) {

    var me = this;
    var forms = me.getForms();

    Ext.each(forms, function (form) {
      form.loadRecord(layerRecord);
    });

    this.titleTpl = new Ext.XTemplate(_T_LAYER_UI('titleTpl' + this.layerType));
    this.setTitle(
      this.titleTpl.apply({ layerTitle: (layerRecord.phantom ? this.emptyLayerTitle : layerRecord.get("layerTitle")) })
    );

    this.callParent([layerRecord]);
  },

  /**
   * Return generic sheet of current window
   * @param string className  class name of generic sheet
   * @returns object
   */
  getSheet: function (className, config) {
    config = config || {};
    return Carmen.Window.Layer.getSheet(className, this.layerType, config);
  },

  preSaveSuccess: function (operation, jsonResponse) {
    this.currentExtent = null;
    var oldId = null;
    if (Carmen.user.map) {
      this.currentExtent = Carmen.application.map.OL_map.getExtent();
    }
  },

  postSaveSuccess: function (operation, jsonResponse) {
    var jsOws = null, layerId = null, layer = null;
    if (Ext.isDefined(jsonResponse) && Ext.isObject(jsonResponse)) {
      jsOws = jsonResponse.jsonContext || jsOws;
      layerId = jsonResponse.layerId || layerId;
      layer = jsonResponse.layer || layer;
    }
    var bReopenNew = false;
    if (operation instanceof Ext.data.operation.Create && layer && Carmen.user.map) {
      // retour de la création d'un layer
      if (jsOws && layerId) {
        Carmen.application.loadLayerFromContext(jsOws, layerId, Carmen.application.ui.getSelectedTreeNode());
      }
      Carmen.user.map.get('layers').addLayer(layer);
      Carmen.application.ui.layerTreePanel.createBackup();
      bReopenNew = true;
    }
    else {
      // retour de la modification d'un layer
      var layerModel = Carmen.user.map.getLayerById(layerId);
      if (layerModel && layer) {
        layerModel.set('layerMetadataUuid', layer.layerMetadataUuid);
        layerModel.set('hrefLayerMetadataUuid', layer.hrefLayerMetadataUuid);
        layerModel.set('layerMetadataFile', layer.layerMetadataFile);
        layerModel.set('hrefLayerMetadataFile', layer.hrefLayerMetadataFile);
      }
      if (jsOws && layerId) {
        Carmen.application.updateLayerFromContext(jsOws, layerId, Carmen.application.ui.getSelectedTreeNode());
      }
      Carmen.application.ui.updateTreesFromMapLayers(Carmen.user.map.get('layers'));
    }
    Carmen.application.ui.refreshTrees(this.currentExtent);
    this.currentExtent = null;

    var TabField = this.getSheet('TabField');
    if (TabField) {
      TabField.down('grid').getStore().commitChanges();
    }
    if (bReopenNew && layerId) {
      var tabpanel = Carmen.Window.Layer.editById(layerId).down('tabpanel');
      if (tabpanel.items.getCount() > 1) tabpanel.setActiveTab(1);
    }
  }

});
