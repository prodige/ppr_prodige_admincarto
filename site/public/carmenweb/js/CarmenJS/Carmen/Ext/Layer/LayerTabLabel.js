var LABEL_TRANSLATE = function (key) {
  return _t("Layer.forms.TabLabel." + key);
};

// The data store containing the list of states
var storealign = Ext.create('Ext.data.Store', {
  fields: ['name', 'value'],
  data: [
    { "name": "gauche", "value": "left" },
    { "name": "centre", "value": "center" },
    { "name": "droite", "value": "right" }
  ]
});

/**
 * Carmen.Model.Layer.Fields
 */
Ext.define('Carmen.Model.Layer.Label', {
  extend: 'Carmen.Model.Layer',
  idProperty: 'layerId',
  fields: [{
    name: "msLabelMinScale",
    type: "int",
    defaultValue: '0'
  }, {
    name: "msLabelMaxScale",
    type: "int"
  }, {
    name: "msLabelFont",
    type: "string"
  },{
    name: "msLabelRepeatDistantce",
    type: "int",
    defaultValue: 0
  }, {
    name: "msLabelFontSize",
    type: "int",
    defaultValue: Carmen.Defaults.FONTSIZE
  },{
    name: "msLabelPositionGroup",
    type: "string",
    defaultValue: 'left'
  }]
});

/**
 * Labels tab
 */
Ext.define('Carmen.FormSheet.Layer.TabLabel', {
  extend: 'Carmen.Generic.Form',
  // xtype: 'cmngenericform',
  title: LABEL_TRANSLATE('title'),
  height: 'auto',
  width: '100%',
  layout: 'anchor',
  statics: {},
  fieldDefaults: {
    //combineErrors: true,
    msgTarget: 'side'
  },

  onChangeCheckbox: function (checkbox) {
    Ext.each(
      checkbox.up('fieldcontainer').query(':not(checkboxfield), radiofield'),
      function (cmp) {
        cmp.setDisabled(!checkbox.checked);
        if (cmp.is('field:not(radiofield)'))
          cmp.allowBlank = !checkbox.checked;
      }
    );
  },

  fireOnChangeCheckbox: function (element) {
    Ext.each(
      element.up('fieldcontainer').query('checkboxfield:not(radiofield)'),
      function (cmp) {
        cmp.fireEvent('change', cmp);
      }
    );
  },

  /**
   * intialisation of component
   */
  initComponent: function () {
    var me = this;

    me.defaults = { defaults: { margin: 10 }, margin: '0 0 10 0' };
    me.items = [
      {
        xtype: 'panel',
        frame: true,
        title: LABEL_TRANSLATE('panel.DefinitionItem'),
        items: [

          {
            xtype: 'fieldcontainer',
            //combineErrors: true,
            layout: 'hbox',
            //        padding : '0 50 0 50',
            items: [ // total flex = 12
              {
                xtype: 'checkbox',
                boxLabel: LABEL_TRANSLATE('fields.msLabelItem'),
                name: 'msLabelItemActif',
                listeners: {
                  change: function () {
                    var checked = this.getValue();
                    var cmp;
                    if (cmp = me.down('[name=msLabelItem]')) {
                      cmp.allowBlank = (!checked);
                      if (!checked) {
                        cmp.setValue(null);
                      }
                    }
                    if (cmp = me.down('[name=mslabel_properties]')) {
                      cmp.setDisabled(!checked);
                    }
                    if (cmp = me.down('[name=msLabelPositionGroup]')) {
                      cmp.allowBlank = (!checked);
                    }
                  }
                },
                flex: 1
              }, {
                xtype: 'combobox',
                name: 'msLabelItem',
                queryMode: 'local',
                margin: '0 0 0 5',
                displayField: 'fieldName',
                valueField: 'fieldName',
                storeId: 'fields',
                editable: false,
                autoLoadOnValue: true,
                autoSelect: true,
                flex: 2,
                listeners: {
                  change: function () {
                    var value = this.getValue();
                    var checked = (Ext.isDefined(value) && value != null && value != "");
                    if (cmp = me.down('[name=msLabelItemActif]')) {
                      cmp.setValue(checked);
                    }
                  }
                }
              }
            ]
          }

        ]
      }

      , {
        xtype: 'panel',
        name: 'mslabel_properties',
        layout: 'anchor',
        frame: true,
        title: LABEL_TRANSLATE('extra.apparence'),
        //        padding : '0 50 0 50',
        items: [{
          xtype: 'form',
          name: 'labelForm',
          items: [
            {
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: LABEL_TRANSLATE('fieldContainers.scale'),
              layout: 'hbox',
              margin: '0 0 15 0',
              anchor: '100%',
              items: [ // total flex = 12
                {
                  html: '<div style = "text-align:center; padding-top: 3px">' + LABEL_TRANSLATE('fields.msLabelMinscale') + '</div>',
                  flex: 1
                }, {
                  xtype: 'numberfield',
                  name: 'msLabelMinScale',
                  flex: 2,
                  padding: '0 20 0 0',
                  maxLength: 10,
                  allowBlank: true,
                  allowDecimals: false,
                  allowExponential: false
                }, {
                  html: '<div style = "text-align:center; padding-top: 3px">' + LABEL_TRANSLATE('fields.msLabelMaxscale') + '</div>',
                  flex: 1
                }, {
                  xtype: 'numberfield',
                  name: 'msLabelMaxScale',
                  flex: 2,
                  padding: '0 20 0 0',
                  maxLength: 10,
                  allowBlank: true,
                  allowDecimals: false,
                  allowExponential: false
                }, {
                  xtype: 'checkboxfield',
                  boxLabel: LABEL_TRANSLATE('fields.msLabelForce'),
                  name: 'msLabelForce',
                  flex: 2
                }
              ]
            },{
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: LABEL_TRANSLATE('fieldContainers.font'),
              layout: 'hbox',
              width: '100%',
              margin: '0 0 40 0',

              defaults: {
                padding: '0 40 0 0'
              },
              items: [ // flex : 12
                {
                  xtype: 'colorfield',
                  layout: 'hbox',
                  fieldLabel: LABEL_TRANSLATE('fields.msLabelFontColor'),
                  name: 'msLabelFontColor',
                  flex: 2
                }, {
                  xtype: 'combobox',
                  fieldLabel: LABEL_TRANSLATE('fields.msLabelFont'),
                  name: 'msLabelFont',
                  flex: 2,
                  queryMode: 'local',
                  displayField: 'displayField',
                  valueField: 'withoutExtension',
                  store: Carmen.Dictionaries.FONTS,
                  editable: false,
                  autoLoadOnValue: true,
                  autoSelect: true,
                }, {
                  xtype: 'combobox',
                  fieldLabel: LABEL_TRANSLATE('fields.msLabelFontSize'),
                  name: 'msLabelFontSize',
                  flex: 2,
                  queryMode: 'local',
                  displayField: 'displayField',
                  valueField: 'valueField',
                  store: Carmen.Dictionaries.FONTSIZES,
                  editable: false,
                  autoLoadOnValue: true,
                  autoSelect: true,
                }]
            }, // end of fontFieldContainer
            {
              //*******  Contour ************
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: "",
              layout: 'hbox',
              width: '100%',
              margin: '0 0 40 0',

              defaults: {
                padding: '0 40 0 0'
              },
              items: [{
                xtype: 'checkbox',
                //layout: 'hbox',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelContour'),
                name: 'msLabelContour',
                flex: 1,
                listeners: {
                  change: function () {
                    var field = me.down('#msOutlineColor');
                    field.setDisabled(!this.getValue());
                    field.fireEvent('change', field);
                    var field = me.down('#msoutlinewidth');
                    field.setDisabled(!this.getValue());
                    field.fireEvent('change', field);
                  }
                }
              }, {
                xtype: 'colorfield',
               // layout: 'hbox',
                id: 'msOutlineColor',
                fieldLabel: LABEL_TRANSLATE('fields.msOutlineColor'),
                name: 'msOutlineColor',
                disabled: true,
                flex: 3
              }, {
                id: 'msoutlinewidth',
                xtype: 'numberfield',
                //layout: 'hbox',
                fieldLabel: LABEL_TRANSLATE('fields.msoutlinewidth'),
                name: 'msoutlinewidth',
                disabled: true,
                flex: 3
              }]
            }, {
              //*******  Ombre ************
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: "",
              layout: 'hbox',
              width: '100%',
              margin: '0 0 10 0',

              defaults: {
                padding: '0 10 0 0'
              },
              items: [{
                xtype: 'checkbox',
                layout: 'hbox',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelShadow'),
                name: 'msLabelShadow',
                flex: 1,
                listeners: {
                  change: function () {
                    let field = me.down('#msLabelShadowColor');
                    field.setDisabled(!this.getValue());
                    if (!this.getValue()) {

                      field.value = 0;
                      field.setValue(0);

                      field.value = null;
                      field.setValue(null);
                    }
                    field.fireEvent('change', field);
                  }
                }
              }, {
                xtype: 'colorfield',
                layout: 'hbox',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelShadowColor'),
                name: 'msLabelShadowColor',
                disabled: true,
                flex: 2,
                id: 'msLabelShadowColor'
              }]
            },

            {
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: LABEL_TRANSLATE('fieldContainers.position'),
              layout: 'fit',
              margin: '0 0 10 0',
              items: [{
                xtype: 'radiogroup',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelPositionRadiogroup'),
                name: 'msLabelPositionGroup',
                allowBlank: true,
                msgTarget: 'side',
                autoFitErrors: false,
                layout: 'column',
                defaultType: 'container',
                items: [{
                  columnWidth: .1,
                  layout: 'center',
                  items: [{
                    xtype: 'radiofield',
                    boxLabel: LABEL_TRANSLATE('fields.msLabelAuto'),
                    name: 'msLabelPosition',
                    id: 'msLabelPosition_auto',
                    inputValue: LABELPOSITION.TYPES.AUTO.value
                  }]
                }, {
                  columnWidth: .1,
                  items: [{
                    html: '<div style = "text-align:center; padding-top: 3px">'
                      + LABEL_TRANSLATE('extra.or') + '</div>'

                  }]
                }, {
                  columnWidth: .2,
                  margin: '0 0 0 10',
                  layout: {
                    type: 'vbox',
                    align: 'left'
                  },
                  items: [
                    fnGetLabelPosition('UPPER', 'LEFT')
                    , fnGetLabelPosition('CENTER', 'LEFT')
                    , fnGetLabelPosition('LOWER', 'LEFT')
                  ]
                }, {
                  columnWidth: .2,
                  layout: {
                    type: 'vbox',
                    align: 'left'
                  },
                  items: [
                    fnGetLabelPosition('UPPER', 'CENTER')
                    , fnGetLabelPosition('CENTER', 'CENTER')
                    , fnGetLabelPosition('LOWER', 'CENTER')
                  ]
                }, {
                  columnWidth: .2,
                  layout: {
                    type: 'vbox',
                    align: 'left'
                  },
                  items: [
                    fnGetLabelPosition('UPPER', 'RIGHT')
                    , fnGetLabelPosition('CENTER', 'RIGHT')
                    , fnGetLabelPosition('LOWER', 'RIGHT')
                  ]
                }, {
                  columnWidth: .2,
                  html: ''
                }]
              }  // end of radiogroup
              ]
            },// end of positionFieldContainer

            {
              //combineErrors: true,
              xtype: 'fieldcontainer',
              layout: 'hbox',
              margin: '0 0 10 0',
              items: [ // flex total = 120
                {
                  xtype: 'checkboxfield',
                  boxLabel: LABEL_TRANSLATE('fields.msLabelOffset'),
                  name: 'msLabelOffsetActif',
                  padding: '0 0 0 0',
                  flex: 11,
                  listeners: {
                    change: me.onChangeCheckbox
                  }
                }, {
                  html: '<div style = "text-align:right; padding-top: 3px">'
                    + LABEL_TRANSLATE('fields.msLabelOffsetX') + '</div>',
                  flex: 8
                }, {
                  xtype: 'numberfield',
                  name: 'msLabelOffsetX',
                  hideTrigger: true,
                  keyNavEnabled: false,
                  mouseWheelEnabled: false,
                  flex: 14
                }, {
                  html: '<div style = "text-align:right; padding-top: 3px">'
                    + LABEL_TRANSLATE('fields.msLabelOffsetY') + '</div>',
                  flex: 8
                }, {
                  xtype: 'numberfield',
                  name: 'msLabelOffsetY',
                  hideTrigger: true,
                  keyNavEnabled: false,
                  mouseWheelEnabled: false,
                  flex: 14,
                  listeners: {
                    afterrender: me.fireOnChangeCheckbox
                  }
                }, {
                  html: '',
                  flex: 20
                }]
            },
            {
              xtype: 'fieldcontainer',
              fieldLabel: LABEL_TRANSLATE('fieldContainers.angle'),
              name: 'angleFieldContainer',
              layout: 'hbox',
              margin: '0 0 10 0',
              items: [ // total flex : 120
                {
                  xtype: 'checkboxfield',
                  boxLabel: LABEL_TRANSLATE('fields.msLabelAngle'),
                  name: 'msLabelAngleActif',
                  flex: 15,
                  listeners: {
                    change: function () {
                      var field = me.down('#msLabelAngleType_degrees');
                      field.setDisabled(!this.getValue());
                      field.fireEvent('change', field);
                      var field = me.down('#msLabelAngleType_fields');
                      field.setDisabled(!this.getValue());
                      field.fireEvent('change', field);
                    }
                  }
                }, {
                  xtype: 'radiofield',
                  name: 'msLabelAngleType',
                  inputValue: 'degrees',
                  id: 'msLabelAngleType_degrees',
                  flex: 2,
                  listeners: {
                    change: (msLabelAngleType_degrees = function () {
                      var field = me.down('[name=msLabelAngleDegrees]');
                      field.setDisabled(!this.getValue());
                    }),
                    enable: msLabelAngleType_degrees,
                    afterrender: msLabelAngleType_degrees
                  }
                }, {
                  xtype: 'numberfield',
                  name: 'msLabelAngleDegrees',
                  hideTrigger: true,
                  keyNavEnabled: false,
                  mouseWheelEnabled: false,
                  flex: 18
                }, {
                  html: '<div style = "text-align:center; padding-top: 3px">'
                    + LABEL_TRANSLATE('fields.msLabelAngleDegrees')
                    + '</div>',
                  flex: 5
                }, {
                  html: '',
                  flex: 10
                }, {
                  xtype: 'radiofield',
                  name: 'msLabelAngleType',
                  inputValue: 'fields',
                  id: 'msLabelAngleType_fields',
                  flex: 2,
                  listeners: {
                    change: (msLabelAngleType_fields = function () {
                      var field = me.down('[name=msLabelAngleField]');
                      field.setDisabled(!this.getValue());
                    }),
                    enable: msLabelAngleType_fields,
                    afterrender: msLabelAngleType_fields
                  }
                }, {
                  html: '<div style = "text-align:center; padding-top: 3px">'
                    + LABEL_TRANSLATE('fields.msLabelAngleField') + '</div>',
                  flex: 5
                }, {
                  xtype: 'combobox',
                  name: 'msLabelAngleField',
                  queryMode: 'local',
                  displayField: 'fieldName',
                  valueField: 'fieldName',
                  storeId: 'fieldsNumeric',
                  editable: false,
                  autoLoadOnValue: true,
                  autoSelect: true,
                  flex: 18,
                  listeners: {
                    afterrender: me.fireOnChangeCheckbox
                  }
                }]
            },// end of angleFieldContainer
            {
              //*******  Alignement ************
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: "",
              layout: 'hbox',
              width: '100%',
              margin: '0 0 10 0',

              defaults: {
                padding: '0 10 0 0'
              },
              items: [{
                xtype: 'checkbox',
                layout: 'hbox',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelMultiLine'),
                name: 'msLabelMultiLine',
                flex: 2,
                listeners: {
                  change: function () {
                    let field = me.down('#msLabelWrapLine');
                    field.setDisabled(!this.getValue());
                    field.fireEvent('change', field);

                    field = me.down('#msLabelalign');
                    console.log(field);
                    field.setDisabled(!this.getValue());
                    field.fireEvent('change', field);
                  }
                }
              }, {
                xtype: 'textfield',
                layout: 'hbox',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelWrapLine'),
                name: 'msLabelWrapLine',
                disabled: true,
                flex: 2,
                id: 'msLabelWrapLine',
                listeners: {
                  beforerender: function () {
                    console.log('TEST', me.getRecord());
                    console.log(me.getRecord().data.msLabel.data.msLabelWrapLine);
                   /* if(this.value){
                      this.setValue(String.fromCharCode(this.value * 1));
                      console.log(me.getRecord().data.msLabel.data.msLabelWrapLine);
                      me.getRecord().data.msLabel.data.msLabelWrapLine = String.fromCharCode(this.value * 1);
                    }*/
                    
                  }
                }
              }, {
                xtype: 'combobox',
                fieldLabel: LABEL_TRANSLATE('fields.msLabelalign'),
                layout: 'hbox',
                name: 'msLabelalign',
                flex: 2,
                queryMode: 'local',
                displayField: 'name',
                valueField: 'value',
                store: storealign,
                editable: false,
                autoLoadOnValue: true,
                autoSelect: true,
                disabled: true,
                id: "msLabelalign"
              }]
            },
            {
              //*******  Repeat distance ************
              xtype: 'fieldcontainer',
              //combineErrors: true,
              fieldLabel: "",
              layout: 'hbox',
              width: '25%',
              margin: '0 0 10 0',

              defaults: {
                padding: '0 10 0 0'
              },
              items: [{
                xtype: 'numberfield',

                fieldLabel: LABEL_TRANSLATE('fields.msLabelRepeatDistantce'),
                name: 'msLabelRepeatDistantce',

              }]
            },
            {
              xtype: 'checkboxfield',
              boxLabel: LABEL_TRANSLATE('fields.msLabelAnchorSymbol'),
              name: 'msLabelAnchorSymbol'
            }]
        }]
      }];// end of mainFieldSet

    this.callParent();

    this.on("afterrender", function () {
      var hasValue = me.down('[name=msLabelItem]').getValue();
      hasValue = Ext.isDefined(hasValue) && hasValue != null && hasValue != "";
      me.down('[name=msLabelItemActif]').setValue(hasValue);
      me.down('[name=msLabelItemActif]').fireEvent('change');
    })
  },// end initcomponent

  /**
   * Update layerRecord and labelRecord (from layerRecord)
   * @param {} layerRecord
   */
  updateRecord: function (layerRecord) {
    this.down('[name=labelForm]').updateRecord((layerRecord && layerRecord instanceof Carmen.Model.LayerVectorial ? layerRecord.get('msLabel') : null));
    this.callParent(arguments);
  },

  loadRecord: function (layerRecord) {

    var labelRecord = layerRecord.get('msLabel');
    this.down('[name=labelForm]').loadRecord(labelRecord);
    this.callParent(arguments);


    if (layerRecord instanceof Carmen.Model.Layer) {
      var fields = layerRecord.get('fields');
      if (fields instanceof Ext.data.Store) {
        var data = fields.getData();
        if (data.filtered) {
          data = data.getSource();
        }
        this.storeFields = Ext.create(fields.$className, {
          model: fields.getModel(),
          data: data.getValues('data')
        });
        var toRemove = this.storeFields.query('fieldDuplicate', true);
        this.storeFields.remove(toRemove.items);
        var fields_combobox = this.query('[storeId=fields]');
        Ext.each(fields_combobox, function (combobox) {
          if (combobox instanceof Ext.grid.column.Column) {
            combobox.widget.store = this.storeFields;
          } else {
            combobox.setStore(this.storeFields);
          }
        }, this);

        this.storeFieldsNumeric = Ext.create(fields.$className, {
          model: fields.getModel(),
          data: data.getValues('data')
        });
        var toRemove = this.storeFieldsNumeric.query('fieldDuplicate', true);
        this.storeFieldsNumeric.remove(toRemove.items);
        var toRemove = this.storeFieldsNumeric.queryBy(function (record) { return record.get('fieldDatatype') != FIELD_DATATYPES.TYPES.NUMBER; });
        this.storeFieldsNumeric.remove(toRemove.items);

        var fields_combobox = this.query('[storeId=fieldsNumeric]');
        Ext.each(fields_combobox, function (combobox) {
          if (combobox instanceof Ext.grid.column.Column) {
            combobox.widget.store = this.storeFieldsNumeric;
          } else {
            combobox.setStore(this.storeFieldsNumeric);
          }
        }, this);
      }
    }
  }
});

function fnGetLabelPosition(y, x) {
  var lowerCaseX = x.toLowerCase()
    , lowerCaseY = y.toLowerCase();
  var upperCaseX = x.toUpperCase()
    , upperCaseY = y.toUpperCase();
  var capitalX = Ext.String.capitalize(lowerCaseX)
    , capitalY = Ext.String.capitalize(lowerCaseY);

  if (['LEFT', 'CENTER', 'RIGHT'].indexOf(upperCaseX) == -1) {
    throw 'fnGetLabelPosition :: Type de position X inconnue : ' + upperCaseX;
  }
  if (['UPPER', 'CENTER', 'LOWER'].indexOf(upperCaseY) == -1) {
    throw 'fnGetLabelPosition :: Type de position Y inconnue : ' + upperCaseY;
  }
  var type = upperCaseY + '_' + upperCaseX;
  var label = 'fields.msLabel' + capitalY + capitalX;

  return {
    xtype: 'radiofield',
    boxLabel: LABEL_TRANSLATE(label),
    name: 'msLabelPosition',
    id: 'msLabelPosition_' + LABELPOSITION.TYPES[type].code,
    inputValue: LABELPOSITION.TYPES[type].value
  }
}
