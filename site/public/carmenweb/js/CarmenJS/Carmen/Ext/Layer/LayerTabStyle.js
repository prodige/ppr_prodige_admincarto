var _T_LAYERSTYLE = function (key) {
  return (_t("Layer.forms.TabStyle." + key) == "Layer.forms.TabStyle." + key ? key : _t("Layer.forms.TabStyle." + key));
};

/**
 * Style tab
 */
Ext.define('Carmen.FormSheet.Layer.TabStyle', {
  extend: 'Carmen.Generic.Form',

  xtype: 'Carmen_FormSheet_Layer_TabStyle',

  title: _T_LAYERSTYLE('title'),

  layerType: null,

  initialEvents: {},

  fieldFilters: {},
  fieldsFiltered: {},
  fieldsDefered: {},

  recordAnalyse: null,
  height: 'auto',

  initComponent: function () {
    var me = this;
    console.log("29");
    switch (this.layerType) {
      case Carmen.Model.Layer.TYPES.VECTOR:
      case Carmen.Model.Layer.TYPES.WFS:
      case Carmen.Model.Layer.TYPES.POSTGIS:
        LEX_ANALYSE_TYPE.clearFilter();
        LEX_ANALYSE_TYPE.filterBy(function (entry) {
          return en_developpement.layerAnalyseType(entry) && entry.get('codeField') != LEX_ANALYSE_TYPE.TYPES.RASTER;
        });
        this.initComponentVector();
        break;
      default:
        this.recordAnalyse = LEX_ANALYSE_TYPE.TYPES.RASTER;
        LEX_ANALYSE_TYPE.clearFilter();
        LEX_ANALYSE_TYPE.filterBy(function (entry) {
          return en_developpement.layerAnalyseType(entry) && entry.get('codeField') == LEX_ANALYSE_TYPE.TYPES.RASTER;
        });
        this.initComponentRaster();
        break;
    }

    this.callParent(arguments);

    me.on('afterrender', function () {
      Ext.iterate(me.initialEvents
        /**
         * @param string componentName
         * @param array  componentEvents
         * @scope this=me
         */
        , function (componentName, componentEvents) {
          var component = me.down('[conditionName=' + componentName + ']') || me.down('[name=' + componentName + ']');
          if (!component) return;
          if (!component.isVisible()) return;
          Ext.each(componentEvents, function (fn) { fn.call(component) });
        }
        , me);

      me.attachEvents();
      me.applyAnalyze("distribution");
    });

    me.on('beforedestroy', function () {
      me.detachEvents();

    });
  },

  _showFields: function () {
    this.showFields();
  },

  showFields: function (fieldsFiltered) {
    var me = this;

    fieldsFiltered = fieldsFiltered || me.fieldsFiltered;

    Ext.iterate(fieldsFiltered, function (cmpName, conditions) {
      var field = me.up('window').down('[conditionName=' + cmpName + ']') || me.up('window').down('[name=' + cmpName + ']') || me.up('window').down('[old_name=' + cmpName + ']');
      if (!field) return;
      var visible = conditions.call(me, field, me.fieldFilters);
      if (!Ext.isDefined(field.visibilityFilter) || field.visibilityFilter) {
        field.setVisible(visible);
      }
      if (field.setDisabled) {
        field.setDisabled(!visible);
      }
      if (Ext.isDefined(field.updateAllowBlank)) {
        field.allowBlank = !visible;
      }
      if (!visible && field.name) {
        field.old_name = field.name;
        field.name = null;
        field.query && Ext.each(field.query('field[name]'), function (el) { el.old_name = el.name; el.name = null; });
      } else if (field.old_name) {
        field.name = field.old_name;
        field.query && Ext.each(field.query('field[old_name]'), function (el) { el.name = el.old_name; });
      }
    });
    Ext.iterate(fieldsFiltered, function (cmpName, conditions) {
      var field = me.up('window').down('[conditionName=' + cmpName + ']') || me.up('window').down('[name=' + cmpName + ']');
      if (!field) return;
      var visible = conditions.call(me, field, me.fieldFilters);
      if (field.setValue && !visible) {
        if (field.value !== null) field.originalValue = field.value;
        field.setValue(null);
      }
      if ((field.xtype != "colorfield") && field.reset && !visible) {
        if (field.value !== null) field.originalValue = field.value;
        field.reset();
      }
    });
  },

  detachEvents: function () {
    var me = this;
    Ext.iterate(me.fieldFilters, function (criteria, component) {
      if (!component) return;
      if (component instanceof Ext.form.field.Base) {
        component.un('change', me._showFields, me)
      }
      if (component instanceof Ext.data.Store) {
        component.un('datachanged', me._showFields, me)
      }
    });
    me.fieldFilters = {};
  },

  attachEvents: function () {
    var me = this;

    me.fieldFilters = {
      msClasses: me.getRecord().get('msClasses'),
      analyses: me.up('window').down('[name=layerAnalyseType]'),
      geometrytypes: me.up('window').down('[name=msGeometryType]'),
      symboltype: me.up('window').down('[name=msStyleTypeSymbolGroup]')
    };

    if (me.fieldFilters.geometrytypes) {
      me.fieldFilters.geometrytypes.on('change', function (geomCmp, geometrytype) {
        var analyseCmp = me.fieldFilters.analyses
        analyseCmp.getStore().removeFilter('allowProportional');
        switch (geometrytype) {
          case LEX_GEOMETRY_TYPE.TYPES.LINE:
            //case LEX_GEOMETRY_TYPE.TYPES.POLYGON : 
            analyseCmp.getStore().addFilter({
              id: 'allowProportional',
              filterFn: function (rec) {
                if (analyseCmp && analyseCmp.rendered && analyseCmp.getSelection() && analyseCmp.getSelection().get('codeField') == LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL) {
                  analyseCmp.setSelection(analyseCmp.getStore().findRecord('codeField', LEX_ANALYSE_TYPE.TYPES.UNIQSYMBOL));
                }
                return rec.get('codeField') != LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL;
              }
            });
            break;
        }
      });
      me.fieldFilters.geometrytypes.fireEvent('change', me.fieldFilters.geometrytypes, me.fieldFilters.geometrytypes.getValue());
    }
    Ext.iterate(me.fieldFilters, function (criteria, component) {
      if (!component) return;
      if (component instanceof Ext.form.field.Base || component instanceof Ext.form.CheckboxGroup) {
        component.on('change', me._showFields, me)
      }
      if (component instanceof Ext.data.Store) {
        component.on('datachanged', me._showFields, me)
      }
      me.showFields();
    });
  },

  registerEvents: function (items) {
    /**
     * @scope LayerTabStyle
     */
    var registerEvent = function (item) {
      if (item.columns) {//recursive registration
        if (Ext.isArray(item.columns)) {
          this.registerEvents(item.columns);
        } else {
          this.registerEvents(item.columns.items);
        }
      }
      if (item.items) {//recursive registration
        this.registerEvents(item.items);
      }
      if (item.dockedItems) {//recursive registration
        this.registerEvents(item.dockedItems);
      }
      if (item.buttons) {//recursive registration
        this.registerEvents(item.buttons);
      }

      var name = item.conditionName || item.name;
      if (!name) return;//only for fields or named elements
      if (!item.conditions) return; // only if conditions is defined

      var conditions = item.conditions;
      var defer = false;
      if (Ext.isObject(conditions)) {
        conditions = Ext.apply({}, conditions);
        defer = (conditions.defer) || false;
        delete conditions.defer;

        if (Ext.isDefined(conditions.fn)) {
          conditions = conditions.fn;
        }
      }

      if (!Ext.isFunction(conditions)) {

        /**
         * <pre>
         * <b>NOTE : </b> 
         * if filter = 'all'        : no filter condition
         * if filter = 'non_raster' : only for non raster analyse type
         * if filter = array or string : is like filter.type = 'in'
         * if filter.type = 'in'       : the visibility is true if the value is in the filter.list
         * if filter.type = 'out'      : the visibility is true if the value is NOT in the filter.list
         * </pre>
         * @param string|array[string]|object{type : (out|in),list : array[string]} filter : the filter conditions relative to the value to keep this field visible
         * @param string|number                                                 value 
         * @return boolean 
         */
        var isVisible = function (forComponent, filter, value) {
          var visible = true;
          if (!Ext.isDefined(filter)) return visible;

          if (Ext.isFunction(filter)) {
            return filter.call(this, forComponent, value);
          }
          if (value === null || !Ext.isDefined(value)) return visible;
          switch (filter) {
            case 'all': break;
            case 'non_raster': visible = (value != LEX_ANALYSE_TYPE.TYPES.RASTER); break;
            default:
              if (Ext.isObject(filter)) {
                var type = filter.type;
                var list = filter.list;
                if (!Ext.isArray(list)) list = [list];

                if (type == 'out') {
                  visible = (list.indexOf(value) == -1);
                } else {
                  visible = (list.indexOf(value) != -1);
                }
              }
              else if (Ext.isArray(filter)) {//array + in
                visible = (filter.indexOf(value) != -1);
              }
              else {//string + in
                visible = (filter == value); break;
              }
              break;
          }
          return visible;
        };
        /**
         * Compute the visibility of thisCmp according the cumulatives conditions (=and) given in fieldFilters
         * The criteriaName is used to find conditions criteria for thisCmp. 
         * The componentCompare give the comparison value to conditions criteria.
         * @param Ext.Component                           thisCmp      : the component that will be hide ou visible
         * @param Object {criteriaName : componentCompare}  fieldFilters : the available criteria.
         * @return boolean  visibility of thisCmp 
         */
        conditions = function (thisCmp, fieldFilters) {

          var visible = true;
          Ext.iterate(fieldFilters, function (criteria, forComponent) {
            if (!forComponent) return;
            if (!thisCmp.conditions) return;
            if (!thisCmp.conditions[criteria]) return;

            if (!Ext.isObject(forComponent)) {
              visible = visible && isVisible.call(thisCmp, null, thisCmp.conditions[criteria], forComponent);
              return visible;
            }
            if (forComponent instanceof Ext.data.Store) {
              visible = visible && isVisible.call(thisCmp, forComponent, thisCmp.conditions[criteria]);
              return visible;
            }
            if (forComponent.rendered) {
              var value = forComponent.getValue();
              if (value === null) {
                visible = visible && isVisible.call(thisCmp, forComponent, thisCmp.conditions[criteria], value);
              } else if (forComponent instanceof Ext.form.field.ComboBox) {
                visible = visible && isVisible.call(thisCmp, forComponent, thisCmp.conditions[criteria], forComponent.getSelection().get('codeField'));
              } else if (forComponent instanceof Ext.form.RadioGroup) {
                visible = visible && isVisible.call(thisCmp, forComponent, thisCmp.conditions[criteria], forComponent.getChecked()[0].inputValue);
              }
            }
            return visible;
          });
          return visible;
        };

      }


      if (defer) {
        this.fieldsDefered[name] = conditions;
        return;
      }
      this.fieldsFiltered[name] = conditions;
    };//fn registerEvent

    if (Ext.isArray(items)) {
      Ext.each(items, registerEvent, this)
    }
    else if (Ext.isObject(items)) {
      Ext.iterate(items, function (key, item) { registerEvent(item); }, this)
    }
    return items;
  },

  loadRecord: function (record) {
    var me = this;


    if (record instanceof Carmen.Model.Layer) {
      if (record.phantom) return;
      //force the record to adopt a fixed analyseType
      if (this.recordAnalyse) {
        var layerAnalyseType = LEX_ANALYSE_TYPE.findRecord("codeField", this.recordAnalyse);
        if (layerAnalyseType && record.get('layerAnalyseType') != layerAnalyseType.get('valueField')) {
          record.set('layerAnalyseType', layerAnalyseType.get('valueField'));
        }
      }

      var msClasses = this.down('[name=msClasses]');
      if (msClasses) {
        var originClasses = record.get('msClasses');
        var copyClasses = Ext.create(originClasses.$className, { model: originClasses.getModel() });
        Ext.each(originClasses.getData().getValues('data'), function (c) {
          var classe = Ext.create(originClasses.getModel(), Ext.apply({ convertOnSet: false }, c));
          classe.convertOnSet = true;
          copyClasses.add(classe);
        })
        msClasses.setStore(copyClasses);
      }

      var fields = record.get('fields');
      if (fields instanceof Ext.data.Store) {
        var data = fields.getData();
        if (data.filtered) {
          data = data.getSource();
        }
        this.storeFields = [];

        var fields_combobox = this.query('[storeId=fields]');
        Ext.each(fields_combobox, function (combobox) {
          var storeFields = Ext.create(fields.$className, {
            proxy: { type: 'memory' },
            model: fields.getModel(),
            data: data.getValues('data')
          });
          var toRemove = storeFields.query('fieldDuplicate', true);
          storeFields.remove(toRemove.items);
          if (combobox instanceof Ext.grid.column.Column) {
            combobox.widget.store = storeFields;
          } else {
            combobox.setStore(storeFields);
          }
          this.storeFields.push(storeFields);
        }, this);
      }

      var copyAnalyse = this.down('[name=copyAnalyse]');
      if (copyAnalyse) {
        var map = record.getMap();
        var layers = map.get('layers');
        layers = Ext.create(layers.$className, { model: layers.model, data: [].concat(layers.data.items) });
        copyAnalyse.setStore(layers);
        copyAnalyse.getStore().filterBy(function (aLayer) {
          if (aLayer == record) return false;
          if (aLayer.get('msGeometryType') != record.get('msGeometryType')) return false;
          var allFields = true;
          var selfFields = record.get('fields');
          if (!selfFields || !(selfFields instanceof Ext.data.Store)) return false;
          aLayer.get('fields').each(function (field) {
            var findIndex = selfFields.findBy(function (sField) {
              return (
                sField.get('fieldName') == field.get('fieldName')
                && sField.get('fieldDatatype') == field.get('fieldDatatype')
              );
            });
            allFields = allFields && findIndex != -1;
            return allFields;
          });
          return allFields;
        });

        copyAnalyse.setVisible(copyAnalyse.getStore().getCount() > 0);
        this.down('[name=copyAnalyseBtn]').setVisible(copyAnalyse.getStore().getCount() > 0);
      }
    }
    this.callParent(arguments);

  },

  updateRecord: function (layer) {
    this.callParent(arguments);
    if (layer) {
      layer.set('msClasses', this.down('[name=msClasses]').getStore());
      var transparency = this.down('[name=layerTransparency]').getValue();
      var opacity = 100 - transparency;
      layer.set("layerOpacity", opacity, { silent: true });

      var treenode = app.layerTreeStore.findRecord("layerId", layer.getId());

      if (treenode) {
        treenode.set("opacity", opacity, { silent: true });
        app.layerTreeStore.OL_MAP.getItemById(treenode.get('OL_ITEM').id).setOpacity(opacity);
      }
    }
  },

  /**
   * Configure the widget
   * @param Ext.grid.column column
   * @param Ext.Component   widget
   * @param Ext.data.Model record
   */
  onWidgetAttach: function (column, widget, record) {
    column.widgets = column.widgets || new Ext.util.Collection();
    column.widgets.add(widget);
    record.widgets = record.widgets || {};
    record.widgets[widget.name] = widget;
    if (Ext.isFunction(widget.getValue)) {
      widget.on('change', function () {
        widget.getWidgetRecord().set(widget.getWidgetColumn().dataIndex, widget.getValue(), { dirty: false, convert: false, commit: true, silent: true });
      });
    }
  },

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Return default components definitions in an named associative object to allow selection of components
   * @return {}
   */
  getDefaultComponents: function () {
    var me = this;
    var components = {
      /**
       * @grid msClasses
       */
      msClasses: me.initMsClassGrid(),
      /**
       * @field layerLegendScale
       */
      layerLegendScale: {
        xtype: 'checkbox',
        name: 'layerLegendScale',
        boxLabel: _T_LAYERSTYLE("fields.layerLegendScale"),
        flex: 6
      },
      /**
       * @field layerTransparency
       */
      layerTransparency: {
        xtype: 'sliderfield',
        flex: 6,
        name: 'layerTransparency',
        fieldLabel: _T_LAYERSTYLE('fields.layerTransparency'),
        width: 200,
        minValue: 0,
        maxValue: 100,
        listeners: {
          afterrender: function () {
            this.textComponent = this.el.createChild({
              tag: "label",
              role: "textbox",
              style: 'margin-left:5px',
              cls: "x-form-item-label-default",
              html: this.getValue() + '%'
            });
            this.inputEl.setWidth(this.el.getWidth());
            this.bodyEl.setWidth(this.el.getWidth());
            this.el.setWidth(this.el.getWidth() + 70);
          },
          change: function () {
            this.textComponent = this.textComponent || me.down('[name=layerTransparency_text]');
            if (this.textComponent)
              this.textComponent.setHtml(this.getValue() + '%');
          }
        }
      }
    };
    return components;
  },

  initComponentRaster: function () {
    var me = this;
    var components = this.getDefaultComponents();

    me.items = this.registerEvents([{
      hidden: true,
      xtype: 'combobox',
      name: 'layerAnalyseType',
      allowBlank: false,
      queryMode: 'local',
      displayField: 'displayField',
      valueField: 'valueField',
      store: Carmen.Dictionaries.ANALYSETYPES
    }, {
      xtype: 'fieldcontainer',
      layout: 'hbox',
      padding: '0 5 5 5',
      items: [
        components.layerTransparency
        , components.layerLegendScale
      ]
    }
      , components.msClasses
    ])
  },

  initComponentVector: function () {
    var me = this;

    var components = this.getDefaultComponents();
    me.defaults = {
      margins: 5
    };
    me.items = this.registerEvents([{
      xtype: 'panel',
      frame: true,
      title: _T_LAYERSTYLE('fieldsets.typeAnalyse'),
      layout: 'fit',
      padding: '5 5 5 5',
      items: [{
        xtype: 'form',
        method: 'GET',
        name: 'formConfigAnalyse',
        fieldDefaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'fieldcontainer',
          layout: 'hbox',
          padding: '0 5 5 5',
          items: [
            components.layerTransparency
            , components.layerLegendScale
          ]
        }, {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          padding: '0 5 5 5',
          minHeight: 30,
          items: [{
            xtype: 'combobox',
            flex: 5,
            name: 'layerAnalyseType',
            allowBlank: false,
            updateAllowBlank: true,
            fieldLabel: _T_LAYERSTYLE('fields.layerAnalyseType'),
            queryMode: 'local',
            displayField: 'displayField',
            valueField: 'valueField',
            store: Carmen.Dictionaries.ANALYSETYPES,
            listeners: {
              change: function (analyse) {
                var analyse = analyse.getSelection();
                if (!analyse) {
                  return;
                }
                analyse = analyse.get('codeField');
                config = LEX_ANALYSE_TYPE.CONFIG[analyse];

                var cmp = me.down('[name=msClassCount]');
                cmp && cmp.setValue(Math.max(cmp.getValue(), config.MINCLASSES || 0));

                if (me.storeFields instanceof Array) {
                  Ext.each(me.storeFields, function (storeFields) {
                    if (storeFields instanceof Ext.data.Store) {
                      storeFields.clearFilter();
                      //if ( !storeFields.data._source.byInternalId ) storeFields.data._source.byInternalId = storeFields.data.byInternalId         
                      if (config && config.FIELDTYPE) {
                        var classitem = me.down('[submitName=classitem][@hidden=false]');
                        if (storeFields.getData().getSource() && !storeFields.getData().getSource().byInternalId) {
                          storeFields.getData().getSource().byInternalId = storeFields.getData().byInternalId;
                        }
                        storeFields.filterBy(function (field) {
                          var datatype = field.get('fieldDatatype');

                          var keep = (Ext.isNumber(datatype)
                            ? datatype == FIELD_DATATYPES.findRecord('codeField', config.FIELDTYPE).get('valueField')
                            : datatype == config.FIELDTYPE);

                          if (!keep && classitem && classitem.getSelection() && classitem.getSelection().get('fieldId') == field.get('fieldId')) {
                            classitem.setSelection(null);
                            classitem.setValue(null);
                            classitem.setRawValue("");
                            classitem.originalValue = null;
                          }
                          return keep;
                        });
                      }
                    }
                  });
                }
              }
            }
          }].concat(en_developpement.copyAnalyse() ? [{ html: '', flex: 1 }, {
            xtype: 'combobox',
            flex: 5,
            name: 'copyAnalyse',
            hidden: en_developpement.copyAnalyse(),
            fieldLabel: _T_LAYERSTYLE('fields.copyAnalyse'),
            labelWidth: 50,
            queryMode: 'local',
            displayField: '_displayname_',
            valueField: 'layerId',
            conditions: {
              analyses: 'non_raster'
            },
            store: new Ext.data.JsonStore({ model: Carmen.Model.Layer })
          }, {
            text: _T_LAYERSTYLE('buttons.copyAnalyse'),
            flex: 1,
            conditions: {
              analyses: 'non_raster'
            },
            padding: '5 5 5 5',
            name: 'copyAnalyseBtn',
            xtype: 'button',
            handler: function () {
              var record = me.getRecord();
              var copyAnalyse = me.down('[name=copyAnalyse]');
              if (!copyAnalyse) return;
              if (!copyAnalyse.getValue()) return;
              var layerId = copyAnalyse.getValue();
              var layer = record.getMap().get('layers').findRecord('layerId', layerId);
              if (!layer) return;
              var copyClasses = layer.get('msClasses');
              if (!copyClasses.getCount()) return;
              var selfClasses = Ext.create(copyClasses.$className, { model: copyClasses.getModel() })
              copyClasses.each(function (aClasse) {
                var copy = aClasse.clone();
                copy.set(copy.idProperty, null);
                copy.set('layer', record);
                selfClasses.add(copy.data);
              });
              selfClasses.commitChanges();
              Ext.each(Carmen.Model.LayerAnalyse, function (field) {
                var form = me.down('[name=formConfigAnalyse]');
                if (!form) return;
                var cmp = form.down('[name=' + field + ']');
                cmp && cmp.setValue(layer.get(field));
              });
              me.down('[name=msClasses]').setStore(selfClasses);

              me.down('[name=msClasses]').getView().refresh();
              me.down('[name=msClasses]').updateLayout();
            }
          }] : [])
        }].concat(this.initMsClassConfiguration())
        , buttonAlign: 'left'
        , buttons: [{
          text: _T_LAYERSTYLE('buttons.applyAnalyze'),
          padding: '5 5 5 5',
          name: 'applyAnalyze',
          xtype: 'button',
          handler: function () { me.applyAnalyze(); }
        }]
      }]
    }, {
      xtype: 'panel',
      frame: true,
      name: 'msClassesFieldset',
      margins: 5,
      layout: 'anchor',
      title: _T_LAYERSTYLE("fieldsets.msClasses"),
      items:
        [
          //line 3 : list of msClasses
          components.msClasses

          //line 4 : fieldset of symbology properties for the msClass selected
          , this.initSymbology()
        ]

    }]);


    var addInitialEvent = function (component) {
      if (component.listeners) {
        me.initialEvents[component.name] = [];
        for (var eventName in component.listeners) {
          me.initialEvents[component.name].push(component.listeners[eventName]);
        }
      }
    };
    Ext.each(me.items, function (name, component) {
      addInitialEvent(component);
      if (component.items) {
        Ext.each(component.items, addInitialEvent);
      }
    });

  },

  /**
   * Construct the msClasses grid with variable visibility according to selected analyse type 
   * @return array of components definitions
   */
  initMsClassGrid: function () {
    var me = this;
    var editClasse = function (msClass) {
      var form = me.down('[name=formSymbology]');
      if (!form) return;
      form.loadRecord(msClass);

      var grid = me.down('[name=msClasses]');
      if (!grid || !msClass) return;
      var view = grid.getView();
      var row = view.getRow(msClass);
      Ext.get(row).scrollIntoView(view.getEl(), null, false);
    };
    var onDataChanged = function (view) {
      var view = this;
      Ext.each(Ext.ComponentQuery.query('[name=deleteClass]'), function (el) {
        el.setDisabled(view.store.getCount() <= 1)
      });
    }
    return {
      xtype: 'gridpanel',
      name: 'msClasses',
      //        autoScroll : true,

      margins: 5,
      padding: 5,
      margin: '10 0 0 0',
      width: '100%',
      columnLines: true,
      //        shrinkWrapDock : true,
      //        frame : true,
      maxHeight: 300,
      scrollable: 'y',

      /**
       * Assure
       */
      setBounds: function (bounds) {
        var store = this.store;
        var count = store.getCount();
        var min = (bounds.MIN === null ? count : (bounds.MIN || 1));
        var max = (bounds.MAX === null ? count : (bounds.MAX || 1));
        if (count < min) {

        }
        if (count > max) {
          store.removeAt(max, count - max);
        }
      },

      viewConfig: {
        stripeRows: true,
        listeners: {
          drop: function (node, data, old_record) {
            data.view.store.id = data.view.store.id || "drop";
            data.view.refresh(true);
            data.view.updateLayout();
          },
          refresh: onDataChanged,
          itemadd: onDataChanged,
          itemremove: onDataChanged,
          itemupdate: onDataChanged
        },
        plugins: [{
          ptype: 'gridviewdragdrop',
          dragText: _T_LAYERSTYLE('extra.reorderClass'),
          /**
           * override onViewRender to get access to the dragZone object
           * then override getDragData of dragZone object to disabled drag under input elements
           */
          dragZone: {
            getDragData: function (e) {
              // do not enable drag under input elements
              if (Ext.get(e.getTarget()).is('input')) return;
              if (Ext.get(e.getTarget()).is('.x-btn')) return;
              return Ext.view.DragZone.prototype.getDragData.apply(this, arguments);
            }
          }
        }]
      },
      columnLines: true,
      dockedItems: [{
        xtype: 'toolbar',
        name: 'msClassTopToolbar',
        dock: 'top',
        items: [{
          name: 'addClass',
          icon: '/images/add.png',
          xtype: 'button',
          text: _T_LAYERSTYLE("buttons.addClass"),
          conditions: {
            fn: function (thisCmp, fieldFilters) {
              var count = fieldFilters.msClasses.getCount();
              var analyse = me.down('[name=layerAnalyseType]');
              if (!analyse) return false;
              if (!analyse.getValue()) return false;
              analyse = analyse.getSelection().get('codeField');
              if (!analyse) return false;
              // si aucune limite : toujours visible
              if (LEX_ANALYSE_TYPE.CONFIG[analyse].NBCLASSES.MAX === null) return true;
              // sinon visible si ne dépasse pas la limite pour ce type d'analyse
              return count < LEX_ANALYSE_TYPE.CONFIG[analyse].NBCLASSES.MAX;
            }
            , defer: true
          },
          handler: function () {
            var record = me.getRecord();
            if (!record) return;
            var msClasses = me.down('[name=msClasses]').getStore();
            if (!msClasses || !(msClasses instanceof Ext.data.Store)) return;

            msClasses.add({ msClassIndex: msClasses.getCount(), msClassName: record.get('layerTitle'), layer: record });
            var view = me.down('[name=msClasses]').getView();
            var count = msClasses.getCount();
            view.focusRow(count - 1);
            //              
            //              var cell = new Ext.grid.CellContext(view);
            //              var count = msClasses.getCount();
            //              var colAction = 1;
            //              if ( msClasses.getCount()>1 ){
            //                for (var i=0; i<msClasses.getCount(); i++){
            //                  cell.setPosition(i, colAction).getCell().down('[name=]');
            //                  view.removeRowCls(i, "x-item-disabled");
            //                }
            //              } else if ( count>0 ){
            //                view.getRow(i).addRowCls(i, "x-item-disabled");
            //              }
          }
        }],
        conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.UNIQVALUE, LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL], defer: true }
      }],

      columns: {
        defaults: {
          hideable: false
          , menuDisabled: true
          , stopSelection: true
          , align: 'center'
        },
        items: [
          { width: 40, name: "rownumberer", renderer: function (v, m, r, rowIndex) { r.set("msClassIndex", rowIndex); return rowIndex + 1; }, conditions: { analyses: 'non_raster', defer: true } },
          {
            conditions: { analyses: 'non_raster', defer: true },
            name: 'actions',
            xtype: 'widgetcolumn',
            width: 75,
            onWidgetAttach: function (column, widget, record) {
              widget.items.each(function (cmp) { cmp.record = record; }); widget.updateLayout();
            },
            widget: {
              bodyStyle: 'background-color:transparent !important;',
              name: 'actionsPanel',
              border: false,
              margin: '-2 0 0 0',
              xtype: 'fieldcontainer',
              layout: 'hbox',
              defaults: {
                margin: '0 4 0 0',
                xtype: 'button'
              },
              items: [{
                icon: '/images/edit.png',  // Use a URL in the icon config
                name: 'editClass',
                tooltip: _T_LAYERSTYLE("buttons.editClass"),
                handler: function () {
                  var msClass = this.record;
                  me.down('[name=msClasses]').getView().focusRow(msClass);
                  editClasse(msClass);
                }
              }, {
                icon: '/images/delete.png',
                name: 'deleteClass',
                tooltip: _T_LAYERSTYLE("buttons.deleteClass.text"),
                handler: function (grid, rowIndex, colIndex) {
                  var msClass = this.record;
                  if (!msClass) return;
                  var store = msClass.store;
                  var gridView = me.down('[name=msClasses]').getView();
                  Ext.Msg.confirm(
                    new Ext.XTemplate(_T_LAYERSTYLE("buttons.deleteClass.confirm.title")).apply(msClass.getData()),
                    new Ext.XTemplate(_T_LAYERSTYLE("buttons.deleteClass.confirm.message")).apply(msClass.getData()),
                    function (btn) {
                      if (btn == "yes") {
                        if (msClass.isEditing) {
                          msClass.isEditing = false;
                          editClasse(null);
                        }
                        store.remove(msClass);
                        if (rowIndex > 0) {
                          gridView.focusRow(rowIndex - 1);
                        }
                      }
                    }
                  ).icon = Ext.Msg.WARNING;
                }
              }]
            }
          }, {
            conditions: { analyses: 'non_raster', defer: true },
            xtype: 'widgetcolumn',
            name: 'msClassLegendIcon',
            dataIndex: 'msClassLegendIcon',
            width: 80,
            stopSelection: true,
            onWidgetAttach: function (column, widget, record) {
              var store = Ext.create(widget.store.$className, {});
              widget.setStore(store);
              store.loadRecords([record], false);
            },
            widget: {
              xtype: 'symbolpreview',
              defaultBindProperty: 'phantomproperty',
              setPhantomproperty: function (phantomproperty) {
              },
              store: Ext.create('Carmen.Store.MsClass', {}),
              margin: '-2 0 0 0',
              listeners: {
                itemclick: function (view) {
                  var msClass = view.getWidgetRecord();
                  editClasse(msClass)
                }
              }
            }
          }, {
            flex: 1,
            xtype: 'widgetcolumn',
            dataIndex: 'msClassName',
            name: 'msClassName',
            header: _T_LAYERSTYLE("fields.msClassName"),
            onWidgetAttach: me.onWidgetAttach,
            minWidth: 250,
            widget: {
              defaultBindProperty: 'value',
              xtype: 'textfield',
              padding: '4 0 0 0',
              name: 'msClassName'
            }
          }, {
            conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL], defer: true },
            flex: 1,
            xtype: 'widgetcolumn',
            dataIndex: 'msClassLowerBound',
            name: 'msClassLowerBound',
            header: _T_LAYERSTYLE("fields.msClassLowerBound"),
            onWidgetAttach: me.onWidgetAttach,
            widget: {
              defaultBindProperty: 'value',
              xtype: 'textfield',
              padding: '5 0 0 0',
              name: 'msClassLowerBound'
            }
          }, {
            conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL], defer: true },
            flex: 1,
            xtype: 'widgetcolumn',
            dataIndex: 'msClassUpperBound',
            name: 'msClassUpperBound',
            header: _T_LAYERSTYLE("fields.msClassUpperBound"),
            onWidgetAttach: me.onWidgetAttach,
            widget: {
              defaultBindProperty: 'value',
              xtype: 'textfield',
              padding: '5 0 0 0',
              name: 'msClassUpperBound'
            }
          }, {
            conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.UNIQVALUE], defer: true },
            flex: 1,
            xtype: 'widgetcolumn',
            dataIndex: 'msClassValue',
            name: 'msClassValue',
            header: _T_LAYERSTYLE("fields.msClassValue"),
            onWidgetAttach: me.onWidgetAttach,
            storeId: "distribution",
            widget: {
              defaultBindProperty: 'value',
              store: new Ext.data.JsonStore({
                xtype: 'json',
                autoLoad: false,
                fields: ["field", "count", {
                  name: "displayField", calculate: function (data) { return data.field + ' (' + data.count + ')'; }
                }, {
                    name: "valueField", calculate: function (data) { return escapeExpression(data.field); }
                  }]
              }),
              queryMode: 'local',
              displayField: "displayField",
              valueField: "valueField",
              xtype: 'combo',
              padding: '5 0 0 0',
              name: 'msClassValue'
            }
          }, {
            conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART], defer: true },
            flex: 1,
            xtype: 'widgetcolumn',
            dataIndex: 'msStyleSizeField',
            name: 'msStyleSizeField',
            conditionName: 'msStyleSizeByField',
            header: _T_LAYERSTYLE("fields.field_list"),
            storeId: 'fields',
            readOnly: true,
            updateAllowBlank: true,
            onWidgetAttach: me.onWidgetAttach,
            widget: {
              readOnly: true,
              defaultBindProperty: 'value',
              updateAllowBlank: true,
              name: 'msStyleSizeField', conditionName: 'msStyleSizeByField',
              padding: '5 0 0 0',
              xtype: 'combo',
              queryMode: 'local',
              displayField: 'fieldName',
              valueField: 'fieldDataName',
              emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyText'),
              listConfig: {
                emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyList')
              },
              forceSelection: true
            }
          }, {
            flex: 0.5,
            xtype: 'widgetcolumn',
            dataIndex: 'msClassVisible',
            header: _T_LAYERSTYLE("fields.msClassVisible"),
            onWidgetAttach: me.onWidgetAttach,
            widget: {
              defaultBindProperty: 'value',
              xtype: 'checkboxfield',
              name: 'msClassVisible',
              listeners: {
                change: function (widget, checked, old_checked) {
                  var record = widget.getWidgetRecord();
                  if (!record) return;
                  var msClassLegend = record.widgets["msClassLegend"];
                  if (msClassLegend) {
                    if (!checked) {
                      msClassLegend.setValue(false);
                    } else {
                      msClassLegend.fireEvent('change', msClassLegend);
                    }
                    if (msClassLegend.rendered) {
                      msClassLegend.setDisabled(!checked);
                    } else {
                      msClassLegend.disabled = (!checked);
                    }
                  }
                }
              }
            }
          }, {
            flex: 0.5,
            xtype: 'widgetcolumn',
            dataIndex: 'msClassLegend',
            header: _T_LAYERSTYLE("fields.msClassLegend"),
            onWidgetAttach: me.onWidgetAttach,
            widget: {
              defaultBindProperty: 'value',
              xtype: 'checkboxfield',
              name: 'msClassLegend',
              listeners: {
                change: function (widget) {
                  var checked = widget.getValue();
                  var disabled = widget.isDisabled();
                  var record = widget.getWidgetRecord();
                  if (!record) return;
                  var msClassName = record.widgets["msClassName"];
                  if (msClassName && !disabled) {
                    msClassName.emptyText = (checked ? "Renseigner le titre de la classe" : 'Classe non visible dans la légende');
                    msClassName.setValue((checked ? msClassName.getValue() || record.get('layer').get('layerTitle') : ''));
                    msClassName.setReadOnly(!checked);
                  }
                }
              }
            }
          }]
      }
    }
  },

  /**
   * Construct the msClasses configuration components with variable visibility according to selected analyse type 
   * @return array of components definitions
   */
  initMsClassConfiguration: function () {
    var me = this;
    Ext.create('Ext.data.Store', {
      storeId: "SYMBOL_LIBRARY",
      autoLoad: false,
      idProperty: 'name',
      fields: ['name', 'index', 'image', 'imagepath', 'imageurl'],
      proxy: {
        url: Routing.generate('carmen_ws_helpers_symbolslibrary', {
          geometrytype: 'POLYGON',
          symbolset: encodeURIComponent(Carmen.user.map.get('symbolset'))
        }),
        type: 'ajax',
        limitParam: '',
        pageParam: '',
        startParam: '',
        reader: {
          type: 'json',
          idProperty: 'name',
          rootProperty: 'data'
        }
      },
      listeners: {
        beforeload: function () {
          return [LEX_GEOMETRY_TYPE.TYPES.POLYGON].indexOf(me.fieldFilters.geometrytypes.getValue()) != -1;
        }
      }
    });
    var numberWidth = 100;
    return [{
      layout: 'hbox',
      anchor: '100%',
      xtype: 'fieldcontainer',
      name: 'configure_msclass_line1',
      padding: '0 5 5 5',
      fieldDefaults: {
        labelAlign: 'left',
        labelSeparator: '',
        padding: '0 10 0 0'
      },
      items: [{
        name: 'msClassItem',
        listeners: {
          show: function (field) {
            if (Ext.isNumber(field.getValue())) field.setValue(null);
          }
        },
        hidden: true,
        submitName: 'classitem',
        category: 'configuration',
        storeId: 'fields',
        hasStats: true,
        fieldLabel: _T_LAYERSTYLE('fields.msClassItem'),
        xtype: 'combo',
        allowBlank: true,
        updateAllowBlank: true,
        queryMode: 'local',
        displayField: 'fieldName',
        valueField: 'fieldDataName',
        emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyText'),
        listConfig: {
          emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyList')
        },
        conditions: {
          analyses: {
            type: 'in',
            list: [LEX_ANALYSE_TYPE.TYPES.UNIQVALUE, LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL]
          }
        }
      }, {
        name: 'msStyleSizeField',
        listeners: {
          show: function (field) {
            if (Ext.isNumber(field.getValue())) field.setValue(null);
          }
        },
        conditionName: 'msStyleSizeByProportional',
        hidden: true,
        submitName: 'classitem',
        category: 'configuration',
        hasStats: true,
        storeId: 'fields',
        fieldLabel: _T_LAYERSTYLE('fields.msClassItem'),
        xtype: 'combo',
        allowBlank: true,
        updateAllowBlank: true,
        queryMode: 'local',
        displayField: 'fieldName',
        valueField: 'fieldDataName',
        emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyText'),
        listConfig: {
          emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyList')
        },
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL]
        }
      }, me.getButtonStatistiques('statistiques', {
        analyses: [LEX_ANALYSE_TYPE.TYPES.UNIQVALUE, LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL, LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL]
      }), {
        name: 'msClassDistribution',
        labelAlign: 'right',
        submitName: 'classdistribution',
        category: 'configuration',
        fieldLabel: _T_LAYERSTYLE('fields.msClassDistribution'),
        xtype: 'combo',
        queryMode: 'local',
        displayField: 'displayField',
        valueField: 'valueField',
        value: Carmen.Dictionaries.CLASSDISTRIBUTION.DEFAULT,
        store: Carmen.Dictionaries.CLASSDISTRIBUTION,
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL]
        }
      }, {
        name: 'msClassCount',
        labelAlign: 'right',
        submitName: 'classcount',
        category: 'configuration',
        fieldLabel: _T_LAYERSTYLE('fields.msClassCount'),
        xtype: 'numberfield',
        minValue: Carmen.Defaults.NB_MIN_MS_CLASSES,
        maxValue: Carmen.Defaults.NB_MAX_MS_CLASSES,
        allowBlank: true,
        updateAllowBlank: true,
        allowDecimals: false,
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR, LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL]
        }
      }, {
        xtype: 'layer-statistics'
      }, {
        //PROPORTIONAL SYMBOLSCALEDENOM
        name: 'msStyleMaxValue',
        xtype: 'hidden',
        hideLabel: true,
        listeners: {
          change: function () {
            var scaledenom = me.down('[name=msSymbolScaleDenomPixels]');
            if (!scaledenom) return;
            //scaledenom.setValue(0);

            //if ( !me.down('[name=msStyleMaxSize]').isVisible()  ) {
            var analyse = getCodeAnalyse(me.down('[name=layerAnalyseType]').getValue());
            if ([LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL].indexOf(analyse) == -1) {
              scaledenom.setValue(0);
              return;
            }
            var maxsize = Ext.Number.from(me.down('[name=msStyleMaxSize]').getValue(), 0);
            var maxvalue = Ext.Number.from(me.down('[name=msStyleMaxValue]').getValue(), 0);

            try {
              scaledenom.setValue(maxsize / maxvalue);
            } catch (error) {
              scaledenom.setValue(0);
            }
          }
        }
      }, {
        name: 'msSymbolScaleDenomPixels',
        xtype: 'hidden',
        hideLabel: true
      }, {
        //STYLES MIN-MAX SIZE
        fieldLabel: _T_LAYERSTYLE('fields.msStyleMinSize'),
        allowBlank: true,
        updateAllowBlank: true,
        name: 'msStyleMinSize',
        labelAlign: 'right',
        category: 'configuration',
        xtype: 'numberfield',
        minValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE,
        maxValue: Carmen.Defaults.MAX_MS_SYMBOL_SIZE,
        allowDecimals: false,
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL]
        }
      }, {
        fieldLabel: _T_LAYERSTYLE('fields.msStyleMaxSize'),
        allowBlank: true,
        updateAllowBlank: true,
        name: 'msStyleMaxSize',
        labelAlign: 'right',
        category: 'configuration',
        xtype: 'numberfield',
        minValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE,
        maxValue: Carmen.Defaults.MAX_MS_SYMBOL_SIZE,
        allowDecimals: false,
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL]
        },
        listeners: {
          show: function () {
            this.fireEvent('change');
          },
          hide: function () {
            this.fireEvent('change');
          },
          change: function () {
            var scaledenom = me.down('[name=msSymbolScaleDenomPixels]');
            if (!scaledenom) return;
            //scaledenom.setValue(0);

            var analyse = getCodeAnalyse(me.down('[name=layerAnalyseType]').getValue());
            if ([LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL].indexOf(analyse) == -1) {
              scaledenom.setValue(0);
              return;
            }
            var maxsize = Ext.Number.from(me.down('[name=msStyleMaxSize]').getValue(), 0);
            var maxvalue = Ext.Number.from(me.down('[name=msStyleMaxValue]').getValue(), 0);

            try {
              scaledenom.setValue(maxsize / maxvalue);
            } catch (error) {
              scaledenom.setValue(0);
            }
          }
        }
      }]
    }, {
      layout: 'hbox',
      anchor: '100%',
      width: '100%',
      xtype: 'fieldcontainer',
      name: 'configure_msclass_line2',
      padding: '0 5 5 5',
      fieldDefaults: {
        labelAlign: 'left',
        labelSeparator: '',
        padding: '0 10 0 0'
      },
      conditions: function (thisCmp, fieldFilters) {
        var analyseCmp = fieldFilters.analyses;
        if (!(analyseCmp)) return false;
        if (!analyseCmp.rendered) return false;
        var analyse = analyseCmp.getSelection();
        if (!(analyse)) return false;
        if (analyse) analyse = analyse.get('codeField');
        if (!(analyse)) return false;

        if ([LEX_ANALYSE_TYPE.TYPES.GRADUATESYMBOL].indexOf(analyse) == -1) return false;

        var min = Carmen.Defaults.MIN_MS_SYMBOL_SIZE
          , max = Carmen.Defaults.MAX_MS_SYMBOL_SIZE
          , minGraduate = min
          , maxGraduate = max
          , symbol = "Carre";
        fieldFilters.msClasses.each(function (record, index, alls) {
          if (index == 0) {
            minGraduate = record.get('msStyleSize');
            if (!record.get('isHatch')) symbol = record.get('msStyleSymbol');
          }
          if (index == alls - 1) maxGraduate = record.get('msStyleSize');
        });

        thisCmp.down('[conditionName=msClassMinSymbolSize]').setValue(Math.min(max, Math.max(min, minGraduate)));
        thisCmp.down('[conditionName=msClassMaxSymbolSize]').setValue(Math.min(max, Math.max(min, maxGraduate)));
        thisCmp.down('[conditionName=msClassSymbolSize]').setValue(symbol);


        var geometryCmp = fieldFilters.geometrytypes;
        if (!(geometryCmp)) return false;
        if (!geometryCmp.rendered) return false;
        var geometry = geometryCmp.getSelection();
        if (!(geometry)) return false;
        if (geometry) geometry = geometry.get('codeField');
        if (!(geometry)) return false;

        if (geometry == LEX_GEOMETRY_TYPE.TYPES.LINE) {
          thisCmp.down('[conditionName=msClassMinSymbolSize]').setMinValue(1);
          thisCmp.down('[conditionName=msClassMaxSymbolSize]').setMinValue(1);
        } else {
          thisCmp.down('[conditionName=msClassMinSymbolSize]').setMinValue(Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
          thisCmp.down('[conditionName=msClassMaxSymbolSize]').setMinValue(Carmen.Defaults.MIN_MS_SYMBOL_SIZE);
        }
        return true;
      },
      fieldLabel: _T_LAYERSTYLE('fields.msClassSymbolSize'),
      items: [{
        //CLASSES MIN-MAX SIZE
        conditionName: 'msClassMinSymbolSize',
        labelWidth: 85,
        labelAlign: 'right',
        submitName: 'classminsymbolsize',
        category: 'configuration',
        fieldLabel: _T_LAYERSTYLE('fields.msClassMinSymbolSize'),
        xtype: 'numberfield',
        minValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE,
        maxValue: Carmen.Defaults.MAX_MS_SYMBOL_SIZE,
        allowBlank: true,
        updateAllowBlank: true,
        allowDecimals: false
      }, {
        conditionName: 'msClassMaxSymbolSize',
        labelAlign: 'right',
        submitName: 'classmaxsymbolsize',
        category: 'configuration',
        fieldLabel: _T_LAYERSTYLE('fields.msClassMaxSymbolSize'),
        xtype: 'numberfield',
        minValue: Carmen.Defaults.MIN_MS_SYMBOL_SIZE,
        maxValue: Carmen.Defaults.MAX_MS_SYMBOL_SIZE,
        allowBlank: true,
        updateAllowBlank: true,
        allowDecimals: false
      }, {
        fieldLabel: _T_LAYERSTYLE('fields.msClassSymbol'),
        pageSize: 0,
        triggerAction: 'all',
        xtype: 'combo',
        category: 'configuration',
        submitName: 'classsymbol',
        conditionName: 'msClassSymbolSize',
        displayField: 'name',
        valueField: 'name',
        tpl: [
          '<tpl for=".">',
          '<div class="x-boundlist-item"><img src="{image}"/>&nbsp;{name}</div>',
          '</tpl>'
        ],
        queryMode: 'local',
        queryParam: false,
        store: Ext.data.StoreManager.lookup("SYMBOL_LIBRARY")
      }]
    }, {
      layout: 'hbox',
      anchor: '100%',
      width: '100%',
      xtype: 'fieldcontainer',
      name: 'configure_msclass_line3',
      padding: '0 5 5 5',
      fieldDefaults: {
        labelAlign: 'left',
        labelSeparator: '',
        padding: '0 10 0 0'
      },
      conditions: function (thisCmp, fieldFilters) {
        var analyseCmp = fieldFilters.analyses;
        if (!(analyseCmp)) return false;
        if (!analyseCmp.rendered) return false;
        var analyse = analyseCmp.getSelection();
        if (!(analyse)) return false;
        if (analyse) analyse = analyse.get('codeField');
        if (!(analyse)) return false;

        if ([LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR].indexOf(analyse) == -1) return false;

        var minGraduate = Carmen.Defaults.MIN_MS_SYMBOL_COLOR
          , maxGraduate = Carmen.Defaults.MAX_MS_SYMBOL_COLOR;
        fieldFilters.msClasses.each(function (record, index, alls) {
          if (index == 0) {
            minGraduate = record.get('msStyleColor');
          }
          if (index == alls - 1) maxGraduate = record.get('msStyleColor');
        });
        thisCmp.down('[conditionName=msClassMinSymbolColor]').setValue(minGraduate);
        thisCmp.down('[conditionName=msClassMaxSymbolColor]').setValue(maxGraduate);
        return true;
      },
      fieldLabel: _T_LAYERSTYLE('fields.msClassSymbolColor'),
      items: [{
        //CLASSES MIN-MAX COLOR
        conditionName: 'msClassMinSymbolColor',
        labelAlign: 'right',
        submitName: 'classminsymbolcolor',
        category: 'configuration',
        fieldLabel: _T_LAYERSTYLE('fields.msClassMinSymbolColor'),
        xtype: "colorfield",
        reset: function () { },
        value: Carmen.Defaults.MIN_MS_SYMBOL_COLOR,
        allowBlank: true,
        updateAllowBlank: true,
        allowDecimals: false,
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR]
        }
      }, {
        conditionName: 'msClassMaxSymbolColor',
        labelAlign: 'right',
        submitName: 'classmaxsymbolcolor',
        category: 'configuration',
        fieldLabel: _T_LAYERSTYLE('fields.msClassMaxSymbolColor'),
        xtype: "colorfield",
        reset: function () { },
        value: Carmen.Defaults.MAX_MS_SYMBOL_COLOR,
        allowBlank: true,
        updateAllowBlank: true,
        allowDecimals: false,
        conditions: {
          analyses: [LEX_ANALYSE_TYPE.TYPES.GRADUATECOLOR]
        }
      }]
    }, {
      //PIECHART SIZE
      layout: 'anchor',
      xtype: 'fieldcontainer',
      fieldLabel: _T_LAYERSTYLE('fields.msPiechartSize'),
      padding: '0 5 5 5',
      name: 'msPiechartSize_cont',
      listeners: {
        show: function () {
          var analyse = getCodeAnalyse(me.down('[name=layerAnalyseType]').getValue());
          if ([LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1)
            this.down('[name=msProcessingChartSize]').setDisabled(false);
        }
      },
      conditions: function (thisCmp, fieldFilters) {
        var analyseCmp = fieldFilters.analyses;
        if (!(analyseCmp)) return false;
        if (!analyseCmp.rendered) return false;
        var analyse = analyseCmp.getSelection();
        if (!(analyse)) return false;
        if (analyse) analyse = analyse.get('codeField');
        if (!(analyse)) return false;
        //conditions
        switch (analyse) {
          case LEX_ANALYSE_TYPE.TYPES.PIECHART:
            thisCmp.setFieldLabel(_T_LAYERSTYLE('fields.msPiechartSize'));
            break;
          case LEX_ANALYSE_TYPE.TYPES.BARCHART:
            thisCmp.setFieldLabel(_T_LAYERSTYLE('fields.msBarchartSize'));
            break;
          default:
            return false;
        }
        return true;
      },
      fieldDefaults: {
        labelAlign: 'left',
        labelSeparator: '',
        padding: '0 10 0 0'
      },
      flex: 12,
      items: [{
        xtype: 'fieldcontainer',
        category: 'configuration',
        layout: 'hbox',
        items: [{
          xtype: 'radio',
          name: 'msProcessingChartSizeMode',
          inputValue: 'bySize',
          conditionName: 'msProcessingChartSizeMode_bySize',
          conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.PIECHART] },
          boxLabel: _T_LAYERSTYLE("fields.msProcessingChartSizeMode.fixed"),
          listeners: {
            change: function () {
              var visible = this.isVisible();
              this.allowBlank = this.checked;
              me.down('[name=msProcessingChartSize]').setDisabled(!this.checked);

              me.down('[name=msProcessingChartSizeRangeItem]').setDisabled(this.checked);
              me.down('[name=msProcessingChartSizeRangeMin]').setDisabled(this.checked);
              me.down('[name=msProcessingChartSizeRangeMax]').setDisabled(this.checked);

              me.down('[name=msProcessingChartSizeRangeItem]').setValue(null);
              me.down('[name=msProcessingChartSizeRangeMin]').setValue(null);
              me.down('[name=msProcessingChartSizeRangeMax]').setValue(null);
            },
            show: function () {
              var analyse = getCodeAnalyse(me.down('[name=layerAnalyseType]').getValue());
              if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) {
                this.setValue([LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1 || !me.down('[name=msProcessingChartSizeRangeItem]').getValue());
                me.down('[name=msProcessingChartSize]').setDisabled(!this.getValue());

              }
            },
            hide: function () {
              var analyse = getCodeAnalyse(me.down('[name=layerAnalyseType]').getValue());
              if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) == -1) {

                me.down('[name=msProcessingChartSize]').setDisabled(true);
                me.down('[name=msProcessingChartSizeRangeItem]').setDisabled(true);
                me.down('[name=msProcessingChartSizeRangeMin]').setDisabled(true);
                me.down('[name=msProcessingChartSizeRangeMax]').setDisabled(true);


                me.down('[name=msProcessingChartSize]').setValue(null);
                me.down('[name=msProcessingChartSizeRangeItem]').setDisabled(true);
                me.down('[name=msProcessingChartSizeRangeMin]').setDisabled(true);
                me.down('[name=msProcessingChartSizeRangeMax]').setDisabled(true);
                me.down('[name=msProcessingChartSizeRangeItem]').setValue(null);
                me.down('[name=msProcessingChartSizeRangeMin]').setValue(null);
                me.down('[name=msProcessingChartSizeRangeMax]').setValue(null);
              }
              if ([LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) {
                me.down('[name=msProcessingChartSize]').setDisabled(false);
                this.setValue(true);
              }
            }
          },
          flex: 1
        }, {
          xtype: 'numberfield',
          name: 'msProcessingChartSize',
          allowBlank: false,
          listeners: {
            enable: function () { this.allowBlank = false; },
            disable: function () { this.allowBlank = true; },
            show: function () {
              var analyse = getCodeAnalyse(me.down('[name=layerAnalyseType]').getValue());
              if ([LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1)
                this.setDisabled(false);
              this.allowBlank = !this.isDisabled();
            },
            hide: function () { this.allowBlank = true; }
          },
          flex: 10
        }]
      }, {
        xtype: 'fieldcontainer',
        category: 'configuration',
        layout: 'hbox',
        conditions: { analyses: [LEX_ANALYSE_TYPE.TYPES.PIECHART] },
        name: 'msProcessingChartSizeMode_Variable',
        items: [{
          xtype: 'radio',
          name: 'msProcessingChartSizeMode',
          inputValue: 'byRange',
          boxLabel: _T_LAYERSTYLE("fields.msProcessingChartSizeMode.variable"),
          listeners: {
            change: function () {
              this.allowBlank = this.checked;

              me.down('[name=msProcessingChartSize]').setDisabled(this.checked);
              me.down('[name=msProcessingChartSizeRangeItem]').setDisabled(!this.checked);
              me.down('[name=msProcessingChartSizeRangeMin]').setDisabled(!this.checked);
              me.down('[name=msProcessingChartSizeRangeMax]').setDisabled(!this.checked);

              me.down('[name=msProcessingChartSize]').setValue(null);
            }
          },
          flex: 1
        }, {
          flex: 4,
          name: 'msProcessingChartSizeRangeItem',
          storeId: 'fields',
          xtype: 'combo',
          queryMode: 'local',
          displayField: 'fieldName',
          valueField: 'fieldDataName',
          emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyText'),
          listConfig: { emptyText: _T_LAYERSTYLE('fields.comboboxFields.emptyList') },
          allowBlank: false,
          listeners: {
            enable: function () { this.allowBlank = false; },
            disable: function () { this.allowBlank = true; },
            show: function () { this.allowBlank = !this.isDisabled(); },
            hide: function () { this.allowBlank = true; }
          }
        }, {
          xtype: 'numberfield',
          name: 'msProcessingChartSizeRangeMin',
          labelAlign: 'right',
          fieldLabel: _T_LAYERSTYLE("fields.msPiechartMinSize"),
          allowBlank: false,
          listeners: {
            enable: function () { this.allowBlank = false; },
            disable: function () { this.allowBlank = true; },
            show: function () { this.allowBlank = !this.isDisabled(); },
            hide: function () { this.allowBlank = true; }
          },
          flex: 3
        }, {
          xtype: 'numberfield',
          name: 'msProcessingChartSizeRangeMax',
          labelAlign: 'right',
          fieldLabel: _T_LAYERSTYLE("fields.msPiechartMaxSize"),
          allowBlank: false,
          listeners: {
            enable: function () { this.allowBlank = false; },
            disable: function () { this.allowBlank = true; },
            show: function () { this.allowBlank = !this.isDisabled(); },
            hide: function () { this.allowBlank = true; }
          },
          flex: 3
        }]
      }]
    }, {
      //CLASS FIELD
      conditions: {
        fn: function (thisCmp, fieldFilters) {
          var msClasses = me.down('[name=msClasses]');
          var analyse = me.down('[name=layerAnalyseType]');
          if (!analyse) return false;
          if (!analyse.getValue()) return false;
          analyse = analyse.getSelection().get('codeField');
          if (!analyse) return false;
          if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) {
            var selection = [];
            thisCmp.getStore().each(function (recordField) {
              if (msClasses.getStore().find('msStyleSizeField', recordField.get('fieldDataName')) != -1) {
                selection.push(recordField);
              }
            });
            thisCmp.getSelectionModel().select(selection);
            return true;
          }
          return false;
        },
        analyses: {
          type: 'in',
          list: [LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART]
        }
      },
      xtype: 'grid',
      border: true,
      category: 'configuration',
      name: 'msClassField',
      forceFit: true,
      selModel: {
        selType: 'checkboxmodel'
      },
      viewConfig: {
        emptyText: 'Aucun champ numérique sur cette donnée',
        deferEmptyText: false
      },
      storeId: 'fields',
      columns: [{
        header: 'Champ',
        dataIndex: 'fieldName'
      }, {
        width: 40,
        align: 'center',
        header: 'Statistiques',
        xtype: 'widgetcolumn',
        dataIndex: 'fieldDataName',
        defaultBindProperty: 'value',
        widget: me.getButtonStatistiques('classStatistiques', {
          analyses: {
            type: 'in',
            list: [LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART]
          }
        }, {
          disabled: false,
          defaultBindProperty: 'value',
          setValue: function (value) { this.value = this.getWidgetRecord() }
        })
      }]
    }]
  },

  getButtonStatistiques: function (name, conditions, config) {
    config = config || {};
    var me = this;
    return Ext.apply({
      margin: '0 10 0 0',
      text: _T_LAYERSTYLE('buttons.statistiques'),
      name: name,
      xtype: 'button',
      disabled: true,
      handler: function (btn) {
        var windowStat = me.down('layer-statistics');
        if (!windowStat) return;

        var record = me.getRecord();
        if (!btn.value) {
          var classitem = me.down('[hasStats=true][@hidden=false]');
          if (!record || !classitem) return;
          classitem.allowBlank = false;
          if (!classitem.isValid()) {
            classitem.allowBlank = true;
            Carmen.Notification.msg(_T_LAYERSTYLE('statistiques.errorConfig'), _T_LAYERSTYLE('statistiques.noFieldSelected'));
            return;
          }
          classitem.allowBlank = true;
          var classitemIndex = classitem.getSelection().get('fieldRank'),
            classitemTitle = classitem.getSelection().get('fieldName'),
            classitemType = classitem.getSelection().get('fieldDatatype'),
            classitem = classitem.getValue();
        } else {
          var classitemIndex = btn.value.get('fieldRank'),
            classitemTitle = btn.value.get('fieldName'),
            classitemType = btn.value.get('fieldDatatype'),
            classitem = btn.value.get('fieldDataName');
        }

        windowStat.showStatistics(record.get('layerMsname'), classitem, classitemIndex, classitemTitle, classitemType);
      },
      conditions: conditions
    }, config);
  },
  /**
   * Construct the Symbology configuration components with variable visibility according to selected analyse type, selected geometry type, ...
   * @return array of components definitions
   */
  initSymbology: function () {
    var me = this;
    var condition_analyse_notchart = { type: 'out', list: [LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART, LEX_ANALYSE_TYPE.TYPES.RASTER] };
    var style_pattern = function (conditionName, conditions) {
      var fieldWidth = 105;
      return {
        conditions: conditions,
        conditionName: conditionName,
        minHeight: 60,
        /* Définition du contour de symbole */
        xtype: 'fieldset',
        name: "msStylePattern",
        title: _T_LAYERSTYLE("fields.msStylePattern.title"),
        listeners: {
          show: function () { Ext.each(this.query('field'), function (el) { el.setDisabled(false) }) },
          hide: function () { Ext.each(this.query('field'), function (el) { el.setDisabled(true) }) }
        },
        checkboxName: 'msStylePattern',
        checkboxToggle: true,
        fieldDefaults: {
          xtype: 'numberfield',
          flex: 3,
          margin: '0 5 5 0'
        },
        layout: {
          type: 'table',
          columns: 4
        },
        items: [{
          conditions: conditions,
          conditionName: (conditionName ? 'msStylePatternVisible1' + conditionName : null),
          width: fieldWidth,
          labelWidth: 30,
          xtype: 'numberfield',
          name: 'msStylePatternVisible1',
          fieldLabel: _T_LAYERSTYLE("fields.msStylePattern.Visible1")
        }, {
          conditions: conditions,
          conditionName: (conditionName ? 'msStylePatternHidden1' + conditionName : null),
          width: fieldWidth,
          labelWidth: 30,
          xtype: 'numberfield',
          name: 'msStylePatternHidden1',
          fieldLabel: _T_LAYERSTYLE("fields.msStylePattern.Hidden1")
        }, {
          conditions: conditions,
          conditionName: (conditionName ? 'msStylePatternVisible2' + conditionName : null),
          width: fieldWidth,
          labelWidth: 30,
          xtype: 'numberfield',
          name: 'msStylePatternVisible2',
          fieldLabel: _T_LAYERSTYLE("fields.msStylePattern.Visible2")
        }, {
          conditions: conditions,
          conditionName: (conditionName ? 'msStylePatternHidden2' + conditionName : null),
          width: fieldWidth,
          labelWidth: 30,
          xtype: 'numberfield',
          name: 'msStylePatternHidden2',
          fieldLabel: _T_LAYERSTYLE("fields.msStylePattern.Hidden2")
        }]
      };
    };

    /**
     * Construct the left column with style fields for a class
     * @return array
     */
    var style_fields = function () {
      return [
        {
          xtype: "radiogroup"
          , name: "msStyleTypeSymbolGroup"
          , conditions: { analyses: condition_analyse_notchart, geometrytypes: [LEX_GEOMETRY_TYPE.TYPES.POLYGON] }
          , items: [{
            name: "msStyleTypeSymbol",
            boxLabel: _T_LAYERSTYLE("fields.msStyleTypeSymbol.isSymbol"),
            inputValue: Carmen.Dictionaries.MS_SYMBOLES.TYPES.SHOWLIBRARY,
            value: true,
            reset: function () {
              if ((me.up('window').down('[name=msStyleTypeSymbol][@value=true]')) !== this) {
                return;
              }
            },
            listeners: {
              change: function () {
                var msStyleSymbol = me.up('window').down('[name=msStyleSymbol]');
                if (!msStyleSymbol) return;
                if (this.getValue()) msStyleSymbol.setValue("Carre");
              }
            }
          }, {
            name: "msStyleTypeSymbol",
            boxLabel: _T_LAYERSTYLE("fields.msStyleTypeSymbol.isHatch"),
            inputValue: Carmen.Dictionaries.MS_SYMBOLES.TYPES.HATCHSYMBOL
            , reset: function () {
              if ((me.up('window').down('[name=msStyleTypeSymbol][@value=true]')) !== this) {
                return;
              }
            },
            listeners: {
              change: function () {
                var msStyleSymbol = me.up('window').down('[name=msStyleSymbol]');
                if (!msStyleSymbol) return;
                if (this.getValue()) msStyleSymbol.setValue(Carmen.Dictionaries.MS_SYMBOLES.TYPES.HATCHSYMBOL);
              }
            }
          }]
        },
        { name: "msStyleSymbol", xtype: "hiddenfield", conditions: { analyses: condition_analyse_notchart, geometrytypes: [LEX_GEOMETRY_TYPE.TYPES.POINT, LEX_GEOMETRY_TYPE.TYPES.POLYGON] } },
        { name: "msClassLegendIcon", xtype: "hiddenfield", conditions: { analyses: condition_analyse_notchart, geometrytypes: [LEX_GEOMETRY_TYPE.TYPES.POINT, LEX_GEOMETRY_TYPE.TYPES.POLYGON] } },


        { name: "msStyleColor", fieldLabel: _T_LAYERSTYLE("fields.msStyleColor"), xtype: "colorfield", conditions: { analyses: 'non_raster' }, reset: function () { } },
        {
          xtype: 'fieldcontainer',
          name: "msStyleSize_container",
          layout: 'hbox',
          fieldDefaults: { padding: '0 5 0 0' },
          items: [
            { name: "msStyleSize", xtype: "numberfield" },
            { name: "msStyleSizeField", xtype: "hiddenfield" },
            { value: _T_LAYERSTYLE("fields.sizeUnit"), xtype: "displayfield" }
          ],
          conditions: function (thisCmp, fieldFilters) {
            var analyseCmp = fieldFilters.analyses,
              geometryCmp = fieldFilters.geometrytypes;
            if (!(analyseCmp && geometryCmp)) return false;
            if (!analyseCmp.rendered) return false;
            var analyse = analyseCmp.getSelection(),
              geometry = geometryCmp.getSelection();
            if (!(analyse && geometry)) return false;
            if (analyse) analyse = analyse.get('codeField');
            if (geometry) geometry = geometry.get('codeField');
            if (!(analyse && geometry)) return false;

            if (geometry == LEX_GEOMETRY_TYPE.TYPES.POINT) {
              thisCmp.setFieldLabel(_T_LAYERSTYLE("fields.msStyleWidth.point"));
              return [LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL, LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART, LEX_ANALYSE_TYPE.TYPES.RASTER].indexOf(analyse) == -1;
            }
            if (geometry == LEX_GEOMETRY_TYPE.TYPES.POLYGON) {
              thisCmp.setFieldLabel(_T_LAYERSTYLE("fields.msStyleWidth.polygon"));
              return [LEX_ANALYSE_TYPE.TYPES.PROPORTIONAL, LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART, LEX_ANALYSE_TYPE.TYPES.RASTER].indexOf(analyse) == -1;
            }
            if (geometry == LEX_GEOMETRY_TYPE.TYPES.LINE) {
              thisCmp.setFieldLabel(_T_LAYERSTYLE("fields.msStyleWidth.line"));
              return [LEX_ANALYSE_TYPE.TYPES.RASTER].indexOf(analyse) == -1;
            }
            return false;
          }
        },
        style_pattern('line_pattern', { analyses: condition_analyse_notchart, geometrytypes: [LEX_GEOMETRY_TYPE.TYPES.LINE] }),
        {
          /* Définition du contour de symbole */
          xtype: 'fieldset',
          name: "msStyleOutline",
          title: _T_LAYERSTYLE("fields.msStyleOutline"),
          checkboxName: 'msStyleOutline',
          checkboxToggle: true,
          listeners: {
            afterrender: function (fieldset) {
              var checkbox = fieldset.checkboxCmp;
              if (!checkbox) return;
              checkbox.on('change', function () {
                var checked = this.getValue();
                Ext.each(fieldset.query('field:not(displayfield):not(fieldcontainer)'), function (item) {
                  if (item.setValue && !checked)
                    item.setValue(null);
                });
              })
            }
          },
          conditions: { analyses: condition_analyse_notchart, geometrytypes: [LEX_GEOMETRY_TYPE.TYPES.POLYGON] },
          items: [
            {
              name: "msStyleOutlineColor", fieldLabel: _T_LAYERSTYLE("fields.msStyleOutlineColor"), xtype: "colorfield"
              , conditions: { analyses: condition_analyse_notchart, geometrytypes: 'all' }, reset: function () { }
              , setValue: function (value) {
                value = value || '#000000';
                Ext.ux.colorpick.Field.prototype.setValue.call(this, value);
              }
            },
            {
              xtype: 'fieldcontainer',
              layout: 'hbox',
              name: "msStyleOutlineWidth_container",
              fieldDefaults: { padding: '0 5 0 0' },
              fieldLabel: _T_LAYERSTYLE("fields.msStyleOutlineWidth"),
              items: [
                { name: "msStyleOutlineWidth", xtype: "numberfield", reset: function () { } },
                { value: _T_LAYERSTYLE("fields.sizeUnit"), xtype: "displayfield" }
              ]
            }
          ]
        }
      ];
    };

    /**
     * Construct the symbol library used in the right column for a class
     * @return object
     */
    var symbol_library = function () {
      return me.getSymbolLibrary('symbol_library');
    };

    /**
     * Construct the symbol preview used in the right column for a class
     * @return object
     */
    var symbol_preview = function () {
      return {
        xtype: 'fieldset',
        title: _T_LAYERSTYLE("fields.symbol_preview.title"),
        layout: 'column',
        items: [{
          columnWidth: 0.5,
          margin: '0 10 0 10',
          border: true,
          xtype: 'symbolpreview',
          name: 'symbol_preview',
          conditions: { analyses: 'non_raster' },
          store: Ext.data.StoreManager.lookup("SYMBOL_PREVIEW") || Ext.create('Carmen.Store.MsClass', {
            storeId: "SYMBOL_PREVIEW"
          })
        }, {
          columnWidth: 0.5,
          margin: '0 10 0 10',
          xtype: 'button',
          text: _T_LAYERSTYLE("fields.symbol_preview.button.text"),
          handler: function () {
            var form = me.down('[name=formSymbology]');
            if (!form) return;
            form.generateSymbol(this.up('fieldset'));
          }
        }]
      };
    };

    var hachure_config = function () {
      var conditions = { analyses: condition_analyse_notchart, geometrytypes: [LEX_GEOMETRY_TYPE.TYPES.POLYGON], symboltype: [Carmen.Dictionaries.MS_SYMBOLES.TYPES.HATCHSYMBOL] };
      return {
        xtype: 'fieldset',
        title: _T_LAYERSTYLE('fieldsets.hatch'),
        name: "msStyleHatch",
        conditions: conditions,
        fieldDefaults: { xtype: 'numberfield', allowDecimals: true },
        items: [{
          name: 'msStyleHatchWidth',
          fieldLabel: _T_LAYERSTYLE('fields.msStyleHatchWidth'), xtype: 'numberfield', allowDecimals: true
        }, {
          name: 'msStyleHatchAngle',
          fieldLabel: _T_LAYERSTYLE('fields.msStyleHatchAngle'), xtype: 'numberfield', allowDecimals: true
        }, {
          name: 'msStyleHatchSize',
          fieldLabel: _T_LAYERSTYLE('fields.msStyleHatchSize'), xtype: 'numberfield', allowDecimals: true
        }, style_pattern('hatch_pattern', conditions)]
      }
    };


    /**
     * SYMBOLOGY PANEL
     */
    return {
      xtype: 'formSymbology',
      layerForm: me,
      name: 'formSymbology',
      fieldDefaults: { padding: '0 5 0 0', labelSeparator: '' },
      conditions: { analyses: 'non_raster' },
      items: [{
        xtype: 'fieldset',
        title: _T_LAYERSTYLE('fieldsets.symbology'),
        defaults: { padding: '0 5 0 5' },
        layout: 'column',
        name: 'symbology',
        height: '100%',
        items: [{
          columnWidth: .5,
          //column left buttons
          buttonAlign: 'left',
          buttons: [{
            text: _T_LAYERSTYLE('buttons.applySymbology'),
            disabled: true,
            name: 'buttons_symbology',
            handler: function () {
              var form = me.down('[name=formSymbology]');
              var grid = me.down('[name=msClasses]');
              if (!form) return;
              var symbol_library = me.down('[name=symbol_library]');
              if (symbol_library && !symbol_library.isValid()) return;
              var record = form.initialRecord;
              if (record) {
                form.generateSymbol(Ext.fly(grid.getView().getRow(record)), record);
              }
              form.loadRecord(null);
            }
          }, {
            text: _T_LAYERSTYLE('buttons.cancelSymbology'),
            disabled: true,
            name: 'buttons_symbology',
            handler: function () { me.down('[name=formSymbology]').loadRecord(null); }
          }],
          //column left items
          items: style_fields()
        }, {
          columnWidth: .5,

          //column right items
          items: [symbol_preview(), symbol_library(), hachure_config()]
        }]
      }]
    };
  },
  reloadSymbolLibraryWithParameters: function (thisCmp) {
    var me = this;
    if (!thisCmp) return;
    var fieldFilters = me.fieldFilters;
    var analyseCmp = fieldFilters.analyses,
      geometryCmp = fieldFilters.geometrytypes,
      symbolGroup = fieldFilters.symboltype;
    if (!(analyseCmp && geometryCmp && symbolGroup)) return false;
    if (!analyseCmp.rendered) return false;
    var analyse = analyseCmp.getSelection(),
      geometry = geometryCmp.getSelection(),
      symboltype = symbolGroup.getValue().msStyleTypeSymbol;
    if (!(analyse && geometry)) return false;
    if (analyse) analyse = analyse.get('codeField');
    if (geometry) geometry = geometry.get('codeField');
    if (!(analyse && geometry)) return false;
    if (!(analyse && geometry && symboltype)) return false;

    //conditions
    if ([LEX_ANALYSE_TYPE.TYPES.RASTER, LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) return false;
    if ([LEX_GEOMETRY_TYPE.TYPES.POINT, LEX_GEOMETRY_TYPE.TYPES.POLYGON].indexOf(geometry) == -1) return false;
    if (symboltype != Carmen.Dictionaries.MS_SYMBOLES.TYPES.SHOWLIBRARY) return false;

    //additional comportement
    var libraryStore = Ext.data.StoreManager.lookup("SYMBOL_LIBRARY");
    if (libraryStore) {

      var url = Routing.generate('carmen_ws_helpers_symbolslibrary', {
        geometrytype: geometry,
        symbolset: encodeURIComponent(Carmen.user.map.get('symbolset'))
      });
      if (!libraryStore.lastOptions || libraryStore.lastOptions.url != url) {
        libraryStore.lastOptions = { url: url };
        thisCmp && thisCmp.mask(_T_LAYERSTYLE("fields.symbol_library.loading"));
        Ext.Ajax.request({
          method: 'GET',
          url: url,
          success: function (response) {
            thisCmp && thisCmp.unmask();
            response = Ext.decode(response.responseText);
            libraryStore.loadData(response.data, false);
            libraryStore.lastOptions.response = response;
            Ext.get("library_name").update(response.symbolset);
          },
          failure: function (response) {
            if (Carmen.notAuthenticatedException(response)) return;

            thisCmp && thisCmp.unmask();
            response = Ext.decode(response.responseText);
            Ext.Msg.alert(_T_LAYERSTYLE("fields.symbol_library.errorMsg"), response.data.results.description);
          }
        });
      }
      else if (libraryStore.lastOptions && libraryStore.lastOptions.response) {
        Ext.get("library_name").update(libraryStore.lastOptions.response.symbolset);
      }
    }

    return true;
  },

  getSymbolLibrary: function (fieldName) {
    var me = this;
    var reloadWithParameters = function () {
      var thisCmp = me.down('[name=' + fieldName + ']') || me.down('[old_name=' + fieldName + ']');
      return me.reloadSymbolLibraryWithParameters(thisCmp);
    };
    var checkSymbol = function (library_store, symbolName) {
      var index = library_store.find('name', symbolName);
      var status = (index == -1 ? _T_LAYERSTYLE("fields.symbol_library.unknownSymbol") : "");
      me.down('[name=invalid_symbol]') && me.down('[name=invalid_symbol]').setVisible(index == -1);
      me.down('[name=msStyleSymbol]') && me.down('[name=msStyleSymbol]').setValue(symbolName);
      Ext.suspendLayouts();
      me.down('[name=' + fieldName + '_selected]').setTitle(new Ext.XTemplate(_T_LAYERSTYLE("fields.symbol_library.selectedTitle")).apply({ name: symbolName, status: status }));
      Ext.resumeLayouts(false);
    };

    /** delete sym files by id */
    function deleteSymById(symId) {
      Ext.Ajax.request({
        method: 'post',
        url: Routing.generate('carmen_ws_filebrowser_deletesymbyid') + "/" + symId
      })
    }

    /** concat sym files */
    function concatSym(fullPathName, wind, symId) {
      if (!Carmen.user.map) return;
      const symbolset = './etc/' + fullPathName;

      console.log({
        mapfile: Carmen.user.map.get('real_mapfile'),
        mapname: Carmen.user.map.get('mapFile').replace('/', "_"),
        from_symbolset: (Carmen.user.map.get('symbolset')),
        with_symbolset: (symbolset)
      });

      Ext.Ajax.request({
        url: Routing.generate('carmen_ws_helpers_concat_symbolset', {
          mapfile: Carmen.user.map.get('real_mapfile'),
          mapname: Carmen.user.map.get('mapFile').replace('/', "_"),
          from_symbolset: encodeURIComponent(Carmen.user.map.get('symbolset')),
          with_symbolset: encodeURIComponent(symbolset)
        })
        , success: (response) => {
          wind.close();
          response = Ext.decode(response.responseText);
          Carmen.user.map.set('symbolset', response.symbolset);

          var libraryStore = Ext.data.StoreManager.lookup("SYMBOL_LIBRARY");
          if (libraryStore) {
            libraryStore.lastOptions = null;
          }
          
          deleteSymById(symId);
          reloadWithParameters();
        }
        , failure: (response) => {
          wind.close();
          if (Carmen.notAuthenticatedException(response)) return;
          response = Ext.decode(response.responseText);
          Ext.Msg.alert(_T_LAYERSTYLE("fields.symbol_library.errorMsg"), response.data.results.description);

          reloadWithParameters();
        }
      });
    }

    return {
      xtype: 'fieldset',
      name: fieldName,
      isValid: function () {
        var valid = true;
        if (!this.isVisible()) return valid;
        var library = this.down('[name=' + fieldName + '_view]');
        if (library) {
          var symbolName = me.down('[name=msStyleSymbol]').getValue();
          if (symbolName == "0" || symbolName == "") return valid;
          var selection = library.getStore().findRecord("name", symbolName);
          valid = !!selection;
          this.down('[name=invalid_symbol]').setVisible(!valid);
        }
        return valid;
      },
      border: true,
      title: _T_LAYERSTYLE("fields.symbol_library.title") + ' [<span id="library_name"></span>]',
      conditions: reloadWithParameters,
      items: [/*Ext.applyIf(new Ext.panel.Panel(*/{
        name: fieldName + '_selected',
        html: '',
        tools: [{
          baseCls: 'x-form-invalid-icon-default',
          name: 'invalid_symbol',
          hidden: true,
          tooltip: _T_LAYERSTYLE("fields.symbol_library.tools.invalid")
        }, {
          type: 'gear',
          tooltip: _T_LAYERSTYLE("fields.symbol_library.tools.plus_svg"),
          callback: function (thisPanel, thisTool, evt) {
            var windowClass = "Ext.ux.FileBrowserWindow";
            var window = Ext.getCmp(CMP_REFERENCE(windowClass))
              || Ext.create(windowClass, {
                id: CMP_REFERENCE(windowClass),
                selectExtension: '*.svg',
                uploadExtension: '.svg',
                dataUrl: Routing.generate('carmen_ws_filebrowser', { routing: 'noroute' }),
                defaultPath: '/Root',
                relativeRoot: '/Root/IHM',
                listeners: {
                  multiselectedfile: (wind, records, relativeRoot, defaultPath) => {
                    if(records && records instanceof Array){
                      records.forEach(record => {
                        $.ajax({
                          url:  Routing.generate('carmen_ws_filebrowser_addsvg'),
                          data: {path: record.get('name')},
                          method: 'POST'
                        }).then( (result) => {
                          const filename = result.file;
                      
                          concatSym(filename, wind, result.id);
                          
                        }, (error) =>{
                          console.log(error);
                        })
                      })

                      wind.close();
                    }

                  },
                  selectedfile: function (wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                    $.ajax({
                      url:  Routing.generate('carmen_ws_filebrowser_addsvg'),
                      data: {path: absolutePath},
                      method: 'POST'
                    }).then( (result) => {
                      const filename = result.file;
                  
                      concatSym(filename, wind, result.id);
                      
                    }, (error) =>{
                      console.log(error);
                    })
                    wind.close();
                  }
                }
              });
            window.show();

          }
        }, {
          type: 'close',
          tooltip: _T_LAYERSTYLE("fields.symbol_library.tools.delete"),
          callback:  (thisPanel, thisTool, evt) => {
           console.log(arguments);
           if(me.selectSymbol){

            $.ajax({
              url: Routing.generate('carmen_ws_filebrowser_removeicon'),
              method: 'POST',
              data:  {
                mapfile: Carmen.user.map.get('real_mapfile'),
                mapfileName: Carmen.user.map.get('mapFile'),
                name: me.selectSymbol.data.name,
                from_symbolset: Carmen.user.map.get('symbolset'),
              }
            }).then((response) => {
              console.log(response);
              const libraryStore = Ext.data.StoreManager.lookup("SYMBOL_LIBRARY");
              if (libraryStore) {
                libraryStore.lastOptions = null;
              }
              reloadWithParameters()
            },(error) => {
              console.error(error);
              const libraryStore = Ext.data.StoreManager.lookup("SYMBOL_LIBRARY");
              if (libraryStore) {
                libraryStore.lastOptions = null;
              }
              reloadWithParameters()
            })
         
            console.log( me.selectSymbol)
           }
          }
        }, {
          type: 'refresh',
          tooltip: _T_LAYERSTYLE("fields.symbol_library.tools.refresh"),
          callback: function (thisPanel, thisTool, evt) {
            var libraryStore = Ext.data.StoreManager.lookup("SYMBOL_LIBRARY");
            if (libraryStore) {
              libraryStore.lastOptions = null;
            }
            reloadWithParameters()
          }
        }, {
          type: 'plus',
          tooltip: _T_LAYERSTYLE("fields.symbol_library.tools.plus"),
          hidden: (VISIBILITY_TOOL_PLUSMOINS ? false : true),
          callback: function (thisPanel, thisTool, evt) {
            var windowClass = "Ext.ux.FileBrowserWindow";
            var reference = "load_symbolset";
            var window = Ext.getCmp(reference)
              || Ext.create(windowClass, {
                id: reference,
                selectExtension: '*.sym,*.txt',
                uploadExtension: '.sym,.txt',
                dataUrl: Routing.generate('carmen_ws_filebrowser', { routing: 'noroute' }),
                defaultPath: '/Root',
                relativeRoot: '/Root/Publication/etc',
                listeners: {
                  selectedfile: function (wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                    if (!Carmen.user.map) return;
                    var symbolset = './etc/' + fullPathName;

                    console.log({
                      mapfile: Carmen.user.map.get('real_mapfile'),
                      mapname: Carmen.user.map.get('mapFile').replace('/', "_"),
                      from_symbolset: (Carmen.user.map.get('symbolset')),
                      with_symbolset: (symbolset)
                    });

                    Ext.Ajax.request({
                      url: Routing.generate('carmen_ws_helpers_concat_symbolset', {
                        mapfile: Carmen.user.map.get('real_mapfile'),
                        mapname: Carmen.user.map.get('mapFile').replace('/', "_"),
                        from_symbolset: encodeURIComponent(Carmen.user.map.get('symbolset')),
                        with_symbolset: encodeURIComponent(symbolset)
                      })
                      , success: function (response) {
                        wind.close();
                        response = Ext.decode(response.responseText);
                        Carmen.user.map.set('symbolset', response.symbolset);
                        reloadWithParameters()
                      }
                      , failure: function (response) {
                        wind.close();
                        if (Carmen.notAuthenticatedException(response)) return;
                        response = Ext.decode(response.responseText);
                        Ext.Msg.alert(_T_LAYERSTYLE("fields.symbol_library.errorMsg"), response.data.results.description);
                        reloadWithParameters()
                      }
                    });
                  }
                }
              });
            window.show();
          }
        }, {
          type: 'minus',
          tooltip: _T_LAYERSTYLE("fields.symbol_library.tools.minus"),
          callback: function (thisPanel, thisTool, evt) {
            Ext.Msg.confirm(
              _T_LAYERSTYLE("fields.symbol_library.reset_symbolset.title")
              , _T_LAYERSTYLE("fields.symbol_library.reset_symbolset.msg")
              , function (btn) {
                if (btn != 'yes') return;

                Ext.Ajax.request({
                  url: Routing.generate('carmen_ws_helpers_reset_symbolset', {
                    mapfile: Carmen.user.map.get('real_mapfile'),
                    mapname: Carmen.user.map.get('mapFile').replace('/', "_")
                  })
                  , success: function (response) {
                    response = Ext.decode(response.responseText);
                    Carmen.user.map.set('symbolset', response.symbolset);
                    reloadWithParameters()
                  }
                  , failure: function (response) {
                    if (Carmen.notAuthenticatedException(response)) return;
                    response = Ext.decode(response.responseText);
                    Ext.Msg.alert(_T_LAYERSTYLE("fields.symbol_library.errorMsg"), response.data.results.description);
                    reloadWithParameters()
                  }
                })
              }
            )
          }
        }],
      }
      
      /*), Ext.form.field.Base.prototype)*/, {
        xtype: 'dataview',
        name: fieldName + '_view',
        tpl: [
          '<div class="symbol-library-view">',
          '<tpl for=".">',
          '<div class="symbol-library-item" title="{name}">',
          '<img src="{image}"/>',
          '</div>',
          '</tpl>',
          '</div>'
        ],
        autoScroll: true,
        listeners: {
          select: function (view, symbol, recordIndex, eOpts) {
            console.log(2406);
            let symbolName = symbol.get('name');
            me.selectSymbol = symbol;
            checkSymbol(view.getStore(), symbolName);
          }
        },
        selectedItemCls: "x-view-item-focused",
        itemSelector: 'div.symbol-library-item',
        store: Ext.data.StoreManager.lookup("SYMBOL_LIBRARY")
      }]
    };
  },

  loadDataDistribution: function (distribution) {
    var me = this;
    var components = me.query("[storeId=distribution]");
    Ext.each(components, function (component) {
      if (component instanceof Ext.grid.column.Column) {
        component.widget.store.loadRawData(distribution, false);
        if (component.widgets) {
          component.widgets.each(function (widget) {
            var record = widget.getWidgetRecord();
            if (record) widget.setValue(record.get(component.dataIndex));
          });
        }
      }
      else if (component.getStore) {
        component.getStore().loadRawData(distribution, false);
      }
    });

    if (Ext.isDefined(distribution.maxValue)) {
      me.down('[name=msStyleMaxValue]').setValue(distribution.maxValue);
    }
  },

  /**
   * Request server to obtain a msClass repartition according to the configuration of the layer analyse 
   * (all fields in the fieldset "Analyse" minus layerTransparency and layerLegendScale)
   */
  applyAnalyze: function (urlType) {
    urlType = urlType || 'symbology';
    var me = this;
    me.showFields(this.fieldsDefered);
    var record = me.getRecord();
    if (!record) return;
    var form = me.down('[name=formConfigAnalyse]');
    if (!form) return;
    var analyseCmp = me.down('[name=layerAnalyseType]');
    if (!analyseCmp) return;
    if (!analyseCmp.getValue()) return;

    var geometry = me.up('window').down('[name=msGeometryType]');
    if (geometry) geometry = (geometry.getValue() ? geometry.getSelection().get('codeField') : "POLYGON");

    var args = {};
    Ext.each(me.query("[submitName]"), function (component) {
      if (!component.isDisabled())
        args[component.submitName] = component.getValue();
    })

    var analyse = analyseCmp.getSelection().get('codeField');

    var classes = me.down('[name=msClasses]');
    if (classes) {
      Ext.suspendLayouts();

      var config = LEX_ANALYSE_TYPE.CONFIG[analyse];
      if (config) {
        classes.setBounds(config.NBCLASSES || { MIN: 1, MAX: 1 });
      }

      if ([LEX_ANALYSE_TYPE.TYPES.PIECHART, LEX_ANALYSE_TYPE.TYPES.BARCHART].indexOf(analyse) != -1) {
        var bounds = config.NBCLASSES || { MIN: 1, MAX: 1 };
        var msClassField = me.down('[name=msClassField]');
        if (msClassField) {
          var selection = msClassField.getSelectionModel(),
            count = selection.getCount();
          if (count < bounds.MIN) {
            Ext.Msg.alert('Choisissez au moins ' + bounds.MIN + ' champ');
            Ext.resumeLayouts(false);
            return;
          }
          if (count >= bounds.MAX) {
            Ext.Msg.alert('Choisissez un champ');
            Ext.resumeLayouts(false);
            return;
          }
          classes.setBounds({ MIN: bounds.MIN, MAX: count });

          args["msStyleSize"] = [];
          for (var i = 0; i < count; i++) {
            args["msStyleSize"].push(selection.getSelection()[i].get('fieldDataName'));
          }
        }
      }

      if (classes.up('panel')) {
        classes.up('panel').setTitle(new Ext.XTemplate(_T_LAYERSTYLE("fieldsets.msClasses")).apply({ analyse: analyseCmp.getSelection().get('displayField').replace('Analyse par ', '') }));
      }
      Ext.resumeLayouts(false);
    }

    var valid = true;
    Ext.each(me.query("[category=configuration]"), function (component) {
      valid = valid && (!component.isValid || component.isValid());
      return valid;
    })
    if (!valid || !form.isValid()) return;

    var cmpMask = me.down('[name=msClassesFieldset]');
    cmpMask.mask(_T_LAYERSTYLE("loading.applyAnalyze"));
    form.submit({
      method: 'GET',
      url: Routing.generate('carmen_ws_helpers_' + urlType, Ext.apply({
        mapfile: Carmen.user.map.get('real_mapfile'),
        layer: record.get('layerMsname'),
        layerTitle: record.get('layerTitle'),
        analyse: analyse,
        geometrytype: geometry
      }, args)),
      success: function (formBasic, request) {
        cmpMask.unmask();
        var response = request.response;
        if (!response) return;
        response = response.responseText;
        if (!response) return;
        response = Ext.decode(response);

        if (!response || !Ext.isObject(response) || !response.distribution) return;
        me.loadDataDistribution(response.distribution, analyse);

        if (!response || !Ext.isObject(response) || !response.classes) return;
        if (!classes) return;
        Ext.each(response.classes, function (classe) {
          classe.layer = record;
        })

        var form = me.down('[name=formSymbology]');
        if (form) {
          form.loadRecord(null);
        }

        classes.getStore().loadRawData(response.classes, false);
        Ext.Function.defer(classes.getView().updateLayout, 500, classes.getView());
      },
      failure: function (formBasic, request) {
        cmpMask.unmask();
        var response = request.response;
        if (Carmen.notAuthenticatedException(response)) return;
        if (!response) return;
        response = response.responseText;
        if (!response) return;
        response = Ext.decode(response);

        Ext.Msg.alert(_T_LAYERSTYLE("messages.error_analyse"), response.data.description);
      }
    });
  }
});


Ext.define('Carmen.FormSheet.Layer.TabStyle.SymbolPreview', {
  extend: 'Ext.view.View',
  xtype: 'symbolpreview',
  tpl: [
    '<div class="symbol-preview">',
    '<tpl for=".">',
    '<div class="symbol-preview-item">',
    '<tpl if="msClassLegendIcon">',
    '<img src="{msClassLegendIcon}"/>',
    '<tpl else>',
    '<img src=""/>',
    '</tpl>',
    '</div>',
    '</tpl>',
    '</div>'
  ],
  itemSelector: 'div.symbol-preview-item'
});


Ext.define('Carmen.Store.MsClass', {
  extend: 'Ext.data.Store',
  store: 'msclass',
  autoLoad: false,
  model: "Carmen.Model.MsClass",
  proxy: {
    type: 'memory',
    reader: {
      type: 'json',
      rootProperty: 'data'
    }
  }
});


Ext.define('Carmen.FormSheet.Layer.TabStyle.Symbology', {
  extend: 'Ext.form.Panel',
  xtype: 'formSymbology',
  disabled: true,
  setDisabled: function (disabled) {
    if (!this.getRecord()) {
      this.callParent([true]);
    } else {
      this.callParent(arguments);
    }
  },

  initialRecord: null,
  loadRecord: function (record) {
    Ext.suspendLayouts();
    this._generate_symbol = false;

    var oldRecord = this.getRecord();
    if (oldRecord) {
      oldRecord.isEditing = false;
    }

    var bottomToolbar = this.up('window').getDockedItems('toolbar[dock="bottom"]')[0];
    this.initialRecord = record;
    if (record) {
      record.isEditing = true;
      record.commit(true);
      record = record.clone();
      this.callParent([record]);
      this.callParent([record]);
      bottomToolbar && bottomToolbar.setDisabled(true);
    }
    else {
      this.getForm()._record = null;
      bottomToolbar && bottomToolbar.setDisabled(false);
    }
    this.setDisabled(!record);
    var name = [_T_LAYERSTYLE('fieldsets.symbology')];
    if (record) {
      name = name.concat([record.get('msClassName'), ('Classe n°' + (record.get('msClassIndex') + 1))]);
    }
    name = name.filter(function (item) { return item !== '' && item !== null });
    this.down('[name=symbology]').setTitle(name.join(' - '));

    var preview = this.down('[name=symbol_preview]');
    if (preview) {
      if (record) {
        preview.getStore().loadRecords([record], false);
      } else {
        preview.getStore().removeAll();
      }
    }
    var library = this.down('[name=symbol_library_view]');
    if (library) {
      if (record) {
        var symbolName = record.get("msStyleSymbol");
        library.selection_name = symbolName;
        library.setSelection(null);
        var selection = library.getStore().findRecord("name", symbolName);
        if (selection) {
          library.setSelection(selection);
        } else if (symbolName != "0" && symbolName != "") {
          library.fireEvent('select', library, library.getStore().getModel().create({ name: symbolName }))
        }
      }
    }

    var fieldsetPattern = this.down('[name=msStylePattern]');
    if (fieldsetPattern != null && fieldsetPattern.expand && record != null && record.data.msStylePattern != null && record.data.msStylePattern != '' && record.data.msStylePattern != 'false')
      fieldsetPattern.expand();

    this._generate_symbol = true;
    Ext.resumeLayouts(false);
  },
  generateSymbol: function (waitingComponent, updatedRecord) {
    if (!this._generate_symbol) return;
    var record = updatedRecord || this.getRecord();
    if (!record) return;
    var me = this;
    Ext.suspendLayouts();
    me.updateRecord(record);
    me.layerForm.updateRecord();
    if (waitingComponent) {
      waitingComponent.mask(_T_LAYERSTYLE("loading.generateSymbol"));
    }
    Ext.Ajax.request({
      url: Routing.generate("carmen_ws_helpers_generatesymbol", {
        geometrytype: this.up('window').down('[name=msGeometryType]').getValue(),
        symbolset: encodeURIComponent(Carmen.user.map.get('symbolset'))
      }),
      method: 'PUT',
      jsonData: Carmen.serializeMapserverRecordForSubmit(record),//Ext.Object.toQueryString(, true),
      success: function (response) {
        me.layerForm.getRecord().reject();
        if (!response.responseText) return;
        response = Ext.decode(response.responseText);
        if (!response) return;
        if (!response.data) return;
        if (updatedRecord) {
          updatedRecord.set('msClassLegendIcon', response.data.image);
          updatedRecord.commit(true);
        } else {
          me.down('[name=msClassLegendIcon]').setValue(response.data.image);
          me.updateRecord(record);
          Ext.each(Ext.ComponentQuery.query('cmngenericwindow > toolbar[dock="bottom"]'), function (cmp) { cmp.setDisabled(true) });
        }
        if (waitingComponent) {
          waitingComponent.unmask();
        }
        Ext.resumeLayouts(false);
      },
      failure: function (response) {
        if (Carmen.notAuthenticatedException(response)) return;
        me.layerForm.getRecord().reject();
        if (waitingComponent) {
          waitingComponent.unmask();
        }
        if (!updatedRecord)
          Ext.each(Ext.ComponentQuery.query('cmngenericwindow > toolbar[dock="bottom"]'), function (cmp) { cmp.setDisabled(true) });
        Ext.resumeLayouts(false);
      }
    })

  }

});


Ext.define('Carmen.FormSheet.Layer.TabStyle.Statistics', {
  extend: 'Ext.window.Window',
  xtype: 'layer-statistics',
  fieldType: 'int',

  constrain: true,
  constrainHeader: true,
  renderTo: Ext.getBody(),
  width: 650,
  height: 650,
  hidden: true,

  closeAction: 'hide',
  alwaysOnTop: true,

  initComponent: function () {
    var me = this;

    this.store = Ext.create('Ext.data.JsonStore', {
      autoLoad: false,
      fields: [{ name: "field", type: 'auto' }, { name: "count", type: 'int' }],
      proxy: {
        type: 'ajax',
        reader: {
          type: 'json',
          rootProperty: 'distribution'
        }
      }
    });
    me.items = [{
      xtype: 'tabpanel',
      deferredRender: true,
      items: [{ title: _T_LAYERSTYLE('statistiques.chartTitle'), xtype: 'panel', name: 'chartcontainer' }, {
        xtype: 'grid',
        title: _T_LAYERSTYLE('statistiques.gridTitle'),
        store: this.store,
        minWidth: this.width - 30,
        height: this.height - 70,
        autoScroll: true,
        columns: [
          { dataIndex: 'field', text: _T_LAYERSTYLE('statistiques.bottomAxis'), renderer: function (value) { if (value === null) return "<i>(NULL)</i>"; if (value === "") return "<i>(VIDE)</i>"; return value; } },
          { dataIndex: 'count', text: _T_LAYERSTYLE('statistiques.leftAxis'), type: "number" }
        ],
        forceFit: true,
        columnLines: true,
        viewConfig: {
          emptyText: _T_LAYERSTYLE('statistiques.noData'),
          stripeRows: true,
          trackOver: true
        }
      }]
    }];

    this.callParent();
    this.on('show', function () {
      if (!this.down('cartesian')) {
        this.down('[name=chartcontainer]').add({
          autoScroll: true,
          xtype: 'cartesian',
          minWidth: this.width - 30,
          height: this.height - 70,
          store: this.store,
          insetPadding: {
            top: 40,
            bottom: 40,
            left: 20,
            right: 40
          },
          interactions: 'itemhighlight',
          axes: [{
            type: 'numeric',
            position: 'left',
            minimum: 40,
            titleMargin: 20,
            grid: true,
            title: {
              text: _T_LAYERSTYLE('statistiques.leftAxis')
            }
          }, {
            type: 'category',
            grid: true,
            position: 'bottom',
            title: {
              text: _T_LAYERSTYLE('statistiques.bottomAxis')
            }
          }],
          animation: Ext.isIE8 ? false : {
            easing: 'backOut',
            duration: 500
          },
          series: {
            type: 'bar',
            xField: 'field',
            yField: 'count',
            style: {
              minGapWidth: 20
            },
            highlight: {
              strokeStyle: 'black',
              fillStyle: 'gold',
              lineDash: [5, 3]
            },
            tooltip: {
              trackMouse: true,
              style: 'background: #fff',
              renderer: function (storeItem, item) {
                this.setHtml('"' + storeItem.get('field') + '" = ' + storeItem.get('count') + _T_LAYERSTYLE('statistiques.unit'));
              }
            }
          },
          sprites: {
            type: 'text',
            fontSize: 22,
            width: 100,
            height: 30,
            x: 40, // the sprite x position
            y: 20  // the sprite y position
          }
        })
      }
    })
    new Ext.LoadMask({ target: this, store: this.store, msg: _T_LAYERSTYLE("loading.generateDistribution") });
  },

  showStatistics: function (layerName, classitem, classitemIndex, classitemTitle, classitemType) {
    if (Ext.isNumber(classitemType)) {
      classitemType = FIELD_DATATYPES.findRecord('valueField', classitemType).get('codeField');
    }
    classitemType = classitemType.toLowerCase();
    this.show();
    Ext.suspendLayouts();
    var geometry = this.up('window').down('[name=msGeometryType]');
    if (geometry && geometry.getValue()) geometry = geometry.getSelection().get('codeField');
    if (typeof geometry != "string") geometry = "POINT";
    var args = { classitem: classitem, classitemIndex: classitemIndex, classitemTitle: classitemTitle };
    this.setTitle(new Ext.XTemplate(_T_LAYERSTYLE('statistiques.title')).apply(args));
    var proxy = this.store.getProxy();
    proxy.setUrl(
      Routing.generate('carmen_ws_helpers_distribution', {
        mapfile: Carmen.user.map.get('real_mapfile'),
        layer: layerName,
        analyse: LEX_ANALYSE_TYPE.TYPES.UNIQVALUE,
        geometrytype: geometry
      })
    );
    proxy.setExtraParams(args);

    this.store.getModel().replaceFields({ name: 'field', type: classitemType });
    this.store.load();
    Ext.resumeLayouts(false);
  }
});



var en_developpement = {
  copyAnalyse: function () { return true; },
  layerAnalyseType: function (rec) {
    return true;
    var types = LEX_ANALYSE_TYPE.TYPES;
    ([types.UNIQSYMBOL, types.UNIQVALUE, types.GRADUATECOLOR, types.GRADUATESYMBOL, types.PROPORTIONAL, types.RASTER].indexOf(rec.get('codeField')) != -1);
  }
}