var PANEL_UNIQUE_VALUE_TRANSLATE = function (key) {
  return _t("Layer.forms.AnalyseType." + key);
};

/**
 * Carmen.Model.Layer.AnalyseType.UniqValue
 */
Ext.define('Carmen.Model.Layer.AnalyseType.UniqValue', {
  extend: 'Ext.data.Model',
  idProperty: 'layerId',
  fields: [
  ]
});


/**
 * Analyse type panel : uniq value analysis
 */
Ext.define('Carmen.FormSheet.Layer.AnalyseType.UniqValue', {
  extend: 'Ext.form.Panel',
  title: PANEL_UNIQUE_VALUE_TRANSLATE('title'),
  frame: true,
  height: 'auto',
  layout: 'fit',
  html: 'uniq value analysis',
  /*items: [
   
   ]*/
});
