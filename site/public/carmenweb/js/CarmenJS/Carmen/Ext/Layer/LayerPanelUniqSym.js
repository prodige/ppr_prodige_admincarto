var PANEL_UNIQUE_SYM_TRANSLATE = function (key) {
  return _t("Layer.forms.AnalyseType." + key);
};

/**
 * Carmen.Model.Layer.AnalyseType.UniqSymbol
 */
Ext.define('Carmen.Model.Layer.AnalyseType.UniqSymbol', {
  extend: 'Ext.data.Model',
  idProperty: 'layerId',
  fields: [
  ]
});


/**
 * Analyse type panel : uniq symbol analysis
 */
Ext.define('Carmen.FormSheet.Layer.AnalyseType.UniqSymbol', {
  extend: 'Ext.form.Panel',
  title: PANEL_UNIQUE_SYM_TRANSLATE('title'),
  frame: true,
  height: 'auto',
  layout: 'fit',
  html: 'uniq symbol analysis',
  /*items: [
   
   ]*/
});
