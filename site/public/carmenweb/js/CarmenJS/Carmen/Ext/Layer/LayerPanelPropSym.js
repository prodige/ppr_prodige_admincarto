var PANEL_PROPSYM_TRANSLATE = function (key) {
  return _t("Layer.forms.AnalyseType." + key);
};

/**
 * Carmen.Model.Layer.AnalyseType.PropSym
 */
Ext.define('Carmen.Model.Layer.AnalyseType.PropSym', {
  extend: 'Ext.data.Model',
  idProperty: 'layerId',
  fields: [
  ]
});


/**
 * Analyse type panel : Proportional symbol analysis
 */
Ext.define('Carmen.FormSheet.Layer.AnalyseType.PropSym', {
  extend: 'Ext.form.Panel',
  title: PANEL_PROPSYM_TRANSLATE('title'),
  frame: true,
  height: 'auto',
  layout: 'fit',
  html: 'proportional symbol analysis',
  /*items: [
   
   ]*/
});
