var _T_LAYER_TILE_VECTOR = function (key) {
  return _t("Layer.forms.GTabTileVector." + key);
};

/**
 * Carmen.Model.LayerTileVector
 */
Ext.define('Carmen.Model.LayerTileVector', {
  extend: 'Carmen.Model.LayerVectorial',
  idProperty: 'layerId',
  fields: [
    { name: "layerType", type: "string", defaultValue: 'TILE_VECTOR', convert: function (value) { if (Ext.isObject(value)) return value.layerTypeCode; return value; } },

    { name: "msLayerConnectionType", type: "string", defaultValue: "TILE_VECTOR" }, // mapfile data : CONNECTIONTYPE

    { name: "msLayerPgSchema", type: "string" }, // mapfile data : DATA (SCHEMA_NAME part)
    { name: "msLayerPgTable", type: "string" }, // mapfile data : DATA (TABLE_NAME part)
    { name: "msLayerPgGeometryField", type: "string" }, // mapfile data : DATA (THE_GEOM part)
    { name: "msLayerPgIdField", type: "string" }, // mapfile data : DATA (GID part)
    { name: "msLayerPgProjection", type: "string" }, // mapfile data : DATA (SRID part)
    {name: "layerTiledServerUrl",      type: "string"}

  ]
});

/**
 * Generic tab for type of GEOJSON layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabTileVector', {
  extend: 'Carmen.Generic.Form',
  name: 'GTabTileVector',
  title: _T_LAYER_TILE_VECTOR('title'),
  height: 'auto',
  layout: 'column',
  otherTabs: [ 'TabMeta', 'TabOGC'],
  modelName: 'Carmen.Model.LayerTileVector',
  defaults: {
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },

  // methods
  initComponent: function () {
    var self = this;

    /**
     * @mode remote
     * @table : wxs (wxs_type=WMS)
     */
    self.storeService = new Ext.data.XmlStore({
      storeId: 'SAVED_VECTPR_TILE_SERVICES',
      autoLoad: true,
      fields: [
        { name: 'valueField', mapping: 'wxsId' },
        { name: 'displayField', mapping: 'wxsName' }
      ],
      proxy: {
        type: 'rest',
        url: Routing.generate('carmen_ws_get_saved_services') + "/vector_tile"
      }
    });

    self.storeServiceStyle = new Ext.data.XmlStore({
      storeId: 'SAVED_VECTPR_TILE_SERVICES',
      autoLoad: true,
      fields: [
        { name: 'valueField', mapping: 'wxsId' },
        { name: 'displayField', mapping: 'wxsName' }
      ],
      proxy: {
        type: 'rest',
        url: Routing.generate('carmen_ws_get_saved_services') + "/vector_tile_style"
      }
    });

    // items on this panel
    self.items = [{
      xtype: 'fieldset',
      layout: 'form',
      padding: 0,
      margin: 0,
      border: false,
      items: [{
        xtype: 'textfield',
        name: 'layerTitle',

        fieldLabel: _T_LAYER_TILE_VECTOR('fields.title'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_TILE_VECTOR('fields.name'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [registerWarningOnIdentifierLength({
          xtype: 'textfield',
          name: 'layerName',
          allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          vtype: 'identifier',
          flex: 5
        }), {
          xtype: 'button',
          name: 'layerName_button',
          text: _T_LAYER_TILE_VECTOR('fields.auto'),
          margin: '0 10px 0 0',
          /**
           * auto calculation of layerName field
           */
          handler: function () {
            var ctrlFrom = self.down('[name=layerTitle]');
            var ctrlTo = self.down('[name=layerName]');
            if (ctrlFrom && ctrlTo) {
              ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
            }
          },
          flex: 1
        }, {
          labelAlign: 'right',
          xtype: 'textfield',
          name: 'layerIdentifier',
          fieldLabel: _T_LAYER_TILE_VECTOR('fields.ident'),
          allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          vtype: 'identifier',
          flex: 5
        }, {
          xtype: 'button',
          name: 'layerIdentifier_button',
          text: _T_LAYER_TILE_VECTOR('fields.auto'),
          /**
           * auto calculation of layerName field
           */
          handler: function () {
            var ctrlFrom = self.down('[name=layerTitle]');
            var ctrlTo = self.down('[name=layerIdentifier]');
            if (ctrlFrom && ctrlTo) {
              ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
            }
          },
          flex: 1
        }]
      },{
        xtype: 'fieldset',
        title: _T_LAYER_TILE_VECTOR('fields.tileServerParam'),
        margins: 8,
  
        layout: 'form',
        items: [{
          xtype: 'combobox',
          name: 'LayerSavedConnections',
          fieldLabel: _T_LAYER_WMS('fields.savedServ'),
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: self.storeService,
          autoLoadOnValue: true,
          autoSelect: true,
          editable: false,
          listeners: {
            /**
             * update fields : msLayerConnectionName and layerServerUrl with values contents in selected record
             * @param {Ext.form.field.ComboBox} combo
             * @param {Ext.data.Model} record
             */
            select: function (combo, record) {
              var ctrlName = self.down('[name=msLayerConnectionName]');
              var ctrlUrl = self.down('[name=layerServerUrl]');
              if (ctrlName && ctrlUrl) {
                ctrlName.setValue(record.get("wxsName"));
                ctrlUrl.setValue(record.get("wxsUrl"));
              }
            }
          }
  
        }, {
          xtype: 'textfield',
          name: 'msLayerConnectionName',
          fieldLabel: _T_LAYER_WMS('fields.nameServ')
        }, {
          xtype: 'textfield',
          name: 'layerServerUrl',
          fieldLabel: _T_LAYER_TILE_VECTOR('fields.urlServ'),
          allowBlank: false,
          listeners: {
            change: function (oThis, newValue, oldValue) {
              var btCall = self.down('[name=btCallWMS]');
              if (btCall) {
                btCall.setDisabled(!!newValue);
              }
            }
          }
        }, {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          anchor: '100%',
          defaults: {
            margin: '0 8 0 0'
          },
          items: [{
            xtype: 'button',
            name: 'btAddWMS',
            hidden: (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WMS('fields.btAddWMS'),
            /**
             * add current connection name and url to wms saved services
             */
            handler: function () {
              var ctrlList = self.down('[name=LayerSavedConnections]');
              var ctrlName = self.down('[name=msLayerConnectionName]');
              var ctrlUrl = self.down('[name=layerServerUrl]');
              if (ctrlName && ctrlUrl && ctrlList) {
                if (!ctrlName.allowBlank && (!ctrlName.isValid() || !ctrlUrl.isValid()) && ctrlName.allowBlank){
                  return false;
                }
                ctrlName.allowBlank = true;
  
                Ext.Ajax.request({
                  url: Routing.generate('carmen_ws_post_saved_services') + "/vector_tile",
                  method: 'POST',
                  params: {
                    wxsName: ctrlName.getValue(),
                    wxsUrl: ctrlUrl.getValue()
                  },
                  success: function (response) {
                    console.log(response);
                    var oRes = JSON.parse( response.responseText );
                    Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMS('msgApplied.addWMSSavedServ'));
                    ctrlList.getStore().reload();
                    // select added service
                    ctrlList.setValue(oRes.wxsId);
                  },
                  failure: function (response) {
                    if (Carmen.notAuthenticatedException(response)) {
                      return false;
                    }

                    var oRes = JSON.parse( response.responseText );
                    var msgKey = "addWMSSavedServ";
                    Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
                      .setIcon(Ext.MessageBox.ERROR);
                  }
                });
              }
            },
            flex: 2
          }, {
            xtype: 'button',
            name: 'btDeleteWMS',
            hidden: (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WMS('fields.btDeleteWMS'),
            /**
             * remove from wms saved services, the current connection name
             */
            handler: function () {
              var ctrlList = self.down('[name=LayerSavedConnections]');
              var ctrlName = self.down('[name=msLayerConnectionName]');
              var ctrlUrl = self.down('[name=layerServerUrl]');
              if (ctrlName && ctrlUrl && ctrlList) {
                Ext.Msg.confirm(
                  new Ext.XTemplate(_t("form.confirmDeleteTitle")).apply(),
                  new Ext.XTemplate(_T_LAYER_WMS("msgApplied.delWMSConfirm")).apply(),
                  function (btn) {
                    if (btn === "yes") {
  
                      Ext.Ajax.request({
                        url: Routing.generate('carmen_ws_delete_saved_services') + "/vector_tile",
                        method: 'DELETE',
                        params: {
                          wxsId: ctrlList.getSelection().getData().wxsId
                        },
                        success: function (response) {
                          Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMS('msgApplied.delWMSSavedServ'));
                          ctrlName.setValue("");
                          ctrlUrl.setValue("");
                          ctrlList.setValue("");
                          ctrlList.getStore().reload();
                        },
                        failure: function (response) {
                          if (Carmen.notAuthenticatedException(response)) return;
                          var oRes = eval("(" + response.responseText + ")");
                          var msgKey = "delWMSSavedServ";
                          Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
                            .setIcon(Ext.MessageBox.ERROR);
                        }
                      });
  
                    }
                  }
                ).icon = Ext.Msg.WARNING;
              }
            },
            flex: 2
          }]
        }]
      },{
        // url tile style server
        xtype: 'fieldset',
        title: _T_LAYER_TILE_VECTOR('fields.tileStyleParam'),
        margins: 8,
  
        layout: 'form',
        items: [{
          xtype: 'combobox',
          name: 'layerStyleConnection',
          fieldLabel: _T_LAYER_WMS('fields.savedServ'),
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: self.storeServiceStyle,
          autoLoadOnValue: true,
          autoSelect: true,
          editable: false,
          listeners: {
            /**
             * update fields : msLayerConnectionName and layerServerUrl with values contents in selected record
             * @param {Ext.form.field.ComboBox} combo
             * @param {Ext.data.Model} record
             */
            select: function (combo, record) {
              var ctrlName = self.down('[name=layerStyleConnection]');
              var ctrlUrl = self.down('[name=layerTiledServerUrl]');
              if (ctrlName && ctrlUrl) {
                ctrlName.setValue(record.get("wxsName"));
                ctrlUrl.setValue(record.get("wxsUrl"));
              }
            }
          }
  
        }, {
          xtype: 'textfield',
          name: 'styleNameService',
          fieldLabel: _T_LAYER_WMS('fields.nameServ')
        }, {
          xtype: 'textfield',
          name: 'layerTiledServerUrl',
          fieldLabel: _T_LAYER_WMS('fields.urlServ'),
          allowBlank: false,
          listeners: {
            change: function (oThis, newValue, oldValue) {
              var btCall = self.down('[name=btCallWMS]');
              if (btCall) {
                btCall.setDisabled(!!newValue);
              }
            }
          }
        }, {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          anchor: '100%',
          defaults: {
            margin: '0 8 0 0'
          },
          items: [{
            xtype: 'button',
            name: 'btAddWMS',
            hidden: (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WMS('fields.btAddWMS'),
            /**
             * add current connection name and url to wms saved services
             */
            handler: function () {
              var ctrlList = self.down('[name=layerStyleConnection]');
              var ctrlName = self.down('[name=styleNameService]');
              var ctrlUrl = self.down('[name=layerTiledServerUrl]');
              if (ctrlName && ctrlUrl && ctrlList) {
                if (!ctrlName.allowBlank && (!ctrlName.isValid() || !ctrlUrl.isValid()) && ctrlName.allowBlank){
                  return false;
                }
                ctrlName.allowBlank = true;
  
                Ext.Ajax.request({
                  url: Routing.generate('carmen_ws_post_saved_services') + "/vector_tile_style",
                  method: 'POST',
                  params: {
                    wxsName: ctrlName.getValue(),
                    wxsUrl: ctrlUrl.getValue()
                  },
                  success: function (response) {
                    console.log(response);
                    var oRes = JSON.parse( response.responseText );
                    Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMS('msgApplied.addWMSSavedServ'));
                    ctrlList.getStore().reload();
                    // select added service
                    ctrlList.setValue(oRes.wxsId);
                  },
                  failure: function (response) {
                    if (Carmen.notAuthenticatedException(response)) {
                      return false;
                    }

                    var oRes = JSON.parse( response.responseText );
                    var msgKey = "addWMSSavedServ";
                    Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
                      .setIcon(Ext.MessageBox.ERROR);
                  }
                });
              }
            },
            flex: 2
          }, {
            xtype: 'button',
            name: 'btDeleteWMS',
            hidden: (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WMS('fields.btDeleteWMS'),
            /**
             * remove from wms saved services, the current connection name
             */
            handler: function () {
              var ctrlList = self.down('[name=layerStyleConnection]');
              var ctrlName = self.down('[name=styleNameService]');
              var ctrlUrl = self.down('[name=layerTiledServerUrl]');
              if (ctrlName && ctrlUrl && ctrlList) {
                Ext.Msg.confirm(
                  new Ext.XTemplate(_t("form.confirmDeleteTitle")).apply(),
                  new Ext.XTemplate(_T_LAYER_WMS("msgApplied.delWMSConfirm")).apply(),
                  function (btn) {
                    if (btn === "yes") {
  
                      Ext.Ajax.request({
                        url: Routing.generate('carmen_ws_delete_saved_services') + "/vector_tile",
                        method: 'DELETE',
                        params: {
                          wxsId: ctrlList.getSelection().getData().wxsId
                        },
                        success: function (response) {
                          Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMS('msgApplied.delWMSSavedServ'));
                          ctrlName.setValue("");
                          ctrlUrl.setValue("");
                          ctrlList.setValue("");
                          ctrlList.getStore().reload();
                        },
                        failure: function (response) {
                          if (Carmen.notAuthenticatedException(response)) return;
                          var oRes = eval("(" + response.responseText + ")");
                          var msgKey = "delWMSSavedServ";
                          Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
                            .setIcon(Ext.MessageBox.ERROR);
                        }
                      });
  
                    }
                  }
                ).icon = Ext.Msg.WARNING;
              }
            },
            flex: 2
          }]
        }]
      }]
    }];

    this.callParent(arguments);
  },

  loadRecord: function (record) {
    var ret = this.callParent(arguments);
    var components = ['msLayerData'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);

    var ctrlPgProj = this.down('[name=msLayerPgProjection]');
    var ctrlPgProjD = this.down('[name=msLayerPgProjectionDisplay]');
    var ctrlPgType = this.down('[name=msLayerPgType]');
    var ctrPgSchema = this.down('[name=msLayerPgSchema]');
    var ctrPgTable = this.down('[name=msLayerPgTable]');

    if (ctrlPgProj)
      ctrlPgProj.setValue(record.get("msLayerPgProjection"));
    if (ctrlPgProjD)
      ctrlPgProjD.setValue(record.get("msLayerPgProjection"));
    if (ctrlPgType)
      ctrlPgType.setValue(record.get("msLayerPgType"));

    return ret;
  }

});
