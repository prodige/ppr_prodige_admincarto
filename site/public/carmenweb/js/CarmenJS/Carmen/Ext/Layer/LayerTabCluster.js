var CMP_NAME = 'Carmen.FormSheet.Layer.TabCluster';
Ext.define('Carmen.FormSheet.Layer.TabCluster', {
  extend: 'Carmen.Generic.Form',
  xtype: CMP_REFERENCE(CMP_NAME),

  title: 'test',
  height: 'auto',
  layout: 'fit',
  items: [
    {
      xtype: 'cmngenericform',
      title: false,
      layout: 'anchor',
      items: [{
        xtype: 'cmnlayerfields',
        name: 'layerFields'
      }]
    }
  ],

  loadRecord: function (record) {
    this.callParent(arguments);

  }
});