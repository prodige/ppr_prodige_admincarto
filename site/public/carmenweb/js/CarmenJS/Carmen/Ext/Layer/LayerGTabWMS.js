var _T_LAYER_WMS = function (key) {
  return _t("Layer.forms.GTabWMS." + key);
};

/**
 * Carmen.Model.LayerWMS
 */
Ext.define('Carmen.Model.LayerWMS', {
  extend: 'Carmen.Model.LayerRasterized',
  idProperty: 'layerId',
  fields: [
    { name: "layerType", type: "string", defaultValue: 'WMS', convert: function (value) { if (Ext.isObject(value)) return value.layerTypeCode; return value; } },

    { name: "LayerSavedConnections", type: "string", persist: false },
    { name: "msLayerConnectionName", type: "string" },                      // mapfile data : NAME
    { name: "msLayerConnectionType", type: "string", defaultValue: "WMS" }, // mapfile data : CONNECTIONTYPE

    { name: "wmsTitle", type: "string" },
    { name: "wmsSrs", type: "string" },

    {
      name: "layerWxsnameRO", type: "string", persist: false, convert: function (value, record) {
        return (record && record.data && record.data.layerWxsname ? record.data.layerWxsname : "");
      }
    },
    {
      name: "layerOutputformatRO", type: "string", persist: false, convert: function (value, record) {
        return (record && record.data && record.data.layerOutputformat ? record.data.layerOutputformat : "");
      }
    }
  ]
});

/**
 * Generic tab for type of WMS layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabWMS', {
  extend: 'Carmen.Generic.Form',
  name: 'GTabWMS',
  title: _T_LAYER_WMS('title'),
  height: 'auto',
  layout: 'column',
  otherTabs: ['TabStyle', 'TabMeta'],
  modelName: 'Carmen.Model.LayerWMS',
  defaults: {
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },

  // methods
  initComponent: function () {
    var heightLayerList = 350;
    var me = this;

    /**
     * @mode remote
     * @table : wxs (wxs_type=WMS)
     */
    me.storeService = new Ext.data.XmlStore({
      storeId: 'SAVED_WMS_SERVICES',
      autoLoad: true,
      fields: [
        { name: 'valueField', mapping: 'wxsId' },
        { name: 'displayField', mapping: 'wxsName' }
      ],
      proxy: {
        type: 'rest',
        url: Routing.generate('carmen_ws_get_saved_services') + "/WMS"
      }
    });

    // items on this panel
    me.items = [{
      xtype: 'fieldset',
      layout: 'form',
      padding: 0,
      margin: 0,
      border: false,
      items: [{
        xtype: 'textfield',
        name: 'layerTitle',

        fieldLabel: _T_LAYER_WMS('fields.title'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_WMS('fields.name'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [registerWarningOnIdentifierLength({
          xtype: 'textfield',
          name: 'layerName',
          allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          vtype: 'identifier',
          flex: 5
        }), {
          xtype: 'button',
          name: 'layerName_button',
          text: _T_LAYER_WMS('fields.auto'),
          margin: '0 10px 0 0',
          /**
           * auto calculation of layerName field
           */
          handler: function () {
            var ctrlFrom = me.down('[name=layerTitle]');
            var ctrlTo = me.down('[name=layerName]');
            if (ctrlFrom && ctrlTo) {
              ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
            }
          },
          flex: 1
        }, {
          labelAlign: 'right',
          xtype: 'textfield',
          name: 'layerIdentifier',
          fieldLabel: _T_LAYER_WMS('fields.ident'),
          allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          vtype: 'identifier',
          flex: 5
        }, {
          xtype: 'button',
          name: 'layerIdentifier_button',
          text: _T_LAYER_WMS('fields.auto'),
          /**
           * auto calculation of layerName field
           */
          handler: function () {
            var ctrlFrom = me.down('[name=layerTitle]');
            var ctrlTo = me.down('[name=layerIdentifier]');
            if (ctrlFrom && ctrlTo) {
              ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
            }
          },
          flex: 1
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_WMSC('fields.proj'),
        width: Carmen.Util.WINDOW_WIDTH - 40,
        layout: 'hbox',
        items: [{
          flex: 6,
          margin: '0 10px 0 0',
          xtype: 'comboboxprojections',
          name: 'layerProjectionEpsg',
          editable: false,
          value: Carmen.Defaults.PROJECTION
        }, {
          labelAlign: 'right',
          xtype: 'checkbox',
          name: 'layerWmsQuery',
          fieldLabel: _T_LAYER_WMS('fields.query'),
          hidden: (VISIBILITY_LAYER_WMSQUERY ? false : true),
          flex: 6
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_WMS('fields.scales'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          labelAlign: 'right',
          labelWidth: 60,
          xtype: 'numberfield',
          name: 'layerMinscale',
          fieldLabel: _T_LAYER_WMS('fields.min'),
          allowBlank: false,
          maxLength: 10,
          enforceMaxLength: true,
          minValue: Carmen.Defaults.MINSCALE,
          maxValue: Carmen.Defaults.MAXSCALE,
          hideTrigger: true,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          allowOnlyWhitespace: false,
          allowDecimals: false,
          allowExponential: false,
          flex: 6,
          margin: '0 10px 0 0'
        }, {
          labelAlign: 'right',
          xtype: 'numberfield',
          name: 'layerMaxscale',
          vtype: '',
          fieldLabel: _T_LAYER_WMS('fields.max'),
          allowBlank: false,
          maxLength: 10,
          enforceMaxLength: true,
          minValue: Carmen.Defaults.MINSCALE,
          maxValue: Carmen.Defaults.MAXSCALE,
          hideTrigger: true,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          allowOnlyWhitespace: false,
          allowDecimals: false,
          allowExponential: false,
          flex: 6
        }]
      }, {
        xtype: 'fieldset',
        title: _t("Layer.window.titleLayerBackground"), //"Paramétrages d'affichages de la couche",
        layout: 'form',
        items: [{
          xtype: 'fieldcontainer',
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [{
            xtype: 'checkbox',
            name: 'layerIsBackground',
            fieldLabel: _t("Layer.forms.fields.layerIsBackground"),
            flex: 6,
            labelWidth: '80%',
            listeners: [{
              change: function (me, value) {
                var show_component = false;
                var is_checked = value;
                if (is_checked) {
                  show_component = true;
                }

                var backgroundDisplayImage = Ext.ComponentQuery.query('[name=backgroundDisplayImage]')[0];
                backgroundDisplayImage.setVisible(show_component);
              }
            }]
          }, {
            xtype: 'checkbox',
            name: 'layerIsDefaultBackground',
            fieldLabel: _t("Layer.forms.fields.layerIsDefaultBackground"),
            flex: 6,
            labelWidth: '80%',
            listeners: [{
              change: function (me, value) {

                var mainFrame = Ext.ComponentQuery.query('[name=GTabWMS]')[0];
                var mainFrameRecord = mainFrame.form._record;
                var layerId = mainFrameRecord.data.layerId;
                var joinedStore = mainFrameRecord.joined;

                for (var i = 0; i < joinedStore.length; i++) {
                  var current_store = joinedStore[i];
                  if (current_store.config.model === "Carmen.Model.Layer") {
                    for (var i = 0; i < current_store.data.items.length; i++) {
                      var current_layer = current_store.data.items[i];
                      if (current_layer.id !== layerId) {
                        current_layer.data.layerIsDefaultBackground = false;
                      }
                    }
                  }
                }
              }
            }]
          }]
        }, {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          hidden: true,
          name: "backgroundDisplayImage",
          items: [{

            fieldLabel: _t("Layer.forms.fields.layerBackgroundImage"),
            name: 'layerBackgroundImage',
            xtype: 'textfield',
            labelWidth: '40%',
            width: Carmen.Util.WINDOW_WIDTH - 90,
            queryMode: 'local',
            displayField: 'displayField',
            valueField: 'valueField',
            store: Carmen.Dictionaries.LOGOPATH_IMAGES,
            editable: false,
            autoLoadOnValue: true,
            autoSelect: true,
            flex: 8
          }, {
            xtype: 'button',
            text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
            name: 'uiLogopath_button',
            flex: 3,
            handler: function () {
              var windowClass = "Ext.ux.FileBrowserWindow";
              var window = Ext.getCmp(CMP_REFERENCE(windowClass))
                || Ext.create(windowClass, {
                  id: CMP_REFERENCE(windowClass),
                  selectExtension: '*.jpg,*.gif,*.png',
                  uploadExtension: '.jpg,.gif,.png',
                  dataUrl: Routing.generate('carmen_ws_filebrowser', { routing: 'noroute' }),
                  defaultPath: '/Root',
                  relativeRoot: '/Root/IHM',
                  listeners: {
                    selectedfile: function (wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                      var ctrlTo = me.down('[name=layerBackgroundImage]');
                      ctrlTo.setValue(fullPathName);
                      wind.close();
                    }
                  }
                });
              window.show();
            },

            listeners: {
              afterrender: function () {
                var check = me.down('[name=uiLogo]');
                if (check) { check.fireEvent('change', check); }
              }
            }
          }]
        }]
      }]
    }, {
      xtype: 'fieldset',
      title: _T_LAYER_WMS('fields.streamParam'),
      margins: 8,

      layout: 'form',
      items: [{
        xtype: 'combobox',
        name: 'LayerSavedConnections',
        fieldLabel: _T_LAYER_WMS('fields.savedServ'),
        queryMode: 'local',
        displayField: 'displayField',
        valueField: 'valueField',
        store: me.storeService,
        autoLoadOnValue: true,
        autoSelect: true,
        editable: false,
        listeners: {
          /**
           * update fields : msLayerConnectionName and layerServerUrl with values contents in selected record
           * @param {Ext.form.field.ComboBox} combo
           * @param {Ext.data.Model} record
           */
          select: function (combo, record) {
            var ctrlName = me.down('[name=msLayerConnectionName]');
            var ctrlUrl = me.down('[name=layerServerUrl]');
            if (ctrlName && ctrlUrl) {
              ctrlName.setValue(record.get("wxsName"));
              ctrlUrl.setValue(record.get("wxsUrl"));
            }
          }
        }

      }, {
        xtype: 'textfield',
        name: 'msLayerConnectionName',
        fieldLabel: _T_LAYER_WMS('fields.nameServ')
        //allowBlank: false,
        /*  recommended that the name not contain spaces, special characters, or begin with a number */
        //vtype: 'identifier'
      }, {
        xtype: 'textfield',
        name: 'layerServerUrl',
        fieldLabel: _T_LAYER_WMS('fields.urlServ'),
        allowBlank: false,
        vtype: 'url',
        listeners: {
          change: function (oThis, newValue, oldValue) {
            var btCall = me.down('[name=btCallWMS]');
            if (btCall) {
              btCall.setDisabled(newValue != "" ? false : true);
            }
          }
        }
      }, {
        xtype: 'fieldcontainer',
        name: "fcWmsInfo",
        fieldLabel: _T_LAYER_WMS('fields.layerName'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'textfield',
          name: "layerWxsnameRO",
          readOnly: true,
          flex: 6,
          margin: '0 10px 0 0'
        }, {
          xtype: 'textfield',
          name: 'layerOutputformatRO',
          fieldLabel: _T_LAYER_WMS('fields.layerOutput'),
          readOnly: true,
          flex: 6
        }]
      }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        anchor: '100%',
        defaults: {
          margin: '0 8 0 0'
        },
        items: [{
          xtype: 'button',
          name: 'btCallWMS',
          text: _T_LAYER_WMS('fields.btCallWMS'),
          disabled: true,
          /**
           * call current URL to load wms context
           */
          handler: function () {
            me.launchGetCapabilities();
          },
          flex: 2
        }, {
          xtype: 'button',
          name: 'btAddWMS',
          hidden: (VISIBILITY_WXS_MANAGE ? false : true),
          text: _T_LAYER_WMS('fields.btAddWMS'),
          /**
           * add current connection name and url to wms saved services
           */
          handler: function () {
            var ctrlList = me.down('[name=LayerSavedConnections]');
            var ctrlName = me.down('[name=msLayerConnectionName]');
            var ctrlUrl = me.down('[name=layerServerUrl]');
            if (ctrlName && ctrlUrl && ctrlList) {
              if (!void (ctrlName.allowBlank = false) && (!ctrlName.isValid() || !ctrlUrl.isValid()) && !void (ctrlName.allowBlank = true))
                return;
              ctrlName.allowBlank = true;

              Ext.Ajax.request({
                url: Routing.generate('carmen_ws_post_saved_services') + "/WMS",
                method: 'POST',
                params: {
                  wxsName: ctrlName.getValue(),
                  wxsUrl: ctrlUrl.getValue()
                },
                success: function (response) {
                  var oRes = eval("(" + response.responseText + ")");
                  Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMS('msgApplied.addWMSSavedServ'));
                  ctrlList.getStore().reload();
                  // select added service
                  ctrlList.setValue(oRes.wxsId);
                },
                failure: function (response) {
                  if (Carmen.notAuthenticatedException(response)) return;
                  var oRes = eval("(" + response.responseText + ")");
                  var msgKey = "addWMSSavedServ";
                  Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
                    .setIcon(Ext.MessageBox.ERROR);
                }
              });
            }
          },
          flex: 2
        }, {
          xtype: 'button',
          name: 'btDeleteWMS',
          hidden: (VISIBILITY_WXS_MANAGE ? false : true),
          text: _T_LAYER_WMS('fields.btDeleteWMS'),
          /**
           * remove from wms saved services, the current connection name
           */
          handler: function () {
            var ctrlList = me.down('[name=LayerSavedConnections]');
            var ctrlName = me.down('[name=msLayerConnectionName]');
            var ctrlUrl = me.down('[name=layerServerUrl]');
            if (ctrlName && ctrlUrl && ctrlList) {
              Ext.Msg.confirm(
                new Ext.XTemplate(_t("form.confirmDeleteTitle")).apply(),
                new Ext.XTemplate(_T_LAYER_WMS("msgApplied.delWMSConfirm")).apply(),
                function (btn) {
                  if (btn == "yes") {

                    Ext.Ajax.request({
                      url: Routing.generate('carmen_ws_delete_saved_services') + "/WMS",
                      method: 'DELETE',
                      params: {
                        wxsId: ctrlList.getSelection().getData().wxsId
                      },
                      success: function (response) {
                        Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMS('msgApplied.delWMSSavedServ'));
                        ctrlName.setValue("");
                        ctrlUrl.setValue("");
                        ctrlList.setValue("");
                        ctrlList.getStore().reload();
                      },
                      failure: function (response) {
                        if (Carmen.notAuthenticatedException(response)) return;
                        var oRes = eval("(" + response.responseText + ")");
                        var msgKey = "delWMSSavedServ";
                        Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
                          .setIcon(Ext.MessageBox.ERROR);
                      }
                    });

                  }
                }
              ).icon = Ext.Msg.WARNING;
            }
          },
          flex: 2
        }]
      }]
    }, {
      xtype: 'fieldset',
      name: 'fsTree',
      title: _T_LAYER_WMS('fields.titleLayersList'),
      columnWidth: 0.5,
      margin: '0 4 0 0',
      height: heightLayerList,
      padding: '0 0 0 0',
      scrollable: true,
      items: []
    }, {
      xtype: 'fieldset',
      name: 'fsForm',
      title: _T_LAYER_WMS('fields.titleLayerProperties'),
      columnWidth: 0.5,
      margin: '0 0 0 4',
      padding: '5 10 5 10',

      height: heightLayerList,
      scrollable: 'y',
      items: []
    }];

    this.callParent(arguments);
  },

  loadRecord: function (record) {
    var ret = this.callParent(arguments);

    var components = ['msLayerData', 'layerServerUrl'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);

    components = ['LayerSavedConnections', 'msLayerConnectionName', 'btCallWMS', 'fsTree', 'fsForm'];
    if (VISIBILITY_WXS_MANAGE) {
      components.push('btAddWMS', 'btDeleteWMS');
    }

    extensions = [''];
    this.setStateFields(record, components, extensions, "hide");

    components = ['fcWmsInfo'];
    extensions = [''];
    this.setStateFields(record, components, extensions, "show");

    return ret;
  },

  /**
   * 
   * @param {type} jsonLayersDesc
   * @param {type} projectionFilter
   * @returns {Array}
   */
  convertNestedLayersToTree: function (nestedLayers, formats, version) {
    var res = [];

    for (var i = 0; i < nestedLayers.length; i++) {
      var nestedLayer = nestedLayers[i];

      var layer = {};
      layer.lname = nestedLayer.name;
      layer.text = nestedLayer.title;
      layer.minScale = Carmen.getFirstDefined(Math.round(Math.min(nestedLayer.minScale, nestedLayer.maxScale)), Carmen.user.preferences.preferenceMinscale, Carmen.Defaults.MINSCALE);
      layer.maxScale = Carmen.getFirstDefined(Math.round(Math.max(nestedLayer.minScale, nestedLayer.maxScale)), Carmen.user.preferences.preferenceMaxscale, Carmen.Defaults.MAXSCALE);
      layer.styles = nestedLayer.styles;
      layer.version = version;
      layer.formats = formats; // list of output format
      layer.bbox = nestedLayer.bbox;    // default srs
      layer.llbbox = nestedLayer.llbbox;  // extent

      // metadata url
      layer.metadataUrl = '';
      if (nestedLayer.metadataURLs instanceof Array &&
        nestedLayer.metadataURLs.length > 0) {
        layer.metadataUrl = nestedLayer.metadataURLs[0].href;
      }

      // list of srs
      layer.srs = [];
      if (nestedLayer.srs) {
        if (nestedLayer.srs instanceof Array &&
          nestedLayer.srs.length > 0) {
          for (var j = 0; j < nestedLayer.srs.length; j++) {
            layer.srs.push(nestedLayer.srs[j]);
          }
        }
        else {
          for (srsName in nestedLayer.srs) {
            if (nestedLayer.srs[srsName] == true)
              layer.srs.push(srsName);
          }
        }
      }

      // style & legends
      layer.styles = [];
      if (nestedLayer.styles) {
        if (nestedLayer.styles instanceof Array &&
          nestedLayer.styles.length > 0) {
          //for (var j=0; j<nestedLayer.styles.length; j++) {
          if (typeof (nestedLayer.styles[0].legend) != "undefined") {
            var legend = {
              //name  : nestedLayer.styles[j].name,
              title: nestedLayer.styles[0].title,
              format: nestedLayer.styles[0].legend.format,
              href: nestedLayer.styles[0].legend.href,
              width: nestedLayer.styles[0].legend.width,
              height: nestedLayer.styles[0].legend.height
            };
            layer.styles.push(legend);
          }
          //}
        }
      }


      // children
      if (nestedLayer.nestedLayers && nestedLayer.nestedLayers.length > 0) {
        layer.children = this.convertNestedLayersToTree(nestedLayer.nestedLayers, formats, version);
        layer.leaf = false;
      } else {
        layer.children = [];
        layer.leaf = true;
      }

      //if (layer.children.length>0)
      res.push(layer);
    }
    return res;
  },

  /**
   * apply the params of the selected layer to generic sheet
   */
  btApplyAction: function (oBt, me) {
    var ctrlNames = [
      ["wmsTitle", "layerTitle"],
      ["layerWxsname", "layerName", true],
      ["layerWxsname", "layerIdentifier", true],
      ["wmsSrs", "layerProjectionEpsg"],
      "layerMinscale",
      "layerMaxscale"
    ];

    var availableSRS = me.down('[name=wmsSrs]').getStore().collect('valueField');
    var comboStore = me.down('comboboxprojections').getStore();
    comboStore.clearFilter();
    comboStore.filterBy(function (projection) {
      return availableSRS.indexOf(parseInt(projection.get('valueField'))) != -1;
    });

    for (var idx in ctrlNames) {
      var nameSrc = (typeof (ctrlNames[idx]) == "string" ? 'form' + ctrlNames[idx] : ctrlNames[idx][0]);
      var nameDest = (typeof (ctrlNames[idx]) == "string" ? ctrlNames[idx] : ctrlNames[idx][1]);
      var bConvert = (typeof (ctrlNames[idx]) != "string" && ctrlNames[idx].length == 3 ? ctrlNames[idx][2] : false);
      var ctrlSrc = me.down('[name=' + nameSrc + ']');
      var ctrlDest = me.down('[name=' + nameDest + ']');
      if (ctrlSrc && ctrlDest) {
        if (bConvert) {
          ctrlDest.setValue(convertNameToIdentifier(ctrlSrc.getValue(), 20));
        } else {
          ctrlDest.setValue(ctrlSrc.getValue());
        }
      }
    }
  },

  /**
   * build the layer form and return its reference
   * @param {object} selectedLayer
   * @returns {Array of Ext Components}
   */
  buildLayerForm: function (selectedLayer) {
    var me = this;

    // current selected projection
    var ctrlUrlSRS = me.down('[name=layerProjectionEpsg]');;
    var currentSRS = ctrlUrlSRS.getValue();
    var currentSRSExists = false;

    // data of srs store
    var storeProjections = Carmen.Dictionaries.PROJECTIONS;
    var srsData = [];
    var srs = selectedLayer.get("srs");
    for (var i = 0; i < srs.length; i++) {
      var srsr = srs[i].replace(/EPSG:|CRS:|urn:x-ogc:def:crs:EPSG:/, '');
      var record = storeProjections.findRecord('valueField', srsr);
      if (record) {
        srsData.push({ "valueField": srsr, "displayField": record.get('displayField') });
      } else {
        //srsData.push({"valueField": srsr, "displayField": srsr});
      }
      currentSRSExists = (currentSRSExists || (srsr == currentSRS) ? true : false);
    }
    if (!currentSRSExists && srs.length > 0) {
      currentSRS = srs[0].replace(/EPSG:|CRS:|urn:x-ogc:def:crs:EPSG:/, '');
    }

    // projections list from the selected layer
    var storeSRS = new Ext.data.JsonStore({
      fields: [{ name: 'valueField', type: 'number' }, { name: 'displayField' }],
      sorters: [{ property: 'valueField', direction: 'ASC' }],
      autoload: true,
      data: srsData,
      proxy: {
        type: 'memory'
      }
    });


    // formats outputs list from the selected layer
    var formatsData = selectedLayer.get("formats");
    var currentFormat = (formatsData.length > 0 ? formatsData[0].valueField : "");
    var storeFormats = new Ext.data.JsonStore({
      fields: [{ name: 'valueField' }, { name: 'displayField' }],
      autoload: true,
      data: formatsData,
      proxy: {
        type: 'memory'
      }
    });

    var items = [{
      xtype: 'fieldcontainer',
      layout: 'hbox',
      anchor: '98%',
      defaults: {
        margin: '0 8 0 0',
        labelWidth: 80,
        labelAlign: 'right'
      },
      items: [{
        xtype: 'displayfield',
        value: '',
        flex: 4
      }, {
        xtype: 'button',
        name: 'btApply',
        text: _T_LAYER_WMS('fields.btApply'),
        flex: 1,
        handler: function (oThis) {
          me.btApplyAction(oThis, me);
        }
      }]
    }, {
      xtype: 'hiddenfield',
      name: "layerServerVersion",
      value: selectedLayer.get("version")
    }, {
      xtype: 'displayfield',
      name: "layerWxsname",
      fieldLabel: _T_LAYER_WMS('fields.layerName'),
      value: selectedLayer.get("lname")
    }, {
      xtype: 'displayfield',
      name: "wmsTitle",
      fieldLabel: _T_LAYER_WMS('fields.layerTitle'),
      value: selectedLayer.get("text")
    }, {
      xtype: 'combobox',
      name: 'wmsSrs',
      width: '95%',
      fieldLabel: _T_LAYER_WMS('fields.layerSRS'),
      queryMode: 'local',
      displayField: 'displayField',
      valueField: 'valueField',
      store: storeSRS,
      autoLoadOnValue: true,
      autoSelect: true,
      editable: false,
      value: currentSRS
    }, {
      xtype: 'combobox',
      name: 'layerOutputformat',
      fieldLabel: _T_LAYER_WMS('fields.layerOutput'),
      queryMode: 'local',
      displayField: 'displayField',
      valueField: 'valueField',
      store: storeFormats,
      autoLoadOnValue: true,
      autoSelect: true,
      editable: false,
      value: currentFormat
    }, {
      xtype: 'displayfield',
      name: 'formlayerMinscale',
      fieldLabel: _T_LAYER_WMS('fields.layerMinscale'),
      value: selectedLayer.get("minScale")
    }, {
      xtype: 'displayfield',
      name: 'formlayerMaxscale',
      fieldLabel: _T_LAYER_WMS('fields.layerMaxscale'),
      value: selectedLayer.get("maxScale")
    }];

    var styles = selectedLayer.get("styles");
    for (var j = 0; j < styles.length; j++) {
      var item = {
        xtype: 'displayfield',
        name: 'formlayerLegend_' + j,
        fieldLabel: _T_LAYER_WMS('fields.layerLegend'),
        value: (styles[j].title != "default" ? styles[j].title + '<br/>' : '') +
          styles[j].format + ' (' + styles[j].width + 'px, ' + styles[j].height + 'px)'
      };
      items.push(item);
      var itemImg = {
        xtype: 'image',
        padding: '0 0 8 24',
        name: 'formlayerLegendImg_' + j,
        src: styles[j].href,
        width: styles[j].width,
        height: styles[j].height
      };
      items.push(itemImg);
    }
    return items;
  },

  /**
   * Destroy all items of the component identified by its name
   * @param {string} name  name of component
   * @return {Component}
   */
  destroyItemsComponent: function (name) {
    var me = this;
    var idx = me.items.findIndex("name", name);
    var component = (idx >= 0 ? me.items.getAt(idx) : null);
    if (component) {
      while (component.items.length > 0) {
        component.items.getAt(0).destroy();
      }
    }
    return component;
  },

  /**
   * build the layers tree and return its reference
   * @param {objet} rootTree
   * @param {int}   height
   * @param {int}   width
   * @returns {Ext.tree.TreePanel}
   */
  buildLayersTree: function (rootTree) {
    var me = this;
    var tree = new Ext.tree.TreePanel({
      animate: true,
      width: "auto",
      name: "layersTree",
      hideHeaders: true,
      viewConfig: {
        emptyText: _T_LAYER_WMS('treeEmptyText')
      },
      root: rootTree,
      selModel: new Ext.selection.TreeModel({
        listeners: {
          selectionchange: function (selection, data) {
            var oFsForm = me.destroyItemsComponent("fsForm");
            if (oFsForm) {
              var itemsLayerForm = me.buildLayerForm(data[0]);
              for (var i = 0; i < itemsLayerForm.length; i++) {
                oFsForm.add(itemsLayerForm[i]);
              }
            }
          }
        }
      })
    });
    return tree;
  },

  /**
   * parses the response of getCapabilities and writes the tree of layers
   * @param {string} response
   */
  parseWMSCapabilities: function (response) {
    var wmsCpblt = new OpenLayers.Format.WMSCapabilities();
    var capabilities = wmsCpblt.read(response.responseText);

    if (capabilities.capability && capabilities.capability.nestedLayers) {
      var nestedLayers = capabilities.capability.nestedLayers;

      var formats = [];
      for (var i = 0; i < capabilities.capability.request.getmap.formats.length; i++) {
        var formatValue = capabilities.capability.request.getmap.formats[i];
        formats.push({ "valueField": formatValue, "displayField": formatValue });
      }

      var layersTree = this.convertNestedLayersToTree(nestedLayers, formats, capabilities.version);
      if (layersTree.length > 0) {
        var rootTree = {
          expanded: true,
          text: capabilities.service.title,
          service: capabilities.service.href,
          abstract: capabilities.service.abstract,
          version: capabilities.version,
          children: layersTree[0].children,
          //leaf        : false
          lname: layersTree[0].lname,
          minScale: Math.round(Math.min(layersTree[0].minScale, layersTree[0].maxScale)),
          maxScale: Math.round(Math.max(layersTree[0].minScale, layersTree[0].maxScale)),
          styles: layersTree[0].styles,
          formats: layersTree[0].formats, // list of output format
          bbox: layersTree[0].bbox,    // default srs
          llbbox: layersTree[0].llbbox,
          srs: layersTree[0].srs,
          metadataUrl: layersTree[0].metadataUrl,
          leaf: layersTree[0].leaf
        };

        this.destroyItemsComponent("fsForm");
        var oFsTree = this.destroyItemsComponent("fsTree");
        if (oFsTree) {
          var layersTree = this.buildLayersTree(rootTree);
          oFsTree.add(layersTree);
        }
      }
    }
  },

  /**
   * Launch the getCapabilities action of openlayer 
   */
  launchGetCapabilities: function () {
    var ctrlUrl = this.down('[name=layerServerUrl]');
    if (!ctrlUrl.isValid()) return;
    var url = ctrlUrl.getValue();
    var me = this;

    Ext.MessageBox.show({
      msg: _T_LAYER_WMS('msgApplied.progressTitle'),
      progressText: _T_LAYER_WMS('msgApplied.progressMsg'),
      width: 300,
      wait: true,
      waitConfig: { interval: 200 }
    });

    OpenLayers.ProxyHost = Routing.generate('carmen_ws_get_layer_information') + "/";

    url = url.split('?');
    var params = Ext.Object.fromQueryString((url.length > 1 ? url[1] : ""));
    url = url[0];
    // launching getCapabilities request
    OpenLayers.Request.GET({
      url: encodeURIComponent(url),
      params: Ext.apply(params, {
        SERVICE: "WMS",
        REQUEST: "GetCapabilities"
      }),
      success: function (response) {
        Ext.MessageBox.hide();
        me.parseWMSCapabilities(response);
      },
      failure: function (response) {
        if (Carmen.notAuthenticatedException(response)) return;
        Ext.MessageBox.hide();
        var oRes = eval("(" + response.responseText + ")");
        var msgKey = "getCapabilities";
        Ext.MessageBox.alert(_T_LAYER_WMS('msgFailure.' + msgKey), (oRes.description || _T_LAYER_WMS('msgFailure.' + msgKey)))
          .setIcon(Ext.MessageBox.ERROR);
      }
    });
  }

});
