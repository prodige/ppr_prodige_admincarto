var _T_LAYER_PGSQL = function (key) {
  return _t("Layer.forms.GTabGeojson." + key);
};

/**
 * Carmen.Model.LayerGeojson
 */
Ext.define('Carmen.Model.LayerGeojson', {
  extend: 'Carmen.Model.LayerVectorial',
  idProperty: 'layerId',
  fields: [
    { name: "layerType", type: "string", defaultValue: 'GEOJSON', convert: function (value) { if (Ext.isObject(value)) return value.layerTypeCode; return value; } },

    { name: "msLayerConnectionType", type: "string", defaultValue: "GEOJSON" }, // mapfile data : CONNECTIONTYPE

    { name: "msLayerPgSchema", type: "string" }, // mapfile data : DATA (SCHEMA_NAME part)
    { name: "msLayerPgTable", type: "string" }, // mapfile data : DATA (TABLE_NAME part)
    { name: "msLayerPgGeometryField", type: "string" }, // mapfile data : DATA (THE_GEOM part)
    { name: "msLayerPgIdField", type: "string" }, // mapfile data : DATA (GID part)
    { name: "msLayerPgProjection", type: "string" } // mapfile data : DATA (SRID part)

  ]
});

/**
 * Generic tab for type of GEOJSON layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabGeojson', {
  extend: 'Carmen.Generic.Form',
  name: 'GTabGeojson',
  title: _T_LAYER_PGSQL('title'),
  height: 'auto',
  layout: 'column',
  otherTabs: ['TabStyle','TabSymbologie', 'TabField', 'TabMeta', 'TabOGC'],
  modelName: 'Carmen.Model.LayerGeojson',
  defaults: {
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },

  // methods
  initComponent: function () {
    var me = this;

    // items on this panel
    me.items = [{
      xtype: 'fieldset',
      layout: 'form',
      padding: 0,
      margin: 0,
      border: false,
      items: [{
        xtype: 'textfield',
        name: 'layerTitle',

        fieldLabel: _T_LAYER_PGSQL('fields.title'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_PGSQL('fields.name'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [registerWarningOnIdentifierLength({
          xtype: 'textfield',
          name: 'layerName',
          allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          vtype: 'identifier',
          flex: 5
        }), {
          xtype: 'button',
          name: 'layerName_button',
          text: _T_LAYER_PGSQL('fields.auto'),
          margin: '0 10px 0 0',
          /**
           * auto calculation of layerName field
           */
          handler: function () {
            var ctrlFrom = me.down('[name=layerTitle]');
            var ctrlTo = me.down('[name=layerName]');
            if (ctrlFrom && ctrlTo) {
              ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
            }
          },
          flex: 1
        }, {
          labelAlign: 'right',
          xtype: 'textfield',
          name: 'layerIdentifier',
          fieldLabel: _T_LAYER_PGSQL('fields.ident'),
          allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          vtype: 'identifier',
          flex: 5
        }, {
          xtype: 'button',
          name: 'layerIdentifier_button',
          text: _T_LAYER_PGSQL('fields.auto'),
          /**
           * auto calculation of layerName field
           */
          handler: function () {
            var ctrlFrom = me.down('[name=layerTitle]');
            var ctrlTo = me.down('[name=layerIdentifier]');
            if (ctrlFrom && ctrlTo) {
              ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
            }
          },
          flex: 1
        }]
      },{
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_PGSQL('fields.proj'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          allowBlank: false,
          xtype: 'comboboxprojections',
          name: 'layerProjectionEpsg',
          editable: false,
          value: Carmen.Defaults.PROJECTION,
          margin: '0 10px 0 0',
          flex: 6
        }, {
          labelAlign: 'right',
          xtype: 'combobox',
          name: 'msGeometryType',
          fieldLabel: _T_LAYER_PGSQL('fields.type'),
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.GEOMETRYTYPES,
          autoLoadOnValue: true,
          autoSelect: true,
          editable: false,
          flex: 6
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_PGSQL('fields.scales'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'numberfield',
          name: 'layerMinscale',
          labelWidth: 60,
          labelAlign: 'right',
          fieldLabel: _T_LAYER_PGSQL('fields.min'),
          allowBlank: false,
          maxLength: 10,
          enforceMaxLength: true,
          minValue: Carmen.Defaults.MINSCALE,
          maxValue: Carmen.Defaults.MAXSCALE,
          hideTrigger: true,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          allowOnlyWhitespace: false,
          allowDecimals: false,
          allowExponential: false,
          flex: 6,
          margin: '0 10px 0 0'
        }, {
          labelAlign: 'right',
          xtype: 'numberfield',
          name: 'layerMaxscale',
          vtype: '',
          fieldLabel: _T_LAYER_PGSQL('fields.max'),
          allowBlank: false,
          maxLength: 10,
          enforceMaxLength: true,
          minValue: Carmen.Defaults.MINSCALE,
          maxValue: Carmen.Defaults.MAXSCALE,
          hideTrigger: true,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          allowOnlyWhitespace: false,
          allowDecimals: false,
          allowExponential: false,
          flex: 6
        }]
      },{
        xtype: 'displayfield',
        fieldLabel: _T_LAYER_PGSQL('fields.geojsonUrl'),
        name: 'layerGeojsonUrl',
        flex: 6
      },]
    }, {
      xtype: 'fieldset',
      title: _t("Layer.window.titleLayerBackground"), //"Paramétrages d'affichages de la couche",
      layout: 'form',
      items: [{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'checkbox',
          name: 'layerIsBackground',
          fieldLabel: _t("Layer.forms.fields.layerIsBackground"),
          flex: 6,
          labelWidth: '80%',
          listeners: [{
            change: function (me, value) {
              var show_component = false;
              var is_checked = value;
              if (is_checked) {
                show_component = true;
              }

              var backgroundDisplayImage = Ext.ComponentQuery.query('[name=backgroundDisplayImage]')[0];
              backgroundDisplayImage.setVisible(show_component);
            }
          }]
        }, {
          xtype: 'checkbox',
          name: 'layerIsDefaultBackground',
          fieldLabel: _t("Layer.forms.fields.layerIsDefaultBackground"),
          flex: 6,
          labelWidth: '80%',
          listeners: [{
            change: function (me, value) {

              var mainFrame = Ext.ComponentQuery.query('[name=GTabGeojson]')[0];
              var mainFrameRecord = mainFrame.form._record;
              var layerId = mainFrameRecord.data.layerId;
              var joinedStore = mainFrameRecord.joined;

              for (var i = 0; i < joinedStore.length; i++) {
                var current_store = joinedStore[i];
                if (current_store.config.model === "Carmen.Model.Layer") {
                  for (var i = 0; i < current_store.data.items.length; i++) {
                    var current_layer = current_store.data.items[i];
                    if (current_layer.id !== layerId) {
                      current_layer.data.layerIsDefaultBackground = false;
                    }
                  }
                }
              }
            }
          }]
        }]
      }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        hidden: true,
        name: "backgroundDisplayImage",
        items: [{

          fieldLabel: _t("Layer.forms.fields.layerBackgroundImage"),
          name: 'layerBackgroundImage',
          xtype: 'textfield',
          labelWidth: '40%',
          width: Carmen.Util.WINDOW_WIDTH - 90,
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.LOGOPATH_IMAGES,
          editable: false,
          autoLoadOnValue: true,
          autoSelect: true,
          flex: 8
        }, {
          xtype: 'button',
          text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
          name: 'uiLogopath_button',
          flex: 3,
          handler: function () {
            var windowClass = "Ext.ux.FileBrowserWindow";
            var window = Ext.getCmp(CMP_REFERENCE(windowClass))
              || Ext.create(windowClass, {
                id: CMP_REFERENCE(windowClass),
                selectExtension: '*.jpg,*.gif,*.png',
                uploadExtension: '.jpg,.gif,.png',
                dataUrl: Routing.generate('carmen_ws_filebrowser', { routing: 'noroute' }),
                defaultPath: '/Root',
                relativeRoot: '/Root/IHM',
                listeners: {
                  selectedfile: function (wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                    var ctrlTo = me.down('[name=layerBackgroundImage]');
                    ctrlTo.setValue(fullPathName);
                    wind.close();
                  }
                }
              });
            window.show();
          },

          listeners: {
            afterrender: function () {
              var check = me.down('[name=uiLogo]');
              if (check) { check.fireEvent('change', check); }
            }
          }
        }]
      }]
    }, {
      xtype: 'fieldset',
      title: _T_LAYER_PGSQL('fields.streamParam'),
      layout: 'form',
      items: [{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'displayfield',
          fieldLabel: _T_LAYER_PGSQL('fields.pgDbName'),
          value: Carmen.user.account.get("accountDbpostgres"),
          flex: 6
        }, {
          labelAlign: 'right',
          xtype: 'displayfield',
          fieldLabel: _T_LAYER_PGSQL('fields.pgHost'),
          value: Carmen.user.account.get("accountHostpostgres"),
          flex: 6
        }]
      }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'displayfield',
          fieldLabel: _T_LAYER_PGSQL('fields.pgUser'),
          value: Carmen.user.account.get("accountUserpostgres"),
          flex: 6
        }, {
          labelAlign: 'right',
          xtype: 'displayfield',
          fieldLabel: _T_LAYER_PGSQL('fields.pgPort'),
          value: Carmen.user.account.get("accountPortpostgres"),
          flex: 6
        }]
      }]
    }, {
      xtype: 'fieldset',
      title: _T_LAYER_PGSQL('fields.titleLayerSelect'),
      columnWidth: 1,
      items: [{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'displayfield',
          name: 'msLayerPgSchema',
          fieldLabel: _T_LAYER_PGSQL('fields.pgSchema'),
          flex: 6
        }, {
          xtype: 'displayfield',
          name: 'msLayerPgTable',
          fieldLabel: _T_LAYER_PGSQL('fields.pgTable'),
          flex: 6
        }]
      }]
    }, {
      xtype: 'fieldset',
      title: _T_LAYER_PGSQL('fields.titleLayerProperties'),
      columnWidth: 1,
      items: [{
        xtype: 'fieldcontainer',
        fieldLabel: '',
        labelAlign: 'top',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'displayfield',
          name: 'msLayerPgGeometryField',
          fieldLabel: _T_LAYER_PGSQL('fields.pgGeometry'),
          flex: 6
        }, {
          xtype: 'displayfield',
          name: 'msLayerPgIdField',
          fieldLabel: _T_LAYER_PGSQL('fields.pgId'),
          flex: 6
        }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: '',
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
          xtype: 'hiddenfield',
          name: 'msLayerPgProjection'
        }, {
          xtype: 'displayfield',
          name: 'msLayerPgProjectionDisplay',
          fieldLabel: _T_LAYER_PGSQL('fields.pgProj'),
          flex: 6
        }, {
          labelAlign: 'right',
          xtype: 'displayfield',
          name: 'msLayerPgType',
          hidden: true,
          fieldLabel: _T_LAYER_PGSQL('fields.pgType'),
          flex: 6
        }]
      }]
    }];

    this.callParent(arguments);
  },

  loadRecord: function (record) {
    var ret = this.callParent(arguments);
    var components = ['msLayerData'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);

    var ctrlPgProj = this.down('[name=msLayerPgProjection]');
    var ctrlPgProjD = this.down('[name=msLayerPgProjectionDisplay]');
    var ctrlPgType = this.down('[name=msLayerPgType]');
    var ctrPgSchema = this.down('[name=msLayerPgSchema]');
    var ctrPgTable = this.down('[name=msLayerPgTable]');

    if (ctrlPgProj)
      ctrlPgProj.setValue(record.get("msLayerPgProjection"));
    if (ctrlPgProjD)
      ctrlPgProjD.setValue(record.get("msLayerPgProjection"));
    if (ctrlPgType)
      ctrlPgType.setValue(record.get("msLayerPgType"));

    return ret;
  }

});
