var _T_LAYER_WMSC = function (key) {
  return _t("Layer.forms.GTabWMSC." + key);
};

Ext.ns('Carmen.WMSC');
Ext.define('Carmen.WMSC.RecordType', {
  extend : 'Ext.data.TreeModel',
  fields : ['boundingbox', 'format', 'height', 'width', 'layers', 'resolutions', 'srs', 'styles'
  , 'id', 'name', 'text'
  , {name:'projection', depends:'srs', convert:function(value, record){var v = String(record.get('srs')).split(':'); return v[v.length-1];}}
  , {name:'outputformat', depends:'format', convert:function(value, record){var v = String(record.get('format')).split('/'); return String(v[v.length-1]).toUpperCase();}}
  ]
})
/**
 * Carmen.Model.LayerWMSC
 */
Ext.define('Carmen.Model.LayerWMSC', {
  extend: 'Carmen.Model.LayerVectorial', //LayerRasterized ?
  idProperty: 'layerId',
  fields: [
    {name: "layerType",   type: "string", defaultValue: 'WMSC', convert : function(value){if ( Ext.isObject(value) ) return value.layerTypeCode; return value;}},

    {name: "layerTiledLayer",      type: "string"},
    {name: "layerTiledServerUrl",      type: "string"},
    {name: "layerTiledResolutions",      type: "string"},
    
    {name: "layerWmscBoundingbox",  type: "string"},

    {name: "LayerSavedConnections", type: "string", persist: false},
    {name: "msLayerConnectionName", type: "string"},                      // mapfile data : NAME
    {name: "msLayerConnectionType", type: "string", defaultValue: "WMSC"}, // mapfile data : CONNECTIONTYPE
    
    {name: "wmscTitle", type: "string"},
    {name: "wmscSrs",   type: "string"}
    
  ]
});

/**
 * Generic tab for type of WMSC layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabWMSC', {
  extend: 'Carmen.Generic.Form',
  name : 'GTabWMSC', 
  title: _T_LAYER_WMSC('title'),
  frame: true,
  height: 'auto',
  layout: 'column',
  otherTabs: ['TabStyle'],
  modelName: 'Carmen.Model.LayerWMSC',
  defaults:{
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },
  
  // methods
  initComponent: function() {
    var me = this;

    /**
     * @mode remote
     * @table : wxs (wxs_type=WMSC)
     */
    me.storeService = new Ext.data.XmlStore({
      storeId : 'SAVED_WMSC_SERVICES',
      autoLoad : true,
      fields : [
        {name : 'valueField', mapping : 'wxsId'},
        {name : 'displayField', mapping : 'wxsName'}
      ],
      proxy: {
        type: 'rest',
        url: Routing.generate('carmen_ws_get_saved_services')+"/WMSC"
      }
    });

    // items on this panel
    me.items = [{
        xtype:'fieldset',
        layout: 'form',
        padding: 0,
        margin: 0,
        border: false,
        items: [{
          xtype: 'textfield',
          name: 'layerTitle',

          fieldLabel: _T_LAYER_WMSC('fields.title'),
          allowBlank: false,
          maxLength: 255,
          enforceMaxLength: true,
          flex: 12
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_LAYER_WMSC('fields.name'),
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 20,
          items: [{
              xtype: 'textfield',
              name: 'layerName',
              allowBlank: false,
              maxLength: 20,
              enforceMaxLength: true,
              /*  recommended that the name not contain spaces, special characters, or begin with a number */
              vtype: 'identifier',
              flex: 5
            }, {
              xtype: 'button',
              name : 'layerName_button',
              text: _T_LAYER_WMSC('fields.auto'),
              margin: '0 10px 0 0',
              /**
               * auto calculation of layerName field
               */
              handler: function() {
                var ctrlFrom = me.down('[name=layerTitle]');
                var ctrlTo = me.down('[name=layerName]');
                if ( ctrlFrom && ctrlTo ) {
                  ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
                }hand
              },
              flex: 1
            }, {
              xtype: 'textfield',
              name: 'layerIdentifier',
              fieldLabel: _T_LAYER_WMSC('fields.ident'),
              allowBlank: false,
              /*  recommended that the name not contain spaces, special characters, or begin with a number */
              vtype: 'identifier',
              flex: 5
            }, {
              xtype: 'button',
              name : 'layerIdentifier_button',
              text: _T_LAYER_WMSC('fields.auto'),
              /**
               * auto calculation of layerName field
               */
              handler: function() {
                var ctrlFrom = me.down('[name=layerTitle]');
                var ctrlTo = me.down('[name=layerIdentifier]');
                if ( ctrlFrom && ctrlTo ) {
                  ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
                }
              },
              flex: 1
            }]
        }, {
          xtype: 'combobox',
          name: 'layerProjectionEpsg',
          fieldLabel: _T_LAYER_WMSC('fields.proj'),
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.PROJECTIONS,
          autoLoadOnValue: true,
          autoSelect: true,
          value: Carmen.Defaults.PROJECTION,
          editable: false
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_LAYER_WMSC('fields.scales'),
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 20,
          items: [{
              xtype: 'numberfield',
              name: 'layerMinscale',
              fieldLabel: _T_LAYER_WMSC('fields.min'),
              allowBlank: false,
              maxLength: 10,
              enforceMaxLength: true,
              minValue: 0,
              hideTrigger: true,
              keyNavEnabled: false,
              mouseWheelEnabled: false,
              allowOnlyWhitespace: false,
              allowDecimals: false,
              allowExponential: false,
              flex: 6,
              margin: '0 10px 0 0'
            }, {
              xtype: 'numberfield',
              name: 'layerMaxscale',
              vtype: '',
              fieldLabel: _T_LAYER_WMSC('fields.max'),
              allowBlank: false,
              maxLength: 10,
              enforceMaxLength: true,
              minValue: 0,
              hideTrigger: true,
              keyNavEnabled: false,
              mouseWheelEnabled: false,
              allowOnlyWhitespace: false,
              allowDecimals: false,
              allowExponential: false,
              flex: 6
            }]
        }, {
        xtype:'fieldset',
        title: _t("Layer.window.titleLayerBackground"), //"Paramétrages d'affichages de la couche",
        layout: 'form',
        items : [{
          xtype: 'fieldcontainer',
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [{
              xtype: 'checkbox',   
              name : 'layerIsBackground',
              fieldLabel: _t("Layer.forms.fields.layerIsBackground"),
              flex: 6, 
              labelWidth : '80%',
              listeners : [{
                      change: function (me, value) {
                          var show_component = false;
                          var is_checked = value; 
                          if(is_checked) {
                              show_component = true;
                          }
                          
                          var backgroundDisplayImage = Ext.ComponentQuery.query('[name=backgroundDisplayImage]')[0]; 
                          backgroundDisplayImage.setVisible(show_component); 
                      }
                }]
          },{
              xtype: 'checkbox',   
              name : 'layerIsDefaultBackground',
              fieldLabel: _t("Layer.forms.fields.layerIsDefaultBackground"),
              flex: 6,
              labelWidth : '80%',
              listeners : [{
                      change: function (me, value) {
                          
                          var mainFrame = Ext.ComponentQuery.query('[name=GTabWMSC]')[0]; 
                          var mainFrameRecord = mainFrame.form._record; 
                          var layerId = mainFrameRecord.data.layerId; 
                          var joinedStore = mainFrameRecord.joined; 
                          
                          for(var i = 0 ; i < joinedStore.length; i++ ) {
                              var current_store = joinedStore[i];                               
                              if(current_store.config.model === "Carmen.Model.Layer") {                                 
                                  for(var i = 0 ; i < current_store.data.items.length; i++ ) {
                                      var current_layer = current_store.data.items[i]; 
                                      if(current_layer.id !== layerId ) {
                                          current_layer.data.layerIsDefaultBackground = false; 
                                      }
                                  }
                              }
                          }
                      }
              }]
          }]
        },{
              xtype: 'fieldcontainer',
              layout: 'hbox',
              width: Carmen.Util.WINDOW_WIDTH - 40,
              hidden : true  ,
              name : "backgroundDisplayImage",
              items: [{
                    
                    fieldLabel: _t("Layer.forms.fields.layerBackgroundImage"),
                    name : 'layerBackgroundImage',
                    xtype: 'textfield',
                    labelWidth : '40%',
                    width: Carmen.Util.WINDOW_WIDTH - 90,
                    queryMode: 'local',
                    displayField: 'displayField',
                    valueField: 'valueField',
                    store: Carmen.Dictionaries.LOGOPATH_IMAGES,
                    editable: false,
                    autoLoadOnValue : true,
                    autoSelect : true,
                    flex : 8
              }, { 
                    xtype: 'button',
                    text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
                    name : 'uiLogopath_button',
                    flex : 3    ,
                    handler: function(){
                      var windowClass = "Ext.ux.FileBrowserWindow";
                      var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
                      || Ext.create(windowClass, {
                         id: CMP_REFERENCE(windowClass),
                         selectExtension: '*.jpg,*.gif,*.png',
                         uploadExtension: '.jpg,.gif,.png',
                         dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                         defaultPath: '/Root',
                         relativeRoot: '/Root/IHM',
                         listeners: {  
                           selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath)  {
                             var ctrlTo = me.down('[name=layerBackgroundImage]');
                             ctrlTo.setValue(fullPathName);
                             wind.close();
                           }
                         }
                      });
                      window.show();
                    },

                    listeners : {
                      afterrender : function(){
                        var check = me.down('[name=uiLogo]');
                        if ( check ){check.fireEvent('change', check);}
                      }
                    }
                  }]
          }]
      }, {
          xtype: 'hiddenfield',
          name: "layerWxsname"
        }, {
          xtype: 'hiddenfield',
          name: "layerTiledResolutions"
        }, {
          xtype: 'hiddenfield',
          name: "layerOutputformat"
        }, {
          xtype: 'hiddenfield',
          name: "layerTiledServerUrl"
        }, {
          xtype: 'hiddenfield',
          name: "layerTiledLayer"
        }, {
          xtype: 'hiddenfield',
          name: "layerWmscBoundingbox"
        }]
      }, {
        xtype:'fieldset',
        title: _T_LAYER_WMSC('fields.streamParam'),
        margins: 8,
        
        layout: 'form',
        items: [{
          xtype: 'combobox',
          name: 'LayerSavedConnections',
          fieldLabel: _T_LAYER_WMSC('fields.savedServ'),
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: me.storeService,
          autoLoadOnValue: true,
          autoSelect: true,
          editable: false,
          listeners: {
            /**
             * update fields : msLayerConnectionName and layerServerUrl with values contents in selected record
             * @param {Ext.form.field.ComboBox} combo
             * @param {Ext.data.Model} record
             */
            select: function(combo, record) {
              var ctrlName = me.down('[name=msLayerConnectionName]');
              var ctrlUrl  = me.down('[name=layerServerUrl]');
              if ( ctrlName && ctrlUrl ) {
                ctrlName.setValue(record.get("wxsName"));
                ctrlUrl.setValue(record.get("wxsUrl"));
              }
            }  
          }

        }, {
          xtype: 'textfield',
          name: 'msLayerConnectionName',
          fieldLabel: _T_LAYER_WMSC('fields.nameServ')
          //allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          //vtype: 'identifier'
        }, {
          xtype: 'textfield',
          name: 'layerServerUrl',
          fieldLabel: _T_LAYER_WMSC('fields.urlServ'),
          allowBlank: false,
          vtype: 'url',
          listeners: {
            change: function(oThis, newValue, oldValue) {
              var btCall = me.down('[name=btCallWMSC]');
              if( btCall ) {
                btCall.setDisabled(newValue != "" ? false : true);
              }
            }
          }
        }, {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          anchor: '100%',
          defaults: {
            margin: '0 8 0 0'
          },
          items: [{
            xtype: 'button',
            name : 'btCallWMSC',
            text: _T_LAYER_WMSC('fields.btCallWMSC'),
            disabled: true,
            /**
             * call current URL to load wmsc context
             */
            handler: function() {
              me.launchGetCapabilities();
            },
            flex: 2
          }, {
            xtype: 'button',
            name : 'btAddWMSC',
            hidden : (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WMSC('fields.btAddWMSC'),
            /**
             * add current connection name and url to wmsc saved services
             */
            handler: function() {
              var ctrlList = me.down('[name=LayerSavedConnections]');
              var ctrlName = me.down('[name=msLayerConnectionName]');
              var ctrlUrl  = me.down('[name=layerServerUrl]');
              if ( ctrlName && ctrlUrl && ctrlList ) {
                if ( !void(ctrlName.allowBlank = false) && ( !ctrlName.isValid() || !ctrlUrl.isValid() ) && !void(ctrlName.allowBlank = true)) 
                  return;
                ctrlName.allowBlank = true;
                
                Ext.Ajax.request({
                  url: Routing.generate('carmen_ws_post_saved_services')+"/WMSC",
                  method: 'POST',
                  params: {
                    wxsName: ctrlName.getValue(),
                    wxsUrl: ctrlUrl.getValue()
                  },
                  success: function (response) {
                    var oRes = eval("("+response.responseText+")");
                    Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMSC('msgApplied.addWMSCSavedServ'));
                    ctrlList.getStore().reload();
                    // select added service
                    ctrlList.setValue(oRes.wxsId);
                  },
                  failure: function (response) {
                    var oRes = eval("("+response.responseText+")");
                    var msgKey = "addWMSCSavedServ";
                    Ext.MessageBox.alert(_T_LAYER_WMSC('msgFailure.'+msgKey), (oRes.description || _T_LAYER_WMSC('msgFailure.'+msgKey)))
                                  .setIcon(Ext.MessageBox.ERROR);
                  }
                });
              }
            },
            flex: 2
          }, {
            xtype: 'button',
            name : 'btDeleteWMSC',
            hidden : (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WMSC('fields.btDeleteWMSC'),
            /**
             * remove from wfs saved services, the current connection name
             */
            handler: function() {
              var ctrlList = me.down('[name=LayerSavedConnections]');
              var ctrlName = me.down('[name=msLayerConnectionName]');
              var ctrlUrl  = me.down('[name=layerServerUrl]');
              if ( ctrlName && ctrlUrl && ctrlList ) {
                Ext.Msg.confirm(
                  new Ext.XTemplate(_t("form.confirmDeleteTitle")).apply(),
                  new Ext.XTemplate(_T_LAYER_WMSC("msgApplied.delWMSCConfirm")).apply(),
                  function(btn){
                    if (btn=="yes"){
                    
                      Ext.Ajax.request({
                        url: Routing.generate('carmen_ws_delete_saved_services')+"/WMSC",
                        method: 'DELETE',
                        params: {
                          wxsId: ctrlList.getSelection().getData().wxsId
                        },
                        success: function (response) {
                          Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WMSC('msgApplied.delWMSCSavedServ'));
                          ctrlName.setValue("");
                          ctrlUrl.setValue("");
                          ctrlList.setValue("");
                          ctrlList.getStore().reload();
                        },
                        failure: function (response) {
                          var oRes = eval("("+response.responseText+")");
                          var msgKey = "delWMSCSavedServ";
                          Ext.MessageBox.alert(_T_LAYER_WMSC('msgFailure.'+msgKey), (oRes.description || _T_LAYER_WMSC('msgFailure.'+msgKey)))
                                        .setIcon(Ext.MessageBox.ERROR);
                        }
                      });
                    }
                  }
                ).icon = Ext.Msg.WARNING;
              }
            },
            flex: 2
          }]
        }]
      }, {
        xtype:'fieldset',
        name:'serverTree',
        title: _T_LAYER_WMSC('fields.titleLayersList'),
        columnWidth: 0.5,
        margin: '0 4 0 0',
        height: 350,
        padding: '0 0 0 0',
        scrollable: true,
        layout : 'form',
        items: []
      }, {
        fieldDefaults : {labelWidth : 100},
        xtype:'fieldset',
        name:'serverForm',
        title: _T_LAYER_WMSC('fields.titleLayerProperties'),
        columnWidth: 0.5,
        margin: '0 0 0 4',
        height: 350,
        padding: '10 10 10 10',
        scrollable: 'y',
        layout : 'form',
        items: []
    }];
    
    this.callParent(arguments);
  },
  
  
  /**
   * build the layer form and return its reference
   * @param {object} selectedLayer
   * @returns {Array of Ext Components}
   */
  buidLayerForm: function(selectedLayer) {
    var me = this;
    //console.log('buidLayerForm function : selectedLayer is : ',selectedLayer);



    var items = [
    {
      xtype: 'displayfield',
      name: "wmscName",
      fieldLabel: _T_LAYER_WMSC('fields.layerName'),
      value: selectedLayer.get("name")

    }, {
      xtype: 'displayfield',
      name: 'layer_outputformat',
      fieldLabel: _T_LAYER_WMSC('fields.layerFORMAT'),
      value : selectedLayer.get('format')
    }, {
      xtype: 'displayfield',
      name: 'layer_srs',
      fieldLabel: _T_LAYER_WMSC('fields.proj'),
      value : selectedLayer.get('srs')
    },
    

    {
      fieldLabel: _T_LAYER_WMSC('fields.layerTiledServerUrl'),
      xtype: 'textfield',
      name: 'layer_tiled_server_url',
      maxLength: 255,
      enforceMaxLength: true
    },
    {
      fieldLabel: _T_LAYER_WMSC('fields.layerTiledLayer'),
      columnWidth : 0.8,
      xtype: 'textfield',
      name: 'layer_tiled_layer',
      maxLength: 255,
      enforceMaxLength: true
    },
    {
      xtype : 'panel',
      padding : 0,
      layout : 'fit',
      buttons : [{
        xtype: 'button',
        name : 'btApply',
        text: _T_LAYER_WMTS('fields.btApply'),
        handler: function(oThis) {
          me.up('window').down('toolbar[dock="bottom"]:last').setDisabled(false);
          me.btApplyAction(selectedLayer);
        }
      }, {
        xtype: 'button',
        name : 'btCancel',
        text: _T_LAYER_WMTS('fields.btCancel'),
        handler: function(oThis) {
          me.up('window').down('toolbar[dock="bottom"]:last').setDisabled(false);
          me.destroyItemsComponent("serverForm").update(_T_LAYER_WMTS('serverForm.beforeLayer'));
          return false;
        }
      }]
    }
    ];

    me.up('window').down('toolbar[dock="bottom"]:last').setDisabled(true);

    return items;
  },

  loadRecord : function(record) {
    var ret = this.callParent(arguments);

    var components = ['layerName', 'layerIdentifier', 'msLayerData', 'layerServerUrl'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);
    
    components = ['LayerSavedConnections', 'msLayerConnectionName', 'btCallWMSC', 'serverTree', 'serverForm'];
    if(VISIBILITY_WXS_MANAGE){
      components.push('btAddWMSC', 'btDeleteWMSC'); 
    }
    
    extensions = [''];
    this.setStateFields(record, components, extensions, "hide");
    
    return ret;
  },
  
  /**
   * 
   * @param {Array} layersList
   * @param {String} tileMatrixSets
   * @param {String} version
   * @returns {Array} res
   */
  convertLayersListToTree : function(tileSets, version) {
    //var mapSRS = window.mapProjection;
    var res = {};
    var layers = [];

    for(var i=0; i<tileSets.length; i++) {
      var tileset = tileSets[i];
      
      var layer = Ext.apply({}, tileset);
      Ext.iterate(layer, function(key, value){
        layer[String(key).toLowerCase()] = value;
        delete(layer[key]);
      });
      layer.boundingbox = [
        layer.boundingbox.minx,
        layer.boundingbox.miny,
        layer.boundingbox.maxx,
        layer.boundingbox.maxy
      ].join(' ');
      layer.id = layer.layers+'_'+layer.srs;
      layer.text = layer.srs;
      layer.name = layer.layers;
      
      var name = layer.layers;
      
      layer.leaf = true;
      layer = Ext.create('Carmen.WMSC.RecordType', layer);
      res[name] = res[name] || [];
      res[name].push(layer);
      layers.push(name);
    }
    
    layers = Ext.Array.sort(Ext.Array.unique(layers), function(v1, v2){return v1<v2 ? -1 : 1});

    var tree = [];
    Ext.each(layers, function(layer){
      res[layer] = Ext.Array.sort(res[layer], function(v1, v2){return v1.text<v2.text ? -1 : 1});
      var children = res[layer];
      tree.push({
        text : layer,
        name : layer,
        expanded : true,
        leaf : false,
        children : children
      });
    })
    
    
    return tree;
  },

   /**
    * apply the params of the selected layer to generic sheet
    */
  btApplyAction: function(selectedLayer) {
    var me = this;
    var form = me.down('[name=serverForm]');

    if ( !form ) return;
    var isValid = true;
    form.query('field').every(function(field){
      isValid = isValid && field.validate();
      return isValid;
    });
    if ( !isValid ) return;
    
    
    var completeFn = function(combo){
      var record = combo.getSelection();
      var fields = Ext.pluck(record.getFields(), 'name');
      Ext.each(fields, function(field){
        if ( [combo.displayField, combo.valueField, 'projectionValue'].indexOf(field)==-1 ){
          var cmp = me.down('[name='+field+']');
          if ( cmp ){
            cmp.setValue(record.get(field));
          }
        }
      });
    };
    
    /*
        }*/
    var ctrlNames = {
      "layerTitle"          : ["wmscName"], 
      "layerWxsname"        : ["wmscName"],
      "layerName"           : ["wmscName", true],
      "layerIdentifier"     : ["wmscName", true], 
      "layerTiledServerUrl" : ["layer_tiled_server_url"],
      "layerTiledLayer"     : ["layer_tiled_layer"] ,
      "layerWmscBoundingbox"      : ["boundingbox", false], 
      "layerOutputformat"     : ["format"]  ,
      "layerProjectionEpsg"     : ["projection"] ,
      "layerTiledResolutions"     : ["resolutions"] 
    };
    
    Ext.iterate(ctrlNames, function(nameDest, configSrc){
      var nameSrc  = configSrc[0];
      var bConvert  = (configSrc.length>1 ? configSrc[1] : false);
      var callback  = (configSrc.length>2 ? configSrc[2] : null);
      
      var valueSrc = "undefined";
      var ctrlSrc  = me.down('[name='+nameSrc+']');
      if ( ctrlSrc ){
        valueSrc = ctrlSrc.getValue();
      } else {
        valueSrc = selectedLayer.get(nameSrc);
      }
      var ctrlDest = me.down('[name='+nameDest+']');
      if( valueSrc!="undefined" && ctrlDest ) {
        if( bConvert ) {
          ctrlDest.setValue(convertNameToIdentifier(valueSrc, 20));
        } else {  
          ctrlDest.setValue(valueSrc);
        }
        
        if ( callback )callback.call(me, ctrlSrc, ctrlDest);
      }
    });
    me.getMsGeometryType();
  },
  
  /**
   * Destroy all items of the component identified by its name
   * @param {string} name  name of component
   * @return {Component}
   */
  destroyItemsComponent: function(name) {
    var me = this;
    var component = me.down("[name="+name+"]");
    if( component ) {
      while(component.items.length>0 ) {
        component.items.getAt(0).destroy();
      }
      component.update('');
    }
    return component;
  },
  
  /**
   * build the layers tree and return its reference
   * @param {objet} rootTree
   * @param {int}   height
   * @param {int}   width
   * @returns {Ext.tree.TreePanel}
   */
  buildLayersTree: function(rootTree) {
    var me = this;
    var tree = new Ext.tree.TreePanel({
      animate:true,
      width: "auto",
      name: "layersTree",
      hideHeaders : true,
      viewConfig : {
        emptyText : _T_LAYER_WMSC('serverTree.emptyServer')
      },
      root: rootTree,
      selModel: new Ext.selection.TreeModel({
        listeners: {
          selectionchange: function (selection, data) {
            var serverForm = me.destroyItemsComponent("serverForm");
            if( serverForm ) {
              var itemsLayerForm = me.buidLayerForm(data[0]);
              for(var i=0; i<itemsLayerForm.length; i++) {
                serverForm.add(itemsLayerForm[i]);
              }
            }
          }
        }
      })
    });
    return tree;
  },
  
  /**
   * parses the response of getCapabilities and writes the tree of layers
   * @param {string} response
   */
  parseWMSCCapabilities: function(response) {
    var me = this;

    //format = new OpenLayers.Format.WMSCCapabilities({yx: {"urn:ogc:def:crs:EPSG::900913": true}}); // Prodige 3.4 
    var wmscCpblt = new OpenLayers.Format.WMSCapabilities();
    
    var capabilities = new OpenLayers.Format.XML().read(response.responseText);
    var service = capabilities;
    
    capabilities = capabilities.getElementsByTagName("VendorSpecificCapabilities");
    
    if( capabilities.length && (capabilities = me.xmlToJson(capabilities[0])) ){
      var tileSets = capabilities.TileSet

      this.destroyItemsComponent("serverForm").update(_T_LAYER_WMSC('serverForm.beforeLayer'));
      var serverTree = this.destroyItemsComponent("serverTree");
      if( serverTree ) {
        var layersTree = this.convertLayersListToTree(tileSets, capabilities.version);
        if( layersTree.length>0 ) {
          var rootTree = {
            expanded : true,
            text        : _T_LAYER_WMSC('serverTree.root'),
            abstract    : 'abstract',
            version     : service.version,
            children    : layersTree,
            lname       : "",
            srs         : "",
            leaf        : false
          };
        
          var layersTree = this.buildLayersTree(rootTree);
          serverTree.add(layersTree);
        } else {
          serverTree.update(_T_LAYER_WMSC('serverTree.emptyServer'));
        }
      }
    }
  },
  // Changes XML to JSON
  xmlToJson : function (xml) {
    var me = this;
  
    // Create the return object
    var obj = {};
  
    if (xml.nodeType == 1) { // element
      // do attributes
      if (xml.attributes.length > 0) {
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj[attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) { // text
      obj = xml.nodeValue;
    }
  
    // do children
    if (xml.hasChildNodes()) {
      for(var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof(obj[nodeName]) == "undefined") {
          obj[nodeName] = me.xmlToJson(item);
        } else {
          if (typeof(obj[nodeName].push) == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(me.xmlToJson(item));
        }
        var otherProps = false;
        for (var prop in obj[nodeName] ){
          otherProps = ( prop!="#text" );
          if ( otherProps ) break;
        }
        if ( !otherProps ){
          obj[nodeName] = obj[nodeName]["#text"];
        } else {
          delete obj[nodeName]["#text"];
        }
      }
    }
    return obj;
  },
  /**
   * Launch the getCapabilities action of openlayer 
   */
  launchGetCapabilities: function() {
    var ctrlUrl  = this.down('[name=layerServerUrl]');
    var wfsUrl = ctrlUrl.getValue();
    if ( !wfsUrl ) return;
    var me = this;
    
    Ext.MessageBox.show({ 
      msg: _T_LAYER_WMSC('msgApplied.progressTitle'), 
      progressText: _T_LAYER_WMSC('msgApplied.progressMsg'), 
      width:300, 
      wait:true, 
      waitConfig: {interval:200}
    });
    
    OpenLayers.ProxyHost = Routing.generate('carmen_ws_get_layer_information')+"/";
    var wfsUrl = decodeURIComponent(wfsUrl).split("?");
    var args = (wfsUrl.length>1 ? Ext.Object.fromQueryString(wfsUrl[1]) : {});
    wfsUrl = wfsUrl[0];
    // launching getCapabilities request
    OpenLayers.Request.GET({
      url: encodeURIComponent(wfsUrl),
      params: Ext.apply(args, {
        SERVICE: "WMS",
        VERSION: "1.1.1", // Warning : this version returns the epsg in the format epsg:xxxx
        REQUEST: "GetCapabilities"
      }),
      success: function(response) {
        Ext.MessageBox.hide();
        
        me.destroyItemsComponent("serverForm").update(_T_LAYER_WMSC('serverForm.beforeServer'));
        me.destroyItemsComponent("serverTree").update(_T_LAYER_WMSC('serverTree.beforeServer'));
        me.parseWMSCCapabilities(response);
      },
      failure: function(response) {
        Ext.MessageBox.hide();
        var oRes = eval("("+response.responseText+")");
        
        var msgKey = "getCapabilities";
        Ext.MessageBox.alert(_T_LAYER_WMSC('msgFailure.'+msgKey), (oRes.description || _T_LAYER_WMSC('msgFailure.'+msgKey)))
                      .setIcon(Ext.MessageBox.ERROR);
      }
    });
  },
  
  /**
   * get layer type from selected file
   * @param {string} selectedFile
   * @returns {undefined}
   */
  getMsGeometryType : function(selectedFile) {return;
    var msGeometryType = this.down('[name=msGeometryType]');
    var vars = ['layerServerUrl', 'layerWxsname'];
    var tpl = new Ext.XTemplate("WFS:{layerServerUrl}@{layerWxsname}");
    var apply = {};
    for (var i=0; i<vars.length; i++){
      var ctrl = this.down('[name='+vars[i]+']');
      if ( !ctrl ) return;
      apply[vars[i]] = ctrl.getValue();
    }
    msGeometryType.mask();
    Ext.Ajax.request({
      url: Routing.generate('carmen_ws_helpers_geometrytype', {
        layertype : "WMSC",
        layerfile: encodeURIComponent(tpl.apply(apply))
      }),
      method: 'GET',
      success: function (response) {
        msGeometryType.unmask();
        var oRes = eval("("+response.responseText+")");
        msGeometryType.setValue(oRes.msGeometryType);
      },
      failure: function (response) {
        msGeometryType.unmask();
        var oRes = eval("("+response.responseText+")");
        var msgKey = "canGetLayerTypeFromSelectedFile";
        Ext.MessageBox.alert(_T_LAYER_VECTOR('msgFailure.'+msgKey), (oRes.description || _T_LAYER_VECTOR('msgFailure.'+msgKey)))
                      .setIcon(Ext.MessageBox.ERROR);
      }
    });
  },
  
  getResolutionFromScale: function(scale, units) {
    var resolution;
    if (scale) {
      if (units == null) {
          units = "degrees";
      }
      var normScale = OpenLayers.Util.normalizeScale(scale);
      resolution = 1 / (normScale * OpenLayers.INCHES_PER_UNIT[units]
                                      * OpenLayers.DOTS_PER_INCH);        
      //Hack based on experimentations
      resolution = (156543.0339 / 147923.76911893254) * resolution * 0.75;
    }
    return resolution;
  }
});
