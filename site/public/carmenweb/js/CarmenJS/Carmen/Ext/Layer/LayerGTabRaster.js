var _T_LAYER_RASTER = function (key) {
  return _t("Layer.forms.GTabRaster." + key);
};

/**
 * Carmen.Model.LayerRaster
 */
Ext.define('Carmen.Model.LayerRaster', {
  extend: 'Carmen.Model.LayerRasterized',
  
  fields: [
    {name: "layerType",         type: "string", defaultValue: 'RASTER', convert : function(value){if ( Ext.isObject(value) ) return value.layerTypeCode; return value;}},
    {name: "msLayerData",       type: "string"},     // mapfile data : DATA si raster unique, TILEINDEX et TILEITEM si tuiles raster
    {name: "msLayerGrayLevel",  type: "boolean"}   
  ]
});

/**
 * Generic tab for type of raster layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabRaster', {
  extend: 'Carmen.Generic.Form',
  name : 'GTabRaster', 
  title: _T_LAYER_RASTER('title'),
  height: 'auto',
  layout: 'form',
  otherTabs: ['TabStyle', 'TabMeta'],
  modelName: 'Carmen.Model.LayerRaster',
  // methods
  initComponent: function() {
    var me = this;
    
    // items on this panel
    this.items = [{
        xtype: 'textfield',
        name: 'layerTitle',
        fieldLabel: _T_LAYER_RASTER('fields.title'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_RASTER('fields.name'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [registerWarningOnIdentifierLength({
            xtype: 'textfield',
            name: 'layerName',
            allowBlank: false,
            /*  recommended that the name not contain spaces, special characters, or begin with a number */
            vtype: 'identifier',
            flex: 5
          }), {
            xtype: 'button',
            name : 'layerName_button',
            text: _T_LAYER_RASTER('fields.auto'),
            margin: '0 10px 0 0',
            /**
             * auto calculation of layerName field
             */
            handler: function () {
              var ctrlFrom = me.down('[name=layerTitle]');
              var ctrlTo = me.down('[name=layerName]');
              if (ctrlFrom && ctrlTo) {
                ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
              }
            },
            flex: 1
          }, {
            xtype: 'textfield',
              labelAlign : 'right',
            name: 'layerIdentifier',
            fieldLabel: _T_LAYER_RASTER('fields.ident'),
            allowBlank: false,
            /*  recommended that the name not contain spaces, special characters, or begin with a number */
            vtype: 'identifier',
            flex: 5
          }, {
            xtype: 'button',
            name : 'layerIdentifier_button',
            text: _T_LAYER_RASTER('fields.auto'),
            /**
             * auto calculation of layerName field
             */
            handler: function () {
              var ctrlFrom = me.down('[name=layerTitle]');
              var ctrlTo = me.down('[name=layerIdentifier]');
              if (ctrlFrom && ctrlTo) {
                ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
              }
            },
            flex: 1
          }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_RASTER('fields.type'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
            xtype: 'combobox',
            name: 'msGeometryType',
            queryMode: 'local',
            displayField: 'displayField',
            valueField: 'valueField',
            store: Carmen.Dictionaries.RASTERTYPES,
            autoLoadOnValue: true,
            autoSelect: true,
            editable: false,
            flex: 6,
            margin: '0 10px 0 0'
          }, {
            xtype: 'checkbox',
            labelAlign : 'right',
            name: 'msLayerGrayLevel',
            fieldLabel: _T_LAYER_RASTER('fields.gray'),
            flex: 6
          }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_RASTER('fields.path'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
            name: 'msLayerData',
            xtype: 'textfield',
            allowBlank: false,
            maxLength: 512,
            enforceMaxLength: true,
            flex: 11
          }, {
            xtype: 'button',
            text: _T_LAYER_RASTER('fields.browse'),
            flex: 1,
            name : 'msLayerData_button',
            handler: function(){
              var windowClass = "Ext.ux.FileBrowserWindow";
              var ctrlType = me.down('[name=msGeometryType]');
              if ( !Ext.isDefined(Carmen.Dictionaries.RASTERTYPES.TYPES[ ctrlType.getValue() ])) return;
              if ( !Ext.isDefined(Carmen.Dictionaries.RASTERTYPES.EXTENSIONS[ ctrlType.getValue() ])) return;
              
              var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
                || Ext.create(windowClass, {
                  id: CMP_REFERENCE(windowClass),
                  selectExtension: '*.'+Carmen.Dictionaries.RASTERTYPES.EXTENSIONS[ctrlType.getValue()].join(',*.'),
                  uploadExtension: '.'+Carmen.Dictionaries.RASTERTYPES.EXTENSIONS[ctrlType.getValue()].join(',.'),
                  dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                  defaultPath: '/Root',
                  relativeRoot: '/Root/Publication',
                  listeners: {  
                    selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                      var ctrlTo = me.down('[name=msLayerData]');
                      ctrlTo.setValue(fullPathName);
                      wind.close();
                    }
                  }
                });
              window.show();
            }
          }]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_RASTER('fields.proj'),
        width: Carmen.Util.WINDOW_WIDTH - 40,
        layout: 'hbox',
        items: [{
          flex : 6,
          margin: '0 10px 0 0',
          xtype: 'comboboxprojections',
          name: 'layerProjectionEpsg',
        queryMode: 'local',
        editable: false,
        value: Carmen.Defaults.PROJECTION
        }, {flex:6}]
      }, {
        xtype: 'fieldcontainer',
        fieldLabel: _T_LAYER_RASTER('fields.scales'),
        layout: 'hbox',
        width: Carmen.Util.WINDOW_WIDTH - 40,
        items: [{
            xtype: 'numberfield',
            name: 'layerMinscale',
            labelAlign : 'right',
            labelWidth : 60,
            fieldLabel: _T_LAYER_RASTER('fields.min'),
            allowBlank: false,
            maxLength: 10,
            enforceMaxLength: true,
            minValue: Carmen.Defaults.MINSCALE,
            maxValue: Carmen.Defaults.MAXSCALE,
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowOnlyWhitespace: false,
            allowDecimals: false,
            allowExponential: false,
            flex: 6,
            margin: '0 10px 0 0'
          }, {
            xtype: 'numberfield',
            name: 'layerMaxscale',
            labelAlign : 'right',
            fieldLabel: _T_LAYER_RASTER('fields.max'),
            allowBlank: false,
            maxLength: 10,
            enforceMaxLength: true,
            minValue: Carmen.Defaults.MINSCALE,
            maxValue: Carmen.Defaults.MAXSCALE,
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            allowOnlyWhitespace: false,
            allowDecimals: false,
            allowExponential: false,
            flex: 6
          }]
      }, {
        xtype:'fieldset',
        title: _t("Layer.window.titleLayerBackground"), //"Paramétrages d'affichages de la couche",
        layout: 'form',
        items : [{
          xtype: 'fieldcontainer',
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [{
              xtype: 'checkbox',   
              name : 'layerIsBackground',
              fieldLabel: _t("Layer.forms.fields.layerIsBackground"),
              flex: 6, 
              labelWidth : '80%',
              listeners : [{
                      change: function (me, value) {
                          var show_component = false;
                          var is_checked = value; 
                          if(is_checked) {
                              show_component = true;
                          }
                          
                          var backgroundDisplayImage = Ext.ComponentQuery.query('[name=backgroundDisplayImage]')[0]; 
                          backgroundDisplayImage.setVisible(show_component); 
                      }
                }]
          },{
              xtype: 'checkbox',   
              name : 'layerIsDefaultBackground',
              fieldLabel: _t("Layer.forms.fields.layerIsDefaultBackground"),
              flex: 6,
              labelWidth : '80%',
              listeners : [{
                      change: function (me, value) {
                          
                          var mainFrame = Ext.ComponentQuery.query('[name=GTabRaster]')[0]; 
                          var mainFrameRecord = mainFrame.form._record; 
                          var layerId = mainFrameRecord.data.layerId; 
                          var joinedStore = mainFrameRecord.joined; 
                          
                          for(var i = 0 ; i < joinedStore.length; i++ ) {
                              var current_store = joinedStore[i];                               
                              if(current_store.config.model === "Carmen.Model.Layer") {                                 
                                  for(var i = 0 ; i < current_store.data.items.length; i++ ) {
                                      var current_layer = current_store.data.items[i]; 
                                      if(current_layer.id !== layerId ) {
                                          current_layer.data.layerIsDefaultBackground = false; 
                                      }
                                  }
                              }
                          }
                      }
              }]
          }]
        },{
              xtype: 'fieldcontainer',
              layout: 'hbox',
              width: Carmen.Util.WINDOW_WIDTH - 40,
              hidden : true  ,
              name : "backgroundDisplayImage",
              items: [{
                    
                    fieldLabel: _t("Layer.forms.fields.layerBackgroundImage"),
                    name : 'layerBackgroundImage',
                    xtype: 'textfield',
                    labelWidth : '40%',
                    width: Carmen.Util.WINDOW_WIDTH - 90,
                    queryMode: 'local',
                    displayField: 'displayField',
                    valueField: 'valueField',
                    store: Carmen.Dictionaries.LOGOPATH_IMAGES,
                    editable: false,
                    autoLoadOnValue : true,
                    autoSelect : true,
                    flex : 8
              }, { 
                    xtype: 'button',
                    text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
                    name : 'uiLogopath_button',
                    flex : 3    ,
                    handler: function(){
                      var windowClass = "Ext.ux.FileBrowserWindow";
                      var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
                      || Ext.create(windowClass, {
                         id: CMP_REFERENCE(windowClass),
                         selectExtension: '*.jpg,*.gif,*.png',
                         uploadExtension: '.jpg,.gif,.png',
                         dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                         defaultPath: '/Root',
                         relativeRoot: '/Root/IHM',
                         listeners: {  
                           selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath)  {
                             var ctrlTo = me.down('[name=layerBackgroundImage]');
                             ctrlTo.setValue(fullPathName);
                             wind.close();
                           }
                         }
                      });
                      window.show();
                    },

                    listeners : {
                      afterrender : function(){
                        var check = me.down('[name=uiLogo]');
                        if ( check ){check.fireEvent('change', check);}
                      }
                    }
                  }]
          }]
      }];
    
    this.callParent(arguments);
  },
  
  loadRecord : function(record) {
    var ret = this.callParent(arguments);

    var components = ['msLayerData', 'msGeometryType'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);
    
    return ret;
  }
});
