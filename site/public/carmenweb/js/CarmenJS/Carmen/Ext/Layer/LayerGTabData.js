var _T_LAYER = function (key) {
  return _t("Layer.forms.GTabData." + key);
};

/**
 * Carmen.Model.Layer
 */
Ext.define('Carmen.Model.LayerData', {
  extend: 'Carmen.Model.Layer',
  idProperty: 'layerId',
  fields: []
});

/** Store type de reprensation */
var displayTypeStore = Ext.create('Ext.data.Store', {
  fields: ['label', 'value'],
  data: [
    { "label": "Vectorielle", "value": "1" },
    { "label": "Image", "value": "2" }
  ]
});

function saveLayer(gTabData) {
  var me = gTabData;
  var layerDisplayType = me.layerDisplayType;
  if (me.copyMapid && me.copyMapid != "") {
    var params = {
      'mapToId': Carmen.user.map.id, /*id for current map file : Carmen.prodige.id*/
      'mapFromId': me.copyMapid, /*id of source mapfile */
      'mode': 'add',

    }

    var jsonData = {
      ar_layer_ids: me.copyLayers,
    }


    jsonData.displayMode = layerDisplayType;

    //default representation
    Ext.Ajax.request({
      url: Routing.generate('carmen_ws_add_layers', params),
      timeout: 24 * 60 * 60 * 1000, //24
      method: 'POST',
      jsonData: jsonData,
      success: function (response) {
        if(me && me.up('window') ){
          me.up('window').close();
        }
        
        
        var jsonResponse = Ext.decode(response.responseText);


        Carmen.Model.Map.onSuccessSave(jsonResponse);
      },
      failure: function (response) {
        console.error('error - response is : ', response);
      }
    });
  } else {
    var submits = [];
    jsonData = {};
    jsonData.displayMode = layerDisplayType;


    Ext.each(me.tabLayers, function (record) { submits.push(record.getData({ persist: true, serialize: true })); });

    jsonData.layers = submits;
    Ext.Ajax.request({
      url: Routing.generate('carmen_ws_postlayers', { 'mapId': Carmen.user.map.get('mapId') }),
      async: false,
      timeout: 24 * 60 * 60 * 1000,
      method: 'POST',
      jsonData: jsonData,
      success: function (response) {
        //me.up('window').close();
        if (me.up('window')) {
          me.up('window').close();
        }
        var jsonResponse = Ext.decode(response.responseText);
        Carmen.Model.Map.onSuccessSave(jsonResponse);
      }
      , failure: function (response) {
        if (Carmen.notAuthenticatedException(response)) return;
      }
    });
  }
}

function selectLayer(gTabData, record) {
  var me = gTabData;
  //reinit on each selection
  me.copyMapid = "";
  me.copyLayers = new Array();
  me.tabLayers = new Array();

  const layerTitle = Ext.ComponentQuery.query('[name=layerTitle]');

  if (layerTitle && layerTitle instanceof Array && layerTitle.length > 0) {
    layerTitle[0].setValue(record.data.text);//layerDisplayType
  }

  me.layerDisplayType = record.data.displayType;
  if (record.data.map_id && record.data.map_id != "") {
    //do nothing
    me.copyMapid = record.data.map_id;
    me.copyLayers = record.data.layers_id;

  } else {
    me.copyMapid = "";
    me.copyLayers = new Array();
    for (var i = 0; i < record.data.layers.length; i++) {
      var layer = record.data.layers[i];

      //if(record.data.srid && record.data.srid != "") {
      if ((layer.type == "RASTER_UNIQUE") || (layer.type == "RASTER_DALLE")) {
        var record_modele = Ext.create('Carmen.Model.LayerRaster', {
          layerTitle: layer.title,
          layerType: "RASTER",
          layerMetadataUuid: record.data.id,
          layerName: convertNameToIdentifier(layer.table_name, 20),
          msLayerName: convertNameToIdentifier(layer.table_name, 20),
          layerIdentifier: convertNameToIdentifier(layer.table_name),
          layerMetadataFile: layer.layerMetadataFile,
          msGeometryType: layer.type,
          msLayerData: layer.table_name,
          layerProjectionEpsg: layer.srid
        });
      }
      else if (layer.type == "VECTOR") {
        var record_modele = Ext.create('Carmen.Model.LayerVector', {
          layerTitle: layer.title + ' [' + layer.table_name + ']',
          layerName: convertNameToIdentifier(layer.table_name, 20),
          msLayerName: convertNameToIdentifier(layer.table_name, 20),
          layerMetadataUuid: record.data.id,
          layerIdentifier: layer.table_name,
          layerProjectionEpsg: layer.srid,
          msGeometryType: layer.geometry,
          layerMetadataFile: layer.layerMetadataFile,
          layerType: "POSTGIS",
          msLayerConnectionType: "POSTGIS",
          msLayerPgSchema: "public",
          msLayerPgTable: layer.table_name,
          msLayerPgGeometryField: "the_geom",
          msLayerPgIdField: "gid",
          msLayerPgProjection: layer.srid

        });
      }

      me.tabLayers.push(record_modele);
    }
    /*}
    else { //TODO Benoist - A vérifier - Absence d'une projection 
      Ext.Msg.alert('Attention', 'Cette couche n\'a pas une projection ....');
    }*/
  }
}

/**
 * Generic tab for type of layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabData', {
  extend: 'Carmen.Generic.Form',

  title: _T_LAYER('title'),
  frame: true,
  height: 'auto',
  width: '1500',
  layout: 'column',
  modelName: 'Carmen.Model.Layer',
  layerSelect: null,
  defaults: {
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },
  selectedLayer: {},
  setWindowButtons: function () {
    var me = this;
    var bbar = me.up('window').down('toolbar[dock="bottom"]');
    bbar.removeAll();
    bbar.add([{
      text: _T_MAP_ADD('btn.save'),
      handler: function () {


        if (me.layerStore.data.items && me.layerStore.data.items instanceof Array) {
          me.layerStore.data.items.forEach((layer) => {
            if (layer.data.selected) {
              selectLayer(me, layer);
              saveLayer(me);
            }

          })
        }


        return false;

      }
    }, {
      text: _T_MAP_ADD('btn.cancel'),
      handler: function () {
        ADDCONTEXTE = false;
        me.up('window').close();
      }
    }]);

    me.layerSelect = !!Ext.getCmp('layerSelect') ? Ext.getCmp('layerSelect') : null;
  },
  // methods
  initComponent: function () {
    var me = this;

    me.copyMapid = "";
    me.copyLayers = new Array();
    me.tabLayers = new Array();

    me.domFilter = "";

    me.sDomStore = new Ext.data.TreeStore({
      storeId: 'SDOM',
      //autoLoad : true,
      //lazyFill: true,
      proxy: {
        type: 'jsonp',
        reader: {
          type: 'json'
        },
        params: {
          mode: 0
        },
        url: Carmen.parameters.catalogue_url + "/geosource/getDomaines"
      }
    });
    me.sDomStore.load();

    me.url = Carmen.parameters.catalogue_url + "/geosource/administrationCartoCouche?SRV_CARTO=carto";

    me.layerStore = new Ext.data.Store({
      storeId: 'layer',
      fields: ['text', 'type', 'table_name', 'icon', 'map_id', 'layers_id', 'layers', 'id',
                { name: "selected", type: "boolean", defaultValue: false}],
      proxy: {
        type: 'jsonp',
        reader: {
          type: 'json'
        },
        url: me.url
      },
      listeners:{
        load: () => {
          me.layerStore.each(function(record){
            record.set('selected', !!me.selectedLayer[record.id]);
          })
        }
      }
    });
    me.on('afterrender', me.setWindowButtons, me);

    me.layerParams = null;

    /** on déselectionne chaque layer à l'ouverture du composant */
   
    // items on this panel
    const selectDomain = {
      border: true,
      xtype: 'treepanel',
      name: 'fsTree',
      columnWidth: 0.4,
      margin: '0 4 10 0',
      height: 200,
      padding: '0 0 0 0',
      scrollable: true,
      store: me.sDomStore,
      rootVisible: false,
      selModel: new Ext.selection.TreeModel({
        listeners: {
          selectionchange: function (selection, node) {
            me.copyMapid = "";
            me.copyLayers = new Array();
            me.tabLayers = new Array();
            node = node[0]; // un seul noeud sélectionnable
            var dom = -1;
            var sdom = -1;
            if (node.data.isDomain == true) {
              //dom
              dom = node.data.id.match(/dom_(\d+)/)[1];
            } else if (node.data.isSubDomain == true) {
              //sdom
              dom = node.data.parentId.match(/dom_(\d+)/)[1];
              sdom = node.data.id.match(/sdom_(\d+)/)[1];
            } else {
              //rubrique
              dom = node.data.id;
            }

            me.layerParams = { coucheType: "0,1,-4",
                               domain : dom,
                               subdomain:sdom};
               //url: me.url + "&coucheType=0,1,-4&domain=" + dom + "&subdomain=" + sdom };
            //me.layerStore.load(me.layerParams);

          }
        }
      })
    };

    // Select couche
    const selectCouche = {
      border: true,
      xtype: 'gridpanel',
      viewConfig: { emptyText: 'Aucune donnée ne répond aux critères sélectionnés' },
      name: 'fsForm',
      id: 'fsForm',
      columnWidth: 0.6,
      margin: '0 0 10 4',
      height: 200,
      padding: '0 0 0 0',
      scrollable: true,
      store: me.layerStore,
      columns: [{
        xtype: 'checkcolumn',
        dataIndex: 'selected',
        listeners: {
          checkchange: (self, rowIndex, checked) => {
            
            me.layerStore.each(record => {
              me.selectedLayer[record.id] = record.data.selected;
            })

          }
        }
      }, {
        // comboBox, sélection du type d'affichage vectoriel
        xtype: 'widgetcolumn',
        dataIndex: 'displayType',
        flex: 3,
        // This is the widget definition for each cell.
        // Its "value" setting is taken from the column's dataIndex
        widget: {
          hidden: true,
          xtype: 'combobox',
          name: 'layerDisplayType',
          store: displayTypeStore,
          queryMode: 'local',
          displayField: 'label',
          valueField: 'value',
          allowBlank: true,
          value: 2,
          maxLength: 255,
          enforceMaxLength: true,
          flex: 12,
          listeners: {
            select: function (datefield, selected, eOpts) {
              this.recordLayer.data.displayType = selected.data.value;
              //var rowIndex = datefield.up('gridpanel').indexOf(datefield.el.up('table'));
              //var record = datefield.up('gridpanel').getStore().getAt(rowIndex);
              //record.set('selected', value);
            }
          }
        },
        onWidgetAttach: function (column, layerDisplayType, record) {
          layerDisplayType.recordLayer = record;
          if (record.data.map_id && record.data.map_id != "") {
            if (layerDisplayType) {
              layerDisplayType.setHidden(record.data.icon.indexOf('point') === -1);
              layerDisplayType.setValue('2');

              record.data.displayType = '2';
            }
          } else {
            for (var i = 0; i < record.data.layers.length; i++) {
              var layer = record.data.layers[i];
              if (layerDisplayType) {
                layerDisplayType.setHidden(layer.geometry !== 'POINT');
                layerDisplayType.setValue('2');
                record.data.displayType = '2';
              }
            }
          }
        }
      }, {
        dataIndex: 'icon', flex: 1, renderer: function (value) { return '<img src="' + value + '"/>'; }
      }, {
        text: 'Text', dataIndex: 'text', flex: 8, scrollable: true
      }],
      hideHeaders: true,
      mode: "SINGLE",
      selModel: Ext.create('Ext.selection.RowModel', {
        listeners: {
          select: {
            fn: function (selModel, record, index) {
              //reinit on each selection
              me.copyMapid = "";
              me.copyLayers = new Array();
              me.tabLayers = new Array();

              const layerTitle = Ext.ComponentQuery.query('[name=layerTitle]');

              if (layerTitle && layerTitle instanceof Array && layerTitle.length > 0) {
                layerTitle[0].setValue(record.data.text);//layerDisplayType
              }

              var layerDisplayType = Ext.getCmp('layerDisplayType');
              me.layerDisplayType = layerDisplayType;
              if (record.data.map_id && record.data.map_id != "") {
                //do nothing
                me.copyMapid = record.data.map_id;
                me.copyLayers = record.data.layers_id;
                if (layerDisplayType) {
                  layerDisplayType.setHidden(record.data.icon.indexOf('point') === -1);
                }
              } else {
                me.copyMapid = "";
                me.copyLayers = new Array();
                for (var i = 0; i < record.data.layers.length; i++) {
                  var layer = record.data.layers[i];
                  if (layerDisplayType) {
                    layerDisplayType.setHidden(layer.geometry !== 'POINT');
                  }
                  //if(record.data.srid && record.data.srid != "") {
                  if ((layer.type == "RASTER_UNIQUE") || (layer.type == "RASTER_DALLE")) {
                    var record_modele = Ext.create('Carmen.Model.LayerRaster', {
                      layerTitle: layer.title,
                      layerType: "RASTER",
                      layerMetadataUuid: record.data.id,
                      layerName: convertNameToIdentifier(layer.table_name, 20),
                      msLayerName: convertNameToIdentifier(layer.table_name, 20),
                      layerIdentifier: convertNameToIdentifier(layer.table_name),
                      layerMetadataFile: layer.layerMetadataFile,
                      msGeometryType: layer.type,
                      msLayerData: layer.table_name,
                      layerProjectionEpsg: layer.srid
                    });
                  }
                  else if (layer.type == "VECTOR") {
                    var record_modele = Ext.create('Carmen.Model.LayerVector', {
                      layerTitle: layer.title + ' [' + layer.table_name + ']',
                      layerName: convertNameToIdentifier(layer.table_name, 20),
                      msLayerName: convertNameToIdentifier(layer.table_name, 20),
                      layerMetadataUuid: record.data.id,
                      layerIdentifier: layer.table_name,
                      layerProjectionEpsg: layer.srid,
                      msGeometryType: layer.geometry,
                      layerMetadataFile: layer.layerMetadataFile,
                      layerType: "POSTGIS",
                      msLayerConnectionType: "POSTGIS",
                      msLayerPgSchema: "public",
                      msLayerPgTable: layer.table_name,
                      msLayerPgGeometryField: "the_geom",
                      msLayerPgIdField: "gid",
                      msLayerPgProjection: layer.srid

                    });
                  }

                  me.tabLayers.push(record_modele);
                }
                /*}
                else { //TODO Benoist - A vérifier - Absence d'une projection 
                  Ext.Msg.alert('Attention', 'Cette couche n\'a pas une projection ....');
                }*/
              }
            }
          }
        }
      })
    }

    // options
    const selectParams = [{
      xtype: 'fieldset',
      layout: 'form',
      padding: 0,
      margin: 0,
      border: false,
      items: [{
        xtype: 'checkbox',
        name: 'multipleSelection',
        fieldLabel: _T_LAYER('fields.multipleSlection'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      }, {
        xtype: 'textfield',
        name: 'layerTitle',
        fieldLabel: _T_LAYER('fields.alias'),
        allowBlank: false,
        maxLength: 255,
        enforceMaxLength: true,
        flex: 12
      },
      ]
    }];

    me.items = [{
      layout: {
        type: 'accordion',
        titleCollapse: true,
        animate: true,
        activeOnTop: false,
        multi: true,
      },

      items: [{
        title: _T_LAYER('fields.titleLayersSettings'),
        border: true,
        bodyStyle: 'padding: 5px 5px 5px 5px;',
        items: [{
            xtype: 'textfield',
            id: 'search-layer',
            width: 300,
            padding: '10 0 10 0',
            emptyText : _T_LAYER('fields.titleLayersPlaceholder')
          }, {
            xtype: 'label',
            text: 'Filtre par domaine / sous-domaine'
          },
          selectDomain, 
          {
            xtype: 'button',
            text: 'Recherche dans le catalogue',
            id: 'search-layer-button',
            padding: '5 0 5 0',
            flex: 2,
            handler: function (btn) {
              if(!me.layerParams){
                me.layerParams = {};
                me.layerParams.coucheType="0,1,-4";
                 //= { url: me.url + "&coucheType=0,1,-4" };
              }
    
              me.layerParams.q = Ext.getCmp('search-layer').value;
    
              me.layerStore.reload({
                params: me.layerParams
              });
            }
          }
        ]
      }, {

        title: _T_LAYER('fields.titleLayerProperties'),
        items: selectCouche,
        itemId: 'layerSelect',
        id: 'layerSelect',
        name: 'layerSelect',
      }]
    }];

    this.callParent(arguments);
  }
});
