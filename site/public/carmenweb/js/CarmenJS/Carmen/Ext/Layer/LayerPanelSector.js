var PANEL_SECTOR_TRANSLATE = function (key) {
  return _t("Layer.forms.AnalyseType." + key);
};

/**
 * Carmen.Model.Layer.AnalyseType.Sector
 */
Ext.define('Carmen.Model.Layer.AnalyseType.Sector', {
  extend: 'Ext.data.Model',
  idProperty: 'layerId',
  fields: [
  ]
});

/**
 * Analyse type panel : sector analysis
 */
Ext.define('Carmen.FormSheet.Layer.AnalyseType.Sector', {
  extend: 'Ext.form.Panel',
  title: PANEL_SECTOR_TRANSLATE('title'),
  frame: true,
  height: 'auto',
  layout: 'fit',
  html: 'sector analysis',
});
