var PANEL_COLOR_TRANSLATE = function (key) {
  return _t("Layer.forms.AnalyseType." + key);
};

/**
 * Carmen.Model.Layer.AnalyseType.Color
 */
Ext.define('Carmen.Model.Layer.AnalyseType.Color', {
  extend: 'Ext.data.Model',
  idProperty: 'layerId',
  fields: [
  ]
});


/**
 * Analyse type panel : graduated color analysis
 */
Ext.define('Carmen.FormSheet.Layer.AnalyseType.Color', {
  extend: 'Ext.form.Panel',
  title: PANEL_COLOR_TRANSLATE('title'),
  frame: true,
  height: 'auto',
  layout: 'fit',
  html: 'graduated color analysis',
  /*items: [
   
   ]*/
});
