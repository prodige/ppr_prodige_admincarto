var OGC_TRANSLATE = function(key) {
  return _t("Layer.forms.TabOGC." + key);
};

/**
 * Carmen.Model.Layer.Meta
 */
Ext.define('Carmen.Model.Layer.OGC', {
  extend : 'Carmen.Model.Layer',
  idProperty : 'layerId',
  fields : [
    {
      name : 'msProjections',
      convert: function(value, record){
        var data = "";
        var separator = " ";
        if( value && Ext.isArray(value) ) {
          Ext.each(value, function(thisProjection){
            if ( Ext.isString(thisProjection) ){
              data = data + thisProjection + separator;
            } else {
             // todo ??
            }
          });
          return data.trim(); // to remove the last " " at the end of the string
        }
      return value;
    }, 
    serialize: function(value, record) {
    }
  }]
});

/**
 * OGC diffusion tab
 */
Ext.define( 'Carmen.FormSheet.Layer.TabOGC', 
  {
    extend : 'Carmen.Generic.Form',
    // xtype: 'cmngenericform',
    title : OGC_TRANSLATE('title'),
    height : 'auto',
    width : '100%',
    layout : 'anchor',
    statics : {},
    autoScroll : true,
    // html: 'OGC diffusion tab',
    
    
    initComponent : function(record) {
      var me = this;      
      
      me.defaults = {margin : '0 0 10 0'};
         
      me.items = [
         {
        name : 'diffusionPanel',
            xtype : 'panel',
            frame : true,
        title : OGC_TRANSLATE('extra.diffusion'),
        items : [{
          padding : '10 10 0 15',
          xtype :'checkbox',
            boxLabel : OGC_TRANSLATE('fields.OGCDiffusionOverall'),
            name : 'OGCDiffusionOverall',
            valueId : 'overalldiffusion',
            listeners : {
              change : function (checkbox, newVal, oldVal) {
              var allCheckBoxes = checkbox.up().query('[xtype=checkboxfield]');
                  if (newVal == '1' && oldVal == '0') {
                  for (var i = 0; i < allCheckBoxes.length; i++) {
                      allCheckBoxes[i].setValue('1');
                    }
                  } else if (newVal == '0' && oldVal == '1') {
                  for (var i = 0; i < allCheckBoxes.length; i++) {
                      allCheckBoxes[i].setValue('0');
                    }
                  }
              }
          }
          },{
          xtype : 'checkboxgroup',
          padding : 0,
          width : '98%',
          defaults : {padding : 10},
          columns : 4,
          items : [{
            xtype : 'checkboxfield',
            boxLabel : OGC_TRANSLATE('fields.OGCDiffusionWMS'),
            name : 'layerWms',
            valueId : 'wms',
            flex : 1
          },{
            xtype : 'checkboxfield',
            boxLabel : OGC_TRANSLATE('fields.OGCDiffusionWFS'),
            name : 'layerWfs',
            valueId : 'wfs',
            flex : 1
          },{
            xtype : 'checkboxfield',
            boxLabel : OGC_TRANSLATE('fields.OGCDiffusionATOM'),
            name : 'layerAtom',
            valueId : 'atom',
            flex : 1
          },{
            xtype : 'checkboxfield',
            boxLabel : OGC_TRANSLATE('fields.OGCDiffusionDownloadable'),
            name : 'layerDownloadable',
            valueId : 'downloadable',
            flex : 2
          }]
        }]
         },// end of DiffusionCheckBoxesFieldContainer
         {
        name : 'SRSPanel',
        
                xtype : 'panel',
              frame : true,
          title : OGC_TRANSLATE('extra.srs'),
          defaults : {padding : 10},
           items : [
             {
               xtype : 'displayfield',
               name: 'msMetadataMapSrs',
               fieldLabel : OGC_TRANSLATE('extra.commons'),
               flex : 6
             },{
               filterPickList: false,
               xtype: 'tagfieldprojections',
               width: '85%',
               fieldLabel: OGC_TRANSLATE('extra.specifics'),
               name : 'msGeoide_wxs_srs'
             }
         ]
         }, { // end of SRSSpecificFieldContainer
          xtype : 'panel',
          frame : true,
          title : OGC_TRANSLATE('extra.fields'),
        name : 'FieldsPanel',
        items : [{padding : '10 10 0 15',
          xtype :'checkbox',
          boxLabel : OGC_TRANSLATE('fields.OGCDiffusionOverall'),
          name : 'checkboxfield_overall',
          listeners : {
            change : function (checkbox, newVal, oldVal) {
              var allCheckBoxes = checkbox.up().down('checkboxgroup').items.items;
              if (newVal == '1' && oldVal == '0') {
                for (var i = 0; i < allCheckBoxes.length; i++) {
                allCheckBoxes[i].setValue('1');
              }
              } else if (newVal == '0' && oldVal == '1') {
                for (var i = 0; i < allCheckBoxes.length; i++) {
                allCheckBoxes[i].setValue('0');
                }
              }
            }
              }
        },{
        xtype : 'checkboxgroup',
        storeCheckboxesId : 'fields',
        padding : 0,
        defaults : {padding : 10},
        columns : 3,
        items : []
        } ]
      }
      // end of FieldsCheckboxesFieldContainer
    ];

    this.callParent();

  }, // end of initComponent

  loadRecord : function(record) {
    this.callParent(arguments);

    if (record instanceof Carmen.Model.Layer) {
      
      // commons Srs (from Map metadata)
      var value = Carmen.user.map.get('msMetadataMap').get('geoide_wms_srs').trim();
      if( Ext.isArray(value) ) {
        value = value.map(function(v){ return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider')+':'+v; }).join(', ');
      }
      this.query('[name=msMetadataMapSrs]')[0].setValue(value);

      var fields = record.get('fields');
      
      var fields_combobox = this.query('[storeId = fields]');
      var fields_checkboxgroup = this.query('[storeCheckboxesId = fields]')[0];
      
      if (fields instanceof Ext.data.Store) {
        var data = fields.getData();
        if (data.filtered) {
          data = data.getSource();
        }
        
        this.storeFields = Ext.create(
          fields.$className, {
            model : fields.getModel(),
            data : data.getValues('data')
        });
        
        var thisStoreFieldItems = this.storeFields.data.items;
  
        for ( var i in thisStoreFieldItems) {
          //if (true) {
          var thisFieldData = thisStoreFieldItems[i].data;
          var new_item = new Ext.form.Checkbox({
            boxLabel : thisFieldData.fieldName,
            padding : 10,
            name : 'checkboxfield_' + thisFieldData.fieldName,
            valueId : thisFieldData.fieldId,
            value : thisStoreFieldItems[i].data.fieldOgc,
            listeners : {
                change : function (checkbox, newVal, oldVal) {
                var rec = fields.findRecord('fieldId', checkbox.valueId);
                if( rec ) {
                  if( rec.get('fieldOgc') != newVal ) {
                      rec.set('fieldOgc', newVal);
                  }
                } 
              }
            }
          });
          fields_checkboxgroup.items.add(new_item);
          //}
        }
        
        fields.addListener('update', function( store, record, operation, modifiedFieldNames, details, eOpts ) {
          Ext.each(fields_checkboxgroup.items.items, function(checkbox){
            if( checkbox.valueId == record.get('fieldId') ) {
              checkbox.setValue( record.get('fieldOgc') );
              return false;
            }
          })
        });
        
      }
    }
  }

});
