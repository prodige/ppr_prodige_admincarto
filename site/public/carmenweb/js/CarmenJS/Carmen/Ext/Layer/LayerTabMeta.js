var META_TRANSLATE = function (key) {
  return _t("Layer.forms.TabMeta." + key);
};

/**
 * Metadatas tab
 */
Ext.define('Carmen.FormSheet.Layer.TabMeta', {
  extend: 'Carmen.Generic.Form',
  //xtype: 'cmngenericform',
  title: META_TRANSLATE('title'),
  width : '100%',
  height : 'auto',
  
  initComponent : function() {
    var me = this;
    me.defaults = {defaults : {margin : 10}, margin : '0 0 10 0'};
    me.items = [
      {
        xtype : 'panel',
        frame : true,
        title : META_TRANSLATE('panel.documents'),
        items : [
          {
            xtype: 'fieldcontainer',
            fieldLabel: _T_LAYER_VECTOR('fields.path'),
            layout: 'hbox',
            width: '90%',
            items: [
              {
                name: 'layerMetadataFile',
                xtype: 'textfield',
                allowBlank: true,
                maxLength: 512,
                enforceMaxLength: true,
                flex: 8
              },
              {
                xtype: 'button',
                text: _T_LAYER_VECTOR('fields.browse'),
                flex: 2,
                name : 'layerMetadataFile_button',
                handler: function(){
                  var windowClass = "Ext.ux.FileBrowserWindow";
                  var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
                  || Ext.create(windowClass, {
                    id: CMP_REFERENCE(windowClass),
                    selectExtension: '*.pdf',
                    uploadExtension: '.pdf',
                    dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                    defaultPath: '/Root/METADATA',
                    relativeRoot: '/Root/METADATA/Publication',
                      listeners: {  
                        selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath) {
                          var ctrlTo = me.down('[name=layerMetadataFile]');
                          ctrlTo.setValue(relativePath);
                          wind.close();
                        }
                      }
                    });
                  window.show();
                }
              },
              {
                html :'',
                flex : 2
              }
            ]
          }
        ]
      },
      {
        xtype : 'panel',
        frame : true,
        title : META_TRANSLATE('panel.geosource'),
        items : [
          {
            xtype : 'fieldcontainer',
           layout: {
             type: 'hbox',
             pack: 'start',
             align: 'stretch'
           },
                width : '98%',
            items : [
              {
                minWidth : 500,
                xtype : 'combobox',
                name: 'layerMetadataUuid',
                store: Carmen.Dictionaries.GEOSOURCE_METADATA,
                displayField: 'displayField',
                valueField: 'valueField',
                filterPickList: true,
                queryMode: 'local',
                editable : false,
                emptyText: ' -- '+ META_TRANSLATE('extra.newsheet') + ' -- ',
                triggers: {
                  create: {
                      cls: 'trigger-add',
                      handler: function(combobox) {
                        combobox.setValue(null);
                        
                        combobox.up('form').getRecord().set('geosourceLayerMetadataNeedsUpdate', true);
  
                        var field_publish = Ext.ComponentQuery.query('[name=geosourceLayerMetadataPublish]')[0];
                        field_publish.setDisabled(false);
  
                        var field_title = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswTitle]')[0];
                        var field_abstract = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswAbstract]')[0];
                        var field_keywords = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswKeywords]')[0];
                        var field_inspiretheme = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswInspiretheme]')[0];
                        field_title.setValue(null);
                        field_abstract.setValue(null);
                        field_keywords.setValue(null);
                        field_inspiretheme.setValue(null);
                      }
                  }
                },
                listeners: {
                  change: function(combobox) {
                    
                    combobox.up('form').getRecord().set('geosourceLayerMetadataNeedsUpdate', false);
                    
                    var field_publish = Ext.ComponentQuery.query('[name=geosourceLayerMetadataPublish]')[0];
                    var field_title = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswTitle]')[0];
                    var field_abstract = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswAbstract]')[0];
                    var field_keywords = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswKeywords]')[0];
                    var field_inspiretheme = Ext.ComponentQuery.query('[name=geosourceLayerMetadataCswInspiretheme]')[0];
  
                    var currentUuid = combobox.getValue() ;
                    if (currentUuid !== null ){
                      field_publish.setDisabled(true);
                      Ext.Ajax.request({
                        url: Routing.generate('carmen_ws_get_csw_record', {'uuid': combobox.getValue()}),
                        success: function(response){
                          var json = Ext.util.JSON.decode(response.responseText);
                          /*
                          data = Ext.create('Carmen.Model.GeosourceLayerMetadata', json);
                          field_title.setValue(data.get('cswTitle'));
                          field_abstract.setValue(data.get('cswAbstract'));
                          field_keywords.setValue(data.get('keywords'));
                          field_inspiretheme.setValue(data.get('inspiretheme'));
                          */
                          field_title.setValue(json.cswTitle||'');
                          field_abstract.setValue(json.cswAbstract||'');
                          field_keywords.setValue(json.cswKeywords ? json.cswKeywords.split(',') : []);
                          field_inspiretheme.setValue(json.cswInspiretheme||'');
                        }
                      });
                    }
                  }
                },
                flex : 8
              },
              {
                flex : 2, html :'', width : 50
              },
              {
                xtype : 'checkboxfield',
                name : 'geosourceLayerMetadataPublish',
                fieldLabel : META_TRANSLATE('fields.public'),
                disabled : true,
                flex :2
              }
            ]
          },
              {
                xtype : 'textfield',
                name: 'geosourceLayerMetadataCswTitle',
                width : '98%',
                fieldLabel : META_TRANSLATE('fields.title'),
                listeners: {
                  change: function(combobox) {
                    combobox.up('form').getRecord().set('geosourceLayerMetadataNeedsUpdate', true);
                  }
                }
              },
              {
                xtype : 'textareafield',
                name: 'geosourceLayerMetadataCswAbstract',
                width : '98%',
                fieldLabel : META_TRANSLATE('fields.abstract'),
                listeners: {
                  change: function(combobox) {
                    combobox.up('form').getRecord().set('geosourceLayerMetadataNeedsUpdate', true);
                  }
                }
              },
              {
                xtype : 'tagfield',
                name: 'geosourceLayerMetadataCswKeywords',
                width : '98%',
                cls : 'tagfield-editable',
                fieldLabel : META_TRANSLATE('fields.keywords'),
                store: [],
                displayField: 'displayField',
                valueField: 'valueField',
                queryMode: 'local',
                hideTrigger : true,
                editable : true,
                createNewOnEnter: true,
                createNewOnBlur: true,
                //triggerOnClick: false,
                forceSelection: false,
                listeners: {
                  change: function(combobox) {
                    combobox.up('form').getRecord().set('geosourceLayerMetadataNeedsUpdate', true);
                  }
                }
              },
              {
                xtype : 'combobox',
                width : '40%',
                name: 'geosourceLayerMetadataCswInspiretheme',
                fieldLabel : META_TRANSLATE('fields.keyword_inspire'),
                store: Carmen.Dictionaries.INSPIRE_THEMES,
                queryMode: 'local',
                displayField: 'displayField',
                valueField: 'valueField',
                editable: false,
                listeners: {
                  change: function(combobox) {
                    combobox.up('form').getRecord().set('geosourceLayerMetadataNeedsUpdate', true);
                  }
                }
              }    
        ]
      }
    ];
    this.callParent();
    
    this.on('beforeactivate', function(){Carmen.Dictionaries.GEOSOURCE_METADATA.reload()}, this, {single:true})
    
  }
  
});
