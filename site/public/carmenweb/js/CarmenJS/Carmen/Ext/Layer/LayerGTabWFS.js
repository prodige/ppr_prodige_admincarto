var _T_LAYER_WFS = function (key) {
  return _t("Layer.forms.GTabWFS." + key);
};

/**
 * Carmen.Model.LayerWFS
 */
Ext.define('Carmen.Model.LayerWFS', {
  extend: 'Carmen.Model.LayerVectorial',
  idProperty: 'layerId',
  fields: [
    {name: "layerType",   type: "string", defaultValue: 'WFS', convert : function(value){if ( Ext.isObject(value) ) return value.layerTypeCode; return value;}},
    
    {name: "LayerSavedConnections", type: "string", persist: false},
    {name: "msLayerConnectionName", type: "string"},                      // mapfile data : NAME
    {name: "msLayerConnectionType", type: "string", defaultValue: "WFS"}, // mapfile data : CONNECTIONTYPE
    
    {name: "wfsTitle", type: "string"},
    {name: "wfsSrs",   type: "string"},
    
    {name: "layerWxsnameRO", type: "string", persist: false, convert: function(value, record) {
        return ( record && record.data && record.data.layerWxsname ? record.data.layerWxsname : "" );
      }   
    }
  ]
});

/**
 * Generic tab for type of WFS layer
 */
Ext.define('Carmen.FormSheet.Layer.GTabWFS', {
  extend: 'Carmen.Generic.Form',
  name : 'GTabWFS', 
  title: _T_LAYER_WFS('title'),
  height: 'auto',
  layout: 'column',
  otherTabs: ['TabStyle', 'TabLabel', 'TabField', 'TabMeta', 'TabOGC'],
  modelName: 'Carmen.Model.LayerWFS',
  defaults:{
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },
  
  // methods
  initComponent: function() {
    var heightLayerList = 350;
    var me = this;

    /**
     * @mode remote
     * @table : wxs (wxs_type=WFS)
     */
    me.storeService = new Ext.data.XmlStore({
      storeId : 'SAVED_WFS_SERVICES',
      autoLoad : true,
      fields : [
        {name : 'valueField', mapping : 'wxsId'},
        {name : 'displayField', mapping : 'wxsName'}
      ],
      proxy: {
        type: 'rest',
        url: Routing.generate('carmen_ws_get_saved_services')+"/WFS"
      }
    });

    // items on this panel
    me.items = [{
        xtype:'fieldset',
        layout: 'form',
        padding: 0,
        margin: 0,
        border: false,
        items: [{
          xtype: 'textfield',
          name: 'layerTitle',

          fieldLabel: _T_LAYER_WFS('fields.title'),
          allowBlank: false,
          maxLength: 255,
          enforceMaxLength: true,
          flex: 12
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_LAYER_WFS('fields.name'),
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [registerWarningOnIdentifierLength({
              xtype: 'textfield',
              name: 'layerName',
              allowBlank: false,
              /*  recommended that the name not contain spaces, special characters, or begin with a number */
              vtype: 'identifier',
              flex: 5
            }), {
              xtype: 'button',
              name : 'layerName_button',
              text: _T_LAYER_WFS('fields.auto'),
              margin: '0 10px 0 0',
              /**
               * auto calculation of layerName field
               */
              handler: function() {
                var ctrlFrom = me.down('[name=layerTitle]');
                var ctrlTo = me.down('[name=layerName]');
                if ( ctrlFrom && ctrlTo ) {
                  ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 0));
                }hand
              },
              flex: 1
            }, {
              labelAlign : 'right',
              xtype: 'textfield',
              name: 'layerIdentifier',
              fieldLabel: _T_LAYER_WFS('fields.ident'),
              allowBlank: false,
              /*  recommended that the name not contain spaces, special characters, or begin with a number */
              vtype: 'identifier',
              flex: 5
            }, {
              xtype: 'button',
              name : 'layerIdentifier_button',
              text: _T_LAYER_WFS('fields.auto'),
              /**
               * auto calculation of layerName field
               */
              handler: function() {
                var ctrlFrom = me.down('[name=layerTitle]');
                var ctrlTo = me.down('[name=layerIdentifier]');
                if ( ctrlFrom && ctrlTo ) {
                  ctrlTo.setValue(convertNameToIdentifier(ctrlFrom.getValue(), 20));
                }
              },
              flex: 1
            }]
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_LAYER_WFS('fields.proj'),
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [{
              xtype: 'comboboxprojections',
              name: 'layerProjectionEpsg',
              editable: false,
              value: Carmen.Defaults.PROJECTION,
              margin: '0 10px 0 0',
              flex: 6
            }, {
              labelAlign : 'right',
              xtype: 'combobox',
              name: 'msGeometryType',
              fieldLabel: _T_LAYER_VECTOR('fields.type'),
              queryMode: 'local',
              displayField: 'displayField',
              valueField: 'valueField',
              store: Carmen.Dictionaries.GEOMETRYTYPES,
              autoLoadOnValue: true,
              autoSelect: true,
              editable: false,
              flex: 6
            },{flex : 0, xtype : 'hiddenfield', name : 'layerDataEncoding'} ]
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_LAYER_WFS('fields.scales'),
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [{
              labelAlign : 'right',
              labelWidth : 60,
              xtype: 'numberfield',
              name: 'layerMinscale',
              fieldLabel: _T_LAYER_WFS('fields.min'),
              allowBlank: false,
              maxLength: 10,
              enforceMaxLength: true,
              minValue: Carmen.Defaults.MINSCALE,
              maxValue: Carmen.Defaults.MAXSCALE,
              hideTrigger: true,
              keyNavEnabled: false,
              mouseWheelEnabled: false,
              allowOnlyWhitespace: false,
              allowDecimals: false,
              allowExponential: false,
              flex: 6,
              margin: '0 10px 0 0'
            }, {
              labelAlign : 'right',
              xtype: 'numberfield',
              name: 'layerMaxscale',
              vtype: '',
              fieldLabel: _T_LAYER_WFS('fields.max'),
              allowBlank: false,
              maxLength: 10,
              enforceMaxLength: true,
              minValue: Carmen.Defaults.MINSCALE,
              maxValue: Carmen.Defaults.MAXSCALE,
              hideTrigger: true,
              keyNavEnabled: false,
              mouseWheelEnabled: false,
              allowOnlyWhitespace: false,
              allowDecimals: false,
              allowExponential: false,
              flex: 6
            }]
        }, {
        xtype:'fieldset',
        title: _t("Layer.window.titleLayerBackground"), //"Paramétrages d'affichages de la couche",
        layout: 'form',
        items : [{
          xtype: 'fieldcontainer',
          layout: 'hbox',
          width: Carmen.Util.WINDOW_WIDTH - 40,
          items: [{
              xtype: 'checkbox',   
              name : 'layerIsBackground',
              fieldLabel: _t("Layer.forms.fields.layerIsBackground"),
              flex: 6, 
              labelWidth : '80%',
              listeners : [{
                      change: function (me, value) {
                          var show_component = false;
                          var is_checked = value; 
                          if(is_checked) {
                              show_component = true;
                          }
                          
                          var backgroundDisplayImage = Ext.ComponentQuery.query('[name=backgroundDisplayImage]')[0]; 
                          backgroundDisplayImage.setVisible(show_component); 
                      }
                }]
          },{
              xtype: 'checkbox',   
              name : 'layerIsDefaultBackground',
              fieldLabel: _t("Layer.forms.fields.layerIsDefaultBackground"),
              flex: 6,
              labelWidth : '80%',
              listeners : [{
                      change: function (me, value) {
                          
                          var mainFrame = Ext.ComponentQuery.query('[name=GTabWFS]')[0]; 
                          var mainFrameRecord = mainFrame.form._record; 
                          var layerId = mainFrameRecord.data.layerId; 
                          var joinedStore = mainFrameRecord.joined; 
                          
                          for(var i = 0 ; i < joinedStore.length; i++ ) {
                              var current_store = joinedStore[i];                               
                              if(current_store.config.model === "Carmen.Model.Layer") {                                 
                                  for(var i = 0 ; i < current_store.data.items.length; i++ ) {
                                      var current_layer = current_store.data.items[i]; 
                                      if(current_layer.id !== layerId ) {
                                          current_layer.data.layerIsDefaultBackground = false; 
                                      }
                                  }
                              }
                          }
                      }
              }]
          }]
        },{
              xtype: 'fieldcontainer',
              layout: 'hbox',
              width: Carmen.Util.WINDOW_WIDTH - 40,
              hidden : true  ,
              name : "backgroundDisplayImage",
              items: [{
                    
                    fieldLabel: _t("Layer.forms.fields.layerBackgroundImage"),
                    name : 'layerBackgroundImage',
                    xtype: 'textfield',
                    labelWidth : '40%',
                    width: Carmen.Util.WINDOW_WIDTH - 90,
                    queryMode: 'local',
                    displayField: 'displayField',
                    valueField: 'valueField',
                    store: Carmen.Dictionaries.LOGOPATH_IMAGES,
                    editable: false,
                    autoLoadOnValue : true,
                    autoSelect : true,
                    flex : 8
              }, { 
                    xtype: 'button',
                    text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
                    name : 'uiLogopath_button',
                    flex : 3    ,
                    handler: function(){
                      var windowClass = "Ext.ux.FileBrowserWindow";
                      var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
                      || Ext.create(windowClass, {
                         id: CMP_REFERENCE(windowClass),
                         selectExtension: '*.jpg,*.gif,*.png',
                         uploadExtension: '.jpg,.gif,.png',
                         dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                         defaultPath: '/Root',
                         relativeRoot: '/Root/IHM',
                         listeners: {  
                           selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath)  {
                             var ctrlTo = me.down('[name=layerBackgroundImage]');
                             ctrlTo.setValue(fullPathName);
                             wind.close();
                           }
                         }
                      });
                      window.show();
                    },

                    listeners : {
                      afterrender : function(){
                        var check = me.down('[name=uiLogo]');
                        if ( check ){check.fireEvent('change', check);}
                      }
                    }
                  }]
          }]
      }]
      }, {
        xtype:'fieldset',
        title: _T_LAYER_WFS('fields.streamParam'),
        margins: 8,
        
        layout: 'form',
        items: [{
          xtype: 'combobox',
          name: 'LayerSavedConnections',
          fieldLabel: _T_LAYER_WFS('fields.savedServ'),
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: me.storeService,
          autoLoadOnValue: true,
          autoSelect: true,
          editable: false,
          listeners: {
            /**
             * update fields : msLayerConnectionName and layerServerUrl with values contents in selected record
             * @param {Ext.form.field.ComboBox} combo
             * @param {Ext.data.Model} record
             */
            select: function(combo, record) {
              var ctrlName = me.down('[name=msLayerConnectionName]');
              var ctrlUrl  = me.down('[name=layerServerUrl]');
              if ( ctrlName && ctrlUrl ) {
                ctrlName.setValue(record.get("wxsName"));
                ctrlUrl.setValue(record.get("wxsUrl"));
              }
            }  
          }

        }, {
          xtype: 'textfield',
          name: 'msLayerConnectionName',
          fieldLabel: _T_LAYER_WFS('fields.nameServ')
          //allowBlank: false,
          /*  recommended that the name not contain spaces, special characters, or begin with a number */
          //vtype: 'identifier'
        }, {
          xtype: 'textfield',
          name: 'layerServerUrl',
          fieldLabel: _T_LAYER_WFS('fields.urlServ'),
          allowBlank: false,
          vtype: 'url',
          listeners: {
            change: function(oThis, newValue, oldValue) {
              var btCall = me.down('[name=btCallWFS]');
              if( btCall ) {
                btCall.setDisabled(newValue != "" ? false : true);
              }
            }
          }
        },{
          xtype: 'textfield',
          name: "layerWxsnameRO",
          fieldLabel:  _T_LAYER_WMS('fields.layerName'),
          readOnly: true,
          flex: 12
        }, {
          xtype: 'fieldcontainer',
          layout: 'hbox',
          anchor: '100%',
          defaults: {
            margin: '0 8 0 0'
          },
          items: [{
            xtype: 'button',
            name : 'btCallWFS',
            text: _T_LAYER_WFS('fields.btCallWFS'),
            disabled: true,
            /**
             * call current URL to load wfs context
             */
            handler: function() {
              me.launchGetCapabilities();
            },
            flex: 2
          }, {
            xtype: 'button',
            hidden : (VISIBILITY_WXS_MANAGE ? false : true),
            name : 'btAddWFS',
            text: _T_LAYER_WFS('fields.btAddWFS'),
            /**
             * add current connection name and url to wfs saved services
             */
            handler: function() {
              var ctrlList = me.down('[name=LayerSavedConnections]');
              var ctrlName = me.down('[name=msLayerConnectionName]');
              var ctrlUrl  = me.down('[name=layerServerUrl]');
              if ( ctrlName && ctrlUrl && ctrlList ) {
                if ( !void(ctrlName.allowBlank = false) && ( !ctrlName.isValid() || !ctrlUrl.isValid() ) && !void(ctrlName.allowBlank = true)) 
                  return;
                ctrlName.allowBlank = true;
                
                Ext.Ajax.request({
                  url: Routing.generate('carmen_ws_post_saved_services')+"/WFS",
                  method: 'POST',
                  params: {
                    wxsName: ctrlName.getValue(),
                    wxsUrl: ctrlUrl.getValue()
                  },
                  success: function (response) {
                    var oRes = eval("("+response.responseText+")");
                    Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WFS('msgApplied.addWFSSavedServ'));
                    ctrlList.getStore().reload();
                    // select added service
                    ctrlList.setValue(oRes.wxsId);
                  },
                  failure: function (response) {
                    if (Carmen.notAuthenticatedException(response) ) return;
                    var oRes = eval("("+response.responseText+")");
                    var msgKey = "addWFSSavedServ";
                    Ext.MessageBox.alert(_T_LAYER_WFS('msgFailure.'+msgKey), (oRes.description || _T_LAYER_WFS('msgFailure.'+msgKey)))
                                  .setIcon(Ext.MessageBox.ERROR);
                  }
                });
              }
            },
            flex: 2            
          }, {
            xtype: 'button',
            name : 'btDeleteWFS',
            hidden : (VISIBILITY_WXS_MANAGE ? false : true),
            text: _T_LAYER_WFS('fields.btDeleteWFS'),
            /**
             * remove from wfs saved services, the current connection name
             */
            handler: function() {
              var ctrlList = me.down('[name=LayerSavedConnections]');
              var ctrlName = me.down('[name=msLayerConnectionName]');
              var ctrlUrl  = me.down('[name=layerServerUrl]');
              if ( ctrlName && ctrlUrl && ctrlList ) {
                Ext.Msg.confirm(
                  new Ext.XTemplate(_t("form.confirmDeleteTitle")).apply(),
                  new Ext.XTemplate(_T_LAYER_WFS("msgApplied.delWFSConfirm")).apply(),
                  function(btn){
                    if (btn=="yes"){
                    
                      Ext.Ajax.request({
                        url: Routing.generate('carmen_ws_delete_saved_services')+"/WFS",
                        method: 'DELETE',
                        params: {
                          wxsId: ctrlList.getSelection().getData().wxsId
                        },
                        success: function (response) {
                          Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_LAYER_WFS('msgApplied.delWFSSavedServ'));
                          ctrlName.setValue("");
                          ctrlUrl.setValue("");
                          ctrlList.setValue("");
                          ctrlList.getStore().reload();
                        },
                        failure: function (response) {
                          if (Carmen.notAuthenticatedException(response) ) return;
                          var oRes = eval("("+response.responseText+")");
                          var msgKey = "delWFSSavedServ";
                          Ext.MessageBox.alert(_T_LAYER_WFS('msgFailure.'+msgKey), (oRes.description || _T_LAYER_WFS('msgFailure.'+msgKey)))
                                        .setIcon(Ext.MessageBox.ERROR);
                        }
                      });
                    }
                  }
                ).icon = Ext.Msg.WARNING;    
              }
            },
            flex: 2
          }]
        }]
      }, {
        xtype:'fieldset',
        name:'fsTree',
        title: _T_LAYER_WFS('fields.titleLayersList'),
        columnWidth: 0.5,
        margin: '0 4 0 0',
        padding: 0,
        height: heightLayerList,
        scrollable: true,
        items: []
      }, {
        xtype:'fieldset',
        name:'fsForm',
        title: _T_LAYER_WFS('fields.titleLayerProperties'),
        columnWidth: 0.5,
        margin: '0 0 0 4',
        padding: '5 10 5 10',
        height: heightLayerList,
        scrollable: 'y',
        items: []
    }];
    
    this.callParent(arguments);
  },
  
  
  /**
   * build the layer form and return its reference
   * @param {object} selectedLayer
   * @returns {Array of Ext Components}
   */
  buildLayerForm: function(selectedLayer) {
    var me = this;
    
    // data of srs store
    var srsData = [];
    var srs = selectedLayer.get("srs");
    
    // current selected projection
    var storeProjections = Carmen.Dictionaries.PROJECTIONS;
    var ctrlUrlSRS = me.down('[name=layerProjectionEpsg]');;
    var currentSRS = ctrlUrlSRS.getValue();
    var currentSRSExists = false;
    
    for(var i=0; i< srs.length; i++){
      var srsr = srs[i].replace(/EPSG:|CRS:|urn:x-ogc:def:crs:EPSG:/, '');
      var record = storeProjections.findRecord('valueField', srsr);
      if ( record ){
          srsData.push({"valueField": srsr, "displayField": record.get('displayField')});
      } else {
          //srsData.push({"valueField": srsr, "displayField": srsr});
      }
      //currentSRSExists = ( currentSRSExists || ( srsr==currentSRS ) ? true : false );
    }
    
    if( !currentSRSExists && srs.length>0 ) {
      currentSRS = srs[0].replace(/EPSG:|CRS:|urn:x-ogc:def:crs:EPSG:/, '');
    }
    

    // projections list from the selected layer
    var storeSRS = new Ext.data.JsonStore({
      fields: [{name : 'valueField', type : 'number'}, {name : 'displayField'}],
      sorters : [{property : 'valueField', direction  : 'ASC'}],
      autoload: true,
      data: srsData,
      proxy: {
        type: 'memory'
      }
    });
    
    var items = [{
      xtype: 'fieldcontainer',
      layout: 'hbox',
      anchor: '98%',
      defaults: {
        margin: '0 8 0 0'
      },
      items: [{
        xtype: 'displayfield',
        value: '',
        flex: 4  
      }, {
        xtype: 'button',
        name : 'btApply',
        text: _T_LAYER_WFS('fields.btApply'),
        flex: 1,
        handler: function(oThis) {
          me.btApplyAction();
        }
      }]
    }, {
      xtype: 'hiddenfield',
      name: "layerServerVersion",
      value: selectedLayer.get("version")
    }, {
      xtype: 'displayfield',
      name: "layerWxsname",
      fieldLabel: _T_LAYER_WFS('fields.layerName'),
      value: selectedLayer.get("lname")
    }, {
      xtype: 'displayfield',
      name: "wfsTitle",
      fieldLabel: _T_LAYER_WFS('fields.layerTitle'),
      value: selectedLayer.get("text")
    }, {
      xtype: 'combobox',
      name: 'wfsSrs',
      fieldLabel: _T_LAYER_WFS('fields.layerSRS'),
      queryMode: 'local',
      width : '95%',
      displayField: 'displayField',
      valueField: 'valueField',
      store: storeSRS,
      autoLoadOnValue: true,
      autoSelect: true,
      editable: false,
      value: currentSRS
    }];
    
    return items;
  },
  
  
  loadRecord : function(record) {
    var ret = this.callParent(arguments);

    var components = ['msLayerData', 'layerServerUrl'];
    var extensions = ['', '_button'];
    this.setStateFields(record, components, extensions);
    
    components = ['LayerSavedConnections', 'msLayerConnectionName', 'btCallWFS', 'fsTree', 'fsForm'];
    if(VISIBILITY_WXS_MANAGE){
      components.push('btAddWFS', 'btDeleteWFS'); 
    }
    
    extensions = [''];
    this.setStateFields(record, components, extensions, "hide");
    
   components = ['layerWxsnameRO'];
    extensions = [''];
    this.setStateFields(record, components, extensions, "show");
    
    this.on('afterrender', function(){
      var msGeometryType = this.down('[name=msGeometryType]').getValue();
      var layerDataEncoding = this.down('[name=layerDataEncoding]').getValue();
    }, this);
    if ( this.rendered ) this.fireEvent('afterrender');
    return ret;
  },
  
  /**
   * 
   * @param {Array} featureTypes
   * @param {String} version
   * @returns {Array}
   */
  convertFeatureTypesToTree : function(featureTypes, version) {
    var res = [];

    for(var i=0; i<featureTypes.length; i++){
      var featureType = featureTypes[i];
      
      var layer = {};
      var prefix = '';
      if(typeof(featureType.featureNS)!=='undefined' ){
        var domainsPart = featureType.featureNS.split(/\//);
        prefix = domainsPart[domainsPart.length-1]+ ':' ;
      }
      layer.lname   = prefix + featureType.name;
      layer.text    = featureType.title;
      layer.version = version;
      layer.srs = new Array();
      //put in version provider:code
      layer.srs.push(featureType.srs.replace("urn:ogc:def:crs:", "").replace("::", ":"));
      if(featureType.OtherSRS){
        for (var j=0; j<featureType.OtherSRS.length; j++){
          layer.srs.push(featureType.OtherSRS[j].replace("urn:ogc:def:crs:", "").replace("::", ":"));
        }
      }
      layer.leaf    = true;

      res.push(layer);
    }
    return res;
  },
  
   /**
    * apply the params of the selected layer to generic sheet
    */
  btApplyAction: function() {
  	var me = this;
    var ctrlNames = {
      "layerTitle"          : ["wfsTitle"], 
      "layerName"           : ["layerWxsname", true],
      "layerIdentifier"         : ["layerWxsname", true], 
      "layerProjectionEpsg" : ["wfsSrs"]
    };
    
    
    var availableSRS = me.down('[name=wfsSrs]').getStore().collect('valueField');
    var comboStore = me.down('comboboxprojections').getStore();
    comboStore.clearFilter();
    comboStore.filterBy(function(projection){
      return availableSRS.indexOf(parseInt(projection.get('valueField')))!=-1;
    });
    
    
    Ext.iterate(ctrlNames, function(nameDest, configSrc){
      var nameSrc  = configSrc[0];
      var bConvert  = (configSrc.length>1 ? configSrc[1] : false);
      
      var ctrlSrc  = me.down('[name='+nameSrc+']');
      var ctrlDest = me.down('[name='+nameDest+']');
      if( ctrlSrc && ctrlDest ) {
        if( bConvert ) {
          ctrlDest.setValue(convertNameToIdentifier(ctrlSrc.getValue(), 20));
        } else {  
          ctrlDest.setValue(ctrlSrc.getValue());
        }
      }
    });
    me.getMsGeometryType();
  },
  
  /**
   * Destroy all items of the component identified by its name
   * @param {string} name  name of component
   * @return {Component}
   */
  destroyItemsComponent: function(name) {
    var me = this;
    var idx = me.items.findIndex("name", name);
    var component = ( idx>=0 ? me.items.getAt(idx) : null );
    if( component ) {
      while(component.items.length>0 ) {
        component.items.getAt(0).destroy();
      }
    }
    return component;
  },
  
  /**
   * build the layers tree and return its reference
   * @param {objet} rootTree
   * @param {int}   height
   * @param {int}   width
   * @returns {Ext.tree.TreePanel}
   */
  buildLayersTree: function(rootTree) {
    var me = this;
    var tree = new Ext.tree.TreePanel({
      animate:true,
      width: "auto",
      name: "layersTree",
      hideHeaders : true,
      viewConfig : {
        emptyText : _T_LAYER_WFS('treeEmptyText')
      },
      root: rootTree,
      selModel: new Ext.selection.TreeModel({
        listeners: {
          selectionchange: function (selection, data) {
            if(data[0].get("leaf")==true){
            var oFsForm = me.destroyItemsComponent("fsForm");
            if( oFsForm ) {
                var itemsLayerForm = me.buildLayerForm(data[0]);
              for(var i=0; i<itemsLayerForm.length; i++) {
                oFsForm.add(itemsLayerForm[i]);
              }
            }
          }
        }
        }
      })
    });
    return tree;
  },
  
  /**
   * parses the response of getCapabilities and writes the tree of layers
   * @param {string} response
   */
  parseWFSCapabilities: function(response) {
    var wfsCpblt = new OpenLayers.Format.WFSCapabilities("1.1.0");
    var capabilities = wfsCpblt.read(response.responseText);
    if (capabilities.featureTypeList && capabilities.featureTypeList.featureTypes) {
      var featureTypes = capabilities.featureTypeList.featureTypes;

      var layersTree = this.convertFeatureTypesToTree(featureTypes, capabilities.version);
      if( layersTree.length>0 ) {
        if(capabilities.version == "1.0.0"){
        var rootTree = {
          expanded : true,
          text        : capabilities.service.title,
          abstract    : capabilities.service.abstract,
          version     : capabilities.version,
          children    : layersTree,
          lname       : "",
          srs         : "",
          leaf        : false
        };
        }else{//version 1.1.0 assumed
          var rootTree = {
              expanded : true,
              text        : capabilities.serviceIdentification.title,
              abstract    : capabilities.serviceIdentification.abstract,
              version     : capabilities.version,
              children    : layersTree,
              lname       : "",
              srs         : "",
              leaf        : false
            };
        }
        
        this.destroyItemsComponent("fsForm");
        var oFsTree = this.destroyItemsComponent("fsTree");
        if( oFsTree ) {
          var layersTree = this.buildLayersTree(rootTree);
          oFsTree.add(layersTree);
        }
      }
    }
  },
  
  /**
   * Launch the getCapabilities action of openlayer 
   */
  launchGetCapabilities: function() {
    var ctrlUrl  = this.down('[name=layerServerUrl]');
    var url = ctrlUrl.getValue();
    var me = this;
    
    Ext.MessageBox.show({ 
      msg: _T_LAYER_WFS('msgApplied.progressTitle'), 
      progressText: _T_LAYER_WFS('msgApplied.progressMsg'), 
      width:300, 
      wait:true, 
      waitConfig: {interval:200}
    });
    
    OpenLayers.ProxyHost = Routing.generate('carmen_ws_get_layer_information')+"/";
    
    url = url.split('?');
    var params = Ext.Object.fromQueryString((url.length>1 ? url[1] : ""));
    url = url[0];
    
    // launching getCapabilities request
    OpenLayers.Request.GET({
      url: encodeURIComponent(url),
      params: Ext.apply(params, {
        SERVICE: "WFS",
        VERSION: "1.1.0", //now request in 1.1.0 version
        REQUEST: "GetCapabilities"
      }),
      success: function(response) {
        Ext.MessageBox.hide();
        me.parseWFSCapabilities(response);
      },
      failure: function(response) {
        if (Carmen.notAuthenticatedException(response) ) return;
        Ext.MessageBox.hide();
        var oRes = eval("("+response.responseText+")");
        
        var msgKey = "getCapabilities";
        Ext.MessageBox.alert(_T_LAYER_WFS('msgFailure.'+msgKey), (oRes.description || _T_LAYER_WFS('msgFailure.'+msgKey)))
                      .setIcon(Ext.MessageBox.ERROR);
      }
    });
  },
  
  /**
   * get layer type from selected file
   * @param {string} selectedFile
   * @returns {undefined}
   */
  getMsGeometryType : function(selectedFile) {
    var msGeometryType = this.down('[name=msGeometryType]');
    var layerDataEncoding = this.down('[name=layerDataEncoding]');
    var vars = ['layerServerUrl', 'layerWxsname'];
    var tpl = new Ext.XTemplate("WFS:{layerServerUrl}@{layerWxsname}");
    var apply = {};
    for (var i=0; i<vars.length; i++){
      var ctrl = this.down('[name='+vars[i]+']');
      if ( !ctrl ) return;
      apply[vars[i]] = ctrl.getValue();
    }
    msGeometryType.mask();
    Ext.Ajax.request({
    	timeout : 3*60*1000,
      url: Routing.generate('carmen_ws_helpers_geometrytype', {
        layertype : "WFS",
        layerfile: encodeURIComponent(tpl.apply(apply))
      }),
      method: 'GET',
      success: function (response) {
      	msGeometryType.unmask();
        var oRes = eval("("+response.responseText+")");
        msGeometryType.setValue(oRes.msGeometryType);
        layerDataEncoding.setValue(oRes.layerDataEncoding);
      },
      failure: function (response) {
        if (Carmen.notAuthenticatedException(response) ) return;
        msGeometryType.unmask();
        var oRes = eval("("+response.responseText+")");
        var msgKey = "canGetLayerTypeFromSelectedFile";
        Ext.MessageBox.alert(_T_LAYER_VECTOR('msgFailure.'+msgKey), (oRes.description || _T_LAYER_VECTOR('msgFailure.'+msgKey)))
                      .setIcon(Ext.MessageBox.ERROR);
      }
    });
  }
  
});