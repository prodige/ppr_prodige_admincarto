var _T = function(key){
  return _t("Account."+key);
};
function getPreferencesForm(){
	return {
    xtype : 'cmngenericform',
    width : '100%',
    id : 'form-preferences',
    successCallback : function(form, record) {
      // update Carmen.user.preferences with the loaded record
      form.loadRecord(record);
      Carmen.user.preferences = record;
    },
    items : [{
      xtype : 'fieldset',
      title : _T('forms.preferences.formTitle'),
      layout : {
      	type : 'table',
      	tableAttrs : {width : '100%', cellspacing : 10},
      	columns : 2
      },
      defaults : {
        labelSeparator : '',
        labelAlign : 'left',
        width : '100%'
      },
      items : [{
        colspan : 2,
        fieldLabel : _T('forms.preferences.fields.preferenceSrs'),
        name : 'preferenceSrs',
        xtype : 'comboboxprojections'
      },
      /////////////////////////
      {
        colspan : 2,
        xtype : 'label',
        text : _T('forms.preferences.extra.preferencesExtent')
      }, {
        xtype : 'numberfield',
        fieldLabel : _T('forms.preferences.fields.preferencesExtentXmin'),
        name : 'preferencesExtentXmin'
      }, {
        xtype : 'numberfield',
        fieldLabel : _T('forms.preferences.fields.preferencesExtentYmin'),
        name : 'preferencesExtentYmin'
      },{
        xtype : 'numberfield',
        fieldLabel : _T('forms.preferences.fields.preferencesExtentXmax'),
        name : 'preferencesExtentXmax'
      }, {
        xtype : 'numberfield',
        fieldLabel : _T('forms.preferences.fields.preferencesExtentYmax'),
        name : 'preferencesExtentYmax'
      },
      //////////////////
      {
        colspan : 2,
        xtype : 'label',
        text : _T('forms.preferences.extra.preferencesScales')
      }, 
      {
        xtype : 'numberfield',
        fieldLabel : _T('forms.preferences.fields.preferenceMinscale'),
        name : 'preferenceMinscale'
      }, {
        xtype : 'numberfield',
        fieldLabel : _T('forms.preferences.fields.preferenceMaxscale'),
        name : 'preferenceMaxscale'
      },
      //////////////////
      {
        colspan : 2,
        xtype : 'label',
        text : _T('forms.preferences.extra.preferencesCustom')
      }, {
        fieldLabel : _T('forms.preferences.fields.preferenceOutputformat'),
        name : 'preferenceOutputformat',
        xtype : 'combobox',
        queryMode : 'local',
        displayField : 'displayField',
        valueField : 'valueField',
        store : Carmen.Dictionaries.OUTPUTFORMATS,
        autoLoadOnValue : true,
        autoSelect : true
      }, {
        fieldLabel : _T('forms.preferences.fields.preferenceUnits'),
        name : 'preferenceUnits',
        xtype : 'combobox',
        queryMode : 'local',
        displayField : 'displayField',
        valueField : 'valueField',
        store : Carmen.Dictionaries.UNITS,
        autoLoadOnValue : true,
        autoSelect : true
      },
      //////////////////
      {
        fieldLabel : _T('forms.preferences.fields.preferenceBackgroundColor'),
        name : 'preferenceBackgroundColor',
        layout : 'hbox',
        xtype : 'colorfield',
        defaults : {
          showSliderH : true,
          showSliderS : true,
          showSliderV : true,
          showSliderA : false
        }
      }, {
        xtype : 'checkboxfield',
        fieldLabel : _T('forms.preferences.fields.preferenceBackgroundTransparency'),
        name : 'preferenceBackgroundTransparency',
        checked : true
      }]
    }]
  }
}

function getContactForm(){
	var defaults = {
    xtype : 'textfield',
    labelAlign : 'left',
    labelWidth : 130,
    padding : '10 0 0 10',
    // width: 275,
    labelSeparator : ''
  };
  var flexTotal = 12, flexHalf = 5.5;
  var fieldContainer2Columns = function(field1, field2){ 
    field1 = Ext.apply({flex:flexTotal}, field1);
    if ( (flexTotal - field1.flex) && !field2 ){
      field2 = {xtype:'panel', html:''};
    }
    if ( field2 ){
      field1.flex = flexHalf;
      field2 = Ext.apply({flex:flexHalf, margin : '0 0 0 10'}, field2);
    }
      
    var remainder = 0;
    var items = [field1];
//    if ( (remainder = (flexTotal - field1.flex - (field2 ? field2.flex : 0))) ){
//      items.push({xtype:'panel', html : '', flex : remainder});
//    }
    if ( field2 )
      items.push(field2)
    return {
      xtype : 'fieldcontainer',
      defaults : Ext.apply(defaults, {}),
      items : items
    }
  };
	return {
    xtype : 'cmngenericform',
    width : '100%',
    layout : {
      type : 'table',
      columns : 2,
      tableAttrs : {width : '100%', style : {'table-layout' : 'fixed'}},
      
      tdAttrs : {style : {'vertical-align':'middle !important'}}
    }, labelWidth : 120,
    defaults : {colspan : 2, width : '100%', labelWidth : 120},
    defaultType : 'textfield',
    id : 'form-contact',
    items : [
      {
        fieldLabel : _T('forms.contact.fields.contactOrganization'),
        name : 'contactOrganization'
      },
      {
        padding : '5 0 5 0',
        fieldLabel : _T('forms.contact.fields.contactName'),
        name : 'contactName'
      },
      {
        fieldLabel : _T('forms.contact.fields.contactPosition'),
        name : 'contactPosition'
      },
      {
        padding : '5 0 5 0',
        fieldLabel : _T('forms.contact.fields.contactAddress'),
        name : 'contactAddress'
      },
      {
        colspan : 1,
        fieldLabel : _T('forms.contact.fields.contactZipcode'),
        name : 'contactZipcode'
      }, {colspan : 1,xtype : 'fieldcontainer'},
      {
      	colspan : 1,
        padding : '5 0 0 0',
        fieldLabel : _T('forms.contact.fields.contactCity'),
        name : 'contactCity'
      }, {
        colspan : 1,
        labelAlign : 'right',
        fieldLabel : _T('forms.contact.fields.contactEmail'),
        name : 'contactEmail',
        vtype : 'email'
      },
      {
        colspan : 1,
        fieldLabel : _T('forms.contact.fields.contactUnit'),
        name : 'contactUnit'
      }, {
        colspan : 1,
        labelAlign : 'right',
        fieldLabel : _T('forms.contact.fields.contactPhone'),
        name : 'contactPhone'
      }, 
      {
        colspan : 1,
        fieldLabel : _T('forms.contact.fields.contactCountry'),
        name : 'contactCountry'
      },{
        colspan : 1,
        labelAlign : 'right',
        fieldLabel : _T('forms.contact.fields.contactFax'),
        name : 'contactFax'
      }
    ]
  }
}
function getProfilForm(){
	return {
            xtype: 'cmngenericform',
            layout: 'column',
            id: 'form-profil',
    items : [{
                  columnWidth: 1,
                  defaults: {
                       layout: 'form',
                       xtype: 'textfield',
                       padding: '10 0 0 10',
                       labelAlign: 'left',
                       labelWidth: 150,
                       width: 275,
                       labelSeparator: ''
                  },
      items : [{
                      fieldLabel: _T('forms.profil.fields.accountName'), 
                      name: 'accountName', 
                      width: '90%'
      }, {
                      fieldLabel: _T('forms.profil.fields.accountId'),
                      xtype: 'displayfield',
                      value: Carmen.user.account.get('accountId')
      }, {
                      fieldLabel: _T('forms.profil.fields.accountPath'),
                      xtype: 'displayfield',
                      value: Carmen.user.account.get('accountPath')
      }, {
                      fieldLabel: _T('forms.general.user'),
                      xtype: 'displayfield',
                      value: Carmen.user.username
      }, {
        fieldLabel : _T('forms.profil.fields.accountGeonetworkurl'),
        xtype : 'displayfield',
        width : '90%',
        value : Carmen.user.account.get('accountGeonetworkurl')
      }, {
                      fieldLabel: _T('forms.profil.fields.accountHostpostgres'),
                      xtype: 'displayfield',
                      width: '100%',
        value : 'host='
            + Carmen.user.account.get('accountHostpostgres') + ' port='
            + Carmen.user.account.get('accountPortpostgres')
            + ' dbname=' + Carmen.user.account.get('accountDbpostgres')
            + ' user=' + Carmen.user.account.get('accountUserpostgres')
      }]
          }]
      }
    }
/**
 * Carmen.Window.Account
 */
Ext.define('Carmen.Window.Account', {
  extend : 'Carmen.Generic.Window',
  id : 'window-account',
  width: 640,
  title : _T('window.title'),
  closable : true,
  layout : 'fit',
  items : [{
    xtype : 'tabpanel',
    tabRotation : 0,
    items : [{
      xtype : 'panel',
      title : _T('forms.preferences.title'),
      layout : 'anchor',
      height : 'auto',
      width : '100%',
      scrollable : 'y',
      items : [getPreferencesForm()]
    }, {
      xtype : 'panel',
      title : _T('forms.contact.title'),
      hidden: (!VISIBILITY_ACCOUNT_FORMS_CONTACT_TITLE ? true : false),
      layout : 'anchor',
      height : 'auto',
      width : '100%',
      scrollable : 'y',
      successCallback : function(form, record) {
        // update Carmen.user.contact with the loaded record
        form.loadRecord(record);
        Carmen.user.contact = record;
      },
      items : [getContactForm()]
    }, {
      xtype : 'panel',
      title : _T('forms.profil.title'),
	  hidden: (!VISIBILITY_ACCOUNT_FORMS_PROFILE_TITLE ? true : false),
      layout : 'anchor',
      height : 'auto',
      width : '100%',
      scrollable : 'y',
      items : [getProfilForm()]
    }]
  }],
  listeners: {
    show: function(win) {    	
      var form = Ext.getCmp('form-preferences');
      form.loadRecord(Carmen.user.preferences);
      
      form = Ext.getCmp('form-contact');
      form.loadRecord(Carmen.user.contact);
      
      form = Ext.getCmp('form-profil');
      form.loadRecord(Carmen.user.account);
    }
  },
  
  saveAllForms : function() {
    var win = this;
    var forms = win.getForms();
    var nbForms = forms.length;
    
    // iterating over forms to save 
    var fields = {};
    var isValid = true;
    Ext.each(forms, function(form, index){
      if ( form.isValid() ){
        var record = form.getRecord();
            if (!record)
              return;
        form.updateRecord(record);
        record.save({
        	success : function(){
        		if ( index==nbForms-1 ){
        			//success of submit the last form
                      Carmen.Notification.msg(win.title, _t("form.msgApplied"),
                          2);
        			win.close();
        		}
        	},
                  failure : Carmen.readFormFailure
        });
        return isValid;
      } else {
        win.down('tabpanel').setActiveItem(form);
        isValid = false;
        return false;
      }
    });
  }
});


/**
 * Compose the Extent form editor for a map UI
 *  
 * @return Ext.form.FieldContainer
 */
function getExtentFields() {
  var defaultsNumber = {
        labelAlign: 'right',
        labelSeparator: '',
        labelWidth: 'auto',
        xtype : 'numberfield',
        allowDecimals : true,
        allowBlank : false
      };
  return Ext.apply(Ext.apply({}, Carmen.Util.defaultFormConfig), {
    xtype: 'fieldcontainer',
    fieldLabel : _T('forms.preferences.extra.preferenceExtent'),
    layout: {
      type: 'table',
      // The total column count must be specified here
      columns: 2
    },
    
    defaults : {
      labelAlign: 'right',
      labelWidth: 70,

          padding : '10 0 0 10',
      labelSeparator: ''
    },
    
    items : [
    //row1
    {
      layout :'column', 
      defaults : defaultsNumber,
      items:[{
        fieldLabel : _T('forms.preferences.fields.preferenceExtentXmin'),
        name : 'preferenceExtentXmin'
      }, {
        fieldLabel : _T('forms.preferences.fields.preferenceExtentYmin'),
        name : 'preferenceExtentYmin'
      }]
    }
    //row2
    , {}, {
      layout :'column', 
      defaults : defaultsNumber,
      items:[{
        fieldLabel : _T('forms.preferences.fields.preferenceExtentXmax'),
        name : 'preferenceExtentXmax'
      }, {
        fieldLabel : _T('forms.preferences.fields.preferenceExtentYmax'),
        name : 'preferenceExtentYmax'
      }]
    }]
  })
}