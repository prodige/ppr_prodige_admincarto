/**
 * Carmen.Model.Preferences
 */
Ext.define('Carmen.Model.Preferences', {
  extend: 'Ext.data.Model',
  idProperty : 'preferenceId',
  fields: [
    {name : 'preferenceId', persist : false}
  , {name : 'user'                             , defaultValue : Carmen.user.userId}
  , {name : 'preferenceSrs'                    , type:'int',     defaultValue : Carmen.Defaults.PROJECTION}
  , {name : 'preferenceMinscale'               , type:'int',     defaultValue : Carmen.Defaults.MINSCALE}
  , {name : 'preferenceMaxscale'               , type:'int',     defaultValue : Carmen.Defaults.MAXSCALE}
  , {name : 'preferenceOutputformat'           ,                 defaultValue : Carmen.Defaults.OUTPUTFORMAT}
  , {name : 'preferenceUnits'                  ,                 defaultValue : Carmen.Defaults.UNITS}
  , {name : 'preferenceBackgroundColor'        , type:'string',  defaultValue : Carmen.Defaults.BGCOLOR}
  , {name : 'preferenceBackgroundTransparency' , type:'boolean', defaultValue : Carmen.Defaults.TRANSPARENCY}
  , {name : 'preferencesExtentXmin'            , type:'float',   defaultValue : Carmen.Defaults.EXTENT.XMIN}
  , {name : 'preferencesExtentYmin'            , type:'float',   defaultValue : Carmen.Defaults.EXTENT.YMIN}
  , {name : 'preferencesExtentXmax'            , type:'float',   defaultValue : Carmen.Defaults.EXTENT.XMAX}
  , {name : 'preferencesExtentYmax'            , type:'float',   defaultValue : Carmen.Defaults.EXTENT.YMAX}
  ],
  proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_entity', {'entity':'preferences'})
  }
});

/**
 * window.onload - creates a record based on the pre-loaded user preferences
 */
{
  // WARNING : preferences.user_id is mapped to Preferences.user, not Preferences.userId !
  Carmen.user.preferences = Ext.create('Carmen.Model.Preferences', Carmen.user.preferences || {user : Carmen.user.userId});
}

/**
 * Carmen.Model.Contact
 */
Ext.define('Carmen.Model.Contact', {
  extend: 'Ext.data.Model',
  idProperty : 'contactId',
  fields: [
    {name : 'contactId', persist : false}
  , {name : 'user'     , defaultValue : Carmen.user.userId}
  , {name : 'contactOrganization' , type:'string'}
  , {name : 'contactName'         , type:'string'}
  , {name : 'contactPosition'     , type:'string'}
  , {name : 'contactAddress'      , type:'string'}
  , {name : 'contactZipcode'      , type:'string'}
  , {name : 'contactCity'         , type:'string'}
  , {name : 'contactUnit'         , type:'string'}
  , {name : 'contactCountry'      , type:'string'}
  , {name : 'contactEmail'        , type:'string'}
  , {name : 'contactPhone'        , type:'string'}
  , {name : 'contactFax'          , type:'string'}
  ],
  proxy: {
      type: 'rest',
      url: Routing.generate('carmen_ws_entity', {'entity':'contact'})
  }
});

/**
 * window.onload - creates a record based on the pre-loaded user account
 */
{
  Carmen.user.contact = Ext.create('Carmen.Model.Contact', Carmen.user.contact || {user : Carmen.user.userId});
}

/**
 * @mode  remote
 */
Ext.define('Carmen.Model.AccountDns', {
  extend: 'Ext.data.Model',
  idProperty : 'dnsId',
  fields : [
    {name : 'dnsId'}
  , {name : 'dnsUrl'}
  , {name : 'dnsPrefixBackoffice'}
  , {name : 'dnsPrefixFrontoffice'}
  , {name : 'dnsPrefixData'}
  , {name : 'dnsPrefixMetadata'}
  , {name : 'dnsScheme'}
  , {name : 'accountGeonetworkurl'}
  ],
  proxy: {
    type: 'memory',
    reader: {
      type: 'json'
    }
  }
});
/**
 * Carmen.Model.Account
 */
Ext.define('Carmen.Model.Account', {
  extend: 'Ext.data.Model',
  idProperty : 'accountId',
  fields: [
    {name : 'acountId'    , persist : false}
  , {name : 'accountName' , type:'string'}
  , {name : 'accountPath' , persist : false}// TODO needed ???
  , {name:  'accountDbpostgres', persist:false}
  , {name:  'accountHostpostgres', persist:false}
  , {name:  'accountPortpostgres', persist:false}
  , {name:  'accountUserpostgres', persist:false}
  , {name : 'accountGeonetworkurl', type:'string' } 
  , {name : 'accountDns', persist : false
    , convert : function(value){
      value = value || {};
      value = (value instanceof Carmen.Model.AccountDns ? value : Ext.create('Carmen.Model.AccountDns', value));
      return value;
    }
  }
  ],
  proxy: {
    type: 'rest',
    url: Routing.generate('carmen_ws_entity', {'entity':'account'})
  }
});

/**
 * window.onload - creates a record based on the pre-loaded user account
 */
{
  Carmen.user.account = Ext.create('Carmen.Model.Account', Carmen.user.account || {});
}
