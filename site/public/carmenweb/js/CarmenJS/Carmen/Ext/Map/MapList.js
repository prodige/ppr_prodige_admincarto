/**
 * Translate text classified by prefix (fixed) and key
 * @param string key
 * @return string
 */
var _T_MAPLIST = function(key){
  return _t("Map.list."+key);
};

var MAP_TYPE_MAP = "MAP";
var MAP_TYPE_MODEL = "MODEL";


///////////////////////////////////////////
// Demo window
///////////////////////////////////////////
var CMP_NAME = 'Carmen.Map.List';
Ext.define('Carmen.Map.List', {
  xtype: CMP_REFERENCE(CMP_NAME),
  extend: 'Ext.grid.Panel', 
  plugins : [{
    ptype: 'cellediting', 
    clicksToEdit: 2,
    listeners : {
      beforeedit : function(plugin, config){
        var record = config.record;
        switch(config.column.name || config.column.dataIndex){
          case 'mapProjectionEpsg' :
            config.value = String(config.value);
          break;
          case 'mapScales' :
            config.field = config.column.name;
            config.originalValue = {
              'mapMinscale' : record.get('mapMinscale'),
              'mapMaxscale' : record.get('mapMaxscale')
            };
            config.value = Ext.apply({}, config.originalValue);
          break;
        }
        return true;
      },
      validateedit : function(plugin, config){
        var record = config.record;
        var value  = config.value;
        switch(config.column.name || config.column.dataIndex){
          case 'mapFile' :
            var exists = config.grid.getStore().findBy(function(compare){
              return compare.getId()!=record.getId() && compare.get('mapFile')==value;
            });
            exists = exists!=-1;
            if ( exists ){
              var qtip = new Ext.tip.QuickTip();
              qtip.register({
                 target: config.row, // Target button's ID
                 title : _T_MAPLIST('edit.errorTitle'),  // QuickTip Header
                 text  : _T_MAPLIST('edit.errorText.mapFile'), // Tip content  
                 dismissDelay : 2000
             });
             qtip.on('hide', function(){
               this.unregister(config.row);
             })
             qtip.show();
            }
            return !exists;
          break;
          case 'mapProjectionEpsg' :
            config.value = String(config.value);
          break;
          case 'mapScales' :
            var dirty = !( (Ext.num(value.mapMinscale, 0) == Ext.num(record.get('mapMinscale'), 0))
                        && (Ext.num(value.mapMaxscale, 0) == Ext.num(record.get('mapMaxscale'), 0))
                        );
            if ( dirty ){
              record.set('mapMinscale', value.mapMinscale);
              record.set('mapMaxscale', value.mapMaxscale);
              record.cancel = false;
              record.dirty = true;
            }
            config.field = config.column.name || config.column.dataIndex;
          break;
        }
        return true;
      }
    }
  }],
  
  flex : 1,
  height : 350,
  width : '100%',

  shrinkWrapDock : true,
  columnLines: true,
  frame: true,
  
  /*SPECIFIC ATTRIBUTES*/
  selected_record : null,
  datatype : MAP_TYPE_MAP,
  
  
  setSelectedRecord : function(record){this.selected_record = record;},
  
  getSelectedRecord : function(){return this.selected_record;},

  initComponent : function(){
    var me = this;
    
    me.title = _T_MAPLIST('grid'+(me.datatype.toLowerCase())+'s.title');
    
    me.viewConfig = {
      cellTpl: [
        '<td class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\\"" + Ext.id() + "\\"" : ""]} ' +
        ' style="width:<tpl if="values.classes.indexOf(\'x-grid-cell-first\') != -1">{column.cellWidth -1}<tpl else>{column.cellWidth}</tpl>px;' +
        '<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}">',
            '<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
                'style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>{value}</div>',
        '</td>', {
            priority: 0
        }
      ],
      stripeRows : true,
      emptyText : _T_MAPLIST('grid'+(me.datatype.toLowerCase())+'s.emptyText'),
      listeners : {
      	refresh : function(){
      		var store = me.getStore();
      		var records = store.getRange();
      		Ext.each(records, function(record){
            var height = 0;
      		  var tables = Ext.query('table[data-recordid="'+record.internalId+'"]', false);
      		  Ext.each(tables, function(table){height = Math.max(height, table.getHeight())});
      		  Ext.each(tables, function(table){table.setHeight(height)});
      		})
      	}
      }
    };
    
    me.store = {
      model: 'Carmen.Model.Map',
      autoLoad : (me.datatype!=MAP_TYPE_MODEL),
      remoteFilter : true,
      remoteSort : true,
      sorters : ['mapFile'],
      filters: [{
        property: 'mapModel',
        value: (me.datatype==MAP_TYPE_MODEL)
      },{
        property: 'publishedMap',
        value: null
      }],
      listeners : {
        load : function(){
          this.setRemoteSort(false);
        },
        single : true
      }
    };
    
    me.columns = {
      defaults : {
        hideable : false,
        menuDisabled : true
        , stopSelection: true
        , width : 120
        , renderer : function(value, meta){
          meta.css = "x-wrap-cell";
          return value;
        }
      },
      items: [
        {dataIndex : "mapId", xtype:'widgetcolumn'
        , width : 40
        , stopSelection :true 
        , locked : true
        , widget : {
            xtype:'radio'
          , name:"mapId"
          , value : false
          , stopSelection :true
          , listeners: {
              change : function(radio, checked){
                var record = radio.getWidgetRecord();
                if ( !checked && me.getSelectedRecord()==record ) {
                  me.setSelectedRecord(null)
                }
                if ( checked ){
                  me.setSelectedRecord(record);
                }
              }//change
            }//listeners
          }//widget
        }//widgetcolumn
      , {dataIndex : "mapFile"           , header : _T_MAPLIST("columns.mapFile")   
        , renderer : function(value, meta){ meta.css = "x-wrap-cell"; return meta.qtip = (me.datatype==MAP_TYPE_MAP ? "" : "modele_") + value + ".map"; }
        , editor : {xtype:'textfield', allowBlanks:false, stopSelection: true}
        , menuDisabled : false
        , locked : true
        , width : 200
        }
      , {dataIndex : "mapTitle"          , header : _T_MAPLIST("columns.mapTitle")         
        , width : 200
        , editor : {xtype:'textfield', allowBlanks:false}
        , menuDisabled : false
        , locked : true
        }
      , {dataIndex : "mapSummary"        , header : _T_MAPLIST("columns.mapSummary")       , editor : {xtype:'textarea'}      
        , width : 150
        }
      , {dataIndex : "mapProjectionEpsg" , header : _T_MAPLIST("columns.mapProjectionEpsg"), editable:true
        
        , renderer : function(value, meta, record){
            meta.css = "x-wrap-cell";
            return (value ? Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', value).get('displayField') : "");
          }
        , editor : {
            xtype : 'comboboxprojections',
            matchFieldWidth : false,
            withCursor : true
          }
        }
      , { name : 'mapScales', header : _T_MAPLIST("columns.mapScale")
        , renderer : function(value, meta, record, rowIndex){
            return new Ext.XTemplate('1/{mapMinscale}<br>1/{mapMaxscale}').apply(record.data);
          }         
        , editor: Ext.apply(
            Carmen.getScaleFields('map', 'Map.list.', {width : 100})
            , {/*
              autoSize: {
                width: 'field', // Width from the field
                height: 'field' // Height from the boundEl
              },*/
              autoSize : function(){this.setWidth(230)},
              width : 230,
              padding : null,
              layout:'form',
              labelAlign : 'top',
              anchor : '100%',
              style: 'background:#fff;',
              originalValue : {},
              
              resetOriginalValue : function(){this.setValue(this.originalValue);},
              setValue : function(value){
                value = {
                  mapMinscale : Carmen.getFirstDefined(value.mapMinscale , Carmen.user.preferences.get('preferenceMinscale') , Carmen.Defaults.MINSCALE),
                  mapMaxscale : Carmen.getFirstDefined(value.mapMaxscale , Carmen.user.preferences.get('preferenceMaxscale') , Carmen.Defaults.MAXSCALE)
                };
                this.originalValue = value;
                this.down('[name=mapMinscale]').setValue(value.mapMinscale);
                this.down('[name=mapMaxscale]').setValue(value.mapMaxscale);
              },
              getValue : function(){
                return {
                  mapMinscale : this.down('[name=mapMinscale]').getValue(),
                  mapMaxscale : this.down('[name=mapMaxscale]').getValue()
                }
              },
              isValid : function(){
                var value = this.getValue();
                return Ext.num(value.mapMinscale, 0)!=0 && Ext.num(value.mapMaxscale, 0)!=0;
              }
            }
          )
        , align : 'right'
        , renderer : function(value, meta, record){
            var scale = [record.get('mapMinscale'), record.get('mapMaxscale')];
            var excludes = ['', null, 0, '0'];
            Ext.each(excludes, function(exclude){
              var index;
              while ( (index = scale.indexOf(exclude))!=-1 ){
                scale.splice(index, 1);
              }
            });
            return '1/'+scale.join('<br/>1/');
          }
        }
      , {dataIndex : "mapDiffusion"      , header : _T_MAPLIST("columns.mapDiffusion"), hidden : me.datatype==MAP_TYPE_MODEL
        , renderer : function(value, meta, record){
            meta.css = "x-wrap-cell";
            return (value || []).join('<br/>');
          }
        }
      , {dataIndex : "mapCatalogable"    , text : _T_MAPLIST("columns.mapCatalogable")
        , xtype:'checkcolumn'
        , hidden : me.datatype==MAP_TYPE_MODEL
        , align : 'center'
        , renderer : Ext.grid.column.Check.prototype.defaultRenderer
        /*, renderer : function(value, meta, record){
            return (value===true ? _T_MAPLIST("data.mapCatalogable.true") : _T_MAPLIST("data.mapCatalogable.false"));
          }
        , editor : {
            xtype : 'checkbox'
        }*/
        , width: 90
        }
      , {dataIndex : "mapDatepublication", header : _T_MAPLIST("columns.mapDatepublication"), menuDisabled : false
        , align : 'center'
        , width : 150
        , renderer : function(value, meta){
            meta.css = "x-wrap-cell";
            return (value instanceof Date ? Ext.Date.format(value, 'd/m/Y H:i:s') : null);
          } 
        }
      , {dataIndex : "mapOnline"          , header : _T_MAPLIST("columns.mapShare")
        , align : 'center'
        , renderer : function(value){
            return (value ? "<a href='"+value+"' target='_blank'>"+_T_MAPLIST("columns.mapShareText")+"</a>" : "");
          }
        }
        
      ]//columns
      
    };
    this.callParent()
  }
});

/**
 * Window with selectable map list to open
 * 
 * @extend Carmen.Generic.Window
 */
Ext.define('Carmen.Window.Map.List',{
  extend: 'Carmen.Generic.Window', 
  reference: 'window-map-list',
  id : CMP_REFERENCE('Carmen.Window.Map.List'),
  
  title: _T_MAPLIST('window.title'),
  scrollable: true,
  closeAction: 'destroy',
  closable: true,
  layout: 'fit',
  
  initComponent : function()
  {
    var me = this;
    
    
    me.items= [Ext.create('Ext.tab.Panel', {
      items : [
      /*@component window/list[Maps] */
      me.gridmaps = Ext.create(me.grid_xtype, {
        datatype : MAP_TYPE_MAP
      }),
      
      /*@component window/list[Models] */
      me.gridmodels = Ext.create(me.grid_xtype, {
      deferredRender : true,
        datatype : MAP_TYPE_MODEL
      })]
    })];
    
    me.buttons = [
    /*@component window/buttons[Open] */
    {
      text : _t('app.btn.open'),
      handler : function(){
                me.openSelectedRecord();
              }
            }
    /*@component window/buttons[Delete] */
    , {
      text : _t('app.btn.delete'),
      handler : function(){
        me.deleteSelectedRecord();
      }
    }
    /*@component window/buttons[Apply] */
    , {
      text : _t('app.btn.apply'),
      handler : function(){
        me.saveModifiedRecords();
      }
    }
    /*@component window/buttons[Cancel] */
    , {
      text : _t('app.btn.cancel'),
      handler : function(){me.close()}
    }];
    
    this.callParent();
    
    this.on('beforeshow', this.reloadStores);
  },  
  
  /*SPECIFIC ATTRIBUTES*/
  gridmaps : null,
  gridmodels : null,
  grid_xtype : CMP_NAME,
  
  /**
   * Return the selected record into the 2 lists
   * @param Carmen.Model.Map allowNull
   */
  getSelectedRecord : function(allowNull)
  {
    var me = this;
    var gridmaps = me.getGridMaps();
    if ( !gridmaps ) return;
    var gridmodels = me.getGridModels();
    if ( !gridmodels ) return;
  
    var selected_record = gridmaps.getSelectedRecord() || gridmodels.getSelectedRecord();
        
    if ( !allowNull && !selected_record ){
      Carmen.Notification.msg(_t('form.msgAppliedTitle'), _T_MAPLIST('extra.no_selected_record'))
    }
    return selected_record;
  },
  /**
   * Return all modified records into the 2 lists
   * @param array
   */
  getModifiedRecords : function()
  {
    var me = this;
    
    var gridmaps = me.getGridMaps();
    if ( !gridmaps ) return;
    var gridmodels = me.getGridModels();
    if ( !gridmodels ) return;
    
    var modified = [].concat(gridmaps.getStore().getModifiedRecords()) . concat(gridmodels.getStore().getModifiedRecords());
    
    return modified;
  },
  
  /**
   * Returns the Grid object containing the list of non-models maps
   * @return Ext.grid.Panel
   */
  getGridMaps : function()
  {
    return this.gridmaps;
  },
  /**
   * Returns the Grid object containing the list of models maps
   * @return Ext.grid.Panel
   */
  getGridModels : function()
  {
    return this.gridmodels;
  },
  
  /**
   * Force reloading of all the stores 
   */
  reloadStores : function(){
    var cmp = this.getGridMaps();
    if ( cmp )cmp.getStore().reload();
    
    var cmp = this.getGridModels();
    if ( cmp )cmp.getStore().reload();
  },
  
  /**
   * Close all other windows
   */
  closeWindows : function(){
    Ext.each(Ext.ComponentQuery.query('cmngenericwindow'), function(win){win.close()});
  },
  
  /**
   * Open the selected map (if exists) :
   * - If there are modified records, confirm the loose of modification when open the selected record
   * - Ajax Request to create a duplicate map specific to this edition.
   * - Apply to the Carmen Application the OWS Context resulting to this duplication
   * - Set the current Map edited by the user
   */
  openSelectedRecord : function()
  {
    var me = this;
    var selected_record = me.getSelectedRecord(false);
    if ( selected_record ){
      var open_record = function(){
        Ext.Ajax.request({
          url : Routing.generate('carmen_ws_edit_map', {id : selected_record.get('mapId')}),
          success : function(response){
          	me.closeWindows();
            var jsonResponse = Ext.decode(response.responseText);
            Carmen.Model.Map.onSuccessSave(jsonResponse);
            me.close();
          }
        })
      };
      
      var modified = me.getModifiedRecords();
      if ( modified.length ){
        Ext.Msg.show({
          title   : _t('form.msgWarningTitle'),
          message : _t('form.msgUnsavedData'),
          buttons : Ext.Msg.YESNO,
          icon    : Ext.Msg.WARNING,
          fn: function(btn) {
            if (btn === 'yes') {
              open_record();
            } 
          }
        });
      } else {
        open_record();
      }
      
    }
  },
  
  deleteSelectedRecord : function()
  {
    var me = this;
    var selected_record = me.getSelectedRecord(false);
    if ( selected_record ){
      Ext.Msg.confirm(
        _t("form.confirmDeleteTitle")
      , new Ext.XTemplate(_T_MAPLIST('extra.confirm_delete')).apply(selected_record.data)
      , function(btn){
          if ( btn=="yes" ){
            Ext.Ajax.request({
              url : Routing.generate('carmen_ws_delete_map', {id : selected_record.get('mapId')}),
              async : false,
              method : 'DELETE'
            , success : function(){
                Carmen.Notification.msg(_t('form.msgAppliedTitle'), _t('form.msgApplied'));
                me.reloadStores();
                if ( Carmen.user.map && Carmen.user.map.get('mapId') == selected_record.get('mapId')){
                  Carmen.Model.Map.onSuccessSave({}, true);
                }
              }
            , failure : function(response){
                if (Carmen.notAuthenticatedException(response) ) return;
                Carmen.Notification.msg(_t('form.msgFailureTitle'), _t('form.msgFailureDelete'));
              }
            });
          }
        }
      );
    }
  },
  
  /**
   * Save all (models+non-models maps) the modified records (in one execution)
   */
  saveModifiedRecords : function()
  {
    var me = this;
    var modified = me.getModifiedRecords();
    var submits = [];
    Ext.each(modified, function(record){submits.push( Ext.apply({mapId : record.getId()}, record.getChanges()) );});
    
    if ( submits.length==0 ) return;
    
    Ext.Ajax.request({
      url : Routing.generate('carmen_ws_post_multi_maps', {}),
      async : false,
      method : 'POST',
      jsonData : {maps : submits}
    , success : function(){
        Carmen.Notification.msg(_t('form.msgAppliedTitle'), _t('form.msgApplied'));
        me.getGridMaps().getStore().commitChanges();
        me.getGridModels().getStore().commitChanges();
      }
    , failure : function(response){
        if (Carmen.notAuthenticatedException(response) ) return;
        Carmen.Notification.msg(_t('form.msgFailureTitle'), _t('form.msgFailure'));
        me.getGridMaps().getStore().rejectChanges();
        me.getGridModels().getStore().rejectChanges();
      }
    });
  }
})
