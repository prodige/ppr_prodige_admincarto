var _T_ONLINERESOURCE = function (key) {
    return _t("Map.onlineResources." + key);
};

/**
 * Carmen.Model.MapOnlineResources
 */
Ext.define('Carmen.Model.MapOnlineResources', {
    extend: 'Ext.data.Model',
    idProperty: 'onlineResourceId',
    fields: [
        { name: "onlineResourceId", persist: false }
        , { name: "onlineResourceName", type: "string" }
        , { name: "onlineResourceUrl", type: "string" }
        , { name: "onlineResourceCategoryName", type: "string" }
    ]
});


Ext.define('Carmen.Store.MapOnlinesResources', {
    extend: 'Ext.data.Store',
    model: 'Carmen.Model.MapOnlineResources',
    data: [],
});


// Store vide : au chargement de la fenetre : aucune ressource n'est affichée
// --------------------------------------------------------------------------
var temp_store_resource = Ext.create('Ext.data.Store', {
    fields: ['onlineResourceCategoryName', 'onlineResourceName', 'onlineResourceUrl', 'onlineResourceId'],
});
// --------------------------------------------------------------------------

// Store contenant l'ensemble des ressources remontée par la bdd
// --------------------------------------------------------------------------
var store_resource;
// --------------------------------------------------------------------------

// tableau de sortie
var onlinesResources = [];


var CMP_NAME = 'Carmen.Map.MapOnlineResources';
Ext.define('Carmen.Map.MapOnlineResources', {
    extend: 'Carmen.Generic.Form',
    xtype: CMP_REFERENCE(CMP_NAME),
    title: _T_ONLINERESOURCE('title'),
    name: 'MapOnlineResourcesForm',
    setValue: function (onlineResources) {

        var ressources = Ext.create('Ext.data.Store', {
            model: 'Carmen.Model.MapOnlineResources',
            name: 'store_resource',
            autoLoad: false,
            data: [],
            listeners: [{
                datachanged: function (me) {
                    var category_combo_store = Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceNameCombo]')[0].getStore();
                    category_combo_store.removeAll();

                    var grid_resource_store = Ext.ComponentQuery.query('[name=resource_gridpanel]')[0].getStore(); // 
                    grid_resource_store.removeAll();

                    var resources = me.getData();
                    onlinesResources = [];
                    for (var i = 0; i < resources.length; i++) {
                        var single_resource = resources.items[i].data;

                        onlinesResources.push(single_resource);
                        category_combo_store.add(resources.items[i].data);
                        grid_resource_store.add(resources.items[i].data);
                    }
                }
            }]
        });

        store_resource = ressources;
        store_resource.add(onlineResources);

        var category_array = [];
        var online = [];
        for (var i = 0; i < onlineResources.length; i++) {
            if (category_array.indexOf(onlineResources[i].onlineResourceCategoryName) === -1) {
                online.push(onlineResources[i]);
                category_array.push(onlineResources[i].onlineResourceCategoryName)
            }
        }

        Ext.ComponentQuery.query('[name=category_gridpanel]')[0].store.add(online);
        Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceNameCombo]')[0].store.add(online);

    },
    initComponent: function () {

        var me = this;
        me.items = [
            {
                xtype: 'panel',
                layout: 'form',
                items: [{
                    xtype: 'fieldset',
                    title: _T_ONLINERESOURCE('category_title'),
                    layout: 'form',
                    items: [

                        {
                            name: 'category_gridpanel',
                            xtype: 'gridpanel',

                            viewConfig: {
                                plugins: {
                                    ptype: 'gridviewdragdrop',
                                    dragText: _T_ONLINERESOURCE('reorderCategoryTooltip'),
                                    /**
                                     * override onViewRender to get access to the dragZone object
                                     * then override getDragData of dragZone object to disabled drag under input elements
                                     */
                                    dragZone: {
                                        getDragData: function (e) {
                                            // do not enable drag under input elements
                                            if (Ext.get(e.getTarget()).is('input')) return;
                                            return Ext.view.DragZone.prototype.getDragData.apply(this, arguments);
                                        }
                                    }
                                }
                            },

                            store: Ext.create('Ext.data.Store', {
                                autoLoad: false,
                                model: 'Carmen.Model.MapOnlineResources',
                                data: []
                            }),
                            scrollable: 'y',
                            minHeight: 150,
                            maxHeight: 200,
                            plugins: {
                                ptype: 'cellediting',
                                clicksToEdit: 2,
                                listeners: [{
                                    validateedit: function (editor, context) {
                                        update_origine_store(store_resource, context.originalValue, context.value);
                                        update_origine_store(temp_store_resource, context.originalValue, context.value);
                                    }
                                }]
                            },
                            columns: [{
                                text: _T_ONLINERESOURCE('category_name'),
                                dataIndex: 'onlineResourceCategoryName',
                                flex:0.9,
                                editor: 'textfield',
                            }, {
                                dataIndex: 'categorie_action',
                                xtype: 'actioncolumn',
                                menuDisabled: true,
                                style: 'text-align: center; vertical-align : center;',
                                align: 'center',
                                items: [{
                                    icon: '/images/delete.png',
                                    tooltip: 'Supprimer',
                                    handler: function (grid, rowIndex, colIndex) {

                                        var category_to_delete = grid.getStore().getData().items[rowIndex].data.onlineResourceCategoryName;
                                        clean_slave_store(store_resource, category_to_delete);
                                        clean_slave_store(temp_store_resource, category_to_delete);

                                        grid.store.removeAt(grid.getStore().find('onlineResourceCategoryName', category_to_delete));
                                    }
                                }],
                                flex: 0.05
                            }],
                            listeners: [{
                                cellclick: function (me, td, cellIndex, record, tr, rowIndex, e, eOpts) {

                                    if (record.data === undefined) {
                                        var selected_categorie = record.onlineResourceCategoryName;
                                    } else {
                                        var selected_categorie = record.data.onlineResourceCategoryName;
                                    }

                                    // récupérer le store du tableau ressource pour le supprimer
                                    var resources_store = Ext.ComponentQuery.query('[name=resource_gridpanel]')[0].getStore();
                                    resources_store.removeAll();

                                    var records = [];
                                    var records_index = [];
                                    for (var i = 0; i < store_resource.data.length; i++) {
                                        var current_index = store_resource.find('onlineResourceCategoryName', selected_categorie, i);
                                        if (current_index !== -1 && records_index.indexOf(current_index) === -1) {
                                            records_index.push(current_index);
                                            records.push(store_resource.data.items[current_index]);
                                        }
                                    }
                                    resources_store.add(records);
                                }
                            }]
                        },








                        {
                            xtype: 'panel',
                            width: Carmen.Util.WINDOW_WIDTH - 30,
                            layout: 'hbox',
                            items: [{
                                name: 'OnlineRessourceAddCategoryName',
                                xtype: 'textfield',
                                fieldLabel: _T_ONLINERESOURCE('category_name'),
                                labelWidth: 200,
                                listeners: [{
                                    change: function (me, value, old_value) {
                                        var button_disabled = true;
                                        if (value !== '') {
                                            button_disabled = false;
                                        }
                                        Ext.ComponentQuery.query('[name=OnlineRessourceCategoryAddButton]')[0].setDisabled(button_disabled);
                                    }
                                }],
                                width: '40%',
                            }, {
                                xtype: 'button',
                                name: 'OnlineRessourceCategoryAddButton',
                                text: 'Ajouter',
                                scope: this,
                                width: '10%',
                                height: 24,
                                style: 'margin: 0; vertical-align: top; ',
                                disabled: true,
                                handler: function () {
                                    create_store_action("OnlineRessourceCategoryAddButton");
                                }
                            }]
                        }]
                }]

            }, {
                xtype: 'fieldset',
                title: _T_ONLINERESOURCE('title'),
                layout: 'form',
                items: [
                    {
                        name: 'resource_gridpanel',
                        xtype: 'gridpanel',
                        viewConfig: {
                            plugins: {
                                ptype: 'gridviewdragdrop',
                                dragText: _T_ONLINERESOURCE('reorderResourcesTooltip'),
                                /**
                                 * override onViewRender to get access to the dragZone object
                                 * then override getDragData of dragZone object to disabled drag under input elements
                                 */
                                dragZone: {
                                    getDragData: function (e) {
                                        // do not enable drag under input elements
                                        if (Ext.get(e.getTarget()).is('input')) return;
                                        return Ext.view.DragZone.prototype.getDragData.apply(this, arguments);
                                    }
                                }
                            }
                        },
                        minHeight: 150,
                        maxHeight: 200,
                        store: Ext.create('Ext.data.Store', {
                            model: 'Carmen.Model.MapOnlineResources',
                            data: []
                        }),
                        scrollable: 'y',
                        top: '10px',
                        plugins: {
                            ptype: 'cellediting',
                            clicksToEdit: 2
                        },
                        columns: [
                            {
                                text: _T_ONLINERESOURCE('category_name'),
                                dataIndex: 'onlineResourceCategoryName',
                                flex: 0.2
                            },
                            {
                                text: _T_ONLINERESOURCE('resource_name'),
                                dataIndex: 'onlineResourceName',
                                flex: 0.2,
                                editor: {
                                    xtype: 'textfield',
                                    allowBlank: false
                                }
                            }, {
                                text: _T_ONLINERESOURCE('resource_url'),
                                dataIndex: 'onlineResourceUrl',
                                flex: 0.6,
                                editor: {
                                    xtype: 'textfield',
                                    allowBlank: false,
                                    regex: new RegExp('(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})'),
                                    regexText: "Veuillez saisir une url valide"
                                }
                            }, {
                                dataIndex: 'resource_action',
                                xtype: 'actioncolumn',
                                menuDisabled: true,
                                style: 'text-align: center; vertical-align : center;',
                                align: 'center',
                                items: [{
                                    icon: '/images/delete.png',
                                    tooltip: 'Delete',
                                    handler: function (grid, rowIndex, colIndex) {

                                        var resource_name = grid.getStore().data.items[rowIndex].data.onlineResourceName;
                                        // suppression dans le store tempo (du tableau)
                                        grid.store.removeAt(grid.getStore().find('onlineResourceName', resource_name));
                                        // suppression dans le store source
                                        store_resource.removeAt(store_resource.find('onlineResourceName', resource_name));
                                    }
                                }],
                                flex: 0.05
                            }]
                    },








                    {
                        xtype: 'panel',
                        width: Carmen.Util.WINDOW_WIDTH - 30,
                        items: [
                            {
                                name: 'OnlineRessourceAddResourceNameCombo',
                                xtype: 'combo',
                                store: Ext.create('Ext.data.Store', {
                                    autoLoad: false,
                                    model: 'Carmen.Model.MapOnlineResources',
                                    data: [],

                                }),
                                displayField: 'onlineResourceCategoryName',
                                fieldLabel: _T_ONLINERESOURCE('category_choice'),
                                labelWidth: 200,
                                width: '40%',
                                queryMode: 'local',
                                listeners: [{
                                    change: function () { set_add_resource_button_enabled(); }
                                }],
                            }, {
                                name: 'OnlineRessourceAddResourceName',
                                xtype: 'textfield',
                                fieldLabel: _T_ONLINERESOURCE('resource_name'),
                                labelWidth: 200,
                                listeners: [{
                                    change: function () { set_add_resource_button_enabled(); }
                                }],
                                width: '40%',
                            }, {
                                name: 'OnlineRessourceAddResourceUrl',
                                xtype: 'textfield',
                                fieldLabel: _T_ONLINERESOURCE('resource_url'),
                                labelWidth: 200,
                                listeners: [{
                                    change: function () { set_add_resource_button_enabled(); }
                                }],
                                width: '40%',
                                regex: new RegExp('(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})'),
                                regexText: "Veuillez saisir une url valide",
                            }, {
                                xtype: 'button',
                                name: 'OnlineRessourceAddResourceButton',
                                text: _T_ONLINERESOURCE('resource_add'),
                                scope: this,
                                height: 24,
                                style: 'margin: 0; vertical-align: top; ',
                                disabled: true,
                                handler: function () {

                                    create_store_action("OnlineRessourceAddResourceButton");     // OnlineRessourceAddResourceButton
                                }
                            }]
                    }]
            }
        ];
        this.callParent(arguments);
    },
    loadRecord: function (record) {

        var mapRecord = record || [];
        this.getForm()._record = mapRecord;

        var onlineresources = mapRecord.get('mapOnlineResources');
        this.setValue(onlineresources);

    },

    updateRecord: function (record) {
        record.set("mapOnlineResources", onlinesResources);
    }
});

function clean_slave_store(store, category_to_delete) {

    var records_to_keep = [];
    for (var i = 0; i < store.data.length; i++) {
        if (store.data.items[i].data.onlineResourceCategoryName !== category_to_delete) {
            records_to_keep.push(store.data.items[i]);
        }
    }
    store.removeAll();
    store.add(records_to_keep);
}

function update_origine_store(store, original_value, value) {

    for (var i = 0; i < store.data.length; i++) {
        if (store.data.items[i].data.onlineResourceCategoryName === original_value) {
            store.data.items[i].data.onlineResourceCategoryName = value;
        }
    }
}

function create_store_action(button_name) {

    var field_to_reset = [];
    var store_categorie = Ext.ComponentQuery.query('[name=category_gridpanel]')[0].getStore();

    if (button_name === "OnlineRessourceCategoryAddButton") {

        var new_category_name = Ext.ComponentQuery.query('[name=OnlineRessourceAddCategoryName]')[0].getValue();
        var categoryExist = store_categorie.findRecord('onlineResourceCategoryName', new_category_name)
        if (categoryExist !== null) {
            Ext.Msg.alert(_t("form.msgFailureTitle"), _T_ONLINERESOURCE('category_exists'));
            return;
        }

        var temp_store_resource = Ext.ComponentQuery.query('[name=resource_gridpanel]')[0].getStore();
        var nextId = get_next_record_id(store_resource);
        var new_resource = {
            onlineResourceCategoryName: new_category_name,
            onlineResourceName: 'ressource' + nextId,
            onlineResourceUrl: 'https://www.url' + nextId + '.com',
            onlineResourceId: nextId
        };

        temp_store_resource.removeAll();
        temp_store_resource.add(new_resource);
        store_resource.add(new_resource);
        store_categorie.add(new_resource);

        field_to_reset.push("OnlineRessourceAddCategoryName");

    }
    if (button_name === "OnlineRessourceAddResourceButton") {

        var category_name = Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceNameCombo]')[0].getValue();
        var resource_name = Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceName]')[0].getValue();
        var resource_url = Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceUrl]')[0].getValue();

        var new_resource = {
            onlineResourceCategoryName: category_name,
            onlineResourceName: resource_name,
            onlineResourceUrl: resource_url,
            onlineResourceId: get_next_record_id(store_resource)
        };
        store_resource.add(new_resource);
        field_to_reset.push("OnlineRessourceAddResourceNameCombo", "OnlineRessourceAddResourceName", "OnlineRessourceAddResourceUrl");
    }

    var category_gridpanel = Ext.ComponentQuery.query('[name=category_gridpanel]')[0];
    //category_gridpanel.fireEvent("cellclick", category_gridpanel,"", "", new_resource);

    // remise des champs à leur état initial
    for (var i = 0; i < field_to_reset.length; i++) {
        Ext.ComponentQuery.query('[name=' + field_to_reset[i] + ']')[0].setValue("");
    }
}


function set_add_resource_button_enabled() {

    var requiered_fiels = ["OnlineRessourceAddResourceNameCombo", "OnlineRessourceAddResourceName", "OnlineRessourceAddResourceUrl"];
    var disabled = false;
    for (var i = 0; i < requiered_fiels.length; i++) {
        if (Ext.ComponentQuery.query('[name=' + requiered_fiels[i] + ']')[0].getValue() === "") {
            disabled = true;
        }
    }
    var input_url = Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceUrl]')[0];
    if (input_url.wasValid !== undefined && input_url.wasValid === false) {
        disabled = true;
    }
    Ext.ComponentQuery.query('[name=OnlineRessourceAddResourceButton]')[0].setDisabled(disabled);
}

function get_next_record_id(store_resource) {
    // recherche du nouvel id 
    var next_id = 1;
    for (var i = 0; i < store_resource.data.length; i++) {
        var current_record_id = store_resource.data.items[i].data.onlineResourceId;
        if (current_record_id >= next_id) {
            next_id = current_record_id + 1;
        }
    }

    return next_id;
}
