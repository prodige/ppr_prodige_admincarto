/**
 * _T_LAYERLIST text classified by prefix (fixed) and key
 * @param string key
 * @return string
 */
var _T_LAYERLIST = function (key) {
  return _t("Layer.list." + key);
};

///////////////////////////////////////////
// Demo window
///////////////////////////////////////////
var CMP_NAME = 'Carmen.Layer.List';
Ext.define('Carmen.Layer.List', {
  xtype: CMP_REFERENCE(CMP_NAME),
  extend: 'Ext.grid.Panel',
  plugins: [{
    ptype: 'cellediting',
    clicksToEdit: 2,
    listeners: {
      beforeedit: function (plugin, config) {
        var record = config.record;
        switch (config.column.name || config.column.dataIndex) {
          case 'layerProjectionEpsg':
            config.value = String(config.value);
            break;
          case 'layerScales':
            config.field = config.column.name;
            config.originalValue = {
              'layerMinscale': record.get('layerMinscale'),
              'layerMaxscale': record.get('layerMaxscale')
            };
            config.value = Ext.apply({}, config.originalValue);
            break;
        }
        return true;
      },
      validateedit: function (plugin, config) {
        var record = config.record;
        var value = config.value;
        switch (config.column.name || config.column.dataIndex) {
          case 'layerProjectionEpsg':
            config.value = String(config.value);
            break;
          case 'layerScales':
            var dirty = !((Ext.num(value.layerMinscale, 0) == Ext.num(record.get('layerMinscale'), 0))
              && (Ext.num(value.layerMaxscale, 0) == Ext.num(record.get('layerMaxscale'), 0))
            );
            if (dirty) {
              record.set('layerMinscale', value.layerMinscale);
              record.set('layerMaxscale', value.layerMaxscale);
              record.cancel = false;
              record.dirty = true;
            }
            config.field = config.column.name || config.column.dataIndex;
            break;
        }
        return true;
      }
    }
  }],

  flex: 1,
  height: 350,
  width: '100%',

  shrinkWrapDock: true,
  columnLines: true,
  frame: true,

  /*SPECIFIC ATTRIBUTES*/
  selected_record: null,
  wintoolbar: null,

  setSelectedRecord: function (record) {
    this.selected_record = record;
    this.up('window').down('button[name=editLayer]').setDisabled(!this.selected_record);
    this.up('window').down('button[name=deleteLayer]').setDisabled(!this.selected_record);
  },

  getSelectedRecord: function () { return this.selected_record; },

  closeWindow: function () {
    var win = this.up('window');
    if (win) { win.close() }
  },

  initAfterRender: function () {
    var me = this;
    var win = me.up('window');
    if (win) {
      var toolbar = win.getDockedItems('toolbar[dock="bottom"]');
      if (toolbar.length) {
        me.wintoolbar = toolbar = toolbar[0];
        toolbar.add(me.addedButtons);
        me.is_added = true;
      }
    }
  },
  initComponent: function () {
    var me = this;

    /*
     * SPECIFIC BUTTONS
     */
    me.addedButtons = [
      Ext.create('Ext.button.Button', {
        hidden: true

        , disabled: true
        , name: 'editLayer'
        , text: _T_LAYERLIST('buttons.editLayer')
        , handler: function () {
          me.openSelectedRecord();
        }
      })
      , Ext.create('Ext.button.Button', {
        hidden: true
        , disabled: true
        , name: 'deleteLayer'
        , text: _T_LAYERLIST('buttons.deleteLayer')
        , handler: function () {
          me.deleteSelectedRecord();
        }
      })
      , Ext.create('Ext.button.Button', {
        hidden: true
        , text: _T_LAYERLIST('buttons.saveLayers')
        , handler: function () {
          me.saveModifiedRecords();
        }
      })
      , Ext.create('Ext.button.Button', {
        hidden: true
        , text: _T_LAYERLIST('buttons.closeWindow')
        , handler: function () {
          me.closeWindow()
        }
      })
    ];
    me.is_added = false;
    me.listeners = {
      afterrender: me.initAfterRender,
      activate: function () {
        if (!me.is_added) {
          me.initAfterRender();
        }
        if (me.wintoolbar) {
          me.wintoolbar.items.each(function (btn) {
            btn.setVisible(me.addedButtons.indexOf(btn) != -1);
          })
        }
      },
      deactivate: function () {
        if (!me.is_added) {
          me.initAfterRender();
        }
        if (me.wintoolbar) {
          me.wintoolbar.items.each(function (btn) {
            btn.setVisible(me.addedButtons.indexOf(btn) == -1);
          })
        }
      }
    };

    /*
     * VIEW
     */
    me.title = _T_LAYERLIST('grid.title');
    me.viewConfig = {
      cellTpl: [
        '<td class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\\"" + Ext.id() + "\\"" : ""]} ' +
        ' style="width:<tpl if="values.classes.indexOf(\'x-grid-cell-first\') != -1">{column.cellWidth -1}<tpl else>{column.cellWidth}</tpl>px;' +
        '<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}">',
        '<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
        'style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>{value}</div>',
        '</td>', {
          priority: 0
        }
      ],
      stripeRows: true,
      emptyText: _T_LAYERLIST('grid.emptyText'),
      listeners: {
        refresh: function () {
          var store = me.getStore();
          var records = store.getRange();
          Ext.each(records, function (record) {
            var height = 0;
            var tables = Ext.query('table[data-recordid="' + record.internalId + '"]', false);
            Ext.each(tables, function (table) { height = Math.max(height, table.getHeight()) });
            Ext.each(tables, function (table) { table.setHeight(height) });
          })
        }
      }
    };

    /*
     * STORE
     */
    me.store = Carmen.user.map.get('layers');


    /*
     * COLUMNS
     */
    me.columns = {
      defaults: {
        hideable: false,
        menuDisabled: true
        , stopSelection: true
        , width: 120
        , renderer: function (value, meta) {
          meta.css = "x-wrap-cell";
          return value;
        }
      },
      items: [
        {
          dataIndex: "layerId", xtype: 'widgetcolumn'
          , width: 40
          , onWidgetAttach: function (column, widget, record) {
            record.radioSelector = widget;
          }
          , widget: {
            xtype: 'radio'
            , name: "layerId"
            , value: false
            , stopSelection: true
            , listeners: {
              change: function (radio, checked) {
                var record = radio.getWidgetRecord();
                if (!checked && me.getSelectedRecord() == record) {
                  me.setSelectedRecord(null)
                }
                if (checked) {
                  me.setSelectedRecord(record);
                }
              }//change
            }//listeners
          }//widget
        }//widgetcolumn
        , {
          dataIndex: "layerTitle",
          header: _T_LAYERLIST("columns.layerTitle"),
          width: 200,
          editor: { xtype: 'textfield', allowBlanks: false, stopSelection: true },
          menuDisabled: false,
        }
        , {
          dataIndex: "layerName",
          header: _T_LAYERLIST("columns.layerName"),
          menuDisabled: false,
          width: 146
        },
        {
          dataIndex: "layerType",
          header: _T_LAYERLIST("columns.layerType"),
          renderer: function (value, meta) {
            if (!Ext.isDefined(value) || value === null) return "";
            var store = Carmen.Dictionaries.LAYERTYPES;
            var index = store.find('valueField', value);
            
            if (index != -1) {
              return _T_LAYERLIST("data.layerType." + store.getAt(index).get('displayField'));
            }

            return value;
          },
          width: 120
        },{
          dataIndex: "layerProjectionEpsg", header: _T_LAYERLIST("columns.layerProjection")
          , renderer: function (value, meta, record) {
            meta.css = "x-wrap-cell";
            value = value || record.data.projection;
            if (!Ext.isDefined(value) || value === null) return "";
            var epsg = String(value).replace(/\+init=(epsg):.+|\+init=(crs):.+/, '$1').toUpperCase();
            value = String(value).replace(/\+init=epsg:|\+init=crs:/, '');
            var store = Carmen.Dictionaries.PROJECTIONS;
            var tpl = new Ext.XTemplate("{0}:{1}");
            var index;
            index = store.find('valueField', value);
            if (index != -1) {
              return store.getAt(index).get('displayField');
            }
            return (value ? tpl.apply([epsg, value]) : "");
          }
          , width: 250
        }
        , {
          name: 'layerScales', header: _T_LAYERLIST("columns.layerScales")
          , renderer: function (value, meta, record, rowIndex) {
            return new Ext.XTemplate('1/{layerMinscale}<br>1/{layerMaxscale}').apply(record.data);
          }
          , editor: Ext.apply(
            Carmen.getScaleFields('layer', 'Layer.list.', { width: 100 })
            , {
              autoSize: function () { this.setWidth(230) },
              width: 310,
              padding: null,
              layout: 'form',
              labelAlign: 'top',
              anchor: '100%',
              style: 'background:#fff;',
              originalValue: {},

              resetOriginalValue: function () { this.setValue(this.originalValue); },
              setValue: function (value) {
                value = {
                  layerMinscale: Carmen.getFirstDefined(value.layerMinscale, Carmen.user.preferences.get('preferenceMinscale'), Carmen.Defaults.MINSCALE),
                  layerMaxscale: Carmen.getFirstDefined(value.layerMaxscale, Carmen.user.preferences.get('preferenceMaxscale'), Carmen.Defaults.MAXSCALE)
                };
                this.originalValue = value;
                this.down('[name=layerMinscale]').setValue(value.layerMinscale);
                this.down('[name=layerMaxscale]').setValue(value.layerMaxscale);
              },
              getValue: function () {
                return {
                  layerMinscale: this.down('[name=layerMinscale]').getValue(),
                  layerMaxscale: this.down('[name=layerMaxscale]').getValue()
                }
              },
              isValid: function () {
                var value = this.getValue();
                return Ext.num(value.layerMinscale, 0) != 0 && Ext.num(value.layerMaxscale, 0) != 0;
              }
            }
          )
          , align: 'right'
          , renderer: function (value, meta, record) {
            var scale = [record.get('layerMinscale'), record.get('layerMaxscale')];
            var excludes = ['', null, 0, '0', '-1'];
            Ext.each(excludes, function (exclude) {
              var index;
              while ((index = scale.indexOf(exclude)) != -1) {
                scale.splice(index, 1);
              }
            });
            return '1/' + scale.join('<br/>1/');
          }
        }
        , {
          xtype: 'widgetcolumn',
          align: 'center',
          header: _T_LAYERLIST("columns.layerStyle"),
          onWidgetAttach: function (column, widget, record) {
            var font;
            switch (record.get('layerType')) {
              case Carmen.Model.Layer.TYPES.RASTER:
              case Carmen.Model.Layer.TYPES.WMS:
              case Carmen.Model.Layer.TYPES.WMSC:
                font = 'falk-raster';
                break;
              default:
                switch (record.get('msGeometryType')) {
                  case LEX_GEOMETRY_TYPE.TYPES.POINT: font = 'falk-point-thin'; break;
                  case LEX_GEOMETRY_TYPE.TYPES.LINE: font = 'falk-polyline'; break;
                  case LEX_GEOMETRY_TYPE.TYPES.POLYGON: font = 'falk-polygon'; break;
                }
                break;
            }
            widget.setIconCls('fa ' + font);
          },
          widget: {
            height: 24,
            width: 24,
            xtype: 'button',
            scale: 'small',
            tooltip: _T_LAYERLIST("tooltips.layerStyle"),
            handler: function () {
              var record = this.getWidgetRecord();
              me.setSelectedRecord(record);
              me.openSelectedRecord(function (editor) {
                if (editor) {
                  var tabStyle = editor.down('Carmen_FormSheet_Layer_TabStyle');
                  if (tabStyle) {
                    editor.down('tabpanel').setActiveItem(tabStyle);
                  }
                }
              });

            }
          }
        }
      ]//columns

    };
    this.callParent()
  },

  /**
   * Return all modified records into the 2 lists
   * @param array
   */
  getModifiedRecords: function () {
    return this.getStore().getModifiedRecords();
  },

  /**
   * Open the selected layer (if exists) :
   * - If there are modified records, confirm the loose of modification when open the selected record
   * - Ajax Request to create a duplicate layer specific to this edition.
   * - Apply to the Carmen Application the OWS Context resulting to this duplication
   * - Set the current Layer edited by the user
   */
  openSelectedRecord: function (callback) {
    var me = this;
    var selected_record = me.getSelectedRecord(false);
    if (selected_record) {
      var open_record = function () {
        me.closeWindow();
        var editor = Carmen.Window.Layer.editWindow(selected_record, selected_record.get('layerType'));
        if (callback && Ext.isFunction(callback)) {
          callback(editor);
        }
      };

      var modified = me.getModifiedRecords();
      if (modified.length) {
        Ext.Msg.show({
          title: _t('form.msgWarningTitle'),
          message: _t('form.msgUnsavedData'),
          buttons: Ext.Msg.YESNO,
          icon: Ext.Msg.WARNING,
          fn: function (btn) {
            if (btn === 'yes') {
              open_record();
            }
          }
        });
      } else {
        open_record();
      }
    }
  },

  deleteSelectedRecord: function () {
    var me = this;
    var selected_record = me.getSelectedRecord(false);
    if (selected_record) {
      var callbackSuccess = function (record, operation) {
        Carmen.Notification.msg(_t('form.msgAppliedTitle'), _t('form.msgApplied'));
        selected_record.radioSelector.setValue(false);
        selected_record.commit();
        me.setSelectedRecord(null);
      };
      var callbackFailure = function (record, operation) {
        selected_record.reject();
        Carmen.Notification.msg(_t('form.msgFailureTitle'), _t('form.msgFailureDelete'));
      };

      var index = app.layerTreeStore.find('nodeId', selected_record.get('layerId'));
      if (index != -1) {
        app.ui.layerTreePanel.deleteNode(
          app.ui.layerTreePanel
          , index
          , null
          , callbackSuccess
          , callbackFailure
        );
      } else {
        Ext.Msg.confirm(
          _t("form.confirmDeleteTitle")
          , new Ext.XTemplate(_T_LAYERLIST('extra.confirm_delete')).apply(selected_record.data)
          , function (btn) {
            if (btn == "yes") {
              selected_record.drop();
              selected_record.save({
                async: false,
                method: 'DELETE'
                , url: Routing.generate('carmen_ws_delete_layer', { mapId: Carmen.user.map.get('mapId') })
                , success: callbackSuccess
                , failure: callbackFailure
              });
            }
          }
        );
      }
    }
  },

  /**
   * Save all (models+non-models layers) the modified records (in one execution)
   */
  saveModifiedRecords: function () {
    var me = this;
    var modified = me.getModifiedRecords();
    var submits = [];
    Ext.each(modified, function (record) { submits.push(record.getData({ persist: false, serialize: true })); });

    if (submits.length == 0) return;

    Ext.Ajax.request({
      url: Routing.generate('carmen_ws_post_multi_layers', {}),
      async: false,
      method: 'POST',
      jsonData: { layers: submits }
      , success: function () {
        Carmen.Notification.msg(_t('form.msgAppliedTitle'), _t('form.msgApplied'));
        me.getStore().commitChanges();
      }
      , failure: function (response) {
        if (Carmen.notAuthenticatedException(response)) return;
        Carmen.Notification.msg(_t('form.msgFailureTitle'), _t('form.msgFailure'));
        me.getStore().rejectChanges();
      }
    });
  }
});