var _T_MAPUISHARE = function (key) {
  return _t("Map.mapUIShare." + key);
};

Ext.namespace('Ext.ux');

Ext.define('Carmen.model.MapUIShare', {
  extend: 'Ext.data.Model',
  fields: [
    {name: "publicMap"}, 
    {name: "WMSUrl"},
    {name: "WMSMetadata"},
    {name: "WFSUrl"},
    {name: "WFSMetadata"},
    {name: "ATOMUrl"},
    {name: "ATOMMetadata"}
  ],
  proxy: {
    type: 'rest',
    url: Routing.generate('carmen_ws_map_datashare')
  }
});

/**
 * window of file browser
 */
Ext.define('Carmen.Window.MapUIShare', {
  // ext properties
  extend: 'Ext.window.Window',
 //minHeight : 260, 
  
  title: _T_MAPUISHARE('title'),
  layout: 'form',
  
  width: Carmen.Util.WINDOW_WIDTH,

  buttons:[],
  
  mapId: 0,

  /**
   * intialisation of component
   */
  initComponent: function () {
    var me = this;
    
    Carmen.model.MapUIShare.load(me.mapId, {
      callback: function(record) {
        
        itemsFs = [];
        var types = ["WMS", "WFS", "ATOM"];
        for(var i in types) {
          var url = record.get(types[i]+"Url");
          var metadataUrl = record.get(types[i]+"Metadata");
          var html = _T_MAPUISHARE("disabledText");
          if( url!="" || metadataUrl != "" ) {
            html = "";
            var glu = "";
            if( url!="" ) {
              html = html + '<label class="x-form-item-label-default x-unselectable">' + _T_MAPUISHARE('urlLabel') + '</label>' +
                '<a target="_blank" href="'+url+'">'+url+'</a>';
              glu = "<br>";
            }
            if( metadataUrl != "" ) {
              html = html + glu + '<label class="x-form-item-label-default x-unselectable">' + _T_MAPUISHARE('metadataLabel') + '</label>' +
                '<a target="_blank" href="'+metadataUrl+'">'+metadataUrl+'</a>';
            }
          }
          var item = {
            xtype: 'displayfield',
            fieldLabel: _T_MAPUISHARE(types[i]+'Label'),
            value: html
          };
          itemsFs.push(item);
        }

        var items = [{
          html: '<span style="font-style: italic">'+_T_MAPUISHARE("warningTitle")+'</span>'
        }, {
          html: '<label class="x-form-item-label-default x-unselectable">'+_T_MAPUISHARE('publicMapLabel')+'</label>'+
            '<a target="_blank" href="'+record.get("publicMap")+'">'+record.get("publicMap")+'</a>'
        }, {
          xtype:'fieldset',
          title: _T_MAPUISHARE('serviceUrlLabel'),
          hidden :  (!VISIBILITY_APP_PRODIGE_SHARE ? true : false),
          defaults: {
            labelWidth: 50
          },
          layout: 'form',
          items: itemsFs
        }];

        me.add(items);
      }
    });
    
    me.callParent(arguments);
  }

});
