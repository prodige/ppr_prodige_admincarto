/**
 * _T_MAPFORM text classified by prefix (fixed) and key
 * @param string key
 * @return string
 */
var _T_MAPFORM = function (key) {
  var root = "Map.forms.general.";
  return (key !== null ? _t(root + key) : root);
};
/**
 * Configure a field container composed by two columns
 * @param object config
 * @return object
 */
function BICOLUMNCONTAINER(config) {
  return Ext.apply(config || {}, {
    layout: {
      type: 'table',
      columns: 2,
      tableAttrs: {
        width: '100%'
      },
      tdAttrs: {
        width: '50%',
        align: 'left'
      }
    },
    defaults: {
      labelWidth: 100,
      labelAlign: 'left',
      labelSeparator: ' '
    }
  })
}
/**
 * Configure a left column
 * @use use with BICOLUMNCONTAINER
 * @param object config
 * @return object
 */
function LEFTCOLUMN(config) {
  return Ext.apply(config || {}, {
  })
}
/**
 * Configure a right column 
 * @use use with BICOLUMNCONTAINER
 * @param object config
 * @return object
 */
function RIGHTCOLUMN(config) {
  return Ext.apply(config || {}, {
  })
}

function msMetadataFields() {
  var fields = ['abstract', 'languages', 'geoide_wxs_srs', 'fees',
    'contactorganization', 'contactperson', 'contactposition', 'address', 'postcode', 'stateorprovince',
    'city', 'contactorganization', 'country', 'contactelectronicmailaddress', 'contactvoicetelephone', 'contactfacsimiletelephone', 'srs'];
  var model = [];
  Ext.each(fields, function (field) {
    var formField = "ms" + (Ext.util.Format.capitalize(field));

    if (field == 'srs') {
      model.push({
        name: 'wms_srs', defaultValue: "",
        convert: function (value) {
          if (value) {
            value = value.split(' ');
            value = value.map(function (v) { return v.replace(/EPSG:|CRS:/, ''); });
          }
          return value;
        },
        serialize: function (value, record) {
          var v = record.get('map').get('mapProjectionEpsg').toString();
          return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider') + ':' + v;
        }
      });
      model.push({
        name: 'wfs_srs', defaultValue: "",
        convert: function (value) {
          if (value) {
            value = value.split(' ');
            value = value.map(function (v) { return v.replace(/EPSG:|CRS:/, ''); });
          }
          return value;
        }, serialize: function (value, record) {
          var v = record.get('map').get('mapProjectionEpsg').toString();
          return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider') + ':' + v;
        }
      });
      model.push({
        name: formField, persist: false, depend: "wfs_srs",
        convert: function (value, record) {
          return value || record.get("wfs_srs") || [(record.get('map').get('mapProjectionEpsg')).toString()]
        }
      });
    } else if (field == 'geoide_wxs_srs') {
      model.push({
        name: 'geoide_wms_srs', defaultValue: "",
        convert: function (value) {
          if (value) {
            value = value.split(' ');
            value = value.map(function (v) { return v.replace(/EPSG:|CRS:/, ''); });
          }
          return value;
        },
        serialize: function (value, record) {
          var field = record.get(formField);

          if (field && Ext.isArray(field)) {
            return field.map(function (v) {
              return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider') + ':' + v;
            }).join(' '); return value;
          }
        }
      });
      model.push({
        name: 'geoide_wfs_srs', defaultValue: "",
        convert: function (value) {
          if (value) {
            value = value.split(' ');
            value = value.map(function (v) { return v.replace(/EPSG:|CRS:/, ''); });
          }
          return value;
        }, serialize: function (value, record) {

          var field = record.get(formField);
          if (field && Ext.isArray(field))
            return field.map(
              function (v) { return Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', v).get('projectionProvider') + ':' + v; }
            ).join(' '); return value;
        }
      });
      model.push({
        name: formField, persist: false, depend: "geoide_wfs_srs",
        convert: function (value, record) {

          return value || record.get("geoide_wfs_srs") || [(record.get('map').get('mapProjectionEpsg')).toString()]
        }
      });
    } else if (field == 'fees') {
      var field_other = field + 'Other';
      var formField_other = formField + 'Other';
      model.push({
        name: "wfs_" + field, defaultValue: "", serialize: function (value, record) {
          return (record.get(formField) == Carmen.Dictionaries.CONDITION.AUTRES_ID ? record.get(formField_other) : record.get(formField));
        }
      });
      model.push({
        name: "wms_" + field, defaultValue: "", serialize: function (value, record) {
          return (record.get(formField) == Carmen.Dictionaries.CONDITION.AUTRES_ID ? record.get(formField_other) : record.get(formField));
        }
      });
      model.push({
        name: formField_other, persist: false
        , mapping: function (data, record) {
          var value = data["wfs_" + field];
          if (value) {
            if (Carmen.Dictionaries.CONDITION.find('valueField', value) != -1) {
              value = null;
            }
          }
          return value;
        }
      });

      model.push({
        name: formField, persist: false, depend: "wfs_" + field
        , mapping: function (data, record) {
          var value = data["wfs_" + field];
          if (value) {
            if (Carmen.Dictionaries.CONDITION.find('valueField', value) == -1) {
              value = Carmen.Dictionaries.CONDITION.AUTRES_ID;
            }
          }
          return value;
        }
      });

    } else {
      model.push({ name: "wfs_" + field, defaultValue: "", serialize: function (value, record) { return record.get(formField) } });
      model.push({ name: "wms_" + field, defaultValue: "", serialize: function (value, record) { return record.get(formField) } });
      model.push({ name: formField, persist: false, depend: "wfs_" + field, convert: function (value, record) { return value || record.get("wfs_" + field) } });
    }
  });
  return model;
}
/**
 * Carmen.Model.Map
 */
Ext.define('Carmen.Model.msMetadataMap', {
  extend: 'Ext.data.Model',
  fields: [
    {
      name: 'map',
      persist: false,
      serialize: function () { return null }
    }
  ].concat(msMetadataFields()).concat({ name: 'id', persist: false })
});

/**
 * Carmen.Model.Map
 */
Ext.define('Carmen.Model.Map', {
  extend: 'Ext.data.Model',

  statics: {
    onSuccessSave: function (jsonResponse, changeMap) {
      if (!Ext.isDefined(changeMap)) changeMap = true;

      var jsOws = null;
      if (Ext.isDefined(jsonResponse) && Ext.isObject(jsonResponse)) {
        jsOws = jsonResponse.jsonContext || jsOws;
      }

      var pressed = Ext.query('.x-btn.cmncontrolpressed');
      if (pressed.length) {
        Ext.getCmp(pressed[0].id).toggle(false);
      }
      var map = null;
      var currentExtent = null;
      var oldId = null;
      if (Carmen.user.map) {
        oldId = Carmen.user.map.getPublishedId();
        currentExtent = Carmen.application.map.OL_map.getExtent();
      }
      if (changeMap) {
        if (jsonResponse.map) {
          console.log(220, jsonResponse.map);
          map = Ext.create('Carmen.Model.Map', jsonResponse.map);
        }
        Carmen.user.map = map;
      }

      if (jsOws) {
        Carmen.application.loadContext(jsOws);
      }
      if (changeMap) {
        Carmen.application.ui.setCurrentMap(map);
      }
      if (!changeMap && currentExtent && Carmen.user.map && oldId == Carmen.user.map.getPublishedId()) {
        Carmen.application.map.OL_map.zoomToExtent(currentExtent);
      }
      if (Carmen.user.map && oldId == Carmen.user.map.getPublishedId()) {
        if (pressed.length) {
          Ext.getCmp(pressed[0].id).toggle(true);
        } else {
          Carmen.application.mapTools['ZoomIn'].btn.toggle(true)
        }
      } else {
        Carmen.application.mapTools['ZoomIn'].btn.toggle(true)
      }
    }
  },
  idProperty: 'mapId',
  identifier: {
    type: 'sequential',
    prefix: ''
  },
  fields: [
    //Related records (not saved)
    {
      persist: false,
      name: 'userPreferences',
      defaultValue: Carmen.user.preferences
    }
    //Management fields
    , { name: "real_mapfile", type: 'string', persist: false }
    , { name: "has_layer_wms", type: 'boolean', defaultValue: false, persist: false }
    , { name: "has_layer_wfs", type: 'boolean', defaultValue: false, persist: false }
    , { name: "has_layer_atom", type: 'boolean', defaultValue: false, persist: false }
    , {
      name: 'mapDiffusion'
      , calculate: function (data) {
        var types = [
          data.has_layer_wms ? 'WMS' : '-1',
          data.has_layer_wfs ? 'WFS' : '-1',
          data.has_layer_atom ? 'ATOM' : '-1'
        ];
        var index;
        while (types.length && (index = types.indexOf('-1')) != -1) {
          types.splice(index, 1);
        }
        return types;
      }
    }
    // database fields
    , { name: "mapId", type: "int", persist: false }
    , { name: "account", type: "int", convert: function (value, record) { return Carmen.user.account.get('accountId'); } }
    , { name: "mapFile", type: "string" }
    , { name: "mapTitle", type: "string" }
    , { name: "mapSummary", type: "string" }
    , { name: "mapCatalogable", type: "boolean", defaultValue: false }
    , { name: "mapPassword", type: "string" }
    , { name: "has_password", type: "boolean", persist: false, depends: ["mapPassword"], convert: function (data, record) { return !!record.get('mapPassword'); } }
    , { name: "mapModel", type: "boolean" }
    , { name: "mapBufferGlobal", type: "number" }
    , { name: "mapExtentXmin", type: "number", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferencesExtentXmin"), Carmen.Defaults.EXTENT.XMIN); }, depends: ['userPreferences'] }
    , { name: "mapExtentYmin", type: "number", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferencesExtentYmin"), Carmen.Defaults.EXTENT.YMIN); }, depends: ['userPreferences'] }
    , { name: "mapExtentXmax", type: "number", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferencesExtentXmax"), Carmen.Defaults.EXTENT.XMAX); }, depends: ['userPreferences'] }
    , { name: "mapExtentYmax", type: "number", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferencesExtentYmax"), Carmen.Defaults.EXTENT.YMAX); }, depends: ['userPreferences'] }
    , {
      name: "mapProjectionEpsg", type: "int", convert: function (value, record) {
        value = (Ext.isObject(value) ? value.projectionEpsg : value);
        var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceSrs"), Carmen.Defaults.PROJECTION);
      }, depends: ['userPreferences']
    }
    , { name: "mapMinscale", type: "number", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceMinscale"), Carmen.Defaults.MINSCALE); }, depends: ['userPreferences'] }
    , { name: "mapMaxscale", type: "number", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceMaxscale"), Carmen.Defaults.MAXSCALE); }, depends: ['userPreferences'] }
    //association fields
    // mapfile fields
    , { name: "mapOutputformat", type: "string", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceOutputformat"), Carmen.Defaults.OUTPUTFORMAT); }, depends: ['userPreferences'] }
    , { name: "mapUnits", type: "string", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceUnits"), Carmen.Defaults.UNITS); }, depends: ['userPreferences'] }
    , { name: "mapBgcolor", type: "string", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceBackgroundColor"), Carmen.Defaults.BGCOLOR); }, depends: ['userPreferences'] }
    , { name: "mapTransparency", type: "boolean", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceBackgroundTransparency"), Carmen.Defaults.TRANSPARENCY); }, depends: ['userPreferences'] }
    , { name: "mapDatepublication", type: "date", dateFormat: 'Y-m-d H:i:s.u', convert: function (value) { value = (Ext.isObject(value) ? value : { date: value }); var r = Ext.data.field.Date.prototype.convert.call(this, value.date); return r; }, persist: false }
    , { name: "publishedMap", reference: "Carmen.Model.Map", persist: false }
    , { name: "user", defaultValue: Carmen.user.userId, persist: false }
    , { name: "mapWmsmetadataUuid", persist: false }
    , { name: "mapWfsmetadataUuid", persist: false }
    , { name: "mapAtommetadataUuid", persist: false }
    , { name: "mapFees", type: "string", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceOutputformat"), Carmen.Defaults.CONDITION); }, depends: ['userPreferences'] }
    , { name: "mapLanguage", type: "string", convert: function (value, record) { var preferences = record.get("userPreferences"); return Carmen.getFirstDefined(value, preferences.get("preferenceOutputformat"), Carmen.Defaults.CONDITION); }, depends: ['userPreferences'] }
    , {
      name: "mapOnlineResources",
      model: "Carmen.Model.MapOnlineResources",
      defaultValue: null,

    }
    //Related records (saved)
    , {
      name: 'mapUi',
      reference: 'Carmen.Model.MapUI',
      unique: true,
      convert: function (value, record) {
        value = value || {};
        if (!(value instanceof Carmen.Model.MapUI)) {
          value.map = record;
        }
        value = (value instanceof Carmen.Model.MapUI ? value : Ext.create('Carmen.Model.MapUI', value));

        // help message is retrieved from ckeditor
        var editor = Ext.getCmp('help_editor');
        if (editor) {
          if (editor.editorReady) {
            // if editor is ready, then set message to edited value
            value.set('helpMessage', editor.getValue());
          }
          else {
            // if editor not ready, then restore initial message to avoid null value
            value.set('helpMessage', editor.msg);
          }
        }
        var cgu = Ext.getCmp('cgu_editor');
        if (cgu) {
            if (cgu.editorReady) {
                // if editor is ready, then set message to edited value
                value.set('cguMessage', cgu.getValue());
            }
            else {
                // if editor not ready, then restore initial message to avoid null value
                value.set('cguMessage', cgu.msg);
            }
        }

        return value;
      },
      /**@see CarmenJS/Carmen/Ext/Util.js*/
      serialize: Carmen.serializeRecordForSubmit,
      depends: ['mapId']
    }
    , {
      name: 'locators', defaultValue: [],
      convert: function (value, record) {
        if (value && Ext.isArray(value)) {
          Ext.each(value, function (criteria) {
            var locatorCriteriaRelated = criteria.locatorCriteriaRelated;
            criteria.locatorCriteriaRelated = (Ext.isObject(locatorCriteriaRelated) ? locatorCriteriaRelated.locatorCriteriaRank : locatorCriteriaRelated);
          })
        }
        return value;
      },
      serialize: function (value, record) {
        return value;
      }
    }
    , {
      name: "tools",
      reference: "Carmen.Model.MapTools",
      unique: true,
      convert: function (value, record) {
        if (Ext.isArray(value)) {
          value = Carmen.Model.MapTools.convertArray(value);
        }
        value = Carmen.getFirstDefined(value, Ext.create('Carmen.Model.MapTools'));
        return value;
      },
      /**@see CarmenJS/Carmen/Ext/Util.js*/
      serialize: Carmen.serializeRecordForSubmit,
      depends: ['mapId']
    }
    , { name: "keywords", defaultValue: [] }
    , {
      name: "layers",
      defaultValue: [],
      convert: function (layers, map) {
        layers = layers || [];
        if (Ext.isArray(layers)) {
          var _layers_ = [];
          Ext.each(layers, function (layer) {
            layer.map = map;
            if (!(layer instanceof Carmen.Model.Layer)) {
              var klass = eval('Carmen.Model.Layer' + Carmen.Model.Layer.TYPES[layer.layerType.layerTypeCode]);
              console.log('Carmen.Model.Layer' + Carmen.Model.Layer.TYPES[layer.layerType.layerTypeCode]);
              console.log(klass)
              layer = klass.loadOneRawData(layer);
            }
            _layers_.push(layer);
          });
          layers = Ext.create('Ext.data.JsonStore', {
            model: 'Carmen.Model.Layer',
            proxy: Carmen.Model.Layer.prototype.getProxy(map.get('mapId')),
            addLayer: function (layer) {
              if (!(layer instanceof Carmen.Model.Layer)) {
                var klass = eval('Carmen.Model.Layer' + Carmen.Model.Layer.TYPES[layer.layerType.layerTypeCode]);
                layer = klass.loadOneRawData(layer);
              }
              if (!layer.getMap()) {
                layer.setMap(map);
              }
              this.loadRawData([layer], true);
            }
          });
          layers.loadRawData(_layers_);
        }
        return layers;
      },
      serialize: Carmen.serializeRecordForSubmit,
      depends: ['mapId']
    }
    , { name: 'symbolset', type: "string", defaultValue: './etc/symbols.sym' }
    , { name: 'web', persist: false }
    , {
      name: 'msMetadataMap',
      depend: "web.metadata",
      unique: true,
      convert: function (value, record) {
        if (!record.get('web')) return;
        value = value || record.get('web').metadata;
        value = value || {};
        if (value instanceof Carmen.Model.msMetadataMap) {
          value.set('map', record);
        } else if (Ext.isObject(value)) {
          var store = Ext.create('Ext.data.JsonStore', {
            autoLoad: false,
            model: "Carmen.Model.msMetadataMap",
            proxy: {
              type: 'memory',
              reader: {
                type: 'json'
              }
            }
          });
          store.loadRawData([Ext.apply({ map: record }, value)], false);
          value = store.getAt(0);
        }
        return value;
      },
      /**@see CarmenJS/Carmen/Ext/Util.js*/
      serialize: Carmen.serializeRecordForSubmit,
      depends: ['mapId', 'web']
    }
    , {
      name: "reference",
      reference: 'Carmen.Model.msReferenceMap',
      defaultValue: null,
      unique: true,
      convert: function (value, record) {
        if (value == null) return value;
        value = value || {};
        if (!(value instanceof Carmen.Model.msReferenceMap)) {
          value.map = record;
        }
        value = (value instanceof Carmen.Model.msReferenceMap ? value : Ext.create('Carmen.Model.msReferenceMap', value));
        return value;
      },
      /**@see CarmenJS/Carmen/Ext/Util.js*/
      serialize: function (record) {
        if (record) {
          if (!record.get('map').get('mapUi').get('uiKeymap'))
            record.data.status = "OFF";
          return Carmen.serializeRecordForSubmit(record);
        }
        return record;
      },
      depends: ['mapId']
    }],
  getPublishedId: function () {
    var publishedMap = Carmen.getFirstDefined(this.get('publishedMap'), this);
    if (Ext.isObject(publishedMap)) {
      if (publishedMap instanceof Carmen.Model.Map || publishedMap instanceof Ext.data.Model) {
        return publishedMap.get('mapId');
      }
      else {
        return publishedMap.mapId;
      }
    }
    return publishedMap;
  },

  getLayerById: function (layerId) {
    var layers = this.get('layers');
    if (!(layers instanceof Ext.data.Store)) return null;
    return layers.findRecord("layerId", layerId);
  },

  getLayerByName: function (layerName) {
    var layers = this.get('layers');
    if (!(layers instanceof Ext.data.Store)) return null;
    return layers.findRecord("layerName", layerName);
  },
  proxy: {
    timeout: 100000,
    type: 'rest',
    url: Routing.generate('carmen_ws_map', {})
  }

});


///////////////////////////////////////////
// Demo window
///////////////////////////////////////////
var CMP_NAME = 'Carmen.FormSheet.Map';
Ext.define('Carmen.FormSheet.Map', {
  extend: 'Carmen.Generic.Form',
  xtype: CMP_REFERENCE(CMP_NAME),

  title: _T_MAPFORM('title'),

  height: 'auto',
  loadRecord: function () {
    this.callParent(arguments);
    this.afterLoadRecord();
  },
  afterLoadRecord: function () {
    this.addExtentByLayer();
  },
  initComponent: function () {
    var me = this;

    padding: '10 10 10 10',
      me.items = [{
        /*first column*/
        columnWidth: 1,
        defaults: Carmen.Util.defaultFormConfig,
        layout: 'form',

        items: [{
          fieldLabel: _T_MAPFORM('fields.mapFile'),
          xtype: 'fieldcontainer',
          layout: 'hbox',
          items: [{
            name: 'mapFile',
            xtype: 'textfield',
            vtype: 'fileidentifier',
            readOnly: (MAPFILE_READONLY ? true : false),//only for PRODIGE, mapfile can't be changed 
            allowBlank: false
          }, {
            xtype: 'displayfield',
            value: _T_MAPFORM('units.mapFile')
          }]
        }, {
          fieldLabel: _T_MAPFORM('fields.mapTitle'),
          name: 'mapTitle',
          xtype: 'textfield',
          allowBlank: false
        }, {
          fieldLabel: _T_MAPFORM('fields.mapSummary'),
          name: 'mapSummary',
          xtype: 'textarea',
          rows: 1
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_MAPFORM('fields.keywords'),
          layout: 'fit', width: '100%',
          hidden: (!VISIBILITY_MAPS_FORMS_GENERAL_FIELDS_KEYWORDS ? true : false),
          items: [{ name: 'keywords', xtype: 'carmen_keywordgrid', height: 200 }]
        }, {
          xtype: 'fieldcontainer',
          fieldLabel: _T_MAPFORM('fields.mapProjectionEpsg'),
          layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
          },
          defaults: {
            labelSeparator: ' '
          },
          items: [{
            flex: 9,
            name: 'mapProjectionEpsg',
            xtype: 'comboboxprojections',
            width: 'auto',
            listeners: {
              change: function (projection) {
                try {
                  var projectionCode = Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', projection.getValue()).get('projectionProvider') + ':' + projection.getValue();
                  var proj4js = new OpenLayers.Projection(projectionCode);
                  if (!(proj4js && proj4js.proj)) return;
                  var units = proj4js.proj.units;
                  //check units from proj4 (m => METERS, no units => DD)
                  if (units == "m") {
                    units = "METERS"
                  }
                  if (units == null) {
                    units = "DD";
                  }
                  var record = Carmen.Dictionaries.UNITS.findRecord('valueField', units);
                  if (record) {
                    this.up('form').down('[name=mapUnits]').setSelection(record);
                  }
                } catch (error) { }
              }
            },
            autoLoadOnValue: true,
            autoSelect: true
          }, { flex: 3 }]
        }

          , this.getExtentFields()

          , Carmen.getScaleFields('map', _T_MAPFORM(null))

          , BICOLUMNCONTAINER({
            xtype: 'fieldcontainer',
            defaults: Carmen.Util.defaultFormConfig,
            fieldLabel: _T_MAPFORM('fields.mapOutputformat'),
            items: [
              LEFTCOLUMN({
                name: 'mapOutputformat',
                xtype: 'combobox',
                queryMode: 'local',

                displayField: 'displayField',
                valueField: 'valueField',
                store: Carmen.Dictionaries.OUTPUTFORMATS,

                autoLoadOnValue: true,
                autoSelect: true
              }),
              RIGHTCOLUMN({
                fieldLabel: _T_MAPFORM('fields.mapUnits'),
                name: 'mapUnits',
                xtype: 'combobox',
                queryMode: 'local',

                displayField: 'displayField',
                valueField: 'valueField',
                store: Carmen.Dictionaries.UNITS,
                autoLoadOnValue: true,
                autoSelect: true
              })]
          })

          , BICOLUMNCONTAINER({
            xtype: 'fieldcontainer',
            layout: 'column',
            defaults: Carmen.Util.defaultFormConfig,
            fieldLabel: _T_MAPFORM('fields.mapBgcolor'),
            items: [
              LEFTCOLUMN({
                fieldLabel: '',
                name: 'mapBgcolor',
                xtype: 'colorfield',
                layout: 'hbox'
              }),
              RIGHTCOLUMN({
                hidden: true,
                fieldLabel: _T_MAPFORM('fields.mapTransparency'),
                name: 'mapTransparency',
                xtype: 'checkbox'
              })]
          })

          , {
          fieldLabel: _T_MAPFORM('fields.mapCatalogable'),
          name: 'mapCatalogable',
          xtype: 'checkbox',
          hidden: (!VISIBILITY_MAPS_FORMS_GENERAL_FIELDS_MAPCATALOGUABLE ? true : false)
        }

          , {
          fieldLabel: _T_MAPFORM('fields.mapPassword'),
          xtype: 'fieldcontainer',
          layout: 'hbox',
          hidden: (!VISIBILITY_MAPS_FORMS_GENERAL_FIELDS_MAPPASSWORD ? true : false),
          items: [{
            id: 'has_password',
            name: 'has_password',
            xtype: 'checkbox',
            listeners: {
              change: function (chk, checked, old) {
                var cmp = Ext.getCmp('mapPassword');
                if (cmp && cmp.rendered) {
                  if (old != checked) cmp.setValue('');
                  cmp.setVisible(checked)
                }
              }
            }
          }, { xtype: 'splitter' }, {
            id: 'mapPassword',
            name: 'mapPassword',
            xtype: 'textfield',
            inputType: 'password',
            listeners: {
              afterrender: function (chk) {
                var chk = Ext.getCmp('has_password');
                if (!chk) return;
                chk.fireEvent('change', chk, chk.getValue(), chk.getValue());
              }
            }
          }]
        }]
      }];
    this.callParent(arguments);
  },

  /**
   * Compose the Extent form editor for a map UI
   *  
   * @return Ext.form.FieldContainer
   */
  getExtentFields: function () {
    var me = this;
    var defaultsNumber = {
      labelAlign: 'right',
      labelSeparator: ' ',
      labelWidth: 'auto',
      padding: '0 0 0 10',
      xtype: 'numberfield',
      allowDecimals: true,
      allowBlank: false
    };
    return Ext.apply(Ext.apply({}, Carmen.Util.defaultFormConfig), {
      xtype: 'fieldcontainer',
      fieldLabel: _T_MAPFORM('extra.mapExtent'),
      name: 'extent_cont',
      layout: {
        type: 'table',
        columns: 3
      },

      defaults: {
        labelAlign: 'right',
        labelWidth: 50,
        labelSeparator: ' ',
        xtype: 'numberfield'
      },

      items: [
        //row1
        {
          columnWidth: 0.3,
          xtype: 'radio',
          boxLabel: _T_MAPFORM('extra.mapExtent_by_coord'),
          id: 'mapExtent_by_coord',
          name: 'map_extent_by',
          value: true
        }, {
          columnWidth: 0.35,
          fieldLabel: _T_MAPFORM('fields.mapExtentXmin'),
          name: 'mapExtentXmin'
        }, {
          columnWidth: 0.35,
          fieldLabel: _T_MAPFORM('fields.mapExtentYmin'),
          name: 'mapExtentYmin'
        }
        //row2
        , {
          columnWidth: 0.3,
          xtype: 'button',
          text: "Capturer l'emprise courante",
          handler: function () {
            var extent = app.map.OL_map.getExtent();
            var corresp = { left: 'mapExtentXmin', right: 'mapExtentXmax', bottom: 'mapExtentYmin', top: 'mapExtentYmax' };
            Ext.iterate(corresp, function (fieldExtent, fieldForm) {
              var cmp = me.down('[name=' + fieldForm + ']');
              if (!cmp) return;
              cmp.setValue(extent[fieldExtent]);
            });
            var msg = Ext.Msg.alert("Succès", "Mise à jour de l'emprise de la carte à partir de l'emprise courante");
            Ext.Function.defer(function () { msg.hide() }, 2000);
          }
        }, {
          columnWidth: 0.35,
          fieldLabel: _T_MAPFORM('fields.mapExtentXmax'),
          name: 'mapExtentXmax'
        }, {
          columnWidth: 0.35,
          fieldLabel: _T_MAPFORM('fields.mapExtentYmax'),
          name: 'mapExtentYmax'
        }
        //row3
      ]
    })
  },

  addExtentByLayer: function () {
    var me = this;
    var map = this.getRecord(); if (!map) return;
    var layers = map.get('layers'); if (!layers) return; if (!(layers instanceof Ext.data.Store)) return;
    var hasLayer = layers.getCount(); if (!hasLayer) return;
    var items = [{
      xtype: 'radio',
      boxLabel: _T_MAPFORM('extra.mapExtent_by_layer'),
      name: 'map_extent_by',
      id: 'mapExtent_by_layer'
    }, {
      colspan: 2,
      padding: '0 0 0 10',
      labelWidth: 15,
      width: Carmen.Util.WINDOW_WIDTH - Carmen.Util.defaultFormConfig.labelWidth * 2 - 80,
      name: 'mapExtentByLayer',
      id: 'mapExtentByLayer',
      xtype: 'combobox',
      queryMode: 'local',
      displayField: 'layerTitle',
      valueField: 'layerMsname',
      store: this.getRecord().get('layers'),
      autoLoadOnValue: true,
      autoSelect: true,
      emptyText: _T_MAPFORM('extra.mapExtentLayer_empty'),
      listeners: {
        select: function (cmp, record) {
          if (!record) return;
          var affectExtent = function (extent) {
            Ext.iterate(extent, function (field, value) {
              var coord = field.toUpperCase().replace(/_?(min|max)_?/gi, '');
              var type = field.toLowerCase().replace(/.*_?(min|max)_?.*/gi, '$1');
              var input = me.down('[name=mapExtent' + coord + type + ']');
              if (input) {
                input.setValue(value);
              }
            });
            Ext.getCmp('mapExtent_by_coord').setValue(true);
            var msg = Ext.Msg.alert("Succès", "Mise à jour de l'emprise de la carte à partir de l'emprise de la couche " + record.get('layerTitle'));
            Ext.Function.defer(function () { msg.hide() }, 2000);
            cmp.reset();
          }
          var layer = record;
          var extent;
          if (extent = layer.get('msLayerExtent')) {
            affectExtent(extent)
            return;
          }
          me.down('[name=extent_cont]').mask();
          Ext.Ajax.request({
            url: Routing.generate('carmen_ws_helpers_layerextent', {
              projection: Carmen.Dictionaries.PROJECTIONS.findRecord('projectionEpsg', me.down('[name=mapProjectionEpsg]').getValue()).get('projectionProvider') + ":" + me.down('[name=mapProjectionEpsg]').getValue(),
              mapfile: Carmen.user.map.get('real_mapfile'),
              layer: layer.get('layerMsname')
            }),
            method: 'GET',
            success: function (response) {
              me.down('[name=extent_cont]').unmask();
              response = Ext.decode(response.responseText);
              if (!response) return;
              var extent = response.extent;
              if (!extent) return;
              //layer.set('msLayerExtent', extent);
              affectExtent(extent);
            },
            failure: function (response) {
              me.down('[name=extent_cont]').unmask();
              if (Carmen.notAuthenticatedException(response)) return;
              cmp.reset();
            }
          });
        }
      }
    }
    ];

    if (!this.down('[name=mapExtentByLayer]')) {
      this.down('[name=extent_cont]').add(items);
    }
  }

});
