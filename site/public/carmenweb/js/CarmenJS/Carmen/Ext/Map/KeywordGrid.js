Ext.define('Carmen.Utilities.KeywordGrid', {
  extend : 'Ext.panel.Panel',
  xtype : 'carmen_keywordgrid',
  layout : 'auto',
border : true,
  
  /*START Form Field comportement*/
  mixins: [
    'Ext.form.Labelable',
    'Ext.form.field.Field'
  ],
  isFormField : true,
  isFieldLabelable : true,
  /**
   * See {@link Ext.form.field.Field#getValue getValue}
   * @return {Object} value The field value
   */
  getValue : function(){
    var res = {};
    res[this.name||'keywords'] = this.store.getData().getValues('data');
    return res;
  },
  /**
   * See {@link Ext.form.field.Field#setValue setValue}
   * @param {Object} value The value to set
   * @return {Ext.form.field.Field} this
   */
  setValue : function(keywords){
    if ( this.store ){
      this.store.loadData(keywords);
    }
  },
  /**
   * See {@link Ext.form.field.Field#getSubmitData getSubmitData}
   * @return {Object} A mapping of submit parameter names to values; each value should be a string, or an array of
   * strings if that particular name has multiple values. It can also return null if there are no parameters to be
   * submitted.
   */
  getSubmitData : function(){
    return this.getValue();
  },
  /**
   * See {@link Ext.form.field.Field#getModelData getModelData}
   * @return {Object} A mapping of submit parameter names to values; each value should be a string, or an array of
   * strings if that particular name has multiple values. It can also return null if there are no parameters to be
   * submitted.
   */
  getModelData : function(){
    return this.getValue();
  },
  /**
   * See {@link Ext.form.field.Field#isDirty isDirty}
   * @return {Boolean} True if this field has been changed from its original value (and is not disabled),
   * false otherwise.
   */
  isDirty : function(){
    return (this.store.getModifiedRecords().length + this.store.getNewRecords().length)>0;
  },
  /**
   * See {@link Ext.form.field.Field#isValid isValid}
   * @return {Boolean} True if the value is valid, else false
   */
  isValid : function(){return true;},
  /**
   * See {@link Ext.form.field.Field#isFileUpload isFileUpload}
   * @return {Boolean}
   */
  isFileUpload : function(){return false;},
  /**
   * See {@link Ext.form.field.Field#validate validate}
   * @return {Boolean} True if the value is valid, else false
   */
  validate : function(){return true;},
  /**
   * See {@link Ext.form.field.Field#reset reset}
   */
  reset : function(){return this.store.removeAll();},
  /*END Form Field comportement*/
  /**
   * 
   */
  initComponent : function() {
    !Carmen.Dictionaries.KEYWORDS.loadCount && Carmen.Dictionaries.KEYWORDS.load();
    var me = this;

    me.store = new Ext.data.JsonStore({
      autoLoad : false,
      fields : [{
        name : 'category',
        id : 'category'
      }, {
        name : 'categoryName',
        id : 'categoryName',
        depends : ['category'],
        convert : function(value, record){
          if ( !value ){
            var category = record.get('category');
            if ( Ext.isObject(category) ){
              value = category['categoryName'];
            } else {
              var storeCategories = Carmen.Dictionaries.KEYWORDS_CATEGORIES;
              var index, record;
              if ( (index = storeCategories.find('categoryId', category))!=-1 ){
                record = storeCategories.getAt(index);
                value = record.get('categoryName');
              }
            }
          }
          return value;
        }
      }, {
        name : 'keywordId',
        id : 'keywordId'
      }, {
        name : 'keywordName',
        id : 'keywordName'
      }, {
        name : 'pkey',
        calculate : function(data) {
          return [data.category, data.keywordId || Ext.id('keyword-')]
              .join('_');
        }
      }],
      idProperty : 'pkey',
      data : []
    });
    //

    me.headerGrid = {
      padding : '0 0 5 0',
      xtype : 'panel',
      layout : {type:'hbox', pack:'stretchmax'},
      defaults : {
        labelWidth : 'auto',
        margin : '0 10 0 0'
      },
      items : [{

        fieldLabel : _t('KeywordGrid.category_keyword'),
        xtype : 'combobox',
        name : 'keywordgrid-category',

        queryMode : 'local',

        displayField : 'displayField',
        valueField : 'valueField',
        store : Carmen.Dictionaries.KEYWORDS_CATEGORIES,
        value : Carmen.Defaults.FREEKEYWORDS_CATEGORY_ID,
        autoLoadOnValue : true,
        autoSelect : true,
        listeners : {
          afterrender : function(combo) {
            combo.fireEvent('select', combo);
          },
          select : function(combo) {
            var categoryStore = Carmen.Dictionaries.KEYWORDS_CATEGORIES;
            var cat_index;
            
            var selected_category = combo.getValue();
            if ((cat_index = categoryStore.find('categoryId', selected_category)) == -1)
              return;
              
            var canCreate = (selected_category == Carmen.Defaults.FREEKEYWORDS_CATEGORY_ID);
            var keywordCmp = me.down('[name=keywordgrid-keyword]');
            if (!keywordCmp)
              return;
            var keywordStore = keywordCmp.getStore();

            keywordStore.clearFilter();
            keywordStore.filterBy(function(record){
            	return me.isKeywordCategory(record, selected_category);
            });

            keywordCmp.emptyText = (canCreate
                ? _t('KeywordGrid.can_select_or_create')
                : _t('KeywordGrid.can_select_only'));
            keywordCmp.setValue(null);
          }
        }
      }, {
        fieldLabel : _t('KeywordGrid.keyword'),
        align : 'center',
        xtype : 'combobox',
        name : 'keywordgrid-keyword',
        emptyText : _t('KeywordGrid.can_select_or_create'),

        queryMode : 'local',
        editable : true,

        displayField : 'displayField',
        valueField : 'valueField',
        store : Carmen.Dictionaries.KEYWORDS

      }, {
        xtype : 'button',
        height : '100%',
        text : _t('KeywordGrid.add_keyword'),

        handler : function() {
          var categoryCmp = me.down('[name=keywordgrid-category]');
          var keywordCmp = me.down('[name=keywordgrid-keyword]');
          var keywordStore = (keywordCmp ? keywordCmp.getStore() : null);
          var categoryId, categoryName, keywordId, keywordName;

          if (!(categoryCmp && keywordCmp))
            return;

          categoryId = categoryCmp.getValue();
          categoryName = categoryCmp.getRawValue();

          if (!categoryId)
            return;
          if (Carmen.Dictionaries.KEYWORDS_CATEGORIES.find('categoryId', categoryId) == -1)
            return;

          keywordId = keywordCmp.getValue();
          keywordName = keywordCmp.getRawValue();

          if (!keywordId)
            return;

          var canCreate = (categoryId == Carmen.Defaults.FREEKEYWORDS_CATEGORY_ID);
          var exists = keywordStore.findBy(function(rec) {
            return me.isKeywordCategory(rec, categoryId)
                && rec.get('keywordId') == keywordId;
          }) != -1;

          var record = {
            category : categoryId,
            categoryName : categoryName,
            keywordId : keywordId,
            keywordName : keywordName
          };

          if (!exists && !canCreate) {
            return;
          } else if (!exists && canCreate) {
            record.keywordId = null;
          }
          me.store.add(record);

        }
      }]
    };

    me.dataGrid = Ext.create('Ext.grid.Panel', {
    	width : 620,
      //width : 602,
    	padding : 0,
      reserveScrollbar : true,
      viewConfig : {
        stripeRows : true,
        enableTextSelection : false,
        scrollable : true
      },
      columnLines: true,
      height:me.height-40,
      border:true,
      store : me.store,
      columns : [{
        width : 250,
        text : _t('KeywordGrid.category_keyword'),
        align : 'center',
        dataIndex : 'categoryName',
        renderer : function(value, meta, record, rowIndex) {
          return value 
            + "<input type='hidden' name='keywords[" + rowIndex + "][category]' value='" + (record.get('category')) + "'/>";
        }
      }, {
        width : 250,
        text : _t('KeywordGrid.keyword'),
        align : 'center',
        dataIndex : 'keywordName',
        renderer : function(value, meta, record, rowIndex) {
          return value 
            + "<input type='hidden' name='keywords[" + rowIndex + "][keywordId]' value='" + (record.get('keywordId')) + "'/>";
        }
      }, {
        width : 100,
        header : _t('KeywordGrid.actions'),
        align : 'center',
        xtype : 'actioncolumn',
        items : [{
              icon : '/images/delete.png',
              tooltip : _t('KeywordGrid.delete_keyword'),
              handler : function(grid, rowIndex, colIndex) {
                if ( !confirm(_t('KeywordGrid.confirm_delete')) ) return;
                var rec = grid.getStore().removeAt(rowIndex);
              }
            }]
      }]
    });
    me.items = [me.headerGrid, me.dataGrid];

    me.callParent();
  },
  
  isKeywordCategory : function(keywordRecord, categoryId)
  {
    var keywordCategory = keywordRecord.getCategory();
    if ( keywordCategory instanceof Ext.data.Model ){
      return keywordCategory.get('categoryId')==categoryId;
    }
    if ( Ext.isObject(keywordCategory) ){
      return keywordCategory['categoryId']==categoryId;
    }
    return keywordCategory==categoryId;
  }
})