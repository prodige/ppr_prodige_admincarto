
///////////////////////////////////////////
// Banner Model Editor
///////////////////////////////////////////
var MODEL_LABEL_TPL = Ext.XTemplate('{name}{extention}@<tpl if="!label">{name}<tpl else>{label}</tpl>');
var CMP_NAME = 'Carmen.Window.MapUIPrintModelEditor';

var TRANSLATOR = function(key){
  var root = "Map.forms.printModelEditor.";
  return (key!==null ? _t( root + key ) : root);
};

Ext.define(CMP_NAME, {
  extend: 'Carmen.Generic.Window', 
  
  with_label : false,
  scrollable : false,
  modal: false,
  frame: true,
  closeAction : 'destroy',
  width: Carmen.Util.WINDOW_WIDTH - 40,
  height: 500,
  padding: 5,
  layout: {
    type: 'vbox',
    pack: 'start',
    align: 'stretch'
  },
  
  getUrl : function(radical){
    return radical+'_'+this.datatype;
  },
  
  getCmpId : function(radical){
    return this.datatype+radical;
  },
  
  getCmp : function(radical){
    return Ext.getCmp(this.getCmpId(radical));
  },
  
  saveData: function(dataValue, data, template, fnSuccess) {
    var logo = Ext.ComponentQuery.query('[name=MapUIModelLogo]')[0].value;
    var me = this;
    var paramurl = {};
    paramurl[me.datatype] = dataValue;
    Ext.Ajax.request({
      method: 'POST',
      url: Routing.generate(me.getUrl('carmen_ws_mapui_post'), paramurl),
      params: {
        content: data,
        template : template, 
        // ajout du logo
        logo: logo
      },
      success: fnSuccess,
      failure: function(response) {
        if (Carmen.notAuthenticatedException(response) ) return;
        Ext.MessageBox.show({
          title: _t('app.btn.save'),
          msg: me.TRANSLATOR('extra.saveErrorMsg'),
          buttons: Ext.MessageBox.OK,
          icon: Ext.MessageBox.ERROR
        });
      }
    });
  },
  
  removeData: function(dataValue, fnSuccess) {
    var me = this;
    var paramurl = {};
    paramurl[me.datatype] = dataValue;
    Ext.Ajax.request({
      method: 'DELETE',
      url: Routing.generate(me.getUrl('carmen_ws_mapui_delete'), paramurl),
      success: fnSuccess,
      failure: function(response) {
        if (Carmen.notAuthenticatedException(response) ) return;
        Ext.MessageBox.show({
          title: _t('form.confirmDeleteTitle'),
          msg: _t('form.msgFailureDelete'),
          buttons: Ext.MessageBox.OK,
          icon: Ext.MessageBox.ERROR
        });
      }
    });
  },
  
  loadData: function(dataValue) {
    var me = this;
    var paramurl = {};
    paramurl[me.datatype] = dataValue;
    
    if( dataValue ) {
    	me.htmleditor.mask(me.TRANSLATOR('extra.loading'))
      Ext.Ajax.request({
          url: Routing.generate(me.getUrl('carmen_ws_mapui_get'), paramurl),
          success: function(response){
          	me.htmleditor.unmask()
            var text = response.responseText;
            me.htmleditor.setValue(text);
          }, 
          failure: function(response) {
            if (Carmen.notAuthenticatedException(response) ) return;
          }
      });
    } else {
    	me.htmleditor.setValue('');
    }

    return true;
  },
  
  addDataWindow : function(){
    var me = this;
    var win;
    win = Ext.create('Ext.Window', {
      closeAction : 'destroy',
      listeners : {
        beforeshow : function(){me.modele_store.reload()}
      },
      title : me.TRANSLATOR('extra.newModelTitle'), 
      width : 500,
      items : [{
        xtype : 'form',
        layout : 'anchor',
        padding : 10,
        defaults : {labelWidth : 200, width : '100%'},
        items : [{
          xtype : 'combobox',
          emptyText : me.TRANSLATOR('extra.emptyTemplate'),
          name : 'from',
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          fieldLabel : me.TRANSLATOR('extra.newModelSelect'),
          store: me.modele_store
        }, {
          xtype : 'textfield',
          name : me.datatype,
          fieldLabel : me.TRANSLATOR('extra.newModelName'),
          allowBlank : false,
          vtype : 'identifier'
        }].concat(
          this.with_label
        ? [{
          xtype : 'textfield',
          name : me.datatype+'_label',
          fieldLabel : me.TRANSLATOR('extra.newModelLabel'),
          allowBlank : false
        }]
        : [] 
        )
      }],
      buttons : [{
        text : _t('app.btn.save'),
        handler : function(){
          var form = win.down('form');
          if ( !form ) {win.close();return;}
          if ( !form.isValid() ) return;
          var values = form.getValues();
          var dataName = values[me.datatype]+me.extension;
          if ( me.with_label ){
          	dataName = MODEL_LABEL_TPL.apply({name : values[me.datatype], label : values[me.datatype+'_label'], extension : me.extention});
          }
          me.saveData(dataName, (!values.from ? me.TRANSLATOR('extra.emptyTemplate') : null), values.from, function(response){
            
            var data = Ext.decode(response.responseText);
            var record
            if ( !(record = me.store.findRecord('id', data.id)) ){
              me.store.loadRawData([data], true);
              record = me.store.findRecord('id', data.id);
            }
            if ( !record ) {
              win.close();
              return;
            }
            // select the newly created value
            me.datalist.select(record);
            win.close();
          });
        }
      },{
        text : _t('app.btn.cancel'),
        handler : function(){win.close()}
      }]
    });
    win.show();
  },
  
  
  patchDataWindow : function (current_model_name) {
      
    var me = this;
    var logovalue = ""; 
    var record = me.store.findRecord('accountModelFile', current_model_name); 
    if(record) {
        logovalue = record.get('accountModelLogo'); 
    }
    
    var win;
    win = Ext.create('Ext.Window', {
        closeAction : 'destroy',
        listeners : {
            beforeshow : function(){me.modele_store.reload()}
        },
        title : me.TRANSLATOR('extra.editModel'), 
        width : 500,
        items : [{
            xtype : 'form',
            layout : 'anchor',
            padding : 10,
            defaults : {labelWidth : 200, width : '100%'},
            items : [{
                xtype : 'textfield',
                value : current_model_name, 
                name : 'oldModelName', 
                hidden: true
            },{
                xtype : 'textfield',
                name : me.datatype,
                fieldLabel : me.TRANSLATOR('extra.newEditModelName'),
                allowBlank : false,
                vtype : 'identifier', 
                value : current_model_name
            },{
                fieldLabel: me.TRANSLATOR('extra.newEditLogoText'),
                name : 'ModelPrintLogoField',
                xtype: 'textfield',
                store: Carmen.Dictionaries.LOGOPATH_IMAGES,
                editable: false,
                autoLoadOnValue : true,
                autoSelect : true,
                value : logovalue,
                listeners : {
                    change: function(me, value) {
                        Ext.ComponentQuery.query('[name=MapUIModelLogo]')[0].setValue(value); 
                        record.set("accountModelLogo", value); 
                    }
                }
            }, { 
                xtype: 'button',
                text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
                name : 'modelUiLogopath_button',
                handler: function(){
                    var windowClass = "Ext.ux.FileBrowserWindow";
                    var window = Ext.getCmp(CMP_REFERENCE(windowClass)) || Ext.create(windowClass, {
                        id: CMP_REFERENCE(windowClass),
                        selectExtension: '*.jpg,*.gif,*.png',
                        uploadExtension: '.jpg,.gif,.png',
                        dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
                        defaultPath: '/Root',
                        relativeRoot: '/Root/IHM',
                        listeners: {  
                            selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath)  {
                                var ctrlTo = Ext.ComponentQuery.query('[name=ModelPrintLogoField]')[0]; 
                                ctrlTo.setValue(fullPathName);
                                wind.close();
                            }
                        }
                    });
                    window.show();
                },
                listeners : {
                    afterrender : function(){
                        var check = me.down('[name=uiLogo]');
                        if ( check ){check.fireEvent('change', check);}
                    }
                }
            }].concat(
                this.with_label
                ? [{
                    xtype : 'textfield',
                    name : me.datatype+'_label',
                    fieldLabel : me.TRANSLATOR('extra.newModelLabel'),
                    allowBlank : false
                }]
                : [] 
            )
        }],
        buttons : [{
            text : _t('app.btn.save'),
            handler : function(){

                var form = win.down('form');
                if ( !form ) {win.close();return;}
                if ( !form.isValid() ) return;
                var values = form.getValues();
                var dataName = values[me.datatype]+me.extension;

                // oldModelName
                var old_model_name = Ext.ComponentQuery.query('[name=oldModelName]')[0].value; 
                var valueField = old_model_name+me.extension;

                var logo = Ext.ComponentQuery.query('[name=MapUIModelLogo]')[0].value;
                var paramurl = {};
                paramurl[me.datatype] = values[me.datatype];

                Ext.Ajax.request({
                  method: 'POST',
                  url: Routing.generate(me.getUrl('carmen_ws_mapui_post'), paramurl),
                  params: {
                    content: "",
                    template : "", 
                    // ajout du logo
                    logo: logo, 
                    old_file : old_model_name+me.extension, 
                    new_model_name : values[me.datatype]+me.extension, 
                  },
                  success: function () {},
                  failure: function(response) {
                    if (Carmen.notAuthenticatedException(response) ) return;
                    Ext.MessageBox.show({
                      title: _t('app.btn.save'),
                      msg: me.TRANSLATOR('extra.saveErrorMsg'),
                      buttons: Ext.MessageBox.OK,
                      icon: Ext.MessageBox.ERROR
                    });
                  }
                });            

                var record =  me.store.findRecord('valueField', valueField); 
                record.set("accountModelFile", dataName); 
                record.set("valueField", dataName); 
                record.set("accountModelName",values[me.datatype]); 
                record.set("displayField",values[me.datatype]);                 

                // select the newly created value
                me.datalist.select(record);
                win.close();                 
            }
        },{
            text : _t('app.btn.cancel'),
            handler : function(){win.close()}
        }]
    });
    win.show();      
  },
  
  initComponent : function() {
    var me = this;
    
    me.title = me.TRANSLATOR('title');
    me.bodyStyle = 'overflow:hidden !important';
    me.buttons = [
      {
        text: _t('app.btn.save'),
        tooltip: me.TRANSLATOR('extra.saveTooltip'),
        handler: function() {
          var dataValue = me.datalist.getValue();
          if( dataValue && dataValue.length>0 ) {
            me.saveData(dataValue, me.htmleditor.getValue(), null, function(response){
              var content = response.responseText;
              me.htmleditor.setValue(content);
              Carmen.Notification.msg(me.TRANSLATOR('title'), _t("form.msgApplied"), 2);
            });
          }
        }
      },
      {
        text: _t('app.btn.close'),
        handler : function(){me.close()}
      }
    ];
    
    this.dicocopy = Ext.create(me.DICTIONARY.$className, me.DICTIONARY.initialConfig);
    var storeConfig = {
      proxy : {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'users'
        }
      },
      reload : function(){
        me.dicocopy.on('load', function(){
          this.loadData(Ext.Array.pluck(me.dicocopy.getData().items, 'data')); 
        }, this, {single : true})
        me.dicocopy.reload();
      }
    };
    this.store = Ext.create('Ext.data.JsonStore', Ext.apply({
      fields : Ext.Array.map([].concat(me.DICTIONARY.getModel().getFields()), Ext.clone), 
      data : Ext.Array.pluck(me.DICTIONARY.getData().items, 'data'), 
      logo : ""
    }, storeConfig));
    this.store.filter('default', false);
    this.store.reload();          

    this.modele_store = Ext.create('Ext.data.JsonStore', Ext.apply({
      fields : Ext.Array.map([].concat(me.DICTIONARY.getModel().getFields()), Ext.clone), 
      data : Ext.Array.pluck(me.DICTIONARY.getData().items, 'data'),          
    }, storeConfig));
    
    me.height = 500;
    me.padding = 10;
    me.items = [{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        items: [{
            xtype: 'combobox',
            fieldLabel: me.TRANSLATOR('extra.selectModel'),
            labelWidth: 140,
            labelAlign: 'left',
            flex: 3,
            padding: '0 0 0 10',
            queryMode: 'local',
            displayField: 'displayField',
            emptyText : me.TRANSLATOR('extra.noSelection'),
            valueField: 'valueField',
            store: me.store,
            editable: false,
            autoLoadOnValue : true,
            autoSelect : true,
            triggers: {
              remove: {
                  setDisabled : function(disabled){
                    this.disabled = disabled;
                    this.el[disabled ? 'mask' : 'unmask']();
                  },
                  onFieldRender : function(){
                  	Ext.form.trigger.Trigger.prototype.onFieldRender.apply(this);
                  	this.setDisabled(this.disabled);
                  },
                  disabled : true,
                  cls: 'trigger-remove',
                  handler: function(combobox) {
                  	if ( this.disabled ) return;
                    var dataValue = combobox.getValue();
                    if( dataValue && dataValue.length>0 ) {
                      Ext.MessageBox.confirm(_t('form.confirmDeleteTitle'), me.TRANSLATOR('extra.removeMsg'), function(btn){
                        if( btn == 'yes' ) {
                          me.removeData(dataValue, function(response){
                            // reload data store
                            me.store.reload();
                            // remove this record from the dictionnary
                            var rec = me.store.find('valueField', dataValue);
                            if ( rec )me.store.remove(rec);
                            // unset current selected data
                            combobox.setValue(null);
                            me.htmleditor.setValue('');
                            // ok done
                            Carmen.Notification.msg(me.TRANSLATOR('title'), _t("form.msgApplied"), 2);
                          });
                        }
                      });
                    }
                  }
              }
            },
            listeners: {
              change: function(combobox, newValue, oldValue, eOpts ) {

                me.loadData(newValue);
              	me.htmleditor.setDisabled(!newValue);
              	var triggers = combobox.getTriggers();
              	triggers.remove.setDisabled(!newValue);
                
                // gestion du bouton Modifier 
                // actif si un modèle est sélectionné
                if(combobox.rawValue !=="") {
                    me.down('[name=patchDataWindowButton]').setDisabled(false);
                    me.down('[name=MapUIModelLogo]').setVisible(true);
                    
                    var record = me.store.findRecord('accountModelFile', newValue); 
                    if(record) {
                        me.down('[name=MapUIModelLogo]').setValue(record.get('accountModelLogo')); 
                    }
                }
              }
            }
          }, {
            margin : '0 0 0 5',
            xtype : 'button', 
            text : me.TRANSLATOR('extra.newEditButton'), 
            handler: function() {
              me.addDataWindow();
            }
          }, {
            margin : '0 0 0 5',
            xtype : 'button', 
            text : me.TRANSLATOR('extra.editModel'), 
            disabled: true,
            name : "patchDataWindowButton",
            handler: function() {
                var model_current_name  = me.down('[xtype=combobox]').rawValue;
                me.patchDataWindow(model_current_name);
            }              
          },{ 
            html:'', 
            flex:1 
        }]
    },{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        items: [{
            padding: '0 0 0 10',
            name : 'MapUIModelLogo',
            xtype : 'displayfield',
            displayField: 'displayfield',
            hidden: true,
            fieldLabel: me.TRANSLATOR('extra.actualEditModelLogo'),           
        }]
    }, {
      	border : false,
        xtype: 'ckeditor',
        disabled : true,
        reference : 'htmleditor',
        height: Math.max(100, me.height-200),
        width : '100%',margin : '0 10 0 10', maskElement : 'body'
    }];
    this.callParent();
    this.on('resize', function(){me.down('ckeditor').setHeight(me.getHeight()-100)}, this);
    this.on('afterrender', function(){
      this.datalist = this.down('combobox');
      this.htmleditor = this.down('ckeditor');
      
      if ( this.initialValue ){
        this.datalist.setValue(this.initialValue);
      }
    }, this);
    this.on('close', function(){
      this.DICTIONARY.reload();
    }, this);
  }
});