/**
 * Translate text classified by prefix (fixed) and key
 * @param string key
 * @return string
 */
var MAPUI_TRANSLATE = function(key){
  var root = "Map.forms.ui.";
  return (key!==null ? _t( root + key ) : root);
};
var MAPUIBANNER_TRANSLATE = function(key){
  var root = "Map.forms.bannerEditor.";
  return (key!==null ? _t( root + key ) : root);
};
var MAPUIPRINT_TRANSLATE = function(key){
  var root = "Map.forms.printModelEditor.";
  return (key!==null ? _t( root + key ) : root);
};


var DEFAULT_REFERENCE_WIDTH = 180; 
var DEFAULT_REFERENCE_HEIGHT = 100; 


/**
 * Carmen.Model.MapLocator
 */
Ext.define('Carmen.Model.MapLocator', {
  extend: 'Ext.data.Model',
  idProperty : 'locatorId',
  fields: [
    { name: "locatorId", persist : false }
  , { name: "locatorCriteriaRank"         , type: "integer" }
  , { name: "locatorCriteriaName"         , type: "string"  }
  , { name: "locatorCriteriaLayerId"      , type: "integer" }
  , { name: "locatorCriteriaFieldId"      , type: "string"  }
  , { name: "locatorCriteriaFieldName"    , type: "string"  }
  , { name: "locatorCriteriaRelated"      /* int or MapLocator*/, defaultValue: null }
  , { name: "locatorCriteriaRelatedField" , type: "string" , defaultValue: null }
  , { name: "locatorCriteriaVisibility"   , type: "boolean" }
  , { name: "locatorInGlobalMapSearchEngine"   , type: "boolean", defaultValue: false }
  ]
});

/**
 * Carmen.Model.MapUI
 */
Ext.define('Carmen.Model.MapUI', {
  extend: 'Ext.data.Model',
  idProperty : 'uiId',
  fields: [
    {
      allowNull : false,
      persist: false,
      name: 'map',
      reference : 'Carmen.Model.Map'
    , serialize : function(map){return map.get('mapId');}
    },
    { name: "uiId", persist : false }
  , { name: "uiLogo"                     , type: "boolean" }
  , { name: "uiLogopath"                 , type: "string"  }
  , { name: "uiCopyright"                , type: "boolean" }
  , { name: "uiCopyrightText"            , type: "string"  }
  , { name: "uiColor"                    , type: "int"     , defaultValue: 1,
      convert: function(value, record){
        if( value && Ext.isObject(value) ) {
          return value.colorId;
        } 
        return value;
      }
    }
  , { name: "uiBanner"                   , type: "boolean" }
  , { name: "uiBannerFile"               , type: "string"  }
  , { name: "uiLegend"                   , type: "boolean" , defaultValue : true}
  , { name: "uiLegendPrint"              , type: "boolean" , defaultValue : true}
  , { name: "uiKeymap"                   , type: "boolean" , defaultValue : true}
  , { name: "uiFocus"                    , type: "boolean" }
  , { name: "uiLocate"                   , type: "boolean" }
  , { name: "uiModels", 
      convert: function(value, record){
        var data = [];
        if( value && Ext.isArray(value) ) {
          Ext.each(value, function(uiModel){
            if ( Ext.isObject(uiModel) ){
              data.push( uiModel.accountModel.accountModelFile );
            } else {
              data.push( uiModel );
            }
          });
          return data;
        }
        else if( value && Ext.isObject(value) ) {
          Ext.iterate(value, function(key, uiModel){
            if ( Ext.isObject(uiModel) ){
              data.push( uiModel.accountModel.accountModelFile );
            } else {
              data.push( uiModel );
            }
          });
          return data;
        }
        return value;
      }
    }
  , { name: "helpDisplay"                   , type: "boolean" }

    //help message is retrieved from ckeditor @see Map.js:256
  , { name: "helpMessage"                   , type: "string" }
  , { name: "cguDisplay"                   , type: "boolean" }
  , { name: "cguMessage"                   , type: "string" }
  ]
});

/**
 * Carmen.Model.msReferenceMap
 */
Ext.define('Carmen.Model.msReferenceMap', {
  extend: 'Ext.data.Model',
  getWidth : function(){
    return this.get('width') || DEFAULT_REFERENCE_WIDTH;
  },
  getHeight : function(){
    return this.get('height') || DEFAULT_REFERENCE_HEIGHT;
  },
  getSize : function(){
    return [this.getWidth(), this.getHeight()].join(' ');
  },
  fields: [
    {
      allowNull : false,
      persist: false,
      name: 'map',
      reference : 'Carmen.Model.Map',
      serialize : function(map){return map.get('mapId');}
    }
  , { name: "status"        , type: "string", defaultValue: "ON" }
  , { name: "color"        , type: "string", defaultValue: null }
  , { name: "outlinecolor" , type: 'string'  , defaultValue: "#140000" }
  , { name: "width"        , type: 'number', defaultValue: DEFAULT_REFERENCE_WIDTH, serialize : function(value, record){return record.getWidth();}}
  , { name: "height"       , type: 'number', defaultValue: DEFAULT_REFERENCE_HEIGHT, serialize : function(value, record){return record.getHeight();}}
  , { name: "size"         , type: 'string', defaultValue: "", 
      serialize : function(value, record){
        return record.getSize();
      }
    }
  , { name: "extent", 
      convert: function(extent, record) {
        if( !extent ) {
          var map = record.get('map');
          // re-compute referenceMap extent based on map extent and referenceMap size
          var extent = Carmen.FormSheet.MapUI.computeExtent(
              map.get('mapExtentXmin'), 
              map.get('mapExtentYmin'), 
              map.get('mapExtentXmax'), 
              map.get('mapExtentYmax'), 
              record.getWidth() , 
              record.getHeight()
          );
        }
        if ( Ext.isArray(extent) ) {
          extent = extent.join(' ');
        }
        if ( Ext.isObject(extent) && Ext.isDefined(extent.xmin) ){ 
          extent = extent.xmin + ' ' + extent.ymin + ' ' + extent.xmax + ' ' + extent.ymax;
        }
        if ( Ext.isObject(extent) && Ext.isDefined(extent.minx) ){ 
          extent = extent.minx + ' ' + extent.miny + ' ' + extent.maxx + ' ' + extent.maxy;
        }
        return extent;
      }
    }
  , { name: "image"       , type: 'string' }
  , { name: "base64image" , type: 'string', persist:false }
  ]
});

///////////////////////////////////////////
// MapLocator Grid
///////////////////////////////////////////
Ext.define('Carmen.grid.MapLocator', {
  extend: 'Ext.panel.Panel',
  xtype : 'cmnmaplocator',
  //columnLines: true,
  flex : 1,
  maxHeight : 250,
  width : '100%',
  scrollable : 'y',
  
  // this grid is considerd a formfield so it can post its data
  isFormField : true,
  isFieldLabelable : true,  
  setValue : function(locators){
    if ( this.store ){
      this.store.loadData(locators || [], false);
    }
  },  
  getValue : function(){
  	return this.store.getData().getValues('data');
  },
  getSubmitData : function(){
    return this.getModelData();
  },
  getModelData : function(){
    var res = {};
    res[this.name||'locators'] = this.getValue();
    return res;
  },
  isDirty : function(){
    return (this.store.getModifiedRecords().length + this.store.getNewRecords().length)>0;
  },
  isValid : function(){return true;},
  isFileUpload : function(){return false},
  validate : function(){return true},
  
  storeRelated : Ext.create('Ext.data.Store', {
    autoLoad: false,
    fields: [
      { name: 'displayField' },
      { name: 'valueField' }
    ],
    data: []
  }),
  
  initComponent : function()
  {
    var me = this;
    
    //inline buttons
    me.buttons = [{
        tooltip: _t('Map.forms.ui.extra.addCriteria'),
        text: _t('Map.forms.ui.extra.addCriteria'),
        style: {
          borderSpacing: 0
        },
        handler: function() {
          me.store.add( Ext.create('Carmen.Model.MapLocator', {locatorCriteriaRank: me.store.count()}) );
          me.grid.focus();
          me.grid.getView().focusRow(me.store.getCount()-1);
        }
    }];
    me.items= [{
      maxHeight : 200,
      width : '100%',
      columnLines: true,
      forceFit: true,
      scrollable : 'y',
      xtype : 'gridpanel',
      sortableColumns : false,
      enableColumnHide : false,
      enableColumnMove : false,
      enableColumnResize : false,
      store : Ext.create('Ext.data.Store', {
        autoLoad: false,
        model: 'Carmen.Model.MapLocator',
        sorters: 'locatorCriteriaRank',
        data: []
      }),
      viewConfig : {
        width : '100%'
      },
      border : true,
    
      columns : {
      defaults: {
          menuDisabled: true,
          width : 150,
          maxWidth : 150
      },
      items : [new Ext.grid.column.RowNumberer({
        width: 30,
        align : 'center'
      }), {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaName'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaName'),
        dataIndex: 'locatorCriteriaName',
        xtype: 'widgetcolumn',
          maxWidth : 150,
        widget: {
          margin : '5 0 5 0',
          xtype: 'textfield',
          listeners: {
            change: function(widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            }
          }
        }
      }, {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaLayerId'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaLayerId'),
        dataIndex: 'locatorCriteriaLayerId',
        xtype: 'widgetcolumn',
        widget: {
          xtype: 'combobox',
          store: Carmen.user.map.get('layers'),
          queryMode: 'local',
          autoLoadOnValue : true,
          autoSelect: true,
          editable: false,
          displayField: 'layerTitle',
          valueField:   'layerId',
          listeners: {
            change: function(widget, value, oldValue) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
              // do not unset fieldId and fieldName on init (ie. when oldValue is null)
              if( oldValue != null && oldValue != value ) {
                widget.getWidgetRecord().set('locatorCriteriaFieldId', null);
                widget.getWidgetRecord().set('locatorCriteriaFieldName', null);
              }
            }
          }
        }
      }, {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaFieldId'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaFieldId'),
        dataIndex: 'locatorCriteriaFieldId',
        xtype: 'widgetcolumn',
        widget: {
          xtype: 'combobox',
          store: Ext.create('Ext.data.Store', {
            autoLoad: false,
            model: 'Carmen.Model.Field'
          }),
          queryMode: 'local',
          editable: false,
          displayField:'fieldName',
          valueField:  'fieldName',
          listeners: {
            change: function(widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            },
            expand: function(widget, eOpts) {
              // display all criteria except the one of the current row
              var layerId = widget.getWidgetRecord().get('locatorCriteriaLayerId');
              var layerRec = Carmen.user.map.get('layers').getAt(Carmen.user.map.get('layers').find('layerId', layerId));
              if( layerRec ) {
                var data = [];
                layerRec.get('fields').each(function(rec){
                  data.push(rec);
                });
                widget.store.loadRecords(data, false);
              }
            },
            beforerender: function(widget, eOpts) {
              // display all criteria except the one of the current row
              var layerId = widget.getWidgetRecord().get('locatorCriteriaLayerId');
              var layerRec = Carmen.user.map.get('layers').getAt(Carmen.user.map.get('layers').find('layerId', layerId));
              if( layerRec ) {
                var data = [];
                layerRec.get('fields').each(function(rec){
                  data.push(rec);
                });
                widget.store.loadRecords(data, false);
                widget.select(widget.getWidgetRecord().get('locatorCriteriaFieldId'));
              }
            }
          }
        }
      }, {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaFieldName'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaFieldName'),
        dataIndex: 'locatorCriteriaFieldName',
        xtype: 'widgetcolumn',
        widget: {
          xtype: 'combobox',
          store: Ext.create('Ext.data.Store', {
            autoload: false,
            model: 'Carmen.Model.Field'
          }),
          queryMode: 'local',
          editable: false,
          displayField:'fieldName',
          valueField:  'fieldName',
          listeners: {
            change: function(widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            },
            expand: function(widget, eOpts) {
              // display all criteria except the one of the current row
              var layerId = widget.getWidgetRecord().get('locatorCriteriaLayerId');
              var layerRec = Carmen.user.map.get('layers').getAt(Carmen.user.map.get('layers').find('layerId', layerId));
              if( layerRec ) {
                var data = [];
                layerRec.get('fields').each(function(rec){
                  data.push(rec);
                });
                widget.store.loadRecords(data, false);
              }
            },
            beforerender: function(widget, eOpts) {
              // display all criteria except the one of the current row
              var layerId = widget.getWidgetRecord().get('locatorCriteriaLayerId');
              var layerRec = Carmen.user.map.get('layers').getAt(Carmen.user.map.get('layers').find('layerId', layerId));
              if( layerRec ) {
                var data = [];
                layerRec.get('fields').each(function(rec){
                  data.push(rec);
                });
                widget.store.loadRecords(data, false);
                widget.select(widget.getWidgetRecord().get('locatorCriteriaFieldName'));
              }
            }
          }
        }
      }, {
        width : 60,
        align : 'center',
        dataIndex: '_',
        renderer : function(){return 'lié à';},
      }, {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaRelated'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaRelated'),
        dataIndex: 'locatorCriteriaRelated',
        xtype: 'widgetcolumn',
        onWidgetAttach : function(column, widget, record){
          record.related_widgets = record.related_widgets || [];
          record.related_widgets.push(widget);
        },
        widget: {
          xtype: 'combobox',
          store: me.storeRelated,
          queryMode: 'local',
          autoLoadOnValue: true,
          autoSelect: true,
          allowBlank: true,
          editable: false,
          displayField:'locatorCriteriaName',
          valueField:  'locatorCriteriaRank',
          listeners: {
            change: function(widget, value, oldValue) {
              value = Ext.isObject(value) ? value.locatorCriteriaRank : value;
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
              // do not unset RelatedField on init (ie. when oldValue is null)
              if( !Ext.isObject(oldValue) && oldValue != null && oldValue != value ) {
                widget.getWidgetRecord().set('locatorCriteriaRelatedField', null);
              }
            },
            expand: function(widget, eOpts) {
              // display all criteria except the one of the current row
              var myRank = widget.getWidgetRecord().get('locatorCriteriaRank');
              var data = [];
              me.store.each(function(e){
                if( myRank != e.get('locatorCriteriaRank') ) {
                  data.push( e.getData() );
                }
              });
              me.storeRelated.loadData(data, false);
              
            },
            beforerender: function(widget, eOpts) {
              // display all criteria except the one of the current row
              var myRank = widget.getWidgetRecord().get('locatorCriteriaRank');
              var data = [];
              me.store.each(function(e){
                if( myRank != e.get('locatorCriteriaRank') ) {
                  data.push( e.getData() );
                }
              });
              me.storeRelated.loadData(data, false);
              // on init, select existing value if any
              var relatedRank = widget.getWidgetRecord().get('locatorCriteriaRelated');
              if( relatedRank != null ) {
                var rec = me.store.findRecord('locatorCriteriaRank', relatedRank);
                if( rec ) {
                  widget.select(rec.get('locatorCriteriaRank'));
                  widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, rec.get('locatorCriteriaRank'));
                }
              }
            }
          }
        }
      }, {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaRelatedField'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaRelatedField'),
        dataIndex: 'locatorCriteriaRelatedField',
        xtype: 'widgetcolumn',
        onWidgetAttach : function(column, widget, record){
          record.related_widgets = record.related_widgets || [];
          record.related_widgets.push(widget);
        },
        widget: {
          xtype: 'combobox',
          store: Ext.create('Ext.data.Store', {
            autoload: false,
            model: 'Carmen.Model.Field'
          }),
          queryMode: 'local',
          editable: false,
          displayField:'fieldName',
          valueField:  'fieldName',
          listeners: {
            change: function(widget, value) {
              widget.getWidgetRecord().set(widget.getWidgetColumn().config.dataIndex, value);
            },
            expand: function(widget, eOpts) {
              
              var layerId = widget.getWidgetRecord().get('locatorCriteriaLayerId');
              var layerRec = Carmen.user.map.get('layers').getAt(Carmen.user.map.get('layers').find('layerId', layerId));
              if( layerRec ) {
                var data = [];
                layerRec.get('fields').each(function(rec){
                  data.push(rec);
                });
                widget.store.loadRecords(data, false);
              }
            },
            beforerender: function(widget, eOpts) {
              var layerId = widget.getWidgetRecord().get('locatorCriteriaLayerId');
              var layerRec = Carmen.user.map.get('layers').getAt(Carmen.user.map.get('layers').find('layerId', layerId));
              if( layerRec ) {
                var data = [];
                layerRec.get('fields').each(function(rec){
                  data.push(rec);
                });
                widget.store.loadRecords(data, false);
                widget.select(widget.getWidgetRecord().get('locatorCriteriaRelatedField'));
              }
            },
            afterrender: function(widget, eOpts) {
              var checkbox = widget.getWidgetRecord().related_enabled;
              if ( !checkbox ) return;
              checkbox.fireEvent('change', checkbox, checkbox.getValue());
            }
          }
        }
      }, {
        text: _t('Map.forms.ui.extra.locators.locatorCriteriaVisibility'),
        tooltip: _t('Map.forms.ui.extra.locators.locatorCriteriaVisibility'),
        dataIndex: 'locatorCriteriaVisibility',
        width:80,
        maxWidth: 80,
        minWidth: 80,
        xtype: 'hidden'
      }, {
          text: _t('Map.forms.ui.extra.locators.locatorInGlobalMapSearchEngine'),
          tooltip : _t('Map.forms.ui.extra.locators.locatorInGlobalMapSearchEngine'),
          dataIndex: "locatorInGlobalMapSearchEngine",
          width:200,
          maxWidth: 200,
          minWidth: 200,
          xtype : "checkcolumn"
      }, {
        xtype:'actioncolumn',
        align : 'center',
        sortable: false,
        menuDisabled: true,
        width: 30,
        maxWidth: 30,
        minWidth: 30,
        items: [
          {
            icon: '/carmenweb/images/delete.png',  // Use a URL in the icon config
            tooltip: _t('Map.forms.ui.extra.removeCriteria'),
            handler: function(grid, rowIndex, colIndex) {
              // TODO show confirm dialog
              Ext.MessageBox.confirm(_t('form.confirmDeleteTitle'), _t('Map.forms.ui.extra.removeMsg'), function(btn){
                if( btn == 'yes' ) {
                  var rec = grid.getStore().getAt(rowIndex);
                  var recRank = rec.get('locatorCriteriaRank');
                  me.store.remove(rec);
                  // clean other locators that depends on this one
                  if( recRank != null ) {
                    grid.getStore().each(function(record){
                      if( record.get('locatorCriteriaRelated') == recRank ) {
                        record.set('locatorCriteriaRelated', null);
                        record.set('locatorCriteriaRelatedField', null);
                      }
                    });
                  }
                  me.focus();
                  me.getView().focusRow(Math.max(0, rowIndex-1));
                }
              });
            }
          }
        ]
      }]
      }
    }];
    
    this.callParent();
    
    me.grid = this.items.items[0];
    me.store = me.grid.getStore();
  }
});




///////////////////////////////////////////
// MapUI Form
///////////////////////////////////////////
var CMP_NAME = 'Carmen.FormSheet.MapUI';
Ext.define('Carmen.FormSheet.MapUI', {
  extend: 'Carmen.Generic.Form', 
  xtype: CMP_REFERENCE(CMP_NAME),

  title: MAPUI_TRANSLATE('title'),
  
  height: 'auto',
  
  statics: {
    /**
     * Create the MapUIBannerEditor window if it does not already exist, then show it
     */
    editBanner: function(initialValue, cmpFocused){
      window.placeholderKeywords = [['Titre de la carte']];
      var window_class = "Carmen.Window.MapUIEditor";
      var win = Ext.getCmp(CMP_REFERENCE(window_class)) || Ext.create(window_class, {
        datatype : 'bannerfile',
        extension : '.html',
        initialValue : initialValue,
        TRANSLATOR : MAPUIBANNER_TRANSLATE,
        DICTIONARY : Carmen.Dictionaries.BANNERMODELS
      });
      win.show();
    },

    /**
     * Create the MapUIPrintModelEditor window if it does not already exist, then show it
     */
    editPrintModel: function(cmpFocused){
      window.placeholderKeywords = [['Titre'], ['Commentaire'], ['Carte de situation'], ['Carte'], ['Legende'], ['Copyright']];
      var window_class = "Carmen.Window.MapUIPrintModelEditor";
      var win = Ext.getCmp(CMP_REFERENCE(window_class)) || Ext.create(window_class, {
        datatype : 'layoutfile',
        extension : '.tpl',
        //with_label :  true,
        TRANSLATOR : MAPUIPRINT_TRANSLATE,
        DICTIONARY : Carmen.Dictionaries.LAYOUTMODELS
      });
      win.show();
    }, 
    
    /**
     * Compute a new extent based on the given extent and new dimensions
     */
    computeExtent: function(x0m, y0m, x0M, y0M, width, height) {
      return { xmin:x0m, ymin:y0m, xmax:x0M, ymax:y0M };
    }
  },
  
  initComponent : function()
  {
    var me = this;
    
    me.items = [{
      xtype: 'panel',
      title: false,
      layout: 'anchor',
      id: 'form-map-ui',
      defaults: {
        layout: 'form',
        xtype: 'textfield',
        padding: '10 10 10 10',
        labelAlign: 'left',
        labelWidth: 100,
        labelSeparator: ' '
      },
      items:  [].concat(
        me.getUiCopyrightLogo(), 
        me.getUiBanner(), 
        me.getUiLegend(), 
        me.getUiLocalisation(), 
        me.getUiLocalize(), 
        me.getUiPrintModels(),
        me.getUiHelpEditor(),
        me.getUiCguEditor(),
      )
    }];
    
    this.callParent();
  },
  
  getUiCopyrightLogo : function(){
    var me = this;
    return [{
      title: MAPUI_TRANSLATE('extra.apparence'),
      xtype: 'panel',
      frame : true,
      layout: 'anchor',
      margin : '0 0 10 0',
      defaults: {
        layout: 'form',
        xtype: 'textfield',
        labelAlign: 'left',
        labelWidth: 100,
        labelSeparator: ' '
      },
      items: [{
        xtype: 'fieldcontainer',
        fieldLabel: MAPUI_TRANSLATE('fields.uiColorId'), 
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [
        {html : ''},
        {
          xtype: 'combobox', 
          name: 'uiColor',
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.COLORID,
          allowBlank: false,
          editable: false,
          autoLoadOnValue : true,
          autoSelect : true
        }, 
        {html : ''}]
      }]
    }, {
      title: MAPUI_TRANSLATE('fields.uiLogo')+" & "+MAPUI_TRANSLATE('fields.uiCopyright'),
      xtype: 'panel',
      frame : true,
      layout: 'anchor',
      margin : '0 0 10 0',
      defaults: {
        layout: 'form',
        xtype: 'textfield',
        labelAlign: 'left',
        labelWidth: 100,
        labelSeparator: ' '
      },
      items: [{
        xtype: 'fieldcontainer',
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'checkboxfield',
          fieldLabel: MAPUI_TRANSLATE('fields.uiLogo'), 
          name: 'uiLogo',
          
          listeners : {
            change : function(){
              me.down('[name=uiLogopath]').setDisabled(!this.checked);
              me.down('[name=uiLogopath]').allowBlank = (!this.checked);
              me.down('[name=uiLogopath_button]').setDisabled(!this.checked);
            }
          }
        }, {
          xtype: 'textfield',
          //fieldLabel: MAPUI_TRANSLATE('fields.uiLogopath'), 
          name: 'uiLogopath',
          padding: '0 0 0 10',
          flex: 1,
          
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.LOGOPATH_IMAGES,
          editable: false,
          autoLoadOnValue : true,
          autoSelect : true
        }, { 
          xtype: 'button',
          text: MAPUI_TRANSLATE('fields.uiBtBrowse'),
          name : 'uiLogopath_button',
          handler: function(){
            var windowClass = "Ext.ux.FileBrowserWindow";
            var window = Ext.getCmp(CMP_REFERENCE(windowClass)) 
            || Ext.create(windowClass, {
               id: CMP_REFERENCE(windowClass),
               selectExtension: '*.jpg,*.gif,*.png',
               uploadExtension: '.jpg,.gif,.png',
               dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'noroute'}),
               defaultPath: '/Root',
               relativeRoot: '/Root/IHM',
               listeners: {  
                 selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath)  {
                   var ctrlTo = me.down('[name=uiLogopath]');
                   ctrlTo.setValue(fullPathName);
                   wind.close();
                 }
               }
            });
            window.show();
          },
         
          listeners : {
            afterrender : function(){
              var check = me.down('[name=uiLogo]');
              if ( check ){check.fireEvent('change', check);}
            }
          }
        },
        {html : '', flex : 2}
        ]
      }, {
        xtype: 'fieldcontainer',
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'checkboxfield',
          fieldLabel: MAPUI_TRANSLATE('fields.uiCopyright'), 
          name: 'uiCopyright',
          listeners : {
            change : function(){
              me.down('[name=uiCopyrightText]').setDisabled(!this.checked);
              me.down('[name=uiCopyrightText]').allowBlank = (!this.checked);
            }
          }
        }, {
          xtype: 'textfield',
          fieldLabel: MAPUI_TRANSLATE('fields.uiCopyrightText'), 
          name: 'uiCopyrightText',
          padding: '0 0 0 10',
          flex: 10,
          listeners : {
            afterrender : function(){
              var check = me.down('[name=uiCopyright]');
              if ( check ){check.fireEvent('change', check);}
            }
          }
        }]
      }]
    }];
  },
  
  getUiBanner : function(){
    Carmen.Dictionaries.BANNERMODELS.reload();
    var me = this;
    return [{
      title: MAPUI_TRANSLATE('extra.banner'),
      hidden:(VISIBILITY_MAPS_FORMS_UI_EXTRA_BANNER_HABILLAGE ? false : true),
      xtype: 'panel',
      frame : true,
      layout: 'anchor',
      /*margin : '0 0 10 0',*/
      defaults: {
        layout: 'form',
        xtype: 'textfield',
        labelAlign: 'left',
        labelWidth: 100,
        labelSeparator: ' '
      },
      items: [{
        xtype: 'fieldcontainer',
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'checkboxfield',
          fieldLabel: MAPUI_TRANSLATE('fields.uiBanner'),
          name: 'uiBanner',
          listeners : {
            change : function(){
              me.down('[name=uiBannerFile]').setDisabled(!this.checked);
              me.down('[name=bannerModelEdit]').setDisabled(!this.checked);
            }
          }
        }, {
          xtype: 'combobox',
          fieldLabel: MAPUI_TRANSLATE('fields.uiBannerFile'),
          labelAlign: 'right',
          name: 'uiBannerFile',
          id: 'uiBannerFile',
          flex: 2,
          padding: '0 5 0 0',
          queryMode: 'local',
          displayField: 'displayField',
          valueField: 'valueField',
          store: Carmen.Dictionaries.BANNERMODELS,
          editable: false,
          autoLoadOnValue : true,
          autoSelect : true,
          listeners : {
            afterrender : function(){
              var cmp = me.down('[name=uiBanner]');
              if ( cmp ){cmp.fireEvent('change', cmp)};
            }
          }
        }]
      }],
      buttons : [{
        xtype: 'button',
        name : 'bannerModelEdit',
        text: MAPUI_TRANSLATE('extra.bannerModelEdit'),
        handler: function() {
          var combo = me.down('[name=uiBannerFile]');
          Carmen.FormSheet.MapUI.editBanner(combo.getValue(), combo);
        }
      }]
    }];
  },
  getUiLegend : function(){
    var me = this;
    return [{
      title: MAPUI_TRANSLATE('extra.legend'),
      xtype: 'panel',
      frame : true,
      layout: 'anchor',
      /*margin : '0 0 10 0',*/
      defaults: {
        layout: 'form',
        xtype: 'textfield',
        labelAlign: 'left',
        labelWidth: 170,
        labelSeparator: ' '
      },
      items: [{
        xtype: 'fieldcontainer',
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [{
        labelWidth: 150,
          xtype: 'checkboxfield',
          fieldLabel: MAPUI_TRANSLATE('fields.uiLegend'),
          name: 'uiLegend'
        }, {
          labelAlign: 'right',
          labelWidth: 170,
          xtype: 'checkboxfield',
          hidden:(VISIBILITY_MAPS_FORMS_UI_LEGEND_PRINT ? false : true),
          name: 'uiLegendPrint',
          flex : 1,
          padding : '0 0 0 10',
          fieldLabel: MAPUI_TRANSLATE('fields.uiLegendPrint')
        }]
      }]
    }];
  },
  
  getUiLocalisation : function(){
    var me = this;
    return [{
      xtype: 'panel',
      frame: true,
      padding : 10, /*margin : '0 0 10 0',*/
      title: MAPUI_TRANSLATE('extra.localization'),
      
      layout: {
        type: 'hbox'
      },
      defaults: {
        labelSeparator: ' '
      },
      items : [{
        xtype: 'fieldcontainer',
        hidden:(VISIBILITY_MAPS_FORMS_UI_FOCUS ? false : true),
        layout: 'vbox',
        flex: 1,
        defaults: {
          labelSeparator: ' '
        },
        items: [
          {
            xtype: 'checkboxfield',
            fieldLabel: MAPUI_TRANSLATE('fields.uiFocus'), 
            name: 'uiFocus'
          }
        ]
      }, {
        xtype: 'fieldcontainer',
        layout: 'vbox',
        flex: 1,
        defaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'checkboxfield',          
          fieldLabel: MAPUI_TRANSLATE('fields.uiKeymap'), 
          name: 'uiKeymap',
          labelWidth: 160,
          listeners : {
            change : function(){
              me.down('[name=generateKeymap]').setDisabled(!this.checked);
              var refPanel = Ext.getCmp('referencePanel'); 
              if(this.checked){
                 refPanel.setHidden(false);      
              } else {
                  refPanel.setHidden(true);  
              }
            }
          }
        }, {
          xtype: 'button',
          name : 'generateKeymap',
          text: (Ext.valueFrom(VISIBILITY_KEYMAP_LIBRARY, false) ? MAPUI_TRANSLATE('extra.manageKeymap') : MAPUI_TRANSLATE('extra.generateKeymap')),
          listeners : {
            afterrender : function(){
              var cmp = me.down('[name=uiKeymap]');
              if ( cmp ){cmp.fireEvent('change', cmp)};
            }
          },
          handler: function(button) {
          	if ( Ext.valueFrom(VISIBILITY_KEYMAP_LIBRARY, false) ){
          		Ext.create('Carmen.Map.KeymapLibrary', {opener : me}).show();
          		return;
          	}
          	me.generateKeymap(button);
          }
        }]
      }, {xtype : 'panel',
        flex: 1, 
        items : [{
        xtype: 'image',
        itemId: 'referenceMapImage',
        style : 'border:1px solid black',
        width : 200
      }]},
      { html:'<span style="color:red">Veuillez générer la carte de situation.</span>', flex:1 , hidden : true, name: "referenceError"},
      { html:'', flex:1 }
      ]
    }];
  },
  
  getUiLocalize : function(){
    var me = this;
    return [{
      xtype: 'panel',
      frame: true,
      layout: 'vbox',
      name : 'uiLocalize',
      title: MAPUI_TRANSLATE('extra.locators.title'),
      items : [{
        xtype: 'fieldcontainer',
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'checkboxfield',
          labelWidth : 100,
          fieldLabel: MAPUI_TRANSLATE('fields.uiLocate'), 
          name: 'uiLocate',
          listeners : {
            change : function(){var cmp = this.up("[name=uiLocalize]").down('cmnmaplocator'); cmp && cmp.setDisabled(!this.getValue());}
          }
        }]
      }, {
        xtype : 'cmnmaplocator',
        padding : 0,margin:0,
        name: 'mapLocators',
        listeners : {
          afterrender : function(){var cmp = this.up("[name=uiLocalize]").down('[name=uiLocate]'); cmp && cmp.fireEvent('change', cmp);}
        }
      }]
    }];
  },
  
  getUiPrintModels : function(){
    Carmen.Dictionaries.LAYOUTMODELS.reload();
    var me = this;
    return [{
      xtype: 'panel',
      frame: true,
      padding : 10, /*margin : '0 0 10 0',*/
      layout: 'form',
      hidden : (VISIBILITY_MAPS_FORMS_UI_EXTRA_PRINTMODELS ? false : true),
      title: MAPUI_TRANSLATE('extra.printModels'),
      items:[
        {
          xtype: 'tagfield',
          fieldLabel: MAPUI_TRANSLATE('fields.models'),
          name: 'uiModels',
          flex: 5,
          padding: '0 5 0 10',
          store: Carmen.Dictionaries.LAYOUTMODELS,
          displayField: 'displayField',
          valueField: 'valueField',
          filterPickList: true,
          queryMode: 'local',
          filterPicked: function(rec) {
              return this.valueCollection.find(this.valueField, rec.get(this.valueField))===null;
          },
          checkRemovedValues : function(store, records){
            var tagfield = this;
            //resolve problem in tagfield destroy
            if ( !tagfield.valueCollection || !tagfield.valueCollection.items ) return;
            var currents = tagfield.getValueRecords() || [];
            var keep = [];
            store.clearFilter(true);
            Ext.each(currents, function(current){
              if (store.findRecord(tagfield.valueField, current.get(tagfield.valueField))){
                keep.push(current);
              }
            });
            tagfield.setValue(keep);
            tagfield.changingFilters = true;
            store.filter(tagfield.listFilter);
            tagfield.changingFilters = false;
          },
          listeners : {
            afterrender : function(tagfield){
              tagfield.getStore().on('load', tagfield.checkRemovedValues, tagfield)
            },
            beforedestroy : function(tagfield){
              tagfield.getStore().un('load', tagfield.checkRemovedValues);
            }
          }
        }
      ],
      buttons : [{
        xtype: 'button',
        text: MAPUI_TRANSLATE('extra.printModelEdit'),
        handler: function() {
          Carmen.FormSheet.MapUI.editPrintModel(me.down('[name=uiModels]'));
        }
      }]
    }]
  },
  
  getUiHelpEditor : function(){
    return {
    	id: 'help_panel',
        xtype: 'panel',
        frame: true,
        layout: 'vbox',
        padding : 10,
        height: 400,
        title: MAPUI_TRANSLATE('fields.uiHelp'),
        items : [{
              xtype: 'fieldcontainer',
              height: 50,
              layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
              },
              defaults: {
                labelSeparator: ' '
              },
              items: [{
                xtype: 'checkboxfield',
                labelWidth : 300,
                fieldLabel: MAPUI_TRANSLATE('fields.uiDisplay'),
                name: 'helpDisplay'
              }]
          }, {
        	  id: 'help_editor',
              xtype: 'ckeditor',
              disabled : false,
              width : '100%',
              name: 'helpMessage',
              listeners: {
            	  render: function() {
            		  var panel = Ext.getCmp('help_panel');
            		  var editor = Ext.getCmp('help_editor');
            		  
            		  editor.setHeight(panel.getHeight());
            		  panel.doLayout();
            	  }
              }
        }]
      };
  },

  getUiCguEditor : function(){
    return {
      id: 'cgu_panel',
      xtype: 'panel',
      frame: true,
      layout: 'vbox',
      padding : 10,
      height: 400,
      title: MAPUI_TRANSLATE('fields.uiCgu'),
      items : [{
        xtype: 'fieldcontainer',
        height: 50,
        layout: {
          type: 'hbox',
          pack: 'start',
          align: 'stretch'
        },
        defaults: {
          labelSeparator: ' '
        },
        items: [{
          xtype: 'checkboxfield',
          labelWidth : 300,
          fieldLabel: MAPUI_TRANSLATE('fields.uiCguDisplay'),
          name: 'cguDisplay'
        }]
      }, {
        id: 'cgu_editor',
        xtype: 'ckeditor',
        disabled : false,
        width : '100%',
        name: 'cguMessage',
        listeners: {
          render: function() {
            var panel = Ext.getCmp('cgu_panel');
            var editor = Ext.getCmp('cgu_editor');

            editor.setHeight(panel.getHeight());
            panel.doLayout();
          }
        }
      }]
    };
  },
  
  isValid : function(){
    var valid = this.callParent(arguments);
    this.down('[name=referenceError]').hide();
    if ( valid && this.down('[name=uiKeymap]').getValue() && !this.getRecord().get('map').get('reference').get('image') ){
    	this.down('[name=referenceError]').show();
      return false;
    }
    return valid;
  },
  loadRecord : function(record) {
    this.callParent(arguments);
    
    if (record instanceof Carmen.Model.MapUI) {
      var referenceMapImage = this.down('[itemId=referenceMapImage]');
      if( referenceMapImage ) {
        var msReferenceMap = record.get('map').get('reference');
        referenceMapImage.setSrc( msReferenceMap.get('base64image') );
        referenceMapImage.setWidth( msReferenceMap.getWidth() );
        referenceMapImage.setHeight( msReferenceMap.getHeight() );
      }
      var locators = record.get('map').get('locators') || [];
      var cmplocators = this.down('[name=mapLocators]');
      cmplocators && cmplocators.setValue(locators); 
      
      // set help text in ckeditor
      var msg = record.get('helpMessage');
      var editor = Ext.getCmp("help_editor");
      editor.setValue(msg);
      editor.msg = msg;

      // set cgu text in ckeditor
      var cgumsg = record.get('cguMessage');
      var cgueditor = Ext.getCmp("cgu_editor");
      cgueditor.setValue(cgumsg);
      cgueditor.msg = cgumsg;
    }
    
  },
  
  updateRecord : function(record){
  	this.callParent(arguments);
  	var cmplocators = this.down('[name=mapLocators]');
    if ( cmplocators )
      var locators = cmplocators.getValue();
      Ext.each(locators, function(locator, index){locator.locatorCriteriaRank = index});
      record.get('map').set('locators', locators);
  },
  
  _getKeymapAjaxParameters : function(libraryParams){
    var me = this;
    var map = me.getRecord().get('map');
    var coordNames = ['mapExtentXmin', 'mapExtentYmin', 'mapExtentXmax', 'mapExtentYmax'];
    var args = [];
    for(var i=0; i<coordNames.length; i++){
      var cmp = me.up('window').down('[name='+coordNames[i]+']');
      if ( cmp ){
        args.push(cmp.getValue());
      } else {
        args.push(map.get(coordNames[i]));
      }
    }
    args.push(map.get('reference').getWidth());
    args.push(map.get('reference').getHeight());
    var extent = Carmen.FormSheet.MapUI.computeExtent.apply(window, args);
    var hiddens = [];
    var recursive = app.layerTreeStore.getRecursive(); 
    !recursive && app.layerTreeStore.setRecursive(true);
    var store = Ext.create('Ext.data.Store', {data : app.layerTreeStore.retrieveChildNodes(app.layerTreeStore.getRoot()), model : Ext.data.TreeModel})
    map.get('layers').each(function(layer){
    	var rec = store.findRecord('layerId', layer.get('layerId'));
    	if ( !rec ) return;
      var OL_ITEM = app.mapContent.getItemById(rec.data.OL_ITEM.id);
      if ( OL_ITEM && !OL_ITEM.isVisible() ){
        hiddens.push(layer.get('layerMsname'));
      }
    });
    !recursive && app.layerTreeStore.setRecursive(false);
    return {
      'id'     : me.getRecord().get('map').get('mapId'),
      'extent' : extent.xmin + ' ' + extent.ymin + ' ' + extent.xmax + ' ' + extent.ymax,
      'size'   : map.get('reference').getSize(),
      'hiddens' : hiddens,
      'libraryParams' : libraryParams || {}
    };
  },
  
  generateKeymap : function(button, callback, libraryParams){
    var me = this;

    button.setDisabled(true);
    Ext.Ajax.request({
      timeout : 100000,
      method : 'POST',
      url: Routing.generate('carmen_ws_mapui_generate_refmap', me._getKeymapAjaxParameters(libraryParams)),
      success: function(response, opts) {
        me._setReferenceImage(true, response, button, callback);
      },
      failure: function(response, opts) {
        me._setReferenceImage(false, response, button, callback);
      }
    });
  },
  
  _setReferenceImage : function(success, response, button, callback){
    var me = this;
    if ( success ){
     button.setDisabled(false);
     var obj = Ext.decode(response.responseText);
     var referenceMapImage = me.down('[itemId=referenceMapImage]');
     if( referenceMapImage && (obj.data||obj.image)) {
       // update image src
       referenceMapImage.setSrc( obj.data );
       // update data to reflect new image has been generated
       var msReferenceMap = me.getRecord().get('map').get('reference');
       msReferenceMap.set('extent', obj.extent.join(' '));
       msReferenceMap.set('base64image', obj.data);
       msReferenceMap.set('image', obj.image);
       msReferenceMap.set('width', obj.width);
       msReferenceMap.set('height', obj.height);
       // null image will force generating a new image server-side 
     }
     Ext.MessageBox.show({
       title: MAPUI_TRANSLATE('fields.uiKeymap'),
       msg: MAPUI_TRANSLATE('extra.uiKeymapUpdateSuccess'),
       buttons: Ext.MessageBox.OK,
       icon: Ext.MessageBox.INFO
     });
     callback && callback(true);
    } 
    else {
      button.setDisabled(false);
      if (Carmen.notAuthenticatedException(response) ) return;
      Ext.MessageBox.show({
        title: MAPUI_TRANSLATE('fields.uiKeymap'),
        msg: MAPUI_TRANSLATE('extra.uiKeymapUpdateError'),
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.ERROR
      });
      callback && callback(false);
    }
  }
});
