
Ext.define('Carmen.Map.Keymap', {
  extend : 'Ext.data.Model',
  fields: ['filename', 'image', 'base64image', 'description', 'modifiedDate', 'extent']
})
Ext.define('Carmen.Map.KeymapLibrary', {
  extend: 'Ext.window.Window', 
  width: Carmen.Util.WINDOW_WIDTH,
  
  opener : null,
  
  initComponent : function()
  {
    var me = this;
    var mapId = (me.opener ? me.opener.getRecord().get('map').get('mapId') : null);
    //me.height = 600;
    me.padding = '0 8 8 8';
    me.layout = {
    	type : 'table',
    	columns : 2,
    	tableAttrs : {width : '100%'}
    };
    var isAdminProdige = Ext.valueFrom(KEYMAP_LIBRARY_ISADMIN, false);
    
    var callback = function(success){success && me.close()};
    
    var URL_KEYMAP_LIBRARY = 'carmen_ws_mapui_keymaplibrary';
    
    me.title = "Définition de la carte de situation";
    me.items = [{
    	title : 'A partir de la vue courante',
    	xtype : 'fieldset',
    	colspan : (!isAdminProdige ? 2 : 1),
      padding : '5 5 10 10',
      margin : '0 10 0 0',
      items : [{
        xtype : 'button',
        text: 'Générer et sélectionner',
        tooltip: 'Mettre à jour la carte de situation de la carte avec la vue courante',
        tooltipType: 'title',
        handler: function(button){
          me.opener && me.opener.generateKeymap(button, callback);
        }
      }]
  	}, {
      width : '100%',
  		padding : '5 5 10 5',
  		title : "Ajouter dans la bibliothèque une carte de situation générée à partir de la vue courante",
      xtype:'fieldset',
      id: 'formNewReference',
      hidden : !isAdminProdige,
      layout:{type:'table', columns:3},
      items : [{
      	allowBlank : false,
        margin : '0 2 0 5',
        xtype : 'textfield',
        name : 'filename',
        emptyText : 'Nom du fichier...',
        width: 200,
        minLength : 3,
        minLengthText : 'Votre description est trop courte, saisissez une description plus longue (> 3 caractères).',
        maxLength : 40,
        maxLengthText : 'Votre texte excède la taille maximale autorisée (40 caractères).',
        selectOnFocus:true
      }, {
        allowBlank : false,
      	margin : '0 2 0 5',
        xtype : 'textfield',
        name : 'description',
        emptyText : "Description de l'image ...",
        width: 300,
        minLength : 3,
        minLengthText : 'Votre description est trop courte, saisissez une description plus longue (> 3 caractères).',
        maxLength : 40,
        maxLengthText : 'Votre texte excède la taille maximale autorisée (40 caractères).',
        selectOnFocus:true
      }, {
        margin : '0 5 0 2',
        xtype : 'button',
        text: 'Ajouter à la bibliothèque et sélectionner',
        tooltip: 'Enregistrer la vue courante comme nouvelle carte de situation ',
        tooltipType: 'title',
        handler: function(button){
          var libraryParams = {};
          var bContinue = true;
          Ext.each(this.up('fieldset').query('field[name]'), function(cmp){
          	cmp.validate();
            if ((bContinue = bContinue && cmp.isValid())){
              libraryParams[cmp.name] = cmp.getValue();
            }
            return bContinue;
          })
          bContinue && me.opener && me.opener.generateKeymap(button, callback, libraryParams);
        } 
      }]
    },{
    //DATAVIEW
    	colspan : 2,
    	id : 'dview',
    	height : 400, 
    	//frame : true,
    	width : "100%",
    	xtype : 'panel',
      title: 'Choisir une carte de situation dans la bibliothèque',
    	viewModel : {
    	},
    	layout : 'border',
    	border : true,
      region : 'center',
    	items : [{
    		padding: "5 0 5 5",
        xtype: 'dataview',
        autoScroll : true,
        region : 'center',
        store : {
        	storeId : 'KEYMAP_LIBRARY',
          model: 'Carmen.Map.Keymap',
          autoLoad: true,
          proxy: {
            type: 'ajax',
            paramsAsJson : true,
            actionMethods : { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
            url : Routing.generate(URL_KEYMAP_LIBRARY),
            reader: {
              type : 'json',
              rootProperty: ''
            }
          }
        },
        reference: 'keymapsView',
        tpl: [
          '<div style="background-color: white;width: 100%;height: 100%;">',
            '<tpl for=".">',
                '<div class="keymap-item thumb-wrap">',
                    '<img src="{base64image}" class="thumb" />',
                    '<span align="center">{filename}</span>',
                '</div>',
            '</tpl>',
          '</div>'
        ],
        itemSelector: 'div.keymap-item'
    	}, {
        padding: "5 5 5 0",
    		split : true,
    		width : 300,
        xtype: 'form',
        region : 'east',
        labelAlign : 'top',
        defaults : {
          xtype : 'displayfield',
          getDisplayValue : function(){var value = this.getRawValue(); return value || '<i>Aucune sélection</i>';},
          labelAlign : 'top',
          margin : 10
        },
        //Description de la sélection
        items : [{
        	fieldLabel : 'Fichier',
          bind: '{keymapsView.selection.filename}'
        },{
          fieldLabel : 'Description',
          bind: '{keymapsView.selection.description}'
        },{
          fieldLabel : 'Date de modification',
          bind: '{keymapsView.selection.modifiedDate}'
        }],
        //actions
        buttons : [{
        	bind: { disabled : '{!keymapsView.selection}', selection : '{keymapsView.selection}' },
        	setSelection : function(selection){this.selection =  selection;},
          xtype : 'button',
          id: 'ok-btn',
          text : 'Sélectionner',
          handler: function(button){
          	if ( !me.opener ) return;
          	if ( !button.selection ) return;
            button.setDisabled(true);
            Ext.Ajax.request({
              timeout : 100000,
              method : 'POST',
              url: Routing.generate(URL_KEYMAP_LIBRARY+'action', {
              	action : 'select',
                id     : mapId,
                filename : button.selection.get('filename')
              }),
              success: function(response, opts) {
                me.opener._setReferenceImage(true, response, button, callback);
              },
              failure: function(response, opts) {
                me.opener._setReferenceImage(false, response, button, callback);
              }
            });
          },
          scope: this,
          tooltip: 'Mettre à jour la carte de situation de la carte avec la carte de situation sélectionnée'
        }, {
          bind: { disabled : '{!keymapsView.selection}', selection : '{keymapsView.selection}' },
          setSelection : function(selection){this.selection =  selection;},
          hidden : !isAdminProdige,
          xtype : 'button',
          id: 'mod-btn',
          text : 'Modifier',
          handler: function(button){
            if ( !me.opener ) return;
            if ( !button.selection ) return;
            Ext.Msg.confirm( 
              "Modification d'une carte de situation de la bibliothèque", 
              "Voulez vous mettre à jour la carte de situation \""+button.selection.get('filename')+"\" avec la vue courante ?",
              function(btn){
                if(btn == 'yes'){
                  Ext.StoreMgr.lookup('KEYMAP_LIBRARY').reload({url : Routing.generate(URL_KEYMAP_LIBRARY+'action', {
                      action   : 'update',
                      id       : mapId,
                      filename : button.selection.get('filename')
                    }),
                    params : 
                    	me.opener._getKeymapAjaxParameters({
                        filename : button.selection.get('filename'),
                        description : button.selection.get('description')
                      }
                    )
                  }, false);
                }
              }
            );
          },
          scope: this,
          tooltip: 'Modifier une carte de situation avec la vue courante'
        }, {
          bind: { disabled : '{!keymapsView.selection}', selection : '{keymapsView.selection}' },
          setSelection : function(selection){this.selection =  selection;},
          hidden : !isAdminProdige,
          xtype : 'button',
          id: 'del-btn',
          text : 'Supprimer',
          handler: function(button){
            if ( !me.opener ) return;
            if ( !button.selection ) return;
            
            Ext.Msg.confirm( 
              "Suppression d'une carte de situation de la bibliothèque", 
              "Confirmez-vous la suppression la carte de situation \""+button.selection.get('filename')+"\" ?",
              function(btn){
                if(btn == 'yes'){
                	Ext.StoreMgr.lookup('KEYMAP_LIBRARY').reload({url : Routing.generate(URL_KEYMAP_LIBRARY+'action', {
                    action   : 'delete',
                    id       : mapId,
                    filename : button.selection.get('filename')
                  })}, false);
                }
              }
            );
          },
          scope: this,
          tooltip: 'Supprimer une carte de situation'
        }]
      }]
    }]
    
    
//
//  
//  //image chooser panel
//  var oImageChooserPanel = new ImageChooser({
//    url:'../GIMS/popup/spec/vueGlobale/getReferenceImages.php?service=' + interfaceCartoDocument.status.PS_SERVICE_IDX.value,
//    width:550,
//    height:350
//  });
//
//  oImageChooserPanel.show(TSVueGlobale_ValiderStored, TSVueGlobale_ModifierStored, TSVueGlobale_SupprimerStored, isAdminProdige); //TSVueGlobale_ValiderStored = callback function
//  
//  var windowVueGlobale = top.frames["administration"].tabExtPopup[popup_vueglobale_pos];
//  
//  var itemsAdmin = [oBtnUpdate, oPanelSave, oImageChooserPanel.win];
//  var itemsNormal = [oBtnUpdate, oImageChooserPanel.win];
//
//  if (oImageChooserPanel.win){
//  //global panel
//    var oPanel = new Ext.Panel({
//      id: 'vueglobalepanel',
//      layout:'table',
//      layoutConfig: {columns:1},
//      height : windowVueGlobale.getInnerHeight(),
//      frame : true,
//      items: (isAdminProdige ? itemsAdmin : itemsNormal)
//      ,
//      buttonAlign: 'center',
//      buttons: [{
//        text: 'Fermer',
//        handler: function(){//close popup
//          window.frames.carto.TSPopupWindow_Fermer(popup_vueglobale_pos);
//          oPanel=null;
//          oImageChooserPanel=null;
//        } 
//      }]
//    });
//
//    windowVueGlobale.add(oPanel);
//    windowVueGlobale.doLayout();
//  }
//  else{
//    alert('Erreur d\'initialisation');
//  }
    this.callParent();
  }
});
//
//var   popup_vueglobale_pos;//position de la popup dans le tableau tabExtPopup
//var   interfaceCartoDocument = null;
//var   interfaceCartoWindow = null;
//var   ImageChooserPanelMask = null;
//
///**
// * @brief Function called at the opening of the popup
// */
//function  TSVueGlobale_Ouvrir()
//{
//  document.title = "Changer la carte de situation";
//  loading();
//  moveExtPopup(popup_vueglobale_pos);
//}
//
///**
// * @brief Set the reference map (carte de situation) with the current view
// */
//function  TSVueGlobale_ValiderCurrent()
//{
//  //set interfaceCarto
//  SetInterfaceCarto();
//
//  cmd = interfaceCartoDocument.getElementById('TS_REFERENCE_SET');
//  referenceIMG = document.getElementById('reference');
//
//  if (interfaceCartoDocument.status.PS_REFERENCE_SIZE)
//  {
//    if (referenceIMG && (referenceIMG.width) && (referenceIMG.height))
//      interfaceCartoDocument.status.PS_REFERENCE_SIZE.value = referenceIMG.width + "," + referenceIMG.height;
//    else
//      interfaceCartoDocument.status.PS_REFERENCE_SIZE.value = "180,100";
//  }
//  if (cmd)
//  {
//    Ext.MessageBox.buttonText.yes = "Oui";
//    Ext.MessageBox.buttonText.no = "Non";
//    Ext.MessageBox.buttonText.cancel = "Annuler";
//    Ext.Msg.show({ 
//      buttons: Ext.Msg.YESNOCANCEL,
//      title : "Continuer ?", 
//      msg : "Voulez vous mettre à jour la carte de situation de la carte avec la vue courante ?",
//      fn : function(btn){
//        if(btn == 'yes'){
//          //close window
//          cmd.click();
//        }
//      }
//    });
//  }
//}
//
///**
// * @brief Set the reference map (carte de situation) with the choosen reference image
// */
//function  TSVueGlobale_ValiderStored(filename)
//{
//  //set interfaceCarto
//  SetInterfaceCarto();
//  
//  //set parameters
//  interfaceCartoWindow.CSParametre_setReferenceSize("180,100");
//  interfaceCartoWindow.CSParametre_setReferenceName(filename);
//
//  //launch command
//  cmd = interfaceCartoDocument.getElementById('TS_REFERENCE_SETSTORED');
//  cmd.click();
//
//}
//
//
//
///**
// * @brief Store the current view as new reference image 
// * (only availaible for Prodige Admin in Prodige, availaible for all users in Carmen)
// */
//function TSVueGlobale_SaveNew(descText){
//  
//  if (descText.length<3){
//    Ext.Msg.alert("Description", "Le texte de description doit faire entre 3 et 60 caractères maximum.");
//    return;
//  }
//  //set interfaceCarto
//  SetInterfaceCarto();
//  
//  //transform description text
//  //special char => _ , accents => char without accents
//  filename = formatText(descText);
//  //set parameters
//  interfaceCartoWindow.CSParametre_setReferenceSize("180,100");
//  interfaceCartoWindow.CSParametre_setReferenceName(filename);
//  interfaceCartoWindow.CSParametre_setReferenceDesc(descText);
//  
//  //launch command
//  cmd = interfaceCartoDocument.getElementById('TS_REFERENCE_SAVE');
//  if (cmd){
//    //load mask
//    ImageChooserPanelMask = new Ext.LoadMask(Ext.getCmp('img-chooser-dlg').body, {msg:"Mise à jour des images..."});
//    ImageChooserPanelMask.show();
//    cmd.click();
//  }
//}
//
///**
// * @brief Modify a stored reference map with the current map
// */
//function TSVueGlobale_ModifierStored(filename, descText){
//  //set interfaceCarto
//  SetInterfaceCarto();
//  interfaceCartoWindow.CSParametre_setReferenceSize("180,100");
//  //delete .PNG from filename
//  filename = filename.replace(/(.PNG)/gi,"");
//  interfaceCartoWindow.CSParametre_setReferenceName(filename);
//  interfaceCartoWindow.CSParametre_setReferenceDesc(descText);
//  //ask for validation
//  cmd = interfaceCartoDocument.getElementById('TS_REFERENCE_SAVE');
//  if (cmd)
//  {
//    Ext.MessageBox.buttonText.yes = "Oui";
//    Ext.MessageBox.buttonText.no = "Non";
//    Ext.MessageBox.buttonText.cancel = "Annuler";
//    Ext.Msg.show({ 
//      buttons: Ext.Msg.YESNOCANCEL,
//      title : "Continuer ?", 
//      msg : "Voulez vous mettre à jour la carte de situation choisie avec la vue courante ?",
//      fn : function(btn){
//        if(btn == 'yes'){
//          //load mask
//          ImageChooserPanelMask = new Ext.LoadMask(Ext.getCmp('img-chooser-dlg').body, {msg:"Mise à jour des images..."});
//          ImageChooserPanelMask.show();
//          //launch command
//          cmd.click();
//        }
//      }
//    });
//  }
//}
//
///**
// * @brief Delete a stored reference map
// */
//function TSVueGlobale_SupprimerStored(filename){
//  //set interfaceCarto
//  SetInterfaceCarto();
//  interfaceCartoWindow.CSParametre_setReferenceName(filename);
//  //launch command
//  cmd = interfaceCartoDocument.getElementById('TS_REFERENCE_DELSTORED');
//  if (cmd){
//    //load mask
//    ImageChooserPanelMask = new Ext.LoadMask(Ext.getCmp('img-chooser-dlg').body, {msg:"Mise à jour des images..."});
//    ImageChooserPanelMask.show();
//    cmd.click();
//  }
//}
//
///**
// * @brief Replace all special chars with _ and delete the accents
// */
//function formatText(descText){
//  temp = descText.replace(/[àâä]/gi,"a");
//  temp = temp.replace(/[éèêë]/gi,"e");
//  temp = temp.replace(/[îï]/gi,"i");
//  temp = temp.replace(/[ôö]/gi,"o");
//  temp = temp.replace(/[ùûü]/gi,"u");
//  temp = temp.replace(/[\s.]/gi,"_");
//  return temp;
//}
//
//
///**
// * @brief Build extjs window
// */
//function loading(){
//  
//  var isAdminProdige = false;
//  
//  //bug with Ext.QuickTips.init() and IE 7 => buttons not clickable
//  if (navigator.appName!="Microsoft Internet Explorer"){
//    Ext.QuickTips.init();
//  }
//  else{
//    version = parseFloat(navigator.appVersion.split("MSIE")[1]);
//    if (version!=7){
//      Ext.QuickTips.init();
//    }
//    else{
//      alert('Erreur de chargement');
//    }
//  }
//  //set interfaceCarto
//  SetInterfaceCarto();
//
//  if (interfaceCartoWindow.CNParametre_getModeCarmenProdige()=="PRODIGE"){
//    if (interfaceCartoWindow.CNParametre_getIsAdminProdige()){
//      isAdminProdige = eval(interfaceCartoWindow.CNParametre_getIsAdminProdige());
//    }
//  }
//  else if(interfaceCartoWindow.CNParametre_getModeCarmenProdige()=="CARMEN"){
//    isAdminProdige = true;
//  }
//    
//  //buttons
//  var oBtnUpdate = new Ext.Button({
//    text: 'Mettre à jour avec la vue courante',
//    tooltip: 'Mettre à jour la carte de situation de la carte avec la vue courante',
//    tooltipType: 'title',
//    handler: function(){
//      TSVueGlobale_ValiderCurrent();
//    }
//  });
//  var oBtnSave = new Ext.Button({
//    text: 'Enregistrer une nouvelle carte de situation',
//    tooltip: 'Enregistrer la vue courante comme nouvelle carte de situation ',
//    tooltipType: 'title',
//    handler: function(){
//      oTextDesc = Ext.getCmp('oTextDesc');
//      if (oTextDesc){
//        oTextDesc.validate();
//        if (oTextDesc.isValid()){
//          TSVueGlobale_SaveNew(oTextDesc.getValue());
//        }
//        else{
//          Ext.Msg.alert("Description", "Le texte de description doit faire entre 3 et 60 caractères maximum.");
//          return;
//        }
//      }
//    } 
//  });
//  var oTextDesc = new Ext.form.TextField({
//    id : 'oTextDesc',
//    emptyText : 'Entrer une description ...',
//    width: 300,
//    minLength : 3,
//    minLengthText : 'Votre description trop courte, saississez une description plus longue (> 3 caractères).',
//    maxLength : 40,
//    maxLengthText : 'Votre texte excède la taille maximale autorisée (40 caractères).',
//    selectOnFocus:true
//  });
//  
//  //panel save
//  var oPanelSave = new Ext.FormPanel({
//    id: 'oPanelSave',
//    layout:'table',
//    layoutConfig: {columns:2},
//    items : [oTextDesc, oBtnSave]
//  });
//  
//  //image chooser panel
//  var oImageChooserPanel = new ImageChooser({
//    url:'../GIMS/popup/spec/vueGlobale/getReferenceImages.php?service=' + interfaceCartoDocument.status.PS_SERVICE_IDX.value,
//    width:550,
//    height:350
//  });
//
//  oImageChooserPanel.show(TSVueGlobale_ValiderStored, TSVueGlobale_ModifierStored, TSVueGlobale_SupprimerStored, isAdminProdige); //TSVueGlobale_ValiderStored = callback function
//  
//  var windowVueGlobale = top.frames["administration"].tabExtPopup[popup_vueglobale_pos];
//  
//  var itemsAdmin = [oBtnUpdate, oPanelSave, oImageChooserPanel.win];
//  var itemsNormal = [oBtnUpdate, oImageChooserPanel.win];
//
//  if (oImageChooserPanel.win){
//  //global panel
//    var oPanel = new Ext.Panel({
//      id: 'vueglobalepanel',
//      layout:'table',
//      layoutConfig: {columns:1},
//      height : windowVueGlobale.getInnerHeight(),
//      frame : true,
//      items: (isAdminProdige ? itemsAdmin : itemsNormal)
//      ,
//      buttonAlign: 'center',
//      buttons: [{
//        text: 'Fermer',
//        handler: function(){//close popup
//          window.frames.carto.TSPopupWindow_Fermer(popup_vueglobale_pos);
//          oPanel=null;
//          oImageChooserPanel=null;
//        } 
//      }]
//    });
//
//    windowVueGlobale.add(oPanel);
//    windowVueGlobale.doLayout();
//  }
//  else{
//    alert('Erreur d\'initialisation');
//  }
//}
//
///**
// * @brief Set the cartographic window variable : interfaceCartoWindow.
// */
//function SetInterfaceCarto(){
//  //set interfaceCarto
//  if ((window.frames.carto) && (window.frames.carto.document.status)){
//    interfaceCartoDocument = window.frames.carto.document;
//    interfaceCartoWindow = window.frames.carto;
//  }
//  
//  if ((interfaceCartoDocument.status) && (interfaceCartoDocument.status.PN_MAP_FILE.value.length > 0))
//  {
//    //continue
//  }
//  else{
//    Ext.Msg.alert("Erreur", "Pas d'interface cartographique définie.");
//    return;
//  }
//}
