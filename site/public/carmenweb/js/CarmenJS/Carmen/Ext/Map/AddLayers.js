var _T_MAP_ADD = function (key) {
  return _t("Map.MapAdd." + key);
};

Ext.define('Carmen.Window.Map.AddLayers', {
  extend : 'Ext.window.Window',
  width : Carmen.Util.WINDOW_WIDTH,
  minHeight : 300,
  scrollable: true,
  closeAction: 'destroy',
  header: true/*(ADDCONTEXTE ? true : false)*/,
  autoDestroy : true,
  modal : true,
  layout: 'column'/*(ADDCONTEXTE ? 'auto' : 'column')*/,
  title: _T_MAP_ADD('fields.titleWindow'),

  defaults:{
    columnWidth: 1
  },
  fieldDefaults: {
    labelWidth: 125
  },
  listeners: {
    close:function() {
      ADDCONTEXTE = false;
    }
  },
  initComponent : function() {
    var me = this;

    if(!ADDCONTEXTE) {
      me.header = false;
      me.mapStore = new Ext.data.TreeStore({
        storeId : 'MAP',
        proxy: {
          type: 'jsonp',
          reader : {
            type : 'json'
          },
          url: Carmen.parameters.catalogue_url + "/geosource/administrationCartoGetCarte"
        }
      });
      me.mapStore.load();
    } else {
        me.setLayout('auto');
    }

    me.url = Routing.generate('carmen_ws_get_map_layers');

    me.layerStore = new Ext.data.Store({
      storeId: 'layer_id',
      fields:['layerTitle', 'layerId', 'layerMsname', 'active', 'icon'],
      proxy: {
        type: 'jsonp',
        reader : {
          type : 'json'
      },
      url: me.url
     }
    });

    me.mapFileId = "";

    me.ar_layer_ids = [];
    //me.ar_layer_msnames = [];

    me.buttons = [{
      text : _T_MAP_ADD('btn.save'),
      handler : function() {
        if(Carmen.user.map.id && (Ext.ComponentQuery.query('[name=mode]')[0].getGroupValue() == "Add" || Ext.ComponentQuery.query('[name=mode]')[0].getGroupValue() == "Replace") && (me.str_layer_ids != "") && (me.str_layer_msnames != "")) {
          Ext.Ajax.request({
            url: Routing.generate('carmen_ws_add_layers', {
                  'mapToId':Carmen.user.map.id, /*id for current map file : Carmen.prodige.id*/
                  'mapFromId': me.mapFileId, /*id of source mapfile */
                  'mode':Ext.ComponentQuery.query('[name=mode]')[0].getGroupValue()
                 }),
            method: 'POST',
            jsonData: {
              ar_layer_ids: me.ar_layer_ids
            },
            success: function (response) {
              me.close();
              var jsonResponse = Ext.decode(response.responseText);
              Carmen.Model.Map.onSuccessSave(jsonResponse);
            },
            failure: function (response) {
              console.log('error - response is : ', response);
            }
          });
        }
        else {
          Ext.Msg.alert('Attention', 'Le formulaire n\'est pas validé !!!');
        }
      }
    }, {
      text : _T_MAP_ADD('btn.cancel'),
      handler : function(){
        ADDCONTEXTE = false;
        me.close();
      }
    }];

    // items on this panel
    me.items =
      [
       {
         xtype: 'form',
         items: 
           [
            {
              xtype: 'fieldcontainer',
              name: 'fc_uploadContext',
              padding: '15 15 15 15',
              hidden: (ADDCONTEXTE ? false : true),
              defaults: {
                  flex: 1
              },
              layout: 'hbox',
              items: [
                      {
                        xtype: 'filefield',
                        name: 'UploadContextField',
                        fieldLabel: 'Cliquez sur Parcourir pour sélectionner le fichier à transférer',
                        labelWidth: 350,
                        msgTarget: 'side',
                        allowBlank: false,
                        anchor: '100%',
                        buttonText: 'Parcourir'
                      }
                     ]
            },
            {
              xtype: 'fieldcontainer',
              name: 'fc_buttonuploadContext',
              padding: '15 15 15 15',
              hidden: (ADDCONTEXTE ? false : true),
              items:
                [
                 {
                    xtype: 'button',
                    name: 'btnUploadContext',
                    text: 'Ajouter',
                    width: 150,
                    handler : function() {
                      var valid_file_extension = (Ext.ComponentQuery.query('[name=UploadContextField]')[0].getValue().split('.')[1] == 'ows');
                      if(valid_file_extension) {
                        var form = this.up('form').getForm();
                        if(form.isValid()) {
                          //VLC & HIL : Cross domain - file upload - solution trouvé ...
                          var formData = new FormData(); // Create new FormData
                          var uploader = Ext.ComponentQuery.query('[name=UploadContextField]')[0];
                          formData.append("UploadContextField", uploader.getEl().down('input[type=file]').dom.files[0]); // Append the file for upload

                          var xhr = new XMLHttpRequest();
                          if(xhr.upload) {
                            xhr.upload.addEventListener("error", function(e) {
                              console.log('error is : ', e);
                            });
                            xhr.upload.addEventListener("abort", function(e) {
                              console.log('abort is : ', e);
                            });

                            xhr.onreadystatechange = function() {
                              if(xhr.readyState == 4) { //ready
                                if(xhr.status == 200) {  //ok 
                                  var response = Ext.decode(xhr.responseText);
                                  me.mapFileId = response["mapfile_id"];
                                  me.layerStore.load({url:  me.url + "/" + response["mapfile_id"]});

                                 if(Ext.ComponentQuery.query('[name=rd_mode]')[0].isVisible() == false) {
                                   Ext.ComponentQuery.query('[name=rd_mode]')[0].setVisible(true);
                                 }
                                } else {
                                  var res = eval("("+xhr.responseText+")");
                                  var msg = (res && res.msg ? res.msg : "");
                                  console.log('error message is : ', msg);
                                }
                              }
                            }
                          }
                          // envoi du fichier
                          xhr.open('POST', Carmen.parameters.catalogue_url + '/geosource/tSAjoutDeCarte_LayerListController', true);
                          xhr.send(formData);
                        }
                      }
                      else {
                        Ext.Msg.alert('Attention', 'Le fichier à télécharger doit avoir le format ows !');
                      }
                    }
                  }
                 ]
            }
      ]
    }
   ,{
         xtype:'treepanel',
         name:'fsTree',
         title: _T_MAP_ADD('fields.titleMaps'),
         columnWidth: 0.5,
         margin: '0 4 0 0',
         height: 250,
         padding: '0 0 0 0',
         scrollable: true,
         hidden: (ADDCONTEXTE ? true : false),
         store: me.mapStore,
         rootVisible: false,
         selModel: new Ext.selection.TreeModel({
           listeners: {
             selectionchange: function (selection, node) {
               node = node[0]; // un seul noeud sélectionnable
               me.mapFileId = node.data.map_id;
               me.layerStore.load({url:  me.url + "/" + node.data.map_name});

              if(Ext.ComponentQuery.query('[name=rd_mode]')[0].isVisible() == false) {
                Ext.ComponentQuery.query('[name=rd_mode]')[0].setVisible(true);
              }
             }
           }
         })
    },
    {
         xtype:'gridpanel',
         name:'gpanel',
         columnWidth: 0.5,
         title: _T_MAP_ADD('fields.titleLayersForMap'),
         scrollable: true,
         margin: '0 0 0 4',
         height: 250,
         padding: '0 0 0 0',
         scrollable: 'y',
         store: me.layerStore,
         hideHeaders: true,
         columns:
           [
            { dataIndex: 'icon', flex: 1, renderer: function(value) {return '<img src="' + value + '"/>'; } },
            { text: 'layerTitle', dataIndex: 'layerTitle', flex: 9 },
            { xtype: 'checkcolumn', text: 'Active', dataIndex: 'active', flex: 2,
              listeners: {
                checkchange : function(checkBox , rowIndex , checked , eOpts) {
                  //TODO take in account checked and not checked
                  me.ar_layer_ids.push(me.layerStore.getAt(rowIndex).data.layerId);
                  //me.ar_layer_msnames.push(me.layerStore.getAt(rowIndex).data.layerMsname);
                }
              }
            }
           ]
      },
      {
         xtype: 'fieldcontainer',
         name: 'rd_mode',
         defaultType: 'radiofield',
         padding: '15 15 15 15',
         hidden: true,
         defaults: {
             flex: 1
         },
         layout: 'hbox',
         items:
           [
            {
              boxLabel  : 'Remplacer les couches de la carte ',
              name      : 'mode',
              inputValue: 'Replace'
            },
            {
              boxLabel  : 'Ajouter aux couches de la carte ',
              name      : 'mode',
              inputValue: 'Add',
              checked: true
            }
           ]
       }
      ];

    this.callParent(arguments);

  }

});