var _T_EDITOR = function(key){return _t("Map.editor."+key);};
var _T_PUBLISH = function(key){return _t("Map.publish."+key);};

Ext.define('Carmen.Window.Map', {
  extend: 'Carmen.Generic.Window', 
  reference: 'window-map',
  statics : {
    openedWindow : null,
    ensureUnique : function(newWindow)
    {
      if ( Carmen.Window.Map.openedWindow ){
        Carmen.Window.Map.openedWindow.close();
        Carmen.Window.Map.openedWindow = null;
      }
      Carmen.Window.Map.openedWindow = newWindow;
    }
  },

  titleTpl: new Ext.XTemplate(_T_EDITOR('titleTpl')),
  emptyMapTitle : _T_EDITOR('emptyMapTitle'),
  scrollable: true,
  width: 1200, 
  layout: 'fit',
  initComponent : function(){
    Carmen.Window.Map.ensureUnique(this);
    this.items = [
      {
        xtype : 'tabpanel',
        tabRotation: 0,
        items: [
          Ext.create('Carmen.FormSheet.Map')
        ]
      }
    ];
    
    this.callParent(arguments);
    
  }, 
  
  /**
   * Open the map editor to edit an existing record or to create a new map 
   * @param Carmen.Model.Map|null record
   */
  edit : function(recordMap){
    recordMap = recordMap || Ext.create('Carmen.Model.Map');
    
    var mainFrom = this.down(CMP_REFERENCE('Carmen.FormSheet.Map'));
    if ( !mainFrom ) return this;
    mainFrom.loadRecord(recordMap); 
    
    var tabpanel = this.down('tabpanel');
    
    if ( tabpanel && Ext.isNumber(recordMap.getId()) ){
      // Pour l'onglet d'Habillage
      var subForm = Ext.create('Carmen.FormSheet.MapUI');
      var subRecord = recordMap.get('mapUi');
      //console.log(subRecord); 
      subForm.loadRecord(subRecord); 
      tabpanel.add(subForm);

      // A adapter aux outils de prodige
      var subForm = Ext.create('Carmen.FormSheet.MapTools');
      var subRecord = recordMap.get('tools');
      subForm.loadRecord(subRecord); 
      tabpanel.add(subForm);
      
      var subForm = Ext.create('Carmen.Layer.List');
      tabpanel.add(subForm);
      
      /* Gestion des ressources associées */
      var subForm = Ext.create('Carmen.Map.MapOnlineResources'); 
      subRecord = recordMap.get('mapOnlineResources'); 
      subForm.loadRecord(recordMap); 
      tabpanel.add(subForm); 
    }
    
    this.setTitle(
      this.titleTpl.apply({ mapTitle : (recordMap.get("mapTitle") || this.emptyMapTitle) } )
    );
    
    this.callParent([recordMap]);
    return this;
  },
  getSubmittedRecord : function(record){
    var record = (record && !(record instanceof Carmen.Model.Map) ? this.callParent() : this.callParent(record));
    return record;
  },
  postSaveSuccess : function(operation, jsonResponse){
    Carmen.Model.Map.onSuccessSave(jsonResponse);
    if ( operation instanceof Ext.data.operation.Create ){
      Ext.create('Carmen.Window.Map').edit(Carmen.user.map).down('tabpanel').setActiveTab(1);
    }
    this.callParent(arguments);
  }
});

/**
 * Ne sera pas utilisé dans la nouvelle version du prodige
 * prodige 4.0
 */
Ext.define('Carmen.Window.Map.Publish', {
  extend : 'Ext.window.MessageBox',
  width : 400,
  
  
  maskElements : function(mask){
    Carmen.application.ui.maskTrees(mask);
    Ext.each(Ext.ComponentQuery.query('toolbar > component:not([name$=Trees]):not(tbspacer):not(tbfill):not([cls~=not-active])'), function(cmp){cmp.setDisabled(mask)});
  },
  
  doSave : function(withThumbnails){
    var me = this;
    
    var saveLegendTrees = true;
    me.maskElements(true);
    var callbackSuccess = function(response){
      me.close();
      response = Ext.decode(response.responseText);
      var publish_id = response.map.publishedId;
      delete response.map.publishedId;
      var publishedMap = Carmen.user.map.get('publishedMap');
      if ( !publishedMap ){
        publishedMap = Ext.create('Carmen.Model.Map', {mapId : publish_id}); 
      } else if (!publishedMap.$className ){
        publishedMap.mapId = publish_id;
      } else {
        publishedMap.set('mapId', publish_id, {silent:true,commit:true});
      }
      response.map.publishedMap = publishedMap;
      Carmen.user.map.set(response.map, {silent:true,commit:true});
      Carmen.application.ui.setCurrentMap(Carmen.user.map);
    }
  , callbackFailure = function(response){
      me.close();
    }
  , moreParams = {
      publishMap : {
        edit_id    : Carmen.user.map.getPublishedId()
      , publish_id : Carmen.user.map.get('mapId')
      , POST : {
          save_thumbnails : withThumbnails
        }
      }
    }
    if ( saveLegendTrees ){
      Carmen.application.ui.saveAllTrees(callbackSuccess, callbackFailure, moreParams);
    }
    else {
      Carmen.application.ui.saveTreePrint(callbackSuccess, callbackFailure, moreParams);
    }
      
  },
  show : function(){
    var me = this;
    
    this.on('close', function(){ me.maskElements(false); });
    this.callParent([{
      buttonText : {yes:  _T_PUBLISH('btn.save_with_thumbnails'), no:  _T_PUBLISH('btn.save_without_thumbnails'), cancel:  _T_PUBLISH('btn.cancel')},
      title : _T_PUBLISH('title'),
      message : _T_PUBLISH('fields.message'),
      buttons: Ext.Msg.YESNOCANCEL,
      fn : function(btn){
      	if ( btn=="cancel" ) return;
      	
      	me.doSave(btn=="yes");
      }
    }]);
  }
});