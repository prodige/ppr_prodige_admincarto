
/**
 * Translate text classified by prefix (fixed) and key
 * @param string key
 * @return string
 */

var MSDIFFUSION_TRANSLATE = function(key) {
	  var root = "Map.forms.msdiffusion.";
	  return (key!==null ? _t( root + key ) : root);
	};


/**
 * @form Carmen.FormSheet.MapTools
 */
var CMP_NAME = 'Carmen.FormSheet.MapDiffusion';
Ext.define('Carmen.FormSheet.MapDiffusion', {
  extend: 'Carmen.Generic.Form', 
  xtype: CMP_REFERENCE(CMP_NAME),
  autoScroll : true,

  title: MSDIFFUSION_TRANSLATE('title'),
  
  height : 600,
  layout : 'anchor',
  initComponent : function() {
    var me = this;
    
    var items_wms = [
      {
        xtype : 'displayfield',
        name: 'wmsUrl',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.url'),
        labelWidth: 160,
        margin : '0',
        padding : '0',
        flex : 1,
        value : MSDIFFUSION_TRANSLATE('extra.notactivated')
      },
      {
        xtype : 'displayfield',
        name: 'wmsMetadatainfo',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.metadatainfo'),
        labelWidth: 160,
        margin : '0',
        padding : '0',
        value : MSDIFFUSION_TRANSLATE('extra.notactivated'),
        flex : 1
      }
    ];
    var items_wfs = [
      {
        xtype : 'displayfield',
        name: 'wfsUrl',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.url'),
        labelWidth: 160,
        margin : '0',
        padding : '0',
        flex : 1,
        value : MSDIFFUSION_TRANSLATE('extra.notactivated')
      },
      {
        xtype : 'displayfield',
        name: 'wfsMetadatainfo',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.metadatainfo'),
        labelWidth: 160,
        margin : '0',
        padding : '0',
        value : MSDIFFUSION_TRANSLATE('extra.notactivated'),
        flex : 1
      }
    ];
    var items_atom = [
      {
        xtype : 'displayfield',
        name: 'atomUrl',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.url'),
        labelWidth: 160,
        margin : '0',
        padding : '0',
        flex : 1,
        value : MSDIFFUSION_TRANSLATE('extra.notactivated')
      },
      {
        xtype : 'displayfield',
        name: 'atomMetadatainfo',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.metadatainfo'),
        labelWidth: 160,
        margin : '0',
        padding : '0',
        value : MSDIFFUSION_TRANSLATE('extra.notactivated'),
        flex : 1
      }
    ];
    
    
    var defaultsContact = {
      xtype : 'textfield',
      labelAlign : 'left',
      labelWidth : 160,
      // width: 275,
      labelSeparator : ''
    };
    var flexTotal = 12, flexHalf = 5.5;
    
    var contactFields = {
      msContactorganization : {
      name : 'msContactorganization',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.organization')
      },
      msContactperson : {
      name : 'msContactperson',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.name')
      },
      msContactposition : {
      name : 'msContactposition',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.position')
      },
      msAddress : {
      name : 'msAddress',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.address')
      },    
      msPostcode : {
        colspan : 1,
      name : 'msPostcode',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.zipcode')
      },
      msCity : {
        colspan : 1,
      name : 'msCity',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.city')   
      },
      msStateorprovince : {
        colspan : 1,
      name : 'msStateorprovince',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.unit')
      },
      msCountry : {
        colspan : 1,
      name : 'msCountry',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.country')
      },
      msContactelectronicmailaddress : {
        colspan : 1,
        labelAlign : 'right',
      name : 'msContactelectronicmailaddress',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.email'),
      vtype:'email'
      },
      msContactvoicetelephone : {
        colspan : 1,
        labelAlign : 'right',
      name : 'msContactvoicetelephone',	
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.phone')     
      },
      msContactfacsimiletelephone : {
        colspan : 1,
        labelAlign : 'right',
      name : 'msContactfacsimiletelephone',
      fieldLabel: MSDIFFUSION_TRANSLATE('fields.fax')
      }
    };
    

    var button_loadcontactdata = Ext.create('Ext.Button', {
      text : MSDIFFUSION_TRANSLATE('buttons.loadsaveddata'),
      margin : '0 0 0 0',
      handler: function() {
        var mapping = {
          'msContactorganization'          : 'contactOrganization',
          'msContactperson'                : 'contactName',
          'msContactposition'              : 'contactPosition',
          'msAddress'                      : 'contactAddress',
          'msPostcode'                     : 'contactZipcode',
          'msCity'                         : 'contactCity',
          'msStateorprovince'              : 'contactUnit',
          'msCountry'                      : 'contactCountry',
          'msContactelectronicmailaddress' : 'contactEmail',
          'msContactvoicetelephone'        : 'contactPhone',
          'msContactfacsimiletelephone'    : 'contactFax'       
        };
        Ext.iterate(mapping, function(destination, origin){
          var cmp = me.down('[name='+destination+']');
          if ( cmp ){
            cmp.setValue(Carmen.user.contact.data[origin]);
          }
        })
      }
    });
    
    var fieldContainer2Columns = function(field1, field2){
      field1 = Ext.apply({flex:flexHalf}, field1);
      if ( (flexTotal - field1.flex) && !field2 ){
      	field2 = {xtype:'panel', html:''};
      }
      if ( field2 )
        field2 = Ext.apply({flex:flexHalf}, field2);
      var remainder = 0;
      var items = [field1];
      if ( (remainder = (flexTotal - field1.flex - (field2 ? field2.flex : 0))) ){
        items.push({xtype:'panel', html : '', flex : remainder});
      }
      if ( field2 )
        items.push(field2)
      return {
        xtype : 'fieldcontainer',
        layout : 'hbox',
        defaults : defaultsContact,
        items : items
          }
    }
    
    me.defaults = {padding : 10, margin : '0 0 10 0', defaults : {labelWidth : 160}};
    me.items = [{
    /* URLPanel */
      xtype : 'panel', frame : true,
      name : 'URLPanel',
      title : MSDIFFUSION_TRANSLATE('panels.url'),
      items : [{                 
        xtype : 'fieldcontainer',
        name : 'WFSURLfield',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.wms'),
            layout : {
              type : 'vbox'
            },
            flex : 11,
            items : items_wms
      }, {                 
        xtype : 'fieldcontainer',
        name : 'WFSURLfield',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.wfs'),
            layout : {
              type : 'vbox'
            },            
            flex : 11,
            items : items_wfs
      }, {                  
          xtype : 'fieldcontainer',
          name : 'ATOMURLfield',
        fieldLabel : MSDIFFUSION_TRANSLATE('fields.atom'),
              layout : {
                type : 'vbox'
              },            
              flex : 11,
              items : items_atom
      }]

           }, {
    /* InfoPanel */
      xtype : 'panel', frame : true,
      name : 'InfoPanel',
      title : MSDIFFUSION_TRANSLATE('panels.info'),
      items : [{ 
             xtype : 'textfield',
             name : 'msAbstract',
        width : '100%',
             fieldLabel : MSDIFFUSION_TRANSLATE('fields.summary'),
        flex : 12
           }, {
             xtype : 'combobox',
             name : 'msLanguages',
             fieldLabel : MSDIFFUSION_TRANSLATE('fields.language'),
        flex : 12,
             store : Carmen.Dictionaries.LANGUAGES,
             displayField: 'displayField',
             valueField: 'valueField',
             queryMode: 'local',
             editable: false
           }, {
             xtype : 'combobox',
             store : Carmen.Dictionaries.CONDITION,
             name: 'msFees',
             displayField: 'displayField',
             valueField: 'valueField',
             queryMode : 'local',
        editable: false,
             fieldLabel : MSDIFFUSION_TRANSLATE('fields.usagelimit'),
        width : 400,
        flex : 12,
        listeners : {
          change : function(){
            this.up().up().down('[name=msFeesOther]').setVisible(this.getValue()==Carmen.Dictionaries.CONDITION.AUTRES_ID)
          }
           }
           }, {
        xtype : 'textfield',
        name: 'msFeesOther',
        width : '100%',
        flex : 12,
        listeners : {
          afterrender : function(){
            var cmp = this.up().up().down('[name=msFees]');
            cmp && cmp.fireEvent('change', cmp);
           }
        }
      }]
    }, {
    /* SrsCommonsPanel */            
      xtype : 'panel', frame : true,
      name : 'SrsCommonsPanel',
      title : MSDIFFUSION_TRANSLATE('panels.srscommons'),
      items : [{
        filterPickList: false, 
        xtype: 'tagfieldprojections',
         name : 'msGeoide_wxs_srs',
         width : '100%',
        hideLabel: true
       }]
           }, {
    /* ContactPanel */ 
      xtype : 'panel', frame : true,
      name : 'ContactPanel',
      title : MSDIFFUSION_TRANSLATE('panels.contact'),
         width : '100%',
      layout : {
        type : 'table',
        columns : 2,
        tableAttrs : {width : '100%', style : {'table-layout' : 'fixed'}},
        
        tdAttrs : {style : {'vertical-align':'middle !important'}}
      }, labelWidth : 160,
      defaults : {colspan : 2, width : '100%', labelWidth : 160},
      defaultType : 'textfield',
           items: [
        contactFields.msContactorganization,
        contactFields.msContactperson,
        contactFields.msContactposition,
        contactFields.msAddress,
        contactFields.msPostcode,       {colspan : 1,xtype : 'fieldcontainer'},
        contactFields.msCity,            contactFields.msContactelectronicmailaddress,
        contactFields.msStateorprovince, contactFields.msContactvoicetelephone,
        contactFields.msCountry,         contactFields.msContactfacsimiletelephone
      ], 
      bbar : {
          defaultButtonUI : 'default', items : [button_loadcontactdata]
      }
     }
   ];
   this.callParent();
  },

  loadRecord : function(record) {
    this.callParent(arguments);
    
    if (record instanceof Carmen.Model.msMetadataMap) {
      
      var accountGeonetworkurl = Carmen.user.account.data.accountGeonetworkurl;
      var accountDns = Carmen.user.account.data.accountDns.data;
      var dnsId = accountDns.dnsId;
      var dnsScheme = accountDns.dnsScheme;
      var dnsPrefixData = accountDns.dnsPrefixData;
      var dnsPrefixMetadata = accountDns.dnsPrefixMetadata;
      var dnsUrl = accountDns.dnsUrl;
      
      var accountId = Carmen.user.account.id;
      var mapName = Carmen.user.map.data.mapFile;
      
      var uuidWfs  = Carmen.user.map.get('mapWfsmetadataUuid');
      var uuidWms  = Carmen.user.map.get('mapWmsmetadataUuid');
      var uuidAtom = Carmen.user.map.get('mapAtommetadataUuid');
      
      var wms_url           = dnsScheme + '://' + dnsPrefixData + '.' + dnsUrl + '/WMS/'  + accountId  + '/' + mapName + '?';
      var wfs_url           = dnsScheme + '://' + dnsPrefixData + '.' + dnsUrl + '/WFS/'  + accountId  + '/' + mapName + '?';
      var atom_url          = dnsScheme + '://' + dnsPrefixData + '.' + dnsUrl + '/ATOM/' + accountId + '/' + mapName + '?';
      var wms_metadatainfo  = accountGeonetworkurl + '/fre/find?uuid=' + uuidWms;
      var wfs_metadatainfo  = accountGeonetworkurl + '/fre/find?uuid=' + uuidWfs;
      var atom_metadatainfo = accountGeonetworkurl + '/fre/find?uuid=' + uuidAtom;
      
      var tpl = new Ext.XTemplate("<a href='{0}' target='_blank'>{0}</a>");
      
      if( Carmen.user.map.get('has_layer_wms') ) {
        this.query('[name=wmsUrl]')[0].setValue(tpl.apply([wms_url]));
        this.query('[name=wmsMetadatainfo]')[0].setValue(tpl.apply([wms_metadatainfo]));
      }
      if( Carmen.user.map.get('has_layer_wfs') ) {
        this.query('[name=wfsUrl]')[0].setValue(tpl.apply([wfs_url]));
        this.query('[name=wfsMetadatainfo]')[0].setValue(tpl.apply([wfs_metadatainfo]));
      }
      if( Carmen.user.map.get('has_layer_atom') ) {
        this.query('[name=atomUrl]')[0].setValue(tpl.apply([atom_url]));
        this.query('[name=atomMetadatainfo]')[0].setValue(tpl.apply([atom_metadatainfo]));
      }
    }
  }

});