/**

 */
Ext.define('Ext.button.Selector', {

    /* Begin Definitions */

    alias: 'widget.selector',

    extend: 'Ext.button.Split',
    alternateClassName: 'Ext.SelectorButton',

    /* End Definitions */

    /**
     * @cfg {Object[]} items
     * An array of {@link Ext.menu.CheckItem} **config** objects to be used when creating the button's menu items (e.g.,
     * `{text:'Foo', iconCls:'foo-icon'}`)
     * 
     * @deprecated 4.0 Use the {@link #cfg-menu} config instead. All menu items will be created as
     * {@link Ext.menu.CheckItem CheckItems}.
     */
    /**
     * @cfg {Boolean} [showText=false]
     * True to display the active item's text as the button text. The Button will show its
     * configured {@link #text} if this config is omitted.
     */
    /**
     * @cfg {String} [prependText='']
     * A static string to prepend before the active item's text when displayed as the button's text (only applies when
     * showText = true).
     */
    /**
     * @cfg {Function} changeHandler
     * A callback function that will be invoked each time the active menu item in the button's menu has changed. If this
     * callback is not supplied, the SplitButton will instead fire the {@link #change} event on active item change. The
     * changeHandler function will be called with the following argument list: (SplitButton this, Ext.menu.CheckItem
     * item)
     */
    /**
     * @cfg {String} forceIcon
     * A css class which sets an image to be used as the static icon for this button. This icon will always be displayed
     * regardless of which item is selected in the dropdown list. This overrides the default behavior of changing the
     * button's icon to match the selected item's icon on change.
     */

    /**
     * @cfg {Number/String} forceGlyph
     * The charCode to be used as the static icon for this button.  This icon will always be
     * displayed regardless of which item is selected in the dropdown list. This override
     * the default behavior of changing the button's icon to match the selected item's icon
     * on change. This property expects a format consistent with that of {@link #glyph}
     */
    /**
     * @property {Ext.menu.Menu} menu
     * The {@link Ext.menu.Menu Menu} object used to display the {@link Ext.menu.CheckItem CheckItems} representing the
     * available choices.
     */


    /**
     * @event change
     * Fires after the button's active menu item has changed. Note that if a {@link #changeHandler} function is
     * set on this CycleButton, it will be called instead on active item change and this change event will not
     * be fired.
     * @param {Ext.button.Cycle} this
     * @param {Ext.menu.CheckItem} item The menu item that was selected
     */
    
    fromItem : false, //used to prevent event cycling in btn/btnItem communication
    
    // @private
    getButtonText: function(item) {
        var me = this,
            text = '';

        if (item && me.showText === true) {
            if (me.prependText) {
                text += me.prependText;
            }
            text += item.text;
            return text;
        }
        return me.text;
    },

    /**
     * Sets the button's active menu item.
     * @param {Ext.menu.CheckItem} item The item to activate
     * @param {Boolean} [suppressEvent=false] True to prevent the button's change event from firing.
     */

    setActiveItem: function(item, suppressEvent) {
        var me = this;

        if (!Ext.isObject(item)) {
            item = me.menu.getComponent(item);
        }

        if (item) {
            if (!me.rendered) {
                me.text = me.getButtonText(item);
                me.iconCls = item.iconCls;
                me.glyph = item.glyph;
            } else {
                me.setText(me.getButtonText(item));
                me.setIconCls(item.iconCls);
                me.setGlyph(item.glyph);
            }
            me.activeItem = item;

            if (me.forceIcon) {
                me.setIconCls(me.forceIcon);
            }
            if (me.forceGlyph) {
                me.setGlyph(me.forceGlyph);
            }
            if (!suppressEvent) {
                me.fireEvent('change', me, item);
            }
        }
    },

    /**
     * Gets the currently active menu item.
     * @return {Ext.menu.CheckItem} The active item
     */

    getActiveItem: function() {
        return this.activeItem;
    },

    changeHandler : function()  {
//      console.log("changeHandler");
      this.selector.setActiveItem(this.item);
      this.selector.hideMenu();
      this.item.pressed = true; 
      this.selector.fromItem = true; 
      this.selector.toggle(true); 
      return false;
    },
    // @private
    initComponent: function() {
        var me      = this,
            items,
            i, iLen, item;



        // Allow them to specify a menu config which is a standard Button config.
        // Remove direct use of "items" in 5.0.
        items = (me.menu.items || []).concat(me.items || []);
        me.menu = Ext.applyIf({
          id : 'menu-'+this.id,
          items: []
        }, me.menu);

        iLen = items.length;
        me.on('toggle', 
          function(btn, pressed, eopts) {
            if (this.activeItem ){
//              console.log("toggle active item de l'outil : "+me.tooltip);
//              console.log("toggle active item"+this.activeItem.tooltip);
              this.activeItem.toggle(pressed);
            }
            /*if(pressed)
              this.activeItem =btn;*/
            this.fromItem = false;
          }, 
        this,
        { target : this });
       
        // Convert all items to CheckItems
        for (i = 0; i < iLen; i++) {
            item = items[i];
            item = Ext.applyIf({
                group        : me.id,
                itemIndex    : i,
                scope        : me
                //enableToggle : true,
                //toggleGroup  : me.id + '-toggleGroup'
            }, item);
            item.on('click', me.changeHandler, {selector: this, item : item});
            me.menu.items.push(item);
        }

        me.itemCount = me.menu.items.length;
        me.callParent(arguments);
        
        me.menu.on('add',
          function(menu, item, index, eOpts) {
            //console.log('item ', item);
            var _me = this;
            item = Ext.apply(item, {
                group        : _me.id,
                itemIndex    : index,
                scope        : _me
                //enableToggle : true,
                //toggleGroup  : me.id + '-toggleGroup'
            });
            item.on('click', _me.changeHandler, {selector: _me, item : item});
            
            //item.on('toggle', function() { console.log(this.text + 'toggled...');}, item);
          },  
          this);
        
        me.setActiveItem(0, me);
    },

// @private
    onClick : function(e) {
      
        var me = this;
        me.doPreventDefault(e);
        if (!me.disabled) {
            if (me.isWithinTrigger(e)) {
                // Force prevent default here, if we click on the arrow part
                // we want to trigger the menu, not any link if we have it
                e.preventDefault();
                me.maybeShowMenu();
                //console.log('arrowclick');
                me.fireEvent("arrowclick", me, e);
                if (me.arrowHandler) {
                    me.arrowHandler.call(me.scope || me, me, e);
                }
            } else {
                //me.doToggle();
              //ici clic sur l'activeItem
                if ( !me.pressed ) me.toggle();
               // me.fireHandler(e);
                /*
                if (me.activeItem) {
                  if (me.activeItem.hasListener('click')) 
                    me.activeItem.fireEvent("click", me.activeItem, e);
                  if (me.activeItem.hasListener('toggle')) 
                    me.activeItem.fireEvent("toggle", me.activeItem, e);
                
                }*/ 
            }
        }
    },
    
     /**
     * @protected
     * Returns true if the passed event's x/y coordinates are within the trigger region
     * @param {Ext.event.Event} e
     */
    isWithinTrigger: function(e) {
        var me = this,
            el = me.el,
            overPosition, triggerRegion;

        overPosition = (me.arrowAlign === 'right' || me.arrowAlign === 'left') ?  Math.abs(e.getX() - me.getX()) : Math.abs(e.getY() - el.getY());
        triggerRegion = me.getTriggerRegion();
        return true;overPosition > triggerRegion.begin && overPosition < triggerRegion.end;
    },

    /**
     * @private
     * Returns an object containing `begin` and `end` properties that indicate the
     * left/right bounds of a right trigger or the top/bottom bounds of a bottom trigger.
     * @return {Object}
     */
    getTriggerRegion: function() {
        var me = this,
            region = me._triggerRegion,
            isRight = me.arrowAlign === 'right',
            isLeft = me.arrowAlign === 'left',
            getEnd = isRight ? 'getRight' : ( isLeft ? 'getLeft' : 'getBottom'),
            btnSize = (isRight || isLeft) ? me.getWidth() : me.getHeight();
        
        
        region.begin = isLeft ? 0 : (btnSize - (me.el[getEnd]() - me.btnEl[getEnd]()));
        region.end = isLeft ? 
          10 : 
          btnSize + 8;
        //region = { 'begin': 0, 'end': 10 };
        return region;
    }


});