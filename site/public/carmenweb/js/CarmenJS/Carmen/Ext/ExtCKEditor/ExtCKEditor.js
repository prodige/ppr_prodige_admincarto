var _T_PLACEHOLDER = function(key){
  var root = 'editor.placeholder.';
  return (_t(root + key)==(root + key) ? key : _t(root + key));
}
if ( typeof window.CKEDITOR != "undefined" ){
  CKEDITOR.on('instanceCreated', function(){
    CKEDITOR.plugins.get('placeholder').beforeInit = function(){
      CKEDITOR.lang.fr.placeholder.toolbar = _T_PLACEHOLDER('toolbar');
    };
    
  });
  window.placeholderKeywords = null;
  CKEDITOR.dialog._.dialogDefinitions['placeholder'] = function( editor ) {
    var generalLabel = _T_PLACEHOLDER('title'),
      validNameRegex = /^[^\[\]<>]+$/;
    var keywords = window.placeholderKeywords; 
  
    return {
      title: generalLabel,
      minWidth: 300,
      minHeight: 80,
      contents: [
        {
          id: 'info',
          label: generalLabel,
          title: generalLabel,
          elements: [
            // Dialog window UI elements.
            {
              id: 'name',
              type: (keywords ? 'select' : 'text'),
              items : keywords,
              style: 'width: 100%;',
              width: '100%',
              label: _T_PLACEHOLDER('name'),
              'default': '',
              required: true,
              validate: CKEDITOR.dialog.validate.regex( validNameRegex, _T_PLACEHOLDER('invalidName') ),
              setup: function( widget ) {
                this.setValue( widget.data.name );
              },
              commit: function( widget ) {
                widget.setData( 'name', this.getValue() );
              }
            }
          ]
        }
      ]
    };
  };
}
Ext.define('Ext.ux.CKeditor', ( typeof window.CKEDITOR == "undefined" 
? {
  extend: 'Ext.form.field.HtmlEditor',
  alias: 'widget.ckeditor'
}
: {
  extend: 'Ext.Panel',
  alias: 'widget.ckeditor',

  defaultListenerScope: true,
  
  listeners: {
    instanceReady: 'instanceReady',
    resize: 'resize',
    boxready : 'onBoxReady'
  },

  heightDelta : 30,
  editorId: null,
  
  editor:null,
  editorReady : false,
  CKConfig: {
    "extraPlugins" : 'imagebrowser',
    "imageBrowser_listUrl" : "not_null",
    resize_enabled : false
  },

  constructor: function () {
      this.callParent(arguments);
  },
  initComponent: function () {
    var me = this;
    me.items = [{xtype:'textarea', width:this.width, height : this.height}];
      this.callParent(arguments);
      this.on("afterrender", function () {
          Ext.apply(this.CKConfig, {
              language : (i18n ? i18n.lng() : 'fr'),
              height: this.getEl().getHeight(true)-this.heightDelta,
              width: '100%'
          });
          Ext.WindowManager.getNextZSeed();
          
          this.editor = CKEDITOR.replace(this.down('textarea').inputEl.id, this.CKConfig);
          this.editorId = this.down('textarea').inputEl.id;
          this.editor.name = this.down('textarea').name;

          this.editor.on("instanceReady", function (ev) {
              this.fireEvent(
                  "instanceReady",
                  this,
                  this.editor
              );
          }, this);
          

      }, this);
  },
  instanceReady : function (ev) {
      // Set read only to false to avoid issue when created into or as a child of a disabled component.
      ev.editor.setReadOnly(false);
      this.editorReady = true; 
  },
  onRender: function (ct, position) {
      this.callParent(arguments);
  },

  setValue: function (value) {
      this.down('textarea').setValue(value);
      if (this.editor) {
          this.editor.setData(value);
      }
  },

  getValue: function () {
      if (this.editor) {
          return this.editor.getData();
      }
      else {
          return ''
      }
  },

  destroy: function(){
      // delete instance
      if(!Ext.isEmpty(CKEDITOR.instances[this.editorId])){
          delete CKEDITOR.instances[this.editorId];
      }
      this.callParent(arguments);
  },

  resize: function(win,width, height,opt) {
  	if ( !this.editorReady ) return;
        var eid = this.editorId,
            editor = CKEDITOR.instances[this.editorId];     
        if (!Ext.isEmpty(CKEDITOR.instances[this.editorId])){
            CKEDITOR.instances[this.editorId].resize(width-2, this.getEl().getHeight(true)-this.heightDelta, false, false);
        }       
  },
  onBoxReady : function(win, width, height, eOpts){
      // used to hook into the resize method
  },
    
  statics : {
    dialogDefinition : function(component){
      return Ext.ux.CKeditor.lastDialogDefinition = function (evt) {
        var extckeditor = Ext.ComponentQuery.query('ckeditor');
        if ( !extckeditor.length ) return;
        extckeditor = extckeditor[0];
        var definition = evt.data.definition,
          element, b, w;
        definition.dialog.getId = function(){return definition.dialog.getElement().$.id}
        definition.dialog.isVisible = function(){return definition.dialog.parts.dialog.isVisible()}
        definition.dialog.on('show', function(){
          this.a = this.a || new Ext.Component({el :Ext.get(definition.dialog.getElement().$).prev('.cke_dialog_background_cover'), rendered : true});
          Ext.WindowManager.register(this.a);
          this.a.toFront(false);
          //
          this.b = this.b || new Ext.Component({el :Ext.get(definition.dialog.getElement().$).first(), rendered : true});
          Ext.WindowManager.register(this.b);
          this.b.toFront(false);
        });
        definition.dialog.on('dialogHide', function(){
          Ext.WindowManager.unregister(this.a);
          this.a.destroy();
          delete this.a;
          //
          Ext.WindowManager.unregister(this.b);
          this.b.destroy();
          delete this.b;
        });
        
        // Associate filebrowser to elements with 'filebrowser' attribute.
        for ( var i = 0; i < definition.contents.length; ++i ) {
          if ( ( element = definition.contents[ i ] ) ) {
            extckeditor.dialog = definition.dialog;
            extckeditor.attachFileBrowser( evt.editor, evt.data.name, definition, element.elements );
            if ( element.hidden && element.filebrowser )
              element.hidden = false 
  
          }
        }
      }
    }
  },
  browseServer : function (element){
  	var me = this;
    var window = Ext.create("Ext.ux.FileBrowserWindow", {
      constraintTo : Ext.WindowManager.getActive( ),
      closeAction : 'destroy',
      selectExtension: '*.jpg,*.gif,*.png',
      uploadExtension: '.jpg,.gif,.png',
      dataUrl: Routing.generate('carmen_ws_filebrowser', {routing:'absolute'}),
      defaultPath: '/Root',
      relativeRoot: '/Root',
      listeners: {  
        selectedfile: function(wind, fileRecord, fileName, fullPathName, relativePath, absolutePath)  {
          me.dialog.setValueOf('info', element.id, fileRecord.get('route') );
          window.close();
        }
      }
    });
    window.show();
  },
  attachFileBrowser : function ( editor, dialogName, definition, elements ) {
    var me = this;
    if ( !elements || !elements.length )
      return;

    var element;

    for ( var i = elements.length; i--; ) {
      element = elements[ i ];

      if ( element.type == 'hbox' || element.type == 'vbox' || element.type == 'fieldset' )
        me.attachFileBrowser( editor, dialogName, definition, element.children );

      if ( !element.filebrowser )
        continue;

      if ( typeof element.filebrowser == 'string' ) {
        var fb = {
          action: ( element.type == 'fileButton' ) ? 'QuickUpload' : 'Browse',
          target: element.filebrowser
        };
        element.filebrowser = fb;
      }

      if ( element.filebrowser.action == 'Browse' ) {
        element.onClick = function(){me.browseServer(element)};
        element.hidden = false;
      }
    }
  }
}));

Ext.onReady(function(){
  if ( typeof window.CKEDITOR != "undefined" ){
    if ( Ext.ux.CKeditor.lastDialogDefinition ){
      CKEDITOR.removeListener( 'dialogDefinition', Ext.ux.CKeditor.lastDialogDefinition );
    }
    CKEDITOR.config.extraAllowedContent = 'div(*)';
    
    CKEDITOR.config.contentsCss = [ '/vendor/bootstrap_4.1.3.min.css', CKEDITOR.config.contentsCss];
    CKEDITOR.on("dialogDefinition", Ext.ux.CKeditor.dialogDefinition(), this);
  }
})