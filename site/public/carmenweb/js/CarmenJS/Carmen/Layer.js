
/**
 * @_requires OpenLayers/Layer.js
 */

/**
 * Class: Carmen.Layer
 
 * Inherits from:
 *  - <OpenLayers.Layer>
 */
 
Carmen.Layer = OpenLayers.Util.extend(Carmen.Layer, OpenLayers.Layer);
Carmen.Layer.CLASS_NAME = "Carmen.Layer";
Carmen.Layer.LEGEND_GRAPHIC_HEIGHT = 10;
Carmen.Layer.LEGEND_GRAPHIC_WIDTH = 20;

