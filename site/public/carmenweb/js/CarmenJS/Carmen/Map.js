
/**
 * @_requires OpenLayers/Map.js
 */

/**
 * Class: Carmen.Map
 
 * Inherits from:
 *  - <OpenLayers.Map>
 */

Carmen.Map = new OpenLayers.Class(OpenLayers.Map, {

  app: null,

  layerTreeManager: null,
  labelLayer: null,
  legend: null,
  toolTipControl: null,

  selectionLayers: [],

  CARMEN_EVENT_TYPES: ["addSelection", "changeSelection", "emptySelection"],

  initialize: function (options) {
    OpenLayers.Util.extend(this.EVENT_TYPES, this.CARMEN_EVENT_TYPES);
    OpenLayers.Map.prototype.initialize.apply(this, arguments);

    this.events.register("move", this, function (e) {
      this.hideControls();
      return true;
    });
  },


  /**
   * LayerManager and layers functions
   */

  getLayerTreeManager: function () {
    if (this.layertreeManager == null) {
      var controls = this.getControlsByClass('Carmen.Control.LayerTreeManager');
      this.layerTreeManager = controls.length == 0 ? null : controls[0];
    }
    return this.layerTreeManager;
  },

  getLegend: function () {
    if (this.legend == null) {
      var legendControls = this.getControlsByClass("Carmen.Control.Legend");
      if (legendControls.length > 0)
        this.legend = legendControls[0];
    }
    return this.legend;
  },

  // clear any selection made on layer
  clearSelection: function () {
    // at the moment, only mapserverGroup layer support feature selection
    for (var i = 0; i < this.selectionLayers.length; i++) {
      layer = this.selectionLayers[i];
      layer.clearSelection();
      layer.setDisplayMode(Carmen.Layer.MapServerGroup.DISPLAY_MAP);
    }
    this.removeAllSelectionLayers();
  },

  // clear any selection made on layer
  clearGraphicalSelection: function () {
    // at the moment, only mapserverGroup layer support feature selection
    for (var i = 0; i < this.selectionLayers.length; i++) {
      layer = this.selectionLayers[i];
      layer.setDisplayMode(Carmen.Layer.MapServerGroup.DISPLAY_MAP);
    }
  },

  /**
   * APIMethod: addLayer
   *
   * Parameters:
   * layer - {<OpenLayers.Layer>} 
   */
  // slightly modified to handle layer with fixedPosition     
  addLayer: function (layer, idx) {

    for (var i = 0, len = this.layers.length; i < len; i++) {
      if (this.layers[i] == layer) {
        var msg = OpenLayers.i18n('layerAlreadyAdded',
          { 'layerName': layer.name });
        //OpenLayers.warn(msg);
        return false;
      }
      else {
        var pos = layer.fixedPosition;
        if (pos != null && pos < this.layers.length) {
          if (this.layers[pos].fixedPosition != null && this.layers[pos].fixedPosition == pos) {
            OpenLayers.Console.warn("Trying to add a layer at a fixed position already taken");
            return false;
          }
        }
      }
    }

    this.events.triggerEvent("preaddlayer", { layer: layer });

    layer.div.className = "olLayerDiv";
    layer.div.style.overflow = "";

    this.setLayerZIndex(layer, this.layers.length);

    if (layer.isFixed) {
      this.viewPortDiv.appendChild(layer.div);
    } else {
      this.layerContainerDiv.appendChild(layer.div);
    }

    this.layers.push(layer);

    layer.setMap(this);

    if (layer.isBaseLayer) {
      if (this.baseLayer == null) {
        // set the first baselayer we add as the baselayer
        this.setBaseLayer(layer);
      } else {
        layer.setVisibility(false);
      }
    } else {
      layer.redraw();
    }

    this.events.triggerEvent("addlayer", { layer: layer });
    idx = layer.fixedPosition != null ? this.layers.length - 1 - layer.fixedPosition : idx;

    if (idx != null) {
      this.setLayerIndex(layer, idx)
    }

    // reorganizing layers wrt fixed positions
    for (var i = this.layers.length - 1; i >= 0; i--) {
      var pos = this.layers[i].fixedPosition;
      if (pos != null) {
        var stackPos = this.layers.length - 1 - pos;
        if (stackPos != this.getLayerIndex(this.layers[i])) {
          this.setLayerIndex(this.layers[i], stackPos);
        }
      }
    }
    for (var i = this.layers.length - 1; i >= 0; i--) {
      var pos = this.layers[i].fixedPosition;
    }
  },


  /********************************************************/
  /*                                                      */
  /*                 Control Functions                    */
  /*                                                      */
  /*     The following functions deal with adding and     */
  /*        removing Controls to and from the Map         */
  /*                                                      */
  /********************************************************/

  /**
   * APIMethod: addControl
   * 
   * Parameters:
   * control - {<OpenLayers.Control>}
   * px - {<OpenLayers.Pixel>}
   */
  addControl: function (control, px) {
    this.controls.push(control);
    this.addControlToMap(control, px);
  },

  /**
   * Method: addControlToMap
   * 
   * Parameters:
   * 
   * control - {<OpenLayers.Control>}
   * px - {<OpenLayers.Pixel>}
   */
  addControlToMap: function (control, px) {

    if (control.CLASS_NAME.indexOf("OpenLayers") > -1) {
      OpenLayers.Map.prototype.addControlToMap.apply(this, arguments);
    }
    else
      control.setMap(this);

    // If the map has a displayProjection, and the control doesn't, set 
    // the display projection.
    if (this.displayProjection && !control.displayProjection) {
      control.displayProjection = this.displayProjection;
    }

    control.draw();
  },

  /**
   * APIMethod: getControl
   * 
   * Parameters:
   * id - {String} ID of the control to return.
   * 
   * Returns:
   * {<OpenLayers.Control>} The control from the map's list of controls 
   *                        which has a matching 'id'. If none found, 
   *                        returns null.
   */
  getControl: function (id) {
    var returnControl = null;
    for (var i = 0, len = this.controls.length; i < len; i++) {
      var control = this.controls[i];
      if (control.id == id) {
        returnControl = control;
        break;
      }
    }
    return returnControl;
  },

  /** 
   * APIMethod: removeControl
   * Remove a control from the map. Removes the control both from the map 
   *     object's internal array of controls, as well as from the map's 
   *     viewPort (assuming the control was not added outsideViewport)
   * 
   * Parameters:
   * control - {<OpenLayers.Control>} The control to remove.
   */
  removeControl: function (control) {
    //make sure control is non-null and actually part of our map
    if ((control) && (control == this.getControl(control.id))) {
      if (control.div && (control.div.parentNode == this.viewPortDiv)) {
        this.viewPortDiv.removeChild(control.div);
      }
      OpenLayers.Util.removeItem(this.controls, control);
    }
  },


  /**
   * Zoom extends
   */

  zoomToSuitableExtent: function (bounds, minScale, maxScale) {
    var center = bounds.getCenterLonLat();
    var zoom = this.getZoomForExtent(bounds);
    if (minScale && maxScale) {
      var resolution = this.getResolutionForZoom(zoom);
      var scale = OpenLayers.Util.getScaleFromResolution(resolution, this.units);
      var newScale = scale < minScale ? minScale :
        (scale > maxScale ? maxScale : scale);
      if (newScale != scale) {
        resolution = OpenLayers.Util.getResolutionFromScale(newScale, this.units);
        zoom = this.getZoomForResolution(resolution);
      }
    } else if (minScale) {
      var resolution = this.getResolutionForZoom(zoom);
      var scale = OpenLayers.Util.getScaleFromResolution(resolution, this.units);
      var newScale = scale < minScale ? minScale : scale;
      if (newScale != scale) {
        resolution = OpenLayers.Util.getResolutionFromScale(newScale, this.units);
        zoom = this.getZoomForResolution(resolution);
      }
    }
    this.setCenter(center, zoom);
  },



  getUniqueLayerName: function (prefix) {
    var pre = (prefix && prefix.length > 0) ? prefix : 'layer';
    var i = layers.length;
    var n = pre + i;
    while (this.getLayersByName(n).length > 0) {
      i++;
      n = pre + i;
    }
    return n;
  },

  setLabelLayer: function (layer) {
    this.labelLayer = layer;
  },

  getLabelLayer: function () {
    return this.labelLayer;
  },

  /****************************************************************/
  /*  ToolTips                                                    */
  /****************************************************************/

  _getTooltipControl: function () {
    if (this.toolTipControl == null) {
      var toolTipControls = this.getControlsByClass("Carmen.Control.ToolTip");
      if (toolTipControls.length > 0)
        this.toolTipControl = toolTipControls[0];
    }
    return this.toolTipControl;
  },

  toolTipActivate: function () {
    this._getTooltipControl();
    if (this.toolTipControl)
      this.toolTipControl.activate();
  },

  toolTipDeactivate: function () {
    this._getTooltipControl();
    if (this.toolTipControl)
      this.toolTipControl.deactivate();
  },

  /***************************************************************/
  /*  Selection                                                  */
  /***************************************************************/
  addToSelectionLayers: function (l) {
    Carmen.Util.set_add(this.selectionLayers, l);
    this.events.triggerEvent('addSelection',
      {
        layers: this.selectionLayers
      });
  },

  removeAllSelectionLayers: function () {
    this.selectionLayers = [];
    this.events.triggerEvent('emptySelection');
  },

  getSelectionLayers: function () {
    return this.selectionLayers;
  },

  hideControls: function () {
    for (var i in this.controls) {
      if (typeof this.controls[i].hide == "function")
        this.controls[i].hide();
    }
  },

  CLASS_NAME: "Carmen.Map"
});
