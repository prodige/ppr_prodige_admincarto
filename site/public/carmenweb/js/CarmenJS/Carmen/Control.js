
/**
 * @_requires OpenLayers/Control.js
 */

/**
 * Class: Carmen.Control
 
 * Inherits from:
 *  - <OpenLayers.Control>
 */
 
var ctrlWidth = 30, ctrlHeight = 30;
 
Carmen.Control = {}

Carmen.Control.wrapper = function(olControl, carmenControlName, btnConfig) {
  return new OpenLayers.Class(olControl, {

    btnConfig : (btnConfig!=null) ? btnConfig : 
      {
        tooltip: 'Button',
        tooltipType: 'title',
        icon: '/IHM/images/defaultTool.gif',
        cls: 'x-btn-icon',
        enableToggle: true,
        toggleGroup: 'mainMapControl'
      },

    btn : Ext.create('Ext.button.Button', btnConfig),
  
    initialize: function(options) {
      olControl.prototype.initialize.apply(this, arguments);
      olControl.options = options;
      if (this.btn.initialConfig.enableToggle)
        this.btn.addListener('toggle', Carmen.Util.buildExt2olHandlerToggle(this));
      else
        this.btn.addListener('click', Carmen.Util.buildExt2olHandlerButton(this));   
      // btn are disabled until they are attached to a map
      this.btn.disable();
    },
    
    setMap: function(map) {
      olControl.prototype.setMap.apply(this, arguments);
      this.btn.enable();
    },
    
    getButton: function() {
      return this.btn;
    },
    
    CLASS_NAME: "Carmen.Control." + carmenControlName,
    displayClass: ("CmnControl" + carmenControlName).replace(/\./g, ""),
    out : (btnConfig.out !=null) ? true : false 
  })
};

Carmen.Control.ZoomIn = Carmen.Control.wrapper(OpenLayers.Control.ZoomBox, 
  "ZoomIn",
  {
    xtype : 'button',
    tooltip: 'Zoom avant',
    tooltipType: 'qtip',
    cls: 'x-btn-icon',
    enableToggle: true,
    toggleGroup: 'mainMapControl',
    text: '<i class="fa fa-search-plus fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  });

Carmen.Control.ZoomOut = Carmen.Control.wrapper(OpenLayers.Control.ZoomBox, 
  "ZoomOut",
  {
    xtype : 'button',
    tooltip: 'Zoom arrière',
    tooltipType: 'qtip',
    cls: 'x-btn-icon',
    enableToggle: true,
    toggleGroup: 'mainMapControl',
    out : true,
    text: '<i class="fa fa-search-minus fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  });

Carmen.Control.DragPan = Carmen.Control.wrapper(OpenLayers.Control.DragPan, 
  "DragPan",
  {
    xtype : 'button',
    tooltip: 'Déplacement',      
    tooltipType: 'qtip',
    cls: 'x-btn-icon',
    enableToggle: true,
    toggleGroup: 'mainMapControl',
    text: '<i class="fa falk-hand fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  });

Carmen.Control.ZoomFitAll = Carmen.Control.wrapper(OpenLayers.Control.ZoomToMaxExtent, 
  "ZoomFitAll",
  {
    xtype : 'button',
    tooltip: 'Zoom global',      
    tooltipType: 'qtip',
    cls: 'x-btn-icon',
    enableToggle: false,
    text: '<i class="fa fa-globe fa-2x"></i>',
    width: ctrlWidth,
    height: ctrlHeight
  });

Carmen.Control.Help = Carmen.Control.wrapper(
  OpenLayers.Class(OpenLayers.Control,
    {
      trigger: function () { Carmen.Util.openWindow('Help', 'http://carmen.ecologie.gouv.fr'); }
            
    }), 
    "Help",
    {
      tooltip: 'Aide',      
      tooltipType: 'title',
      cls: 'x-btn-icon',/* cmn-tool-help',*/
      enableToggle: false,
      text: '<i class="fa fa-support fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
    });

Carmen.Control.DataDownload = Carmen.Control.wrapper( 
  OpenLayers.Class(OpenLayers.Control,
  {
    trigger: Carmen.Util.openDownloadData       
  }),
  "DataDownload",
  {
      tooltip: 'Téléchargement',
      tooltipType: 'title',
      cls: 'x-btn-icon', /* cmn-tool-dnload',*/
      enableToggle: false,
      disabled: false,
      text: '<i class="fa fa-download fa-2x"></i>',
      width: ctrlWidth,
      height: ctrlHeight
    });
             
