/**
 * @_requires OpenLayers/Basetypes/Class.js
 * @_requires Carmen/Map.js
 */


Ext.define('cmn.layerTreeModel', {
  extend: 'Ext.data.TreeModel',
  fields: [{ name: 'text', mapping: 'name' }
    , { name: 'disabled', type: 'bool', defaultValue: false }
  ],


  // new functions
  clone: function (transform) {
    var clone = this.copy(Ext.id());
    if (this.attributes)
      clone.attributes = this.attributes;
    if (transform)
      clone = transform.apply(this, [clone]);
    for (var i = 0; i < this.childNodes.length; i++) {
      clone.appendChild(this.childNodes[i].clone(transform));
    }
    return clone;
  },

  // filters the current and its child acording to the test function
  // return true if the current node has been removed, else in other cases
  filter: function (test) {
    var res = false;
    var i = 0;
    while (i < this.childNodes.length) {
      var child = this.childNodes[i];
      if (!child.filter(test))
        i++;
    }

    if (!test(this) && this.getDepth() > 0) {
      res = true;
      this.remove();
    }

    return res;
  },

  collect: function (attributeName, onlyLeaf) {
    var collected = new Array();

    if ((onlyLeaf && this.isLeaf()) || !onlyLeaf) {
      if (attributeName in this)
        collected.push(this[attributeName]);
      else if (attributeName in this.attributes)
        collected.push(this.attributes[attributeName]);
    }
    for (var i = 0; i < this.childNodes.length; i++) {
      var childCollected = this.childNodes[i].collect(attributeName, onlyLeaf);
      collected = collected.concat(childCollected);
    }

    return collected;
  },

  collectChildNodes: function (test) {
    var collected = new Array();
    for (var i = 0; i < this.childNodes.length; i++) {
      if (test(this.childNodes[i]))
        collected.push(this.childNodes[i]);
      collected = collected.concat(this.childNodes[i].collectChildNodes(test));
    }
    return collected;
  },

  findChildByAttribute: function (att, value) {
    var res = null;
    if (this.hasChildNodes()) {
      var i = 0;
      while (i < this.childNodes.length && res == null) {
        var child = this.childNodes[i];
        if (att in child && child[att] == value)
          res = child;
        else if (att in child.attributes && child.attributes[att] == value)
          res = child;
        else
          res = child.findChildByAttribute(att, value);
        i++;
      }
      return res;
    }
  },

  updateIndex: function (applyToChildNodes) {
    this.attributes.index = this.parentNode == null ? 0 :
      this.parentNode.indexOf(this);
    if (applyToChildNodes) {
      for (var i = 0; i < this.childNodes.length; i++) {
        this.childNodes[i].updateIndex(applyToChildNodes);
      }
    }
  }
});


Carmen.Application = new OpenLayers.Class({

  ui: null,

  map: null,

  context: null,

  initialize: function (jsonContext) {
    console.log('initialize');
    // CARMEN specific parameters
    // set custom resolution
    OpenLayers.DOTS_PER_INCH = Carmen.DOTS_PER_INCH;
    // switch disgusting pink onImageLoadError in IE to transparent
    OpenLayers.Util.onImageLoadErrorColor = "transparent";

    // custom vtypes...
    Ext.apply(Ext.form.field.VTypes, {

      //  vtype validation function
      owsContext: function (val, field) {
        return /^.*\.(ows)$/.test(val);
      },
      // vtype Text property to display error Text
      // when the validation function returns false
      owsContextText: 'Seuls les fichiers de contexte ows sont autorisés',
      // vtype Mask property for keystroke filter mask
      fileMask: /[a-z_\.]/i

    });


    if (jsonContext != null) {
      this.context = new Carmen.JSONContext(jsonContext);
    }

    if (this.context != null) {
      var displayNavPanel = this.context.isToolActiv('referenceMap') ||
        this.context.isToolActiv('zoomToPlace') ||
        this.context.isToolActiv('legend');

      this.ui = new Carmen.ApplicationUI(this.context,
        { displayNavPanel: displayNavPanel, app: this }
      );
      this.loadContext(this.context);
      // set initial extent
      this.zoomToInitialExtent();
    }
  },

  initMap: function (config) {
    //obedel migration 4 -> 5
    var id_mapDiv = this.ui.getMapDiv().id;
    this.map = new Descartes.Map.ContinuousScalesMap(this.ui.getMapDiv().id, this.mapContent, config);
    this.map.app = this;
    return this.map;
  },

  getMap: function () {
    if (this.map == null) {
      this.initMap([]);
    }
    return this.map;
  },

  zoomToInitialExtent: function () {
    var initialExtent = this.context.getExtent();
    var adjusted = false;
    // looking for contextual parameters in URL
    var urlParams = Ext.urlDecode(window.location.href);
    var extent = Ext.valueFrom(urlParams.extent, null, false);
    var object = Ext.valueFrom(urlParams.object, null, false);
    if (extent == null && object != null) {
      layername = object.split(';')[0];
      var layerDesc = this.context.getLayerDescFromMdataValue('LAYER_TITLE', layername);
      if (layerDesc != null) {
        var minScale = Ext.valueFrom(layerDesc.MinScaleDenominator, null, false);
        var maxScale = Ext.valueFrom(layerDesc.MaxScaleDenominator, null, false);
        if (minScale != null && minScale > 0.0 && maxScale != null && maxScale > 0.0) {
          adjusted = true;
          this.map.zoomToSuitableExtent(initialExtent, minScale, maxScale);
        }
      }
    }
    if (!adjusted) {
      this.map.zoomToExtent(initialExtent);
    }

  },

  loadContext: function (jsOws) {
    //alert("application 4 1");
    console.log('loadContext ', arguments)
    var ctx = this.context;

    var mapOptions = {
      maxExtent: ctx.getMaxExtent(),
      projection: ctx.getProjection(),
      maxResolution: 'auto',
      fractionalZoom: false,
      numZoomLevels: 128,
      allOverlays: true,
      units: ctx.getProjectionUnits(),
      resolutions: Carmen.Util.MAP_RESOLUTIONS,
      controls: []
    };

    // checking for Tile Cache (aka WMS-C layers)
    // if tile cache --> predefined zoom levels
    // so chganging default mapOptions
    if (ctx.hasLayerWMSC()) {
      mapOptions.fractionalZoom = false;
      mapOptions.numZoomLevels = null;
      mapOptions.maxResolution = null;
      mapOptions.resolutions = Carmen.Util.WMSC_RESOLUTIONS;
    }

    // retrieving map min/max scale from context
    var map_minScaleDenom = ctx.getMinScaleDenom();
    var map_maxScaleDenom = ctx.getMaxScaleDenom();

    // filtering resolutions with respect to map min/max scale options
    if (map_minScaleDenom != null && map_maxScaleDenom != null) {
      if (mapOptions.resolutions != null) {
        var res_filtered = [];
        for (var i = 0; i < mapOptions.resolutions.length; i++) {
          var sc = OpenLayers.Util.getScaleFromResolution(mapOptions.resolutions[i], "m").toFixed();
          if (sc >= map_minScaleDenom && sc <= map_maxScaleDenom) {
            res_filtered.push(mapOptions.resolutions[i]);
          }
        }
        mapOptions.resolutions = res_filtered;
        mapOptions.numZoomLevels = mapOptions.resolutions.length;
        mapOptions.minScale = map_minScaleDenom;
        mapOptions.maxScale = map_maxScaleDenom;
        mapOptions.maxResolution = mapOptions.resolutions[0];
        mapOptions.minResolution = mapOptions.resolutions[mapOptions.resolutions.length - 1];
      }
      else {
        mapOptions.minScale = map_minScaleDenom;
        mapOptions.maxScale = map_maxScaleDenom;
      }
    }

    this.initMap(mapOptions);

    // setting document title
    var title = ctx.getTitle();

    title = 'Carte demo BackOffice 2015';
    //this.ui.setMapTitle(title);
    this.ui.map = this.map;
    document.title = title;

    // adding toolbar tools
    var map = this.map;
    map.addControl(new OpenLayers.Control.Navigation({
      'zoomWheelEnabled': true,
      dragPanOptions: {
        enableKinetic: true
      }
    }));

    if (ctx.isToolActiv('fitall'))
      map.addControl(new Carmen.Control.ZoomFitAll({
        toolbarTarget: ctx.getToolbarTarget('fitall'),
        toolbarPos: 0
      }));

    if (ctx.isToolActiv('zoom')) {
      map.addControl(
        new Carmen.Control.ZoomIn({
          toolbarTarget: ctx.getToolbarTarget('zoom'),
          toolbarPos: 0
        })
      );
      map.addControl(
        new Carmen.Control.ZoomOut({
          toolbarTarget: ctx.getToolbarTarget('zoom'),
          toolbarPos: 1
        })
      );
      map.addControl(
        new Carmen.Control.ZoomHistory({
          displayConfig: Carmen.Control.ZoomHistory.PREV_BTN,
          toolbarTarget: ctx.getToolbarTarget('zoom'),
          toolbarPos: 2
        })
      );
    }
    if (ctx.isToolActiv('pan')) {
      map.addControl(
        new Carmen.Control.DragPan({
          toolbarTarget: ctx.getToolbarTarget('pan'),
          toolbarPos: 3
        })
      );
    }

    if (ctx.isToolActiv('buffer')) {
      map.addControl(new Carmen.Control.Buffer(
        window.CARMEN_URL_SERVER_FRONT + '/services/Buffer/index.php',
        window.mapfile));
    }





    if (ctx.isToolActiv('measure')) {
      map.addControl(new Carmen.Control.Measure());
    }
    // not used anymore

    var controlScaleChooser = null;
    if (ctx.isToolActiv('scale')) {
      controlScaleChooser = new Carmen.Control.ScaleChooser();
      map.addControl(controlScaleChooser);
    }

    if (ctx.isToolActiv('geobookmark')) {
      geoBookmarkConfig = ctx.getFavoriteAreas();
      map.addControl(new Carmen.Control.FavoriteAreas(geoBookmarkConfig));
    }

    if (ctx.isToolActiv('context')) {
      map.addControl(new Carmen.Control.ContextManager(
        window.CARMEN_URL_SERVER_FRONT + '/services/SaveContext/index.php',
        window.mapfile,
        {
          toolbarTarget: ctx.getToolbarTarget('context'),
          toolbarPos: 5
        }
      ));
    }
    if (ctx.isToolActiv('annotation')) {
      var annotationConfig = ctx.getGeneralConfig('Annotation');
      var labelConfig = ctx.getGeneralConfig('Label');
      var annotationOptions = {
        bufferOptions: {
          serviceUrl: window.CARMEN_URL_SERVER_FRONT + '/services/Buffer/index.php',
          mapfile: window.mapfile
        }
      };
      config = annotationConfig && labelConfig ?
        {
          Annotation: annotationConfig,
          Label: labelConfig
        } : null;

      Annotations = new Carmen.Control.AdvancedAnnotation(config, annotationOptions);
      map.addControl(Annotations);
    }

    if (window.AreaExtractorHandlerURL && window.AreaExtractorHandlerCallback) {
      var areaExtractor = new Carmen.Control.AreaExtractor({
        activeAtStartup: true,
        processResponseURL: AreaExtractorHandlerURL,
        processResponseCallback: AreaExtractorHandlerCallback,
        sizeLimit: window.AreaExtractorSizeLimit ? window.AreaExtractorSizeLimit : null,
        rectOnly: window.AreaExtractorRectOnly ? window.AreaExtractorRectOnly : false
      });
      map.addControl(areaExtractor);
      this.inAreaExtractor = true;
    }

    if (ctx.isToolActiv('queryAttributes')) {
      map.addControl(new Carmen.Control.AdvancedQueryAttributes(
        window.CARMEN_URL_SERVER_FRONT + '/services/AdvancedQueryAttributes/index.php',
        window.mapfile,
        {
          toolbarTarget: ctx.getToolbarTarget('queryAttributes'),
          toolbarPos: 4
        }

      ));
    }
    if (Carmen.Util.FORCE_SPATIAL_QUERY_TOOL || ctx.isToolActiv('querySpatial')) {
      map.addControl(new Carmen.Control.QuerySpatial(
        window.CARMEN_URL_SERVER_FRONT + '/services/GetInformation/index.php',
        window.mapfile));
    }

    if (ctx.isToolActiv('help'))
      map.addControl(new Carmen.Control.Help());

    if (ctx.isToolActiv('exportImg'))
      map.addControl(new Carmen.Control.ExportImg());
    if (ctx.isToolActiv('exportPdf'))
      map.addControl(new Carmen.Control.ExportPdf());
    if (ctx.isToolActiv('print'))
      map.addControl(new Carmen.Control.Print({
        toolbarTarget: ctx.getToolbarTarget('print'),
        toolbarPos: 6
      })
      );
    if (ctx.isToolActiv('download'))
      map.addControl(new Carmen.Control.DataDownload());

    if (ctx.isToolActiv('addLayerOGC'))
      map.addControl(new Carmen.Control.AddOGC());
    map.addControl(new Carmen.Control.Coordinate());

    // referenceMap
    if (ctx.isToolActiv('referenceMap')) {
      var rm_config = ctx.mdataMap.ReferenceMap;
      var rm_imgUrl = rm_config.OnlineResource.attributes.href;
      var rm_size = new OpenLayers.Size(rm_config.attributes.width, rm_config.attributes.height);
      var rm_lc = rm_config.BoundingBox.LowerCorner.split(" ");
      var rm_uc = rm_config.BoundingBox.UpperCorner.split(" ");
      var rm_maxExtent = new OpenLayers.Bounds(rm_lc[0],
        rm_lc[1], rm_uc[0], rm_uc[1]);
      var rm_projection = ctx.getProjection();
      var rm_projectionUnits = ctx.getProjectionUnits();

      // Warning a layer used for an overviewmap should not be used as a base layer 
      // for the general map or for another overviewmap control
      var layerReference = new OpenLayers.Layer.Image(
        "Reference Map",
        rm_imgUrl,
        rm_maxExtent,
        rm_size,
        {
          projection: rm_projection,
          units: rm_projectionUnits,
          singleTile: true
        });
      var RmOptions = {
        size: new OpenLayers.Size(180, 100),
        layers: [layerReference],
        minRectSize: 5,
        mapOptions: {
          numZoomLevels: 1,
          maxExtent: rm_maxExtent,
          projection: rm_projection,
          units: rm_projectionUnits
        }
      };
      map.addControl(new Carmen.Control.ReferenceMap(layerReference, RmOptions));
    }

    // Layer Tree Manager
    var ctx = this.context;
    var map = this.map;

    var layerTreeManager = new Carmen.Control.LayerTreeManager(
      { visible: ctx.isToolActiv('legend') }
    );

    var jsonLayerTree = ctx.mdataMap.LayerTree;
    var jsonLayerList = ctx.layer;
    layerTreeManager.buildTree(jsonLayerTree, jsonLayerList);

    this.map.addControl(layerTreeManager);

    var ctx = this.context;
    var map = this.map;
    var layerTreeManager = map.getLayerTreeManager();
    var layerTree = layerTreeManager.getLayerTree();
    // need a base layer, get one from mapserver with no layers selected... 
    // build and add layers
    var labelLayer = null;
    var layers = this._buildLayers2(layerTree, ctx, labelLayer);
    for (var i = layers.length - 1; i >= 0; i--) {
      var symbolscaledenom = -1;
      var isProportional = false;
      for (var a = 0; a < ctx.layer.length; a++) {
        if (ctx.layer[a].Title == layers[i].name) {
          if (ctx.layer[a].Extension.TYPE_SYMBO && ctx.layer[a].Extension.TYPE_SYMBO == "PROPORTIONAL") {
            isProportional = true;
            symbolscaledenom = (ctx.layer[a].Extension.layerSettings_symbolScaleDenom ? ctx.layer[a].Extension.layerSettings_symbolScaleDenom : -1);
          }
        }
      }
      if (isProportional) {
        layers[i].params["ISPROPORTIONAL"] = isProportional;
        layers[i].params["SYMBOLSCALEDENOM"] = symbolscaledenom;
      }
      if (layers[i].maxExtent == null)
        layers[i].maxExtent = ctx.getMaxExtent();
      // handling resolutions for WMSC layers
      if (layers[i].resolutions != null && map.resolutions != null) {
        // taking default WMS-C resolutions in map and inserting new ones from WMS-C layers 
        // or replacing existing which are close to new resolutions by new resolutions 
        var newRes = [];
        // if map min/max resolutions defined, filtering layer resolutions
        //  before merging them with map resolutions
        if (map_minScaleDenom != null && map_maxScaleDenom != null) {
          for (var k = 0; k < layers[i].resolutions.length; k++) {
            var res = layers[i].resolutions[k];
            var sc = OpenLayers.Util.getScaleFromResolution(res, "m").toFixed();
            if (sc >= map_minScaleDenom && sc <= map_maxScaleDenom) {
              newRes.push(res);
            }
          }
        }
        else
          newRes = [].concat(layers[i].resolutions);
        for (var j = 0; j < map.resolutions.length; j++) {
          newRes = Carmen.Util.set_add(
            newRes,
            map.resolutions[j],
            Carmen.Util.resNearTester);
        }
        newRes.sort(Carmen.Util.descendingOrder);
        map.resolutions = newRes;

      }
      // layers considered as overlay
      layers[i].setIsBaseLayer(false);
      map.addLayer(layers[i]);
    }
    // other approach : delay processing
    Ext.defer(layerTreeManager.treeLayoutInit, 250, layerTreeManager);
    // updating scaleChooser scales wrt to resolutions in case of WMSC layers
    if (map.resolutions != null && controlScaleChooser != null) {
      var scales = [];
      for (var i = 0; i < map.resolutions.length; i++) {
        var sc = OpenLayers.Util.getScaleFromResolution(map.resolutions[i], "m").toFixed();
        scales.push(Carmen.Util.roundIntToDigits(sc));
      }
      controlScaleChooser.updateScaleValues(scales);
    }
    // Should always be present with a map   
    //preventing mouse offset when resizing border regions of the viewport
    this.ui.getMainPanel().on('resize',
      function (p, w, h) {
        this.map.events.clearMouseCache();
        this.map.updateSize();
      }, this);
    return this.context;

  },

  getLocateByAddress: function () {
    var c_config = "";
    Ext.Ajax.timeout = 120000;
    Ext.Ajax.request({
      url: '/services/GetByAddress/index.php',
      params: { service: 'getconfig' },
      scope: this,
      success: function (response) {
        var c_res = eval("(" + response.responseText + ")");
        c_config = c_res["prodige_settings_value"];
        if (c_config != "") {
          this.map.addControl(new Carmen.Control.LocateByAddress({ "locateaddress": c_config }));
          return true;
        } else
          return false;
      },
      failure: function (response) {
        if (Carmen.notAuthenticatedException(response)) return;
        return false;
      }
    });
  },

  updateContext: function (bPrint) {
    // extent
    this.context.setExtent(this.map.getExtent());

    // layers visibility
    var layerMgr = this.map.getLayerTreeManager();
    var layersVisibility = layerMgr.getLayersVisibility();
    for (var i = 0; i < layersVisibility.length; i++) {
      layerVis = layersVisibility[i];
      this.context.setLayerVisibility(layerVis.layerIdx, layerVis.visible);
    }

    this.context.setGeneralConfig(layerMgr.serializeForContext());


    // favorite areas
    var favoriteAreas = this.map.getControlsByClass('Carmen.Control.FavoriteAreas');
    if (favoriteAreas.length > 0) {
      favoriteAreas = favoriteAreas[0];
      this.context.setGeneralConfig(favoriteAreas.serializeForContext());
    }

    // annotations
    var annotation = this.map.getControlsByClass('Carmen.Control.AdvancedAnnotation');
    if (annotation.length > 0) {
      annotation = annotation[0];
      if (bPrint) {
        this.context.setGeneralConfig({ Annotation: annotation.exportFeatures() });
        this.context.setGeneralConfig(annotation.exportLabels());
      }
      else {
        this.context.setGeneralConfig(annotation.serializeForContext());
      }
    }

    return this.context.getObj();
  },

  /*********************************************************************************************
   * Building Layers....
   *********************************************************************************************/

  NODE_LAYER: 0,
  NODE_GROUP: 1,

  _buildLayers2: function (layerTree, ctx, labelLayer) {

    var layers = new Array();
    var providerGrp = new Array();
    this._getProvider2layers(layerTree, providerGrp);

    for (var i = 0; i < providerGrp.length; i++) {
      var layerNames = '';
      if (providerGrp[i] instanceof Array) {
        for (var j = 0; j < providerGrp[i].length; j++) {
          var nodes = providerGrp[i];
          layerNames = layerNames + '+' + providerGrp[i][j].attributes.jsonLayerDesc.attributes.name;
        }
        var layer = this._buildLayer2(providerGrp[i], i, ctx, labelLayer);
        if (layer != null)
          layers[layers.length] = layer;
      }
    }
    return layers;
  },

  /*
   * @brief descend the tree to fill the provider2layers array so that 
   * provider2layers[providerGrpId]=[list of layers node...]
   */
  _getProvider2layers: function (node, provider2layers) {
    if (node.attributes.type == this.NODE_LAYER) {
      var providerId = node.attributes.providerId;
      if (!(provider2layers[providerId] instanceof Array)) {
        provider2layers[providerId] = new Array();
      }
      provider2layers[providerId][provider2layers[providerId].length] = node;
    }
    else if (node.attributes.type == this.NODE_GROUP) {
      //obedel migration
      for (var i = 0; i < node.childNodes.length; i++) {
        this._getProvider2layers(node.childNodes[i], provider2layers);
      }
    }
  },

  _buildMapserverLayer2: function (layerNodes, providerUrl, ctx, labelLayer) {
    var msParams = {
      LAYERS: '',
      map_imagetype: ctx.mdataMap.outputFormat,
      map_transparent: 'TRUE'
    };
    var layerNames = "";
    var handleSelection = false;
    for (var i = 0; i < layerNodes.length; i++) {
      var node = layerNodes[i];
      var lname = node.attributes.jsonLayerDesc.attributes.name;
      layerNames = layerNames + '+' + lname;
      handleSelection = handleSelection ||
        (layerNodes[i].attributes.jsonLayerDesc.Extension.layerSettings_infoFields != '')
    }
    layerNames = layerNames.slice(1);

    // little hack to retrieve mapfile from wfs...
    var mapfile = node.attributes.jsonLayerDesc.Extension.layerSettings_mapfile;
    mapfile = mapfile != null ? mapfile : window.mapfile;
    var layer = new Carmen.Layer.MapServerGroup(
      layerNames,
      providerUrl,
      msParams,
      {
        projection: ctx.getProjection(),
        units: ctx.getProjectionUnits(),
        singleTile: true,
        mapfile: mapfile,
        selectionSupport: handleSelection
      });
    layer.setVisibility(false);
    for (var i = 0; i < layerNodes.length; i++) {
      var desc = layerNodes[i].attributes.jsonLayerDesc;
      var lname = desc.attributes.name;
      var lvis = !(desc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis);
      var hasLabel = desc.Extension.hasLabel == "1";
      layerNodes[i].hasLabel = hasLabel;

      if (hasLabel) {
        // first, switch off labels for each class of the layer
        var classes = [];
        // TODO put this in Util : arrayFromAttribute...
        if ('StyleList' in desc) {
          classes = desc.StyleList.Style instanceof Array ?
            desc.StyleList.Style :
            [].concat(desc.StyleList.Style);
        }
        var params = {};

        // then, add a copy of this layer in the composite label layer,
        // and switch off its symbols
        labelLayer.addSubLayer(lname, lvis);
        labelLayer.params['map.layer[' + lname + ']'] = 'OPACITY 1';
      }
    }
    return layer;
  },

  _buildWMSLayer2: function (layerNodes, providerUrl, ctx) {
    var layerNames = "";
    for (var i = 0; i < layerNodes.length; i++) {
      var name = layerNodes[i].attributes.jsonLayerDesc.Title;
      layerNames = layerNames + ',' + name;
    }

    layerNames = layerNames.slice(1);
    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    var version = Url.decode(firstLayerDesc.Server.attributes.version);
    var outputFormat = Url.decode(firstLayerDesc.Extension.wms_format);
    var projection = Ext.valueFrom(Url.decode(firstLayerDesc.Extension.wms_srs), ctx.getProjection(), false);
    var projUnits = Carmen.Util.getProjectionUnits(projection);
    var opacity = layerNodes[0].attributes.opacity;

    var layer = new Carmen.Layer.WMSGroup(
      layerNames,
      providerUrl,
      {
        LAYERS: '',
        version: version,
        format: outputFormat,
        transparent: true
      },
      {
        isBaseLayer: false,
        singleTile: true,
        projection: projection,
        units: projUnits,
        opacity: opacity
      }
    );

    layer.setVisibility(false);
    for (var i = 0; i < layerNodes.length; i++) {
      var lname = layerNodes[i].attributes.jsonLayerDesc.attributes.name;
      var lwmsName = layerNodes[i].attributes.jsonLayerDesc.Name
      var lvis = !(layerNodes[i].attributes.jsonLayerDesc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis, lwmsName);
    }
    return layer;
  },

  _buildWMSCLayer2: function (layerNodes, providerUrl, ctx) {
    var layerNames = "";
    for (var i = 0; i < layerNodes.length; i++) {
      var name = layerNodes[i].attributes.jsonLayerDesc.Title;
      layerNames = layerNames + ',' + name;
    }
    layerNames = layerNames.slice(1);
    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    var version = Url.decode(firstLayerDesc.Server.attributes.version);
    var outputFormat = Url.decode(firstLayerDesc.Extension.wms_format);
    var projection = Ext.valueFrom(Url.decode(firstLayerDesc.Extension.wms_srs), ctx.getProjection(), false);
    var projUnits = Carmen.Util.getProjectionUnits(projection);
    var opacity = layerNodes[0].attributes.opacity;

    layerOptions = {
      isBaseLayer: false,
      buffer: 0,
      tileSize: new OpenLayers.Size(256, 256),
      projection: projection,
      resolutions: null,
      units: projUnits,
      opacity: opacity
    };

    // getting wmsc boundingbox if defined
    var str_boundingbox = Ext.valueFrom(firstLayerDesc.Extension.wmsc_boundingbox, null, false);

    layerOptions.maxExtent = ctx.getMaxExtent();
    if (str_boundingbox) {
      var sep = str_boundingbox.indexOf(',') != -1 ? ',' : ' ';
      var bb = Carmen.Util.chompAndSplit(Url.decode(str_boundingbox), sep);
      layerOptions.maxExtent = new OpenLayers.Bounds(bb[0], bb[1], bb[2], bb[3]);
    }
    // getting wmsc resolution if defined
    var str_res = Ext.valueFrom(firstLayerDesc.Extension.wmsc_resolution, null, false);
    if (str_res != null) {
      str_res = Url.decode(str_res);
      var sep = str_res.indexOf(',') != -1 ? ',' : ' ';
      var res = Carmen.Util.chompAndSplit(str_res, sep);
      // kind of defensive prog to prevent wrong format...
      if (!isNaN(new Number(res[0]))) {
        layerOptions.resolutions = Carmen.Util.array_map(res, parseFloat);
      }
    }
    //this.context.mapExtent = layerOptions.maxExtent;
    var layer = new Carmen.Layer.WMSGroup(
      layerNames,
      providerUrl,
      {
        LAYERS: '',
        version: version,
        format: outputFormat
      },
      layerOptions);
    layer.setVisibility(false);
    for (var i = 0; i < layerNodes.length; i++) {
      var lname = layerNodes[i].attributes.jsonLayerDesc.attributes.name;
      var lwmsName = layerNodes[i].attributes.jsonLayerDesc.Name;
      var lvis = !(layerNodes[i].attributes.jsonLayerDesc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis, lwmsName);
      // updating wms icon
    }

    return layer;
  },

  _buildWMTSLayer2: function (layerNodes, providerUrl, ctx) {
    var layerNames = "";
    for (var i = 0; i < layerNodes.length; i++) {
      var name = layerNodes[i].attributes.jsonLayerDesc.Title;
      layerNames = layerNames + ',' + name;
    }
    layerNames = layerNames.slice(1);
    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    var outputFormat = firstLayerDesc.Extension.wms_format;
    var style = firstLayerDesc.Extension.wmts_style;
    var tileset = firstLayerDesc.Extension.wmts_tileset;
    var wmts_layer_name = firstLayerDesc.Extension.wmts_wms_layer;
    // resolution
    var serverResolution = null;
    var str_res = Url.decode(firstLayerDesc.Extension.wmts_resolutions);
    var res = Carmen.Util.chompAndSplit(str_res, ' ');
    if (!isNaN(new Number(res[0]))) {
      serverResolution = Carmen.Util.array_map(res, parseFloat);
    }
    // matrixIds
    var matrixIds = [];
    var str_matrixids = Url.decode(firstLayerDesc.Extension.wmts_matrixids);
    var ids_matrix = Carmen.Util.chompAndSplit(str_matrixids, ' ');
    var tileOrigins = [];
    var str_tileOrigins = Url.decode(firstLayerDesc.Extension.wmts_tileorigin);
    var tileorigins_coords = Carmen.Util.chompAndSplit(str_tileOrigins, ' ');
    for (var i = 0; i < ids_matrix.length; i++) {

      var coords = Carmen.Util.chompAndSplit(tileorigins_coords[i], ',');
      var tlc = new OpenLayers.LonLat(
        parseFloat(coords[0]),
        parseFloat(coords[1])
      );

      matrixIds.push({
        identifier: ids_matrix[i],
        topLeftCorner: tlc
      }
      );

    }
    // tileOrigin
    var str_tileorigin = Url.decode(firstLayerDesc.Extension.wmts_tileorigin);
    var tileorigin_coords = Carmen.Util.chompAndSplit(str_tileorigin, ' ');
    var tileorigin = new OpenLayers.LonLat(
      parseFloat(tileorigin_coords[0]),
      parseFloat(tileorigin_coords[1]));
    var opacity = layerNodes[0].attributes.opacity;

    var layer = new Carmen.Layer.WMTSGroup({
      name: wmts_layer_name,
      url: providerUrl,
      layer: wmts_layer_name,
      matrixSet: tileset,
      matrixIds: matrixIds,
      format: "image/png",
      style: style,
      opacity: opacity,
      isBaseLayer: false,
      fixedPosition: null,
      serverResolutions: serverResolution,
      resolutions: serverResolution,
      visibility: false
    });

    for (var i = 0; i < layerNodes.length; i++) {
      var lname = layerNodes[i].attributes.jsonLayerDesc.attributes.name;
      var lwmtsName = layerNodes[i].attributes.jsonLayerDesc.Extension.wmts_wms_layer;
      var lvis = !(layerNodes[i].attributes.jsonLayerDesc.attributes.hidden == "1");
      layer.addSubLayer(lname, lvis, lwmtsName);
      // updating wms icon
      //var domIconEl = layerNodes[i].getUI().getIconEl();
      //domIconEl.src = layer.getLegendGraphicURL(lwmsName);
    }


    return layer;
  },

  _buildVectorLayer2: function (node, ctx) {
    var desc = node.attributes.jsonLayerDesc;
    var layer = new Carmen.Layer.Vector(desc.Title,
      {
        context_kml: desc.Document,
        context_style: desc.Extension.layerSettings_olStyle,
        useStyleMap: true
      });
    layer.setVisibility(true);
    return layer;
  },

  _buildLayer2: function (layerNodes, providerGrpId, ctx, labelLayer) {
    console.log('_buildLayer2 ', arguments)
    var layer = null;
    var providerUrl = null;
    if (!layerNodes) {
      return false;
    }

    firstLayerDesc = layerNodes[0].attributes.jsonLayerDesc;
    if ('Server' in firstLayerDesc) {

      //TODO élément de différenciation à améliorer
      if (!('wms_layer' in firstLayerDesc.Extension)) {
        // Mapserver-CGI layer ? 
        providerUrl = firstLayerDesc.Server.OnlineResource.attributes.href;
        // @Todo: add better mapserver detection 
        var isMapserverLayer = (providerUrl.search(/mapserv/i) > 0);
        if (isMapserverLayer) {
          layer = this._buildMapserverLayer2(layerNodes, providerUrl, ctx, labelLayer);
        }
      }
      else if ('Server' in firstLayerDesc) {
        // WMS layer ?
        var service = firstLayerDesc.Server.attributes.service;

        var providerUrl = firstLayerDesc.Server.OnlineResource.attributes.href;
        var isWMS = (service.search(/WMS/i) > -1);
        if (isWMS) {
          layer = this._buildWMSLayer2(layerNodes, providerUrl, ctx);
        }
        var isWMSC = Ext.valueFrom(firstLayerDesc.Extension.layerSettings_isWmsc, "OFF") == "YES";
        if (isWMSC) {
          layer = this._buildWMSCLayer2(layerNodes, providerUrl, ctx);
        }
        var isWMTS = Ext.valueFrom(firstLayerDesc.Extension.layerSettings_isWmts, "OFF") == "YES";
        if (isWMTS) {
          layer = this._buildWMTSLayer2(layerNodes, providerUrl, ctx);
        }
      }

      if (layer != null) {
        // linking nodes with OpenLayers layer 
        for (var i = 0; i < layerNodes.length; i++) {
          var node = layerNodes[i];
          var layerDesc = layerNodes[i].attributes.jsonLayerDesc;
          node.attributes.OlLayer = layer;
          node.attributes.labelLayer = labelLayer;
          // linking check control to layer display
        }
      }
    }
    else if ('Document' in firstLayerDesc) {
      node = layerNodes[0];
      layer = this._buildVectorLayer2(node, ctx);
      if (layer != null) {
        // linking nodes with OpenLayers layer 
        node.attributes.OlLayer = layer;
        // linking check control to layer display
      }
    }
    return layer;
  }


});

Carmen.Application.CLASS_NAME = "Carmen.Application";

/*********************************************************************************************
 * Context Class definition
 *********************************************************************************************/

Carmen.JSONContext = new OpenLayers.Class({
  jsOws: null,
  generalExt: null,
  layer: [],
  mapProjection: null,

  mdataTool: null,
  mdataMap: null,
  mdataLayer: null,
  mapExtent: null,
  maxExtent: null,

  annotationAttributes: null,

  initialize: function (context) {
    this.jsOws = context;
    this.generalExt = this.jsOws.OWSContext.General.Extension;
    if (this.jsOws.OWSContext.ResourceList.Layer)
      this.layer = this.jsOws.OWSContext.ResourceList.Layer instanceof Array ?
        this.jsOws.OWSContext.ResourceList.Layer :
        [].concat(this.jsOws.OWSContext.ResourceList.Layer);

    //Deal with unique result
    if (this.jsOws.OWSContext.ResourceList.Layer[0] === undefined)
      this.jsOws.OWSContext.ResourceList.Layer = [].concat(this.jsOws.OWSContext.ResourceList.Layer);
    ;

    this.parseMdata();
  },

  getObj: function () {
    return this.jsOws;
  },

  getProjection: function () {
    if (this.mapProjection == null)
      this.mapProjection = this.jsOws.OWSContext.General.BoundingBox.attributes.crs.split('=')[1].toLocaleUpperCase();
    return this.mapProjection;
  },

  getProjectionUnits: function () {
    if (this.mapProjectionUnits == null)
      this.mapProjectionUnits = Carmen.Util.getProjectionUnits(this.getProjection());
    return this.mapProjectionUnits;
  },

  getExtent: function () {
    if (this.mapExtent == null) {
      this.mapExtent = Carmen.Util.convertContextBBtoolBounds(
        this.jsOws.OWSContext.General.BoundingBox);
    }
    return this.mapExtent;
  },

  getMaxExtent: function () {
    if (this.maxExtent == null) {
      this.maxExtent = Carmen.Util.convertContextBBtoolBounds(
        this.jsOws.OWSContext.General.MaxBoundingBox);
    }
    return this.maxExtent;
  },

  // Modif SWT [P2-5] 7590 : Saisie de plusieurs attributs pour les annotations
  // On ajoute une fonction pour récupérer la liste des attributs plutôt qu'un
  // unique attribut.
  // NB : Les noms d'attributs renvoyés peuvent contenir des caractères spéciaux.
  getAnnotationAttributes: function () {
    if (this.annotationAttributes == null) {
      var attrs = null;
      if (this.mdataTool.attribut_value_annotation) {
        // Attention, il y a bien deux niveaux d'encodage !
        // La chaine complète pour encoder les séparateurs.
        attrs = decodeURIComponent(this.mdataTool.attribut_value_annotation).split(";");
        // Et chaque élément qui pourrait contenir un séparateur
        // ou des caractères spéciaux.
        for (var i = 0; i < attrs.length; i++) {
          attrs[i] = decodeURIComponent(attrs[i]);
        }
      }
      this.annotationAttributes = attrs;
    }
    return this.annotationAttributes;
  },

  getTitle: function () {
    return Url.decode(this.jsOws.OWSContext.General.Title);
  },

  getFavoriteAreas: function () {
    //modif dds
    if (this.generalExt.GeoBookmark) {
      return this.generalExt.GeoBookmark;
    }
    if (this.generalExt.zones_favorites) {
      var fav = decodeURIComponent(this.generalExt.zones_favorites);
      fav = fav.replace(/'/g, '"');
      fav = fav.replace(/__G__/g, '\'');
      return JSON.parse(fav);
    }
    return this.generalExt.GeoBookmark ? this.generalExt.GeoBookmark : null;
  },

  setExtent: function (bounds) {
    this.jsOws.OWSContext.General.BoundingBox.LowerCorner = bounds.left + " " + bounds.bottom;
    this.jsOws.OWSContext.General.BoundingBox.UpperCorner = bounds.right + " " + bounds.top;
  },

  setLayerVisibility: function (layerIdx, visible) {
    this.jsOws.OWSContext.ResourceList.Layer[layerIdx].attributes.hidden = visible ? "0" : "1";
  },

  setGeneralConfig: function (config) {
    for (p in config)
      this.generalExt[p] = config[p];
  },

  getGeneralConfig: function (configName) {
    var config = null;
    if (configName in this.generalExt)
      config = this.generalExt[configName];
    return config;
  },

  isToolActiv: function (toolName) {
    var toolValue = Ext.valueFrom(this.mdataTool[toolName], "0")
    return (toolValue == "1" || toolValue == "ON");
  },

  // Modif SWT :
  // [P2-10] 6785 : Séparation des outils recentrer et localiser
  // [P2-14] 7527 : Recherche par numéro de parcelle
  // [P2-15] 7527 (& 7807 ): Recherche par adresse
  // J'ajoute une méthode pour savoir si la métadonnée existe.
  // Ca permet de gérer la backward compatibility :
  // Si un des 3 nouveau outils n'est pas défini, c'est que le
  // MapFile a été créé avec l'ancienne version. Et le
  // comportement n'est pas le même (voir utilisation dans
  // Application.js de Descartes).
  isToolDefined: function (toolName) {
    return this.mdataTool[toolName];
  },

  hasLayerWMSC: function () {
    var found = false;
    for (var k in this.mdataLayer) {
      if (Ext.valueFrom(this.mdataLayer[k].wmscLayer, "OFF", false) == "ON") {
        found = true;
        break;
      }
    }
    return found;
  },

  // fill hash mdataTool, mdataMap and mdataLayer 
  // with mdata from Extension parts of the context
  parseMdata: function () {
    this.mdataTool = {};
    this.mdataMap = {};
    for (var k in this.generalExt) {
      var matched = k.match(/^tool_(.+)$/);
      if (matched != null) {
        var toolName = matched[1];
        this.mdataTool[toolName] = this.generalExt[k];
      }
      else {
        matched = k.match(/^mapSettings_(.+)$/);
        if (matched != null) {
          var setting = matched[1];
          this.mdataMap[setting] = this.generalExt[k];
        }
        else
          this.mdataMap[k] = this.generalExt[k];
      }
    }

    this.mdataLayer = {};
    if (this.layer) {
      if (this.layer instanceof Array) {
        for (var i = 0; i < this.layer.length; i++) {
          var layerName = this.layer[i].Title;
          this.mdataLayer[layerName] = {};
          for (var k in this.layer[i].Extension) {
            var matched = k.match(/^layerSettings_(.+)$/);
            var setting = matched != null ? matched[1] : k;
            this.mdataLayer[layerName][setting] = this.layer[i].Extension[k];
          }
        }
      }
      else {
        var layerName = this.layer.Title;
        this.mdataLayer[layerName] = {};
        for (var k in this.layer.Extension) {
          var matched = k.match(/^layerSettings_(.+)$/);
          var setting = matched != null ? matched[1] : k;
          this.mdataLayer[layerName][setting] = this.layer.Extension[k];
        }
      }
    }
  },


  getNextLayerId: function () {
    return this.layer.length;
  },

  addLayerDescs: function (descs) {
    for (var i = 0; i < descs.length; i++)
      this.layer.push(descs[i]);
    return (this.layer.length - 1);
  },

  getLayerDescFromMdataValue: function (attributeName, value) {
    var i = 0;
    for (var k in this.mdataLayer) {
      var v = Ext.valueFrom(Url.decode(this.mdataLayer[k][attributeName]), null, false);
      if (v == value) {
        break;
      }
      i++;
    }
    return i <= (this.layer.length - 1) ? this.layer[i] : null;
  },

  /**
   * @brief find the first node of layerTreeNode
   * whose attribute is equals to value
   * browsing in depth first
   */
  getLayerTreeNodeFromMdataValue: function (attributeName, value) {
    var res = null;
    if (this.mdataMap.LayerTree.LayerTreeNode != null)
      res = this._findTreeNode(this.mdataMap.LayerTree.LayerTreeNode, attributeName, value);
    return res;
  },

  /**
   * @brief find the first node of layerTreeNode
   * whose attribute is equals to value
   * browsing in depth first
   */
  getLayerTreeNodePrintFromMdataValue: function (attributeName, value) {
    var res = null;
    if (this.mdataMap.LayerTreePrint.LayerTreeNode != null)
      res = this._findTreeNode(this.mdataMap.LayerTreePrint.LayerTreeNode, attributeName, value);
    return res;
  },

  /**
   * @brief find the first node of subnodes 
   * whose attribute is equals to value
   */
  _findTreeNode: function (node, attributeName, value) {
    var res = null;

    if (('LayerTreeNode' in node) && !(node.LayerTreeNode instanceof Array)) {
      node = node.LayerTreeNode;
    }

    if ((attributeName in node.attributes) && (node.attributes[attributeName] == value)) {
      res = node;
    }
    else if (('LayerTreeNode' in node) && (node.LayerTreeNode instanceof Array)) {
      for (var i = 0; i < node.LayerTreeNode.length; i++) {
        var tmp = this._findTreeNode(node.LayerTreeNode[i], attributeName, value);
        if (tmp !== null) {
          res = tmp;
          break;
        }
      }
    }
    return res;
  }
});

Carmen.Application.debugDD = function (layerTree, map) {
  var layerNodes = layerTree.collectChildNodes(
    function (n) {
      return ('type' in n.attributes &&
        (n.attributes.type == Carmen.Control.LayerTreeManager.NODE_LAYER))
    });
  for (var i = 0; i < layerNodes.length; i++) {
    var node = layerNodes[i];
    node.setText(
      map.getLayerIndex(node.attributes.OlLayer) + '  ' +
      node.attributes.OlLayer.getSubLayerInfo(node.attributes.layerName).index + ' ' +
      node.attributes.layerName
    );
  }
};

Carmen.JSONContext.CLASS_NAME = "Carmen.JSONContext";
