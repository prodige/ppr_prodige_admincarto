
/**
 * @_requires OpenLayers/Layer/Vector.js
 */

/**
 * Class: Carmen.Layer.Vector
 * Instances of Carmen.Layer.MapServer are used to display
 * data from a MapServer CGI instance and allow handle several 
 * mapserver layer in the same OpenLayers layer.
 *
 * Inherits from:
 *  - <OpenLayers.Layer.Vector>
 */
Carmen.Layer.Vector = new OpenLayers.Class(OpenLayers.Layer.Vector, {
 
  // dimension specific styles
  geometryDefaultStyles : {
    Point : OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']),
    LineString : OpenLayers.Util.extend(
     OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']),
     { strokeWidth : 2 }),
    Polygon : OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default'])
  },

 sketchStyle : OpenLayers.Util.extend(
     OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']),
     { strokeColor : "#000000",
       fillColor : "#000000" }),

  geomTypePrefix : { 
   "Point" : 'pt',
   "LineString" : 'ln',
   "Polygon" : 'po' }, 
  
  currentStyle : 0,
  updateStamp : 0,
  useStyleMap : false,
  
  // constext serialization options
  context_kml : null,
  context_style : null,  

  KMLAttributes : new OpenLayers.Class(OpenLayers.Format.KML, {
   createPlacemarkXML : function(feature) {
      var placemarkNode = OpenLayers.Format.KML.prototype.createPlacemarkXML.apply(this, arguments);
  
      for (var att in feature.attributes) {
        if (att!= 'name' && att !='description') {
          value = att;
          var attDesc = this.createElementNS(this.kmlns, att);
          attDesc.appendChild(this.createTextNode(feature.attributes[att]));
          placemarkNode.appendChild(attDesc);
        }
      }
      return placemarkNode;
    }
  }),  

  initialize: function(name, options) {
    OpenLayers.Layer.Vector.prototype.initialize.apply(this, arguments);
    if (this.context_kml || this.context_style)
      this.initFromContext(this.context_kml, this.context_style);
    
    if (!this.context_style && this.useStyleMap) {
      this.styleMap.styles['default'].addRules(
        new OpenLayers.Rule({ elseFilter : true }) );
      // adding unique value rule based on feature attribute 'description'
      this.styleMap.addUniqueValueRules('default', 'description', 
         { 'pt0' : OpenLayers.Util.extend({}, this.geometryDefaultStyles['Point']),
           'ln0' : OpenLayers.Util.extend({}, this.geometryDefaultStyles['LineString']),
           'po0' : OpenLayers.Util.extend({}, this.geometryDefaultStyles['Polygon'])
         });
      // cloning defaultStyle to avoid side effects
      this.styleMap.styles['default'].defaultStyle =  
        OpenLayers.Util.extend({},
          OpenLayers.Feature.Vector.style['default']);  
     }
  },
  
  initFromContext : function(context_kml, context_style) {
    if (context_style) {
      this._initStyleMap(context_style);
    }
    if (context_kml) {
      var kml = new OpenLayers.Format.KML({extractStyles: true, extractAttributes: true});
    	var features = kml.read(context_kml);
    	this.addFeatures(features);  
    }
  },
  
  serializeForContext : function(kmlAsString) {
  	var out_options = {
              'internalProjection': new OpenLayers.Projection(mapProjection),
              'externalProjection': new OpenLayers.Projection(mapProjection)
      };
					
    
    var features = this.features;
    for (var i=0; i< this.features.length; i++) {
      this.features[i].attributes.name = 
       this.features[i].attributes.textConfig_label ?
       this.features[i].attributes.textConfig_label : "";   
    }
    
    var kml = new this.KMLAttributes(out_options)
    var strKml = kml.write(this.features);
    var style = this._serializeStyleMap(this.styleMap); 

    return kmlAsString ?  
      { kml  : strKml , style : style} : 
      { kml  : {"#text" : strKml }, style : style};
  },
  
  
  exportAsKML : function(geomType) {
    var out_options = {
              'internalProjection': new OpenLayers.Projection(mapProjection),
              'externalProjection': new OpenLayers.Projection(mapProjection)
      };
		
    var kml = new this.KMLAttributes(out_options)
    var strKml = "";
    var features = this.features;
    for (var i=0; i< this.features.length; i++) {
      this.features[i].attributes.name = 
       this.features[i].attributes.textConfig_label ?
       this.features[i].attributes.textConfig_label : "";   
    }
    
    if (geomType) {
      features = [];
      for (var i=0; i< this.features.length; i++) {
        if (this.getGeomType(this.features[i].geometry)==geomType) {
          features.push(this.features[i]);
        }
      }
    }
    
    if (features.length>0)
     strKml = kml.write(features);
    
    return strKml;
  },


  _serializeStyleMap : function() {
		var stl = this.styleMap.styles['default'];
		var attMap = {};
		var elseSymbolizer = {};
		var filterProperty = '';
		for(var i=0; i<stl.rules.length; i++) {
		  var r = stl.rules[i];
		  if (r.elseFilter) 
		  	elseSymbolizer = stl.defaultStyle;
		  else {
		  	attMap[r.filter.value.toString()] =  r.symbolizer;
		  	filterProperty = r.filter.property; 
		  }
		};
		
		var serializeMap = {
		  defaultStyle : stl.defaultStyle,
		  filterRules : {
		    elseSymbolizer : elseSymbolizer,
		    filterProperty : filterProperty,
		    map : attMap
		  },
		  currentStyle : this.currentStyle 
		};	  
		return serializeMap
	},

	_initStyleMap : function(serializedMap) {
    var symboliser = {};
    var attMap  = serializedMap.filterRules.map;
    if (attMap!="") {
      for (var k in attMap) {
       symboliser[k] = OpenLayers.Util.extend({}, attMap[k]);
       // repasrsing of boolean values
       // should be handled better way by serialization...
       for (var l in symboliser[k]) {
         symboliser[k][l] = 
          (symboliser[k][l] == "false" || symboliser[k][l] == "true") ?
           symboliser[k][l] == "true" : symboliser[k][l];
       }
   	  }
   	  
   	  
    }
    this.styleMap.styles['default'].defaultStyle = 
     OpenLayers.Util.extend({}, serializedMap.defaultStyle);
    this.styleMap.styles['default'].addRules(
      new OpenLayers.Rule({ elseFilter : true }) );
    var stl = OpenLayers.Util.extend({}, 
			this.styleMap.styles['default'].defaultStyle);
	 	stl = OpenLayers.Util.extend(stl, serializedMap.defaultStyle);
		this.styleMap.styles['default'].defaultStyle = stl;
    this.styleMap.styles['default'].rules = [];
  	this.styleMap.addUniqueValueRules('default', serializedMap.filterRules.filterProperty, symboliser);
  	this.currentStyle = serializedMap.currentStyle;    
	},
  
  drawFeature: function(feature, style) {
    //console.log(this.name + " " + this.getZIndex());
    // do not apply automatic style map to displacement or editing nodes  
    if (feature._sketch) {
      feature.style = this.sketchStyle;
    }
    if (this.useStyleMap) {
      if (style==null && feature.style==null && 
       feature.attributes.description==null) {
        this.addFeatureStyle(feature);
      }
    }
    OpenLayers.Layer.Vector.prototype.drawFeature.apply(this, [feature, style]);
  },
  
  addFeatureStyle : function(feature, style, derive) {
     var geomType = this.getGeomType(feature.geometry);
     if (feature.attributes.description && derive) {
       var symboliser = {};
       var featureSymboliser = this.styleMap.createSymbolizer(feature, 'default');
       feature.attributes.description = feature.attributes.description + '_' + this.updateStamp.toString();
       this.updateStamp++; 
       symboliser[feature.attributes.description] =
         OpenLayers.Util.extend(
           OpenLayers.Util.extend({}, featureSymboliser), style);
       this.styleMap.addUniqueValueRules('default', 'description',  symboliser);
     } 
     else {
       if (style != null) {
         this.currentStyle++;
         var symboliser = {};
         symboliser[this.geomTypePrefix[geomType] + this.currentStyle.toString()] =
           OpenLayers.Util.extend({}, this.geometryDefaultStyles[geomType]);
         this.styleMap.addUniqueValueRules('default', 'description',  symboliser);
         this.redraw();
       }
       feature.attributes.description = this.geomTypePrefix[geomType] + this.currentStyle.toString();
     }
   },
   
   updateGeometryDefaultStyle : function(pointStyle, lineStyle, polygonStyle) {
     OpenLayers.Util.extend(this.geometryDefaultStyles['Point'], pointStyle);
     OpenLayers.Util.extend(this.geometryDefaultStyles['LineString'], lineStyle);
     OpenLayers.Util.extend(this.geometryDefaultStyles['Polygon'], polygonStyle);  
     this.currentStyle++;
     var symboliser = {};
     symboliser[this.geomTypePrefix['Point'] + this.currentStyle.toString()] =
       OpenLayers.Util.extend({}, this.geometryDefaultStyles['Point']);
     symboliser[this.geomTypePrefix['LineString'] + this.currentStyle.toString()] =
       OpenLayers.Util.extend({}, this.geometryDefaultStyles['LineString']);
     symboliser[this.geomTypePrefix['Polygon'] + this.currentStyle.toString()] =
       OpenLayers.Util.extend({}, this.geometryDefaultStyles['Polygon']);
     this.styleMap.addUniqueValueRules('default', 'description',  symboliser);
   },
   
   // private   
   getGeomType : function(geometry) {
     var res = 'Point';
     switch (geometry.CLASS_NAME) {
      case "OpenLayers.Geometry.Point":
            res = "Point"; 
            break;
        case "OpenLayers.Geometry.LineString":
            res = "LineString";
            break;
        case "OpenLayers.Geometry.Polygon":
             res = "Polygon";
            break;  
      }
     return res;
   }, 
  
  /**
   * @return the first feature having att equl to value or null if none found
   */
  getFeatureByAttribute : function(att, value) {
    var found = null;
    var i=0;
    while (found == null && i < this.features.length) {
      found = (this.features[i].attributes[att]!=null && 
          this.features[i].attributes[att]==value) ? 
        this.features[i] :
        null;
      i++; 
    }
    return found;
  },
  
  CLASS_NAME : "Carmen.Layer.Vector"	
});










