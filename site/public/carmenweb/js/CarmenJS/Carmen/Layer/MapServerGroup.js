
/**
 * @_requires OpenLayers/Layer/Mapserver.js
 */

/**
 * Class: Carmen.Layer.MapServerGroup
 * Instances of Carmen.Layer.MapServer are used to display
 * data from a MapServer CGI instance and allow handle several 
 * mapserver layer in the same OpenLayers layer.
 *
 * Inherits from:
 *  - <OpenLayers.Layer.Mapserver>
 */

 
var oldMapfile = "";
var oldUrl	   = "";
	
Carmen.Layer.MapServerGroup = new OpenLayers.Class(OpenLayers.Layer.MapServer, {

  subLayers : null,  
  
  mainLayers : '',
  
  // Properties used to dislay a layer feature selection
  // due to mapserver cgi qstring limitation, only one feature 
  // can be highlighted at a time
  displayMode : 0,
  qstring : '',
  qitem : '',
  selectionSupport : true,
  selectionStore : null,
  selectionColumnModel : null,
  
  initialize: function(options) {
    OpenLayers.Layer.MapServer.prototype.initialize.apply(this, arguments);
    
    this.ratio = 1.0;
    
    this.mainLayers = this.params.LAYERS; 
    this.subLayers = [];
    
    // IE hack, may be a pb if imagetype gif not defined in mapfile...
    this.params.map_imagetype = this.params.map_imagetype;
    
  },
  
    
  hasSubLayers : function() {
  	return this.subLayers.length>0;
  },
  
  addSubLayer : function(layerName, visible) {
  	var info = this.getSubLayerInfo(layerName); 
  	if (!info) {
  		this.subLayers.push({ name: layerName, visible: visible, index: this.subLayers.length});
  	}
  	else {
  		info.visible = visible;
    }
    
  	var layerVisibility = false;
    for (var i=0; i<this.subLayers.length; i++) {
      layerVisibility = layerVisibility || this.subLayers[i].visible;  
    } 
    this.setVisibility(layerVisibility);
  },	

	moveSubLayer : function(layerName, index) {
		var info = this.getSubLayerInfo(layerName);
		//console.log("moving SubLayer " + layerName + " from " + info.index + " to  " + index);
		if (info != null) {
			// removing sublayer
			var toBeUpdated = this.subLayers.splice(info.index + 1);
			//console.log(toBeUpdated);
			this.subLayers.pop();
			//console.log(this.subLayers);
			for (var i=0; i<toBeUpdated.length; i++)
				toBeUpdated[i].index = toBeUpdated[i].index -1;
			this.subLayers = this.subLayers.concat(toBeUpdated);
			//console.log(this.subLayers);
			
			
			// inserting subLayer at the rightPlace
			var toBeUpdated = this.subLayers.splice(index);
			//console.log(toBeUpdated);
			//console.log(this.subLayers);
			info.index = index;
			this.subLayers.push(info);
			for (var i=0; i<toBeUpdated.length; i++)
				toBeUpdated[i].index = toBeUpdated[i].index + 1;
			this.subLayers = this.subLayers.concat(toBeUpdated);
			
			//console.log("checking subLayers order start");
			//for (var i=0; i<this.subLayers.length; i++)
			//	console.log(this.subLayers[i].index);
			//console.log("checking finish");
		}
	},

		/**
     * Method: clone
     * Create a clone of this layer
     *
     * Returns:
     * {<Carmen.Layer.MapServerGroup>} An exact clone of this layer
     */
    clone: function (obj) {
      if (obj == null) {
          obj = new Carmen.Layer.MapServerGroup(this.name,
                                         this.url,
                                         this.params,
                                         this.options);
      }
      //get all additions from superclasses
      obj = OpenLayers.Layer.Grid.prototype.clone.apply(this, [obj]);

      // copy/set any non-init, non-simple values here
      obj.setVisibility(this.visibility);
      obj.setIsBaseLayer(this.isBaseLayer);
      
      obj.subLayers = [].concat(this.subLayers);
      obj.mainLayers = this.mainLayers;
      obj.displayMode = this.DISPLAY_MAP;
			obj.qstring = '';
			obj.qitem = ''; 

      return obj;
	},

	/**
	 * @brief split the current layer in two layers.
	 * 	The first layer is composed of the [0.. index] sub layers 
	 * 	of the current layer, the second layer of the 
	 * 	[index+1.. subLayersCount-1] sub layers...   
	 * @param index : the index of the sublayer to split the  
	 * @return an array composed of the two splitted layers, the 
	 * first layer returned is a reference to current layer changed 
	 * in place, the second is a new layer 
	 * */

	split : function(index) {
		var res = [];
		if (index> this.subLayers.length) {
			res.push(this);
			res.push(null);
		}
		else if (index<0) {
			this.subLayers = [];
			newLayer = this.clone();
			res.push(this);
			res.push(newLayer);
		} 
		else {
			var newLayer = this.clone();
			newLayer.subLayers = new Array();
			var subLayers2 = new Array();
			for (var i=0; i<this.subLayers.length; i++) {
				if (i<=index) 
					subLayers2.push(this.subLayers[i]);
				else
					newLayer.subLayers.push(this.subLayers[i]);
			}
			delete this.subLayers;
			this.subLayers = subLayers2;
			
			// updating sub layer index
			for (var i=0; i<this.subLayers.length; i++) {
				this.subLayers[i].index = i;
			}
			for (var i=0; i<newLayer.subLayers.length; i++) {
				newLayer.subLayers[i].index = i;
			}
			
			res.push(this);
			res.push(newLayer); 
		}
		return res;
	},
	

  getSubLayerInfo : function(layerName) {
  	var i=0;
  	while (i<this.subLayers.length && this.subLayers[i].name!=layerName) {
  		i++;
  	}
  	return (i<this.subLayers.length) ? this.subLayers[i] : null;
  },

  setSubLayerVisibility: function(layerName, isVisible) {
    // updating sublayer visibility
  	var subLayer = this.getSubLayerInfo(layerName);
  	if (subLayer!=null)
      subLayer.visible = isVisible;
  	//updating layer visibility
  	var layerVisibility = false;
  	for (var i=0; i<this.subLayers.length; i++) {
  		layerVisibility = layerVisibility || this.subLayers[i].visible;  
  	} 
  	
  	this.setVisibility(layerVisibility);
  	this.display(layerVisibility);
    this.redraw();
    if (this.map != null) {
      this.map.events.triggerEvent("changelayer", {
          layer: this,
          property: "visibility"
        });
    }
    this.events.triggerEvent("visibilitychanged");
  },
  
  
  getSubLayerVisibility: function(layerName) {
  	var subLayer = this.getSubLayerInfo(layerName);
  	return subLayer.visible;
  },
  
  getVisibility: function(layerName) {
		var vis = true;
		if (layerName==null || layerName==this.mainLayers) 
			vis = OpenLayers.Layer.WMS.prototype.getVisibility.apply(this,[]);
		else
			vis = this.getSubLayerVisibility(layerName);
		return vis;
  },
  



  _getLayersParam : function() {
  	var str = this.mainLayers;
  	for (var i=0; i<this.subLayers.length; i++) {
      if (this.subLayers[i].visible) {
        str = str + " " + this.subLayers[i].name; 
      }
  	}
  	str = (str.length > 0) && (str[0]==' ') ? str.slice(1) : str;
  	return str;
  },

  /**
   * Method: getURL
   * Return a query string for this layer. Alternativ 
   * to OpenLayers.Layer.Mapserver.getURL based on 
   * a center point and map scale, rather than on
   * map extent
   *
   * Parameters:
   * bounds - {<OpenLayers.Bounds>} A bounds representing the bbox 
   *                                for the request
   *
   * Returns:
   * {String} A string with the layer's url and parameters and also 
   *          the passed-in bounds and appropriate tile size specified 
   *          as parameters.
   */
  getURL: function (bounds) {
    var url = null;
    console.log(261);
    // we ensure map is set...
    // and we are in Mapserver map mode
    // cos querymap mode does not seem to support
    // mapxy and scaledenom param call
    if (Ext.valueFrom(this.map, null)!=null && 
      this.displayMode == this.DISPLAY_MAP) {   
      bounds = this.adjustBounds(bounds);
      // Make a list, so that getFullRequestString uses literal "," 
      var extent = [bounds.left, bounds. bottom, bounds.right, bounds.top];
      var imageSize = this.getImageSize();
      
      var center = bounds.getCenterLonLat();
      var scale = this.map.getScale();  
      
      // make lists, so that literal ','s are used 
      url = this.getFullRequestString(
                 {  
                  //imgext:   extent,
                  map_size: [imageSize.w, imageSize.h],
                  imgx:     imageSize.w / 2,
                  imgy:     imageSize.h / 2,
                  imgxy:    [imageSize.w, imageSize.h],
                  mapxy :   [center.lon, center.lat],
                  scaledenom : scale
                 });
    }
    else {
      url = OpenLayers.Layer.MapServer.prototype.getURL.apply(this, arguments);
    }
    return url;
 },


 redraw: function() {
    this.params.LAYERS = this._getLayersParam();
    this.params.map_resolution = Carmen.Mapserver_DOTS_PER_INCH;
    var redrawn = OpenLayers.Layer.MapServer.prototype.redraw.apply(this, arguments);
    return redrawn;
  },


  setDisplayMode : function(mode, qlayer, fid, qfile, ogrMapfile) {
  	switch(mode) {
      case this.DISPLAY_QUERYMAP :
        this.params.mode = 'INDEXQUERY';
        this.params['QLAYER'] = qlayer;
        this.params['SHAPEINDEX'] = fid;
        delete this.params['QUERYFILE'];
        delete this.params['TIME'];
        break; 
      case this.DISPLAY_NQUERYMAP :
    	if(typeof ogrMapfile != "undefined" && ogrMapfile != ""){
    	  oldMapfile = this.mapfile;
    	  oldUrl	 = this.url;
    	  oldLayer	 = this.mainLayers;
    	  this.params.mode = 'map';
    	  this.mainLayers += ' layerOgr';
    	  this.params.LAYERS += ' layerOgr';
    	  this.mapfile = ogrMapfile;
    	  this.url = window.CARMEN_URL_SERVER_DATA + "/cgi-bin/mapserv?map=" + ogrMapfile;
          delete this.params['SHAPEINDEX'];
          delete this.params['QLAYER'];
          delete this.params['QUERYFILE'];
          delete this.params['TIME'];
    	}else{
    	  this.params.mode = 'nquery';
    	  this.params['qformat'] = 'png24';
          this.params['QUERYFILE'] = qfile;
          this.params['TIME'] = new Date().getTime();
          delete this.params['SHAPEINDEX'];
          delete this.params['QLAYER'];	
    	}
        break;
  	  case this.DISPLAY_MAP :
      default:
    	if(oldMapfile != "" && oldUrl != ""){
    	  this.mapfile 		 = oldMapfile;
    	  this.url	   		 = oldUrl;
    	  this.mainLayers 	 = oldLayer;
    	  this.params.LAYERS = oldLayer;
    	}
        this.params.mode = 'map';
        delete this.params['SHAPEINDEX'];
        delete this.params['QLAYER'];
        delete this.params['QUERYFILE'];
        delete this.params['TIME'];
  		  break; 
    }
    this.displayMode = mode;
    this.redraw();
  },

  DISPLAY_MAP : 0,
  DISPLAY_QUERYMAP : 1,
  DISPLAY_NQUERYMAP : 2,
  CLASS_NAME : "Carmen.Layer.MapServerGroup",
  
  //////////////////////////////////
  // Selection handling part
  //////////////////////////////////
  
  handleSelection : function(lname) {
    return this.selectionSupport;   
  },
  
  hasSelection : function(lname) {
    var res = false;
    if (lname==null) {
      for(var i=0; i<this.subLayers.length;i++) {
        res = res || this.hasSelection(this.subLayers[i].name);
      } 
    }
    else {
      var lInfo = this.getSubLayerInfo(lname);
      if (lInfo && lInfo.selection && lInfo.selection.store) 
        res = lInfo.selection.store && lInfo.selection.store.getCount()>0;
    }
    return res;
  }, 

  clearSelection : function(lname) {
    if (lname==null) {
      for (var i=0;i<this.subLayers.length; i++) {
        this.clearSelection(this.subLayers[i].name);
      }
    }
    else {
      var lInfo = this.getSubLayerInfo(lname);
      if (lInfo && lInfo.selection && lInfo.selection.store)
        lInfo.selection.store.removeAll();
    }
  },
  
  setSelectionConfig : function(lname, config) {
    var lInfo = this.getSubLayerInfo(lname);
    if (lInfo) {
      lInfo.selection = config;
    }
  },
  
  setSelectionStore : function(lname, st) {
    var lInfo = this.getSubLayerInfo(lname);
    if (lInfo) {
      lInfo.selection.store = st;
    }
  },
  
  getSelection : function(lname) {
    var lInfo = this.getSubLayerInfo(lname);
    return lInfo ? lInfo.selection : null;
  }
});

Carmen.Layer.MapServerGroup.DISPLAY_MAP = 0;
Carmen.Layer.MapServerGroup.DISPLAY_QUERYMAP = 1;
Carmen.Layer.MapServerGroup.DISPLAY_NQUERYMAP = 2;


