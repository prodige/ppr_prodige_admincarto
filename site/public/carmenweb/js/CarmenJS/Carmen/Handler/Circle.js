/**
 * @_requires OpenLayers/Handler.js
 */

/**
 * Class: Carmen.Handler.Circle
 * Handler to draw a circle (regular polygon) on the map.  Circle is displayed on first click
 *     is modified on mouse move, and is finished on second click.
 *     Circle is canceled by rightclick
 *     The handler triggers callbacks for 'done' and 'cancel'.  Create a new
 *     instance with the <Carmen.Handler.Circle> constructor.
 * 
 * Inherits from:
 *  - <OpenLayers.Handler>
 */
Carmen.Handler = {};

Carmen.Handler.Circle = OpenLayers.Class(OpenLayers.Handler, {
    /**
     * Property: startedCircle (private)
     * {Boolean} true when drawing has begun
     */
    startedCircle : false,

    /**
     * APIProperty: sides
     * {Integer} Number of sides for the regular polygon.  Needs to be greater
     *     than 2.  Defaults to 64.
     */
    sides: 64,

    /**
     * APIProperty: radius
     * {Float} Optional radius in map units of the regular polygon.  If this is
     *     set to some non-zero value, a polygon with a fixed radius will be
     *     drawn and dragged with mose movements.  If this property is not
     *     set, dragging changes the radius of the polygon.  Set to null by
     *     default.
     */
    radius: null,
    
    /**
     * APIProperty: persist
     * {Boolean} Leave the feature rendered until clear is called.  Default
     *     is false.  If set to true, the feature remains rendered until
     *     clear is called, typically by deactivating the handler or starting
     *     another drawing.
     */
    persist: true,

    /**
     * Property: angle
     * {Float} The angle from the origin (mouse down) to the current mouse
     *     position, in radians.  This is measured counterclockwise from the
     *     positive x-axis.
     */
    angle: null,

    /**
     * Property: feature
     * {<OpenLayers.Feature.Vector>} The currently drawn polygon feature
     */
    feature: null,

    /**
     * Property: layer
     * {<OpenLayers.Layer.Vector>} The temporary drawing layer
     */
    layer: null,

    /**
     * Property: origin
     * {<OpenLayers.Geometry.Point>} Location of the first mouse down
     */
    origin: null,

    /**
     * Constructor: OpenLayers.Handler.RegularPolygon
     * Create a new regular polygon handler.
     *
     * Parameters:
     * control - {<OpenLayers.Control>} The control that owns this handler
     * callbacks - {Object} An object with a properties whose values are
     *     functions.  Various callbacks described below.
     * options - {Object} An object with properties to be set on the handler.
     *     If the options.sides property is not specified, the number of sides
     *     will default to 4.
     *
     * Named callbacks:
     * create - Called when a sketch is first created.  Callback called with
     *     the creation point geometry and sketch feature.
     * done - Called when the sketch drawing is finished.  The callback will
     *     recieve a single argument, the sketch geometry.
     * cancel - Called when the handler is deactivated while drawing.  The
     *     cancel callback will receive a geometry.
     */
    initialize: function(control, callbacks, options) {
        this.style = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']);
        this.style = OpenLayers.Util.extend(this.style,  
        {
         // taken form olHandlerZoomBox style...
         strokeWidth: 2,
         strokeColor: '#ff0000',
         strokeOpacity: 0.5,
         fillOpacity: 0.5,
         fillColor: '#ffffff'
        });

        OpenLayers.Handler.prototype.initialize.apply(this,
                                                [control, callbacks, options]);
        this.options = (options) ? options : new Object();
    },
    
    /**
     * APIMethod: setOptions
     * 
     * Parameters:
     * newOptions - {Object} 
     */
    setOptions: function (newOptions) {
        OpenLayers.Util.extend(this.options, newOptions);
        OpenLayers.Util.extend(this, newOptions);
    },
    
    /**
     * APIMethod: activate
     * Turn on the handler.
     *
     * Return:
     * {Boolean} The handler was successfully activated
     */
    activate: function() {
        var activated = false;
        if(OpenLayers.Handler.prototype.activate.apply(this, arguments)) {
            // create temporary vector layer for rendering geometry sketch
            var options = {
                displayInLayerSwitcher: false,
                // indicate that the temp vector layer will never be out of range
                // without this, resolution properties must be specified at the
                // map-level for this temporary layer to init its resolutions
                // correctly
                calculateInRange: function() { return true; },
                style : this.style
            };
            this.layer = new OpenLayers.Layer.Vector(this.CLASS_NAME, options);
            this.map.addLayer(this.layer);
            
            // rightclick handler
            // OL hack cos rightclick handler not captured
            Ext.fly(this.map.app.ui.getMapDiv()).on("contextmenu", 
              this.rightclick, this, {stopEvent : true, normalized : false});
            
            // css stuff
            OpenLayers.Element.addClass(this.map.viewPortDiv, "olDrawBox");
            
            activated = true;
        }
        return activated;
    },

    /**
     * APIMethod: deactivate
     * Turn off the handler.
     *
     * Return:
     * {Boolean} The handler was successfully deactivated
     */
    deactivate: function() {
        var deactivated = false;
        if(OpenLayers.Handler.prototype.deactivate.apply(this, arguments)) {
            // call the cancel callback if mid-drawing
            if(this.startedCircle) {
                this.cancel();
            }
            // If a layer's map property is set to null, it means that that
            // layer isn't added to the map. Since we ourself added the layer
            // to the map in activate(), we can assume that if this.layer.map
            // is null it means that the layer has been destroyed (as a result
            // of map.destroy() for example.
            if (this.layer.map != null) {
                this.layer.destroy(false);
                if (this.feature) {
                    this.feature.destroy();
                }
            }
            this.layer = null;
            this.feature = null;
            
            // rightclick handler
            // OL hack cos rightclick handler not captured
            Ext.fly(this.map.app.ui.getMapDiv()).un("contextmenu", 
              this.rightclick, this);

            // css stuff
            //OpenLayers.Element.removeClass( this.map.viewPortDiv, "olDrawBox" );

            deactivated = true;
        }
        return deactivated;
    },


    click: function(evt) {
        if (!this.startedCircle) {                      
          this.startedCircle = true;
          var maploc = this.map.getLonLatFromPixel(evt.xy);
          this.origin = new OpenLayers.Geometry.Point(maploc.lon, maploc.lat);
          // create the new polygon
          if(this.persist) {
              this.clear();
          }
          this.feature = new OpenLayers.Feature.Vector();
          this.radius = this.map.getResolution() * 2;
          this.createGeometry();
          this.callback("create", [this.origin, this.feature]);
          this.layer.addFeatures([this.feature], {silent: true});
          this.layer.drawFeature(this.feature, this.style);
        }
    },
    
    // rightclick handler    
    rightclick: function(evt) {
      if (this.startedCircle) {
        this.cancel(); 
      }
      else
        this.clear();
    },
    
    

    /**
     * Method: move
     * Respond to drag move events
     *
     * Parameters:
     * evt - {Evt} The move event
     */
    mousemove: function(evt) {
        if (this.startedCircle) {
          var maploc = this.map.getLonLatFromPixel(evt.xy);
          var point = new OpenLayers.Geometry.Point(maploc.lon, maploc.lat);
          this.calculateAngle(point, evt);
          this.radius = Math.max(this.map.getResolution() / 2,
                                     point.distanceTo(this.origin));
          this.modifyGeometry();
          this.layer.drawFeature(this.feature, this.style);
        }
        return false;
    },
    
    dblclick : function(evt) {
      if (this.startedCircle) { 
        var res = { 
          origin : this.origin, 
          radius : this.radius 
        };
        this.callback("done", [res]);
        this.startedCircle = false;
      }

      return false;
    },
    mouseup : null,
    
    
    
    /**
     * Method: createGeometry
     * Create the new polygon geometry.  This is called at the start of the
     *     drag and at any point during the drag if the number of sides
     *     changes.
     */
    createGeometry: function() {
        this.angle = Math.PI * ((1/this.sides) - (1/2));
        this.feature.geometry = OpenLayers.Geometry.Polygon.createRegularPolygon(
            this.origin, this.radius, this.sides, this.snapAngle
        );
    },
    
    /**
     * Method: modifyGeometry
     * Modify the polygon geometry in place.
     */
    modifyGeometry: function() {
        var angle, dx, dy, point;
        var ring = this.feature.geometry.components[0];
        // if the number of sides ever changes, create a new geometry
        if(ring.components.length != (this.sides + 1)) {
            this.createGeometry();
            ring = this.feature.geometry.components[0];
        }
        for(var i=0; i<this.sides; ++i) {
            point = ring.components[i];
            angle = this.angle + (i * 2 * Math.PI / this.sides);
            point.x = this.origin.x + (this.radius * Math.cos(angle));
            point.y = this.origin.y + (this.radius * Math.sin(angle));
            point.clearBounds();
        }
    },
    
    /**
     * Method: calculateAngle
     * Calculate the angle based on settings.
     *
     * Parameters:
     * point - {<OpenLayers.Geometry.Point>}
     * evt - {Event}
     */
    calculateAngle: function(point, evt) {
        var alpha = Math.atan2(point.y - this.origin.y,
                               point.x - this.origin.x);
        this.angle = alpha;
    },

    /**
     * APIMethod: cancel
     * Finish the geometry and call the "cancel" callback.
     */
    cancel: function() {
        // the polygon geometry gets cloned in the callback method
        this.callback("cancel", null);
        this.startedCircle = false;
        this.clear();
    },



    /**
     * APIMethod: clear
     * Clear any rendered features on the temporary layer.  This is called
     *     when the handler is deactivated, canceled, or done (unless persist
     *     is true).
     */
    clear: function() {
        this.layer.renderer.clear();
        this.layer.destroyFeatures();
    },
    
    /**
     * Method: callback
     * Trigger the control's named callback with the given arguments
     *
     * Parameters:
     * name - {String} The key for the callback that is one of the properties
     *     of the handler's callbacks object.
     * args - {Array} An array of arguments with which to call the callback
     *     (defined by the control).
     */
    callback: function (name, args) {
       if (this.callbacks[name]) {
            this.callbacks[name].apply(this.control,args);}

        // since sketch features are added to the temporary layer
        // they must be cleared here if done or cancel
        if(!this.persist && (name == "done" || name == "cancel")) {
            this.clear();
        }
    },

    CLASS_NAME: "Carmen.Handler.Circle"
});
