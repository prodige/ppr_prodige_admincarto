var app;
var mapProjection;

////////////////////////
// constantes
////////////////////////

init = function() {  
  Ext.tip.QuickTipManager.init();
  initApp(true);
};

// remove Loading mask
clearLoading = function() {
  Ext.get('loading-mask').fadeOut({
    remove :false,
    endOpacity: 0.0, // can be any value between 0 and 1 (e.g. .5)
    easing: 'easeOut',
    duration: 250, // duration in milliseconds
    useDisplay: true // for I.E.
  });
  Ext.get('loading').fadeOut({
    remove :false,
    endOpacity: 0.0, // can be any value between 0 and 1 (e.g. .5)
    easing: 'easeOut',
    duration: 500, // duration in milliseconds
    useDisplay: true // for I.E.
  });
};

initApp = function(clearLoading) {
  Carmen.application = app = new Descartes.Application();
  
 // app.loadContext(jsOws);
  
  //mapProjection = app.context.getProjection();
  //var maxExtent = app.context.getMaxExtent();
  //var extent = app.context.getExtent();
  //var map = app.getMap();
  if (clearLoading)
    window.clearLoading();
};
 

getDemoContext = function(url, ajaxMode) {
  if (!ajaxMode) {
    scriptTag = "<script src='" + url + "'></script>"; 
    document.write(scriptTag);
    app.loadContext(jsOws);
  }
};



/*
 * returns a json layer description from its name @Todo to be tested..
 */
var getLayer = function(jsonLayerList, layerName) {
  var layer = null;
  if (jsonLayerList instanceof Array) {
    var i = 0;
    while (i < jsonLayerList.length
        && jsonLayerList[i].attributes.name != layerName) {
      i++;
    }
    if (i < jsonLayerList.length) {
      layer = jsonLayerList[i];
    }
  } else {
    if (jsonLayerList.attributes.name != layerName) {
      layer = jsonLayerList;
    }
  }
  return layer;
};
