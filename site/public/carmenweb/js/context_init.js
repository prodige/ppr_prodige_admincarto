var jsOws = {
    "ViewContext": {
        "attributes": {
            "version": "1.1.0",
            "id": "Atlas ANRU Pays de la Loire_defaut_ctx",
            "schemaLocation": "http:\/\/www.opengeospatial.net\/context context.xsd"
        },
        "General": {
            "BoundingBox": {
                "attributes": {
                    "SRS": "EPSG:4326",
                    "minx0": "248373.75",
                    "miny0": "6548321.465",
                    "maxx0": "557627.95",
                    "maxy0": "6857575.665",
                    "minx": "320554.21182765",
                    "miny": "6746043.4263942",
                    "maxx": "393382.19764963",
                    "maxy": "6811549.9596346",
                    "crs": "+init=epsg:2154"
                }
            },
            "Title": "Atlas%20ANRU%20Pays%20de%20la%20Loire",
            "LogoURL": {
                "attributes": {
                    "width": "44",
                    "height": "25",
                    "format": "gif"
                },
                "OnlineResource": {
                    "attributes": {
                        "type": "simple",
                        "href": "http:\/\/prodige34.alkante.al:23000\/IHM\/IHM\/cartes\/PRRA.gif"
                    }
                }
            },
            "Extension": {
                "MaxBoundingBox": {
                    "attributes": {
                        "SRS": "EPSG:4326",
                        "minx0": "248373.75",
                        "miny0": "6548321.465",
                        "maxx0": "557627.95",
                        "maxy0": "6857575.665",
                        "minx": "248025.95804604",
                        "miny": "6740284.2002228",
                        "maxx": "495681.92969002",
                        "maxy": "6825587.923789",
                        "crs": "+init=epsg:2154"
                    }
                },
                "MaxScaleDenom": "10000000",
                "MinScaleDenom": "1",
                "tool_exportPdf": "advanced",
                "wms_srs": "epsg%3A27582",
                "AJOUT_COUCHE_WFS": "ON",
                "tool_info": "simple",
                "tool_legend": "ON",
                "mapSettings_backgroundColor": "cccee847",
                "tool_geobookmark": "ON",
                "tool_zoomToPlace": "ON",
                "mapSettings_inlay": "1",
                "tool_scale": "ON",
                "tool_referenceMap": "ON",
                "display_header": "ON",
                "mapSettings_scaleTxt": "1",
                "mapSettings_scaleImg": "1",
                "tool_buffer": "advanced",
                "mapSettings_layoutModels": "modele1.tpl%40L%C3%A9gende%20et%20carte%20de%20situation%20%C3%A0%20droite%3Bmodele2.tpl%40L%C3%A9gende%2C%20carte%20de%20situation%20et%20copyright%20%C3%A0%20gauche%3Bmodele3.tpl%40Grandes%20l%C3%A9gendes%3Bmodele4.tpl%40L%C3%A9gende%20%C3%A0%C2%A0gauche%2C%20carte%20de%20situation%20au%20dessus",
                "mapSettings_fontSize": "12",
                "tool_refresh": "ON",
                "tool_queryAttributes": "advanced",
                "mapSettings_copyrightTxt": "%28c%29%20Copyright",
                "mapSettings_font": "tahomabd",
                "mapSettings_logo": "1",
                "COMMENTAIRE": null,
                "header_model": "bandeau.html",
                "tool_symbology": "ON",
                "AJOUT_COUCHE": "ON",
                "tool_zoom": "simple",
                "EXT_THEME_COLOR": "olive",
                "mapSettings_inCatalogue": "OFF",
                "tool_exportImg": "advanced",
                "tool_pan": "simple",
                "tool_context": "advanced",
                "tool_annotation": "advanced",
                "tool_download": "OFF",
                "OUTIL_TRANSPARENCE": "ON",
                "mapSettings_copyright": "1",
                "tool_addLayerOGC": "ON",
                "tool_print": "simple",
                "tool_measure": "advanced",
                "tool_fitall": "simple",
                "mapSettings_fontColor": "4f21c7",
                "wms_title": "Fond%20de%20plan",
                "mapSettings_mapfile": "\/home\/devperso\/bfontaine\/prodige3.4\/prodige\/cartes\/Publication\/atlas_anru_r52.map",
                "mapSettings_outputFormat": "png",
                "ReferenceMap": {
                    "attributes": {
                        "width": "180",
                        "height": "100"
                    },
                    "BoundingBox": {
                        "attributes": {
                            "crs": "+init=epsg:2154"
                        },
                        "LowerCorner": "124672.07 6548321.465",
                        "UpperCorner": "681329.63 6857575.665"
                    },
                    "OnlineResource": {
                        "attributes": {
                            "href": "bundles/carmenweb/images/REFERENCE.PNG"
                        }
                    }
                },
                "mapSettings_backgroundTransparency": "ON",
                "LayerTree": {
                    "LayerTreeNode": {
                        "attributes": {
                            "type": "group",
                            "name": "root",
                            "open": "1",
                            "depth": "0",
                            "opacity": "1",
                            "forceOpacityControl": ""
                        },
                        "LayerTreeNode": [{
                                "attributes": {
                                    "type": "layer",
                                    "layerId": "0",
                                    "layerIdx": "0",
                                    "name": "OpenStreetMap : carte style 'd\u00e9faut'",
                                    "mapName": "layer28",
                                    "visible": "1",
                                    "displayClass": "1",
                                    "providerGrpId": "10",
                                    "depth": "2",
                                    "opacity": "1",
                                    "forceOpacityControl": "1"
                                }
                            
                        }]
                    }
                }
            }
        },
        "LayerList": {
            "Layer": [{
                "attributes": {
                    "name": "layer28",
                    "hidden": "0",
                    "queryable": "0"
                },
                "Extension": {
                    "wms_srs": "EPSG%3A2154",
                    "wms_onlineresource": "http%3A%2F%2Fosm.geobretagne.fr%2Fgwc01%2Fservice%2Fwmts",
                    "LAYER_TITLE": "OpenStreetMap%20%3A%20carte%20style%20%27d%C3%A9faut%27",
                    "wmts_wms_onlineresource": null,
                    "wmts_tileorigin": "-357823.2365%2C46112025%20-357823.2365%2C26074517%20-357823.2365%2C16055763%20-357823.2365%2C11046386%20-357823.2365%2C8541697%20-357823.2365%2C7289353%20-357823.2365%2C7289353%20-357823.2365%2C7289353%20-357823.2365%2C7289353%20-357823.2365%2C7289353%20-357823.2365%2C7250217%20-357823.2365%2C7230649%20-357823.2365%2C7230649%20-357823.2365%2C7235541%20-357823.2365%2C7233095%20-357823.2365%2C7231872%20-357823.2365%2C7231261%20-357823.2365%2C7230955%20-357823.2365%2C7230802%20-357823.2365%2C7230802%20-357823.2365%2C7230764%20-357823.2365%2C7230745",
                    "layerSettings_order": "23",
                    "layerSettings_wmscLayer": "ON",
                    "layerSettings_group": "Fonds%20de%20plan%5B%2B%5D",
                    "wmts_matrixids": "EPSG%3A2154%3A0%20EPSG%3A2154%3A1%20EPSG%3A2154%3A2%20EPSG%3A2154%3A3%20EPSG%3A2154%3A4%20EPSG%3A2154%3A5%20EPSG%3A2154%3A6%20EPSG%3A2154%3A7%20EPSG%3A2154%3A8%20EPSG%3A2154%3A9%20EPSG%3A2154%3A10%20EPSG%3A2154%3A11%20EPSG%3A2154%3A12%20EPSG%3A2154%3A13%20EPSG%3A2154%3A14%20EPSG%3A2154%3A15%20EPSG%3A2154%3A16%20EPSG%3A2154%3A17%20EPSG%3A2154%3A18%20EPSG%3A2154%3A19%20EPSG%3A2154%3A20%20EPSG%3A2154%3A21",
                    "wmts_style": "_null",
                    "wmts_resolutions": "156543.033928041%2078271.51696402048%2039135.758482010235%2019567.87924100512%209783.93962050256%204891.96981025128%202445.98490512564%201222.99245256282%20611.49622628141%20305.74811314070485%20152.8740565703525%2076.43702828517624%2038.21851414258813%2019.10925707129406%209.554628535647032%204.777314267823516%202.388657133911758%201.194328566955879%200.5971642834779395%200.2985821417389697%200.1492910708694849%200.07464553543474242",
                    "wms_format": "image%2Fpng",
                    "layerSettings_legendScaleDisplay": "ON",
                    "wmts_wms_layer": "osm%3Amap",
                    "wms_server_version": "1.1.1",
                    "wmts_tileset": "EPSG%3A2154",
                    "layerSettings_ActivThemes": "ON",
                    "layerSettings_isWmts": "YES",
                    "layerSettings_symbolScaleDenom": "-1",
                    "hasLabel": null,
                    "wms_layer": "1"
                },
                "Title": "OpenStreetMap%20%3A%20carte%20style%20%27d%C3%A9faut%27",
                "Server": {
                    "attributes": {
                        "service": "urn:ogc:serviceType:WMS",
                        "version": "1.1.1",
                        "title": ""
                    },
                    "OnlineResource": {
                        "attributes": {
                            "type": "simple",
                            "href": "http:\/\/osm.geobretagne.fr\/gwc01\/service\/wmts"
                        }
                    }
                },
                "Name": "OpenStreetMap",
                "FormatList": {
                    "attributes": {
                        "current": "1"
                    },
                    "Format": "image\/png"
                }
            }] 
        }
    }
};
var CARMEN_URL_SERVER_DATA = './';
var mapfile = 'demo_brgm.map';
var mapfile_name = 'demo_brgm.map';
var CARMEN_URL_SERVER_FRONT = './';
var IHMUrl = './IHM/IHM/cartes/';
var CARMEN_DEBUG = '1';
var overloadExtTheme = 'olive';
var CARMEN_URL_ADMIN_CARTO = './';
var CARMEN_URL_PATH_DATA = './';
var CARMEN_URL_SERVER_DOWNLOAD = './';
var PRODIGE_VERIFY_RIGHTS_URL = './';
var CARMEN_URL_CATALOGUE = './';
var PRO_REQUETEUR_NB_LIMIT_OBJ = '40';
var tabContextParam = new Array(['one_day', 'une journée'], ['seven_days', 'une semaine'], ['one_month', 'un mois'], ['permanent', 'permanente']);
var tabEPSG = new Array(['2154', 'RGF93/Lambert 93'], ['4171', 'RGF93 2D'], ['3942', 'Lambert 93 Conique conforme 1'], ['3943', 'Lambert 93 Conique conforme 2'], ['3944', 'Lambert 93 Conique conforme 3'], ['3945', 'Lambert 93 Conique conforme 4'], ['3946', 'Lambert 93 Conique conforme 5'], ['3947', 'Lambert 93 Conique conforme 6'], ['3948', 'Lambert 93 Conique conforme 7'], ['3949', 'Lambert 93 Conique conforme 8'], ['3950', 'Lambert 93 Conique conforme 9'], ['27571', 'NTF(Paris) / Lambert zone 1'], ['27572', 'NTF(Paris) / Lambert zone 2'], ['27573', 'NTF(Paris) / Lambert zone 3'], ['27574', 'NTF(Paris) / Lambert zone 4'], ['27561', 'NTF(Paris) / Lambert Nord France'], ['27562', 'NTF(Paris) / Lambert centre France'], ['27563', 'NTF(Paris) / Lambert Sud France'], ['27564', 'NTF(Paris) / Lambert Corse'], ['4258', 'ETRS89 (INSPIRE)'], ['32620', 'WGS84 / UTM 20 Nord'], ['2972', 'RGFG95 / UTM 22 Nord'], ['2975', 'RGR92 / UTM 40 Nord'], ['2971', 'CSG 67 / UTM 22 Nord'], ['3312', 'CSG 67 / UTM 21 Nord'], ['2973', 'Fort Desaix / UTM 20'], ['4625', 'Fort Desaix'], ['3727', 'Piton des neiges / Gauss Laborde Reunion'], ['2987', 'IGN1950 / UTM 21 Nord'], ['32738', 'WGS84 / UTM 38 Sud'], ['32301', 'WGS72 / UTM 1 Sud'], ['3296', 'RGPF / UTM 5 Sud'], ['3297', 'RGPF / UTM 6 Sud'], ['2980', 'Combani 1950 / UTM 38 sud'], ['3163', 'RGNC91-93 / Lambert New Caledoni'], ['3170', 'RGNC91-93 / UTM 58'], ['3171', 'RGNC91-93 / UTM 59'], ['32630', 'WGS84 / UTM 30 Nord'], ['32631', 'WGS84 / UTM 31 Nord'], ['32632', 'WGS84 / UTM 32 Nord']);;

