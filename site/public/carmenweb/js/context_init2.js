var jsOws = {
"OWSContext": {
   "General": {
      "BoundingBox": {
         "LowerCorner": "92852.540532145 6679151.9936135",
         "UpperCorner": "403755.61800533 6912134.5002914",
         "attributes": {
            "crs": "+init=epsg:2154"
         }
      },
      "MaxBoundingBox": {
         "LowerCorner": "92852.540532145 6679151.9936135",
         "UpperCorner": "403755.61800533 6912134.5002914",
         "attributes": {
            "crs": "+init=epsg:2154"
         }
      },
      "Title": "R%C3%A9seau%20NATURA%202000",
      "Extension": {
//            "display_header": "ON",
//            "header_model": "bandeau.html",
//            "inspire_flag": "1",

         "IHM_MEDD_PARAMETRE": "7%2C8%2C9%2C10%2C11%2C12%2C13%2C14%2C15",
         "FICHE": "HECTARES%7CSuperficie%20%28ha%29%7CTXT%7C%7C1%3BEUROPE%7CSite%20Natura%202000%20de%20Bretagne%7CURL%7C%7C1",
         "EXT_THEME_COLOR": "green",
//            "RESUME": "ZONE%7CNom%7CTXT%7C%7C1",
//            "OUTIL_TRANSPARENCE": "ON",
         "MINIFICHE": "ZONE%7CNom%7CTXT%7C",
//            "MINSCALEDENOM": "50",
         "CHANGEMENT_RESOLUTION": "OFF",
//            "MAXSCALEDENOM": "1e%2B07",

//            "mapSettings_pwd": "",
         "mapSettings_backgroundColor": "ffffff4c",
//         "mapSettings_css": "interface_verte_v15.css",
//         "mapSettings_inCatalogue": "ON",
         "mapSettings_mapfile": "/mnt/data_carmen/BRE/Publication/Natura_2000.map",
         "mapSettings_outputFormat": "png",
         "mapSettings_fontColor": "000000",
//         "mapSettings_copyright": "1",
//         "mapSettings_copyrightTxt": "%C2%A9%20MEDDM%2C%20DREAL%20Bretagne%2C%20IGN",
         "mapSettings_font": "Arial",
         "mapSettings_inlay": "1",
//         "mapSettings_scaleTxt": "0",
//         "mapSettings_scaleImg": "1",
         "mapSettings_fontSize": "6",
         "mapSettings_layoutModels": "modele1.tpl%40L%C3%A9gende%20et%20carte%20de%20situation%20%C3%A0%20droite%3Bmodele2.tpl%40L%C3%A9gende%2C%20carte%20de%20situation%20et%20copyright%20%C3%A0%20gauche%3Bmodele3.tpl%40Grandes%20l%C3%A9gendes%3Bmodele4.tpl%40L%C3%A9gende%20%C3%A0%C2%A0gauche%2C%20carte%20de%20siuation%20au%20dessus",
//         "mapSettings_logo": "1",
//         "mapSettings_logoPath": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/./bretagne_dreal_mini.jpg",

//         "tool_exportPdf": "ON",
//         "tool_info": "ON",
//         "tool_legend": "ON",
//         "tool_geobookmark": "OFF",
//         "tool_zoomToPlace": "ON",
//         "tool_scale": "ON",
//         "tool_referenceMap": "ON",
         "tool_zoomToPlaceConfig": "D%C3%A9partement%7Clayer6%7CINSEE_D%C3%A9partement%7CNom_D%C3%A9partement%7C1%3BCommune%7Clayer9%7CINSEE_Commune%7CNom_Commune%7C1%7C1%7CINSEE_D%C3%A9partement",
//         "tool_buffer": "ON",
//         "tool_refresh": "ON",
//         "tool_pan": "ON",
//         "tool_context": "OFF",
//         "tool_annotation": "OFF",
//         "tool_download": "ON",
//         "tool_addLayer": "ON",
//         "tool_zoom": "ON",
//         "tool_queryAttributes": "ON",
//         "tool_symbology": "ON",
//         "tool_exportImg": "ON",
//         "tool_print": "ON",
//         "tool_measure": "ON",
//         "tool_fitall": "ON",

         "metadata_wms_uuid": "47510621-388a-4e69-935e-4fdcb6fd7c88",
         "wms_abstract": "",
         "wms_stateorprovince": "",
         "wms_contactvoicetelephone": "",
         "wms_contactposition": "",
         "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
         "wms_keywordlist_ISO_items": "infoMapAccessService%2CWMS%201.1.1%2CWMS%201.3.0%2CSLD%201.1.0",
         "wms_contactorganization": "",
         "wms_contactfacsimiletelephone": "",
         "wms_contactperson": "",
         "wms_keywordlist_vocabulary": "ISO",
         "wms_address": "",
         "wms_contactelectronicmailaddress": "",
         "wms_country": "",
         "wms_fees": "",
         "wms_languages": "fre",
         "wms_accessconstraints": "Aucun%20obstacle%20technique",
         "wms_addresstype": "postal",
         "wms_city": "",
         "wms_postcode": "",

         "metadata_wfs_uuid": "5503588b-b64d-435f-9a9b-13ca7d3d38e4",
         "wfs_contactperson": "",
         "wfs_keywordlist": "infoFeatureAccessService",
         "wfs_city": "",
         "wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
         "wfs_addresstype": "postal",
         "wfs_contactposition": "",
         "wfs_contactelectronicmailaddress": "",
         "wfs_stateorprovince": "",
         "wfs_postcode": "",
         "wfs_languages": "fre",
         "wfs_contactvoicetelephone": "",
         "wfs_contactorganization": "",
         "wfs_address": "",
         "wfs_contactfacsimiletelephone": "",
         "wfs_title": "WFS%20R%C3%A9seau%20NATURA%202000",
         "wfs_country": "",
         "wfs_abstract": "",
         "wfs_enable_request": "%2A",
         "wfs_fees": "",
         "wfs_accessconstraints": "Aucun%20obstacle%20technique",


         "metadata_atom_uuid": "9fef8678-728d-4c2a-9278-0c9c79caaf1f",

         "ReferenceMap": {
            "BoundingBox": {
               "LowerCorner": "-5428.3 2244840",
               "UpperCorner": "402821 2471650",
               "attributes": {
                  "crs": "+init=epsg:2154"
               }
            },
            "OnlineResource": {
               "attributes": {
                  "href": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/./reference/reference_11.png"
               }
            },
            "attributes": {
               "width": "180",
               "height": "100"
            }
         },
         "LayerTree": {
            "LayerTreeNode": {
               "LayerTreeNode": [
                  {
                     "LayerTreeNode": [
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "10",
                              "layerIdx": "10",
                              "name": "Directive habitats (ZSC,SIC,pSIC)",
                              "mapName": "layer10",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "0",
                              "depth": "2",
                              "opacity": "0.6",
                              "forceOpacityControl": ""
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "9",
                              "layerIdx": "9",
                              "name": "Directive oiseaux (ZPS)",
                              "mapName": "layer11",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "0",
                              "depth": "2",
                              "opacity": "0.4",
                              "forceOpacityControl": ""
                           }
                        }
                     ],
                     "attributes": {
                        "type": "group",
                        "name": "Natura 2000|Nat_0",
                        "open": "1",
                        "depth": "1",
                        "opacity": "1",
                        "forceOpacityControl": "1"
                     }
                  },
                  {
                     "LayerTreeNode": [
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "8",
                              "layerIdx": "8",
                              "name": "Commune",
                              "mapName": "layer9",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "1",
                              "depth": "2",
                              "opacity": "1",
                              "forceOpacityControl": ""
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "7",
                              "layerIdx": "7",
                              "name": "Sous-préfecture",
                              "mapName": "layer8",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "1",
                              "depth": "2",
                              "opacity": "1",
                              "forceOpacityControl": ""
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "6",
                              "layerIdx": "6",
                              "name": "Préfecture",
                              "mapName": "layer7",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "1",
                              "depth": "2",
                              "opacity": "1",
                              "forceOpacityControl": ""
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "5",
                              "layerIdx": "5",
                              "name": "Département",
                              "mapName": "layer6",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "1",
                              "depth": "2",
                              "opacity": "1",
                              "forceOpacityControl": ""
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "4",
                              "layerIdx": "4",
                              "name": "Autres régions",
                              "mapName": "layer5",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "1",
                              "depth": "2",
                              "opacity": "1",
                              "forceOpacityControl": ""
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "3",
                              "layerIdx": "3",
                              "name": "Mer",
                              "mapName": "layer4",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "1",
                              "depth": "2",
                              "opacity": "1",
                              "forceOpacityControl": ""
                           }
                        }
                     ],
                     "attributes": {
                        "type": "group",
                        "name": "Données administratives|Don_0",
                        "open": "",
                        "depth": "1",
                        "opacity": "1",
                        "forceOpacityControl": "1"
                     }
                  },
                  {
                     "LayerTreeNode": [
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "2",
                              "layerIdx": "2",
                              "name": "© IGN-SCAN100",
                              "mapName": "layer12",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "2",
                              "depth": "2",
                              "opacity": "0.6",
                              "forceOpacityControl": "1"
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "1",
                              "layerIdx": "1",
                              "name": "© IGN-SCAN25",
                              "mapName": "layer15",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "3",
                              "depth": "2",
                              "opacity": "0.6",
                              "forceOpacityControl": "1"
                           }
                        },
                        {
                           "attributes": {
                              "type": "layer",
                              "layerId": "0",
                              "layerIdx": "0",
                              "name": "© IGN-BD Ortho",
                              "mapName": "layer14",
                              "visible": "1",
                              "displayClass": "1",
                              "providerGrpId": "4",
                              "depth": "2",
                              "opacity": "0.75",
                              "forceOpacityControl": "1"
                           }
                        }
                     ],
                     "attributes": {
                        "type": "group",
                        "name": "Fonds de plan|Fon_0",
                        "open": "1",
                        "depth": "1",
                        "opacity": "1",
                        "forceOpacityControl": "1"
                     }
                  }
               ],
               "attributes": {
                  "type": "group",
                  "name": "root",
                  "open": "1",
                  "depth": "0",
                  "opacity": "1",
                  "forceOpacityControl": ""
               }
            }
         },
         "LayerTreePrint": {
            "LayerTreeNode": {
               "LayerTreeNode": [
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "1",
                        "layerIdx": "10",
                        "name": "Directive habitats (ZSC,SIC,pSIC)",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "2",
                        "layerIdx": "9",
                        "name": "Directive oiseaux (ZPS)",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "3",
                        "layerIdx": "8",
                        "name": "Commune",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "4",
                        "layerIdx": "7",
                        "name": "Sous-préfecture",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "5",
                        "layerIdx": "6",
                        "name": "Préfecture",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "6",
                        "layerIdx": "5",
                        "name": "Département",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "7",
                        "layerIdx": "4",
                        "name": "Autres régions",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "8",
                        "layerIdx": "3",
                        "name": "Mer",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "9",
                        "layerIdx": "2",
                        "name": "© IGN-SCAN100",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "10",
                        "layerIdx": "1",
                        "name": "© IGN-SCAN25",
                        "visible": ""
                     }
                  },
                  {
                     "attributes": {
                        "type": "layer",
                        "layerId": "11",
                        "layerIdx": "0",
                        "name": "© IGN-BD Ortho",
                        "visible": ""
                     }
                  }
               ],
               "attributes": {
                  "type": "group",
                  "name": "root",
                  "open": "1"
               }
            }
         },
         "Labels": {
            "Layer": [
               {
                  "Title": "layer7",
                  "MinScaleDenominator": "250000",
                  "MaxScaleDenominator": "10000000",
                  "StyleList": {
                     "Style": {
                        "Name": "Préfecture",
                        "Title": "",
                        "LegendURL": {
                           "attributes": {
                              "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                              "mapfile": "Natura_2000",
                              "layer": "layer7",
                              "classIdx": "0"
                           }
                        }
                     }
                  },
                  "Extension": {
                     "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
      //               "LAYER_TITLE": "Pr%C3%A9fecture",
      //               "layerSettings_order": "5",
                     "geoide_WFS_STATUS": "ON",
                     "ows_include_items": "all",
      //               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
      //               "layerSettings_wfsStatus": "ON",
                     "wms_name": "Prefecture",
                     "geoide_WMS_STATUS": "ON",
                     "wfs_name": "Prefecture",
      //               "layerSettings_wmsStatus": "ON",
                     "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "geoide_gml_include_items": "all",
                     "gml_include_items": "all",
      //               "layerSettings_ActivThemes": "ON",
      //               "LAYER_ID": "lay_6",
                     "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "wms_title": "Pr%C3%A9fecture"
                  },
                  "DataURL": {
                     "OnLineResource": {
                        "attributes": {
                           "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                        }
                     }
                  },
                  "attributes": {
                     "name": "layer7",
                     "queryable": "",
                     "hidden": "0",
                     "hasLabel": "1"
                  }
               },
               {
                  "Title": "layer8",
                  "MinScaleDenominator": "50000",
                  "MaxScaleDenominator": "10000000",
                  "StyleList": {
                     "Style": {
                        "Name": "Sous-préfectures",
                        "Title": "",
                        "LegendURL": {
                           "attributes": {
                              "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                              "mapfile": "Natura_2000",
                              "layer": "layer8",
                              "classIdx": "0"
                           }
                        }
                     }
                  },
                  "Extension": {
      //               "LAYER_ID": "lay_7",
      //               "LAYER_TITLE": "Sous-pr%C3%A9fecture",
      //               "layerSettings_order": "4",
      //               "layerSettings_queryURL": "",
      //               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
      //               "layerSettings_download": "OFF",
      //               "layerSettings_infoFields": "",
      //               "layerSettings_wmsStatus": "ON",
      //               "layerSettings_briefFields": "",
      //               "layerSettings_wfsStatus": "ON",
      //               "layerSettings_ActivThemes": "ON",
      //               "layerSettings_ToolTipFields": "",
                     "geoide_WMS_STATUS": "ON",
                     "geoide_WFS_STATUS": "ON",
                     "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "geoide_gml_include_items": "all",
                     "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "ows_include_items": "all",
                     "gml_include_items": "all",
                     "wms_name": "Sous_prefecture",
                     "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "wms_title": "Sous-pr%C3%A9fecture",
                     "wfs_name": "Sous_prefecture"
                   },
                  "DataURL": {
                     "OnLineResource": {
                        "attributes": {
                           "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                        }
                     }
                  },
                  "attributes": {
                     "name": "layer8",
                     "queryable": "",
                     "hidden": "0",
                     "hasLabel": "1"
                  }
               },
               {
                  "Title": "layer9",
                  "MinScaleDenominator": "60000",
                  "MaxScaleDenominator": "150000",
                  "StyleList": {
                     "Style": {
                        "Name": "Commune",
                        "Title": "",
                        "LegendURL": {
                           "attributes": {
                              "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                              "mapfile": "Natura_2000",
                              "layer": "layer9",
                              "classIdx": "0"
                           }
                        }
                     }
                  },
                  "Extension": {
                     "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
      //               "LAYER_TITLE": "Commune",
      //               "layerSettings_order": "3",
                     "geoide_WFS_STATUS": "ON",
                     "ows_include_items": "all",
      //               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
      //               "layerSettings_wfsStatus": "ON",
                     "wms_name": "Commune",
                     "geoide_WMS_STATUS": "ON",
                     "wfs_name": "Commune",
      //               "layerSettings_Tiled": "OFF",
      //               "layerSettings_wmsStatus": "ON",
                     "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "geoide_gml_include_items": "all",
                     "gml_include_items": "all",
      //               "layerSettings_ActivThemes": "ON",
      //               "LAYER_ID": "lay_8",
                     "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
                     "wms_title": "Commune"
                  },
                  "DataURL": {
                     "OnLineResource": {
                        "attributes": {
                           "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                        }
                     }
                  },
                  "attributes": {
                     "name": "layer9",
                     "queryable": "",
                     "hidden": "0",
                     "hasLabel": "1"
                  }
               }
            ]
         }
      }
   },
   "ResourceList": {
      "Layer": [
         {
            "Title": "layer14",
            "MinScaleDenominator": "50",
            "MaxScaleDenominator": "15000",
            "Extension": {
               "MINSCALEDENOM": "50",
               "wms_srs": "EPSG%3A2154",
               "wms_onlineresource": "http%3A%2F%2Fmapsref.brgm.fr%2FWMS%2Fmapserv%3Fmap%3D%2Fcarto%2FRefCom%2FmapFiles%2FFXX_RefIGN-RGF.map",
//               "LAYER_TITLE": "%C2%A9%20IGN-BD%20Ortho",
//               "layerSettings_order": "11",
//               "layerSettings_group": "Fonds%20de%20plan%7CFon_0%5B%2B%5D",
//               "layerSettings_download": "OFF",
               "wms_name": "ORTHO",
               "wms_format": "image%2Fpng",
//               "layerSettings_legendScaleDisplay": "OFF",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_server_version": "1.1.1",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_0",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154"
            },
            "Server": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://mapsref.brgm.fr/WMS/mapserv?map=/carto/RefCom/mapFiles/FXX_RefIGN-RGF.map"
                  }
               },
               "attributes": {
                  "service": "urn:ogc:serviceType:WMS",
                  "version": "1.1.1"
               }
            },
            "attributes": {
               "name": "layer14",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer15",
            "MinScaleDenominator": "10000",
            "MaxScaleDenominator": "50000",
            "Extension": {
               "wms_srs": "EPSG%3A2154",
               "wms_onlineresource": "http%3A%2F%2Fmapsref.brgm.fr%2FWMS%2Fmapserv%3Fmap%3D%2Fcarto%2FRefCom%2FmapFiles%2FFXX_RefIGN-RGF.map",
//               "LAYER_TITLE": "%C2%A9%20IGN-SCAN25",
//               "layerSettings_order": "10",
//               "layerSettings_group": "Fonds%20de%20plan%7CFon_0%5B%2B%5D",
//               "layerSettings_download": "OFF",
               "wms_name": "SCAN25TOPO",
               "wms_format": "image%2Fpng",
//               "layerSettings_legendScaleDisplay": "OFF",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_server_version": "1.1.1",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_1",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154"
            },
            "Server": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://mapsref.brgm.fr/WMS/mapserv?map=/carto/RefCom/mapFiles/FXX_RefIGN-RGF.map"
                  }
               },
               "attributes": {
                  "service": "urn:ogc:serviceType:WMS",
                  "version": "1.1.1"
               }
            },
            "attributes": {
               "name": "layer15",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer12",
            "MinScaleDenominator": "45000",
            "MaxScaleDenominator": "200000",
            "Extension": {
               "wms_srs": "EPSG%3A2154",
               "wms_onlineresource": "http%3A%2F%2Fmapsref.brgm.fr%2FWMS%2Fmapserv%3Fmap%3D%2Fcarto%2FRefCom%2FmapFiles%2FFXX_RefIGN-RGF.map",
//               "LAYER_TITLE": "%C2%A9%20IGN-SCAN100",
//               "layerSettings_order": "9",
//               "layerSettings_group": "Fonds%20de%20plan%7CFon_0%5B%2B%5D",
//               "layerSettings_download": "OFF",
               "wms_name": "SCAN100",
               "wms_format": "image%2Fpng",
//               "layerSettings_legendScaleDisplay": "OFF",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_server_version": "1.1.1",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_2",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154"
            },
            "Server": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://mapsref.brgm.fr/WMS/mapserv?map=/carto/RefCom/mapFiles/FXX_RefIGN-RGF.map"
                  }
               },
               "attributes": {
                  "service": "urn:ogc:serviceType:WMS",
                  "version": "1.1.1"
               }
            },
            "attributes": {
               "name": "layer12",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer4",
            "MinScaleDenominator": "60000",
            "MaxScaleDenominator": "10000000",
            "StyleList": {
               "Style": {
                  "Name": "Mer",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer4",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "Mer",
//               "layerSettings_order": "8",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "all",
//               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Mer",
               "geoide_WMS_STATUS": "ON",
               "wfs_name": "Mer",
//               "layerSettings_wmsStatus": "ON",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "geoide_gml_include_items": "all",
               "gml_include_items": "all",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_3",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_title": "Mer"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer4",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer5",
            "MinScaleDenominator": "200000",
            "MaxScaleDenominator": "10000000",
            "StyleList": {
               "Style": {
                  "Name": "Autres régions",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer5",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "Autres%20r%C3%A9gions",
//               "layerSettings_order": "7",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "all",
//               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Autres_regions",
               "geoide_WMS_STATUS": "ON",
               "wfs_name": "Autres_regions",
//               "layerSettings_wmsStatus": "ON",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "geoide_gml_include_items": "all",
               "gml_include_items": "all",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_4",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_title": "Autres%20r%C3%A9gions"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer5",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer6",
            "MinScaleDenominator": "200000",
            "MaxScaleDenominator": "10000000",
            "StyleList": {
               "Style": {
                  "Name": "Département",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer6",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "D%C3%A9partement",
//               "layerSettings_order": "6",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "all",
//               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Departement",
               "geoide_WMS_STATUS": "ON",
               "wfs_name": "Departement",
//               "layerSettings_Tiled": "OFF",
//               "layerSettings_wmsStatus": "ON",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "geoide_gml_include_items": "all",
               "gml_include_items": "all",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_5",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_title": "D%C3%A9partement"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer6",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer7",
            "MinScaleDenominator": "250000",
            "MaxScaleDenominator": "10000000",
            "StyleList": {
               "Style": {
                  "Name": "Préfecture",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer7",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "Pr%C3%A9fecture",
//               "layerSettings_order": "5",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "all",
//               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Prefecture",
               "geoide_WMS_STATUS": "ON",
               "wfs_name": "Prefecture",
//               "layerSettings_wmsStatus": "ON",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "geoide_gml_include_items": "all",
               "gml_include_items": "all",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_6",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_title": "Pr%C3%A9fecture"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer7",
               "queryable": "",
               "hidden": "0",
               "hasLabel": "1"
            }
         },
         {
            "Title": "layer8",
            "MinScaleDenominator": "50000",
            "MaxScaleDenominator": "250000",
            "StyleList": {
               "Style": {
                  "Name": "Sous-préfectures",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer8",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "Sous-pr%C3%A9fecture",
//               "layerSettings_order": "4",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "all",
//               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
//               "layerSettings_download": "OFF",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Sous_prefecture",
               "geoide_WMS_STATUS": "ON",
//               "layerSettings_queryURL": "",
               "wfs_name": "Sous_prefecture",
//               "layerSettings_infoFields": "",
//               "layerSettings_wmsStatus": "ON",
//               "layerSettings_briefFields": "",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "geoide_gml_include_items": "all",
               "gml_include_items": "all",
//               "layerSettings_ActivThemes": "ON",
//               "layerSettings_ToolTipFields": "",
//               "LAYER_ID": "lay_7",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_title": "Sous-pr%C3%A9fecture"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer8",
               "queryable": "",
               "hidden": "0",
               "hasLabel": "1"
            }
         },
         {
            "Title": "layer9",
            "MinScaleDenominator": "100000",
            "MaxScaleDenominator": "500000",
            "StyleList": {
               "Style": {
                  "Name": "Commune",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer9",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "Commune",
//               "layerSettings_order": "3",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "all",
//               "layerSettings_group": "Donn%C3%A9es%20administratives%7CDon_0%5B-%5D",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Commune",
               "geoide_WMS_STATUS": "ON",
               "wfs_name": "Commune",
//               "layerSettings_Tiled": "OFF",
//               "layerSettings_wmsStatus": "ON",
               "geoide_wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "geoide_gml_include_items": "all",
               "gml_include_items": "all",
//               "layerSettings_ActivThemes": "ON",
//               "LAYER_ID": "lay_8",
               "geoide_wfs_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "wms_title": "Commune"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer9",
               "queryable": "",
               "hidden": "0",
               "hasLabel": "1"
            }
         },
         {
            "Title": "layer11",
            "MinScaleDenominator": "5000",
            "MaxScaleDenominator": "2500000",
            "StyleList": {
               "Style": {
                  "Name": "Directive oiseaux",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer11",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "gml_EUROPE_alias": "EUROPE",
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
//               "LAYER_TITLE": "Directive%20oiseaux%20%28ZPS%29",
//               "layerSettings_order": "2",
               "gml_DESCRIPTIO_alias": "DESCRIPTIO",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "EUROPE%2CZONE%2CHECTARES",
//               "layerSettings_group": "Natura%202000%7CNat_0%5B%2B%5D",
//               "layerSettings_download": "ON",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Directive_oiseaux__ZPS_",
               "geoide_WMS_STATUS": "ON",
               "gml_ZONE_alias": "ZONE",
//               "layerSettings_queryURL": "",
               "wfs_name": "Directive_oiseaux__ZPS_",
//               "layerSettings_infoFields": "HECTARES%7CSuperficie%20%28ha%29%7CTXT%7C%7C%3BEUROPE%7CSite%20Natura%202000%20de%20Bretagne%7CURL%7Chttp%3A%2F%2Finpn.mnhn.fr%2Fsite%2Fnatura2000%2F%3CEUROPE%3E%7C",
//               "layerSettings_Tiled": "OFF",
               "gml_HECTARES_alias": "HECTARES",
//               "layerSettings_wmsStatus": "ON",
//               "layerSettings_briefFields": "ZONE%7CNom%7CTXT%7C%7C",
               "gml_PROCEDUREs_alias": "PROCEDURE_",
               "geoide_wms_srs": "EPSG%3A27582%20EPSG%3A2154%20EPSG%3A4326%20EPSG%3A4258%20CRS%3A84%20EPSG%3A3857",
               "geoide_gml_include_items": "EUROPE%2CZONE%2CHECTARES",
               "gml_include_items": "all",
////               "layerSettings_MetadataFile": "Fiche_metadata_html%2FZPS_BZH.html",
//               "layerSettings_ActivThemes": "ON",
//               "layerSettings_ToolTipFields": "ZONE%7CNom%7CTXT%7C%7C%7CTITLE",
//               "LAYER_ID": "lay_10",
               "geoide_wfs_srs": "EPSG%3A27582%20EPSG%3A2154%20EPSG%3A4326%20EPSG%3A4258%20CRS%3A84%20EPSG%3A3857",
               "wms_title": "Directive%20oiseaux%20%28ZPS%29",
//               "layerSettings_Metadata": "http://metadata.carmen.developpement-durable.gouv.fr/geosource/10/fre/find?uuid=ddd7da8e-8275-40db-ab39-b5a3178ab570"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer11",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         },
         {
            "Title": "layer10",
            "MinScaleDenominator": "5000",
            "MaxScaleDenominator": "2500000",
            "StyleList": {
               "Style": {
                  "Name": "Directive habitats",
                  "Title": "",
                  "LegendURL": {
                     "attributes": {
                        "server": "http://carmen.developpement-durable.gouv.fr/IHM/IHM/BRE/LEGEND/",
                        "mapfile": "Natura_2000",
                        "layer": "layer10",
                        "classIdx": "0"
                     }
                  }
               }
            },
            "Extension": {
               "wms_srs": "EPSG%3A3857%20CRS%3A84%20EPSG%3A4258%20EPSG%3A4326%20EPSG%3A2154",
               "gml_CODE_alias": "CODE",
//               "LAYER_TITLE": "Directive%20habitats%20%28ZSC%2CSIC%2CpSIC%29",
//               "layerSettings_order": "1",
               "geoide_WFS_STATUS": "ON",
               "ows_include_items": "CODE_EUROP%2CNOM%2CSURF_HA%2CAAMP_MAIA",
//               "layerSettings_group": "Natura%202000%7CNat_0%5B%2B%5D",
//               "layerSettings_download": "ON",
//               "layerSettings_wfsStatus": "ON",
               "wms_name": "Directive_habitats__ZSC_SIC_pSIC_",
               "gml_NOM_alias": "NOM",
               "geoide_WMS_STATUS": "ON",
//               "layerSettings_queryURL": "",
               "wfs_name": "Directive_habitats__ZSC_SIC_pSIC_",
//               "layerSettings_Tiled": "OFF",
//               "layerSettings_infoFields": "CODE_EUROP%7CSite%20Natura%202000%20de%20Bretagne%7CURL%7Chttp%3A%2F%2Finpn.mnhn.fr%2Fsite%2Fnatura2000%2F%3CCODE_EUROP%3E%7C%3BAAMP_MAIA%7CAire%20marine%20prot%C3%A9g%C3%A9e%7CURL%7CAAMP_MAIA%7C",
//               "layerSettings_wmsStatus": "ON",
//               "layerSettings_briefFields": "NOM%7CNom%7CTXT%7C%7C",
               "gml_CODE_EUROP_alias": "CODE_EUROP",
               "geoide_wms_srs": "EPSG%3A27582%20EPSG%3A2154%20EPSG%3A4326%20EPSG%3A4258%20CRS%3A84%20EPSG%3A3857",
               "geoide_gml_include_items": "CODE_EUROP%2CNOM%2CSURF_HA%2CAAMP_MAIA",
               "gml_include_items": "all",
////               "layerSettings_MetadataFile": "Fiche_metadata_html%2FZSC_BZH.html",
//               "layerSettings_ActivThemes": "ON",
//               "layerSettings_ToolTipFields": "NOM%7CNom%7CTXT%7C%7C%7CTITLE",
//               "LAYER_ID": "lay_9",
               "gml_SURFACE_HA_alias": "SURFACE_HA",
               "geoide_wfs_srs": "EPSG%3A27582%20EPSG%3A2154%20EPSG%3A4326%20EPSG%3A4258%20CRS%3A84%20EPSG%3A3857",
               "wms_title": "Directive%20habitats%20%28ZSC%2CSIC%2CpSIC%29",
//               "layerSettings_Metadata": "http://metadata.carmen.developpement-durable.gouv.fr/geosource/10/fre/find?uuid=0b4fe074-5827-47c6-b00e-1c74613ba9cf"
            },
            "DataURL": {
               "OnLineResource": {
                  "attributes": {
                     "url": "http://ws.carmen.developpement-durable.gouv.fr/cgi-bin/mapserv?map=/mnt/data_carmen/BRE/Publication/Natura_2000.map"
                  }
               }
            },
            "attributes": {
               "name": "layer10",
               "queryable": "",
               "hidden": "0",
               "hasLabel": ""
            }
         }
      ]
   },
   "attributes": {
      "version": "0.3.0",
      "id": "Réseau NATURA 2000_defaut_ctx"
   }
}
};

jsOws.ViewContext = jsOws.OWSContext;


var CARMEN_URL_SERVER_DATA = './';
var mapfile = 'demo_brgm.map';
var mapfile_name = 'demo_brgm.map';
var CARMEN_URL_SERVER_FRONT = './';
var IHMUrl = './IHM/IHM/cartes/';
var CARMEN_DEBUG = '1';
var overloadExtTheme = 'olive';
var CARMEN_URL_ADMIN_CARTO = './';
var CARMEN_URL_PATH_DATA = './';
var CARMEN_URL_SERVER_DOWNLOAD = './';
var PRODIGE_VERIFY_RIGHTS_URL = './';
var CARMEN_URL_CATALOGUE = './';
var PRO_REQUETEUR_NB_LIMIT_OBJ = '40';
var tabContextParam = new Array(['one_day', 'une journée'], ['seven_days', 'une semaine'], ['one_month', 'un mois'], ['permanent', 'permanente']);
var tabEPSG = new Array(['2154', 'RGF93/Lambert 93'], ['4171', 'RGF93 2D'], ['3942', 'Lambert 93 Conique conforme 1'], ['3943', 'Lambert 93 Conique conforme 2'], ['3944', 'Lambert 93 Conique conforme 3'], ['3945', 'Lambert 93 Conique conforme 4'], ['3946', 'Lambert 93 Conique conforme 5'], ['3947', 'Lambert 93 Conique conforme 6'], ['3948', 'Lambert 93 Conique conforme 7'], ['3949', 'Lambert 93 Conique conforme 8'], ['3950', 'Lambert 93 Conique conforme 9'], ['27571', 'NTF(Paris) / Lambert zone 1'], ['27572', 'NTF(Paris) / Lambert zone 2'], ['27573', 'NTF(Paris) / Lambert zone 3'], ['27574', 'NTF(Paris) / Lambert zone 4'], ['27561', 'NTF(Paris) / Lambert Nord France'], ['27562', 'NTF(Paris) / Lambert centre France'], ['27563', 'NTF(Paris) / Lambert Sud France'], ['27564', 'NTF(Paris) / Lambert Corse'], ['4258', 'ETRS89 (INSPIRE)'], ['32620', 'WGS84 / UTM 20 Nord'], ['2972', 'RGFG95 / UTM 22 Nord'], ['2975', 'RGR92 / UTM 40 Nord'], ['2971', 'CSG 67 / UTM 22 Nord'], ['3312', 'CSG 67 / UTM 21 Nord'], ['2973', 'Fort Desaix / UTM 20'], ['4625', 'Fort Desaix'], ['3727', 'Piton des neiges / Gauss Laborde Reunion'], ['2987', 'IGN1950 / UTM 21 Nord'], ['32738', 'WGS84 / UTM 38 Sud'], ['32301', 'WGS72 / UTM 1 Sud'], ['3296', 'RGPF / UTM 5 Sud'], ['3297', 'RGPF / UTM 6 Sud'], ['2980', 'Combani 1950 / UTM 38 sud'], ['3163', 'RGNC91-93 / Lambert New Caledoni'], ['3170', 'RGNC91-93 / UTM 58'], ['3171', 'RGNC91-93 / UTM 59'], ['32630', 'WGS84 / UTM 30 Nord'], ['32631', 'WGS84 / UTM 31 Nord'], ['32632', 'WGS84 / UTM 32 Nord']);;

