/**
 *
 */

var VISIBILITY_APP_VECTOR_LAYER = false;
var VISIBILITY_APP_RASTER_LAYER = false;
var VISIBILITY_APP_PRODIGE_LAYER = true;
var VISIBILITY_APP_POSTGIS_LAYER = false;

var VISIBILITY_APP_BTN_CATALOG = false;
var VISIBILITY_APP_BTN_POSTGIS = false;
var VISIBILITY_APP_BTN_SHARE = false;
var VISIBILITY_APP_BTN_GEOSOURCE = false;

var VISIBILITY_ACCOUNT_FORMS_PROFILE_TITLE = false;
var VISIBILITY_ACCOUNT_FORMS_PREFERENCES_TITLE = (isAdmin ? true : false);
var VISIBILITY_ACCOUNT_FORMS_CONTACT_TITLE = false;

var VISIBILITY_APP_BTN_NEW = false;
var VISIBILITY_APP_BTN_OPEN = false;

var VISIBILITY_MAPS_FORMS_GENERAL_FIELDS_MAPPASSWORD = false;
var VISIBILITY_MAPS_FORMS_GENERAL_FIELDS_MAPCATALOGUABLE = false;
var VISIBILITY_MAPS_FORMS_GENERAL_FIELDS_KEYWORDS = false;

var VISIBILITY_MAPS_FORMS_UI_EXTRA_BANNER_HABILLAGE =  false;
var VISIBILITY_MAPS_FORMS_UI_LEGEND_PRINT =  false;
var VISIBILITY_MAPS_FORMS_UI_FOCUS = false;
var VISIBILITY_MAPS_FORMS_UI_EXTRA_PRINTMODELS = (isAdmin ? true : false);
var VISIBILITY_APP_BTN_ACCOUNT = (isAdmin ? true : false);

var VISIBILITY_PRINT_lEGEND = false;

var TABSFORDELETING = ['TabMeta', 'TabOGC'];

var VISIBILITY_LAYER_WMSQUERY = false;

var VISIBILITY_LAYER_FORMS_TABFIELD_COLUMNS_FIELDOGC = false;

var VISIBILITY_TOOL_PLUSMOINS = (isAdmin ? true : false);

var ADDCONTEXTE = false;

var VISIBILITY_APP_PRODIGE_SHARE = false;

var VISIBILITY_WXS_MANAGE = (isAdmin ? true : false);


var VISIBILITY_KEYMAP_LIBRARY = true;

var KEYMAP_LIBRARY_ISADMIN = (isAdmin ? true : false);

var MAPFILE_READONLY = true;
