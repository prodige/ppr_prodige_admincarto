#!/bin/bash
#backup 
cp /var/www/html/data/logs/WFS.csv /var/www/html/data/logs/WFS.csv.backup_sed
cp /var/www/html/data/logs/WMS.csv /var/www/html/data/logs/WMS.csv.backup_sed

# fix retour chariot
sed -i -r 's/([0-9]{4}-...-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2})/\n\1/g' /var/www/html/data/logs/WFS.csv
# supprimer le premier match qui crée une ligne vide :
sed -i '/^$/d' /var/www/html/data/logs/WFS.csv
# fix format date
sed -i 's/-Jan-/-01-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Feb-/-02-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Mar-/-03-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Apr-/-04-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-May-/-05-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Jun-/-06-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Jul-/-07-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Aug-/-08-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Sep-/-09-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Oct-/-10-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Nov-/-11-/' /var/www/html/data/logs/WFS.csv
sed -i 's/-Dec-/-12-/' /var/www/html/data/logs/WFS.csv


# fix retour chariot
sed -i -r 's/([0-9]{4}-...-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2})/\n\1/g' /var/www/html/data/logs/WMS.csv
# supprimer le premier match qui crée une ligne vide :
sed -i '/^$/d' /var/www/html/data/logs/WMS.csv
# fix format date
sed -i 's/-Jan-/-01-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Feb-/-02-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Mar-/-03-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Apr-/-04-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-May-/-05-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Jun-/-06-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Jul-/-07-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Aug-/-08-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Sep-/-09-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Oct-/-10-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Nov-/-11-/' /var/www/html/data/logs/WMS.csv
sed -i 's/-Dec-/-12-/' /var/www/html/data/logs/WMS.csv
