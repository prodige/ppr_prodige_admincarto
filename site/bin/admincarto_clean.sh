#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
PRODIGE_PATH_DATA="/var/www/html/data"

# Log folder
LOGDIR="/var/www/html/site/var/log/cron"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"

LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"
LOCKDIR="${LOGDIR}/${SCRIPTNAME%.*}.lock"

echolog()
{
  echo "`date '+%b %d %H:%M:%S'`  ${1}"
}

lock()
{
   mkdir "${LOCKDIR}" 2> /dev/null && trap 'rm -rf ${LOCKDIR}; exit' 0 2
}
lock || { echolog "Erreur ${SCRIPTNAME} est déjà en cours d'exécution, fin du script"; exit 1;}

mainjob()
{
  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication ] && find ${PRODIGE_PATH_DATA}/cartes/Publication/ -type f -name "temp_admin_*" -mtime +7 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication ] && find ${PRODIGE_PATH_DATA}/cartes/Publication/ -type f -name "TMP_*" -mtime +1 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication/temp ] &&  find ${PRODIGE_PATH_DATA}/cartes/Publication/temp/ -type f -mtime +30 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/mapimage ] && find ${PRODIGE_PATH_DATA}/mapimage/ -type f -mtime +1 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/tmpcontext ] && find ${PRODIGE_PATH_DATA}/tmpcontext/ -type f -mtime +1 -exec rm -f {} \;

  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/seven_days ] && find ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/seven_days/ -type f -mtime +7 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/one_month ] &&  find ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/one_month/ -type f -mtime +31 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/one_day ] && find ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/one_day/ -type f -mtime +1 -exec rm -f {} \;
  [ -d ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/temp ] && find ${PRODIGE_PATH_DATA}/cartes/Publication/wfs/temp/ -type f -mtime +3 -exec rm -f {} \;
}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10 -exec rm -f {} \;
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
exec 1>&6 6>&- 2>&7 7>&-
