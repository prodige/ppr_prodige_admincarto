#!/bin/bash
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

# Install old dependencies
#for lib in public/vendor/*.zip; do
#    if [ ! -d 'public/vendor/${lib/.zip/}' ]; then
#    unzip -oq $lib -d public/vendor ;
#    rm -f $lib ;
#  fi ;
#done;

# Migrate migrations to Symfony 5.
docker exec --user www-data -w /var/www/html/site ppr_prodige_admincarto_web /bin/bash -c " \
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20191130040102' --add ;\
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20200922090059' --add ;\
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20200924072357' --add ;\
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20200925110519' --add ;\
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20201013124402' --add ;\
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20210112105500' --add ;\
  php bin/console doctrine:migrations:version 'DoctrineMigrations\Version20210319090902' --add "
