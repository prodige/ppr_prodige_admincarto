#!/bin/bash
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

# Wait postgresql restart
sleep 3

# gen assets
OWNER="www-data"
function run_as {
  su -pc "$1" -s /bin/bash $OWNER
}

echo "FIX ulimit to 65536"
echo "APACHE_ULIMIT_MAX_FILES='ulimit -n 65536'" >> /etc/apache2/envvars

run_as "php $SCRIPTDIR/console cache:clear --env dev"
run_as "php $SCRIPTDIR/console cache:clear --env prod"

# Migrate database
run_as "php $SCRIPTDIR/console doctrine:migrations:migrate --no-interaction"
