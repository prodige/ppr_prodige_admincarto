#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
HOMEPRODIGE="/var/www/html/site"
# Log folder
LOGDIR="/var/www/html/site/var/log/cron"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"

LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"
LOCKDIR="${LOGDIR}/${SCRIPTNAME%.*}.lock"

echolog()
{
  echo "`date '+%b %d %H:%M:%S'`  ${1}"
}

lock()
{
   mkdir "${LOCKDIR}" 2> /dev/null && trap 'rm -rf ${LOCKDIR}; exit' 0 2
}
lock || { echolog "Erreur ${SCRIPTNAME} est déjà en cours d'exécution, fin du script"; exit 1;}

mainjob()
{
  cd ${HOMEPRODIGE} && /usr/bin/php bin/console carmen:cleanMaps --env=prod
}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10 -exec rm -f {} \;
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
exec 1>&6 6>&- 2>&7 7>&-
