<?php

namespace Carmen\WebBundle\Controller;

use Carmen\ApiBundle\Controller\BaseController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @author alkante
 */
class DefaultController extends BaseController
{
    /**
     * @isGranted("ROLE_USER")
     * @Route("/", name="carmen_ws_index", options={"expose"=true})
     */
    public function indexAction(Request $request, ContainerInterface $container)
    {
        $session = $request->getSession();

        if ($request->get('from', false) !==  'prodige') {
            $session->clear();
        }

        $uuid = $session->get('uuid', null);

        if ($uuid) {
            $serializer = $container->get('jms_serializer');
            $context = new SerializationContext();
            $context->setSerializeNull(true);
            $user = $serializer->serialize($this->getUser(), 'json', $context);

            $config = $container->get('carmen_config')->getWebConfig();
            $defaults = $container->getParameter('jsdefaults', array());
            $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));

            $connection = $this->getDoctrine()->getConnection();
            if ($connection) {
                try {
                    $settings = $connection->fetchAll("select prodige_settings_constant, prodige_settings_value from parametrage.prodige_settings");
                    foreach ($settings as $row) {
                        $config[$row["prodige_settings_constant"]] = $row["prodige_settings_value"];
                    }
                } catch (\Exception $exception) {
                }
            }
            // return $this->render('CarmenWebBundle:Default:index.html.twig', array(
            return $this->render('WebBundle/Default/index.html.twig', array(
                'user' => $user,
                'parameters' => $config,
                'jsdefaults' => $defaults,
                'isAdmin' => $request->get('isAdmin', 0)
            ));
        } else {
            // on est normalement ejecté avant par les vérif niveau CasProvider
            return $this->createAccessDeniedException("Paramètre manquant : uuid");
        }
    }
    
    /**
     * Returns the default DBAL PDO Connection.
     * 
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection($connection_name = null, $schema = "carmen")
    {
        return $this->getDoctrine()->getConnection();
    }
}
