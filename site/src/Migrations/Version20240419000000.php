<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240419000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (FALSE, 'route', 'tool')");
        $this->addSql("insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (FALSE, 'isochrone', 'tool')");
     
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
