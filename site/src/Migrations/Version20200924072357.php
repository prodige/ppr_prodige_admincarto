<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200924072357 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        // insertion du type geojson
        $this->addSql(
            "INSERT INTO carmen.lex_layer_type(layer_type_code, layer_type_name) SELECT 'GEOJSON', 'Geojson'
            WHERE NOT EXISTS (
                SELECT layer_type_code FROM carmen.lex_layer_type WHERE layer_type_code = 'GEOJSON'
            );"
        );

        // création de la tables des paramètres de geojson
       $this->addSql(
            "CREATE TABLE carmen.geojson_params (
                geojson_params_id integer NOT NULL,
                geojson_distance integer default 10,
                geojson_single_color boolean default false,
                geojson_color text,
                geojson_fixed_size boolean default false,
                geojson_size integer,
                geojson_transition_effect boolean default false,
                geojson_zoom_on_click boolean default false,
                layer_id integer NOT NULL
            );"
        );

        // clé primaire
        $this->addSql("ALTER TABLE ONLY carmen.geojson_params ADD CONSTRAINT pk_geojson_params PRIMARY KEY (geojson_params_id)");

        // séquence
        $this->addSql("CREATE SEQUENCE carmen.geojson_params_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.geojson_params_id_seq OWNED BY carmen.geojson_params.geojson_params_id");
        $this->addSql("ALTER TABLE ONLY carmen.geojson_params ALTER COLUMN geojson_params_id SET DEFAULT nextval('carmen.geojson_params_id_seq'::regclass)");

        // index
        $this->addSql("CREATE INDEX idx_geojson_params ON carmen.geojson_params USING btree (geojson_params_id)");

        // relation MantyToOne entre geojson param et layer
        $this->addSql("ALTER TABLE ONLY carmen.geojson_params ADD CONSTRAINT fk_geojson_layer FOREIGN KEY (layer_id) REFERENCES carmen.layer(layer_id)");   
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

        // supression de la relation entre layer geojson prams
        $this->addSql("ALTER TABLE carmen.geojson_params DROP CONSTRAINT  if exists fk_geojson_layer");
        
        // supression table geojson_params
        $this->addSql("DROP TABLE if exists carmen.geojson_params");

        $this->addSql("DROP SEQUENCE if exists carmen.geojson_params_id_seq");
        $this->addSql("DROP INDEX if exists idx_geojson_params");
    }
}
