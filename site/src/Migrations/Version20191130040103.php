<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191130040103 extends AbstractMigration
{
    public function getDescription():string
    {
        return 'Ajout des contraintes de la base PRODIGE';
    }

    public function up(Schema $schema):void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('set search_path to public');
        $this->addSql("ALTER TABLE ONLY carmen.account ADD CONSTRAINT fk_account FOREIGN KEY (account_dns_id) REFERENCES carmen.lex_dns(dns_id)");
        $this->addSql("ALTER TABLE ONLY carmen.account_model ADD CONSTRAINT fk_account_model FOREIGN KEY (account_id) REFERENCES carmen.account(account_id)");
        $this->addSql("ALTER TABLE ONLY carmen.contact ADD CONSTRAINT fk_contact_user FOREIGN KEY (user_id) REFERENCES carmen.users(user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.favorite_area ADD CONSTRAINT fk_favorite_areas FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.field ADD CONSTRAINT fk_field_lex_field_datatype FOREIGN KEY (field_datatype_id) REFERENCES carmen.lex_field_datatype(field_datatype_id)");
        $this->addSql("ALTER TABLE ONLY carmen.field ADD CONSTRAINT fk_fields FOREIGN KEY (layer_id) REFERENCES carmen.layer(layer_id)");
        $this->addSql("ALTER TABLE ONLY carmen.field ADD CONSTRAINT fk_fields_0 FOREIGN KEY (field_type_id) REFERENCES carmen.lex_field_type(field_type_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_group ADD CONSTRAINT fk_group_map FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.keyword ADD CONSTRAINT fk_keywords FOREIGN KEY (category_id) REFERENCES carmen.lex_category_keyword(category_id)");
        $this->addSql("ALTER TABLE ONLY carmen.layer ADD CONSTRAINT fk_layer FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.layer ADD CONSTRAINT fk_layer_0 FOREIGN KEY (layer_analyse_type_id) REFERENCES carmen.lex_analyse_type(analyse_type_id)");
        $this->addSql("ALTER TABLE ONLY carmen.layer ADD CONSTRAINT fk_layer_lex_layer_type FOREIGN KEY (layer_type_code) REFERENCES carmen.lex_layer_type(layer_type_code)");
        $this->addSql("ALTER TABLE ONLY carmen.map ADD CONSTRAINT fk_map FOREIGN KEY (account_id) REFERENCES carmen.account(account_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_annotation_attribute ADD CONSTRAINT fk_map_annotations_attributs FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_audit ADD CONSTRAINT fk_map_audit_account_id FOREIGN KEY (account_id) REFERENCES carmen.account(account_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_keyword ADD CONSTRAINT fk_map_keywords FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_keyword ADD CONSTRAINT fk_map_keywords_0 FOREIGN KEY (keyword_id) REFERENCES carmen.keyword(keyword_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map ADD CONSTRAINT fk_map_lex_projection FOREIGN KEY (map_projection_epsg) REFERENCES carmen.lex_projection(projection_epsg)");
        $this->addSql("ALTER TABLE ONLY carmen.map_locator ADD CONSTRAINT fk_map_locator FOREIGN KEY (locator_criteria_related) REFERENCES carmen.map_locator(locator_id) ON DELETE SET NULL");
        $this->addSql("ALTER TABLE ONLY carmen.map_locator ADD CONSTRAINT fk_map_locator_0 FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map ADD CONSTRAINT fk_map_publishedid FOREIGN KEY (published_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tool ADD CONSTRAINT fk_map_tools FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tool ADD CONSTRAINT fk_map_tools_0 FOREIGN KEY (tool_id) REFERENCES carmen.lex_tool(tool_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tree ADD CONSTRAINT fk_map_tree_map FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tree_print ADD CONSTRAINT fk_map_tree_print_map FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_ui ADD CONSTRAINT fk_map_ui FOREIGN KEY (map_id) REFERENCES carmen.map(map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_ui ADD CONSTRAINT fk_map_ui_0 FOREIGN KEY (ui_color_id) REFERENCES carmen.lex_color_id(color_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map ADD CONSTRAINT fk_map_users FOREIGN KEY (user_id) REFERENCES carmen.users(user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.preferences ADD CONSTRAINT fk_preferences_lex_units FOREIGN KEY (preference_units) REFERENCES carmen.lex_units(unit_code)");
        $this->addSql("ALTER TABLE ONLY carmen.preferences ADD CONSTRAINT fk_preferences_user FOREIGN KEY (user_id) REFERENCES carmen.users(user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.ui_model ADD CONSTRAINT fk_ui_model FOREIGN KEY (ui_id) REFERENCES carmen.map_ui(ui_id)");
        $this->addSql("ALTER TABLE ONLY carmen.ui_model ADD CONSTRAINT fk_ui_model_0 FOREIGN KEY (account_model_id) REFERENCES carmen.account_model(account_model_id)");
        $this->addSql("ALTER TABLE ONLY carmen.users ADD CONSTRAINT fk_user FOREIGN KEY (account_id) REFERENCES carmen.account(account_id)");
        $this->addSql("ALTER TABLE ONLY carmen.user_audit ADD CONSTRAINT fk_user_audit FOREIGN KEY (user_id) REFERENCES carmen.users(user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.wxs ADD CONSTRAINT fk_wxs FOREIGN KEY (account_id) REFERENCES carmen.account(account_id)");
        $this->addSql("ALTER TABLE ONLY carmen.wxs ADD CONSTRAINT fk_wxs_0 FOREIGN KEY (wxs_type_id) REFERENCES carmen.lex_wxs_type(wxs_type_id)");
    }

    public function down(Schema $schema) :void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("ALTER TABLE ONLY carmen.account DROP CONSTRAINT fk_account");
        $this->addSql("ALTER TABLE ONLY carmen.account_model DROP CONSTRAINT fk_account_model");
        $this->addSql("ALTER TABLE ONLY carmen.contact DROP CONSTRAINT fk_contact_user");
        $this->addSql("ALTER TABLE ONLY carmen.favorite_area DROP CONSTRAINT fk_favorite_areas");
        $this->addSql("ALTER TABLE ONLY carmen.field DROP CONSTRAINT fk_field_lex_field_datatype");
        $this->addSql("ALTER TABLE ONLY carmen.field DROP CONSTRAINT fk_fields");
        $this->addSql("ALTER TABLE ONLY carmen.field DROP CONSTRAINT fk_fields_0");
        $this->addSql("ALTER TABLE ONLY carmen.map_group DROP CONSTRAINT fk_group_map");
        $this->addSql("ALTER TABLE ONLY carmen.keyword DROP CONSTRAINT fk_keywords");
        $this->addSql("ALTER TABLE ONLY carmen.layer DROP CONSTRAINT fk_layer");
        $this->addSql("ALTER TABLE ONLY carmen.layer DROP CONSTRAINT fk_layer_0");
        $this->addSql("ALTER TABLE ONLY carmen.layer DROP CONSTRAINT fk_layer_lex_layer_type");
        $this->addSql("ALTER TABLE ONLY carmen.map DROP CONSTRAINT fk_map");
        $this->addSql("ALTER TABLE ONLY carmen.map_annotation_attribute DROP CONSTRAINT fk_map_annotations_attributs");
        $this->addSql("ALTER TABLE ONLY carmen.map_audit DROP CONSTRAINT fk_map_audit_account_id");
        $this->addSql("ALTER TABLE ONLY carmen.map_keyword DROP CONSTRAINT fk_map_keywords");
        $this->addSql("ALTER TABLE ONLY carmen.map_keyword DROP CONSTRAINT fk_map_keywords_0");
        $this->addSql("ALTER TABLE ONLY carmen.map DROP CONSTRAINT fk_map_lex_projection");
        $this->addSql("ALTER TABLE ONLY carmen.map_locator DROP CONSTRAINT fk_map_locator");
        $this->addSql("ALTER TABLE ONLY carmen.map_locator DROP CONSTRAINT fk_map_locator_0");
        $this->addSql("ALTER TABLE ONLY carmen.map DROP CONSTRAINT fk_map_publishedid");
        $this->addSql("ALTER TABLE ONLY carmen.map_tool DROP CONSTRAINT fk_map_tools");
        $this->addSql("ALTER TABLE ONLY carmen.map_tool DROP CONSTRAINT fk_map_tools_0");
        $this->addSql("ALTER TABLE ONLY carmen.map_tree DROP CONSTRAINT fk_map_tree_map");
        $this->addSql("ALTER TABLE ONLY carmen.map_tree_print DROP CONSTRAINT fk_map_tree_print_map");
        $this->addSql("ALTER TABLE ONLY carmen.map_ui DROP CONSTRAINT fk_map_ui");
        $this->addSql("ALTER TABLE ONLY carmen.map_ui DROP CONSTRAINT fk_map_ui_0");
        $this->addSql("ALTER TABLE ONLY carmen.map DROP CONSTRAINT fk_map_users");
        $this->addSql("ALTER TABLE ONLY carmen.preferences DROP CONSTRAINT fk_preferences_lex_units");
        $this->addSql("ALTER TABLE ONLY carmen.preferences DROP CONSTRAINT fk_preferences_user");
        $this->addSql("ALTER TABLE ONLY carmen.ui_model DROP CONSTRAINT fk_ui_model");
        $this->addSql("ALTER TABLE ONLY carmen.ui_model DROP CONSTRAINT fk_ui_model_0");
        $this->addSql("ALTER TABLE ONLY carmen.users DROP CONSTRAINT fk_user");
        $this->addSql("ALTER TABLE ONLY carmen.user_audit DROP CONSTRAINT fk_user_audit");
        $this->addSql("ALTER TABLE ONLY carmen.wxs DROP CONSTRAINT fk_wxs");
        $this->addSql("ALTER TABLE ONLY carmen.wxs DROP CONSTRAINT fk_wxs_0");

    }
}
