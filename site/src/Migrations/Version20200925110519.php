<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200925110519 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //geojson_params_geojson_params_id_seq
        //ALTER SEQUENCE name RENAME TO new_name

        $this->addSql("ALTER SEQUENCE carmen.geojson_params_id_seq RENAME TO geojson_params_geojson_params_id_seq");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER SEQUENCE carmen.geojson_params_geojson_params_id_seq RENAME TO geojson_params_id_seq");

    }
}
