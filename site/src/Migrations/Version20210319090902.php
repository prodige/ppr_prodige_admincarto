<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210319090902 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER DOMAIN phone_number drop constraint phone_number_check;");
        $this->addSql("ALTER DOMAIN phone_number add constraint phone_number_check CHECK ( value ~ '^(\+33|\+590|\+594|\+262|\+596|\+269|\+687|\+689|\+590|\+508|\+681)(\d){6,9}$' );");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        
    }
}