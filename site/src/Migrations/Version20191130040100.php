<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191130040100 extends AbstractMigration
{
    public function getDescription() :string
    {
        return 'Création des tables de la base PRODIGE';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA carmen');
        $this->addSql('CREATE SCHEMA local_data');
        $this->addSql('CREATE SCHEMA parametrage');
        
        $this->addSql('set search_path to public');
       
       $this->addSql("DROP FUNCTION if exists public.st_aslatlontext(public.geometry)");
       $this->addSql("
          CREATE FUNCTION public.st_aslatlontext(public.geometry) RETURNS text
              LANGUAGE sql IMMUTABLE STRICT
              AS \$_\$ SELECT ST_AsLatLonText($1, '') \$_\$;
          ");
          
       $this->addSql("DROP FUNCTION if exists public.updateviewsgeometrycolumns()");
       $this->addSql("
          CREATE FUNCTION public.updateviewsgeometrycolumns() RETURNS SETOF record
              LANGUAGE plpgsql
              AS \$_\$
          declare
            r_tables record;
            r_views record;
            srid int;
            geometrytype text;
            func text[];
          begin
            for r_tables in 
              select geo.f_table_schema as table_schema, geo.f_table_name as table_name, geo.f_geometry_column as column_name
              from public.geometry_columns geo
              left join information_schema.views on (geo.f_table_schema=views.table_schema and geo.f_table_name=views.table_name) 
              where views.table_name is null and (geo.type='GEOMETRY' or geo.srid=0)
            loop
              srid := null;
              geometrytype := null;
              EXECUTE 
                \$\$ select distinct geometrytype \$\$||
                \$\$, srid \$\$||
                \$\$, (case when geometrytype like 'MULTI%' then 0 when geometrytype like '%POLYGON' then 1 else 2 end) as priority \$\$||
                \$\$ from (select distinct public.geometrytype(\$\$||r_tables.column_name||\$\$) as geometrytype \$\$||
                \$\$, public.st_srid(\$\$||r_tables.column_name||\$\$) as srid \$\$||
                \$\$ from \$\$||r_tables.table_schema||\$\$.\"\$\$||r_tables.table_name||\$\$\") foo \$\$||
                \$\$ where geometrytype is not null \$\$ ||
                \$\$ order by priority limit 1 \$\$
              INTO geometrytype, srid;

              geometrytype := upper(geometrytype);
              if ( not(geometrytype like 'MULTI%') ) then
                geometrytype := 'MULTI'||geometrytype;
              end if ;

              if ( srid is not null and geometrytype is not null and srid<>0 and geometrytype<>'GEOMETRY' ) then
                for r_views in 
                  select vtu.*
                  from information_schema.view_table_usage vtu 
                  where vtu.table_schema=r_tables.table_schema and vtu.table_name=r_tables.table_name
                loop
                  return query select (r_tables.table_schema||'.\"'||r_tables.table_name||'\"')::text
                  , ('drop view if exists '||r_views.view_schema||'.\"'||r_views.view_name||'\" cascade;')::text;
                end loop;

                func := null::text[];
                if ( geometrytype like 'MULTI%' ) then
                  func := func || ARRAY['public.st_multi'];
                end if;

                return query select (r_tables.table_schema||'.\"'||r_tables.table_name||'\"')::text
                , ('ALTER TABLE '||r_tables.table_schema||'.\"'||r_tables.table_name||'\" alter column '||r_tables.column_name||' type geometry('||geometrytype||', '||srid||')'||(case when coalesce(array_length(func,1), 0)>0 then ' using '||array_to_string(func, '(')||'('||r_tables.column_name||repeat(')', array_length(func,1)) else '' end))::text;

                for r_views in 
                  with recursive usages(view_order, view_schema, view_name, view_definition) as (
                    select 0 as view_order, vtu.view_schema, vtu.view_name, viewdef.view_definition
                    from information_schema.view_table_usage vtu 
                    inner join information_schema.views viewdef on (vtu.view_schema=viewdef.table_schema and vtu.view_name=viewdef.table_name)
                    where vtu.table_schema=r_tables.table_schema and vtu.table_name=r_tables.table_name

                    union all

                    select view_order+1 as view_order, vtu.view_schema, vtu.view_name, viewdef.view_definition
                    from usages
                    inner join information_schema.view_table_usage vtu on (vtu.table_schema=usages.view_schema and vtu.table_name=usages.view_name)
                    inner join information_schema.views viewdef on (vtu.view_schema=viewdef.table_schema and vtu.view_name=viewdef.table_name) 
                  )
                  select * from usages order by view_order
                loop
                  return query select (r_tables.table_schema||'.\"'||r_tables.table_name||'\"')::text
                  , ('create or replace view '||r_views.view_schema||'.\"'||r_views.view_name||'\" as '||r_views.view_definition)::text;
                end loop;
              end if;

            end loop;
          end;
          \$_\$;
          ");

        //$this->addSql('CREATE OPERATOR FAMILY public.gist_geometry_ops USING gist');
        $this->addSql('DROP DOMAIN if exists public.image');
        $this->addSql('CREATE DOMAIN public.image AS text');
        
        $this->addSql("CREATE TABLE carmen.account (    account_id integer NOT NULL,    account_dbpostgres character varying(64),    account_dns_id integer,    account_geonetworkurl character varying(500),    account_hostpostgres character varying(255),    account_name character varying(255),    account_passwordpostgres character varying(255),    account_path character varying(255),    account_portpostgres integer DEFAULT 5432 NOT NULL,    account_userpostgres character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.account IS 'List of all CARMEN accounts'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_dbpostgres IS 'Database name PostgresQL'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_dns_id IS 'account DNS name'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_geonetworkurl IS 'URL of Geonetwork/Geosource application for query the account metadata'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_hostpostgres IS 'Host PostgresSQL'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_name IS 'account name'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_passwordpostgres IS 'Password Read-only Account PostgreSQL'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_path IS 'account path directory'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_portpostgres IS 'Port PostgresSQL'");
        $this->addSql("COMMENT ON COLUMN carmen.account.account_userpostgres IS 'Identifier Read-only Account PostgreSQL'");
        $this->addSql("CREATE SEQUENCE carmen.account_account_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.account_account_id_seq OWNED BY carmen.account.account_id");
        $this->addSql("CREATE TABLE carmen.account_model (    account_model_id integer NOT NULL,    account_id integer NOT NULL,    account_model_name text,    account_model_file text)");
        $this->addSql("COMMENT ON TABLE carmen.account_model IS 'List of account models'");
        $this->addSql("COMMENT ON COLUMN carmen.account_model.account_model_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.account_model.account_id IS 'account identifier'");
        $this->addSql("COMMENT ON COLUMN carmen.account_model.account_model_name IS 'Model name'");
        $this->addSql("COMMENT ON COLUMN carmen.account_model.account_model_file IS 'Model file name'");
        $this->addSql("CREATE SEQUENCE carmen.account_model_account_model_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.account_model_account_model_id_seq OWNED BY carmen.account_model.account_model_id");
        $this->addSql("CREATE TABLE carmen.contact (    contact_id integer NOT NULL,    contact_address character varying(1024),    contact_city character varying(255),    contact_country character varying(255),    contact_email character varying(255),    contact_fax character varying(255),    contact_name character varying(255),    contact_organization character varying(255),    contact_phone character varying(255),    contact_position character varying(255),    contact_unit character varying(255),    contact_zipcode character varying(10),    user_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.contact IS 'Contact information associated with the account'");
        $this->addSql("COMMENT ON COLUMN carmen.contact.contact_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.contact.user_id IS 'user identifier'");
        $this->addSql("CREATE SEQUENCE carmen.contact_contact_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.contact_contact_id_seq OWNED BY carmen.contact.contact_id");
        $this->addSql("CREATE TABLE carmen.favorite_area (    area_id integer NOT NULL,    area_name character varying(255),    area_xmax numeric,    area_xmin numeric,    area_ymax numeric,    area_ymin numeric,    map_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.favorite_area IS 'List of favorites area by map'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.area_id IS 'Area ID'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.area_name IS 'name de la area'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.area_xmax IS 'XMAX coordinate for the area - Projection WGS84'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.area_xmin IS 'XMIN coordinate for the area - Projection WGS84'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.area_ymax IS 'YMAX coordinate for the area - Projection WGS84'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.area_ymin IS 'YMIN coordinate for the area - Projection WGS84'");
        $this->addSql("COMMENT ON COLUMN carmen.favorite_area.map_id IS 'Map ID'");
        $this->addSql("CREATE SEQUENCE carmen.favorite_area_area_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.favorite_area_area_id_seq OWNED BY carmen.favorite_area.area_id");
        $this->addSql("CREATE TABLE carmen.field (    field_id integer NOT NULL,    field_alias character varying(255),    field_datatype_id integer,    field_duplicate boolean,    field_headers boolean,    field_name character varying(255),    field_ogc boolean,    field_queries boolean,    field_rank integer,    field_tooltips boolean,    field_type_id integer,    field_url text,    layer_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.field IS 'List of fields for a layer'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_alias IS 'Field alias'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_duplicate IS 'Field is a duplicate of an original field ?'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_headers IS 'Field enabled in headers ?'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_name IS 'Field name'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_ogc IS 'Field enabled in OGC ?'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_queries IS 'Field enabled in queries ?'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_rank IS 'Field rank in layer'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_tooltips IS 'Field enabled in tooltips ?'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_type_id IS 'Field type ID'");
        $this->addSql("COMMENT ON COLUMN carmen.field.field_url IS 'Field URL prefix'");
        $this->addSql("COMMENT ON COLUMN carmen.field.layer_id IS 'Layer identifier'");
        $this->addSql("CREATE SEQUENCE carmen.field_field_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.field_field_id_seq OWNED BY carmen.field.field_id");
        $this->addSql("CREATE TABLE carmen.keyword (    keyword_id integer NOT NULL,    category_id integer,    keyword_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.keyword IS 'List of keywords'");
        $this->addSql("COMMENT ON COLUMN carmen.keyword.keyword_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.keyword.category_id IS 'category ID'");
        $this->addSql("COMMENT ON COLUMN carmen.keyword.keyword_name IS 'Keyword'");
        $this->addSql("CREATE SEQUENCE carmen.keyword_keyword_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.keyword_keyword_id_seq OWNED BY carmen.keyword.keyword_id");
        $this->addSql("CREATE TABLE carmen.layer (    layer_id integer NOT NULL,    map_id integer,    layer_title character varying(255),    layer_name character varying(255),    layer_type_code character varying(20) NOT NULL,    layer_analyse_type_id integer,    layer_identifier character varying(255),    layer_msname character varying(100),    layer_minscale bigint,    layer_maxscale bigint,    layer_projection_epsg numeric,    layer_opacity integer,    layer_outputformat character varying(100),    layer_data_encoding character varying(100),    layer_haslabel boolean,    layer_legend boolean,    layer_legend_scale boolean,    layer_field_url text,    layer_metadata_file character varying(255),    layer_metadata_uuid character varying(255),    layer_server_url text,    layer_server_version character varying(100),    layer_tiled_layer text,    layer_tiled_server_url text,    layer_tiled_resolutions text,    layer_wmsc_boundingbox text,    layer_wmts_matrixids text,    layer_wmts_tileorigin text,    layer_wmts_tileset text,    layer_wmts_style text,    layer_wxsname character varying(100),    layer_wms_query boolean,    layer_atom boolean,    layer_visible boolean,    layer_wfs boolean,    layer_wms boolean,    layer_downloadable boolean)");
        $this->addSql("COMMENT ON TABLE carmen.layer IS 'List of map layers'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.map_id IS 'Map ID'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_title IS 'Layer title'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_name IS 'Layer name (for WMS calls)'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_type_code IS 'layer type code'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_analyse_type_id IS 'Layer analyse type'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_identifier IS 'The identifier of the layer for context URL'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_minscale IS 'Layer min scale visibility'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_maxscale IS 'Layer max scale visibility'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_projection_epsg IS 'Layer EPSG projection ID'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_opacity IS 'Layer opacity (0-100)'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_outputformat IS 'Output format for this layer (ex : png, ...)'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_data_encoding IS 'source data (vector data) encoding'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_haslabel IS 'True if the layer shows its labels'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_legend IS 'Visibility of the layer in legend'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_legend_scale IS 'Scale for visibility of the layer in legend'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_field_url IS 'URL prefix for all fields in layer'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_metadata_file IS 'The filename of metadata store in the Carmen Metadata directory'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_metadata_uuid IS 'UUID of the Geonetwork  metadata combinated with the account Geonetwork URL'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_server_url IS 'URL of the server for W(x)S layers'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_server_version IS 'Version of the server for W(x)S layers'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_wxsname IS 'The name of the layer in server WxS'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_atom IS 'Layer can be Atom ?'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_visible IS 'Layer visibility'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_wfs IS 'WFS exportable ?'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_wms IS 'WMS exportable ?'");
        $this->addSql("COMMENT ON COLUMN carmen.layer.layer_downloadable IS 'Layer downloadable ?'");
        $this->addSql("CREATE SEQUENCE carmen.layer_layer_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.layer_layer_id_seq OWNED BY carmen.layer.layer_id");
        $this->addSql("CREATE TABLE carmen.lex_analyse_type (    analyse_type_id integer NOT NULL,    analyse_type_code character varying(20),    analyse_type_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_analyse_type IS 'Analyse Types'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_analyse_type.analyse_type_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_analyse_type.analyse_type_code IS 'Analyse type code'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_analyse_type.analyse_type_name IS 'Analyse type name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_analyse_type_analyse_type_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_analyse_type_analyse_type_id_seq OWNED BY carmen.lex_analyse_type.analyse_type_id");
        $this->addSql("CREATE TABLE carmen.lex_category_keyword (    category_id integer NOT NULL,    category_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_category_keyword IS 'List of keyword categories. Predefined category : (0, `Mots clés libres`)'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_category_keyword.category_id IS 'Category ID'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_category_keyword.category_name IS 'Category name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_category_keyword_category_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_category_keyword_category_id_seq OWNED BY carmen.lex_category_keyword.category_id");
        $this->addSql("CREATE TABLE carmen.lex_color_id (    color_id integer NOT NULL,    color_key character varying(100),    color_name character varying(100))");
        $this->addSql("COMMENT ON TABLE carmen.lex_color_id IS 'Glossary of colors'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_color_id.color_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_color_id.color_key IS 'Color key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_color_id.color_name IS 'Color name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_color_id_color_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_color_id_color_id_seq OWNED BY carmen.lex_color_id.color_id");
        $this->addSql("CREATE TABLE carmen.lex_dns (    dns_id integer NOT NULL,    dns_prefix_backoffice character varying(150),    dns_prefix_data character varying(150),    dns_prefix_frontoffice character varying(150),    dns_prefix_metadata character varying(150),    dns_scheme character varying(10) DEFAULT 'http'::character varying,    dns_url character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_dns IS 'DNS types'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_dns.dns_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_dns.dns_url IS 'DNS URL'");
        $this->addSql("CREATE SEQUENCE carmen.lex_dns_dns_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_dns_dns_id_seq OWNED BY carmen.lex_dns.dns_id");
        $this->addSql("CREATE TABLE carmen.lex_field_datatype (    field_datatype_id integer NOT NULL,    field_datatype_code character varying(20),    field_datatype_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_field_datatype IS 'Field data Types'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_field_datatype.field_datatype_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_field_datatype.field_datatype_code IS 'Field data type code'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_field_datatype.field_datatype_name IS 'Field data type name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_field_datatype_field_datatype_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_field_datatype_field_datatype_id_seq OWNED BY carmen.lex_field_datatype.field_datatype_id");
        $this->addSql("CREATE TABLE carmen.lex_field_type (    field_type_id integer NOT NULL,    field_type_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_field_type IS 'Field types'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_field_type.field_type_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_field_type.field_type_name IS 'Field type name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_field_type_field_type_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_field_type_field_type_id_seq OWNED BY carmen.lex_field_type.field_type_id");
        $this->addSql("CREATE TABLE carmen.lex_inspire_theme (    theme_id integer NOT NULL,    theme_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_inspire_theme IS 'List of Inspire themes'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_inspire_theme.theme_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_inspire_theme.theme_name IS 'Name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_inspire_theme_theme_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_inspire_theme_theme_id_seq OWNED BY carmen.lex_inspire_theme.theme_id");
        $this->addSql("CREATE TABLE carmen.lex_iso_condition (    condition_id integer NOT NULL,    condition_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_iso_condition IS 'List of available conditions'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_condition.condition_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_condition.condition_name IS 'Name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_iso_condition_condition_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_iso_condition_condition_id_seq OWNED BY carmen.lex_iso_condition.condition_id");
        $this->addSql("CREATE TABLE carmen.lex_iso_constraint (    constraint_id integer NOT NULL,    constraint_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_iso_constraint IS 'Glossary of available constraints'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_constraint.constraint_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_constraint.constraint_name IS 'Name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_iso_constraint_constraint_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_iso_constraint_constraint_id_seq OWNED BY carmen.lex_iso_constraint.constraint_id");
        $this->addSql("CREATE TABLE carmen.lex_iso_language (    language_code character varying(3) NOT NULL,    language_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_iso_language IS 'Glossary of available languages for OGC services'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_language.language_code IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_language.language_name IS 'language name'");
        $this->addSql("CREATE TABLE carmen.lex_iso_theme (    theme_id integer NOT NULL,    theme_code character varying(255),    theme_name character varying(255))");
        $this->addSql("COMMENT ON TABLE carmen.lex_iso_theme IS 'List of ISO themes'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_theme.theme_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_theme.theme_code IS 'code (name anglais) associé'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_iso_theme.theme_name IS 'Name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_iso_theme_theme_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_iso_theme_theme_id_seq OWNED BY carmen.lex_iso_theme.theme_id");
        $this->addSql("CREATE TABLE carmen.lex_layer_type (    layer_type_code character varying(20) NOT NULL,    layer_type_name character varying(20))");
        $this->addSql("COMMENT ON TABLE carmen.lex_layer_type IS 'layers type'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_layer_type.layer_type_code IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_layer_type.layer_type_name IS 'layer type name'");
        $this->addSql("CREATE TABLE carmen.lex_map_audit_status (    map_audit_status_id integer NOT NULL,    map_audit_status_name text NOT NULL)");
        $this->addSql("COMMENT ON TABLE carmen.lex_map_audit_status IS 'Glossary of map transactions (create, update...)'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_map_audit_status.map_audit_status_id IS 'primary key'");
        $this->addSql("CREATE SEQUENCE carmen.lex_map_audit_status_map_audit_status_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_map_audit_status_map_audit_status_id_seq OWNED BY carmen.lex_map_audit_status.map_audit_status_id");
        $this->addSql("CREATE TABLE carmen.lex_projection (    projection_epsg numeric NOT NULL,    projection_name character varying(255),    projection_provider character varying(255) DEFAULT 'EPSG'::character varying)");
        $this->addSql("COMMENT ON TABLE carmen.lex_projection IS 'Glossary of authorized projections'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_projection.projection_epsg IS 'EPSG code of projection'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_projection.projection_name IS 'Projection name'");
        $this->addSql("CREATE TABLE carmen.lex_tool (    tool_id integer NOT NULL,    tool_default_enabled boolean DEFAULT false NOT NULL,    tool_identifier character varying(255),    tool_xml_nodeprefix character varying(50) DEFAULT 'tool'::character varying NOT NULL,    tool_xml_nodevalue character varying(50))");
        $this->addSql("COMMENT ON TABLE carmen.lex_tool IS 'Glossary of tools'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_tool.tool_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_tool.tool_default_enabled IS 'Set true if this tool is activate by default for a new map'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_tool.tool_identifier IS 'Tool ID in context'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_tool.tool_xml_nodeprefix IS 'Give the OWS Context / Json node prefix for this tool. Default is `tool` but may be `mapSettings`'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_tool.tool_xml_nodevalue IS 'Give the OWS Context / Json node name for specify the tool value.'");
        $this->addSql("CREATE SEQUENCE carmen.lex_tool_tool_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_tool_tool_id_seq OWNED BY carmen.lex_tool.tool_id");
        $this->addSql("CREATE TABLE carmen.lex_units (    unit_code character varying(20) NOT NULL,    unit_name character varying(100))");
        $this->addSql("COMMENT ON TABLE carmen.lex_units IS 'Glossary of measurement units'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_units.unit_code IS 'Mapserver code of UNIT'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_units.unit_name IS 'Label for the unit'");
        $this->addSql("CREATE TABLE carmen.lex_wxs_type (    wxs_type_id integer NOT NULL,    wxs_type_name character varying(100))");
        $this->addSql("COMMENT ON TABLE carmen.lex_wxs_type IS 'Services type'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_wxs_type.wxs_type_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.lex_wxs_type.wxs_type_name IS 'Service name'");
        $this->addSql("CREATE SEQUENCE carmen.lex_wxs_type_wxs_type_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_wxs_type_wxs_type_id_seq OWNED BY carmen.lex_wxs_type.wxs_type_id");
        $this->addSql("CREATE TABLE carmen.map (    map_id integer NOT NULL,    account_id integer NOT NULL,    map_atommetadata_uuid character varying(255),    map_buffer_global real,    map_catalogable boolean,    map_datepublication timestamp without time zone,    map_extent_xmax double precision,    map_extent_xmin double precision,    map_extent_ymax double precision,    map_extent_ymin double precision NOT NULL,    map_file character varying(255),    map_maxscale real,    map_minscale real,    map_model boolean,    map_password text,    map_projection_epsg numeric,    map_bgcolor character varying(20) DEFAULT 'ffffff'::character varying,    map_refmap_extent_xmax double precision,    map_refmap_extent_xmin double precision,    map_refmap_extent_ymax double precision,    map_refmap_extent_ymin double precision,    map_refmap_image character varying(255),    map_summary text,    map_title character varying(255),    map_wfsmetadata_uuid character varying(255),    map_wmsmetadata_uuid character varying(255),    published_id integer,    user_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.map IS 'List of Maps'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.map.account_id IS 'account ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_buffer_global IS 'value, in meters, of the global buffer'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_catalogable IS 'Can be added to catalogue ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_datepublication IS 'Date of publication for published maps or Creation date for draft maps'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_extent_xmax IS 'xmax map extent, expressed in map projectionnn@init database preferences/preferences_extent_xmaxnn@used mapfile  MAP/EXTENT{sting3}n@used context  OWSContext/General/BoundingBox/UpperCorner{string1}n@used context  OWSContext/General/MaxBoundingBox/UpperCorner{string1}'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_extent_xmin IS 'xmin map extent, expressed in map projectionnn@init database preferences/preferences_extent_xminnn@used mapfile  MAP/EXTENT{sting1}n@used context  OWSContext/General/BoundingBox/LowerCorner{string1}n@used context  OWSContext/General/MaxBoundingBox/LowerCorner{string1}'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_extent_ymax IS 'ymax map extent, expressed in map projectionnn@init database preferences/preferences_extent_ymaxnn@used mapfile  MAP/EXTENT{sting4}n@used context  OWSContext/General/BoundingBox/UpperCorner{string2}n@used context  OWSContext/General/MaxBoundingBox/UpperCorner{string2}'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_extent_ymin IS 'ymin map extent, expressed in map projectionnn@init database preferences/preferences_extent_yminnn@used mapfile  MAP/EXTENT{sting2}n@used context  OWSContext/General/BoundingBox/LowerCorner{string2}n@used context  OWSContext/General/MaxBoundingBox/LowerCorner{string2}'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_file IS 'path to MapFile'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_maxscale IS 'Max scale applied to the mapnn@init database preferences/preference_maxscalenn@used mapfile  MAP/WEB/MAXSCALEDENOM'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_minscale IS 'Min scale applied to the mapnn@init database preferences/preference_minscalenn@used mapfile  MAP/WEB/MINSCALEDENOM'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_model IS 'This map is a model ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_password IS 'Map password'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_projection_epsg IS 'Map EPSG projection ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_summary IS 'Map summary'");
        $this->addSql("COMMENT ON COLUMN carmen.map.map_title IS 'Map titlenn@used mapfile  MAP/NAMEn@used context  OWSContext[id]n@used context  OWSContext/General/Title'");
        $this->addSql("COMMENT ON COLUMN carmen.map.published_id IS 'ID of the published map for this draft map (not null = it is a draft map, null = it is a published map)'");
        $this->addSql("COMMENT ON COLUMN carmen.map.user_id IS 'Current user who edits this map'");
        $this->addSql("CREATE TABLE carmen.map_annotation_attribute (    attribute_id integer NOT NULL,    attribute_name character varying(255),    attribute_rank integer,    map_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.map_annotation_attribute IS 'List of annotation attributes for the map'");
        $this->addSql("COMMENT ON COLUMN carmen.map_annotation_attribute.attribute_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.map_annotation_attribute.attribute_name IS 'Attribute name'");
        $this->addSql("COMMENT ON COLUMN carmen.map_annotation_attribute.attribute_rank IS 'Attibute rank'");
        $this->addSql("COMMENT ON COLUMN carmen.map_annotation_attribute.map_id IS 'Map ID'");
        $this->addSql("CREATE SEQUENCE carmen.map_annotation_attribute_attribute_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.map_annotation_attribute_attribute_id_seq OWNED BY carmen.map_annotation_attribute.attribute_id");
        $this->addSql("CREATE TABLE carmen.map_audit (    map_audit_id integer NOT NULL,    account_id integer NOT NULL,    map_audit_date timestamp without time zone NOT NULL,    map_audit_status character varying(50) NOT NULL,    map_file character varying(255),    user_email text NOT NULL)");
        $this->addSql("COMMENT ON TABLE carmen.map_audit IS 'Statistics on map transactions'");
        $this->addSql("COMMENT ON COLUMN carmen.map_audit.map_audit_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.map_audit.account_id IS 'Account ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_audit.map_audit_date IS 'Map transaction date'");
        $this->addSql("COMMENT ON COLUMN carmen.map_audit.map_audit_status IS 'Map transaction'");
        $this->addSql("COMMENT ON COLUMN carmen.map_audit.map_file IS 'Map file'");
        $this->addSql("COMMENT ON COLUMN carmen.map_audit.user_email IS 'User ID for transaction'");
        $this->addSql("CREATE SEQUENCE carmen.map_audit_map_audit_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.map_audit_map_audit_id_seq OWNED BY carmen.map_audit.map_audit_id");
        $this->addSql("CREATE TABLE carmen.map_group (    group_id integer NOT NULL,    group_name text NOT NULL,    group_identifier character varying(255),    map_id integer NOT NULL)");
        $this->addSql("COMMENT ON TABLE carmen.map_group IS 'List of layer groups'");
        $this->addSql("COMMENT ON COLUMN carmen.map_group.group_id IS 'group ID (auto incremented number shared with layer ID)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_group.group_name IS 'Group name'");
        $this->addSql("COMMENT ON COLUMN carmen.map_group.group_identifier IS 'Group identifier for context URL'");
        $this->addSql("COMMENT ON COLUMN carmen.map_group.map_id IS 'Map ID'");
        $this->addSql("CREATE SEQUENCE carmen.map_group_group_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.map_group_group_id_seq OWNED BY carmen.map_group.group_id");
        $this->addSql("CREATE TABLE carmen.map_keyword (    keyword_id integer,    map_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.map_keyword IS 'Keywords for map'");
        $this->addSql("COMMENT ON COLUMN carmen.map_keyword.keyword_id IS 'Keyword ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_keyword.map_id IS 'Map ID'");
        $this->addSql("CREATE TABLE carmen.map_locator (    locator_id integer NOT NULL,    locator_criteria_field_id character varying(255),    locator_criteria_field_name character varying(255),    locator_criteria_layer_id integer,    locator_criteria_name character varying(255),    locator_criteria_rank integer,    locator_criteria_related integer,    locator_criteria_related_field character varying(255),    locator_criteria_visibility boolean,    map_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.map_locator IS 'Search locator : criteria settings'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_field_id IS 'Field ID for criteria'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_field_name IS 'Field name for criteria'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_layer_id IS 'Layer ID for criteria'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_name IS 'Criteria name'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_rank IS 'Criteria rank'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_related IS 'Related criteria ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_related_field IS 'Related criteria field'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.locator_criteria_visibility IS 'Criteria is visible ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_locator.map_id IS 'Map ID'");
        $this->addSql("CREATE SEQUENCE carmen.map_locator_locator_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.map_locator_locator_id_seq OWNED BY carmen.map_locator.locator_id");
        $this->addSql("CREATE SEQUENCE carmen.map_map_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.map_map_id_seq OWNED BY carmen.map.map_id");
        $this->addSql("CREATE TABLE carmen.map_tool (    map_id integer NOT NULL,    tool_id integer NOT NULL,    maptool_value text)");
        $this->addSql("COMMENT ON TABLE carmen.map_tool IS 'List of available tools by map'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tool.map_id IS 'Map ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tool.tool_id IS 'Tool ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tool.maptool_value IS 'The value of tool if it is enabled'");
        $this->addSql("CREATE TABLE carmen.map_tree (    node_id integer NOT NULL,    map_id integer NOT NULL,    node_is_layer boolean NOT NULL,    node_opened boolean DEFAULT true NOT NULL,    node_parent integer,    node_pos integer DEFAULT 0 NOT NULL,    CONSTRAINT ck_map_tree_nodirectcycle CHECK ((node_id <> node_parent)))");
        $this->addSql("COMMENT ON TABLE carmen.map_tree IS 'Tree for order and classify layers and groups in map'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree.node_id IS 'Node ID (group_id ou layer_id)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree.map_id IS 'Map ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree.node_is_layer IS 'Node is a layer ? (false=is a group)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree.node_opened IS 'Node is opened ? (used in Groups nodes only)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree.node_parent IS 'Parent ID for current node. (root = null)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree.node_pos IS 'Node position (order) in current level (start with 0 for each level)'");
        $this->addSql("CREATE TABLE carmen.map_tree_print (    node_id integer NOT NULL,    map_id integer NOT NULL,    node_is_layer boolean NOT NULL,    node_opened boolean DEFAULT true NOT NULL,    node_parent integer,    node_pos integer DEFAULT 0 NOT NULL,    CONSTRAINT ck_map_tree_printnodirectcycle CHECK ((node_id <> node_parent)))");
        $this->addSql("COMMENT ON TABLE carmen.map_tree_print IS 'Print mode : Tree for order and classify layers and groups in map'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree_print.node_id IS 'Print mode : Node ID (group_id ou layer_id)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree_print.map_id IS 'Print mode : Map ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree_print.node_is_layer IS 'Print mode : Node is a layer ? (false=is a group)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree_print.node_opened IS 'Print mode : Node is opened ? (used in Groups nodes only)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree_print.node_parent IS 'Print mode : Parent ID for current node. (root = null)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_tree_print.node_pos IS 'Print mode : Node position (order) in current level (start with 0 for each level)'");
        $this->addSql("CREATE TABLE carmen.map_ui (    ui_id integer NOT NULL,    map_id integer,    ui_banner boolean,    ui_banner_file text,    ui_color_id integer,    ui_copyright boolean,    ui_copyright_background_color character varying(7),    ui_copyright_font character varying(255),    ui_copyright_font_color character varying(7),    ui_copyright_font_size integer,    ui_copyright_text text,    ui_copyright_transparency integer DEFAULT 0,    ui_focus boolean,    ui_keymap boolean,    ui_legend boolean,    ui_legend_print boolean DEFAULT true,    ui_locate boolean,    ui_logo boolean,    ui_logopath text,    help_display boolean DEFAULT false,    help_message text)");
        $this->addSql("COMMENT ON TABLE carmen.map_ui IS 'UI settings, by map'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.map_id IS 'Map ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_banner IS 'Banner enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_banner_file IS 'Banner file'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_color_id IS 'UI color ID'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright IS 'Copyright enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright_background_color IS 'Copyright background color'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright_font IS 'Copyright font'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright_font_color IS 'Copyright font color'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright_font_size IS 'Copyright font size'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright_text IS 'Copyright text'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_copyright_transparency IS 'Copyright transparency (0..100)'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_focus IS 'Focus action enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_keymap IS 'Keymap enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_legend IS 'Legend enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_legend_print IS 'Legend enabled during map print ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_locate IS 'Locate action enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_logo IS 'Logo enabled ?'");
        $this->addSql("COMMENT ON COLUMN carmen.map_ui.ui_logopath IS 'Logo path'");
        $this->addSql("CREATE SEQUENCE carmen.map_ui_ui_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.map_ui_ui_id_seq OWNED BY carmen.map_ui.ui_id");
        $this->addSql("CREATE TABLE carmen.preferences (    preference_id integer NOT NULL,    preference_background_color character varying(7),    preference_background_transparency boolean,    preference_maxscale integer,    preference_minscale integer,    preference_outputformat character varying(10),    preference_srs integer,    preference_units character varying(20),    preferences_extent_xmax real,    preferences_extent_xmin real,    preferences_extent_ymax real,    preferences_extent_ymin real,    user_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.preferences IS 'Storage of map preferences by user'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_background_color IS 'Background color (hexa)'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_background_transparency IS 'Background transparency'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_maxscale IS 'maximal scale'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_minscale IS 'minimal scale'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_outputformat IS 'output format'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_srs IS 'projection EPSG code'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preference_units IS 'Measure units'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preferences_extent_xmax IS 'Map extent - XMAX'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preferences_extent_xmin IS 'Map extent - XMIN'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preferences_extent_ymax IS 'Map extent - YMAX'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.preferences_extent_ymin IS 'Map extent - YMIN'");
        $this->addSql("COMMENT ON COLUMN carmen.preferences.user_id IS 'account identifier'");
        $this->addSql("CREATE SEQUENCE carmen.preferences_preference_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.preferences_preference_id_seq OWNED BY carmen.preferences.preference_id");
        $this->addSql("CREATE TABLE carmen.ui_model (    ui_model_id integer NOT NULL,    account_model_id integer,    model_rank integer,    ui_id integer)");
        $this->addSql("COMMENT ON TABLE carmen.ui_model IS 'List of models used in map'");
        $this->addSql("COMMENT ON COLUMN carmen.ui_model.ui_model_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.ui_model.account_model_id IS 'modele id'");
        $this->addSql("COMMENT ON COLUMN carmen.ui_model.model_rank IS 'Model rank'");
        $this->addSql("COMMENT ON COLUMN carmen.ui_model.ui_id IS 'Map UI ID'");
        $this->addSql("CREATE SEQUENCE carmen.ui_model_ui_model_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.ui_model_ui_model_id_seq OWNED BY carmen.ui_model.ui_model_id");
        $this->addSql("CREATE TABLE carmen.user_audit (    user_id integer NOT NULL,    user_creation_date date NOT NULL,    user_last_connection_date date)");
        $this->addSql("COMMENT ON TABLE carmen.user_audit IS 'Statistics on users (create date, connection date, ...)'");
        $this->addSql("COMMENT ON COLUMN carmen.user_audit.user_creation_date IS 'User DB creation date'");
        $this->addSql("COMMENT ON COLUMN carmen.user_audit.user_last_connection_date IS 'User last connection date'");
        $this->addSql("CREATE TABLE carmen.users (    user_id integer NOT NULL,    account_id integer,    user_email text)");
        $this->addSql("COMMENT ON TABLE carmen.users IS 'List of users by account'");
        $this->addSql("COMMENT ON COLUMN carmen.users.user_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.users.account_id IS 'Account ID'");
        $this->addSql("COMMENT ON COLUMN carmen.users.user_email IS 'User email (sent by authentication service)'");
        $this->addSql("CREATE SEQUENCE carmen.users_user_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.users_user_id_seq OWNED BY carmen.users.user_id");
        $this->addSql("CREATE TABLE carmen.wxs (    wxs_id integer NOT NULL,    account_id integer,    wxs_name character varying(255),    wxs_rank integer,    wxs_type_id integer,    wxs_url character varying(1024))");
        $this->addSql("COMMENT ON TABLE carmen.wxs IS 'List of WXS services for the account'");
        $this->addSql("COMMENT ON COLUMN carmen.wxs.wxs_id IS 'primary key'");
        $this->addSql("COMMENT ON COLUMN carmen.wxs.account_id IS 'Account ID'");
        $this->addSql("COMMENT ON COLUMN carmen.wxs.wxs_name IS 'Service name'");
        $this->addSql("COMMENT ON COLUMN carmen.wxs.wxs_rank IS 'Service rank'");
        $this->addSql("COMMENT ON COLUMN carmen.wxs.wxs_type_id IS 'Service type'");
        $this->addSql("COMMENT ON COLUMN carmen.wxs.wxs_url IS 'Service url'");
        $this->addSql("CREATE SEQUENCE carmen.wxs_wxs_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.wxs_wxs_id_seq OWNED BY carmen.wxs.wxs_id");
        $this->addSql("CREATE TABLE parametrage.prodige_computed_field_info (    pk_view bigint NOT NULL,    field_name character varying DEFAULT 'new_field'::character varying NOT NULL,    expression character varying NOT NULL,    layers character varying(2500),    tables character varying(2500),    visible boolean DEFAULT true NOT NULL,    view_fields character varying(2500),    fieldtype character varying(200) DEFAULT 'varchar'::character varying NOT NULL)");
        $this->addSql("COMMENT ON TABLE parametrage.prodige_computed_field_info IS 'Prodige computed fields (in views) metadata table'");
        $this->addSql("COMMENT ON COLUMN parametrage.prodige_computed_field_info.view_fields IS 'computed fields of the view which is used in this field expression'");
        $this->addSql("COMMENT ON COLUMN parametrage.prodige_computed_field_info.fieldtype IS 'data type of the computed field'");
        $this->addSql("CREATE TABLE parametrage.prodige_database_requests (    prodige_database_request_id integer NOT NULL,    request_description character varying(30),    sql_request text)");
        $this->addSql("CREATE SEQUENCE parametrage.prodige_database_requests_prodige_database_request_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE parametrage.prodige_database_requests_prodige_database_request_id_seq OWNED BY parametrage.prodige_database_requests.prodige_database_request_id");
        $this->addSql("CREATE TABLE parametrage.prodige_download_param (    pk_critere_moteur integer,    critere_moteur_nom character varying,    critere_moteur_order integer,    critere_moteur_table character varying,    critere_moteur_champ_id character varying,    critere_moteur_champ_nom character varying,    critere_moteur_id_join integer,    critere_moteur_champ_join character varying,    critere_moteur_extractionattributaire integer DEFAULT 0 NOT NULL,    b_flux_atom integer DEFAULT 0)");
        $this->addSql("CREATE TABLE parametrage.prodige_grouping_agregate_info (    pk_view bigint NOT NULL,    field_name character varying(2000) NOT NULL,    field_agregate character varying(2000) NOT NULL,    field_pos integer NOT NULL)");
        $this->addSql("CREATE TABLE parametrage.prodige_grouping_info (    pk_view_composite bigint NOT NULL,    grouping_fields character varying(2000) NOT NULL,    grouping_name character varying(2000),    pk_view_origin bigint NOT NULL,    grouping_pos integer DEFAULT 1,    pk_view_grouped integer)");
        $this->addSql("CREATE TABLE parametrage.prodige_join_info (    pk_view bigint NOT NULL,    table_name character varying NOT NULL,    join_pos integer DEFAULT 1 NOT NULL,    join_type integer DEFAULT 0 NOT NULL,    join_criteria character varying DEFAULT ''::character varying NOT NULL,    table_fields character varying DEFAULT ''::character varying)");
        $this->addSql("COMMENT ON TABLE parametrage.prodige_join_info IS 'prodige joins (in views) metadata table'");
        $this->addSql("CREATE TABLE parametrage.prodige_msg_template (    pk_modele_id integer NOT NULL,    modele_name character varying(80) NOT NULL,    msg_title character varying(80) NOT NULL,    msg_body text,    layer_name character varying(80))");
        $this->addSql("CREATE SEQUENCE parametrage.prodige_msg_template_pk_modele_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE parametrage.prodige_msg_template_pk_modele_id_seq OWNED BY parametrage.prodige_msg_template.pk_modele_id");
        $this->addSql("CREATE TABLE parametrage.prodige_search_param (    pk_critere_moteur integer,    critere_moteur_nom character varying,    critere_moteur_order integer,    critere_moteur_table character varying,    critere_moteur_champ_id character varying,    critere_moteur_champ_nom character varying,    critere_moteur_id_join integer,    critere_moteur_champ_join character varying,    critere_moteur_extractionattributaire integer DEFAULT 0 NOT NULL)");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_settings    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE parametrage.prodige_settings (    pk_prodige_settings_id integer DEFAULT nextval('parametrage.seq_prodige_settings'::regclass) NOT NULL,    prodige_settings_constant text NOT NULL,    prodige_settings_title text,    prodige_settings_value text,    prodige_settings_desc text,    prodige_settings_type text DEFAULT 'textfield'::text NOT NULL)");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_settings_automate    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE parametrage.prodige_settings_automate (    pk_prodige_settings_automate_id integer DEFAULT nextval('parametrage.seq_prodige_settings_automate'::regclass) NOT NULL,    prodige_settings_constant text NOT NULL,    prodige_settings_title text,    prodige_settings_value text,    prodige_settings_desc text,    prodige_settings_type text DEFAULT 'textfield'::text NOT NULL)");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_settings_fluxatom    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE parametrage.prodige_settings_fluxatom (    pk_prodige_settings_fluxatom_id integer DEFAULT nextval('parametrage.seq_prodige_settings_fluxatom'::regclass) NOT NULL,    prodige_settings_constant text NOT NULL,    prodige_settings_title text,    prodige_settings_value text,    prodige_settings_desc text,    prodige_settings_type text DEFAULT 'textfield'::text NOT NULL)");
        $this->addSql("CREATE TABLE parametrage.prodige_view_composite_info (    pk_view bigint NOT NULL,    grouping_count integer DEFAULT 0,    view_name character varying(2000))");
        $this->addSql("CREATE TABLE parametrage.prodige_view_info (    pk_view bigint NOT NULL,    view_name character varying DEFAULT 'anonymous_view'::character varying NOT NULL,    layer_name character varying NOT NULL,    join_count integer DEFAULT 0 NOT NULL,    layer_fields character varying(2500) DEFAULT 'all'::character varying,    temporary timestamp with time zone,    view_type integer,    auxiliary_grouping boolean DEFAULT false,    sorting_field character varying(512) DEFAULT 'gid'::character varying NOT NULL,    sorting_method character varying(16) DEFAULT 'ASC'::character varying NOT NULL,    filter_expression character varying)");
        $this->addSql("COMMENT ON TABLE parametrage.prodige_view_info IS 'Prodige view metadata table'");
        $this->addSql("COMMENT ON COLUMN parametrage.prodige_view_info.temporary IS 'wheter the view should be automatically removed by a cron process or not. NULL implies the view is not temporary. The date corresponds to the creation date of the view.'");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_wmts_layers    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE parametrage.prodige_wmts_layers (    pk_prodige_wmts_layers_id integer DEFAULT nextval('parametrage.seq_prodige_wmts_layers'::regclass) NOT NULL,    prodige_wmts_settings_name character varying(255) NOT NULL,    prodige_wmts_settings_layer character varying(255) NOT NULL,    prodige_wmts_settings_scale text NOT NULL,    prodige_wmts_settings_extent character varying(255) NOT NULL,    prodige_wmts_settings_proj character varying(255) NOT NULL,    prodige_wmts_settings_format character varying(255) NOT NULL,    prodige_wmts_settings_status integer NOT NULL,    prodige_wmts_settings_time time without time zone)");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_wmts_settings    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE parametrage.prodige_wmts_settings (    pk_prodige_wmts_settings_id integer DEFAULT nextval('parametrage.seq_prodige_wmts_settings'::regclass) NOT NULL,    prodige_wmts_settings_constant text NOT NULL,    prodige_wmts_settings_title text,    prodige_wmts_settings_value text,    prodige_wmts_settings_desc text,    prodige_wmts_settings_type text DEFAULT 'textfield'::text)");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_download_param    START WITH 100    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE parametrage.seq_prodige_search_param    START WITH 100    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE public.departements (    gid integer NOT NULL,    the_geom public.geometry(LineString,2154),    id_geofla integer,    nature character varying,    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(the_geom) = 2)),    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(the_geom) = 2154)))");
        $this->addSql("CREATE SEQUENCE public.departements_ogc_fid_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE public.departements_ogc_fid_seq OWNED BY public.departements.gid");
        $this->addSql("CREATE SEQUENCE public.seq_contexte    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE public.seq_critere_moteur    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE public.v4_11_exec_updateviewsgeometrycolumns (    rownumber integer NOT NULL,    execution timestamp without time zone DEFAULT now(),    table_name text,    state text,    message text,    queries text)");
        $this->addSql("CREATE TABLE public.v4_11_memory_updateviewsgeometrycolumns (    now timestamp with time zone,    id bigint,    table_name text,    updateviewsgeometrycolumns text)");
        $this->addSql("CREATE TABLE public.v4_11_query_updateviewsgeometrycolumns (    id bigint,    table_name text,    updateviewsgeometrycolumns text)");
                
        $this->addSql("ALTER TABLE ONLY carmen.account ALTER COLUMN account_id SET DEFAULT nextval('carmen.account_account_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.account_model ALTER COLUMN account_model_id SET DEFAULT nextval('carmen.account_model_account_model_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.contact ALTER COLUMN contact_id SET DEFAULT nextval('carmen.contact_contact_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.favorite_area ALTER COLUMN area_id SET DEFAULT nextval('carmen.favorite_area_area_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.field ALTER COLUMN field_id SET DEFAULT nextval('carmen.field_field_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.keyword ALTER COLUMN keyword_id SET DEFAULT nextval('carmen.keyword_keyword_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.layer ALTER COLUMN layer_id SET DEFAULT nextval('carmen.layer_layer_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_analyse_type ALTER COLUMN analyse_type_id SET DEFAULT nextval('carmen.lex_analyse_type_analyse_type_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_category_keyword ALTER COLUMN category_id SET DEFAULT nextval('carmen.lex_category_keyword_category_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_color_id ALTER COLUMN color_id SET DEFAULT nextval('carmen.lex_color_id_color_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_dns ALTER COLUMN dns_id SET DEFAULT nextval('carmen.lex_dns_dns_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_field_datatype ALTER COLUMN field_datatype_id SET DEFAULT nextval('carmen.lex_field_datatype_field_datatype_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_field_type ALTER COLUMN field_type_id SET DEFAULT nextval('carmen.lex_field_type_field_type_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_inspire_theme ALTER COLUMN theme_id SET DEFAULT nextval('carmen.lex_inspire_theme_theme_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_condition ALTER COLUMN condition_id SET DEFAULT nextval('carmen.lex_iso_condition_condition_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_constraint ALTER COLUMN constraint_id SET DEFAULT nextval('carmen.lex_iso_constraint_constraint_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_theme ALTER COLUMN theme_id SET DEFAULT nextval('carmen.lex_iso_theme_theme_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_map_audit_status ALTER COLUMN map_audit_status_id SET DEFAULT nextval('carmen.lex_map_audit_status_map_audit_status_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_tool ALTER COLUMN tool_id SET DEFAULT nextval('carmen.lex_tool_tool_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_wxs_type ALTER COLUMN wxs_type_id SET DEFAULT nextval('carmen.lex_wxs_type_wxs_type_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.map ALTER COLUMN map_id SET DEFAULT nextval('carmen.map_map_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.map_annotation_attribute ALTER COLUMN attribute_id SET DEFAULT nextval('carmen.map_annotation_attribute_attribute_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.map_audit ALTER COLUMN map_audit_id SET DEFAULT nextval('carmen.map_audit_map_audit_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.map_group ALTER COLUMN group_id SET DEFAULT nextval('carmen.map_group_group_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.map_locator ALTER COLUMN locator_id SET DEFAULT nextval('carmen.map_locator_locator_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.map_ui ALTER COLUMN ui_id SET DEFAULT nextval('carmen.map_ui_ui_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.preferences ALTER COLUMN preference_id SET DEFAULT nextval('carmen.preferences_preference_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.ui_model ALTER COLUMN ui_model_id SET DEFAULT nextval('carmen.ui_model_ui_model_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.users ALTER COLUMN user_id SET DEFAULT nextval('carmen.users_user_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.wxs ALTER COLUMN wxs_id SET DEFAULT nextval('carmen.wxs_wxs_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_database_requests ALTER COLUMN prodige_database_request_id SET DEFAULT nextval('parametrage.prodige_database_requests_prodige_database_request_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_msg_template ALTER COLUMN pk_modele_id SET DEFAULT nextval('parametrage.prodige_msg_template_pk_modele_id_seq'::regclass)");
        $this->addSql("ALTER TABLE ONLY carmen.preferences ADD CONSTRAINT idx_preferences UNIQUE (user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.account_model ADD CONSTRAINT pk_account_model PRIMARY KEY (account_model_id)");
        $this->addSql("ALTER TABLE ONLY carmen.account ADD CONSTRAINT pk_accounts PRIMARY KEY (account_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_analyse_type ADD CONSTRAINT pk_analyse_type PRIMARY KEY (analyse_type_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_category_keyword ADD CONSTRAINT pk_category PRIMARY KEY (category_id)");
        $this->addSql("ALTER TABLE ONLY carmen.contact ADD CONSTRAINT pk_contact PRIMARY KEY (contact_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_dns ADD CONSTRAINT pk_dns PRIMARY KEY (dns_id)");
        $this->addSql("ALTER TABLE ONLY carmen.favorite_area ADD CONSTRAINT pk_favorite_areas PRIMARY KEY (area_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_field_datatype ADD CONSTRAINT pk_field_datatype PRIMARY KEY (field_datatype_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_field_type ADD CONSTRAINT pk_field_type PRIMARY KEY (field_type_id)");
        $this->addSql("ALTER TABLE ONLY carmen.field ADD CONSTRAINT pk_fields PRIMARY KEY (field_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_group ADD CONSTRAINT pk_group PRIMARY KEY (group_id)");
        $this->addSql("ALTER TABLE ONLY carmen.keyword ADD CONSTRAINT pk_keywords PRIMARY KEY (keyword_id)");
        $this->addSql("ALTER TABLE ONLY carmen.layer ADD CONSTRAINT pk_layer_0 PRIMARY KEY (layer_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_color_id ADD CONSTRAINT pk_lex_color_id PRIMARY KEY (color_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_inspire_theme ADD CONSTRAINT pk_lex_inspire_theme PRIMARY KEY (theme_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_constraint ADD CONSTRAINT pk_lex_iso_constraints PRIMARY KEY (constraint_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_condition ADD CONSTRAINT pk_lex_iso_constraints_0 PRIMARY KEY (condition_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_language ADD CONSTRAINT pk_lex_iso_languages PRIMARY KEY (language_code)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_iso_theme ADD CONSTRAINT pk_lex_iso_theme PRIMARY KEY (theme_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_layer_type ADD CONSTRAINT pk_lex_layer_type PRIMARY KEY (layer_type_code)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_map_audit_status ADD CONSTRAINT pk_lex_map_audit_status PRIMARY KEY (map_audit_status_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_projection ADD CONSTRAINT pk_lex_projections PRIMARY KEY (projection_epsg)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_units ADD CONSTRAINT pk_lex_units PRIMARY KEY (unit_code)");
        $this->addSql("ALTER TABLE ONLY carmen.map ADD CONSTRAINT pk_map PRIMARY KEY (map_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_annotation_attribute ADD CONSTRAINT pk_map_annotations_attributs PRIMARY KEY (attribute_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_audit ADD CONSTRAINT pk_map_audit PRIMARY KEY (map_audit_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_locator ADD CONSTRAINT pk_map_locator PRIMARY KEY (locator_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tool ADD CONSTRAINT pk_map_tool PRIMARY KEY (map_id, tool_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tree ADD CONSTRAINT pk_map_tree PRIMARY KEY (node_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_tree_print ADD CONSTRAINT pk_map_tree_print PRIMARY KEY (node_id)");
        $this->addSql("ALTER TABLE ONLY carmen.map_ui ADD CONSTRAINT pk_map_ui PRIMARY KEY (ui_id)");
        $this->addSql("ALTER TABLE ONLY carmen.preferences ADD CONSTRAINT pk_preferences PRIMARY KEY (preference_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_tool ADD CONSTRAINT pk_tool PRIMARY KEY (tool_id)");
        $this->addSql("ALTER TABLE ONLY carmen.ui_model ADD CONSTRAINT pk_ui_model PRIMARY KEY (ui_model_id)");
        $this->addSql("ALTER TABLE ONLY carmen.users ADD CONSTRAINT pk_user PRIMARY KEY (user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.user_audit ADD CONSTRAINT pk_user_audit PRIMARY KEY (user_id)");
        $this->addSql("ALTER TABLE ONLY carmen.wxs ADD CONSTRAINT pk_wxs PRIMARY KEY (wxs_id)");
        $this->addSql("ALTER TABLE ONLY carmen.lex_wxs_type ADD CONSTRAINT pk_wxs_type PRIMARY KEY (wxs_type_id)");
        $this->addSql("ALTER TABLE ONLY carmen.users ADD CONSTRAINT uk_user_email UNIQUE (user_email)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_settings ADD CONSTRAINT pk_prodige_settings PRIMARY KEY (pk_prodige_settings_id)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_settings_automate ADD CONSTRAINT pk_prodige_settings_automate PRIMARY KEY (pk_prodige_settings_automate_id)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_settings_fluxatom ADD CONSTRAINT pk_prodige_settings_fluxatom PRIMARY KEY (pk_prodige_settings_fluxatom_id)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_wmts_settings ADD CONSTRAINT pk_prodige_wmts_settings PRIMARY KEY (pk_prodige_wmts_settings_id)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_computed_field_info ADD CONSTRAINT prodige_computed_field_info_pkey PRIMARY KEY (pk_view, field_name)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_grouping_info ADD CONSTRAINT prodige_grouping_info_pk PRIMARY KEY (pk_view_composite, pk_view_origin)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_join_info ADD CONSTRAINT prodige_join_info_pkey PRIMARY KEY (pk_view, table_name, join_pos)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_msg_template ADD CONSTRAINT prodige_msg_template_pkey PRIMARY KEY (pk_modele_id)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_view_composite_info ADD CONSTRAINT prodige_view_composite_info_pk PRIMARY KEY (pk_view)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_database_requests ADD CONSTRAINT request_pk PRIMARY KEY (prodige_database_request_id)");
        $this->addSql("ALTER TABLE ONLY parametrage.prodige_view_info ADD CONSTRAINT view_info_pkey PRIMARY KEY (pk_view)");
        $this->addSql("ALTER TABLE ONLY public.departements ADD CONSTRAINT departements_pk PRIMARY KEY (gid)");
        $this->addSql("ALTER TABLE ONLY public.v4_11_exec_updateviewsgeometrycolumns ADD CONSTRAINT pk_exec PRIMARY KEY (rownumber)");
        $this->addSql("CREATE INDEX idx_account ON carmen.account USING btree (account_dns_id)");
        $this->addSql("CREATE INDEX idx_account_model ON carmen.account_model USING btree (account_id)");
        $this->addSql("CREATE INDEX idx_contact ON carmen.contact USING btree (user_id)");
        $this->addSql("CREATE INDEX idx_favorite_areas ON carmen.favorite_area USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_fields ON carmen.field USING btree (layer_id)");
        $this->addSql("CREATE INDEX idx_fields_0 ON carmen.field USING btree (field_type_id)");
        $this->addSql("CREATE INDEX idx_group ON carmen.map_group USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_keywords ON carmen.keyword USING btree (category_id)");
        $this->addSql("CREATE INDEX idx_layer ON carmen.layer USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_layer_0 ON carmen.layer USING btree (layer_analyse_type_id)");
        $this->addSql("CREATE INDEX idx_layer_type_id ON carmen.layer USING btree (layer_type_code)");
        $this->addSql("CREATE INDEX idx_map ON carmen.map USING btree (account_id)");
        $this->addSql("CREATE INDEX idx_map_0 ON carmen.map USING btree (map_projection_epsg)");
        $this->addSql("CREATE INDEX idx_map_1 ON carmen.map USING btree (user_id)");
        $this->addSql("CREATE INDEX idx_map_annotations_attributs ON carmen.map_annotation_attribute USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_audit ON carmen.map_audit USING btree (account_id)");
        $this->addSql("CREATE INDEX idx_map_audit_0 ON carmen.map_audit USING btree (map_file)");
        $this->addSql("CREATE INDEX idx_map_audit_1 ON carmen.map_audit USING btree (map_audit_status)");
        $this->addSql("CREATE INDEX idx_map_audit_2 ON carmen.map_audit USING btree (user_email)");
        $this->addSql("CREATE INDEX idx_map_keyword ON carmen.map_keyword USING btree (map_id, keyword_id)");
        $this->addSql("CREATE INDEX idx_map_keywords ON carmen.map_keyword USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_keywords_0 ON carmen.map_keyword USING btree (keyword_id)");
        $this->addSql("CREATE INDEX idx_map_locator ON carmen.map_locator USING btree (locator_criteria_related)");
        $this->addSql("CREATE INDEX idx_map_locator_0 ON carmen.map_locator USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_tools ON carmen.map_tool USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_tools_0 ON carmen.map_tool USING btree (tool_id)");
        $this->addSql("CREATE INDEX idx_map_tree ON carmen.map_tree USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_tree_print ON carmen.map_tree_print USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_ui ON carmen.map_ui USING btree (map_id)");
        $this->addSql("CREATE INDEX idx_map_ui_0 ON carmen.map_ui USING btree (ui_color_id)");
        $this->addSql("CREATE INDEX idx_preferences_0 ON carmen.preferences USING btree (preference_units)");
        $this->addSql("CREATE INDEX idx_ui_model ON carmen.ui_model USING btree (ui_id)");
        $this->addSql("CREATE INDEX idx_ui_model_0 ON carmen.ui_model USING btree (account_model_id)");
        $this->addSql("CREATE INDEX idx_user ON carmen.users USING btree (account_id)");
        $this->addSql("CREATE INDEX idx_wxs ON carmen.wxs USING btree (account_id)");
        $this->addSql("CREATE INDEX idx_wxs_0 ON carmen.wxs USING btree (wxs_type_id)");
        $this->addSql("CREATE INDEX departements_geom_idx ON public.departements USING gist (the_geom)");
        $this->addSql("CREATE INDEX idx_gist_departements ON public.departements USING gist (the_geom)");
        
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to public');
        $this->addSql('DROP SCHEMA carmen cascade');
        $this->addSql('DROP SCHEMA local_data cascade');
        $this->addSql('DROP SCHEMA parametrage cascade');

        $this->addSql("DROP TABLE public.departements cascade");
        $this->addSql("DROP SEQUENCE if exists public.departements_ogc_fid_seq");
        $this->addSql("DROP SEQUENCE if exists public.seq_contexte");
        $this->addSql("DROP SEQUENCE if exists public.seq_critere_moteur");
        $this->addSql("DROP TABLE public.v4_11_exec_updateviewsgeometrycolumns cascade");
        $this->addSql("DROP TABLE public.v4_11_memory_updateviewsgeometrycolumns cascade");
        $this->addSql("DROP TABLE public.v4_11_query_updateviewsgeometrycolumns cascade");

        $this->addSql("DROP FUNCTION if exists public.st_aslatlontext(public.geometry)");
        $this->addSql("DROP FUNCTION public.updateviewsgeometrycolumns()");
        $this->addSql('DROP DOMAIN public.image');
    }
}
