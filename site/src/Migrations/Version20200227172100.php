<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200227172100 extends AbstractMigration
{
    public function getDescription():string
    {
        return 'Version tampon :  Ajout de la conf doctrine si elle n\'existe pas';
    }

    public function up(Schema $schema):void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (FALSE, 'infoTooltip', 'tool')");
        $this->addSql("insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='info') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='infoTooltip')");

        // si la base est déjà dans cette version, il faut exécuter ces requêtes à la base PRODIGE puis jouer la procédure de mise à jour
        
        /*
        CREATE TABLE public.doctrine_migration_prodige (version character varying(14) NOT NULL, executed_at timestamp(0) without time zone NOT NULL);
        ALTER TABLE public.doctrine_migration_prodige OWNER TO user_prodige;
        COMMENT ON COLUMN public.doctrine_migration_prodige.executed_at IS '(DC2Type:datetime_immutable)';
        INSERT INTO public.doctrine_migration_prodige VALUES ('20191130040100', '2019-12-01 12:12:12');
        INSERT INTO public.doctrine_migration_prodige VALUES ('20191130040102', '2019-12-01 12:12:22');
        INSERT INTO public.doctrine_migration_prodige VALUES ('20191130040103', '2019-12-01 12:12:32');
        INSERT INTO public.doctrine_migration_prodige VALUES ('20191130040104', '2019-12-01 12:12:42');
        INSERT INTO public.doctrine_migration_prodige VALUES ('20200129105500', '2020-01-29 11:00:00');
        INSERT INTO public.doctrine_migration_prodige VALUES ('20200227172100', '2020-02-27 18:00:00');
        ALTER TABLE ONLY public.doctrine_migration_prodige ADD CONSTRAINT doctrine_migration_prodige_pkey PRIMARY KEY (version);
        */
    }

    public function down(Schema $schema):void
    {
        // mettre en commentaire cette ligne si vous souhaitez réellement réinitialiser votre base.
        $this->abortIf(true, 'Si vous continuez, vous risquez de perdre toutes vos données. Traitement interrompu par sécurité.');
    }
}

