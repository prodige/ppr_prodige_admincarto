<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210818093615 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO carmen.lex_wxs_type(wxs_type_id, wxs_type_name) VALUES (6, 'vector_tile');");
        $this->addSql("INSERT INTO carmen.lex_wxs_type(wxs_type_id, wxs_type_name) VALUES (7, 'vector_tile_style');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM carmen.lex_wxs_type where wxs_type_id =  6 and wxs_type_name = vector_tile");
        $this->addSql("DELETE FROM carmen.lex_wxs_type where wxs_type_id =  7 and wxs_type_name = vector_tile_style");
    }
}
