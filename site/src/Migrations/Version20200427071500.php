<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200427071500 extends AbstractMigration
{
    public function getDescription():string
    {
        return 'Version 4.2.5';
    }

    public function up(Schema $schema) :void
    {
        //Creation schema dans prodige
        $this->addSql("CREATE SCHEMA bdterr;");
        // Ajout table de mapping insee pour les lots (dans le schema bdterr de prodige)
        $this->addSql("CREATE TABLE bdterr.bdterr_mapping (
            mapping_lot_id integer NOT NULL,
            mapping_object_id integer NOT NULL,
            mapping_insee text NOT NULL
        );");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_mapping ADD CONSTRAINT bdterr_mapping_pkey PRIMARY KEY (mapping_lot_id, mapping_object_id, mapping_insee);");

    }

    public function down(Schema $schema) :void
    {
        $this->addSql("DROP SCHEMA bdterr;");
    }
}

