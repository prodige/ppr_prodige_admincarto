<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\Process\Process;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231019000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $cmd = 'bash /var/www/html/site/bin/admincarto_clean_wxs.sh';
        $process = Process::fromShellCommandline($cmd);
        $process->setTimeout(10800);
        $process->run();
        echo  $process->getOutput();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
