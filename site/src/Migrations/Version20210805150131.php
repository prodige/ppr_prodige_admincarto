<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210805150131 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO carmen.lex_display_mode(display_mode_id, display_mode_name) VALUES (3, 'tile_vector');");

        // insertion du type geojson
        $this->addSql(
            "INSERT INTO carmen.lex_layer_type(layer_type_code, layer_type_name) SELECT 'TILE_VECTOR', 'Tile vector'
            WHERE NOT EXISTS (
                SELECT layer_type_code FROM carmen.lex_layer_type WHERE layer_type_code = 'TILE_VECTOR'
            );"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
