<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200922090059 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        // this up() migration is auto-generated, please modify it to your needs
        
        // Ajout table de mapping insee pour les lots (dans le schema bdterr de prodige)
        $this->addSql("CREATE TABLE carmen.lex_display_mode (
            display_mode_id integer NOT NULL,
            display_mode_name text NOT NULL
        );");

        
        $this->addSql("ALTER TABLE ONLY carmen.lex_display_mode ADD CONSTRAINT pk_display_mode PRIMARY KEY (display_mode_id)");
        $this->addSql("CREATE SEQUENCE carmen.lex_display_mode_display_mode_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER SEQUENCE carmen.lex_display_mode_display_mode_id_seq OWNED BY carmen.lex_display_mode.display_mode_id");
        

        $this->addSql("CREATE INDEX idx_display_mode ON carmen.lex_display_mode USING btree (display_mode_id)");

        $this->addSql("ALTER TABLE ONLY carmen.lex_display_mode ALTER COLUMN display_mode_id SET DEFAULT nextval('carmen.lex_display_mode_display_mode_id_seq'::regclass)");

        $this->addSql("INSERT INTO carmen.lex_display_mode(display_mode_name) VALUES('vector')");
        $this->addSql("INSERT INTO carmen.lex_display_mode(display_mode_name) VALUES('image')");

        $this->addSql("ALTER TABLE carmen.Layer ADD COLUMN layer_display_mode_id integer");
        $this->addSql("ALTER TABLE ONLY carmen.layer ADD CONSTRAINT fk_lex_display_mode FOREIGN KEY (layer_display_mode_id) REFERENCES carmen.lex_display_mode(display_mode_id)");
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE carmen.layer DROP CONSTRAINT  if exists fk_lex_display_mode");
        $this->addSql("ALTER TABLE carmen.layer DROP COLUMN if exists layer_display_mode_id");
        $this->addSql("DROP TABLE if exists carmen.lex_display_mode");
        $this->addSql("DROP SEQUENCE if exists carmen.lex_display_mode_display_mode_id_seq");
        $this->addSql("DROP INDEX if exists idx_display_mode");

      
    }
}
