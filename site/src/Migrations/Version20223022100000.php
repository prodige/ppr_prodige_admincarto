<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20223022100000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon :  Ajout de la conf doctrine si elle n\'existe pas';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (TRUE, 'infoExport', 'tool')");
        $this->addSql("insert into carmen.map_tool (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='info') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='infoExport')");

    }

    public function down(Schema $schema): void
    {
        // mettre en commentaire cette ligne si vous souhaitez réellement réinitialiser votre base.
        $this->abortIf(true, 'Si vous continuez, vous risquez de perdre toutes vos données. Traitement interrompu par sécurité.');
    }
}

