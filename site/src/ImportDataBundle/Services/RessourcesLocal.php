<?php
class RessourcesLocal
{
		//racine du repertoire du serveur Carto
		//deporte des FTP
		//public static $ROOT_FTP					= "/home/prodige/cartes/";

		public static $GEOM_POSTGIS				= "the_geom";
		public static $GID_POSTGIS				= "gid";

		//Parametres de convertion de fichier
	//	public static $CONVERT_REP				= "/applications/carmen/mapimage/tmpimport/";

		//Parametres du nommage des tables d'archivages
		//des données sur le serveur Carto deporte
		public static $PREFIXE_TBL_ARC			= "";
		public static $POSTFIXE_TBL_ARC			= "_ARC";

		//Commande IMPORTATION
		// mettre dans le path: C:\\Program Files\\FWTools1.3.2\\bin
		// mettre dans le path: C:\\Program Files\\PostgreSQL\\8.2\\bin

		//charlotte remettre
		//public static $CMD_BIN_SHP2SQL        = "/applications/postgis/bin/shp2pgsql";
		//public static $CMD_BIN_SHP2SQL				= "shp2pgsql";
		//charlotte remettre
		//public static $CMD_BIN_PGSQL				= "/applications/postgres/bin/psql";
		//public static $CMD_BIN_PGSQL        = "psql";
		//charlotte remettre 
		//public static $CMD_BIN_TAB2SHP				= "/applications/lib/gdal141/bin/ogr2ogr";
		//public static $CMD_BIN_OGR2OGR        = "ogr2ogr";
		//set PGCLIENTENCODING = Latin 1 (encoding of the data) because database in utf8
		public static $CMD_BIN_PGCLIENTENCODING = "export PGCLIENTENCODING=";
    
    // table de correspondance PG => OGR
    public static $PG_OGR_TYPE = array("int4" => array("ogr" => array("Integer"),
                                                       "type" => "Numérique"
                                                       ),
                                       "int2" => array("ogr" => array("Integer"),
                                                       "type" => "Numérique"
                                                       ),
                                       "int8" => array("ogr" => array("Integer"),
                                                       "type" => "Numérique"
                                                       ),
                                       "float8" => array("ogr" => array("Real"),
                                                         "type" => "Numérique"
                                                         ),
                                       "float4" => array("ogr" => array("Real"),
                                                         "type" => "Numérique"
                                                         ),
                                       "numeric" => array("ogr" => array("Integer", "Real"),
                                                          "type" => "Numérique"
                                                          ),
                                       "varchar" => array("ogr" => array("String"),
                                                          "type" => "Chaine de caractères"
                                                          ),
                                       "bpchar" => array("ogr" => array("String"),
                                                         "type" => "Chaine de caractères"
                                                         ),
                                       "text" => array("ogr" => array("String"),
                                                       "type" => "Chaine de caractères"
                                                       ),
                                       "date" => array("ogr" => array("Date"),
                                                       "type" => "Date"
                                                      ),
                                       "timestamp" => array("ogr" => array("Timestamp"),
                                                      "type" => "Timestamp"
                                                      )               
                                    );
    
    
}
