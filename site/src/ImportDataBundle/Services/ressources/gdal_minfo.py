#!/usr/bin/env python

###################################"
# (c) Alkante 2010 (obedel)
#  gdalinfo like utility...
###################################"


try:
    from osgeo import gdal
except ImportError:
    import gdal
try:
    from osgeo import gdalconst
except ImportError:
    import gdalconst

import sys

verbose = 1


# *****************************************************************************
class file_info:
    """A class holding information about a GDAL file."""

    def init_from_name(self, filename):
        """
        Initialize file_info from filename

        filename -- Name of file to read.

        Returns 1 on success or 0 if the file can't be opened.
        """
        fh = gdal.Open( filename )
        if fh is None:
            return 0
            
        firstBand = fh.GetRasterBand(1)	

        self.filename = filename
        self.bands = fh.RasterCount
        self.xsize = fh.RasterXSize
        self.ysize = fh.RasterYSize
        self.band_type = fh.GetRasterBand(1).DataType
        if (fh.GetRasterBand(1).GetRasterColorInterpretation()==gdal.GCI_GrayIndex) or (fh.GetRasterBand(1).GetRasterColorInterpretation()==gdal.GCI_PaletteIndex):
            self.band_colorInterp = 0
        else:
            self.band_colorInterp = 1
        self.projection = fh.GetProjection()
        self.geotransform = fh.GetGeoTransform()
        self.ulx = self.geotransform[0]
        self.uly = self.geotransform[3]
        self.lrx = self.ulx + self.geotransform[1] * self.xsize
        self.lry = self.uly + self.geotransform[5] * self.ysize

        ct = fh.GetRasterBand(1).GetRasterColorTable()
        if ct is not None:
            self.ct = ct.Clone()
        else:
            self.ct = None

        return 1

    def report( self ):
        print('Filename: ')+ self.filename
        print('File Size: %dx%dx%d') \
              % (self.xsize, self.ysize, self.bands)
        print('Pixel Size: %f x %f') \
              % (self.geotransform[1],self.geotransform[5])
        print('UL:(%f,%f)   LR:(%f,%f)') \
              % (self.ulx,self.uly,self.lrx,self.lry)
        
    def print_summary( self ):
        self.outputFormat = "%d,%d,%d,%f %f %f %f, %f, %f, %s"
        print(self.outputFormat % ( self.bands, self.xsize, self.ysize, 
            self.ulx, self.uly, self.lrx, self.lry, 
            self.geotransform[1], self.geotransform[5], self.band_colorInterp))

# =============================================================================
def Usage():
    print('Usage: gdal_minfo.py [-s] input_file')
    print('   output in the form : nbBands, cols, rows, ulx uly lrx lry, resx, resy')

# =============================================================================
#
# Program mainline.
#

if __name__ == '__main__':
    summary = 0
    names = []
 
    gdal.AllRegister()
    argv = gdal.GeneralCmdLineProcessor( sys.argv )
    if argv is None:
        sys.exit( 0 )

    # Parse command line arguments.
    i = 1
    while i < len(argv):
        arg = argv[i]

        if arg == '-s':
            summary = 1

        else:
            names.append( arg )
            
        i = i + 1

    if len(names) == 0:
        print('No input files selected.')
        Usage()
        sys.exit( 1 )

    # Collect information on all the source files.
    fi = file_info()
    if fi.init_from_name( names[0] ) == 1:
    	fi.print_summary()
