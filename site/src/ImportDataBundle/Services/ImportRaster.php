<?php
namespace Carmen\ImportDataBundle\Services;
use Symfony\Component\Process\Process;

//Date de creation: 31 mai 2006
//Description: Classe permettant la gestion des imports de donnees POSTGIS

/**
 * Procède à l'import des données géographiques de type Raster :
 * - accepte les formats raster et index de rasters par Shapefiles
 * - génération d'un fichier VRT par donnée source
 */
class ImportRaster {
	protected static $db_connection;
	protected static $CMD_OGR2OGR;
	
	
	protected static $CMD_GDAL_VRTMERGE;
	protected static $CMD_GDAL_MININFO;
	/**
	 * @param \Doctrine\DBAL\Connection $db_connection
	 */
	public static function initCommands($CMD_OGR2OGR){
		self::$CMD_OGR2OGR = $CMD_OGR2OGR;
		
		self::$CMD_GDAL_VRTMERGE = "python3 ".__DIR__."/ressources/gdal_vrtmerge.py";
		self::$CMD_GDAL_MININFO  = "python3 ".__DIR__."/ressources/gdal_minfo.py";
	}
	
	protected static function shptovrt($fileInfo, $dataPath) {
		
		$fullname = $fileInfo['dirname'] . '/' . $fileInfo['basename']; 
		
		$required_extensions = array("shp", "dbf", "shx");
		$files = glob( $fileInfo['dirname'] . '/' . $fileInfo['filename'].'.*');
		$found_extensions = array();
		foreach($files as $file){
			$extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
			if ( in_array($extension, $required_extensions) ) $found_extensions[] = $extension;
		}
		if ( count($found_extensions)<count($required_extensions) ){
			throw new \Exception("Pour la donnée Shapefile ".str_replace($dataPath, "", $fullname).", il manque le(s) fichier(s) d'extension '".implode("', '", array_diff($required_extensions, $found_extensions))."'") ;
		}
		$filename = $fileInfo['filename'];
		$csv = $dataPath . "tmp/" . $filename . ".csv";
        
        @mkdir (dirname($csv), 0750);
        @unlink($csv);
		
		$errors = array();
		$status = null; 

		if (file_exists($csv))
			throw new \Exception("Unable to delete temp file");	
		
        $cmd = self::$CMD_OGR2OGR . ' -f CSV "'.$csv.'" "'.$fullname.'"';
		$process = Process::fromShellCommandline($cmd);
        $process->setTimeout(null);
        $process->run(); 
        $errors = explode (" ", $process->getOutput());
        $status = $process->getErrorOutput();                 

		if (!file_exists($csv))
			throw new \Exception("Unable to perform " . $cmd. " : [status={$status}] [return={$return}]".print_r($errors, 1)) ;
				
			$vrtname = tempnam($dataPath . "raster_info/", $filename);
			@unlink($vrtname);
			$vrtFullname = $vrtname . ".vrt";
                        

                
		$errors = array(); $status = null; 
		$cmd = self::$CMD_GDAL_VRTMERGE .' -o "'.$vrtFullname.'" "'.$csv.'" 2>&1';
		$process = Process::fromShellCommandline($cmd);
		$process->setTimeout(null);
		$process->run(); 
		$errors = explode (" ", $process->getOutput());
		$status = $process->getErrorOutput();                    
                
		if (!file_exists($vrtFullname))
			throw new \Exception("Unable to perform " . $cmd. " : [status={$status}] [return={$return}]".print_r($errors, 1));
		
		return $vrtFullname;
	}
	
	protected static function tovrt($fileInfo, $dataPath) {
		$fullname = $fileInfo['dirname'] . '/' . $fileInfo['basename']; 
		$filename = $fileInfo['filename'];
		$vrtname = tempnam($dataPath . "raster_info/", $filename);
		@unlink($vrtname);
		$vrtFullname = $vrtname . ".vrt";
                
		$errors = array(); $status = null; 
		$cmd = self::$CMD_GDAL_VRTMERGE .' -o "'.$vrtFullname.'" "'.$fullname.'" 2>&1';
		
		$process = Process::fromShellCommandline($cmd);
		$process->setTimeout(null);
		$process->run(); 
		$errors = explode (" ", $process->getOutput());
		$status = $process->getErrorOutput();   

		if (!file_exists($vrtFullname))
			throw new \Exception("Unable to perform " . $cmd. " : [status={$status}] [return={$return}]".print_r($errors, 1));
		return $vrtFullname;
	}
	
	// raster_info : 0 => srid, 
	// 1,4 =>  ulx, uly, lrx, lry, 
	// 5 => bands, 
	// 6,7 => cols, rows, 
	// 8,9 => resx, resy, 
	// 10 => format, 
	// 11 => vrt_path, 
	// 12 => compression
	// 13 => color_interp
	protected static function vrtToInfo($vrtFullname) {
		
		
		$returned = array();
		$status = null;
		$process_cmd =  self::$CMD_GDAL_MININFO . " -s '".$vrtFullname."' 2>&1"    ;    
		$process = Process::fromShellCommandline($process_cmd);
		$process->setTimeout(null);
		$process->run(); 
		$errors = explode (" ", $process->getOutput());
		$res = $process->getOutput();
		$status = $process->getErrorOutput();   


                
                //$cmd = self::$CMD_GDAL_MININFO .' -s "'.$vrtFullname.'" 2>&1';
		//$res = exec($cmd, $returned, $status);
		if ($status!=0)
			throw new \Exception("Error in executing " .$cmd. " : ".print_r($errors, 1));
		
		list( $bands, $cols, $rows, $extent, $resx, $resy, $color_interp ) = explode(",", $res);
		list( $ulx, $uly, $lrx, $lry )                                     = explode(" ", $extent); 
		
		return compact(
			"ulx", 
			"uly", 
			"lrx", 
			"lry", 
			"bands", 
			"cols", 
			"rows", 
			"resx", 
			"resy", 
			"color_interp"
		);
// 		id	bigint	
// name	character varying	
// srid	integer	

// format	character varying	
// vrt_path	character varying	
// compression	character varying	

	}
	
	/**
	 * convert raster to VRT format
	 * @param string $dataDirectory  the storage directory of GIS data
	 * @param string $rasterFile     the relative path from dataDirectory to the raster file
	 */
	public static function rasterToVRT($pk_couche_donnees, $dataDirectory, $rasterFile, $defaultProjection)
	{			
		if (!file_exists($rasterFile) )
			throw new \Exception("Missing parameter filename ".$rasterFile);
		
		// building vrt files and getting raster infos descriptions
		// raster_info : 0 => srid, 
		// 1,4 =>  ulx, uly, lrx, lry, 
		// 5 => bands, 
		// 6,7 => cols, rows, 
		// 8,9 => resx, resy, 
		// 10 => format, 
		// 11 => vrt_path, 
		// 12 => compression
		// 13 => color interpretation (0->palette, 1->bands)
		
		if (file_exists($rasterFile) && is_file($rasterFile)) {
			$info_path = pathinfo($rasterFile);
			$extension = strtoupper($info_path['extension']);
			$rasterInfo = array("id"=>$pk_couche_donnees, "name"=>"1_".$pk_couche_donnees);
		
			if ($extension=="SHP") {
				$rasterInfo["format"] = "TILED";
				$rasterInfo["vrt_path"] = self::shptovrt($info_path, $dataDirectory);
			}
			else { 
				if ($extension=="TIFF" || $extension=="TIF")
					$rasterInfo["format"] = "GTIF";
				else if ($extension=="ECW")
					$rasterInfo["format"] = "ECW";
				else if ($extension=="JPEG" || $extension=="JPG" || $extension=="JP2") 
					$rasterInfo["format"] = "JPG";
				else 
					throw new \Exception("Unknown raster file format (" . $info_path['extension'] . ")");
				
				$rasterInfo["vrt_path"] = self::tovrt($info_path, $dataDirectory);
			}
			$rasterInfo["srid"] = $defaultProjection; // default projection of the platform
			$rasterInfo["compression"] = "NONE";
			$rasterInfo = array_merge($rasterInfo, self::vrtToInfo($rasterInfo["vrt_path"]));
		}
		else {
			throw new \Exception("Unable to access file " . $rasterFile);
		}
		
		
		return $rasterInfo;
	}
}
