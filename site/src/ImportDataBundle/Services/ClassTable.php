<?php

class Table {
  
  protected $strFileName;
  protected $tabField;
  protected $tabError;
  
  function __construct($strFileName)
  {
    $this->strFileName = $strFileName;
    $this->tabField = array();
    $this->tabError = array();
  }
  
  /**
   * add an error to errors
   * @param title  title of the error
   * @param msg    msg of the error
   */
  protected function error($title, $msg)
  {
    $this->tabError[] = array("title" => $title, "msg" => $msg);
  }
  
  /**
   * @return array tab errors
   */
  public function getTabError()
  {
    return $this->tabError;
  }
  
  /**
   * @return string message errors
   */
  public function getMsgError()
  {
    $msg = "";
    foreach ( $this->tabError as $TabTitleMsg ) {
      $msg.= $TabTitleMsg["title"]." : ".$TabTitleMsg["msg"]."<br/>";
    }
    return $msg;
  }
  
  /**
   * create table into database
   * @return boolean true if table created, false otherwise
   */
  protected function createTable($strLeafName, $dbconn, $strTableName)
  {
    $strSql = "create sequence seq_".$strTableName." INCREMENT BY 1  NO MAXVALUE  NO MINVALUE CACHE 1;";
    $strSql.= "CREATE TABLE ".$strTableName." (gid integer DEFAULT nextval('seq_".$strTableName."'::regclass) NOT NULL,
    ";
    foreach ( $this->tabField as $iNumColumn => $tabNameType ) {
      $strSql.= "\"".strtolower($tabNameType['name'])."\" ";
      switch ( $tabNameType['type'] ) {
        case "date" :
          $strSql.= "date";
        break;
        case "number" :
        case "float" :
          $strSql.= "double precision";
        break;
        default :
          $strSql.= "varchar";
        break;
      }
      $strSql.= ",";
    }
    $strSql = mb_substr($strSql, 0, -1);
    $strSql.= ")";
    
    $strSql = utf8_decode($strSql);
    
    if ( !pg_query($dbconn,$strSql) ) {
      $this->error("Création de la table échouée", "Impossible de créer la table \"".$strTableName."\" en base de donnée");
      $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
      return false;
    }
    
    return true;
  }
  
  /**
   * delete table from database
   * @return boolean true if table deleted, false otherwise
   */
  protected function deleteTable($dbconn, $strTableName)
  {
    
    // check if table exists before deleting
    $strSql = "SELECT relname FROM pg_class WHERE relname='".$strTableName."'";
    $rs = pg_query($dbconn, $strSql);
    if ( $rs === false ) {
      $this->error("Vérification de la table échoué", "Impossible de vérifier l'existence de la table \"".$strTableName."\" en base de donnée");
      $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
      return false;
    }
    if ( !pg_fetch_all($rs) ) return true;
    
    $strSql = "";
    $strSql.= "DROP TABLE ".$strTableName." CASCADE;";
    $strSql.= "DROP SEQUENCE seq_".$strTableName.";";
    if ( !pg_query($dbconn, $strSql) ) {
      $this->error("Suppression de la table échouée", "Impossible de supprimer la table \"".$strTableName."\" en base de donnée");
      $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
      return false;
    }
    
    return true;
  }
  
}
?>
