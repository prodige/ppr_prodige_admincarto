<?php
/**
 * Convert OpenDocument Spreadsheet to csv (semicolon)
 *
 * @author Anakeen 2000
 * @version $Id: ods2csv.php,v 1.8 2008/12/02 15:22:32 eric Exp $
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package FREEDOM
 * @subpackage
 */
/**
 */

use Symfony\Component\Process\Process;

define ("SEPCHAR", ',');
define ("ALTSEPCHAR", ' --- ');

$inrow=false;
$incell=false;
$nrow=0;
$ncol=0;
$rows=array();
$colrepeat=0;
function startElement($parser, $name, $attrs) {
  global $rows,$nrow,$inrow,$incell,$ncol,$colrepeat,$celldata;
  if ($name ==  "TABLE:TABLE-ROW") {
    $inrow=true;
    if (isset($rows[$nrow])) {
      // fill empty cells
      $idx=0;
      foreach ($rows[$nrow] as $k=>$v) {
        if (! isset($rows[$nrow][$idx])) $rows[$nrow][$idx]='';
        $idx++;
      }
      ksort($rows[$nrow],SORT_NUMERIC);
    }
    $nrow++;
    $ncol=0;
    $rows[$nrow] = array();
  }

  if ($name ==  "TABLE:TABLE-CELL") {
    $incell=true;
    $celldata = "";
    if (array_key_exists("TABLE:NUMBER-COLUMNS-REPEATED", $attrs) && $attrs["TABLE:NUMBER-COLUMNS-REPEATED"]) {
      $colrepeat=intval($attrs["TABLE:NUMBER-COLUMNS-REPEATED"]);
    }
  }
  if ($name ==  "TEXT:P") {
    if (array_key_exists($ncol, $rows[$nrow]) && strlen($rows[$nrow][$ncol]) > 0) $rows[$nrow][$ncol].='\n';
  }
}

function endElement($parser, $name) {
  global $rows,$nrow,$inrow,$incell,$ncol,$colrepeat,$celldata;
  if ($name ==  "TABLE:TABLE-ROW") {
    // Remove trailing empty cells
    $i = $ncol - 1;
    while( $i >= 0 ) {
      if( strlen($rows[$nrow][$i]) > 0 ) {
        break;
      }
      $i--;
    }
    array_splice($rows[$nrow], $i+1);
    $inrow=false;
  }

  if ($name ==  "TABLE:TABLE-CELL") {
    $incell=false;

    $rows[$nrow][$ncol] = "\"".preg_replace("/^(-{0,1}\d+),(\d+)$/", "$1.$2", $celldata)."\"";  // replace , by . for float values

    if ( $colrepeat > 1 ) {
      $rval=$rows[$nrow][$ncol];
      for ($i=1;$i<$colrepeat;$i++) {
        $ncol++;
        $rows[$nrow][$ncol]=$rval;
      }
    }
    //$ncol+=intval($attrs["TABLE:NUMBER-COLUMNS-REPEATED"]);

    $ncol++;
    $colrepeat=0;
  }


}

function characterData($parser, $data)
{
  global $rows,$nrow,$inrow,$incell,$ncol,$celldata;
  if ($inrow && $incell) {
    $celldata.=preg_replace(
      '/^\s*[\r\n]\s*$/ms', '', $data//str_replace(SEPCHAR,ALTSEPCHAR,$data)  // do not replace SEPCHAR by ALTSEPCHAR => unuseful because data are escaped by double quotes
    );
  }
  //  print $data. "- ";
}


function xmlcontent2csv($xmlcontent,&$fcsv) {
  global $rows;
  $xml_parser = xml_parser_create();
  // Utilisons la gestion de casse, de maniere a etre surs de trouver la balise dans $map_array
  xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
  xml_parser_set_option($xml_parser, XML_OPTION_SKIP_WHITE, 0);
  xml_set_element_handler($xml_parser, "startElement", "endElement");
  xml_set_character_data_handler($xml_parser, "characterData");



  if (!xml_parse($xml_parser,$xmlcontent )) {
    return (sprintf("error XML : %s line %d",
    xml_error_string(xml_get_error_code($xml_parser)),
    xml_get_current_line_number($xml_parser)));
  }

  xml_parser_free($xml_parser);
  foreach ($rows as $k=>$row) {
    $reverseIndex = count($row)-1;
    foreach (array_reverse($row) as $col=>$val) { // delete the last empty values
      if ($val == "\"\"") {
        unset($row[$reverseIndex]);
        $reverseIndex--;
      } else {
        break;
      }
    }
    if(!empty($row) && implode("",array_diff($row, array("\"\"")))!="") // do not add empty lines or lines with empty values
      $fcsv.= implode(SEPCHAR,$row)."\n";
  }
}

function ods2content($odsfile,&$content) {
  if (! file_exists($odsfile)) return "file $odsfile not found";
  $cibledir=uniqid("/var/tmp/ods");

  $cmd = sprintf("unzip -j %s content.xml -d %s >/dev/null", $odsfile, $cibledir );
  $process = new Process([$cmd]);
  $process->setTimeout(null); 
  $process->run();
    //system($cmd);
  //allow openning big files
  ini_set('memory_limit', '160M');
  $contentxml=$cibledir."/content.xml";
  if (file_exists($contentxml)) {
    $content=file_get_contents($contentxml);
    unlink($contentxml);
  }
  
  rmdir($cibledir);
}

function treatContent(&$xmlcontent, $leafName="Feuille1"){
  $doc = new DOMDocument('1.0', 'utf-8');
  $doc->loadXML($xmlcontent);
  $NodeList = $doc->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:table:1.0', 'table');
  $NodeListLength  = $NodeList->length;
  for ($idx = 0; $idx < $NodeListLength; $idx++) {
    if(property_exists($NodeList->item($idx), "has_attribute") && $NodeList->item($idx)->has_attribute){
      if($NodeList->item($idx)->getAttribute("table:name")!=$leafName) 
        $NodeList->item($idx)->parentNode->removeChild($NodeList->item($idx));
    }
  }
  $xmlcontent= $doc->saveXML();
  
}
/**
 * get tab Leafs Name from ods file
 * @param $strFileName file name
 * @return  array
 */
function getLeafs($strFileName){
  $content="";
  $tabLeafs = array();
  $err = ods2content($strFileName, $content);
  if ($err == "") {
    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($content);
    $imageList = $doc->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:table:1.0', 'table');
    $imageCnt  = $imageList->length;
    for ($idx = 0; $idx < $imageCnt; $idx++) {
      $tabLeafs[] = $imageList->item($idx)->getAttribute("table:name");
    }
  }
  return $tabLeafs;
}

function Doods2csv($strFileInput, $strFileOutput, $leafName){
  
  if ($strFileInput=="") {
    print "odsfile needed :usage  --odsfile=<ods file> [--csvfile=<csv file output>]\n";
    return;
  }
  
  $err = ods2content($strFileInput,$content);
  if ($err == "") {
    //Keep only one leaf 
    treatContent($content, $leafName);
    
    $err=xmlcontent2csv($content,$csv);
    if ($err=="") {
      if ($strFileOutput) {
        $n = file_put_contents($strFileOutput,utf8_decode($csv));
        if ($n > 0) 
          return true;
        else 
          $err = "fichier vide";
      } 
        else $err= "paramètre manquant";
    } 
    
  }
  if ($err != "") return $err;

}
/*
$odsfile = $_GET["odsfile"]; // file ods (input)
$csvfile = $_GET["csvfile"]; // file xml (output)
$leafName = $_GET["leafName"]; // file xml (output)

if ($odsfile=="") {
  print "odsfile needed :usage  --odsfile=<ods file> [--csvfile=<csv file output>]\n";
  return;
}

$err=ods2content($odsfile,$content);
if ($err == "") {
  //Keep only one leaf 
  treatContent($content, $leafName);
  
  $err=xmlcontent2csv($content,$csv);
  if ($err=="") {
    if ($csvfile) {
      $n=file_put_contents($csvfile,$csv);
      if ($n > 0) print sprintf(_("csv file <%s> wroted")."\n",$csvfile);
      else $err=sprintf(_("cannot write  %s"),$csvfile);
    } else print $csv;
  }
}
if ($err != "") print "ERROR:$err\n";
*/


?>