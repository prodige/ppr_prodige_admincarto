<?php

require_once __DIR__.'/ClassTable.php';

use Symfony\Component\Process\Process;

/**
 * class ODS
 * allow to manage ODS file
 * encoding: UTF-8 
 */
class ODS extends Table {
  
  protected $content;
  
  protected
    $inrow,
    $incell,
    $nrow,
    $ncol,
    $rows,
    $colrepeat,
    $celldata,
    $celltype;
  
  public function __construct($strFileName)
  {
    parent::__construct($strFileName);
    
    $this->inrow = false;
    $this->incell = false;
    $this->nrow = 0;
    $this->ncol = 0;
    $this->rows = array();
    $this->colrepeat = 0;
    $this->celldata = "";
    $this->celltype = "";
  }
  
  /**
   * parse xml content
   */
  protected function parse()
  {
    $xml_parser = xml_parser_create();
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
    xml_parser_set_option($xml_parser, XML_OPTION_SKIP_WHITE, 0);
    xml_set_element_handler($xml_parser, array(&$this, "startElement"), array(&$this, "endElement"));
    xml_set_character_data_handler($xml_parser, array(&$this, "characterData"));
  
    if (!xml_parse($xml_parser,$this->content)) {
      $this->error("Erreur XML", xml_error_string(xml_get_error_code($xml_parser))." ligne ".xml_get_current_line_number($xml_parser));
      return false;
    }
  
    xml_parser_free($xml_parser);
    
    return true;
  }
  
  public function startElement($parser, $name, $attrs) {
    if ($name ==  "TABLE:TABLE-ROW") {
      $this->inrow=true;
      if (isset($this->rows[$this->nrow])) {
        // fill empty cells
        $idx=0;
        foreach ($this->rows[$this->nrow] as $k=>$v) {
          if (! isset($this->rows[$this->nrow][$idx])) $this->rows[$this->nrow][$idx]=array('value' => '', 'type' => '');
          $idx++;
        }
        ksort($this->rows[$this->nrow],SORT_NUMERIC);
      }
      $this->nrow++;
      $this->ncol=0;
      $this->rows[$this->nrow] = array();
    }
  
    if ($name ==  "TABLE:TABLE-CELL") {
      $this->incell=true;
      $this->celldata = "";
      if ($attrs["TABLE:NUMBER-COLUMNS-REPEATED"]) {
        $this->colrepeat=intval($attrs["TABLE:NUMBER-COLUMNS-REPEATED"]);
      }
      if ($attrs["OFFICE:VALUE-TYPE"]) {
        $this->celltype = $attrs["OFFICE:VALUE-TYPE"];
      }
    }
  }
  
  public function endElement($parser, $name) {
    if ($name ==  "TABLE:TABLE-ROW") {
      // Remove trailing empty cells
      $i = $this->ncol - 1;
      while( $i >= 0 ) {
        if( strlen($this->rows[$this->nrow][$i]['value']) > 0 ) {
          break;
        }
        $i--;
      }
      array_splice($this->rows[$this->nrow], $i+1);
      $this->inrow=false;
    }
  
    if ($name ==  "TABLE:TABLE-CELL") {
      $this->incell=false;
  
      $this->rows[$this->nrow][$this->ncol]['value'] = $this->celldata;
      $this->rows[$this->nrow][$this->ncol]['type'] = $this->celltype;
  
      if ( $this->colrepeat > 1 ) {
        $rval=$this->rows[$this->nrow][$this->ncol]['value'];
        $rtype=$this->rows[$this->nrow][$this->ncol]['type'];
        for ($i=1;$i<$this->colrepeat;$i++) {
          $this->ncol++;
          $this->rows[$this->nrow][$this->ncol]['value']=$rval;
          $this->rows[$this->nrow][$this->ncol]['type']=$rtype;
        }
      }
      
      $this->ncol++;
      $this->colrepeat=0;
    }
  }
  
  public function characterData($parser, $data)
  {
    if ($this->inrow && $this->incell) {
      $this->celldata.= preg_replace('/^\s*[\r\n]\s*$/ms', '', $data);
    }
  }
  
  /**
   * import data into database
   * delete table if already exists
   * database is supposed to be encoding in ISO-8859-1
   * @return true if data imported ok, false otherwise
   */
  public function doImport($strLeafName, $host, $port, $dbname, $user, $password, $strTableName)
  {
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 300);
    
    $dbconn = @pg_connect("host=".$host." port=".$port." dbname=".$dbname." user=".$user." password=".$password);
    
    if ( !$dbconn ) {
      $this->error("Connexion échouée", "La connexion à la base de données PostgreSQL a échoué");
      return false;
    }
    
    if ( $this->getTabField($strLeafName) === false ) return false;
    
    if ( !$this->deleteTable($dbconn, $strTableName) ) return false;
    
    if ( !$this->createTable($strLeafName, $dbconn, $strTableName) ) return false;
    
    if ( !$this->treatContent($strLeafName) ) return false;
    
    if ( !$this->insertData($dbconn, $strTableName) ) return false;
    
    pg_query($dbconn,"commit");
    
    pg_close($dbconn);
    
    return true;
  }
  
  /**
   * update data into database
   * new fields must be added before calling this function
   * @return true if data updated ok, false otherwise
   */
  public function doUpdate($strLeafName, $host, $port, $dbname, $user, $password, $strTableName, $strFieldPk="", $tabTypeFields=array())
  {
    if ( !$strFieldPk ) {
      $this->error("Clé primaire non fournie", "Le champ utilisé comme clé primaire pour la mise à jour des données n'a pas été fourni");
      return false;
    }
    
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 300);
    
    $dbconn = @pg_connect("host=".$host." port=".$port." dbname=".$dbname." user=".$user." password=".$password);
    
    if ( !$dbconn ) {
      $this->error("Connexion échouée", "La connexion à la base de données PostgreSQL a échoué");
      return false;
    }
    
    if ( $this->getTabField($strLeafName) === false ) return false;
    
    if ( !$this->treatContent($strLeafName) ) return false;
    
    if ( !$this->updateData($dbconn, $strTableName, $strFieldPk, $tabTypeFields) ) return false;
    
    pg_query($dbconn,"commit");
    
    pg_close($dbconn);
    
    return true;
  }
  
  /**
   * insert data into database
   * @return boolean true if data inserted ok, false otherwise
   */
  protected function insertData($dbconn, $strTableName, $tabTypeFields=array())
  {
    $strSql = "";
    $strFields ="";
    foreach($this->tabField as $key => $tabInfo){
      $strFields.="\"".strtolower($tabInfo["name"])."\",";
    }
    $strFields= substr($strFields, 0, -1);
    
    for ( $iNumRow=2; $iNumRow<=count($this->rows); $iNumRow++ ) {
      if ( empty($this->rows[$iNumRow]) ) continue;
      $strSql.= "INSERT INTO ".$strTableName."(".$strFields.") VALUES (";
      for ( $iNumColumn=0; $iNumColumn<count($this->rows[1]); $iNumColumn++ ) {
        if ( array_key_exists($this->tabField[$iNumColumn]['name'], $tabTypeFields) && preg_match("/^.*string|char|text.*$/i", $tabTypeFields[$this->tabField[$iNumColumn]['name']]) == 0 || $this->tabField[$iNumColumn]['type'] == "float" ) {
          $strSql.= str_replace(",", ".", $this->convertValue(pg_escape_string($this->rows[$iNumRow][$iNumColumn]['value'])));
        } else {
          $strSql.= "'".$this->convertValue(pg_escape_string($this->rows[$iNumRow][$iNumColumn]['value']))."'";
        }
        $strSql.= ",";
      }
      $strSql = mb_substr($strSql, 0, -1);
      $strSql.= ");";
    }
    
    $strSql = utf8_decode($strSql);
    
    if ( !pg_query($dbconn,$strSql) ) {
      $this->error("Import des données échoué", "L'importation des données en base a échoué");
      $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
      return false;
    }
    
    return true;
  }
  
  /**
   * update data into database
   * @return boolean true if data updated ok, false otherwise
   */
  protected function updateData($dbconn, $strTableName, $strFieldPk, $tabTypeFields=array())
  {
    // delete all data in database which exist in the file
    $strSql = "";
    $strValuesUpdate = "";
    $quote = "'";
    $iColumnFieldPk = -1;
    foreach ( $this->tabField as $iNumColumn => $tabNameType ) {
      if ( strcasecmp($strFieldPk, $tabNameType['name']) == 0 ) {
        $iColumnFieldPk = $iNumColumn;
        if ( array_key_exists($tabNameType['name'], $tabTypeFields) && preg_match("/^.*string|char|text.*$/i", $tabTypeFields[$tabNameType['name']]) == 0 || $tabNameType['type'] == "float" ) {
          $quote = "";
        }
      }
    }
    for ( $iNumRow=2; $iNumRow<=count($this->rows); $iNumRow++ ) {
      $strValuesUpdate.= $quote.pg_escape_string($this->rows[$iNumRow][$iColumnFieldPk]['value']).$quote.",";
    }
    if ( $strValuesUpdate ) {
      $strValuesUpdate = mb_substr($strValuesUpdate, 0, -1);
      $strSql.= "DELETE FROM ".$strTableName." WHERE ".$strFieldPk." IN (".$strValuesUpdate.")";
      
      $strSql = utf8_decode($strSql);
    
      if ( !pg_query($dbconn,$strSql) ) {
        $this->error("Mise à jour des données échouée", "Impossible de modifier les données existantes");
        $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
        return false;
      }
    }
    
    // insert all data from the file to database
    if ( !$this->insertData($dbconn, $strTableName, $tabTypeFields) ) return false;
    
    return true;
  }
  
  /**
   * @return array tabField
   */
  public function getTabField($strLeafName)
  {
    
    if ( !$this->ods2content() ) return false;
    
    if ( !$this->treatContent($strLeafName) ) return false;
    
    if ( !$this->parse() ) return false;
    
    /* get fields name */
    for ( $iNumColumn=0; $iNumColumn<count($this->rows[1]); $iNumColumn++ ) {
      $this->tabField[$iNumColumn]['name'] = $this->convertValue($this->rows[1][$iNumColumn]['value']);
    }
    
    /* get fields type */
    for ( $iNumColumn=0; $iNumColumn<count($this->rows[2]); $iNumColumn++ ) {
      $this->tabField[$iNumColumn]['type'] = $this->convertValue($this->rows[2][$iNumColumn]['type']);
    }
    
    //print_r($this->tabField);
    
    return $this->tabField;
  }
  
  /**
   * get tab Leafs Name from ODS file encoding in ISO-8859-1
   * @return array
   */
  public function getLeafs()
  {
    $tabLeafs = array();
    
    if ( !$this->ods2content() ) return false;
     
    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($this->content);
    $imageList = $doc->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:table:1.0', 'table');
    $imageCnt  = $imageList->length;
    for ($idx = 0; $idx < $imageCnt; $idx++) {
      $tabLeafs[] = utf8_decode($imageList->item($idx)->getAttribute("table:name"));
    }
    
    return $tabLeafs;
  }
  
  protected function ods2content() {
    if (! file_exists($this->strFileName)) {
      $this->error("Fichier inexistant", "Le fichier \"".$this->strFileName."\" n'existe pas");
      return false;
    }
    $cibledir=uniqid("/var/tmp/ods");
  
    $cmd = sprintf("unzip -j %s content.xml -d %s >/dev/null", $this->strFileName, $cibledir);
    $process = new Process([$cmd]); 
    $process->setTimeout(null);
    $process->run(); 
        system($cmd);
    //allow openning big files
    ini_set('memory_limit', '160M');
    $contentxml=$cibledir."/content.xml";
    if (file_exists($contentxml)) {
      $this->content=file_get_contents($contentxml);
      unlink($contentxml);
    }
    
    rmdir($cibledir);
    
    return true;
  }
  
  /**
   * keep only the leaf
   * @return boolean
   */
  protected function treatContent($leafName="Feuille1"){
    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($this->content);
    
    $NodeList = $doc->getElementsByTagNameNS('urn:oasis:names:tc:opendocument:xmlns:table:1.0', 'table');
    $NodeListLength = $NodeList->length;
    $nodesToRemove = Array();
    //scanning table nodes
    for ($idx = 0; $idx < $NodeListLength; $idx++) {
      if ($NodeList->item($idx)->hasAttribute("table:name")) {
        if ($NodeList->item($idx)->getAttribute("table:name")!=$leafName) 
          array_push($nodesToRemove, $NodeList->item($idx));
      }
    }
    // removing the nodes that are not selected
    for ($i=0;$i<count($nodesToRemove);$i++) {
		$NodeList->item(0)->parentNode->removeChild($nodesToRemove[$i]);
	}
        
    $this->content= $doc->saveXML();
    
    return true;
  }
  
  /**
   * convert value to system encoding (supposed to be UTF-8)
   */
  protected function convertValue($value)
  {
    return mb_convert_encoding($value, "UTF-8", mb_detect_encoding($value, "UTF-8, ISO-8859-15, ISO-8859-1"));
  }
}
?>
