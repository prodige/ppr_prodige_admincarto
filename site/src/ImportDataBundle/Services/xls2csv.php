<?php
// Test CVS

/**
 * @deprecated v3.3 - 23 juil. 13
 * use XLS class instead
 */

require_once __DIR__.'/ClassSpreadsheet_Excel_Reader.php';
//allow openning big files
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 300);
define ("SEPCHAR", ',');

/**
 * get tab Leafs Name from ods file
 * @param $strFileName file name
 * @return  array
 */
function getLeafs($strFileName){
  $tabLeafs = array();
  $data = new Spreadsheet_Excel_Reader();
  // Set output Encoding.
  $data->setOutputEncoding('CP1251');
  $data->read($strFileName);
  foreach($data->boundsheets as $key => $value){
    $tabLeafs[$key] = $value["name"];
  }
  return $tabLeafs;
}

/**
 * transform xls file to csv file
 * @param $strFileInput
 * @param $strFileOutput
 * @param $leafName leaf name
 * @return unknown_type
 */
function Doxls2csv($strFileInput, $strFileOutput, $leafName){
  
  $data = new Spreadsheet_Excel_Reader();
  // Set output Encoding.
  $data->setOutputEncoding('CP1251');
  $data->read($strFileInput);
  $idSheet = getSheetNumber($data, $leafName);
  if(!is_numeric($idSheet))
    return $idSheet;
  $strCsv = "";
  for ($i = 1; $i <= $data->sheets[$idSheet]['numRows']; $i++) {
    for ($j = 1; $j <= $data->sheets[$idSheet]['numCols']; $j++) {
      $strCsv .= "\"".$data->sheets[$idSheet]['cells'][$i][$j]."\"".SEPCHAR;
    }
    $strCsv= substr($strCsv, 0, -1)."\n";
  }
  if ($strCsv!="") {
    if ($strFileOutput) {
      $n = file_put_contents($strFileOutput,$strCsv);
      if ($n > 0) 
        return true;
      else 
        $err = "fichier vide";
    } 
      else $err= "paramètre manquant";
  } 
  return $err;
  
}

/**
 * 
 * @param $data
 * @param $leafName
 * @return unknown_type
 */
function getSheetNumber($data, $leafName){
  foreach($data->boundsheets as $key => $value){
      if($value["name"] ==$leafName)
        return $key;
  }
  return "[erreur:feuille non reconnue]";
}
 /*
print_r($data);
error_reporting(E_ALL ^ E_NOTICE);

for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
	for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
		echo "\"".$data->sheets[0]['cells'][$i][$j]."\",";
	}
	echo "\n";

}
*/

//print_r($data);
//print_r($data->formatRecords);
?>
