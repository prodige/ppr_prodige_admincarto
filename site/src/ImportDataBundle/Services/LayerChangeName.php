<?php
namespace Carmen\ImportDataBundle\Services;
use Symfony\Component\Process\Process;

class LayerChangeName {
    protected $db_connection;

    /**
     * Constructeur
     * @param \Doctrine\DBAL\Connection $db_connection
     */
    public function __construct(\Doctrine\DBAL\Connection $db_connection)
    {
        $this->db_connection = $db_connection;
    }
    /**
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array    $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeQuery($query, $params = array())
    {
        $stmt = null;
        $queries = (is_array($query) ? $query : explode(";", $query));
        foreach($queries as $query){
            $query = trim($query);
            if(empty($query))continue;
            $query = preg_replace("!;$!", "", $query);
            $stmt= $this->db_connection->executeQuery($query, $params);
        }
        return $stmt;
    }
    
    /**
     * Rename a layer inserted as table In Postgis 
     * @param unknown $tbl_carto
     * @param unknown $new_tbl_carto
     * @throws Exception
     */
    function changeLayerNameInPostgis($tbl_carto, $new_tbl_carto){
        global $dao;
        
        $query = array();
        $query[] = "ALTER TABLE \"".$tbl_carto."\" RENAME TO \"".$new_tbl_carto."\";";
        $query[] = "update geometry_columns set f_table_name = :new_tbl_carto where f_table_name = :tbl_carto;";
        //modification dans les définitions de vues
        $query[] = "update parametrage.prodige_view_info set layer_name = :new_tbl_carto where layer_name = :tbl_carto;";
        $query[] = "update parametrage.prodige_join_info set table_name = :new_tbl_carto where table_name = :tbl_carto;";
        $query[] = "update parametrage.prodige_computed_field_info set layers = replace(layers, :tbl_carto, :new_tbl_carto) where layers like '%'||:tbl_carto||'%';";
        $query[] = "update parametrage.prodige_computed_field_info set tables = replace(tables, :tbl_carto, :new_tbl_carto) where tables like '%'||:tbl_carto||'%';";
        
        try {
            $this->db_connection->beginTransaction();
            $this->executeQuery($query, compact("tbl_carto","new_tbl_carto"));
            $this->db_connection->isTransactionActive() && $this->db_connection->commit();
        } catch (\Exception $exception){
            $this->db_connection->isTransactionActive() && $this->db_connection->rollBack();
            throw $exception;
        }
    }
    
    /**
     * Rename a layer referenced as a file path in maps
     * @param unknown $tbl_carto
     * @param unknown $new_tbl_carto
     * @throws Exception
     */
    function ScanDirectory($Directory, $tbl_carto, $new_tbl_carto, $isRasterUpdate=0)
    {
        $patterns = "!(\b)" . preg_quote($tbl_carto, "!") . "(\b)!i";
        $replacements = "$1".$new_tbl_carto."$2";
        $mapfiles = array();
        
        $process = Process::fromShellCommandline('find "'.$Directory.'" -maxdepth 1 -iname "*.map"'); 
        $process->setTimeout(null);
        $process->run();
        $mapfiles = explode ("\n", $process->getOutput());
        
        //@exec('find "'.$Directory.'" -maxdepth 1 -iname "*.map"', $mapfiles);
        foreach($mapfiles as $mapfile){
            try {
                if ( !preg_match($patterns, file_get_contents($mapfile)) ) continue;
                ms_ResetErrorList();
                $oMap = @ms_newMapObj ($mapfile);
                $bUpdate = false;
                if ($oMap) {
                    for($i = 0; $i < $oMap->numlayers; $i ++) {
                        $oLayer = $oMap->getLayer($i);
                        if ( $oLayer->connectiontype == MS_POSTGIS || $isRasterUpdate == 1 ) {
                            if ( preg_match($patterns, $oLayer->data) ) {
                                $oLayer->set("data", preg_replace($patterns, $replacements, $oLayer->data));
                                if ( is_file($new_tbl_carto) ){
                                    $extensionNewTblCarto = strtolower(pathinfo($new_tbl_carto, PATHINFO_EXTENSION));
                                    if ( $extensionNewTblCarto=="shp" ) {
                                        $oLayer->set("tileindex", $new_tbl_carto);
                                    } else {
                                        $oLayer->set("tileindex", null);
                                    }
                                }
                                $bUpdate = true;
                            }
                        }
                    }
                    if ($bUpdate) $oMap->save($mapfile);
                }
            } catch(\Exception $e){
            		$message = "";
                $error = ms_GetErrorObj();
                while($error && $error->code != MS_NOERR)
                {
                  $message .= printf("%s: %s<br/>", $error->routine, $error->message);
                  $error = $error->next();
                }
                throw new \Exception($e->getMessage()."<br/>".$message);
            }
        }
    }
}
