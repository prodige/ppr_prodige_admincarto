<?php

require_once __DIR__.'/ClassTable.php';
require_once __DIR__.'/PHPExcel.php';

/**
 * class XLS
 * allow to manage XLS file
 * encoding: UTF-8
 * use PHPExcel library
 */
class XLS extends Table {
  
  protected $data;
  
  public function __construct($strFileName)
  {
    parent::__construct($strFileName);
  }
  
  /**
   * @deprecated v3.3 - 23 juil. 13
   * do import data into database
   * delete table if already exists
   * database is supposed to be encoding in ISO-8859-1
   * @return true if data imported ok, false otherwise
   */
  /*
  public function doImport($strLeafName, $host, $port, $dbname, $user, $password, $strTableName)
  {
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 300);
    
    $dbconn = @pg_connect("host=".$host." port=".$port." dbname=".$dbname." user=".$user." password=".$password);
    
    if ( !$dbconn ) {
      $this->error("Connexion échouée", "La connexion à la base de données PostgreSQL a échoué");
      return false;
    }
    
    if ( $this->getTabField($strLeafName) === false ) return false;
    
    if ( !$this->deleteTable($dbconn, $strTableName) ) return false;
    
    if ( !$this->createTable($strLeafName, $dbconn, $strTableName) ) return false;
    
    if ( !$this->insertData($strLeafName, $dbconn, $strTableName) ) return false;
    
    pg_query($dbconn,"commit");
    
    pg_close($dbconn);
    
    return true;
  }
  */
  
  /**
   * @deprecated v3.3 - 23 juil. 13
   * do update data into database
   * new fields must be added before calling this function
   * @return true if data updated ok, false otherwise
   */
  /*
  public function doUpdate($strLeafName, $host, $port, $dbname, $user, $password, $strTableName, $strFieldPk="", $tabTypeFields=array())
  {
    if ( !$strFieldPk ) {
      $this->error("Clé primaire non fournie", "Le champ utilisé comme clé primaire pour la mise à jour des données n'a pas été fourni");
      return false;
    }
    
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 300);
    
    $dbconn = @pg_connect("host=".$host." port=".$port." dbname=".$dbname." user=".$user." password=".$password);
    
    if ( !$dbconn ) {
      $this->error("Connexion échouée", "La connexion à la base de données PostgreSQL a échoué");
      return false;
    }
    
    if ( $this->getTabField($strLeafName) === false ) return false;
    
    if ( !$this->updateData($strLeafName, $dbconn, $strTableName, $strFieldPk, $tabTypeFields) ) return false;
    
    pg_query($dbconn,"commit");
    
    pg_close($dbconn);
    
    return true;
  }
  */
  
  /**
   * return an array of the name and type of the columns
   * type can be [number|string], date is not identifiable
   * @return array tabField
   */
  public function getTabField($strLeafName)
  {
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 300);
    
    $oReader = new PHPExcel_Reader_Excel5();
    $oReader->setLoadSheetsOnly($strLeafName);
    $oReader->setReadDataOnly(false);
    
    $oFilterFields = new ReaderFields();
    $oReader->setReadFilter($oFilterFields);
    
    $oPHPExcel = $oReader->load($this->strFileName);
    
    $worksheet = $oPHPExcel->getActiveSheet();
    
    /* return the usable type depending of PHPExcel type [number|string] */
    function getMappingType($type) {
      $mapping = array(
        'n' => 'number',
      );
      if ( array_key_exists($type, $mapping) ) {
        $type = $mapping[$type];
      } else {
        $type = 'string';
      }
      return $type;
    }
    $iNumColumn=0;
    while ( $fieldName = $this->convertValue($worksheet->getCellByColumnAndRow($iNumColumn, 1)->getValue()) ) {
      $this->tabField[$iNumColumn]['name'] = $fieldName;
      $this->tabField[$iNumColumn]['type'] = $this->convertValue(getMappingType($worksheet->getCellByColumnAndRow($iNumColumn, 2)->getDataType()));
      $iNumColumn++;
    }
    
    return $this->tabField;
  }
  
  /**
   * @deprecated v3.3 - 23 juil. 13
   * insert data into database
   * @return boolean true if data inserted ok, false otherwise
   */
  /*
  protected function insertData($strLeafName, $dbconn, $strTableName, $tabTypeFields=array())
  {
    $strSql = "";
    $strFields = "";
    foreach($this->tabField as $key => $tabInfo){
      $strFields.="\"".strtolower($tabInfo["name"])."\",";
    }
    $strFields= substr($strFields, 0, -1);
    $idSheet = $this->getSheetNumber($strLeafName);
    for ( $iNumRow=2; $iNumRow<=$this->data->sheets[$idSheet]['numRows']; $iNumRow++ ) {
    	if(isset($this->data->sheets[$idSheet]['cells'][$iNumRow])){
    		$strSql.= "INSERT INTO ".$strTableName."(";
	      $strSql.= $strFields.")";
	      $strSql.=" VALUES (";
	      for ( $iNumColumn=1; $iNumColumn<=$this->data->sheets[$idSheet]['numCols']; $iNumColumn++ ) {
	      	if ( array_key_exists($this->tabField[$iNumColumn]['name'], $tabTypeFields) && preg_match("/^.*string|char|text.*$/i", $tabTypeFields[$this->tabField[$iNumColumn]['name']]) == 0 || $this->tabField[$iNumColumn]['type'] == "float" ) {
	      	  $strSql.= $this->convertValue(pg_escape_string(str_replace(",", ".", $this->data->sheets[$idSheet]['cells'][$iNumRow][$iNumColumn])));
	      	} else {
	      	  $strSql.= "'".$this->convertValue(pg_escape_string($this->data->sheets[$idSheet]['cells'][$iNumRow][$iNumColumn]))."'";
	      	}
	        $strSql.= ",";
	      }
	      $strSql = mb_substr($strSql, 0, -1);
	      $strSql.= ");";
    	}
    }
    
    $strSql = utf8_decode($strSql);
    if ( !pg_query($dbconn,$strSql) ) {
      $this->error("Import des données échoué", "L'importation des données en base a échoué");
      $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
      return false;
    }
    
    return true;
  }
  */
  
  /**
   * @deprecated v3.3 - 23 juil. 13
   * update data into database
   * @return boolean true if data updated ok, false otherwise
   */
  /*
  protected function updateData($strLeafName, $dbconn, $strTableName, $strFieldPk, $tabTypeFields=array())
  {
    // delete all data in database which exist in the file
    $strSql = "";
    $strValuesUpdate = "";
    $quote = "'";
    $iColumnFieldPk = -1;
    foreach ( $this->tabField as $iNumColumn => $tabNameType ) {
      if ( strcasecmp($strFieldPk, $tabNameType['name']) == 0 ) {
        $iColumnFieldPk = $iNumColumn;
        if ( array_key_exists($tabNameType['name'], $tabTypeFields) && preg_match("/^.*string|char|text.*$/i", $tabTypeFields[$tabNameType['name']]) == 0 || $tabNameType['type'] == "number" ) {
          $quote = "";
        }
      }
    }
    $idSheet = $this->getSheetNumber($strLeafName);
    for ( $iNumRow=2; $iNumRow<=$this->data->sheets[$idSheet]['numRows']; $iNumRow++ ) {
      $strValuesUpdate.= $quote.pg_escape_string($this->data->value($iNumRow, $iColumnFieldPk, $idSheet)).$quote.",";
    }
    if ( $strValuesUpdate ) {
      $strValuesUpdate = mb_substr($strValuesUpdate, 0, -1);
      $strSql.= "DELETE FROM ".$strTableName." WHERE ".$strFieldPk." IN (".$strValuesUpdate.")";
      
      $strSql = utf8_decode($strSql);
    
      if ( !pg_query($dbconn,$strSql) ) {
        $this->error("Mise à jour des données échouée", "Impossible de modifier les données existantes");
        $this->error("Erreur PostgreSQL", $this->convertValue(str_replace(" ", "&nbsp;", pg_last_error($dbconn))));
        return false;
      }
    }
    
    // insert all data from the file to database
    if ( !$this->insertData($strLeafName, $dbconn, $strTableName, $tabTypeFields) ) return false;
    
    return true;
  }
  */
  
  /**
   * get tab Leafs Name from XLS file in file encoding (supposed to be in ISO-8859-1)
   * @return  array
   */
  public function getLeafs()
  {
    $oReader = new PHPExcel_Reader_Excel5();
    try {
      return $oReader->listWorksheetNames($this->strFileName);
    } catch (Exception $e) {
    	return $e->getMessage();
    }
  }
  
  /**
   * generate CSV file from XLS file
   * @param $strFileOutput  CSV filepath
   * @param $strLeafName    name of XLS worksheet
   * @param $sep            CSV fields separator
   * @return 1 : ok
   *         0 : CSV filepath or XLS worksheet not defined
   *        -1 : worksheet does not exist
   *        -2 : nothing to write into CSV file
   *        -3 : CSV empty generated file
   */
  public function xls2csv($strFileOutput, $strLeafName, $delimiter=",")
  {
    if ( !$strFileOutput || !$strLeafName ) {
      return 0;
    }
    
    $oReader = new PHPExcel_Reader_Excel5();
    $tabWorksheetInfo = $oReader->listWorksheetInfo($this->strFileName);
    
    $bWorksheetExists = false;
    $_worksheetInfo = null;
    foreach ( $tabWorksheetInfo as $worksheetInfo ) {
      if ( $worksheetInfo['worksheetName'] == $strLeafName ) {
        $_worksheetInfo = $worksheetInfo;
        $bWorksheetExists = true;
        break;
      }
    }
    if ( !$bWorksheetExists ) {
      return -1;
    }
    
    $bUseXls2Csv = false;
    if ( $_worksheetInfo['totalRows'] > 10000 ) { // only use xls2csv if number of rows > 10000 to avoid memory limit and time limit exceeded
      $bUseXls2Csv = true;
    }
    
    if ( $bUseXls2Csv ) {
      // try to generate with catdoc xls2csv
      $return_var = null;
      $cmd = "xls2csv -d 8859-1 -b \"\" -f %d/%m/%Y \"".$this->strFileName."\" > \"".$strFileOutput."\"";
      passthru($cmd, $return_var);
    }
    
    // convert with catdoc xls2csv ok
    if ( $bUseXls2Csv && $return_var == 0 ) {
      $lineBegin = 0;
      $lineEnd   = 0;
      // get lines of the worksheet in the CSV file
      foreach ( $tabWorksheetInfo as $worksheetInfo ) {
        if ( $worksheetInfo['worksheetName'] == $strLeafName ) {
          $lineBegin = ( $lineBegin == 0 ? 1 : $lineBegin );
          $lineEnd = $lineBegin + $worksheetInfo['totalRows']-1;
          break;
        } else {
          $lineBegin += $worksheetInfo['totalRows']+1;
        }
      }
      // remove the other worksheets lines in the CSV file
      $cmd = "sed '".$lineBegin.",".$lineEnd."!d' \"".$strFileOutput."\" > \"".$strFileOutput.".tmp\" | mv \"".$strFileOutput.".tmp\" \"".$strFileOutput."\"";
      passthru($cmd);
    }
    // convert with PHPExcel
    else {
      ini_set('memory_limit', '512M');
      ini_set('max_execution_time', 300);
      
      require_once 'PHPExcel/Shared/String.php';
      setLocale(LC_TIME, 'fr_FR');
      PHPExcel_Shared_String::setDecimalSeparator('.');
      PHPExcel_Shared_String::setThousandsSeparator('');
      
      $oReader->setLoadSheetsOnly($strLeafName);
      $oReader->setReadDataOnly(false);
      
      $oPHPExcel = $oReader->load($this->strFileName);
      
      $csv = new PHPExcel_Writer_CSV($oPHPExcel);
      $csv->setDelimiter($delimiter);
      $csv->setLatin1(true);
      $csv->save($strFileOutput);
    }
    
    if ( !file_exists($strFileOutput) ) {
      return -2;
    }
    
    if ( filesize($strFileOutput) == 0 ) {
      return -3;
    }
    
    return 1;
  }
  
  /**
   * @deprecated v3.3 - 23 juil. 13
   * @param $leafName
   * @return unknown_type
   */
  /*
  protected function getSheetNumber($leafName)
  {
    foreach($this->data->boundsheets as $key => $value){
        if($value["name"] ==$leafName)
          return $key;
    }
    return -1;
  }
  */
  
  /**
   * convert value to system encoding (supposed to be UTF-8)
   */
  protected function convertValue($value)
  {
    return mb_convert_encoding($value, "UTF-8", mb_detect_encoding($value, "UTF-8, ISO-8859-15, ISO-8859-1"));
  }
}

/**
 * PHPExcel reader filter class used to get column names and types
 */
class ReaderFields implements PHPExcel_Reader_IReadFilter {
  public function readCell($column, $row, $worksheetName='') {
    if ($row >= 1 && $row <= 2) {
      $tabCell = array();
      $letter = 'A';
      while($letter !== 'AAA'){
      	$tabCell[] = $letter++;
      }
      if(in_array($column, $tabCell))
      	return true;
    } 
    return false; 
  }
}

/**  Define a Read Filter class implementing PHPExcel_Reader_IReadFilter  */ 
class MyReadFilter implements PHPExcel_Reader_IReadFilter {
  private $_startRow = 0;
  private $_endRow   = 0;
  private $_columns  = array();

  /**  Get the list of rows and columns to read  */
  public function __construct($startRow, $endRow, $columns) {
    $this->_startRow  = $startRow;
    $this->_endRow    = $endRow;
    $this->_columns   = $columns;
  } 

  public function readCell($column, $row, $worksheetName = '') {
    //  Only read the rows and columns that were configured
    if ($row >= $this->_startRow && $row <= $this->_endRow) {
      if (in_array($column,$this->_columns)) {
        return true;
      }
    }
    return false;
  }
}
?>
