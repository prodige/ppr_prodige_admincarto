<?php

namespace Carmen\ImportDataBundle\Controller;

use Doctrine\DBAL\Types\Type;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Prodige\ProdigeBundle\Common\Modules\Standard\StandardCtrlQualite;
use Prodige\ProdigeBundle\Common\PostgresqlUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/importdata/table_structure")
 */
class TableStructureController extends AbstractImportDataController {
    /**
     * @isGranted("ROLE_USER")
     * @Route("/index/{metadata_id}/{table_name}", name="carmen_tablestructure_index", options={"expose"=true}, defaults={"table_name"=null}, requirements={"metadata_id"="[-_%.a-zA-Z0-9]+"})
     */
    public function indexAction(Request $request, $metadata_id, $table_name=null) {
        $configReader = $this->get('prodige.configreader');
        global $PRO_IMPORT_EPSG;
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $table_name = "";
        $typeStockage = "";

        if ( !$table_name ) {
          //  $table_name
            $result = $CATALOGUE->fetchAll("select couchd_emplacement_stockage,couchd_type_stockage from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) where fmeta_id=:fmeta_id", array("fmeta_id"=>$metadata_id));
            $result = !empty($result) ? $result[0] : array('couchd_emplacement_stockage' => "",  'couchd_type_stockage' => "");
            $table_name = $result['couchd_emplacement_stockage'];
            $typeStockage =  $result['couchd_type_stockage'];
        }

        $url_back = $request->get("url_back", null);
        $ar_parameters = array("url_back"=>$url_back,"typeStockage" => $typeStockage, "table_name"=>$table_name, "metadata_id"=>$metadata_id, "PRODIGE_DEFAULT_EPSG" =>$PRO_IMPORT_EPSG );

        return $this->render('ImportDataBundle/Default/tableStructureManager.html.twig', $ar_parameters);

    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/params/{metadata_id}", name="carmen_tablestructure_params", options={"expose"=true}, requirements={"metadata_id"="[-_%.a-zA-Z0-9]+"})
     */
    public function getParamsAction ($metadata_id) {


        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $PDO = $CATALOGUE->getWrappedConnection();
        $res = $CATALOGUE->fetchAssoc("select * from public.metadata where id=:fmeta_id", array("fmeta_id"=>$metadata_id));


        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]";
        $sql_metadata_title = " array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_title";
        $sql_metadata_abstract = " array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_abstract";
        $stmt = $PDO->prepare("select xml_is_well_formed(data::text) from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata where id=:metadata_id");
        $stmt->execute(array("metadata_id"=>$metadata_id));
        if( !$stmt->fetchColumn() ) {
            $sql_metadata_title = "null as metadata_title";
            $sql_metadata_abstract = "null as metadata_abstract";
        }

        $stmt = $PDO->prepare("select ".$sql_metadata_title.", ".$sql_metadata_abstract."  from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata where id=:metadata_id");
        $stmt->execute(array("metadata_id"=>$metadata_id));
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        $result['name'] = $row["metadata_title"];
        $result['layer_desc'] = $row["metadata_abstract"];
        $status = true;

        return new JsonResponse(array(
            "success" => $status ?: false,
            "layer_name" => $result['name']  ?: '',
            "layer_desc" => $result['layer_desc'] ?: ''
        ));

    }
    /**
     * @isGranted("ROLE_USER")
     * @Route("/services/{action}", name="carmen_tablestructure_services", options={"expose"=true}, requirements={"action"="[-_%.a-zA-Z0-9]+"})
     */
    public function servicesAction(Request $request, $action) {
        if(isset($action)) {
            $action .= 'Action';
            if ( method_exists($this, $action) ) return $this->$action($request);
        }
        return new JsonResponse(array('error'=>"Fonction [{$action}] non définie"));
    }


    protected function transactionalQueries(\Doctrine\DBAL\Connection $connection, array $queries, array $params=array(), array $types=array(), $withException = true)
    {
        try {
            $connection->beginTransaction();
            foreach($queries as $query){
                $connection->executeQuery($query, $params, $types);
            }
            $connection->commit();
            return true;
        }catch(\Exception $exception){
            $connection->rollBack();

            if($withException){
                throw $exception;
            }

        }
        return false;
    }


    protected function createTableAction(Request $request)
    {
        $PRODIGE   = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $metadata_id      = $fmeta_id = $request->get("metadata_id", null);
        $tableName        = strtolower($request->get("tableName", null));
        $geometryType     = $request->get("geometryType", null);
        $projectionsValue = $request->get("projectionsValue", null);
        $dataType         = $request->get("dataType", "vectoriel");

        // @hrt : ajout description et array_valuesom de couche de données
        $coucheName = $request->get("couchehd_name", "Structure d'édition libre");
        $coucheDescription = $request->get("descriptiohd_name", "Structure d'édition libre");

        $createTable = false;

        if(isset($tableName) && isset($metadata_id)  && ($dataType == 'tabulaire' || ( $dataType == 'vectoriel' && isset($projectionsValue) && isset($geometryType) ))) {
            $schema = self::PRODIGE_SCHEMA_PUBLIC;

           try {
                $queries = ( $dataType == 'vectoriel' ) ? $this->getVectorielTabSql($tableName, $schema, $geometryType, $projectionsValue) : $this->getTabulaireTabSql($tableName, $schema);

                $params = array(
                    "f_table_catalog" => "",
                    "f_table_schema" => $schema,
                    "f_table_name" => $tableName,
                );

                if($dataType == 'vectoriel'){
                    $params["f_geometry_column"] = "the_geom";
                    $params["coord_dimension"] = 2;
                    $params["srid"] = $projectionsValue;
                    $params["type"] = $geometryType;
                }


                $pk_couche_donnees = $CATALOGUE->fetchColumn("select pk_couche_donnees from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) where fmeta_id=:fmeta_id and couchd_emplacement_stockage=:couchd_emplacement_stockage", array("fmeta_id"=>$metadata_id, "couchd_emplacement_stockage"=>$tableName));
                $addCoucheDonnees = false;
                if ( !$pk_couche_donnees ){
                    $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                    $couchd_emplacement_stockage = $tableName;
                    list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                    $i = 1;
                    while ( !$unique ){
                      $couchd_emplacement_stockage = $tableName."_".($i++);
                      list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                    }
                    $queries = str_replace($tableName, $couchd_emplacement_stockage, $queries);
                    $params = str_replace($tableName, $couchd_emplacement_stockage, $params);
                    $tableName = $couchd_emplacement_stockage;
                    $addCoucheDonnees = true;
                }
                $createTable = $this->transactionalQueries($PRODIGE, $queries, $params);

                if ( $addCoucheDonnees ){
                    $coucheFields = array(
                        "pk_couche_donnees" => $pk_couche_donnees,
                        "couchd_id" => (string)$pk_couche_donnees,
                        "couchd_nom" => $coucheName,
                        "couchd_description" => $coucheDescription,
                        "couchd_type_stockage" =>( ($dataType == 'vectoriel') ? self::DEFAULT_TYPE_STOCKAGE : self::TABULAIRE_TYPE_STOCKAGE ),
                        "couchd_emplacement_stockage" => $tableName,
                        "couchd_visualisable" => 1,
                        "couchd_wms" => 0,
                        "couchd_wfs" => 0,
                        "couchd_fk_acces_server" => 1,
                    );

                    $queries = array(
                        "insert into catalogue.couche_donnees (".implode(", ", array_keys($coucheFields)).") values (:".implode(", :",  array_keys($coucheFields)).")",
                        "insert into catalogue.fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)",
                    );

                    $params = array_merge($params,
                        $coucheFields,
                        compact("fmeta_id", "pk_couche_donnees")
                    );

                    $createTable = $createTable && $this->transactionalQueries($CATALOGUE, $queries, $params);

                    $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees);
                }
            }catch(\Exception $exception){throw $exception;
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(array( "createTable" => $createTable, "tableName"=>$tableName ));
    }

    protected function dropTableAction(Request $request)
    {
        $metadata_id       = $request->get("metadata_id", null);
        $tableName         = strtolower($request->get("tableName", null));
        $dropTable = false;

        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        if((isset($tableName)) && ($tableName != "")) {
            $schema = self::PRODIGE_SCHEMA_PUBLIC;

            try {
                list($pk_fiche_metadonnees, $pk_couche_donnees) = $CATALOGUE->fetchArray("select pk_fiche_metadonnees, pk_couche_donnees from catalogue.couche_donnees inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) where fmeta_id=:fmeta_id and couchd_emplacement_stockage=:couchd_emplacement_stockage", array("fmeta_id"=>$metadata_id, "couchd_emplacement_stockage"=>$tableName));
                if ( $pk_fiche_metadonnees && $pk_couche_donnees){
                    $queries = array(
                        "delete from catalogue.fiche_metadonnees where pk_fiche_metadonnees=:pk_fiche_metadonnees",
                        "delete from catalogue.couche_donnees where pk_couche_donnees=:pk_couche_donnees",
                    );
                    $params = compact("pk_fiche_metadonnees", "pk_couche_donnees");
                    $dropTable = $this->transactionalQueries($CATALOGUE, $queries, $params);
                }
                $queries = array(
                    "DROP TABLE IF EXISTS ".$tableName,
                    "DROP SEQUENCE IF EXISTS ".$tableName."_gid_seq",
                );
                $params = array();
                $dropTable = $this->transactionalQueries($this->getProdigeConnection("public"), $queries, $params);
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }

        return new JsonResponse(array( "dropTable" => $dropTable ));

    }

    protected function alterTableAction(Request $request)
    {
        $schema = self::PRODIGE_SCHEMA_PUBLIC;

        $PRODIGE = $this->getProdigeConnection($schema);
        $tableName        = strtolower($request->get("tableName", null));
        $fieldsToAdd      = $request->get("fieldsToAdd", null);
        $alterTable = false;

        if(isset($fieldsToAdd) && isset($tableName)) {
            $queries = array();

            $fieldsToAddJSON = json_decode($fieldsToAdd);

            $alters = array();

            foreach($fieldsToAddJSON as $cmpt=>$value) {

                $strSQL = "";
                if($cmpt < 2) {
                    continue;
                }
                else {
                    if($value->Type == 'enum') {
                        if($value->EnumName != "") {
                            $strSQL.= " ADD \"".$value->FieldName."\" \"".$value->EnumName."\"" ;
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue(trim($value->DefaultValue), Type::STRING)."'";
                            }
                        } else continue;
                    }
                    elseif($value->Type == 'array') {
                       if($value->EnumName != "") {
                          $strSQL.= " ADD \"".$value->FieldName."\" ".$value->EnumName."[] " ;
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '{".$PRODIGE->convertToDatabaseValue(implode(', ', $value->DefaultValue), Type::STRING)."}'";
                            }
                       } else continue;
                    }
                    elseif($value->Type == 'text' || $value->Type == 'image' || $value->Type == 'email' || $value->Type == 'phone_number') {
                        $strSQL.= " ADD \"".$value->FieldName."\" ".$value->Type."" ;
                        if($value->Mandatory) {
                            $strSQL.= " NOT NULL";
                        }
                        if($value->DefaultValue != "") {
                            $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue($value->DefaultValue, Type::STRING)."'";
                        }
                    }
                    else {
                        $strSQL.= " ADD \"".$value->FieldName."\" ".$value->Type."" ;
                        if($value->Type=='varchar') {
                            if($value->VarcharLength != "") {
                                $strSQL.= "(".$value->VarcharLength.")";
                            }
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue($value->DefaultValue, Type::STRING)."'";
                            }
                        }
                        elseif($value->Type=='date') {
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue != "") {
                                $strSQL.= " DEFAULT '".$PRODIGE->convertToDatabaseValue($value->DefaultValue, Type::STRING)."'";
                            }
                        }
                        elseif(($value->Type=='bigint') || ($value->Type=='real')) {
                            if($value->Mandatory) {
                                $strSQL.= " NOT NULL";
                            }
                            if($value->DefaultValue !== "") {
                                $strSQL.= " DEFAULT ".$PRODIGE->convertToDatabaseValue($value->DefaultValue, ($value->Type=='bigint' ? Type::BIGINT : Type::FLOAT));
                            }
                        }
                        else continue;
                    }

                    $alters[] = $strSQL;
                }
            }

            if ( !empty($alters) ){
                $queries[] = "ALTER TABLE ".$schema.".".$tableName." ".implode(", ", $alters);
            }
            foreach($fieldsToAddJSON as $i=>$value) {
                if($value->Comment != '') {
                    $queries[] = " COMMENT ON COLUMN ".$schema.".".$tableName.".\"".$value->FieldName."\" IS '".$PRODIGE->convertToDatabaseValue(trim($value->Comment), Type::STRING)."'";
                }
            }
            try {
                $alterTable = $this->transactionalQueries($this->getProdigeConnection("public"), $queries);
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }

        }
        return new JsonResponse(compact( "alterTable" ));
    }

    protected function getAllEnumAction(Request $request) {

        $enums = array();

        try{
            $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
            $strSQL = "select n.nspname as enum_schema, t.typname as enum_name, string_agg(e.enumlabel, ', ') as enum_value from pg_type t join pg_enum e on t.oid = e.enumtypid join pg_catalog.pg_namespace n ON n.oid = t.typnamespace group by enum_schema, enum_name";
            $enums = $PRODIGE->fetchAll($strSQL);
        }catch(\Exception $exception){
            return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
        }
        return new JsonResponse(array(
            "getAllEnum" => true,
            "allEnums" => $enums ?: array()
        ));
    }

    protected function createEnumAction(Request $request) {

        $schema = self::PRODIGE_SCHEMA_PUBLIC;
        $PRODIGE = $this->getProdigeConnection($schema);

        $alterEnum =  false;
        $createEnum = false;

        $enum = $request->get("enum", null);
        $enumValuesToAdd = json_decode($request->get("enumValuesToAdd", null));

        if(isset($enumValuesToAdd) && isset($enum) && !empty($enumValuesToAdd)) {
            try{
                $enumValuesToAdd = array_map(function($value) use($PRODIGE) {return "'".$PRODIGE->convertToDatabaseValue(trim($value), Type::STRING)."'";}, $enumValuesToAdd);
                $strSQL = "CREATE TYPE \"". $enum."\" AS ENUM (".implode(", ", $enumValuesToAdd).")";
                $createEnum = $this->transactionalQueries($this->getProdigeConnection("public"), array($strSQL));
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(compact( "createEnum", "alterEnum" ));
    }

    protected function alterEnumAction(Request $request) {
        $schema = self::PRODIGE_SCHEMA_PUBLIC;
        $PRODIGE = $this->getProdigeConnection($schema);

        $alterEnum =  false;
        $createEnum = false;

        $enum = $request->get("enum", null);
        $enumValuesToAdd = json_decode($request->get("enumValuesToAdd", null));

        if(isset($enumValuesToAdd) && isset($enum) && !empty($enumValuesToAdd)) {
            try{
                $enumValuesToAdd = array_map(function($value) use($PRODIGE) {return "'".$PRODIGE->convertToDatabaseValue(trim($value), Type::STRING)."'";}, $enumValuesToAdd);
                $queries = array();
                foreach($enumValuesToAdd as $value){
                    $queries[] = "ALTER TYPE \"". $enum."\" ADD VALUE IF NOT EXISTS ".$value;
                }
                foreach($queries as $query){
                   $PRODIGE->executeQuery($query);
                }
                $alterEnum = true;
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(compact( "createEnum", "alterEnum" ));
    }

    protected function deleteEnumAction(Request $request) {

        $enum = $request->get("enum", null);
        $deleteEnum = false;

        if(isset($enum)) {
            try{
                $deleteEnum = $this->transactionalQueries($this->getProdigeConnection("public"), array("DROP TYPE \"".$enum."\""), array());
            }catch(\Exception $exception){
                return new JsonResponse(array("error" => "Erreur : ".$exception->getMessage()));
            }
        }
        return new JsonResponse(compact( "deleteEnum" ));
    }

    /**
     * Liste des standards
     *
     * @isGranted("ROLE_USER")
     *
     * @Route("/getStandards", name="carmen_tablestructure_standard", options={"expose"=true} )
     *
     * @param Request $request  request object
     */
    function getStandard(Request $request){
        $conn = $this->getCatalogueConnection();

        $sql = "SELECT standard_id as id, standard_name as name from catalogue.standards_standard";

        $sth = $conn->prepare($sql);
        $sth->execute();

        $result = $sth->fetchAll();


        return new JsonResponse($result, Response::HTTP_OK);
    }

    /**
     * @isGranted("ROLE_USER")
     *
     * @Route("/createFromStandard", name="carmen_tablestructure_createFromStandard", options={"expose"=true} )
     *
     * @param Request $request  request object
     */
    function createFromStandard(Request $request){
        // connection aux bases de donnée
        $conn = $this->getCatalogueConnection();
        $connProdige = $this->getProdigeConnection();

        // pour l'import du standard
        $dbProdigeName = $this->container->getParameter('prodige_name');

        $result = array();

        // récupération des params
        /** Schéma, prefix suffix, standard id, projection, , metadata id */
        $standardId = $request->get("standardId", null);
        $metadataId = $request->get("metadataId", null);
        $schema = $request->get("schema", null);

        $prefix = $request->get("prefix", "");
        $suffix = $request->get("suffix", "");

        $projection = $request->get("projection", null);


        // on vérifie les paramètre
        if(!$standardId || !$schema || !$metadataId ){
            return new JsonResponse(array('mesg' => 'Paramètre manquant'), Response::HTTP_NOT_FOUND);
        }

        // on vérifie si le schema existe 
        if($this->chekcIfSchemaExist($connProdige, $schema)){
            return new JsonResponse(array('msg' => "Le schéma $schema existe déjà" ), Response::HTTP_NOT_FOUND);
        }

        // on récupère les données du standard
        $standard = StandardCtrlQualite::getStandardById($conn, $standardId);

        if(!$standard){
            return new JsonResponse(array('msg' => "Aucune donnée trouvée pour le standard (id:$standardId)"), Response::HTTP_NOT_FOUND);
        }

        // on exporte les structures des tables en fonction du schéma
        $output = $this->importStandardInProdige($standard['database'], $standard['schema'], $dbProdigeName, $schema);

        //on récupère le nom des tables 
        $tables = $this->getTablesFromDatabase($connProdige, $schema);

        // on modifie les noms des tables avec les sufix et postfix 
        $connProdige->beginTransaction();
        $conn->beginTransaction();

        $isOk = $this->addPrefixAndSuffix($connProdige, $tables, $schema, $prefix, $suffix);

        // on lie les couches données au metadata
        foreach($tables as $table){
            $isOk = $isOk && $this->addAndLinkCoucheDonnee($conn, $standard['database'] ,$prefix, $table['table_name'], $suffix, $metadataId,$table['table_schema'] );

        }

        // on modifie la projection des collone geometrie
        // TODO: vérfier les droit sur spatial_ref
        if($projection){
            $isOk = $isOk && $this->setSchemaProjection($connProdige, $schema, $projection);
        }
        if($isOk){
            $validResult = $this->validMetadataFromStandard($conn, $connProdige, $standard, $metadataId, $standardId, $prefix, $suffix);

            $isOk = $isOk && isset($validResult["url"]);
        }

        if($isOk){
            $result['url'] = $validResult["url"];
            $connProdige->commit();
            $conn->commit();
        }
        else{
            $connProdige->rollBack();
            $conn->rollBack();
        }

        $result['debug'] = array($output, $prefix, $suffix, array("addAndLinkCouche" => $isOk ));
        $result['success'] = $isOk;

        return new JsonResponse($result,$isOk ? Response::HTTP_OK : Response::HTTP_NOT_FOUND);
    }

    function validMetadataFromStandard($conn, $connProdige, $standard, $metadataId, $standardId, $prefix, $suffix){
        // On se connecte à la base tampon
        $tamponConn = $this->getDBTamponConnection($standard['database']);

        $ctrlQualite = new StandardCtrlQualite($conn, $connProdige, $tamponConn );

        // On récupère les nom des tables de la base tampons
        $tablesTampons = PostgresqlUtil::getTablesFromDatabase($tamponConn, $standard['schema'] );

        // On récupère les noms des tables liées au Metadata
        $metadataTables = $ctrlQualite->getMetadataTablesByUuid(null, $metadataId );

        // Comparaison des table metadata avec le standard
        $results = $ctrlQualite->compareMetadataToStandard($metadataTables, $tablesTampons, $prefix, $suffix);

        $data['result'] = $results;

        // Création du fichier html
        $html =  $this->render('@ProdigeProdige/Templates/standard/repport.html.twig', $results);

        $data["url"] = $ctrlQualite->updateOrAddStandardConformite($uuid, $standardId, $prefix, $suffix, $results['allOk'], $html, $metadataId);

        $urlCatalogue = $this->container->hasParameter('PRODIGE_URL_ADMINSITE') ? $this->container->getParameter('PRODIGE_URL_ADMINSITE') : "";

        $data['url'] = str_replace(PRO_PATH_WEB, "", $data['url'] );
        $urlCatalogue = str_replace("/app_dev.php", "", $urlCatalogue);

        $data['url'] = $urlCatalogue."/".$data['url'];

        return $data;
    }

    function getVectorielTabSql($tableName, $schema, $geometryType, $projectionsValue){
        return array(
            "DROP TABLE IF EXISTS ".$tableName,
            "DROP SEQUENCE IF EXISTS ".$tableName."_gid_seq",
            "DELETE FROM public.geometry_columns WHERE f_table_name = :f_table_name",
            "CREATE SEQUENCE ".$tableName."_gid_seq  START 1",
            "CREATE TABLE ".$schema.".".$tableName." (gid bigint DEFAULT nextval('".$tableName."_gid_seq'::regclass) NOT NULL PRIMARY KEY, the_geom geometry(".$geometryType.",".$projectionsValue.") NOT NULL)",
            "CREATE INDEX idx_gist_base_prodige_".$tableName."_geometry ON ".$tableName." USING gist (the_geom)",
            "INSERT INTO public.geometry_columns (f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, type) VALUES (:f_table_catalog, :f_table_schema, :f_table_name, :f_geometry_column, :coord_dimension, :srid, :type)",
        );
    }

    function getTabulaireTabSql($tableName, $schema){
        return array(
            "DROP TABLE IF EXISTS ".$tableName,
            "DROP SEQUENCE IF EXISTS ".$tableName."_gid_seq",
            "DELETE FROM public.geometry_columns WHERE f_table_name = :f_table_name",
            "CREATE SEQUENCE ".$tableName."_gid_seq  START 1",
            "CREATE TABLE ".$schema.".".$tableName." (gid bigint DEFAULT nextval('".$tableName."_gid_seq'::regclass) NOT NULL PRIMARY KEY)",
        );
    }
}
