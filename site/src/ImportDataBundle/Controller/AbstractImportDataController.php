<?php

namespace Carmen\ImportDataBundle\Controller;

use Carmen\ApiBundle\Controller\BaseController as CarmenBaseController;
use Carmen\ApiBundle\Exception\ApiException;
use Carmen\ImportDataBundle\Services\ImportInPostgis;
use Carmen\ImportDataBundle\Services\ImportRaster;
use Carmen\ImportDataBundle\Services\LayerChangeName;
use Doctrine\DBAL\Connection;
use Exception;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Controller\BaseController as ProdigeBaseController;
use Prodige\ProdigeBundle\Controller\FeatureCatalogueController;
use Prodige\ProdigeBundle\Controller\UpdateArboController;
use Prodige\ProdigeBundle\Controller\UpdateDomSdomController;
use Prodige\ProdigeBundle\Controller\UpdateMetadataController;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

/**
 * @author alkante
 */
abstract class AbstractImportDataController extends CarmenBaseController
{
    const DEFAULT_TYPE_STOCKAGE = "1"; //vector
    const TABULAIRE_TYPE_STOCKAGE = "-3"; //tabulaire
    const PRODIGE_SCHEMA_PUBLIC = "public";
    const PRODIGE_SCHEMA_LOCALDATA = "local_data";
    const CATALOGUE_SCHEMA_CATALOGUE = "catalogue";
    const CATALOGUE_SCHEMA_PUBLIC = "public";

    const TAMPON_PREFIX_DB_NAME = "tampon_";
    const TAMPON_TEMPLATE = "tampon_tpl";

    const ACTION_CREATE = 'action_create';
    const ACTION_ADD = 'action_add';
    const ACTION_REPLACE = 'action_replace';

    const ACTION_DELETE_DB = 'delete';

    protected static $DATATYPE_TO_STOCKAGE = array(
        "0" => "raster",
        "1" => "vector",
        "01" => "mnt",
        "-1" => "multi",
        "-2" => "majic",
        "-3" => "tabulaire",
        "-4" => "join",
    );
    protected static $ADDITIONAL_RASTER_EXT = array("tfw", "tab");
    protected static $DATATYPE_EXTENSION = array(
        "VECTOR" => array(
            'shp' => array('prj', 'dbf', 'shx', 'qpj'),
            'mif' => array('mid'),
            'tab' => array('id', 'map', 'dat', 'ind')
        ),
        "TABULAIRE" => array('csv', 'ods', 'xls', 'xlsx', 'json'),
        "MNT" => array('asc', 'xyz',),
        "SPREADSHEET" => array('ods', 'xls', 'xlsx',),
        "RASTER" => array(
            'RASTER' => array('gtiff', 'tiff', 'tif', 'ecw', 'jp2', 'jpg', 'jpeg'),
            'TILEINDEX' => array(
                'shp' => array('prj', 'dbf', 'shx', 'qpj'),
                'tab' => array('id', 'map', 'dat', 'ind'),
            )
        )
    );
    protected static $REQUIRED_EXTENSION = array(
        "VECTOR" => array(
            'shp' => array('prj', 'dbf', 'shx'),
            'mif' => array('mid'),
            'tab' => array('id', 'map', 'dat')
        ),
        "TABULAIRE" => array('csv', 'ods', 'xls', 'xlsx', 'json'),
        "MNT" => array('asc', 'xyz',),
        "SPREADSHEET" => array('ods', 'xls', 'xlsx',),
        "RASTER" => array(
            'RASTER' => array('gtiff', 'tiff', 'tif', 'ecw', 'jp2', 'jpg', 'jpeg'),
            'TILEINDEX' => array(
                'shp' => array('prj', 'dbf', 'shx'),
                'tab' => array('id', 'map', 'dat'),
            )
        )
    );

    protected static $TABLE_STOCKAGES = array("1" => "public", "-3" => "public", "-4" => "public");

    protected function formatJsonSuccessCallback($callback)
    {
        $args = array_slice(func_get_args(), 1);
        $response = call_user_func_array(array($this, "formatJsonSuccess"), $args);
        if ($response instanceof Response) {
            if ($callback) return new Response($callback . '(' . $response->getContent() . ')');
            return $response;
        }
        return $response;
    }

    protected function formatJsonErrorCallback($callback)
    {
        $args = array_slice(func_get_args(), 1);
        $response = call_user_func_array(array($this, "formatJsonError"), $args);
        if ($response instanceof Response) {
            if ($callback) return new Response($callback . '(' . $response->getContent() . ')');
            return $response;
        }
        return $response;
    }

    /**
     * replace passwords in a string
     * @param type $content
     * @return type
     */
    protected function formatStringError($content)
    {

        $PRODIGE = $this->getProdigeConnection(null);
        $CATALOGUE = $this->getCatalogueConnection(null);
        $content = str_replace($PRODIGE->getPassword(), "******", $content);
        $content = str_replace($CATALOGUE->getPassword(), "******", $content);
        $content = str_replace($this->container->getParameter("tampon_password"), "******", $content);

        $content = str_replace($PRODIGE->getUsername(), "******", $content);
        $content = str_replace($CATALOGUE->getUsername(), "******", $content);
        $content = str_replace($this->container->getParameter("tampon_user"), "******", $content);
        return $content;
    }

    protected function formatJsonError($status = 500, $error = "Internal Server Error", $description = "", array $extra = array())
    {
        $response = parent::formatJsonError($status, $error, $description, $extra);
        $content = $response->getContent();
        $PRODIGE = $this->getProdigeConnection(null);
        $CATALOGUE = $this->getCatalogueConnection(null);
        $content = str_replace($PRODIGE->getPassword(), "******", $content);
        $content = str_replace($CATALOGUE->getPassword(), "******", $content);
        $content = str_replace($this->container->getParameter("tampon_password"), "******", $content);

        $content = str_replace($PRODIGE->getUsername(), "******", $content);
        $content = str_replace($CATALOGUE->getUsername(), "******", $content);
        $content = str_replace($this->container->getParameter("tampon_user"), "******", $content);

        $response->setContent($content);
        return $response;
    }

    protected function formatJsonSuccess(array $extra = array(), $status = 200)
    {
        $response = parent::formatJsonSuccess($extra, $status);
        $content = $response->getContent();
        $PRODIGE = $this->getProdigeConnection(null);
        $CATALOGUE = $this->getCatalogueConnection(null);
        $content = str_replace($PRODIGE->getPassword(), "******", $content);
        $content = str_replace($CATALOGUE->getPassword(), "******", $content);
        $content = str_replace($this->container->getParameter("tampon_password"), "******", $content);

        $content = str_replace($PRODIGE->getUsername(), "******", $content);
        $content = str_replace($CATALOGUE->getUsername(), "******", $content);
        $content = str_replace($this->container->getParameter("tampon_user"), "******", $content);

        $response->setContent($content);
        return $response;
    }

    /**
     * Change search_path on  Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function setSearchPath(\Doctrine\DBAL\Connection $conn, $schema = "public")
    {
        $conn->exec('set search_path to ' . $schema);
        return $conn;
    }

    /**
     * Get Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name = null, $schema = "public")
    {
        $conn = $this->getDoctrine()->getConnection($connection_name);
        $schema && $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    public function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(ProdigeBaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    public function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(ProdigeBaseController::CONNECTION_CATALOGUE, $schema);
    }

    /**
     * @param \Doctrine\DBAL\Connection $db_connection
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function curlToGeosource($route, array $parameters = array(), $method = "GET")
    {

        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

        $url_parameters = ($method == "GET" ? empty($parameters) ? "" : "?" . http_build_query($parameters) : "");
        $post_parameters = ($method == "GET" ? array() : $parameters);

        $parameters = (empty($parameters) ? "" : "?" . http_build_query($parameters));
        $url = $route . $url_parameters;

        if ($method == "GET") {
            return $geonetwork->get($url);
        } else {
            return $geonetwork->post($url, $post_parameters);
        }
    }

    /**
     * @param \Doctrine\DBAL\Connection $db_connection
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function curlToCatalogue($route, array $parameters = array(), $method = "GET")
    {
        $url_parameters = ($method == "GET" ? empty($parameters) ? "" : "?" . http_build_query($parameters) : "");
        $post_parameters = ($method == "GET" ? array() : $parameters);
        $url = rtrim($this->container->getParameter("PRODIGE_URL_CATALOGUE"), '/') . $route . $url_parameters;
        return $this->curl($url, $method, array(), $post_parameters);
    }

    /**
     * Update Geosource Metadata information and create the attribute catalogue
     * @param integer $metadata_id
     * @param array $metadata_fields
     * @param boolean $bUpdateArbo
     */
    public function duplicateGeosourceMetadata($uuid)
    {
        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        $sql = " select m.id from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m where m.uuid= :uuid";
        $fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $uuid));
        $geonetwork = $this->getGeonetworkInterface();
        $group_id = $geonetwork->getMetadataGroupOwner($CATALOGUE, $fmeta_id);

        $relatedid = null;
        $relatedUuid = null;
        /*
        * Validation de la transaction puis redirection vers la duplication de la fiche geonetwork (qui génÃ¨rera la fiche_metadonnees)
        */
        try {
            $errormsg = "Echec de la duplication / Geosource indisponible";
            $json = $geonetwork->createMetadata($fmeta_id, $group_id);
        } catch (\Exception $exception) {
            throw new \Exception($errormsg, $exception);
        }

        $json = (is_string($json) ? json_decode($json) : $json);
        if ($json && $json->id) {
            $relatedid = $json->id;
            $query = "select uuid from public.metadata where id=:id";
            $relatedUuid = $CATALOGUE->fetchColumn($query, array("id" => $relatedid));
        } else {
            throw new \Exception("Impossible de dupliquer la métadonnée {$uuid}");
        }

        try {
            $errormsg = "Echec de la duplication / Erreur d'affectation des domaines/sous-domaines";
            $query = 'INSERT INTO ssdom_dispose_metadata (uuid, ssdcouch_fk_sous_domaine)' .
                ' SELECT :relatedUuid, ssdcouch_fk_sous_domaine FROM ssdom_dispose_metadata WHERE uuid = :uuid';
            $CATALOGUE->executeQuery($query, compact("relatedUuid", "uuid"));
        } catch (\Exception $exception) {
            throw new \Exception($errormsg, $exception);
        }
        $bVerify = 0;

        $pk_couche_donnees = null;
        $query = 'SELECT pk_couche_donnees, couchd_emplacement_stockage,couchd_type_stockage,accs_adresse_admin ' .
            ' from couche_donnees couche ' .
            ' inner join fiche_metadonnees fmeta on (fmeta.fmeta_fk_couche_donnees=couche.pk_couche_donnees) ' .
            ' left join acces_serveur on (couchd_fk_acces_server=pk_acces_serveur)' .
            ' where fmeta.fmeta_id = :fmeta_id';
        $couches = $CATALOGUE->executeQuery($query, compact("fmeta_id"));
        foreach ($couches as $couche) {
            $couchd_emplacement_stockage = $couche["couchd_emplacement_stockage"];
            $couchd_type_stockage = $couche["couchd_type_stockage"];
            $pk_couche_donnees = $couche["pk_couche_donnees"];

            if ($couchd_type_stockage == 0) {
                $getCopyName = function ($file) use ($CATALOGUE) {
                    $namefile = basename($file);
                    list($tmp_namefile, $extension) = explode(".", $namefile);
                    $files = glob(dirname($file) . "/" . $tmp_namefile . "_copy*." . $extension);
                    if (empty($files)) {
                        $num = 1;
                    } else {
                        sort($files);
                        $last = array_pop($files);
                        list($origname, $suite) = explode("_copy", $last);
                        $num = intval($suite) ?: 0;
                        $num++;
                        if (dirname($file) != '.')
                            $newname = dirname($file) . "/" . $origname . "_copy" . $num . "." . $extension;
                        else
                            $newname = $origname . "_copy" . $num . "." . $extension;
                    }
                    return $newname;
                };
            } else {
                $getCopyName = function ($table) use ($CATALOGUE) {
                    list($origname, $suite) = explode("_copy", $table);
                    $last = $CATALOGUE->fetchColumn("select max(couchd_emplacement_stockage) from couche_donnees where  WHERE couchd_emplacement_stockage like :table||'_copy%'", compact("table"));
                    if (!$last) {
                        $num = 1;
                    } else {
                        list($origname, $suite) = explode("_copy", $last);
                        $num = intval($suite) ?: 0;
                        $num++;
                    }
                    $newname = $origname . "_copy" . $num;
                    return $newname;
                };
            }
            $copy_couchd_emplacement_stockage = $getCopyName($couchd_emplacement_stockage);

            /*if ($bVerify == 0) {
                $accs_adresse = $couche["accs_adresse_admin"];
                
                $accs_adresse = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http").'://' . $accs_adresse . '/PRRA/Administration/Administration/Actions/ActionsDuplication.php';
                                
                if ($couchd_type_stockage == 0)
                    header('location:' .$accs_adresse . '?ACTION=couchefichierExist&SRCFILE=' . $couchd_emplacement_stockage . '&DESFILE=' . $copy_couchd_emplacement_stockage . '&id=' . $metadataId . '&group=2'./*'&domaine=' . $domaine . '&sousdomaine=' . $sousdomaine . '&coucheId=' . $coucheId . '&NOM=' . $_GET["NOM"]);
                else
                    header('Location:' .$accs_adresse . '?ACTION=coucheExist&SRCTABLE=' . $couchd_emplacement_stockage . '&DESTTABLE=' . $copy_couchd_emplacement_stockage . '&id=' . $metadataId . '&group=2'./*'&domaine=' . $domaine . '&sousdomaine=' . $sousdomaine . '&coucheId=' . $coucheId . '&NOM=' . $_GET["NOM"]);
                exit ();
            }*/

            $next_couche_donnees = $CATALOGUE->fetchColumn("SELECT NEXTVAL('SEQ_COUCHE_DONNEES')");
            /*
            * Duplication de la couche de donnees
            */
            try {
                $errormsg = "Echec de la duplication / Erreur de la duplication de la donnée";
                $paramsbind = array(
                    "pk_couche_donnees" => $next_couche_donnees,
                    "couchd_id" => $next_couche_donnees,
                    "couchd_emplacement_stockage" => $copy_couchd_emplacement_stockage,
                );
                $fields = array(
                    "couchd_nom",
                    "couchd_description",
                    "couchd_type_stockage",
                    "couchd_fk_acces_server"
                );
                $query = 'INSERT INTO couche_donnees (pk_couche_donnees, couchd_id, couchd_emplacement_stockage, ' . implode(", ", $fields) . ') ' .
                    ' SELECT :pk_couche_donnees, :couchd_id, :couchd_emplacement_stockage, ' . implode(", ", $fields) .
                    ' FROM couche_donnees ' .
                    ' WHERE pk_couche_donnees = :from_pk_couche_donnees';
                $CATALOGUE->executeQuery($query, array_merge($paramsbind, array("from_pk_couche_donnees" => $pk_couche_donnees)));
            } catch (\Exception $exception) {
                throw new \Exception($errormsg, $exception);
            }

            /*
            * Ajout des domaines sous-domaines
            */
            try {
                $errormsg = "Echec de la duplication / Erreur d'affectation des domaines/sous-domaines";
                $query = 'INSERT INTO ssdom_dispose_couche (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine)' .
                    ' SELECT :to_couche_donnees, ssdcouch_fk_sous_domaine FROM ssdom_dispose_couche WHERE ssdcouch_fk_couche_donnees = :from_couche_donnees';
                $CATALOGUE->executeQuery($query, array("to_couche_donnees" => $next_couche_donnees, "from_pk_couche_donnees" => $pk_couche_donnees));
            } catch (\Exception $exception) {
                throw new \Exception($errormsg, $exception);
            }

            /*
            * Ajout de la relation donnnée/métadonnée
            */
            try {
                $errormsg = "Echec de la duplication / Erreur de création de la relation donnnée/métadonnée";
                $query = 'INSERT INTO FICHE_METADONNEES (FMETA_ID, FMETA_FK_COUCHE_DONNEES, STATUT) VALUES (:relatedId, :next_couche_donnees, 1)';
                $CATALOGUE->executeQuery($query, compact("next_couche_donnees", "relatedId"));
            } catch (\Exception $exception) {
                throw new \Exception($errormsg, $exception);
            }
        }
    }

    /**
     * Update Geosource Metadata information and create the attribute catalogue
     * @param integer $metadata_id
     * @param array $metadata_fields
     * @param boolean $bUpdateArbo
     */
    public function updateAtomArboMetadata($metadata_id, $metadata_fields = array(), $bUpdateArbo = false)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();


        $controller = new UpdateArboController();
        $controller->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "metadata_id" => $metadata_id,
        ));
        $response = $controller->updateArboAction($request);


        $bOpenProdige && !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
    }

    public function deleteGeosourceFeatureCatalogue($metadata_id, $couchd_emplacement_stockage)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();

        $featureCatalogue = new FeatureCatalogueController();
        $featureCatalogue->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "category" => "fcat",
            "metadata_id" => $metadata_id,
            "fcat_fieldOfApplication" => $couchd_emplacement_stockage,
            "mode" => "delete",
        ));
        $response = $featureCatalogue->featureCatalogueAction($request);

        $bOpenProdige && !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
    }

    public function generateGeosourceFeatureCatalogue($metadata_id, $couchd_nom, $couchd_emplacement_stockage, array $couche_fields = array())
    {
        if (empty($couche_fields)) return;
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $PDO = $CATALOGUE->getWrappedConnection();

        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();

        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]";
        $sql_metadata_title = " array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_title";
        $stmt = $PDO->prepare("select xml_is_well_formed(data::text) from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata where id=:metadata_id");
        $stmt->execute(array("metadata_id" => $metadata_id));
        if (!$stmt->fetchColumn()) {
            $sql_metadata_title = "null as metadata_title";
        }

        $stmt = $PDO->prepare("select " . $sql_metadata_title . " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata where id=:metadata_id");
        $stmt->execute(array("metadata_id" => $metadata_id));
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        $metadata_title = $row["metadata_title"];


        $featureCatalogue = new FeatureCatalogueController();
        $featureCatalogue->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "category" => "fcat",
            "metadata_id" => $metadata_id,
            "fcat_MD_DataIdentification" => ($metadata_title ? $metadata_title . " - " : "") . $couchd_nom . ' [' . $couchd_emplacement_stockage . ']',
            "fcat_fieldOfApplication" => $couchd_emplacement_stockage,
            "mode" => "replace",
            "champs" => $couche_fields
        ));
        $response = $featureCatalogue->featureCatalogueAction($request);

        $bOpenProdige && !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    }

    /**
     * Update Geosource Metadata information and create the attribute catalogue
     * @param integer $metadata_id
     * @param array $metadata_fields
     * @param boolean $bUpdateArbo
     */
    public function updateDomSDomMetadata($metadata_id, $sdom = array())
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();

        //update representationType in metadata

        $updateDomSdom = new UpdateDomSdomController();
        $updateDomSdom->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "id" => $metadata_id,
            "mode" => "couche",
            'sdom' => $sdom
        ));
        $response = $updateDomSdom->updateDomSdomAction($request);

        $bOpenProdige && !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    }

    /**
     * Update Geosource Metadata date of change
     * @param integer $metadata_id
     * @param boolean $bUpdateArbo
     */
    public function rebuildViews($couchd_emplacement_stockage)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();

        $viewFactory = new ViewFactory($PRODIGE);
        $viewFactory->rebuildViews($couchd_emplacement_stockage, $this->container);

        $bOpenProdige && !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
    }


    /**
     * Update Geosource Metadata date of change
     * @param integer $metadata_id
     * @param boolean $bUpdateArbo
     */
    public function updateMetadataDateValidity($metadata_id)
    {
        $updateMetadata = new UpdateMetadataController();
        $updateMetadata->setContainer($this->container);
        $request = new Request();
        $response = $updateMetadata->updateDateValidityAction($request, $metadata_id);
    }

    /**
     * Update Geosource Layer Metadata extent
     * @param string $couchd_emplacement_stockage
     */
    public function updateMetadataExtent($couchd_emplacement_stockage)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $mapPath = $this->container->getParameter("PRODIGE_PATH_DATA") . '/cartes/Publication/';

        $mapfileWMS = $mapPath . "wms.map";
        $this->setLayersExtent($mapfileWMS, $PRODIGE, "wms", $couchd_emplacement_stockage);

        $mapfileWFS = $mapPath . "wfs.map";
        $this->setLayersExtent($mapfileWFS, $PRODIGE, "wfs", $couchd_emplacement_stockage);

        $arrayFiles = scandir($mapPath);
        foreach ($arrayFiles as $file) {
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            if ($extension == "map") {
                if (substr($file, 0, 4) == "wms_") {
                    $this->setLayersExtent($mapPath . $file, $PRODIGE, "wms", $couchd_emplacement_stockage);
                }
                if (substr($file, 0, 4) == "wfs_") {
                    $this->setLayersExtent($mapPath . $file, $PRODIGE, "wfs", $couchd_emplacement_stockage);
                }
            }
        }
    }

    protected function setLayersExtent($mapfile, $conn, $type, $couchd_emplacement_stockage)
    {
        if (file_exists($mapfile)) {
            try {
                $oMap = @ms_newMapObj($mapfile);
                if ($oMap) {
                    for ($i = 0; $i < $oMap->numlayers; $i++) {
                        $oLayer = $oMap->getLayer($i);
                        if ($couchd_emplacement_stockage == $oLayer->getMetadata($type . "_abstract")) {
                            $wms_extent = $oLayer->getMetadata($type . "_extent");
                            if (empty($wms_extent)) {
                                $matches = array();
                                preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayer->data, $matches);
                                $table = $matches[2];
                                if (!empty($table) && $table == $couchd_emplacement_stockage) {
                                    $extentDB = $conn->fetchAll("SELECT st_extent(the_geom) FROM public." . $table);
                                    $resultExtentDB = $extentDB[0]["st_extent"];
                                    $resultExtentDB = str_replace("BOX(", "", $resultExtentDB);
                                    $resultExtentDB = str_replace(")", "", $resultExtentDB);
                                    $extent = str_replace(",", " ", $resultExtentDB);
                                    $oLayer->setMetadata($type . "_extent", $extent);
                                    $oMap->save($mapfile);
                                }
                            }
                        }
                    }
                }
            }
            catch (\Exception $exception) {
                $this->getLogger()->error($exception->getMessage());
            }
        }
    }

    /**
     * @param \Doctrine\DBAL\Connection $db_connection
     * @param string|array $query The SQL query with or without comma (simple or multiple queries)
     * @param array $params
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeQuery(\Doctrine\DBAL\Connection $db_connection, $query, $params = array())
    {
        $stmt = null;
        $queries = (is_array($query) ? $query : explode(";", $query));
        foreach ($queries as $query) {
            $query = trim($query);
            if (empty($query)) continue;
            $query = preg_replace("!;$!", "", $query);
            $stmt = $db_connection->executeQuery($query, $params);
        }
        return $stmt;
    }

    protected function analyseRequest(ImportInPostgis $importeur)
    {
        $file_sheet = $importeur->GetImportFeuille();

        $bIsOk = true;

        //       // transform XLS to CSV file to use as input for ogr
        //       if ( strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_XLS) ) {
        //        $bIsOk = $importeur->Xls2csv($file_sheet);
        //        $importeur->setm_fichier($importeur->getm_fichierShape());
        //       }
        //       // transform ODS to CSV file to use as input for ogr
        //       if ( strtolower($importeur->GetTypeFichier())==strtolower(ImportInPostgis::$EXT_ODS) ) {
        //        $bIsOk = $importeur->Ods2csv($file_sheet);
        //        $importeur->setm_fichier($importeur->getm_fichierShape());
        //       }
        $bCSVToDelete = 0;
        $couchd_visualisable = 1;
        $old_extension = "";
        if (strtolower($importeur->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_XYZ)) {
            $importeur->setm_fichier(str_ireplace(".XYZ", ".csv", $importeur->GetImportFichier()));
            $bCSVToDelete = 1;
            $couchd_visualisable = 0;
            $old_extension = ".XYZ";
        }
        if (strtolower($importeur->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_ASC)) {
            $importeur->setm_fichier(str_ireplace(".ASC", ".csv", $importeur->GetImportFichier()));
            $bCSVToDelete = 1;
            $couchd_visualisable = 0;
            $old_extension = ".ASC";
        }

        return array_values(compact('bIsOk', "couchd_visualisable", "bCSVToDelete", "old_extension"));
    }

    /**
     * Analyze the layer file to find the layerfields, the layer encoding, the Geometry, the file sheets
     * @param integer $pk_couche_donnees cf Table couche_donnees
     * @param string $layerfile Relative path to the layer file
     * @param integer $couchd_type_stockage cf Table couche_donnees
     * @param string $layersheet Name of sheet to read or null if it is not found or available
     * @param string $layertype The layer type (VECTOR|TABULAIRE|RASTER)
     * @param string $couchd_emplacement_stockage If necessary, specify the couchd_emplacement_stockage= the db table to create
     * @param string $directory If necessary, specify the directory of the layer file
     * @return array
     * @throws Exception
     */
    protected function convertMNT($directory, $layerfile)
    {
        $xyz = array();
        $asc = array();
        $layerfile = explode('|', $layerfile);

        if (empty($layerfile)) return $layerfile;

        if (is_dir($directory . $layerfile[0])) {
            $dir = $directory . $layerfile[0];
            $xyz = array();

            // appel via Symfony Processs 
            $process = new Process(array("find", $dir, "-maxdepth", "1", "-type", "f", "-iname", "*.xyz"));

            $process->setTimeout(null);
            $process->setTimeout(10800);
            $process->run();
            $xyz = explode(" ", $process->getOutput());

            // version avant passage par symfony Process
            //$cmd = "find ".$dir." -type f -maxdepth 1 -iname '*.xyz' ";
            //exec($cmd, $xyz);

            $asc = array();
            // appel via Symfony Processs 
            $process = new Process(array("find", $dir, "-maxdepth", "1", "-type", "f", "-iname", "*.asc"));

            $process->setTimeout(null);
            $process->setTimeout(10800);
            $process->run();
            $asc = explode(" ", $process->getOutput());

            // version avant passage par symfony Process    
            //$cmd = "find ".$dir." -type f -maxdepth 1 -iname '*.asc' ";
            //exec($cmd, $asc);
        } else {
            foreach ($layerfile as $index => $file) {
                $layerfile[$index] = $file = str_replace("Publication/", "", $file);
                $extension = explode(".", $file);
                $extension = strtolower(end($extension));
                if ($extension == "asc") {
                    $asc[] = $directory . $file;
                }
                if ($extension == "xyz") {
                    $xyz[] = $directory . $file;
                }
            }
        }
        if (empty($xyz) && empty($asc)) return implode('|', $layerfile);

        $deletables = array();
        $files = array();
        if (!empty($xyz)) {
            $files = $xyz;
        } else if (!empty($asc)) {
            //Cas de fichiers .asc, on fait la conversion en fichier xyz
            foreach ($asc as $file) {
                $extension = explode(".", $file);
                $extension = strtolower(end($extension));
                $fileConvertedXYZ = str_ireplace("." . $extension, ".XYZ", $file);

                $res = array();
                $return = "";
                // $cmd = "gdal_translate -of XYZ '" . $file . "' '" . $fileConvertedXYZ . "'  2>&1";
                $cmd = ["gdal_translate", "-of", "XYZ", "' . $file . '", "' . $fileConvertedXYZ . '", "2>&1"];
                $process = new Process($cmd);
                $process->setTimeout(10800);
                $process->run();
                $return = $process->getErrorOutput();
                $res = explode(" ", $process->getOutput());
                // avant passage des appels de commande par Process 
                //$cmd = "gdal_translate -of XYZ '".$file."' '".$fileConvertedXYZ."'";
                //exec($cmd." 2>&1", $res, $return);
                if ($return) {
                    throw new \Exception("Problème lors de la conversion du fichier " . $file . " à l'aide de gdal_translate." . implode("<br>", $res));
                }
                $files[] = $fileConvertedXYZ;
                $deletables[] = $fileConvertedXYZ;
            }
        }

        $tempFile = tempnam($directory . "/temp", "mnt_concat_");
        @unlink($tempFile);
        $csvFile = $tempFile . ".CSV";
        @unlink($csvFile);
        $HEADERS = array("separator" => "\t", "columns" => array("X", "Y", "Z"));
        $separators = array("\t", " ", ",");
        $cmds = array();
        foreach ($files as $file) {
            $res = array();
            $return = "";
            foreach ($separators as $separator) {
                $fopen = fopen($file, "r");
                $headers = fgetcsv($fopen, null, $separator);
                $sheader = implode($separator, $headers);
                fclose($fopen);
                if (is_array($headers) && count($headers) > 1) {
                    $HEADERS["separator"] = $separator;
                    break;
                }
            }
            $hasHeader = array_reduce($headers, function ($previous, $item) {
                return $previous && !is_numeric($item);
            }, true);

            if ($hasHeader) {
                $HEADERS["columns"] = array_merge($HEADERS["columns"], $headers);
                $cmds[] = "egrep -v '" . $sheader . "' '" . $file . "' >> '" . $csvFile . "'";
            } else {
                $cmds[] = "cat '" . $file . "' >> '" . $csvFile . "'";
            }
        }
        $HEADERS["columns"] = array_unique($HEADERS["columns"]);

        file_put_contents($csvFile, implode($HEADERS["separator"], $HEADERS["columns"]) . "\n");
        foreach ($cmds as $cmd) {
            $res = array();
            $return = "";
            $process = new Process([$cmd, "2>&1"]);
            $process->setTimeout(10800);
            $process->run();
            $res = explode(" ", $process->getOutput());
            $return = $process->getErrorOutput();
            // avant passages des appels par Process
            //exec($cmd." 2>&1", $res, $return);
            if ($return) {
                throw new \Exception("Problème lors de la mise en relation des fichiers sélectionnés.");
            }
        }
        @array_map("unlink", $deletables);
        file_put_contents($tempFile . ".filelist", $directory . "/" . implode("\n" . $directory . "/", $layerfile));
        return $csvFile;
    }

    /**
     * Analyze the layer file to find the layerfields, the layer encoding, the Geometry, the file sheets
     * @param integer $pk_couche_donnees cf Table couche_donnees
     * @param string $layerfile Relative path to the layer file
     * @param integer $couchd_type_stockage cf Table couche_donnees
     * @param string $layersheet Name of sheet to read or null if it is not found or available
     * @param string $layertype The layer type (VECTOR|TABULAIRE|RASTER)
     * @param string $couchd_emplacement_stockage If necessary, specify the couchd_emplacement_stockage= the db table to create
     * @param string $directory If necessary, specify the directory of the layer file
     * @return array
     * @throws Exception
     */
    protected function getGISFileProperties($pk_couche_donnees, $layerfile, $couchd_type_stockage, $layersheet = null, $layertype = "VECTOR", $couchd_emplacement_stockage = null, $directory = null)
    {
        $tabDatatype = array(
            "int" => "int4",
            "int4" => "int4",
            "integer" => "int4",
            "numeric0" => "int4",
            "integer64" => "int4",
            "bigint" => "int4",
            "float" => "float",
            "real" => "float",
            "numeric" => "float",
            "double" => "float",
            "double precision" => "float",
            "string" => "text",
            "varchar" => "text",
            "character varying" => "text",
            "text" => "text",
            "date" => "date",
            "timestamp without time zone" => "timestamp",
        );

        $layerfileJson = "";

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);

        $extension = strtolower(pathinfo($layerfile, PATHINFO_EXTENSION));
        $isSpreadSheet = in_array($extension, self::$DATATYPE_EXTENSION["SPREADSHEET"]);

        $columns = array();
        try {
            if (!$couchd_emplacement_stockage && $pk_couche_donnees) {
                $couchd_emplacement_stockage = $CATALOGUE->fetchColumn(
                    "select couchd_emplacement_stockage from couche_donnees where pk_couche_donnees=:pk_couche_donnees",
                    compact("pk_couche_donnees"),
                    0
                );
            }

            if ($couchd_emplacement_stockage && array_key_exists($couchd_type_stockage, self::$TABLE_STOCKAGES)) {

                $querycolumns = $PRODIGE->executeQuery(
                    "select column_name, data_type, numeric_scale " .
                    " from information_schema.columns  " .
                    " where table_schema=:table_schema and table_name=:table_name",
                    array("table_schema" => self::$TABLE_STOCKAGES[$couchd_type_stockage], "table_name" => $couchd_emplacement_stockage)
                );
                foreach ($querycolumns as $row) {
                    $data_type = strtolower($row["data_type"]);
                    if ($data_type == "numeric" && $row["numeric_scale"] == 0) {
                        $data_type = "numeric0";
                    }
                    $columns[$row["column_name"]] = $tabDatatype[$data_type];
                }
            }
        } catch (\Exception $exception) {
        }

        $get_encoding = "data";
        $original_layertype = $layertype;
        $_layertype = $layertype;
        if ($_layertype == "TABULAIRE") $_layertype = "VECTOR";
        if ($_layertype == "MULTI") $_layertype = "VECTOR";
        $directory = rtrim(($directory ?: $this->getMapfileDirectory()), '/') . '/';
        $layerfile = urldecode($layerfile);
        $layerfile = preg_replace("!^Publication/!", "", $layerfile);

        if ($layertype == "MNT") {
            $get_encoding = "headers";
            $layerfile = $this->convertMNT($directory, $layerfile);
            $_layertype = "VECTOR";
            $layertype = "TABULAIRE";
        }

        if ($extension == "json" && $layertype == "TABULAIRE") {
            $layerfileJson = $layerfile;
            try {
                $layerfile = $this->checkJson($directory . $layerfile);
            } catch (\Exception $exception) {
                return array("erreurJson" => $exception->getMessage());
            }
        }

        if (file_exists($layerfile))
            $layerfile = str_replace(rtrim(realpath($directory), "/") . "/", "", realpath($layerfile));
        else if (file_exists($directory . $layerfile))
            $layerfile = str_replace(rtrim(realpath($directory), "/") . "/", "", realpath($directory . $layerfile));
        $layerfile = urlencode($layerfile);

        $this->getLogger()->debug(__METHOD__ . "  -  LAYERFILE  :", (array)$layerfile);
        $parameters = array("layertype" => $_layertype, "layerfile" => $layerfile, "layersheet" => $layersheet);
        $parameters = array_diff($parameters, array(null));
        $params = array_merge(array("directory" => $directory), /*$request->query->all(), $request->request->all()*/ array());

        $carmen_ws_helpers_geometrytype =
            str_replace("app_dev.php/", "", str_replace(
                "/api/mapserver",
                "",
                str_replace(
                    "%2F",
                    "%252F",
                    $this->generateUrl("carmen_ws_helpers_geometrytype", array_merge($parameters, array("encoding" => "headers")))
                )
            ));

        $carmen_ws_helpers_getsheets =
            str_replace("app_dev.php/", "", str_replace(
                "/api/mapserver",
                "",
                str_replace(
                    "%2F",
                    "%252F",
                    $this->generateUrl("carmen_ws_helpers_getsheets", $parameters)
                )
            ));

        $carmen_ws_helpers_layerfields =
            str_replace("app_dev.php/", "", str_replace(
                "/api/mapserver",
                "",
                str_replace(
                    "%2F",
                    "%252F",
                    $this->generateUrl("carmen_ws_helpers_layerfields", array_merge($parameters, array("encoding" => $get_encoding)))
                )
            ));

        $geometrytype = $this->callMapserverApi($carmen_ws_helpers_geometrytype, "GET", $params, array());
        $bGetFields = ($isSpreadSheet ? !is_null($layersheet) : true);
        $properties = array("layerfields" => array(), "nbFieldsShared" => 0, "columns" => $columns, "sheets" => array(), "encoding" => "UTF-8");

        if ($bGetFields) {
            try {
                $fields = $this->callMapserverApi($carmen_ws_helpers_layerfields, "GET", $params, array());
            } catch (ApiException $exception) {
                $data = $exception->getData();
                if (isset($data) && isset($data["results"])) {
                    return $data["results"];
                }
            }

            if (!$fields) $fields = array();
            if (!isset($fields["fields_encoded"])) $fields["fields_encoded"] = array();
            if (isset($fields["encoding"])) $properties["encoding"] = $fields["encoding"];

            $properties["layerfields"] = $fields["fields_encoded"];
            $nbFieldsShared = 0;

            foreach ($properties["layerfields"] as $field_index => $field) {
                $field["field_datatype"] = $tabDatatype[strtolower($field["field_datatype"])];
                if ($original_layertype == "MNT") {
                    $field["field_datatype"] = "float";
                }
                $field["field_index"] = $field_index;
                $field["field_visible"] = (empty($columns) || (array_key_exists(strtolower($field["field_name"]), $columns) && $columns[strtolower($field["field_name"])] == $field["field_datatype"]));
                if ($field["field_visible"] === true && !empty($columns)) $nbFieldsShared++;

                $properties["layerfields"][$field_index] = $field;
            }

            $properties["import_action"] = "update,replace";
            if ($nbFieldsShared == 0) {
                if (empty($columns)) $properties["import_action"] = "create";
                else $properties["import_action"] = "replace";
            }
            $properties["nbFieldsShared"] = $nbFieldsShared;
        }

        if (stripos($properties["encoding"], "UNKNOW") !== false) $properties["encoding"] = "Latin-1";
        if (stripos($properties["encoding"], "ASCII") !== false) $properties["encoding"] = "UTF-8";
        if (stripos($properties["encoding"], "ISO-8859") !== false) $properties["encoding"] = "Latin-1";

        $properties["columns"] = $columns;
        if ($isSpreadSheet) {
            $sheets = $this->callMapserverApi($carmen_ws_helpers_getsheets, "GET", $params, array());
            $properties["sheets"] = $sheets["sheets"];
        }
        $result = array_merge($geometrytype, $properties);
        $result["layerfile"] = str_replace(realpath($directory . "/"), "", $layerfile);

        if ($layerfileJson == "") {
            $result["layerfile"] = str_replace(realpath($directory . "/"), "", $layerfile);
        } else {
            $result["layerfile"] = str_replace(realpath($directory . "/"), "", $layerfileJson);
            $cmd = 'rm -rf "' . $this->getMapfileDirectory() . trim(urldecode($layerfile)) . '"';
            $process = new Process([$cmd]);
            $process->run();
        }
        return $result;
    }

    /**
     * Configure l'importeur dans Postgis. Récupère les champs du fichier au préalable
     * @param ImportInPostgis $importeur
     * @param unknown $table
     * @param unknown $fichier
     * @param unknown $metadata_id
     * @param string $feuille
     * @param string $fichierShape
     * @param string $data_encoding
     */
    protected function setImportInPostgisConfig(ImportInPostgis $importeur, $pk_couche_donnees, $layertype, $couchd_emplacement_stockage, $file_source, $fmeta_id = -1, $layersheet = null, $fichierShape = null, $data_encoding = 'UTF-8', $fields)
    {
        //$properties = $this->getGISFileProperties($pk_couche_donnees, $file_source, $couchd_type_stockage, $layersheet, $layertype);
        $importeur->setFileFields($fields);
        $importeur->setDataExecution($couchd_emplacement_stockage, $file_source, $fmeta_id ?: -1, $layersheet, $fichierShape, $data_encoding);
    }

    /**
     * Get all fields in db table with unique values
     * @param ImportInPostgis $importeur
     * @return array
     */
    protected function getPGTableUniqueFields(ImportInPostgis $importeur)
    {
        $champs = array();
        $pgChps = $importeur->GetChampsPostGIS();
        foreach ($pgChps as $name => $type) {
            if (strtolower($name) != "the_geom" && strtolower($name) != "gid") {
                if ($importeur->IsChampUnique($name)) {
                    $champs[utf8_encode($name)] = array("field_name" => $name);
                }
            }
        }
        return array_values($champs);
    }

    /**
     * Supprime les domaines sous-domaines de la métadonnée à ses couches
     * @param Connection $CATALOGUE
     * @param int $fmeta_id
     * @param int $pk_couche_donnees
     */
    protected function detachDomaineSousDomaine(Connection $CATALOGUE, $pk_couche_donnees)
    {
        $params = array($pk_couche_donnees);

        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $tabSql = array(
            "delete from ssdom_dispose_couche where ssdcouch_fk_couche_donnees=:pk_couche_donnees"
        );
        try {
            foreach ($tabSql as $query) {
                $CATALOGUE->executeQuery($query, $params);
            }
            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
        }
    }

    /**
     * Affecte les domaines sous-domaines de la métadonnée à ses couches
     * @param Connection $CATALOGUE
     * @param int $fmeta_id
     * @param int $pk_couche_donnees
     */
    protected function attachDomaineSousDomaine(Connection $CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction = true)
    {
        $params = compact("pk_couche_donnees", "fmeta_id");

        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        $tabSql = array(
            "delete from ssdom_dispose_couche where ssdcouch_fk_couche_donnees=:pk_couche_donnees", "insert into ssdom_dispose_couche (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine) " .
            " select donnees.pk_couche_donnees, ssdcouch_fk_sous_domaine" .
            " from ssdom_dispose_metadata dommeta " .
            " inner join public.metadata meta using (uuid) " .
            " inner join fiche_metadonnees fmeta on (fmeta.fmeta_id=meta.id::text) " .
            " inner join couche_donnees donnees on (donnees.pk_couche_donnees = fmeta.fmeta_fk_couche_donnees) " .
            " where fmeta.fmeta_id=:fmeta_id " .
            " and donnees.pk_couche_donnees=:pk_couche_donnees"
        );

        try {
            foreach ($tabSql as $query) {
                $CATALOGUE->executeQuery($query, $params);
            }
            //clean cache
            $meminstance = new \Memcached(); 
            $meminstance->addServer("datacarto-memcached", 11211);
            $meminstance->flush();
            $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
        } catch (\Exception $exception) {
            $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
        }
    }

    /**
     * Vérifie si une base de donnée existe
     *
     * @param  $dbname
     *
     * @return
     */
    protected function isDbExist($dbname)
    {
        $TAMPON_AUTH = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD = $this->container->getParameter("tampon_password");
        $cmd = 'PGPASSWORD=' . $TAMPON_AUTH_PWD . ' psql -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $TAMPON_AUTH . ' -lqt | cut -d \| -f 1 | grep -w "' . $dbname . '"';

        $process = Process::fromShellCommandline($cmd);
        $process->setTimeout(10800);
        $process->run();

        // ancien standart Symfony Process
        //  $cmd = 'PGPASSWORD='.$TAMPON_AUTH_PWD.' psql -h '.$this->container->getParameter('prodige_host').' -U '.$TAMPON_AUTH.' -lqt | cut -d \| -f 1 | grep -w "'.$dbname.'"';
        //$process = new \Symfony\Component\Process\Process($cmd);
        //$process->run();

        return (!empty($process->getOutput()));
    }

    /**
     * Retourne une connection vers la base tampon
     *
     * @param  $dbname
     *
     * @return \Doctrine\DBAL\Connection
     */
    protected function getDBTamponConnection($dbname)
    {
        if ($this->isDbExist($dbname)) {
            $TAMPON_AUTH = $this->container->getParameter("tampon_user");
            $TAMPON_AUTH_PWD = $this->container->getParameter("tampon_password");

            return $this->get('doctrine.dbal.connection_factory')
                ->createConnection(array('pdo' => new \PDO(str_replace('pdo_', '', $this->container->getParameter('prodige_driver')) . ":host=" . $this->container->getParameter('prodige_host') . ";dbname=$dbname", $TAMPON_AUTH, $TAMPON_AUTH_PWD)));
        }

        return null;
    }

    /**
     * Supprime une base
     *
     * @param $dbname
     */
    protected function dropDb($dbname)
    {
        $TAMPON_AUTH = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD = $this->container->getParameter("tampon_password");

        $cmd = 'PGPASSWORD=' . $TAMPON_AUTH_PWD . ' dropdb -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $TAMPON_AUTH . ' --if-exists ' . $dbname;
        $process = new \Symfony\Component\Process\Process($cmd);
        $process->setTimeout(10800);
        $process->run();

        // ancien standart Symfony Process
        //$cmd = 'PGPASSWORD='.$TAMPON_AUTH_PWD.' dropdb -h '.$this->container->getParameter('prodige_host').' -U '.$TAMPON_AUTH.' --if-exists '.$dbname;
        //$process = new \Symfony\Component\Process\Process($cmd);
        //$process->run();
    }

    /**
     * Supprime un schéma d'une base
     *
     * @param  $schema
     * @param  $dbname
     */
    protected function dropSchema($schema, $dbname)
    {
        $cmd = 'PGPASSWORD=' . $this->container->getParameter('prodige_password') . ' psql -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $this->container->getParameter('prodige_user') . ' -d ' . $dbname . ' -c \' DROP SCHEMA IF EXISTS ' . $schema . ' CASCADE \' ';
        $process = new \Symfony\Component\Process\Process($cmd);
        $process->setTimeout(10800);
        $process->run();
        // ancien standart Symfony Process
        //$cmd = 'PGPASSWORD='.$this->container->getParameter('prodige_password').' psql -h '.$this->container->getParameter('prodige_host').' -U '.$this->container->getParameter('prodige_user').' -d '.$dbname.' -c \' DROP SCHEMA IF EXISTS '.$schema.' CASCADE \' ';
        //$process = new \Symfony\Component\Process\Process($cmd);
        //$process->run();
    }

    /**
     * Retourne la liste des schemas d'une base
     *
     * @param  $connection
     *
     * @return array
     */
    protected function getSchemasFromDB($connection)
    {
        $schemas = array();

        if ($connection) {
            $sql = "SELECT DISTINCT table_schema FROM information_schema.tables WHERE table_schema NOT IN ('pg_catalog', 'information_schema'); ";
            $results = $connection->fetchAll($sql);
            if (count($results)) {
                $schemas = array_column($results, 'table_schema');
            }
        }

        return $schemas;
    }

    /**
     * Retourne le nom des tables d'une base
     *
     * @param  $connection
     * @param  $schema
     *
     * @return
     */
    protected function getTablesFromDB($connection, $schema = null)
    {
        $tables = array();

        if ($connection) {
            $sql = "SELECT DISTINCT c.table_name
                    FROM information_schema.columns c
                    INNER JOIN information_schema.tables t
                    ON c.table_name = t.table_name 
                    WHERE t.table_schema NOT IN ('pg_catalog', 'information_schema')" .
                (!is_null($schema) ? " AND t.table_schema = '$schema' " : "") .
                "AND t.table_type = 'BASE TABLE'
                    AND t.table_name != 'spatial_ref_sys'; ";

            $results = $connection->fetchAll($sql);
            if (count($results)) {
                $tables = array_values(array_column($results, 'table_name'));
            }
        }

        return $tables;
    }

    /**
     * Retourne le nom du schema de la base Tampon
     *
     * @param  $connection
     *
     * @return
     */
    protected function getTamponSchema($connection)
    {
        $schema = '';

        $schemas = $this->getSchemasFromDB($connection);
        if (count($schemas) == 1 && in_array(self::CATALOGUE_SCHEMA_PUBLIC, $schemas)) {
            $schema = $schemas[0];
        }

        if (count($schemas) == 2 && in_array(self::CATALOGUE_SCHEMA_PUBLIC, $schemas)) {
            $schema = ($schemas[0] != self::CATALOGUE_SCHEMA_PUBLIC ? $schemas[0] : $schemas[1]);
        }

        return $schema;
    }

    /**
     * Retourne la liste des schemas d'un fichier
     *
     * @param  $file
     *
     * @return
     */
    protected function getSchemasFromFile($file)
    {
        $schemas = array();

        if (file_exists($file)) {

            $process = new Process(array('grep', '-i', 'create schema', $file));
            // ancien standart Symfony Process
            //$regexExp = 'create schema';
            //$cmd = 'grep -i "'.$regexExp.'" '.$file;
            //$process = new \Symfony\Component\Process\Process($cmd);
            $process->setTimeout(10800);
            $process->run();
            $strOutput = $process->getOutput();

            $regexExp = 'create schema';
            if (!empty($strOutput)) {
                $arOutput = explode($regexExp, strtolower($strOutput));
                if (count($arOutput)) {
                    foreach ($arOutput as $output) {
                        if (!empty(trim($output))) {
                            $strSchemas = str_replace('IF NOT EXISTS ', '', str_replace(';', '', trim(strtolower($output))));
                            $arSchemas = explode(',', $strSchemas);
                            if (count($arSchemas)) {
                                foreach ($arSchemas as $schema) {
                                    preg_match('/^[a-z][a-z0-9_]*$/', $schema, $matches);
                                    if (count($matches)) {
                                        $schemas[] = trim($schema);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $schemas;
    }

    /**
     * Retourne le nom des tables d'un fichier
     *
     * @param  $file
     *
     * @return
     */
    protected function getTablesFromFile($file)
    {
        $tables = array();

        if (file_exists($file)) {

            $process = new Process(array('grep', '-i', 'create table', $file));
            //            $cmd = 'grep -i "'.$regexExp.'" '.$file;
            //            $process = new \Symfony\Component\Process\Process($cmd);
            $process->setTimeout(10800);
            $process->run();
            $strOutput = $process->getOutput();

            $regexExp = 'create table';
            if (!empty($strOutput)) {
                $arOutput = explode($regexExp, strtolower($strOutput));
                if (count($arOutput)) {
                    foreach ($arOutput as $output) {
                        if (!empty(trim($output))) {
                            //preg_match('/^.*\.(.*)\s{0,}\(.*$/', trim($output), $matches) || preg_match('/^(.*)\s{0,}\(.*$/', trim($output), $matches);
                            preg_match('/^[^(]\.([^(\s*])\s*\(.*$/', trim($output), $matches) || preg_match('/^([^(\s*]+)\s*\(.*$/', trim($output), $matches);
                            if (count($matches)) {
                                $tables[] = $matches[1];
                            }
                        }
                    }
                }
            }
        }

        return $tables;
    }

    /**
     * Retourne le couche type stockage d'une table de la base temporaire tampon
     *
     * @param $connection connexion à la base de données
     * @param $dbname
     * @param $couchd_emplacement_stockage
     *
     * @return
     */
    protected function getCouchdTypeStockage($connection, $dbname, $couchd_emplacement_stockage)
    {
        if ($connection) {
            $sql = "SELECT 
                        CASE
                            WHEN 'geometry' IN (SELECT udt_name FROM information_schema.columns WHERE table_name = :table_name) THEN 1
                            ELSE -3
                        END AS couchd_type_stockage; ";

            return $connection->fetchColumn($sql, array("table_name" => $couchd_emplacement_stockage), 0);
        }

        return -3;
    }

    /**
     * Importe un fichier sql dans une base temporaire
     *
     * @param  $mode
     * @param  $dbname
     * @param  $file_source
     *
     * @return
     */
    protected function ImportDumpFile($mode, $dbname, $file_source)
    {
        $TAMPON_AUTH = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD = $this->container->getParameter("tampon_password");

        $directory = $this->getMapfileDirectory() . '/';
        $sqlfile = urldecode($file_source);
        $sqlfile = preg_replace("!^Publication/!", "", $sqlfile);
        if (file_exists($directory . $sqlfile)) {
            $sqlfile = str_replace(rtrim(realpath($directory), "/") . "/", "", realpath($directory . $sqlfile));
        }

        $cmd = '';
        // $process = new Process('');
        $process = new Process([]);
        //$process = new \Symfony\Component\Process\Process($cmd);
        $process->setTimeout(10800);

        if (in_array($mode, [self::ACTION_CREATE, self::ACTION_REPLACE])) {
            if ($mode == self::ACTION_REPLACE) {
                $this->dropDb($dbname);
            }

            $cmd_args = array(
                'PGPASSWORD=' . $TAMPON_AUTH_PWD,
                'createdb', '-h', $this->container->getParameter('prodige_host'),
                '-U', $TAMPON_AUTH,
                '-E', 'UTF8',
                '-O', $TAMPON_AUTH,
                '-T', self::TAMPON_TEMPLATE,
                $dbname
            );

            $cmd = 'PGPASSWORD=' . $TAMPON_AUTH_PWD . ' createdb -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $TAMPON_AUTH . ' -E UTF8 -O ' . $TAMPON_AUTH . ' -T ' . self::TAMPON_TEMPLATE . ' ' . $dbname;
            $process->setCommandLine($cmd);
            $process->setTimeout(10800);
            $process->run();
            if (!empty($process->getErrorOutput())) {
                $this->dropDb($dbname);

                return array('status' => 500, 'description' => $this->formatStringError($process->getErrorOutput()));
            }
        }

        $cmd = 'PGPASSWORD=' . $TAMPON_AUTH_PWD . ' psql -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $TAMPON_AUTH . ' -d ' . $dbname . ' -f ' . $directory . $sqlfile;
        $process->setCommandLine($cmd);

        $process->setTimeout(10800);
        $process->run();
        if (!empty($process->getErrorOutput())) {
            if ($mode != self::ACTION_ADD) {
                $this->dropDb($dbname);
            }

            return array('status' => 500, 'description' => $this->formatStringError($process->getErrorOutput()));
        }

        return array('status' => 200);
    }

    /**
     * Transfére un schéma de la base tampon vers la base prodige
     *
     * @param  $dbanmeSource
     * @param  $schemaSource
     * @param  $dbanmeDestination
     * @param  $schemaDestination
     * @param  $mode
     *
     * @return
     */
    protected function ImportSchema($dbanmeSource, $schemaSource, $dbanmeDestination, $schemaDestination, $mode)
    {
        $TAMPON_AUTH = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD = $this->container->getParameter("tampon_password");

        $schemasProdige = $this->getSchemasFromDB($this->getProdigeConnection());
        // Mode replace : le schema existe normalement, on le supprime
        if ($mode == self::ACTION_REPLACE) {
            if (in_array(strtolower(trim($schemaDestination)), array_map(function ($schema) {
                return strtolower(trim($schema));
            }, $schemasProdige))) {
                $this->dropSchema($schemaDestination, $dbanmeDestination);
            }
        }
        // Mode création : le schema ne doit pas exister
        if ($mode == self::ACTION_CREATE) {
            if (in_array(strtolower(trim($schemaDestination)), array_map(function ($schema) {
                return strtolower(trim($schema));
            }, $schemasProdige))) {
                return array('status' => 500, 'description' => "Le nom du schéma de la base $dbanmeDestination de données existe déjà ");
            }
        }

        $cmd = '  PGPASSWORD=' . $TAMPON_AUTH_PWD . ' pg_dump -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $TAMPON_AUTH . ' -x -O -n ' . $schemaSource . ' -T spatial_ref_sys ' . $dbanmeSource . ' ' .
            '| sed \'/CREATE SCHEMA ' . $schemaSource . '/d\' ' . /* supprimer create schema si exist */
            '| sed \'1 i\CREATE SCHEMA ' . $schemaDestination . ';\' ' . /* ajouter create schema avec le nouveau nom (schema destination) */
            '| sed \'s/^SET search_path.*$/SET search_path to ' . $schemaDestination . ', public ;/\' ' .
            '| sed \'s/' . $schemaSource . '\./' . $schemaDestination . '\./\' ' .
            '| PGPASSWORD=' . $this->container->getParameter('catalogue_password') . ' psql -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $this->container->getParameter('prodige_user') . ' -d ' . $dbanmeDestination;
        $process = new \Symfony\Component\Process\Process($cmd);
        $process->setTimeout(10800);
        $process->run();
        if (!empty($process->getErrorOutput())) {
            return array('status' => 500, 'description' => $this->formatStringError($process->getErrorOutput()));
        }

        return array('status' => 200);
    }

    /**
     * Importe un fichier dump sql dans la base prodige
     *
     * @param  $request
     * @param  $uuid
     * @param  $fmeta_id
     * @param  $layertype
     * @param  $schemaDestination
     * @param  $mode
     * @param  $action
     *
     * @return
     */
    public function doImportDumpInPostgis(ParameterBag $parametersBag, $uuid, $fmeta_id, $layertype, $schemaDestination, $mode, $action)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);

        $data = $parametersBag->get('data', array());
        $file_source = $parametersBag->get('file_source', '');

        $dbname = self::TAMPON_PREFIX_DB_NAME . $uuid;

        $schema = '';
        $tables = array();
        try {
            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
            !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();

            $response = $this->ImportDumpFile($mode, $dbname, $file_source);
            if ($response['status'] == 500 && !$this->isDbExist($dbname)) {
                return array("status" => $response['status'], "description" => $response['description'], "extra" => ['schema' => $schema, 'tables' => $tables]);
            }

            $connection = $this->getDBTamponConnection($dbname);

            $schemaSource = $this->getSchemasFromDB($connection)[0];
            $dbanmeDestination = $this->container->getParameter('prodige_name');
            $response = $this->ImportSchema($dbname, $schemaSource, $dbanmeDestination, $schemaDestination, $mode);

            $schema = $this->getTamponSchema($connection);
            $tables = $this->getTablesFromDB($connection);

            /* fermer les connexions des bases de données explicitement */
            //unset($connection);
            $connection = null;
            gc_collect_cycles();

            if ($response['status'] == 500) {
                if ($mode != self::ACTION_ADD) {
                    $this->dropDb($dbname);
                }

                return array("status" => $response['status'], "description" => $response['description'], "extra" => ['schema' => $schema, 'tables' => $tables]);
            }

            if ($action == self::ACTION_DELETE_DB) {
                $this->dropdb($dbname);
            }

            // IMPORT IN POSTGIS
            $importeur = new ImportInPostgis($this);

            $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_description", "couchd_type_stockage", "couchd_emplacement_stockage", "couchd_fk_acces_server");
            foreach ($data as $element) {
                extract($element);
                $couchd_id = (isset($element['couchd_id']) && !empty($element['couchd_id']) ? (int)$element['couchd_id'] : null);
                if (!$couchd_id) {
                    // ajout d'une nouvelle couche
                    $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                    $couchd_id = (string)$pk_couche_donnees;
                    $couchd_type_stockage = $this->getCouchdTypeStockage($PRODIGE, $dbname, str_replace($schemaDestination . '.', '', $element['couchd_emplacement_stockage']));
                    $couchd_fk_acces_server = 1;
                    $CATALOGUE->executeQuery("insert into couche_donnees (" . implode(", ", $coucheFields) . ") values (:" . implode(", :", $coucheFields) . ")", compact($coucheFields));
                    $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees, schema) values (:fmeta_id, :pk_couche_donnees, :schema)", array_merge(compact("fmeta_id", "pk_couche_donnees"), array('schema' => $schemaDestination)));
                } else {
                    // maj d'une couche existante
                    $pk_couche_donnees = (int)$couchd_id;
                    $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_description", "couchd_type_stockage", "couchd_emplacement_stockage");

                    $couchd_type_stockage = $this->getCouchdTypeStockage($PRODIGE, $dbname, str_replace($schemaDestination . '.', '', $element['couchd_emplacement_stockage']));
                    $set = array_combine($coucheFields, $coucheFields);
                    $set = str_replace("=", "=:", http_build_query($set, null, ", "));
                    $CATALOGUE->executeQuery("update couche_donnees set " . $set . " where pk_couche_donnees=:pk_couche_donnees", compact($coucheFields));
                }

                $tab = explode(".", $couchd_emplacement_stockage);

                $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees);
                $couche_fields = $importeur->getFeatureCatalogFields(end($tab), $tab[0]);
                $this->generateGeosourceFeatureCatalogue($fmeta_id, $couchd_nom, $couchd_emplacement_stockage, $couche_fields);
                $this->updateDomSDomMetadata($fmeta_id);
                $this->updateAtomArboMetadata($fmeta_id);
                $this->updateMetadataDateValidity($fmeta_id);
                $this->updateMetadataExtent(end($tab));

                $PRODIGE->executeQuery("set search_path to public");
                $PRODIGE->executeQuery("select populate_geometry_columns((:schemaname||'.'||:tablename)::regclass)", array("tablename" => end($tab), "schemaname" => $tab[0]));

                $this->rebuildViews($couchd_emplacement_stockage);
            }

            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
            $PRODIGE->isTransactionActive() && $PRODIGE->commit();

            return array("extra" => ['schema' => $schema, 'tables' => $tables], "status" => 200);
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();

            return array("status" => 500, "description" => $exception->getMessage(), "extra" => []);
        }
    }

    /**
     * Exectute the import of the data for an insertion into Postgis Database
     * @param Request $request
     * @param unknown $fmeta_id
     * @param string $layertype
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doImportInPostgis(ParameterBag $parametersBag, $fmeta_id, $layertype = "VECTOR", $closeTransaction = true, $schema = self::PRODIGE_SCHEMA_PUBLIC)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $result = null;
        /**
         * extraction des paramètres
         * @link ImportDataController::verifyIntegrityAction
         */ {
        !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();

        $defaults = $this->container->getParameter('jsdefaults', array());
        $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));
        $l_layertype = strtolower($layertype);
        $typeCouche = $l_layertype;
        /*TODO*/
        $xField = $yField = $zField = "";
        $adrField = [];
        $couchd_nom = "Nom inconnu";

        //extract all submitted values
        $requestValues = $parametersBag->all();
        extract($requestValues);

        $fields = $parametersBag->get('layerfields', array());
        if (empty($fields)) $fields = array("drops" => array(), "types" => array(), "xField" => null, "yField" => null, "adrField" => null);
        if (!isset($fields["drops"])) $fields["drops"] = array();
        $strDropFields = implode(",", $fields["drops"]);
        $tabTypeFields = array();
        foreach ($fields["types"] as $field) {
            $tabTypeFields[($field["field_name"])] = $field["field_datatype"];
        }

        $import_action = $parametersBag->get('import_action', 'create');
        $encoding_source = $parametersBag->get('encoding_source', 'auto');
        $projection_source = $parametersBag->get('projection_source', 'auto');
        $projection_cible = $parametersBag->get('projection_cible', $defaults["PROJECTION"]);
        if ($projection_source == "auto") {
            if ($layertype == "MNT") {
                $projection_source = $projection_cible;
            } else {
                $projection_source = -1;
            }
        }

        $couchd_emplacement_stockage = $parametersBag->get('couchd_emplacement_stockage', '');
        $couchd_type_stockage = intval(array_search(strtolower($layertype), self::$DATATYPE_TO_STOCKAGE));

        $file_georeferenced = $parametersBag->get('file_georeferenced', false);
        $file_source = $parametersBag->get('file_source', '');
        $file_sheet = $parametersBag->get('file_sheet', '');
        $field_key = $parametersBag->get('field_key', null);

        if ($file_georeferenced) {
            $xField = ($fields["xField"]);
            $yField = ($fields["yField"]);
            if (isset($fields["adrField"]) && $fields["adrField"] != "" && $xField == "" && $yField == "") {
                foreach ($fields["types"] as $adr) {
                    if ($adr["adrField"]) {
                        $adrField[] = $adr["field_utf8"];
                    }
                }
                $file_source_old = $file_source;
                $file_geocodage = $this->getFileGeocodage($adrField, $file_source);
                $file_source = $this->mergeCSVFileGeocodage($file_source, $file_geocodage, $adrField);

                $columnName = $this->getCSVFileColumnName($file_geocodage, $delimiter);
                foreach ($columnName as $column) {
                    if (!in_array($column, $adrField)) {
                        if ($column == "latitude" || $column == "longitude") {
                            $type = "float";
                        } else {
                            $type = "text";
                        }

                        $tabTypeFields[$column] = $type;
                        $fields["types"][] = array(
                            "field_datatype" => $type,
                            "field_index" => 0,
                            "field_name" => $column,
                            "field_utf8" => $column,
                            "field_visible" => true);
                    }
                }
                $xField = "longitude";
                $yField = "latitude";
            }
        }

        $layer_type = $parametersBag->get('layer_type', "");
        if ($this->isJson($file_source)) {
            if (!$this->isGeoJson($file_source)) {
                $csvFile = $this->checkJson($file_source);
                $file_source = $csvFile;
            }
        } else if($layer_type == "Tabulaire"){
            $file_source = $this->createTabulaireFile($file_source, $parametersBag->get('data_type', ""));
        }

        $pk_couche_donnees = $parametersBag->get('pk_couche_donnees', 0);
        $synchronisation_mapfiles = $parametersBag->get('synchronisation_mapfiles', false);
    }

        $importeur = null;

        //since schema can be posted with no value
        if ($schema == "") {
            $schema = self::PRODIGE_SCHEMA_PUBLIC;
        }
        try {

            // IMPORT IN POSTGIS
            $importeur = new ImportInPostgis($this, $schema, $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr"));

            //EXIT IF NO DATA SOURCE SUBMITTED
            if ($file_source == "") {
                $date = $CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
                if ($date) $result = "Importée le " . date("d/m/Y à H:i:s", strtotime($date));
                else $result = "Import à faire";
                $fmeta_id && $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);

                $couche_fields = $importeur->getFeatureCatalogFields($couchd_emplacement_stockage);
                $this->generateGeosourceFeatureCatalogue($fmeta_id, $couchd_nom, $couchd_emplacement_stockage, $couche_fields);
                $this->updateDomSDomMetadata($fmeta_id);
                $this->updateAtomArboMetadata($fmeta_id);
                $this->updateMetadataDateValidity($fmeta_id);
                $this->updateMetadataExtent($couchd_emplacement_stockage);
                $this->rebuildViews($couchd_emplacement_stockage);
                return array("status" => "NOTHING_TO_DO", "success" => true, "result" => array("import_status" => $result, "pk_couche_donnees" => $pk_couche_donnees));
            }


            list($unique, $table_exists, $used_in_views) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));

            $this->setImportInPostgisConfig($importeur, $pk_couche_donnees, $layertype, $couchd_emplacement_stockage, $file_source, $fmeta_id, $file_sheet, null, $encoding_source, $fields["types"]);
            //$importeur->setDataExecution($couchd_emplacement_stockage, $file_source, $fmeta_id, $file_sheet, null, "LATIN1");
            if (!$table_exists && $import_action != "create")
                $import_action = "create";

            $bIsOk = true;
            // IMPORT IN DATABASE
            switch ($import_action) {
                case "create":

                    if ($bIsOk) {
                        if ($xField == "" && $yField == "" && strtolower($importeur->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_CSV)) {
                            $result = $importeur->CreatingDataFromTable($strDropFields, $typeCouche, $tabTypeFields);
                        } else {
                            $result = $importeur->CreatingData($projection_source, $projection_cible, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields);
                        }
                    }

                    //TODO : $res = json_decode(file_get_contents("http://".$PRO_SITE_URL."/PRRA/Services/setMetadataVisualisable.php?mode=".$bViusalisable."&metadata_id=".$metadata_id));


                    break;
                case "update":
                    if (empty($field_key)) {
                        throw new \Exception("Erreur: Aucune clé d'intégration n'est définie");
                    } else {
                        $result = array();
                        $result[] = $importeur->ArchivingData();
                        if ($bIsOk) {
                            if ($xField == "" && $yField == "" && strtolower($importeur->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_CSV)) {
                                $result[] = $importeur->UpdatingDataFromTable($field_key, $strDropFields, $tabTypeFields);
                            } else {
                                $result[] = $importeur->UpdatingData($field_key, $projection_source, $projection_cible, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields);
                            }
                        }
                    }
                    break;
                case "replace":
                    $result = array();
                    // $result[] = $importeur->IntegritingData($xField, $yField, $zField, $strDropFields, $tabTypeFields);
                    $result[] = $importeur->ArchivingData();
                    if ($bIsOk) {
                        if ($xField == "" && $yField == "" && strtolower($importeur->GetTypeFichier()) == strtolower(ImportInPostgis::$EXT_CSV)) {
                            $result[] = $importeur->ReplacingDataFromTable($strDropFields, $typeCouche, $tabTypeFields);
                        } else {
                            $result[] = $importeur->ReplacingData($projection_source, $projection_cible, $xField, $yField, $zField, $strDropFields, $typeCouche, $tabTypeFields);
                        }
                    }
                    break;
            } // FIN IMPORT IN DATABASE

            //if ( $this->getEnv()!="dev" ){
            if (strpos(realpath(dirname($file_source)) . "/", realpath(rtrim($this->getMapfileDirectory(), "/") . "/temp") . "/") !== false) {
                $importeur->DeletingFileTempo($file_source, strtolower($layertype));
            }
            if ($file_georeferenced && $adrField != "") {
                if (strpos(realpath(dirname($file_source)) . "/", realpath(rtrim($this->getMapfileDirectory(), "/") . "/temp") . "/") !== false) {
                    $importeur->DeletingFileTempo($file_source, strtolower($layertype));
                }
                if (strpos(realpath(dirname($file_geocodage)) . "/", realpath(rtrim($this->getMapfileDirectory(), "/") . "/temp") . "/") !== false) {
                    $importeur->DeletingFileTempo($file_geocodage, strtolower($layertype));
                }
            }
            //}


            $PRODIGE->executeQuery("set search_path to public");
            $tab = explode(".", $couchd_emplacement_stockage);
            $PRODIGE->executeQuery("select populate_geometry_columns((:schemaname||'.'||:tablename)::regclass)", array("tablename" => end($tab), "schemaname" => $schema));
            $PRODIGE->isTransactionActive() && $PRODIGE->commit();

            //On met à jour l'information couchd_visualisable apres import de donnees (si 3D => 0, sinon =>1)
            if ($pk_couche_donnees) {
                if ($layertype == "MNT") {
                    $couchd_visualisable = "0";
                } else {
                    $couchd_visualisable = "1";
                }
                $CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));

                $synchronisation_mapfiles && $synchronisation_mapfiles = $this->curlToCatalogue("/geosource/dellayersfrommap/" . $couchd_type_stockage . "/" . $couchd_emplacement_stockage);


                if (!is_bool($synchronisation_mapfiles)) {
                    $synchronisation_mapfiles = json_decode($synchronisation_mapfiles, true);
                    if ($synchronisation_mapfiles) {
                        if ($synchronisation_mapfiles["success"]) {
                            $result[] = $synchronisation_mapfiles["action"];
                            //TODO ????
                            $CATALOGUE->executeQuery("update couche_donnees set couchd_wms=0, couchd_wfs=0 where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
                        }
                    }
                }
                $fmeta_id && $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);

                $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
                $closeTransaction && $PRODIGE->isTransactionActive() && $PRODIGE->commit();

                $couche_fields = $importeur->getFeatureCatalogFields();
                $this->generateGeosourceFeatureCatalogue($fmeta_id, $couchd_nom, $couchd_emplacement_stockage, $couche_fields);
                $this->updateDomSDomMetadata($fmeta_id);
                $this->updateAtomArboMetadata($fmeta_id);
                $this->updateMetadataDateValidity($fmeta_id);
                $this->updateMetadataExtent($couchd_emplacement_stockage);
                $this->rebuildViews($couchd_emplacement_stockage);
            } else {
                $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
            }
        } catch (\Exception $exception) {
            $PRODIGE->isTransactionActive() && $PRODIGE->rollBack();
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            
            $message = $this->formatStringError($exception->getMessage ());
            //$message = "Une erreur s'est produite".(php_sapi_name()=="cli" ? "<br/>".$message."<br/>(".basename($exception->getFile())."[".$exception->getLine()."])" : "");
            
            throw new ApiException ( $message, array(
                "data" => explode("\n", str_replace( array("<br>", "<br/>"), array("\n", "\n"), $this->formatStringError($exception->getMessage()))),
                "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
                "stack" => explode("#", $exception->getTraceAsString()),
                "status" => "FAILURE",
                "details" => "<div class='error_details'>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*[:.]\s*!i", "", $message) . "</div>",
                "result" => array("import_status" => $message),
                "success" => false
            ), $exception->getCode(), $exception);
        }

        $import_status = $result;
        //$import_status = (is_array($result) ? array_map("utf8_encode", $result) : $result);
        $import_status = array_merge((array)$import_status, array("Importée le " . date("d/m/Y à H:i:s", strtotime($CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"))))));
        $import_status = array_diff($import_status, array(null, ""));
        $import_status = implode("<br/>", $import_status);
        $import_action = ($import_action == "create" ? "update" : $import_action);
        $unique_fields = $this->getPGTableUniqueFields($importeur);
        list($couchd_emplacement_stockage, $couchd_wms, $couchd_wfs) = $CATALOGUE->fetchArray("select couchd_emplacement_stockage, couchd_wms, couchd_wfs from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
        $file_source = "";
        $file_name = "";
        $layerfields = array();
        return array("status" => "SUCCESS", "success" => true, "result" => compact(
            "import_status",
            "pk_couche_donnees",
            "import_action",
            "used_in_views",
            "unique_fields",
            "couchd_emplacement_stockage",
            "couchd_wms",
            "couchd_wfs",
            "file_source",
            "file_name",
            "layerfields"
        ));
    }


    /**
     * Exectute the import of the data for an insertion into Postgis Database
     * @param Request $request
     * @param unknown $fmeta_id
     * @param string $layertype
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doImportRaster(ParameterBag $parametersBag, $fmeta_id, $layertype, $closeTransaction = true)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $dataDirectory = rtrim($this->getMapfileDirectory(), "/") . "/";

        $result = null;


        $delete_files = array();

        //extract all submitted values
        $requestValues = $parametersBag->all();
        extract($requestValues);

        $import_action = $parametersBag->get('import_action', 'create');


        $file_source = $parametersBag->get('file_source', '');


        $temp_source = $file_source;
        $file_source = preg_replace("!^(/?)temp/!", "$1raster/", $file_source);
        @mkdir($dataDirectory . dirname($file_source), 0770, true);
        if ($temp_source != $file_source) {
            $extension = pathinfo($dataDirectory . $temp_source, PATHINFO_EXTENSION);
            $bCopy = true;
            if (strtolower($extension) == "shp") {
                $extensions = array("shp", "dbf", "shx", "prj", 'qpj');
            } else {
                $extensions = array_merge(array($extension), self::$ADDITIONAL_RASTER_EXT);
            }
            $_temp_source = preg_replace("!" . $extension . "$!", "", $temp_source);
            $_file_source = preg_replace("!" . $extension . "$!", "", $file_source);
            $extensions = array_merge($extensions, array_map("strtolower", $extensions), array_map("strtoupper", $extensions));
            $extensions = array_unique($extensions);
            foreach ($extensions as $extension) {
                if (!file_exists($dataDirectory . $_temp_source . $extension)) continue;
                $bCopy = $bCopy && @copy($dataDirectory . $_temp_source . $extension, $dataDirectory . $_file_source . $extension);
                $delete_files[] = $dataDirectory . $_temp_source . $extension;
            }

            if (!$bCopy) {
                $file_source = $temp_source;
                $delete_files = array();
            }
        }

        $couchd_emplacement_stockage = preg_replace("!^Publication/!", "", $file_source, 1);
        $file_source = preg_replace("!^Publication/!", "", $file_source, 1);
        $couchd_type_stockage = array_search(strtolower($layertype), self::$DATATYPE_TO_STOCKAGE);
        $synchronisation_mapfiles = $parametersBag->get('synchronisation_mapfiles', false);


        try {
            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();


            $pk_couche_donnees = $parametersBag->get('pk_couche_donnees', 0);
            $old_emplacement = $CATALOGUE->fetchColumn("select couchd_emplacement_stockage from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));

            $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_description", "couchd_type_stockage", "couchd_emplacement_stockage", "couchd_wms", "couchd_wfs");

            if (!$pk_couche_donnees) {
                // ajout d'une nouvelle couche
                $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                $couchd_id = (string)$pk_couche_donnees;

                $couchd_visualisable = 1;
                $coucheFields[] = "couchd_visualisable";

                //force value to 1
                $couchd_fk_acces_server = 1;
                $coucheFields[] = "couchd_fk_acces_server";
                $CATALOGUE->executeQuery("insert into couche_donnees (" . implode(", ", $coucheFields) . ") values (:" . implode(", :", $coucheFields) . ")", compact($coucheFields));
                $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));
            } else {
                // maj d'une couche existante
                $couchd_id = (string)$pk_couche_donnees;

                //force value to 1
                $couchd_fk_acces_server = 1;
                $coucheFields[] = "couchd_fk_acces_server";
                $set = array_combine($coucheFields, $coucheFields);
                $set = str_replace("=", "=:", http_build_query($set, null, ", "));
                $CATALOGUE->executeQuery("update couche_donnees set " . $set . " where pk_couche_donnees=:pk_couche_donnees", compact($coucheFields));
            }

            if ($file_source == "" || !file_exists($dataDirectory . $file_source)) {
                $date = $CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees and couchd_visualisable=1", compact("pk_couche_donnees"));
                if ($date) $result = "Importée le " . date("d/m/Y à H:i:s", strtotime($date));
                else $result = "Import à faire";
                $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);
                $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
                return array("status" => "NOTHING_TO_DO", "success" => true, "f" => $file_source, "result" => array("import_status" => $result, "pk_couche_donnees" => $pk_couche_donnees, "import_action" => "update"));
            }

            ImportRaster::initCommands($this->container->getParameter("CMD_OGR2OGR", "ogr2ogr"));

            $rasterInfo = ImportRaster::rasterToVRT($pk_couche_donnees, $dataDirectory, $dataDirectory . $file_source, (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->getParameter('PRODIGE_DEFAULT_EPSG', '2154')));
            $rasterFields = array_keys($rasterInfo);
            if ($CATALOGUE->fetchColumn("select true from raster_info where id=:pk_couche_donnees", compact("pk_couche_donnees")) !== true) {
                $CATALOGUE->executeQuery("insert into raster_info (" . implode(", ", $rasterFields) . ") values (:" . implode(", :", $rasterFields) . ")", $rasterInfo);
            } else {
                $set = array_combine($rasterFields, $rasterFields);
                $set = str_replace("=", "=:", http_build_query($set, null, ", "));
                $CATALOGUE->executeQuery("update raster_info set " . $set . " where id=:id", $rasterInfo);
            }

            if ($old_emplacement && $old_emplacement != $couchd_emplacement_stockage) {
                $layerChangeNameService = new LayerChangeName($CATALOGUE);
                $layerChangeNameService->ScanDirectory($dataDirectory, $old_emplacement, $couchd_emplacement_stockage); //Publication
                $layerChangeNameService->ScanDirectory($dataDirectory . "/layers", $old_emplacement, $couchd_emplacement_stockage); //Publication/layers 
            }

            //On met à jour l'information couchd_visualisable apres import de donnees (si 3D => 0, sinon =>1)
            $couchd_visualisable = 1;
            $CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));

            $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, $closeTransaction);

            $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();

            $this->updateDomSDomMetadata($fmeta_id);
            $this->updateMetadataDateValidity($fmeta_id);
            $this->updateMetadataExtent($couchd_emplacement_stockage);
            $this->rebuildViews($couchd_emplacement_stockage);

            $synchronisation_mapfiles && $this->curlToCatalogue("/geosource/dellayersfrommap/" . $couchd_type_stockage . "/" . $couchd_emplacement_stockage);

            $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
        } catch (\Exception $exception) {
            $closeTransaction && $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            //throw $exception;
            $message = $this->formatStringError($exception->getMessage());
            $message = "Une erreur s'est produite." . (php_sapi_name() == "cli" ? "<br/>" . $message . "<br/>(" . basename($exception->getFile()) . "[" . $exception->getLine() . "])" : "");

            throw new ApiException("Une erreur s'est produite", array(
                "data" => explode("\n", str_replace(array("<br>", "<br/>"), array("\n", "\n"), $this->formatStringError($exception->getMessage()))),
                "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
                "stack" => explode("#", $exception->getTraceAsString()),
                "status" => "FAILURE",
                "details" => "<div class='error_details'>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*[:.]\s*!i", "", $message) . "</div>",
                "result" => array("import_status" => $message),
                "success" => false
            ));
        }

        @array_map("unlink", $delete_files);

        //On met à jour l'information couchd_visualisable apres import de donnees (si 3D => 0, sinon =>1)
        $CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));
        //On met à jour le status d'import
        $CATALOGUE->executeQuery("update couche_donnees set changedate=now(), couchd_visualisable=:couchd_visualisable where pk_couche_donnees=:pk_couche_donnees", compact("couchd_visualisable", "pk_couche_donnees"));

        $import_status = (is_array($result) ? array_map("utf8_encode", $result) : $result);
        $import_status = array_merge((array)$import_status, array("Importée le " . date("d/m/Y à H:i:s", strtotime($CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"))))));
        $import_status = array_diff($import_status, array(null, ""));
        $import_status = implode("<br/>", $import_status);

        $import_action = "update";
        return array("status" => "SUCCESS", "success" => true, "result" => compact("import_status", "pk_couche_donnees", "import_action", "file_source"));
    }

    /**
     * Checks if the table name (couchd_emplacement_stockage) is unique and exists
     * @param string $couchd_emplacement_stockage
     * @param integer $pk_couche_donnees
     * @return array(unique, table_exists)
     */
    public function isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees = -1)
    {
        $connection = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $sql = " select 1 " .
            " from couche_donnees cd " .
            " where cd.couchd_emplacement_stockage=:couchd_emplacement_stockage" .
            " and cd.pk_couche_donnees <> :pk_couche_donnees";
        $exists = $connection->executeQuery($sql, compact("couchd_emplacement_stockage", "pk_couche_donnees"));

        $connection = $this->getProdigeConnection();

        $unique = ($exists->rowCount() == 0);
        $table_exists = true;
        $used_in_views = false;
        $table_schema = self::PRODIGE_SCHEMA_PUBLIC;
        if ($unique) {

            $sql = " select 1 " .
                " from information_schema.tables  " .
                " where table_name=:couchd_emplacement_stockage" .
                " and table_schema = :table_schema";
            $exists = $connection->executeQuery($sql, compact("couchd_emplacement_stockage", "table_schema"));
            $table_exists = ($exists->rowCount() > 0);
        }
        if ($table_exists) {
            $sql = " select 1 " .
                " from parametrage.prodige_view_info " .
                " where layer_name=:couchd_emplacement_stockage limit 1";
            $exists = $connection->executeQuery($sql, compact("couchd_emplacement_stockage", "table_schema"));
            $used_in_views = ($exists->rowCount() > 0);
        }

        return compact("unique", "table_exists", "used_in_views");
    }

    /**
     * Get all (all depth) GIS files extracted from the ZIP file according to layertype (VECTOR|RASTER|TABULAIRE) and only if all required files by extension are present
     * @param string $filezip The full path to the ZIP file
     * @param string $layertype VECTOR|RASTER|TABULAIRE
     * @return array
     * @throws \Exception
     */
    protected function analyseZipFile($filezip, $layertype)
    {
        $tableMaxLength = 60;
        $DATATYPE_TO_STOCKAGE = array_change_key_case(array_flip(self::$DATATYPE_TO_STOCKAGE), CASE_UPPER);
        $DATATYPE_TO_STOCKAGE["MULTI"] = $DATATYPE_TO_STOCKAGE["VECTOR"];
        if (!file_exists($filezip)) {
            throw new \Exception("L'archive " . basename($filezip) . " n'existe pas");
        }
        $extractTo = str_replace("." . pathinfo($filezip, PATHINFO_EXTENSION), "", $filezip);

        $zip = new \ZipArchive();
        $res = $zip->open($filezip);
        if ($res === TRUE) {
            $bOk = $zip->extractTo($extractTo);
            $zip->close();
            if (!$bOk) throw new \Exception("Impossible d'extraire l'archive " . basename($filezip));
        } else {
            $oClass = new \ReflectionClass("\\ZipArchive");
            $cst = $oClass->getConstants();
            throw new \Exception("Impossible d'ouvrir l'archive " . ($filezip) . " [" . $res . "={" . print_r($cst, 1) . "}]");
        }
        $insert_files = array();

        $ext_georeferenced = array();
        switch ($layertype) {
            case "VECTOR":
                $ext_georeferenced = self::$DATATYPE_EXTENSION["TABULAIRE"];
            case "MULTI":
                $ext_vectoriel = array_keys(self::$DATATYPE_EXTENSION["VECTOR"]);
                //recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
                $cmd = "find " . $extractTo . " -iname '*." . implode("' -or -iname '*.", array_merge($ext_vectoriel, $ext_georeferenced)) . "' ";
                $files = array();
                $process = Process::fromShellCommandline($cmd);
                $process->setTimeout(10800);
                $process->run();
                $files = explode("\n", $process->getOutput());
                // avant passage des appels de commande par Process

                foreach ($files as $file) {
                    if (!file_exists($file)) {
                        continue;
                    }

                    $georeferenced = false;
                    $pathinfo = pathinfo($file);
                    $extension = strtolower($pathinfo["extension"]);
                    $no_extension_file = str_ireplace("." . $extension, "", $file);
                    $inserted_file = array(
                        "couchd_nom" => $pathinfo["filename"],
                        "file" => $file,
                        "stockage" => strtolower(substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength)),
                        "type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
                        "extension" => $pathinfo["extension"],
                        "no_extension_file" => $no_extension_file,
                        "required_extension" => array(),
                        "georeferenced" => false,
                    );
                    if (in_array($extension, $ext_vectoriel)) {
                        // fichier vectoriel => des fichiers associés sont obligatoires
                        $inserted_file["required_extension"] = array_merge(self::$REQUIRED_EXTENSION["VECTOR"][$extension], array($extension));
                    } else {
                        // fichier georeferencé
                        $inserted_file["georeferenced"] = true;
                    }
                    $insert_files[$pathinfo["filename"]] = $inserted_file;
                }

                break;
            case "MNT":
                $ext_georeferenced = self::$DATATYPE_EXTENSION["MNT"];
                //recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
                $cmd = "find " . $extractTo . " -iname '*." . implode("' -or -iname '*.", $ext_georeferenced) . "' ";
                $files = array();
                $process = Process::fromShellCommandline($cmd);
                $process->setTimeout(10800);
                $process->run();
                $files = explode(" ", $process->getOutput());
                // avant passage des appels de commande par Process
                
                foreach ($files as $file) {
                    $georeferenced = false;
                    $pathinfo = pathinfo($file);
                    $extension = strtolower($pathinfo["extension"]);
                    $no_extension_file = str_ireplace("." . $extension, "", $file);
                    $insert_files[$pathinfo["filename"]] = array(
                        "couchd_nom" => $pathinfo["filename"],
                        "file" => $file,
                        "stockage" => strtolower(substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength)),
                        "type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
                        "extension" => $pathinfo["extension"],
                        "no_extension_file" => $no_extension_file,
                        "required_extension" => array(),
                        "georeferenced" => true,
                    );
                }
                break;
            case "TABULAIRE":
                $ext_georeferenced = self::$DATATYPE_EXTENSION["TABULAIRE"];
                //recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
                $cmd = "find " . $extractTo . " -iname '*." . implode("' -or -iname '*.", $ext_georeferenced) . "' ";
                $files = array();
                $process = Process::fromShellCommandline($cmd);
                $process->setTimeout(10800);
                $process->run();
                $files = explode(" ", $process->getOutput());
                // avant passage des appels de commande par Process
                
                foreach ($files as $file) {
                    $georeferenced = false;
                    $pathinfo = pathinfo($file);
                    $extension = strtolower($pathinfo["extension"]);
                    $no_extension_file = str_ireplace("." . $extension, "", $file);
                    $insert_files[$pathinfo["filename"]] = array(
                        "couchd_nom" => $pathinfo["filename"],
                        "file" => $file,
                        "stockage" => strtolower(substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength)),
                        "type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
                        "extension" => $pathinfo["extension"],
                        "no_extension_file" => $no_extension_file,
                        "required_extension" => array(),
                        "georeferenced" => false,
                    );
                }
                break;
            case "RASTER":
                //TODO
                $ext_raster = self::$DATATYPE_EXTENSION["RASTER"]["RASTER"];
                $ext_tileindex = array_keys(self::$DATATYPE_EXTENSION["RASTER"]["TILEINDEX"]);
                //recherche dans le répertoire de tous les fichiers (en profondeur) ayant une des extensions 
                $cmd = "find " . $extractTo . " -iname '*." . implode("' -or -iname '*.", array_merge($ext_raster, $ext_tileindex)) . "' ";
                $process = Process::fromShellCommandline($cmd);
                $process->setTimeout(10800);
                $process->run();
                $files = explode(" ", $process->getOutput());
                // avant passage des appels de commande par Process
                
                foreach ($files as $file) {
                    $georeferenced = false;
                    $pathinfo = pathinfo($file);
                    $extension = strtolower($pathinfo["extension"]);
                    $no_extension_file = str_ireplace("." . $extension, "", $file);
                    $inserted_file = array(
                        "couchd_nom" => $pathinfo["filename"],
                        "file" => $file,
                        "stockage" => $pathinfo["filename"],
                        "type_stockage" => $DATATYPE_TO_STOCKAGE[$layertype],
                        "extension" => $pathinfo["extension"],
                        "no_extension_file" => $no_extension_file,
                        "required_extension" => array(),
                        "georeferenced" => false,
                    );
                    if (in_array($extension, $ext_tileindex)) {
                        // fichier vectoriel => des fichiers associés sont obligatoires
                        // fichier georeferencé
                        $inserted_file["georeferenced"] = true;
                        $inserted_file["stockage"] = substr(preg_replace("!\W!", "_", $pathinfo["filename"]), 0, $tableMaxLength);
                        $inserted_file["required_extension"] = array_merge(self::$REQUIRED_EXTENSION["RASTER"]["TILEINDEX"][$extension], array($extension));
                    }

                    $insert_files[$pathinfo["filename"]] = $inserted_file;
                }
                break;
        }
        ksort($insert_files);
        $tmp = array();
        foreach ($insert_files as $inserted_file) {
            $no_extension_file = $inserted_file["no_extension_file"];
            $required_extension = $inserted_file["required_extension"];
            if (!empty($required_extension)) {
                $searchAll = glob($no_extension_file . ".*");
                $required_exists = true;
                $searchAll = array_map("strtolower", $searchAll);
                foreach ($required_extension as $extension) {
                    $required_exists = $required_exists && in_array(strtolower($no_extension_file . "." . $extension), $searchAll);
                }
                if (!$required_exists) continue; //pas d'ajout du fichier en base s'il manque un des fichiers requis
            }
            $tmp[] = $inserted_file;
        }
        $insert_files = $tmp;
        return $insert_files;
    }


    /**
     * generate the Raster Tileindex for all available files in directory
     * @param $extension string
     * @param $shapefile string
     * @param $directory string
     */
    public function generateRasterTileindexAction($extension, $shapefile, $directory)
    {
        $defaults = $this->container->getParameter('jsdefaults', array());
        $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));
        $extensionInit = $extension;
        $rootDir = realpath($this->getMapfileDirectory()) . "/";
        $extractTo = rtrim(realpath($rootDir . $directory), "/") . "/";
        $toDirectory = preg_replace("!" . $rootDir . "temp/!", $rootDir . "raster/", $extractTo);
        @mkdir($toDirectory, 0770, true);


        if (!file_exists($extractTo) || !is_dir($extractTo))
            return $this->formatJsonError(404, "Le répertoire {
        $directory} n'existe pas.");

        $selectedFiles = array();
        $selectedExtension = null;
        if ($extension == "_first_") {
            $extensionInit = "";
            $extensions = self::$DATATYPE_EXTENSION["RASTER"]["RASTER"];
            $extensions = array_merge(array_map("strtoupper", $extensions), array_map("strtolower", $extensions));
            foreach ($extensions as $extension) {
                $selectedFiles = glob($extractTo . "*." . $extension);
                if (!empty($selectedFiles)) {
                    $selectedExtension = $extension;
                    break;
                }
            }
        } else {
            $selectedFiles = glob($extractTo . "*." . $extension);
            if (!empty($selectedFiles)) {
                $selectedExtension = $extension;
            }
        }

        if (is_null($selectedExtension))
            return $this->formatJsonError(404, "Aucun fichier Raster " . ($extensionInit ? "d'extension " . $extensionInit : "") . " dans le répertoire {
        $directory}");

        $files = array_combine($selectedFiles, $selectedFiles);

        if ($extractTo != $toDirectory) {
            $files = array();
            foreach ($selectedFiles as $file) {
                $newfile = str_replace($extractTo, $toDirectory, dirname($file) . "/") . preg_replace("![^\w.]!", '_', basename($file));
                if (rename($file, $newfile))
                    $files[$file] = $newfile;
            }
        }
        $rm = "rm -rf " . $toDirectory . $shapefile . ".sh* " . $toDirectory . $shapefile . ".prj " . $toDirectory . $shapefile . ".dbf " . $toDirectory . $shapefile . ".qpj ";

        $shapefilePath = $toDirectory . $shapefile . ".shp";

        $logFile = dirname($shapefilePath) . "/gdaltindex.log";

        $cmd = $rm . " ; gdaltindex -t_srs EPSG:" . $defaults["PROJECTION"] . " " . $shapefilePath . " " . implode(" ", array_map('escapeshellarg', $files)) . " 2> " . $logFile;
        $messages = array();
        $process = Process::fromShellCommandline($cmd);
        $process->setTimeout(10800);
        $process->run();
        $messages = explode(" ", $process->getOutput());
        $return = $process->getErrorOutput();
        // avant passage des appels de commande par Process
        //$last = exec($cmd, $messages, $return);


        $success = $selectedFiles;
        $failures = array();
        $logs = trim(file_get_contents($logFile));
        if ($logs == "") {
            @unlink($logFile);
            $logFile = null;
        } else {
            foreach ($files as $oldfile => $newfile) {
                if (strpos($logs, $newfile) !== false) {
                    $failures[] = $oldfile;
                    unset($success[array_search($oldfile, $success)]);
                }
            }
        }
        if ($return) { //there is an error
            foreach ($files as $old => $new) {
                if (file_exists($new) && !file_exists($old)) copy($old, $new);
            }
            return $this->formatJsonError(404, "La génération du fichier d'index n'a pas réussi : <br/>[{
        $cmd}]" . implode("<br/>", $messages));
        }

        $shapefile = str_replace($rootDir, "", $shapefilePath);
        $success = str_replace($rootDir, "/Publication/", $success);
        $failures = str_replace($rootDir, "/Publication/", $failures);
        $logs = str_replace($rootDir, "/Publication/", $logs);
        return $this->formatJsonSuccess(array("cmd" => $cmd, "logs" => nl2br($logs), "file_source" => $shapefile, "success" => $success, "failures" => $failures, "rastersused" => $files));
    }

    /**
     *
     */
    public function importStandardInProdige($dbanmeSource, $schemaSource, $dbanmeDestination = null, $schemaDestination = null)
    {
        // On récupère les param-tre de connection des bases tampons
        $TAMPON_AUTH = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD = $this->container->getParameter("tampon_password");

        // On export la structrue 
        $cmd = 'PGPASSWORD=' . $TAMPON_AUTH_PWD . ' pg_dump -s -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $TAMPON_AUTH . ' -x -O -n ' . $schemaSource . ' -T spatial_ref_sys ' . $dbanmeSource . ' '
            . '| sed \'/CREATE SCHEMA ' . $schemaSource . '/d\' ' /* supprimer create schema si exist */
            . '| sed \'1 i\CREATE SCHEMA ' . $schemaDestination . ';\' '  /* ajouter create schema avec le nouveau nom (schema destination) */
            . '| sed \'s/^SET search_path.*$/SET search_path to ' . $schemaDestination . ', public ;/\' '
            . '| sed \'s/' . $schemaSource . '\./' . $schemaDestination . '\./\' '
            . '| PGPASSWORD=' . $this->container->getParameter('catalogue_password') . ' psql -h ' . $this->container->getParameter('prodige_host') . ' -U ' . $this->container->getParameter('prodige_user') . ' -d ' . $dbanmeDestination;

        $process = Process::fromShellCommandline($cmd);
        $process->setTimeout(10800);
        $process->run();

        return empty($process->getErrorOutput()) ? array('status' => 200, 'description' => "") : array('status' => 500, 'description' => $this->formatStringError($process->getErrorOutput()));
    }

    /**
     *
     */
    public function chekcIfSchemaExist($conn, $schema)
    {
        $sql = "SELECT schema_name from information_schema.schemata where schema_name = :schema";

        $sth = $conn->prepare($sql);
        $sth->execute(array(':schema' => $schema));

        $data = $sth->fetchAll();

        return !empty($data);
    }

    /**
     *
     * Récupère la liste des tables d'une base en fonction de son schéma
     *
     * @return Array
     */
    function getTablesFromDatabase($conn, $schema = 'public')
    {
        $sql = "SELECT tablename as table_name, schemaname as table_schema from pg_tables 
                WHERE schemaname = :schema and tableowner <> 'postgres'";

        $sth = $conn->prepare($sql);
        $sth->execute(array(':schema' => $schema));

        return $sth->fetchAll();
    }

    /**
     * Ajoutes les sufixe et préfixe aux tables d'un schéma
     *
     * @return Boolean True si tout c'est bien passé
     */
    function addPrefixAndSuffix($conn, $tables, $schema, $prefix = "", $suffix = "")
    {
        $sql = "ALTER TABLE  RENAME TO new_name";
        $isOk = false;
        $inTransct = !$conn->isTransactionActive();
        try {
            $inTransct && $conn->beginTransaction();

            foreach ($tables as $table) {
                $sql = "ALTER TABLE " . $table['table_schema'] . '.' . $table['table_name'] . " "
                    . "RENAME TO " . $prefix . $table['table_name'] . $suffix;

                $conn->prepare($sql)->execute();
            }

            $inTransct && $conn->commit();

            $isOk = true;
        } catch (Exception $exception) {
            !$inTransct && $conn->rollBack();
        }

        return $isOk;
    }

    /**
     *
     */
    function addAndLinkCoucheDonnee($conn, $dbname, $prefix, $emplacementStockage, $suffix, $metadataId, $schema)
    {
        $isOk = false;
        $pk_couche_donnees = $conn->fetchColumn("select nextval('catalogue.seq_couche_donnees'::regclass)", array(), 0);
        $couchdId = "" . $pk_couche_donnees;

        $tamponConnection = $this->getDBTamponConnection($dbname);
        $couchd_type_stockage = $this->getCouchdTypeStockage($tamponConnection, $dbname, $emplacementStockage);
        $couchd_fk_acces_server = 1;
        $emplacementStockage = $prefix . $emplacementStockage . $suffix;
        //Paramètre de couche données
        $coucheFields = array(
            "pk_couche_donnees" => $pk_couche_donnees,
            "couchd_id" => $couchdId,
            "couchd_nom" => $emplacementStockage,
            "couchd_description" => "",
            "couchd_type_stockage" => $couchd_type_stockage,
            "couchd_emplacement_stockage" => $schema . "." . $emplacementStockage,
            "couchd_visualisable" => 1,
            "couchd_wms" => 0,
            "couchd_wfs" => 0,
            "couchd_fk_acces_server" => 1,
        );

        // Requête pour l'ajout de couche données
        $sqlAddCoucheDonne = "insert into catalogue.couche_donnees (" . implode(", ", array_keys($coucheFields)) . ") values (:" . implode(", :", array_keys($coucheFields)) . ")";
        $paramsAddCoucheDonne = $coucheFields;

        // Requête pour lier la metadata aux couche données 
        $sqlAddFicheMeta = "insert into catalogue.fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees, schema) values (:metadataId, :coucheId, :schema)";
        $paramAddFicheMeta = array('metadataId' => $metadataId, 'coucheId' => $couchdId, 'schema' => $schema);

        // Insertion avec transaction
        $inTransct = !$conn->isTransactionActive();
        try {
            $inTransct && $conn->beginTransaction();

            $conn->executeQuery($sqlAddCoucheDonne, $paramsAddCoucheDonne);
            $conn->executeQuery($sqlAddFicheMeta, $paramAddFicheMeta);

            //reprise de $this->attachDomaineSousDomaine($conn, $metadataId, $pk_couche_donnees);
            $params = array(
                "pk_couche_donnees" => $pk_couche_donnees,
                "fmeta_id" => $metadataId
            );

            $tabSql = array(
                "DELETE from catalogue.ssdom_dispose_couche where ssdcouch_fk_couche_donnees=:pk_couche_donnees",
                "INSERT into catalogue.ssdom_dispose_couche (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine)
                 SELECT donnees.pk_couche_donnees, ssdcouch_fk_sous_domaine
                 FROM catalogue.ssdom_dispose_metadata dommeta 
                 INNER join public.metadata meta using (uuid) 
                 INNER join catalogue.fiche_metadonnees fmeta on (fmeta.fmeta_id=meta.id::text) 
                 INNER join catalogue.couche_donnees donnees on (donnees.pk_couche_donnees = fmeta.fmeta_fk_couche_donnees) 
                 WHERE fmeta.fmeta_id=:fmeta_id 
                 AND donnees.pk_couche_donnees=:pk_couche_donnees"
            );

            foreach ($tabSql as $query) {
                $conn->executeQuery($query, $params);
            }

            $inTransct && $conn->commit();

            $isOk = true;
        } catch (Exception $exception) {
            $inTransct && $conn->rollBack();
        }

        return $isOk;
    }

    /**
     * Change la projection pour toutes les collones d'un schéma
     *
     * @param connProdige connection PDO à la base prodige
     *
     * @return Boolean
     */
    function setSchemaProjection($connProdige, $schema, $projection)
    {
        $conn = $connProdige;
        $isOk = false;

        $sql = "SELECT UpdateGeometrySRID(c.table_schema, c.table_name, c.column_name,  $projection)
                from pg_catalog.pg_statio_all_tables as st
                inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
                right outer join information_schema.columns c on (pgd.objsubid=c.ordinal_position and  c.table_schema=st.schemaname and c.table_name = st.relname)
                inner join pg_tables pgt on ( pgt.tablename = c.table_name and pgt.schemaname = c.table_schema )
                where table_schema = :schema and c.udt_name = 'geometry' and pgt.tableowner <> 'postgres'";

        $inTransct = !$conn->isTransactionActive();
        try {
            $inTransct && $conn->beginTransaction();

            $sth = $conn->prepare($sql);
            $sth->execute(array('schema' => $schema));
            $test = $sth->fetchAll();
            $inTransct && $conn->commit();

            $isOk = true;
        } catch (Exception $exception) {
            $inTransct && $conn->rollBack();
        }

        return $isOk;
    }

    protected function getFileGeocodage($adrField, $fileName)
    {
        //$urlApiBan = $this->container->getParameter('URL_API_BAN')."search/csv/";
        $urlApiBan = "https://api-adresse.data.gouv.fr/" . "search/csv/";
        $geocodingData = $this->getMapfileDirectory() . "temp/";
        $inputFile = $fileName;

        if (file_exists($inputFile)) {
            $path_details = pathinfo($inputFile);
            //$outputFileName = str_replace("export", "import", $path_details['filename']);
            $outputFileName = "import_" . $path_details['filename'];
            $outputFile = $geocodingData . $outputFileName . "." . $path_details['extension'];

            //$this->logToFile("Appeler la fonction du filtrage 'filterCSVFile'. ");
            $this->filterCSVFile($inputFile, $outputFile, $adrField);

            if (file_exists($outputFile)) {
                //$this->logToFile("Constitution du fichier à géocoder");
                //This needs to be the full path to the file you want to send.
                $file_name_with_full_path = realpath($outputFile);
                //cURL headers for file uploading
                $headers = array("Content-Type:multipart/form-data");
                // data to post
                $postfields = array(
                    "data" => new \CURLFile($file_name_with_full_path)//,
                    //"citycode" => "code_insee_commune"
                );

                $curl = curl_init();

                // cURL options
                $options = array(
                    CURLOPT_URL => $urlApiBan,
                    CURLOPT_POST => true,
                    CURLOPT_FOLLOWLOCATION=> true,
                    CURLOPT_HTTPHEADER => $headers,
                    CURLOPT_POSTFIELDS => $postfields,
                    CURLOPT_HEADER => false, //Not return header
                    CURLOPT_SSL_VERIFYPEER => false, //Don't veryify server certificate
                    CURLOPT_RETURNTRANSFER => true
                );

                curl_setopt_array($curl, $options);
                //$this->logToFile("Ouverture de la connexion CURL.");
                $geocoding_data = curl_exec($curl);

                curl_close($curl);

                $output = fopen($outputFile, 'w+');
                if (!$output) {
                    //$this->logToFile("Echec dans l'ouverture du fichier de sortie.");
                    return null;
                } else {
                    //$this->logToFile("Ouverture d'un fichier de sortie, en mode écriture pour enregistrer les information de géocodage.");
                    fwrite($output, $geocoding_data);

                    //$this->logToFile("Fermeture du fichier de sortie.");
                    fclose($output);
                    //$this->logToFile("Récupération du géocodage réussie : ".$outputFile."[".filesize($outputFile)."]");
                    return $outputFile;
                }
            }
        } else {
            //$this->logToFile("Le fichier à géocoder n'existe pas");
            return null;
        }
    }

    private function logToFile($msgToLog, $logFile)
    {
        $time = @date('[d/M/Y:H:i:s]');
        $fd = fopen($logFile, "a+");
        if (!$fd) {
            fwrite($fd, $time . " : Echec dans l'ouverture du fichier de log : " . $logFile . "\n");
        } else {
            $msgToWrite = $time . " : " . $msgToLog;
            fwrite($fd, $msgToWrite . "\n");
            fclose($fd);
        }
    }

    private function filterCSVFile($inputFile, $filteredFile, $ar_filter_column)
    {

        $delimiter = $this->detectDelimiter($inputFile);

        $temp = array();
        foreach ($ar_filter_column as $column_name) {
            $temp[] = $this->returnIndexColumnName($inputFile, $delimiter, $column_name);
        }
        $ar_filter_column = $temp;

        $fileToRead = fopen($inputFile, 'r');
        $fileToWrite = fopen($filteredFile, 'w');

        if ($fileToRead && $fileToWrite) {
            while (($row = fgets($fileToRead)) !== FALSE) {
                $data = str_getcsv($row, $delimiter);
                $outputData = array();
                for ($i = 0; $i < count($ar_filter_column); $i++) {
                    $outputData[] = $data[$ar_filter_column[$i]];
                }
                fputcsv($fileToWrite, $outputData, $delimiter);
            }
        }

        fclose($fileToRead);
        fclose($fileToWrite);
    }

    protected function mergeCSVFileGeocodage($inputFile, $fileGeocodage, $ar_filter_column)
    {
        $mergeFile = str_replace("import_", "import_geocodage_", $fileGeocodage);
        $delimiter = $this->detectDelimiter($inputFile);

        $temp = array();
        foreach ($ar_filter_column as $column_name) {
            $temp[] = $this->returnIndexColumnName($inputFile, $delimiter, $column_name);
        }
        $ar_filter_column = $temp;

        $headerInputFile = $this->getCSVFileColumnName($inputFile);
        $nbColumnInputFile = count($headerInputFile);
        $headerFileGeocodage = $this->getCSVFileColumnName($fileGeocodage);
        $nbColumnFileGeocodage = count($headerFileGeocodage);

        $fileToRead1 = fopen($inputFile, 'r');
        $fileToRead2 = fopen($fileGeocodage, 'r');
        $fileToWrite = fopen($mergeFile, 'w');

        if ($fileToRead1 && $fileToRead2 && $fileToWrite) {
            while (($row1 = fgets($fileToRead1)) !== FALSE && ($row2 = fgets($fileToRead2)) !== FALSE) {
                $data = str_getcsv(preg_replace("/\r|\n/", "", $row1 . $delimiter . $row2), $delimiter);
                $outputData = array();
                for ($i = 0; $i < $nbColumnInputFile; $i++) {
                    $outputData[] = $data[$i];
                }
                for ($i = count($ar_filter_column); $i < $nbColumnFileGeocodage; $i++) {
                    $outputData[] = $data[$i + $nbColumnInputFile];
                }
                fputcsv($fileToWrite, $outputData);
            }
        }

        fclose($fileToRead1);
        fclose($fileToRead2);
        fclose($fileToWrite);
        return $mergeFile;
    }

    /**
     * Detect delimiter in csv file
     * @param $csvFile
     * @return $Delimiter
     */
    private function detectDelimiter($csvFile)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );
        $handle = fopen($csvFile, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }
        return array_search(max($delimiters), $delimiters);
    }

    /**
     * Return index for column name
     * @param : $csvFile
     * @param : $delimiter
     * @param : $columnName
     * */
    private function returnIndexColumnName($csvFile, $delimiter, $columnName)
    {
        $index = -1;

        if (file_exists($csvFile)) {
            $handle = fopen($csvFile, "r");
            $firstLine = fgets($handle);
            fclose($handle);

            $header = str_getcsv($firstLine, $delimiter);

            for ($i = 0; $i < count($header); $i++) {
                if ($header[$i] == $columnName) {
                    $index = $i;
                    break;
                }
            }
        }
        return $index;
    }

    private function getCSVFileColumnName($csvFile)
    {
        if (file_exists($csvFile)) {
            $delimiter = $this->detectDelimiter($csvFile);
            $handle = fopen($csvFile, "r");
            $firstLine = fgets($handle);
            fclose($handle);

            $header = str_getcsv($firstLine, $delimiter);
            return $header;
        }
    }

    //Vérifie si le fichier json passé en paramètre (via chemin ou url) est bien formé pour l'import et retourne le nom du fichier csv créé contenant les données du fichier json
    private function checkJson($file)
    {

        if (file_exists($file)) {
            $fileJson = fopen($file, "r");
            if (!$fileJson) {
                throw new \Exception("Impossible d'ouvrir le fichier " . $file);
            }
            $strFileJson = "";
            while (($ligne = fgets($fileJson)) !== FALSE) {
                $strFileJson .= $ligne;
            }
            fclose($fileJson);
            $csvFile = $this->getMapfileDirectory() . "temp/" . pathinfo($file, PATHINFO_FILENAME) . ".csv";
        } else {
            $strFileJson = file_get_contents(urlencode($file));
            if ($strFileJson === FALSE) {
                throw new \Exception("Le fichier " . $file . " n'existe pas");
            }
            $csvFile = $this->getMapfileDirectory() . "temp/fileJsonUrl.csv";

        }

        $tabJson = json_decode($strFileJson, true);
        if ($tabJson == null) {
            throw new \Exception("Le fichier ne contient pas du json valide");
        }

        $header = array_keys($tabJson[0]);
        foreach ($tabJson as $row) {
            foreach ($row as $field) {
                if (is_array($field)) {
                    throw new \Exception("Le fichier json est récursif");
                }
            }
            $headerRow = array_keys($row);
            if (array_diff($header, $headerRow) != array() || array_diff($headerRow, $header) != array()) {
                throw new \Exception("Les entités n'ont pas toutes les mêmes attributs");
            }
        }

        $csvFile = $this->getMapfileDirectory() . "temp/" . pathinfo($file, PATHINFO_FILENAME) . ".csv";
        $fileToWrite = fopen($csvFile, "w");
        if (!$fileToWrite) {
            throw new \Exception("Impossible de créer le fichier " . $csvFile);
        }

        $data = str_getcsv(implode(",", $header), ",");
        fputcsv($fileToWrite, $data);
        foreach ($tabJson as $row) {
            fputcsv($fileToWrite, $row);
        }
        fclose($fileToWrite);
        return $csvFile;
    }

    public function isJson($file)
    {
        if (file_exists($file)) {
            $path_parts = pathinfo($file);
            if ($path_parts['extension'] !== 'json' && $path_parts['extension'] !== 'geojson') {
                return false;
            }
            $handleFileJson = fopen($file, "r");
            if (!$handleFileJson) {
                throw new \Exception("Impossible d'ouvrir le fichier " . $file);
            }
            $fileJson = "";
            while (($ligne = fgets($handleFileJson)) !== FALSE) {
                $fileJson .= $ligne;
            }
            fclose($handleFileJson);
        } else {
            $curl = curl_init($file);
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false,
                CURLOPT_FOLLOWLOCATION=> true,
                CURLOPT_CONNECTTIMEOUT => 30,
                CURLOPT_TIMEOUT => 30,
            ));
            $fileJson = curl_exec($curl);
        }
        $fileJson = trim($fileJson);
        $fileJson = json_decode($fileJson, true);
        if ($fileJson == null) {
            return false;
        }
        return true;
    }

    public function isGeoJson($file)
    {
        $process_args = array("ogrinfo", "-q", $file);
        $process = new Process($process_args);
        $process->run();

        $search = "Unable to open";
        $test = trim($process->getOutput());
        if (strpos($test, $search)) {
            return false;
        }
        return true;
    }

    public function createTabulaireFile($urlFile, $data_type){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlFile);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        $result = curl_exec($ch);
        curl_close($ch);

        switch($data_type){
            case "CSV":
                $extension = ".csv";
                break;
            case "ODS":
                $extension = ".ods";
                break;
            case "XLS":
                $extension = ".xls";
                break;
                
        }

        $csvFile = $this->getMapfileDirectory() . "temp/synchroTabulaire" . date("Ymd-His") . $extension;
        $fp = fopen($csvFile, 'w');

        if (!$fp) {
            throw new \Exception("Impossible de créer le fichier " . $csvFile);
        }

        if($result == ""){
            throw new \Exception("Impossible de récupérer les données depuis " . $urlFile);
        }

        fwrite($fp, $result);
        fclose($fp);

        return $csvFile;
    }
}
