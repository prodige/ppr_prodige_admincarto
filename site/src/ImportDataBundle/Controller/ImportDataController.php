<?php

namespace Carmen\ImportDataBundle\Controller;

use Carmen\ApiBundle\Exception\ApiException;
use Carmen\ImportDataBundle\Services\ImportInPostgis;
use Carmen\ImportDataBundle\Services\LayerChangeName;
use JMS\Serializer\SerializationContext;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Controller\FeatureCatalogueController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @author alkante
 * @Route("/prodige/importdata")
 */
class ImportDataController extends AbstractImportDataController
{
    protected $debug = false;

    /**
     * @isGranted("ROLE_USER")
     * @Route("/manage/{uuid}/{_format}", name="carmen_importdata", defaults={"_format"="html"}, requirements={"uuid"="[-_%.a-zA-Z0-9]+", "_format"="html|json"}, options={"expose"=true})
     */
    public function indexAction($uuid, $_format)
    {
        // vérification de sécurité
        if (!$this->debug) \Carmen\ApiBundle\Controller\ProdigeController::checkSecurity($this->container, $uuid);

        $session = $this->getRequest()->getSession();

        if ($this->getRequest()->query->get('from', false) !== 'prodige') {
            $session->clear();
        }

        if ($uuid !== null) {
            $dataDirectory = rtrim($this->getMapfileDirectory(), "/") . "/";
            $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
            $PDO = $CATALOGUE->getWrappedConnection();

            $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]";
            $metadata_desc = ", array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_desc";
            $metadata_title = ", array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml, {$namespaces}), ' ') as metadata_title";
            $metadata_type = ", array_to_string(xpath('//gmd:hierarchyLevel/gmd:MD_ScopeCode/@codeListValue'::text, data::xml, {$namespaces}), ' ') as metadata_type";
            $stmt = $PDO->prepare("select xml_is_well_formed(data::text) from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata where uuid=:uuid");
            $stmt->execute(array("uuid" => $uuid));
            if (!$stmt->fetchColumn()) {
                //if ( !$CATALOGUE->fetchColumn("select xml_is_well_formed(data::text) from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata where uuid=:uuid", compact("uuid")) ){
                $metadata_title = ", '<i>Lecture du nom impossible</i>' as metadata_title";
                $metadata_desc = ", '' as metadata_desc";
                $metadata_type = ", 'dataset' as metadata_type";
            }

            $sql = " select m.id, m.uuid" .
                $metadata_title .
                $metadata_desc .
                $metadata_type .
                " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
                " where m.uuid= :uuid";
            //$stmt = $CATALOGUE->executeQuery($sql, array("uuid"=>$uuid, "undefined_metadata_title"=>"<i>Lecture du nom impossible</i>"));
            $stmt = $PDO->prepare($sql);
            $stmt->execute(array("uuid" => $uuid/*, "undefined_metadata_title"=>"<i>Lecture du nom impossible</i>"*/));
            $metadata = array();
            $row = $stmt->fetch(\PDO::FETCH_ASSOC);
            $metadata = $row;
            $metadata["couches"] = array();
            if ($row["metadata_type"] == "series") {
                $metadata["metadata_datatype"] = "multi";
            } else {
                $sql = " select cd.couchd_visualisable, cd.couchd_type_stockage, cd.couchd_emplacement_stockage, cd.pk_couche_donnees , fm.schema " .
                    " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
                    " inner join fiche_metadonnees fm on (m.id=fm.fmeta_id::bigint) " .
                    " inner join couche_donnees cd on (cd.pk_couche_donnees=fm.fmeta_fk_couche_donnees) " .
                    " where m.uuid= :uuid limit 1";
                list($couchd_visualisable, $couchd_type_stockage, $couchd_emplacement_stockage, $pk_couche_donnees, $schema) = $CATALOGUE->fetchArray($sql, array("uuid" => $uuid));

                if (!is_null($schema) && $schema != "" && $schema != "public") {
                    $metadata["schema"] = $schema;
                    $metadata["metadata_datatype"] = "modele";
                } else
                    if ($couchd_type_stockage !== false && $couchd_type_stockage !== null) {
                        switch (self::$DATATYPE_TO_STOCKAGE[$couchd_type_stockage]) {
                            case "vector" : //vector
                                if ($couchd_visualisable) {
                                    $metadata["metadata_datatype"] = "vector";
                                } else {
                                    $metadata["metadata_datatype"] = "mnt";
                                }
                                break;
                            case "join" :
                                $metadata["metadata_datatype"] = self::$DATATYPE_TO_STOCKAGE[$couchd_type_stockage];
                                $metadata["pk_couche_donnees_join"] = $pk_couche_donnees;
                                $metadata["view_name"] = $couchd_emplacement_stockage;
                                break;
                            default:
                                $metadata["metadata_datatype"] = self::$DATATYPE_TO_STOCKAGE[$couchd_type_stockage];
                                break;
                        }
                    }
            }

            if ($_format == "html") {
                $serializer = $this->get('jms_serializer');
                $context = new SerializationContext();
                $context->setSerializeNull(true);
                $user = $serializer->serialize($this->getUser(), 'json', $context);

                $config = $this->get('carmen.config')->getWebConfig();
                $parameters = $this->container->getParameter('config', array());
                if (isset($parameters["copyright"])) {
                    $config["copyright"] = htmlentities($parameters["copyright"]);
                }

                $defaults = $this->container->getParameter('jsdefaults', array());
                $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));
                $prodigeConfig = $this->get('prodige.configreader');
                $prodigeConfig instanceof ConfigReader;

                $defaults["CATALOGUE_URL"] = $this->container->getParameter("PRODIGE_URL_CATALOGUE", "");
                $defaults["DATATYPE_TO_STOCKAGE"] = array_merge(self::$DATATYPE_TO_STOCKAGE, array_flip(self::$DATATYPE_TO_STOCKAGE));
                $defaults["AddToWebServices"] = $this->container->getParameter("PRODIGE_URL_CATALOGUE") . "/geosource/layerAddToWebService";

                return $this->render('ImportDataBundle/Default/importdata.html.twig', array(
                    'user' => $user,
                    'parameters' => $config,
                    'jsdefaults' => $defaults,
                    "uuid" => $uuid,
                    'metadata' => $metadata,
                ));
            }

            $uuids = array($uuid => $metadata);
            if ($row["metadata_type"] == "series") {
                $geonetwork = new GeonetworkInterface();
                $related = $geonetwork->getMetadataRelations($metadata["uuid"], "children", "json");
                $related = json_decode($related, true);
                $uuids = array();
                if (!isset($related)) $related = array();
                if (isset($related["relation"])) $related = $related["relation"];
                if (isset($related["children"])) $related = $related["children"];
                if (!is_numeric(key($related))) $related = array($related);

                foreach ($related as $child) {
                    $geonetInfo = $child;
                    $geonetInfo["uuid"] = $geonetInfo["id"];
                    $geonetInfo["id"] = $CATALOGUE->fetchColumn("select id from public.metadata where uuid=:uuid", array("uuid" => $geonetInfo["uuid"]));
                    $uuids[$geonetInfo["uuid"]] = array(
                        "uuid" => $geonetInfo["uuid"],
                        "id" => $geonetInfo["id"],
                        "metadata_type" => $child["type"],
                        "metadata_title" => current($child["title"]),
                        //"metadata_desc" => $child["abstract"],
                        "mtype" => "Metadata",
                    );
                }
            }
            $couches = array();
            foreach ($uuids as $a_uuid => $a_metadata) {
                $children = array();
                $sql = " select fm.*, cd.* " .
                    " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
                    " inner join fiche_metadonnees fm on (m.id=fm.fmeta_id::bigint) " .
                    " inner join couche_donnees cd on (cd.pk_couche_donnees=fm.fmeta_fk_couche_donnees) " .
                    " where m.uuid= :uuid";
                $stmt = $CATALOGUE->executeQuery($sql, array("uuid" => $a_uuid));
                foreach ($stmt as $row) {
                    $row = array_map("html_entity_decode", $row);
                    $pk_couche_donnees = $row["pk_couche_donnees"];

                    $couchd_type_stockage = self::$DATATYPE_TO_STOCKAGE[$row["couchd_type_stockage"] ?: self::DEFAULT_TYPE_STOCKAGE];

                    switch ($couchd_type_stockage) {
                        case "vector" : //vector
                            if ($row["couchd_visualisable"]) {
                                if (!isset($uuids[$a_uuid]["metadata_datatype"])) $uuids[$a_uuid]["metadata_datatype"] = $couchd_type_stockage;
                                if (!isset($metadata["metadata_datatype"])) $metadata["metadata_datatype"] = $couchd_type_stockage;
                            } else {
                                if (!isset($uuids[$a_uuid]["metadata_datatype"])) $uuids[$a_uuid]["metadata_datatype"] = "mnt";
                                if (!isset($metadata["metadata_datatype"])) $metadata["metadata_datatype"] = "mnt";
                            }
                            break;
                        default:
                            if (!isset($uuids[$a_uuid]["metadata_datatype"])) $uuids[$a_uuid]["metadata_datatype"] = $couchd_type_stockage;
                            if (!isset($metadata["metadata_datatype"])) $metadata["metadata_datatype"] = $couchd_type_stockage;
                            break;
                    }
                    $row["couchd_nom"] = html_entity_decode($row["couchd_nom"], ENT_QUOTES);
                    $row["couchd_description"] = html_entity_decode($row["couchd_description"], ENT_QUOTES);
                    $children[$pk_couche_donnees] = $row;
                    $children[$pk_couche_donnees]["uuid"] = $a_uuid;
                    $children[$pk_couche_donnees]["mtype"] = "CoucheDonnees";
                    $children[$pk_couche_donnees]["leaf"] = true;

                    if (array_key_exists($row["couchd_type_stockage"], self::$TABLE_STOCKAGES)) {
                        $PRODIGE = $this->getProdigeConnection(self::$TABLE_STOCKAGES[$row["couchd_type_stockage"]]);
                        $importeur = new ImportInPostgis($this, isset($a_metadata["schema"]) ? $a_metadata["schema"] : self::PRODIGE_SCHEMA_PUBLIC, $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr"));

                        $queryParams = array(
                            "table_schema" => (isset($a_metadata["schema"]) ? $a_metadata["schema"] : self::PRODIGE_SCHEMA_PUBLIC),
                            "table_name" => $row["couchd_emplacement_stockage"]
                        );
                        $query = "SELECT srid, type from public.geometry_columns where f_table_schema=:table_schema and f_table_name=:table_name";
                        $tabTypeGeom = $PRODIGE->executeQuery($query, $queryParams);

//                        if (count($tabTypeGeom) != 0) {
//                            foreach ($tabTypeGeom as $type) {
//                                $typeGeom = $type["type"];
//                            }
//                            $children[$pk_couche_donnees]["typeGeom"] = $typeGeom;
//                        }

                        $tabTypeGeom = $PRODIGE->executeQuery($query, $queryParams);
                        foreach ($tabTypeGeom as $type) {
                            $typeGeom = $type["type"];
                        }
                        if (isset($typeGeom)) {
                            $children[$pk_couche_donnees]["typeGeom"] = $typeGeom;
                        }

                        $importeur->setDataExecution($row["couchd_emplacement_stockage"], "", $row["fmeta_id"], "", "", "LATIN1");
                        // 	              $table_exists = $PRODIGE->executeQuery(
                        // 	              	"select 1 from information_schema.tables ".
                        // 	              	" where table_schema=:table_schema and table_name=:table_name"
                        // 	              , array(
                        // 	              	"table_schema" => self::$TABLE_STOCKAGES[$row["couchd_type_stockage"]],
                        // 	              	"table_name"   => $row["couchd_emplacement_stockage"]
                        // 	              ));

                        list($unique, $table_exists, $used_in_views) = array_values($this->isUniqueStorageAction($row["couchd_emplacement_stockage"], $pk_couche_donnees));
                        $children[$pk_couche_donnees]["used_in_views"] = $used_in_views;
                        $children[$pk_couche_donnees]["unique_fields"] = array();
                        $tab = explode(".", $row["couchd_emplacement_stockage"]);
                        $children[$pk_couche_donnees]["fieldnames"] = $PRODIGE->executeQuery(
                            "select column_name from information_schema.columns " .
                            " where table_name=:table_name and table_schema=:table_schema and column_name not in ('the_geom')",
                            array(
                                "table_name" => end($tab),
                                "table_schema" => isset($a_metadata["schema"]) ? $a_metadata["schema"] : self::$TABLE_STOCKAGES[$row["couchd_type_stockage"]],
                            ))->fetchAll(\PDO::FETCH_NUM);
                        if ($table_exists) {
                            $children[$pk_couche_donnees]["import_status"] = "Importée le " . date("d/m/Y à H:i:s", strtotime($row["changedate"]));
                            $children[$pk_couche_donnees]["import_action"] = ($used_in_views ? "replace" : "update");
                        } else {
                            $children[$pk_couche_donnees]["import_status"] = "Import à faire";
                            $children[$pk_couche_donnees]["import_action"] = "create";
                        }
                    } else if ($row["couchd_type_stockage"] == array_search("raster", self::$DATATYPE_TO_STOCKAGE)) {
                        // couches raster : vérification de l'existence du fichier

                        // transfère l'emplacement dans le champ fichier
                        $children[$pk_couche_donnees]["file_source"] = $row["couchd_emplacement_stockage"];
                        $children[$pk_couche_donnees]["file_name"] = str_replace("temp/", "", $row["couchd_emplacement_stockage"]);
                        $children[$pk_couche_donnees]["couchd_emplacement_stockage"] = $children[$pk_couche_donnees]["file_name"];

                        $vrt_path = $CATALOGUE->fetchColumn("select vrt_path from raster_info where id=:pk_couche_donnees", compact("pk_couche_donnees"));

                        if (file_exists($dataDirectory . $row["couchd_emplacement_stockage"]) && ($vrt_path && file_exists($vrt_path))) {
                            $children[$pk_couche_donnees]["import_status"] = "Importée le " . date("d/m/Y à H:i:s", strtotime($row["changedate"]));
                            $children[$pk_couche_donnees]["import_action"] = "replace";
                        } else {
                            $children[$pk_couche_donnees]["import_status"] = "Import à faire";
                            $children[$pk_couche_donnees]["import_action"] = "create";
                        }
                    }
                }
                if (empty($children)) {
                    unset($uuids[$a_uuid]);
                } else {
                    $uuids[$a_uuid]["leaf"] = false;
                    $uuids[$a_uuid]["children"] = array_values($children);
                }
            }
            $data = array();
            if (isset($uuids[$uuid]["children"])) $data = $uuids[$uuid]["children"];
            else {
                $data = array_values($uuids);
            }
            return new JsonResponse(array("children" => $data));
        } else {
            return $this->render('ImportDataBundle/Default/forbidden.html.twig');
        }
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/uniquefields/{fmeta_id}/{couchd_emplacement_stockage}", name="carmen_ws_uniquefields", options={"expose"=true},
     *     requirements={"couchd_emplacement_stockage"="[-_%.a-zA-Z0-9]+", "fmeta_id"="[-_%.a-zA-Z0-9]+"})
     */
    public function uniqueFieldsAction($fmeta_id, $couchd_emplacement_stockage)
    {
        $PRODIGE = $this->getProdigeConnection(self::$TABLE_STOCKAGES["1"]);
        $importeur = new ImportInPostgis($this, self::PRODIGE_SCHEMA_PUBLIC, $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr"));

        if (!is_numeric($fmeta_id)) {
            $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

            $sql = " select m.id " .
                " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
                " where m.uuid= :uuid";
            $fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $fmeta_id), 0);
        }
        $importeur->setDataExecution($couchd_emplacement_stockage, "", $fmeta_id, "", "", "LATIN1");
        $fields = array();
        if ($importeur->IsExistTableStockage()) {
            $fields = $this->getPGTableUniqueFields($importeur);
        }
        return new JsonResponse($fields);
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/uniquestorage/{couchd_emplacement_stockage}/{pk_couche_donnees}", name="carmen_ws_uniquestorage", options={"expose"=true},
     *      requirements={"couchd_emplacement_stockage"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
     */
    public function uniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees = -1)
    {
        $result = $this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees);
        return new JsonResponse($result);
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/gettablelist", name="carmen_ws_gettablelist", options={"expose"=true})
     */
    public function getTableListAction()
    {
        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        $query = 'SELECT distinct couchd_emplacement_stockage, couchd_nom FROM couche_donnees where couchd_type_stockage in (1, -3,-4) order by couchd_emplacement_stockage';
        $result = $CATALOGUE->fetchAll($query);
        return new JsonResponse(array("data" => $result));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/deletelayer/{fmeta_id}/{pk_couche_donnees}", name="carmen_ws_deletelayer", options={"expose"=true},
     *     requirements={"fmeta_id"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
     */
    public function deleteLayerAction(Request $request, $fmeta_id, $pk_couche_donnees)
    {
        set_time_limit(500000);
        /*
        $ScanDirectory = function ($Directory, $searchLayer, $type_stockage, $bSubDir = false ){
            $files = array();
            exec(implode( '| ', array(
                // recherche des fichiers map et comptabilisation des occurences du terme $searchLayer dans les fichiers map trouvés
                'find '.$Directory.' '.(!$bSubDir ? '-maxdepth 1' : '').' -iname "*.map" -exec grep -H -c "'.$searchLayer.'" {} \; ',
                // exclusion des fichiers map n'ayant aucun grep
                ' grep -v ":0" ', 
            )), $files);
            foreach($files as $file){
                $file = str_replace("./", rtrim($Directory, "/")."/", preg_replace("!:\d+!", "", $file));
            try{
                $oMap = @ms_newMapObj($file);
            }catch(\Exception $exception){continue;}
                $bHasOne = false;
                for($i = 0; $i < $oMap->numlayers; $i ++) {
                    $oLayer = $oMap->getLayer ( $i );
                    switch ($type_stockage) {
                        case "raster" :
                            if ($oLayer->data == ($Directory . $searchLayer)) {
                                $oLayer->set ( "status", MS_DELETE );
                                $bHasOne = true;
                            }
                            break;
                        case "vector" :
                        case "view" :
                            // echo $oLayer->data." ".$data." ".preg_match("/\b".$data."\b/i", $oLayer->data)."<br>";
                            if (preg_match ( "/\b" . $searchLayer . "\b/i", $oLayer->data )) {
                                // if (strpos($oLayer->data, $data." as foo using unique gid") !==false || (strpos($oLayer->data, $data.") as foo using unique gid") !==false ))
                                $oLayer->set ( "status", MS_DELETE );
                                $bHasOne = true;
                            }
                            break;
                    }
                }
                if ($bHasOne) {
                    $oMap->save ( $file );
                }
            }
        };*/

        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        $PRODIGE = $this->getProdigeConnection("public");
        try {


            $CATALOGUE->beginTransaction();
            $PRODIGE->beginTransaction();

            $queryParams = array(
                "fmeta_id" => $fmeta_id,
                "pk_couche_donnees" => $pk_couche_donnees,
                "fmeta_fk_couche_donnees" => $pk_couche_donnees
            );

            // test la suppression d'une fiche serie fille + parent
            // echo PRO_CATALOGUE_URLBASE."Services/updateArbo.php?uuid=".$uuid.""; die();
            // construction de l'url de suppression des données
            $query = 'SELECT couchd_type_stockage, couchd_emplacement_stockage, uuid FROM catalogue.couche_donnees cd ' .
                ' inner join fiche_metadonnees fm on (cd.pk_couche_donnees=fm.fmeta_fk_couche_donnees) ' .
                ' inner join public.metadata m on (m.id=fm.fmeta_id::bigint) ' .
                ' WHERE pk_couche_donnees = :pk_couche_donnees';
            $rs = $CATALOGUE->executeQuery($query, $queryParams);
            foreach ($rs as $row) {
                $couchd_type_stockage = $row["couchd_type_stockage"];
                $couchd_emplacement_stockage = $row["couchd_emplacement_stockage"];
                $type = (isset(self::$DATATYPE_TO_STOCKAGE[$couchd_type_stockage]) ? self::$DATATYPE_TO_STOCKAGE[$couchd_type_stockage] : null);

                if ($type == "tabulaire") { // tables
                    //TODO PRODIGE40
                    if ($mode == "majic") {
                        $deldataurl .= '/HTML_MAJIC/deleteData.php?metadata_id=' . $fmeta_id;
                    }
                } else {
                    switch ($type) {
                        case 'raster' :
                            // couche raster
                            break;
                        case 'vector' : // couche vecteur
                        case 'tabulaire' : // table
                            $rs = $PRODIGE->executeQuery('SELECT TABLENAME FROM PG_TABLES WHERE TABLENAME = :couchd_emplacement_stockage', $row);
                            if ($rs->rowCount() > 0) {
                                $query = array(
                                    'DROP TABLE "' . $couchd_emplacement_stockage . '" cascade',
                                    'DROP TABLE IF EXISTS "' . $couchd_emplacement_stockage . '_ARC"',
                                    'DROP SEQUENCE IF EXISTS "' . $couchd_emplacement_stockage . '_seq"',
                                    'DROP SEQUENCE IF EXISTS "seq_' . $couchd_emplacement_stockage . '"'
                                );
                                $this->executeQuery($PRODIGE, $query);
                            }
                            $this->deleteGeosourceFeatureCatalogue($fmeta_id, $couchd_emplacement_stockage);
                            break;
                        case 'view' : // vues	
                            $rs2 = $PRODIGE->executeQuery('select pk_view, view_name from parametrage.prodige_view_info where view_name=:view_name', array(
                                "view_name" => ViewFactory::$INITIAL_VIEW_PREFIX . '_' . $couchd_emplacement_stockage
                            ));
                            if ($rs2->rowCount() > 0) {
                                $f_table_schema = "public";
                                $row = $rs2->fetch(\PDO::FETCH_ASSOC);
                                $pk_view = $row["pk_view"];
                                $view_composite_name = $row["view_name"];
                                $strSQL = "DELETE FROM parametrage.prodige_computed_field_info WHERE pk_view=:pk_view;";
                                $strSQL .= "DELETE FROM parametrage.prodige_join_info WHERE pk_view=:pk_view;";
                                $strSQL .= "DELETE FROM parametrage.prodige_view_info WHERE pk_view=:pk_view;";
                                $strSQL .= "DELETE FROM parametrage.prodige_view_composite_info WHERE pk_view=:pk_view;";
                                if ($couchd_emplacement_stockage != "")
                                    $strSQL .= "DROP VIEW IF EXISTS \"" . $couchd_emplacement_stockage . "\";";
                                if ($view_composite_name != "")
                                    $strSQL .= "DROP VIEW IF EXISTS \"" . $view_composite_name . "\";";
                                $this->executeQuery($PRODIGE, $strSQL, compact("pk_view", "view_composite_name", "couchd_emplacement_stockage", "f_table_schema"));
                            }
                            break;
                        default :
                            continue 2;
                        break;
                    }// end switch
                }
            }
            // suppression de la métadonnée et informations liées
            try {
                if ($row["uuid"] != "") {
                    $this->curlToCatalogue("/geosource/wxs/remove_dataset_from_wfs/" . $row["uuid"] . "/" . $pk_couche_donnees);
                }

                $query = 'DELETE FROM catalogue.FICHE_METADONNEES WHERE FMETA_ID = :fmeta_id and fmeta_fk_couche_donnees=:fmeta_fk_couche_donnees';
                $CATALOGUE->executeQuery($query, $queryParams);
                $query = 'DELETE FROM catalogue.COUCHE_DONNEES WHERE PK_COUCHE_DONNEES = :pk_couche_donnees';
                $CATALOGUE->executeQuery($query, $queryParams);
                $this->detachDomaineSousDomaine($CATALOGUE, $pk_couche_donnees);
                // suppression des eventuelles fiches de métadonnées de services WFS


            } catch (\Exception $exception) {
                throw new \Exception('Erreur lors de la suppression de la fiche de couche id=' . $pk_couche_donnees, 500, $exception);
            }

            // suppression de la couche de toutes les cartes
            /*$Directory = rtrim($this->getMapfileDirectory(), "/")."/";
            $ScanDirectory($Directory, $couchd_emplacement_stockage, $type);
            // suppression des cartes personnelles
            $ScanDirectory($Directory . "carteperso/", $couchd_emplacement_stockage, $type, true);*/


            // suppression des cartes par défaut
            /*if ( file_exists($Directory . "layers/" . $couchd_emplacement_stockage . ".map" ) ){
                    unlink($Directory . "layers/" . $couchd_emplacement_stockage . ".map");
            }
            if ( file_exists($Directory . "layers/WMS/" . $couchd_emplacement_stockage . ".map" ) ){
                    unlink($Directory . "layers/WMS/" . $couchd_emplacement_stockage . ".map");
            }*/

            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
            $PRODIGE->isTransactionActive() && $PRODIGE->commit();
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            $PRODIGE->isTransactionActive() && $PRODIGE->rollBack();

            $message = $exception->getMessage();
            $message = "Erreur : <br/>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*:\s*!i", "", $message);

            return $this->formatJsonError(200, "Une erreur s'est produite", "", array(
                "data" => explode("\n", str_replace(array("<br>", "<br/>"), array("\n", "\n"), $exception->getMessage())),
                "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
                "stack" => explode("#", $exception->getTraceAsString()),
                "status" => "FAILURE",
                "message" => $message,
                "success" => false,
            ));
        }
        return new JsonResponse(array("status" => "SUCCESS", "success" => true));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/data/file/{layertype}/{couchd_type_stockage}/{pk_couche_donnees}/{layerfile}/{layersheet}", name="carmen_ws_data_file",
     *     options={"expose"=true}, defaults={"layersheet"=null}, requirements={"layerfile"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
     */
    public function dataFromFileAction(Request $request, $pk_couche_donnees, $layerfile, $couchd_type_stockage, $layersheet = null, $layertype = "VECTOR")
    {
        try {
            $directory = rtrim($this->getMapfileDirectory(), '/') . '/';
            $layerfile = urldecode($layerfile);
            if (file_exists($directory . $layerfile))
                $layerfile = $directory . $layerfile;
            if (!file_exists($layerfile))
                return new JsonResponse(array("message" => "Le fichier " . $layerfile . " n'existe pas.", "success" => false));

            $layername = (!empty($layersheet) ? $layersheet : pathinfo($layerfile, PATHINFO_FILENAME));

            $limit = $request->query->get("limit", 20);
            $offset = $request->query->get("start", 0);
            $cmd = "export OGR_XLS_HEADERS='FORCE';export OGR_ODS_HEADERS='FORCE';" . str_replace("ogr2ogr", "ogrinfo", $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr")) .
                ' -geom=NO -ro  -al -dialect "SQLite" -sql "select * from \'' . $layername . '\' limit ' . $limit . ' offset ' . $offset . '"  "' . $layerfile . '" ' . (!empty($layersheet) ? '"' . $layersheet . '"' : '');
            $result = array();
            exec($cmd . " 2> /dev/null", $result);

            $start = false;
            $data = array();
            foreach ($result as $line) {
                if (strpos($line, "OGRFeature(SELECT)") !== false) {
                    $start = true;
                    $adata = array();
                    continue;
                }
                if ($start && preg_match("!^\s*$!", $line)) {
                    $start = false;
                    $data[] = $adata;
                    continue;
                }
                if ($start) {
                    $old = (isset($parts) ? $parts : null);
                    $parts = array();
                    preg_match("!^\s+(\S+)\s+\(\w+\)\s+=\s+(.+)$!s", $line, $parts);
                    if (count($parts) != 3) {//continuité d'une valeur multiligne
                        preg_match("!^(.+)$!s", $line, $parts);
                        if (!empty($parts) && $old) {
                            $parts = array(1 => $old[1], 2 => $old[2] . "<br/>" . $parts[1]);
                        } else {
                            continue;
                        }
                    }
                    if (mb_detect_encoding($parts[1]) != "ASCII")
                        $parts[1] = utf8_encode($parts[1]);

                    if (mb_detect_encoding($parts[2]) != "ASCII")
                        $parts[2] = utf8_encode($parts[2]);

                    $adata[$parts[1]] = $parts[2];
                }
            }


            $total = 0;
            $cmd_last = "";
            $result = array();
            if (!empty($layersheet)) {
                $cmd_last = "'" . $layersheet . "'";
            }
            $process_args = array(
                "export", "OGR_XLS_HEADERS='FORCE';",
                "export", "OGR_ODS_HEADERS='FORCE';",
                str_replace("ogr2ogr", "ogrinfo", $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr")),
                "-geom=NO", "-noextent", "-al", '-so', $layerfile, $cmd_last/*, 
                "2>", "/dev/null"*/
            );
            $process = new Process($process_args);
            $process->setTimeout(null);
            $process->run();
            $result = explode(" ", $process->getOutput());

            // avant passage des appels par Symfony Process
            //$cmd = "export OGR_XLS_HEADERS='FORCE';export OGR_ODS_HEADERS='FORCE';".str_replace("ogr2ogr", "ogrinfo", $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr")).' -geom=NO -noextent -al -so "'.$layerfile.'" ' .(!empty($layersheet) ? '"'.$layersheet.'"' : '');
            //$result = array();
            //exec($cmd." 2> /dev/null", $result);
            foreach ($result as $line) {
                if (strpos($line, "Feature Count:") !== false) {
                    $total = preg_replace("!\D!", "", $line);
                    break;
                }
            }

            $total = max((int)$total, count($data));
            return new JsonResponse(array("data" => $data, "total" => $total, "success" => true));
        } catch (ApiException $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("data" => $exception->getData(), "stack" => $exception->getTraceAsString()));
        } catch (\Exception $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("stack" => $exception->getTraceAsString()));
        }

        return new Response("");
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/data/database/{couchd_emplacement_stockage}", name="carmen_ws_data_database", options={"expose"=true},
     *     requirements={"couchd_emplacement_stockage"="[-_%.a-zA-Z0-9]+"})
     */
    public function dataFromBaseAction(Request $request, $couchd_emplacement_stockage)
    {
        try {
            $limit = $request->query->get("limit", 20);
            $offset = $request->query->get("start", 0);

            $PRODIGE = $this->getProdigeConnection("public");

            $rs = $PRODIGE->executeQuery("select * from " . $couchd_emplacement_stockage . " limit :limit offset :offset", compact("limit", "offset"));

            $fields = array();
            $data = array();
            foreach ($rs as $row) {
                unset($row["gid"]);
                unset($row["the_geom"]);
                $fields = array_keys($row);
                $data[] = $row;
            }

            $total = $PRODIGE->fetchColumn("select count(*) from " . $couchd_emplacement_stockage);

            $columns = array();
            foreach ($fields as $field) {
                $columns[] = array("header" => $field, "dataIndex" => $field);
            }
            $metaData = array(
                "fields" => $fields,
                "columns" => $columns,
            );
            return new JsonResponse(array("metaData" => $metaData, "data" => $data, "total" => $total, "success" => true));
        } catch (ApiException $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("data" => $exception->getData(), "stack" => $exception->getTraceAsString()));
        } catch (\Exception $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("stack" => $exception->getTraceAsString()));
        }

        return new Response("");
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/checkSchemaName/{uuid}", name="carmen_ws_checkSchemaName", options={"expose"=true}, defaults={})
     */
    public function checkSchemaNameAction(Request $request, $uuid)
    {
        $return = array('valid' => true);

        $schema = $request->query->get('q', '');

        if (!empty($schema)) {

            preg_match('/^[a-z][a-z0-9_]*$/', $schema, $matches);

            $schemas = $this->getSchemasFromDB($this->getProdigeConnection());
            if (!count($matches) || in_array(strtolower(trim($schema)), array_map(function ($schema) {
                    return strtolower(trim($schema));
                }, $schemas))) {
                $return['valid'] = false;
            }
        }

        return new JsonResponse($return);
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/checkDbExist/{uuid}", name="carmen_ws_checkDbExist", options={"expose"=true}, defaults={})
     */
    public function checkcheckDbExistAction(Request $request, $uuid)
    {
        $return = array('exist' => true);

        $dbname = self::TAMPON_PREFIX_DB_NAME . $uuid;

        if (!$this->isDbExist($dbname)) {
            $return['exist'] = false;
        }

        return new JsonResponse($return);
    }


    /**
     * @isGranted("ROLE_USER")
     * @Route("/importsqlfile/{uuid}/{sqlfile}/{mode}", name="carmen_ws_importsqlfile", options={"expose"=true}, defaults={})
     */
    public function importSqlFileAction(Request $request, $uuid, $sqlfile, $mode)
    {
        $directory = $this->getMapfileDirectory() . '/';
        $sqlfile = urldecode($sqlfile);
        $sqlfile = preg_replace("!^Publication/!", "", $sqlfile);
        if (file_exists($directory . $sqlfile)) {
            $sqlfile = str_replace(rtrim(realpath($directory), "/") . "/", "", realpath($directory . $sqlfile));
        }

        try {
            $schema = '';
            $tables = array();

            if ($mode == self::ACTION_ADD) {
                $dbname = self::TAMPON_PREFIX_DB_NAME . $uuid;
                $connection = $this->getDBTamponConnection($dbname);
                if (!$connection) {
                    return $this->formatJsonError(500, "Internal Server Error ", "$dbname n'existe pas ! ", ['extra' => ['tables' => $tables]]);
                }
                $schema = $this->getTamponSchema($connection);
            }

            $schemas = $this->getSchemasFromFile($directory . $sqlfile);

            if (count($schemas) == 0) {
                return $this->formatJsonError(500, "Internal Server Error", "Le fichier dump ne contient pas de schéma spécifique (CREATE SCHEMA NOT FOUND)", ['extra' => ['tables' => $tables]]);
            }

            if (count($schemas) > 1) {
                return $this->formatJsonError(500, "Internal Server Error", "Un seul schéma est autorisé seulement !", ['extra' => ['tables' => $tables]]);
            }

            if (!empty($schema) && count($schemas) == 1 && $schema != $schemas[0]) {
                return $this->formatJsonError(500, "Internal Server Error", "Le nom du schéma du fichier dump importé " . $schemas[0] . " est différent !", ['extra' => ['schema' => $schema, 'tables' => $tables]]);
            }

            $schema = (count($schemas) ? $schemas[0] : '');
            $tables = $this->getTablesFromFile($directory . $sqlfile);

            return $this->formatJsonSuccess(['extra' => ['schema' => $schema, 'tables' => $tables]]);
        } catch (\Exception $exception) {
            return $this->formatJsonError(500, "Internal Server Error ", $exception->getMessage());
        }
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/readfile/{layertype}/{couchd_type_stockage}/{pk_couche_donnees}/{layerfile}/{layersheet}", name="carmen_ws_readfile",
     *     options={"expose"=true}, defaults={"layersheet"=null}, requirements={"layerfile"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
     */
    public function readFileAction(Request $request, $pk_couche_donnees, $layerfile, $couchd_type_stockage, $layersheet = null, $layertype = "VECTOR")
    {
        set_time_limit(0);
        $callback = $request->get("callback", "");
        try {
            $result = $this->getGISFileProperties($pk_couche_donnees, $layerfile, $couchd_type_stockage, $layersheet, $layertype);
            $this->getLogger()->debug(__METHOD__, (array)$result);

            if (is_array($result) && isset($result["erreurJson"])) {
                return $this->formatJsonErrorCallback($callback, Response::HTTP_INTERNAL_SERVER_ERROR, $result["erreurJson"], $result["erreurJson"], array("data" => "", "stack" => ""));
            }

            if (!is_bool($result)) {
                return $this->formatJsonSuccessCallback($callback, $result);
            }
        } catch (ApiException $exception) {
            return $this->formatJsonErrorCallback($callback, Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("data" => $exception->getData(), "stack" => $exception->getTraceAsString()));
        } catch (\Exception $exception) {
            return $this->formatJsonErrorCallback($callback, Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("stack" => $exception->getTraceAsString()));
        }

        return new Response(($callback ? $callback . "(null)" : ""));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/changetablename/{pk_couche_donnees}/{old_tablename}/{new_tablename}", name="carmen_ws_changetablename", options={"expose"=true},
     *      requirements={"pk_couche_donnees"="[-_%.a-zA-Z0-9]+", "old_tablename"="[-_%.a-zA-Z0-9]+", "new_tablename"="[-_%.a-zA-Z0-9]+"})
     */
    public function changeTableNameAction($pk_couche_donnees, $old_tablename, $new_tablename)
    {
        // vérification de sécurité
//         \Carmen\ApiBundle\Controller\ProdigeController::checkSecurity($this->container, $uuid);

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);

        $table_schema = self::PRODIGE_SCHEMA_PUBLIC;

        $couchd_emplacement_stockage = $new_tablename;
        $sqlparameters = compact("pk_couche_donnees", "old_tablename", "new_tablename", "table_schema", "couchd_emplacement_stockage");

        $dataDirectory = rtrim($this->getMapfileDirectory(), "/") . "/";
        try {
            $exists = $PRODIGE->fetchColumn("SELECT 1 FROM information_schema.tables where table_schema=:table_schema and table_name=:old_tablename", $sqlparameters);
            if (!$exists) {
                throw new \Exception("La table à renommer " . $old_tablename . " n'existe pas");
            }

            list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
            if (!$unique) {
                throw new \Exception("La table " . $new_tablename . " est déjà utilisée par une autre donnée.");
            }
            if ($table_exists) {
                throw new \Exception("La table " . $new_tablename . " existe déjà");
            }
            //TODO à checker
            $layerChangeNameService = new LayerChangeName($PRODIGE);
            //renommage des données en base
            $layerChangeNameService->changeLayerNameInPostgis($old_tablename, $new_tablename);
            //renommage du layer dans les données mapfiles
            $directories = array($dataDirectory, $dataDirectory . "/layers", $dataDirectory . "/layers/WMS");
            foreach ($directories as $directory) {
                $layerChangeNameService->ScanDirectory($directory, $old_tablename, $new_tablename); //Publication
            }
            //renommage des fichiers mapfiles associés
            //TODO renommage base de données
            $directories = array($dataDirectory . "/layers", $dataDirectory . "/layers/WMS");
            foreach ($directories as $directory) {
                if (file_exists($directory . $old_tablename . ".map"))
                    rename($directory . $old_tablename . ".map", $directory . $new_tablename . ".map");
            }

            $CATALOGUE->executeQuery("update couche_donnees set couchd_emplacement_stockage = :couchd_emplacement_stockage where pk_couche_donnees = :pk_couche_donnees", $sqlparameters);

        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $message = "Erreur : <br/>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*:\s*!i", "", $message);

            return $this->formatJsonError(200, "Une erreur s'est produite", "", array(
                "data" => explode("\n", str_replace(array("<br>", "<br/>"), array("\n", "\n"), $exception->getMessage())),
                "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
                "stack" => explode("#", $exception->getTraceAsString()),
                "status" => "FAILURE",
                "message" => $message,
                "success" => false,
                "tablename" => $old_tablename
            ));
        }


        return new JsonResponse(array("status" => "SUCCESS", "success" => true, "result" => "", "tablename" => $new_tablename));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/verifyintegrity/{uuid}/{layertype}", name="carmen_ws_verifyintegrity", options={"expose"=true},
     *     requirements={"layertype"="[-_%.a-zA-Z0-9]+", "uuid"="[-_%.a-zA-Z0-9]+"})
     */
    public function verifyIntegrityAction(Request $request, $uuid, $layertype = "VECTOR")
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $sql = " select m.id " .
            " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
            " where m.uuid= :uuid";
        $fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $uuid), 0);

        $parametersBag = $request->request;
        /**
         * extraction des paramètres
         * @link AbstractImportDataController::doImportInPostgis
         */
        {
            $defaults = $this->container->getParameter('jsdefaults', array());
            $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));
            $l_layertype = strtolower($layertype);
            $typeCouche = $l_layertype;
            /*TODO*/
            $xField = $yField = $zField = $adrField = "";

            //extract all submitted values
            $requestValues = $parametersBag->all();
            extract($requestValues);

            $fields = $parametersBag->get('layerfields', array());
            if (empty($fields)) $fields = array("drops" => array(), "types" => array(), "xField" => null, "yField" => null, "adrField" => null);
            if (!isset($fields["drops"])) $fields["drops"] = array();
            $strDropFields = implode(",", $fields["drops"]);
            $tabTypeFields = array();
            foreach ($fields["types"] as $field) {
                $tabTypeFields[($field["field_name"])] = $field["field_datatype"];
            }

            $import_action = $parametersBag->get('import_action', 'create');
            $encoding_source = $parametersBag->get('encoding_source', 'auto');
            $projection_source = $parametersBag->get('projection_source', 'auto');
            if ($projection_source == "auto") $projection_source = $defaults["PROJECTION"];
            $projection_cible = $parametersBag->get('projection_cible', $defaults["PROJECTION"]);

            $couchd_emplacement_stockage = $parametersBag->get('couchd_emplacement_stockage', '');
            $couchd_type_stockage = array_search(strtolower($layertype), self::$DATATYPE_TO_STOCKAGE);

            $file_georeferenced = $parametersBag->get('file_georeferenced', false);
            $file_source = $parametersBag->get('file_source', '');
            $file_sheet = $parametersBag->get('file_sheet', '');
            $field_key = $parametersBag->get('field_key', null);

            if ($file_georeferenced) {
                $xField = ($fields["xField"]);
                $yField = ($fields["yField"]);
                $adrField = ($fields["adrField"]);
            }

            $pk_couche_donnees = $parametersBag->get('pk_couche_donnees', 0);
        }


        try {
            $importeur = new ImportInPostgis($this, self::PRODIGE_SCHEMA_PUBLIC, $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr"));
            $this->setImportInPostgisConfig($importeur, $pk_couche_donnees, $layertype, $couchd_emplacement_stockage, $file_source, $fmeta_id, $file_sheet, null, $encoding_source, $fields["types"]);
            //$importeur->setDataExecution($couchd_emplacement_stockage, $file_source, $fmeta_id, $file_sheet, null, "LATIN1");
            if (!$importeur->IsExistTableStockage()) {
                if ($import_action == "update") $import_action = "create";
            }

            $result = array();
            $result["message"] = $importeur->IntegritingData($xField, $yField, $zField, $strDropFields, $tabTypeFields, $adrField);
            $result["success"] = true;
        } catch (\Exception $exception) {
            $PRODIGE->isTransactionActive() && $PRODIGE->rollBack();
            $result["message"] = $exception->getMessage();
            $result["success"] = false;
        }
        return new JsonResponse($result);
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/directimport/{uuid}/{layertype}/{schemaDestination}/{mode}/{action}", name="carmen_ws_directimport", options={"expose"=true},
     *     requirements={"layertype"="[-_%.a-zA-Z0-9]+", "uuid"="[-_%.a-zA-Z0-9]+"})
     */
    public function directImportAction(Request $request, $uuid, $layertype = "VECTOR", $schemaDestination = null, $mode = self::ACTION_CREATE, $action = null)
    {
        set_time_limit(0);
        // vérification de sécurité
        if (!$this->debug) \Carmen\ApiBundle\Controller\ProdigeController::checkSecurity($this->container, $uuid);

        $sql = " select m.id " .
            " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
            " where m.uuid= :uuid";
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $uuid), 0);

        // contrôle avant import
        /*$table_name =$request->request->get('couchd_emplacement_stockage'); 
        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        $query = 'SELECT couchd_emplacement_stockage, couchd_nom FROM couche_donnees where couchd_emplacement_stockage =:couchd_nom order by couchd_emplacement_stockage ';
        $result = $CATALOGUE->fetchAll($query, array("couchd_nom"=>$table_name));
        // fin contrôle
        if(empty($result)) {*/

        switch ($layertype) {
            case "VECTOR" :
            case "TABULAIRE" :
            case "MNT" :
                return $this->ImportInPostgis($request, $fmeta_id, $layertype);
            case "RASTER" :
                return $this->ImportRaster($request, $fmeta_id, $layertype);
            case "MODELE" :
                return $this->ImportDumpInPostgis($request, $uuid, $fmeta_id, $layertype, $schemaDestination, $mode, $action);
        }
        /*} else {
            // la table cible existe déjà, on sort avec affichage d'un message d'erreur
            // construction de responseText 
            $responseText = array(); 
            $responseText["status"] = "FAILURE"; 
            $responseText["success"] = false; 
            $responseText["result"] = array(); 
            $responseText["result"]["import_status"] = "Erreur lors de la création de la table $table_name. Cette table existe déjà"; 
            $responseText["result"]["import_result"] = "Fail, table name already exists"; 

            return new Response(json_encode($responseText)); 
        }*/


        return new Response("");
    }


    /**
     * Exectute the import of the data for an insertion into Postgis Database
     * @param Request $request
     * @param unknown $fmeta_id
     * @param string $layertype
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function ImportInPostgis(Request $request, $fmeta_id, $layertype = "VECTOR")
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $couchd_emplacement_stockage = $request->request->get('couchd_emplacement_stockage', "");
        extract($request->request->all());
        $couchd_emplacement_stockage = strtolower($couchd_emplacement_stockage);
        $request->request->set('couchd_emplacement_stockage', $couchd_emplacement_stockage);

        $pk_couche_donnees = $request->request->get('pk_couche_donnees', 0);
        $file_source = urldecode($request->request->get('file_source', ''));
        try {
            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();

            $couchd_id = (string)$pk_couche_donnees;
            $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_description", "couchd_type_stockage", "couchd_emplacement_stockage", "couchd_wms", "couchd_wfs", "couchd_cgu_display", "couchd_cgu_message");
            if (!$pk_couche_donnees) {
                // ajout d'une nouvelle couche
                $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                $couchd_id = (string)$pk_couche_donnees;
                $couchd_visualisable = $layertype == "MNT" ? 0 : 1;
                $coucheFields[] = "couchd_visualisable";
                //force value to 1
                $couchd_fk_acces_server = 1;
                $coucheFields[] = "couchd_fk_acces_server";
                $CATALOGUE->executeQuery("insert into couche_donnees (" . implode(", ", $coucheFields) . ") values (:" . implode(", :", $coucheFields) . ")", compact($coucheFields));
                $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));
            } else {
                //si pas de réimport de donnée on ne change pas l'emplacement de stockage
                if ($file_source == "") unset($coucheFields["couchd_emplacement_stockage"]);
                // maj d'une couche existante
                $set = array_combine($coucheFields, $coucheFields);

                $set = str_replace("=", "=:", http_build_query($set, null, ", "));
                $CATALOGUE->executeQuery("update couche_donnees set " . $set . " where pk_couche_donnees=:pk_couche_donnees", compact($coucheFields));
            }
            $file_source = ($file_source != "" ? rtrim($this->getMapfileDirectory(), "/") . "/" . $file_source : "");
            $request->request->set('pk_couche_donnees', $pk_couche_donnees);
            $request->request->set('file_source', $file_source);

            $result = $this->doImportInPostgis($request->request, $fmeta_id, $layertype);

            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
        } catch (ApiException $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            //throw $exception;
            return $this->formatJsonError(200, $exception->getMessage(), "", $exception->getData());
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            //throw $exception;
            return $this->formatJsonError(200, $exception->getMessage(), "");
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            return $this->formatJsonError(200, $exception->getMessage(), "");
        }

        return new JsonResponse($result);
    }


    /**
     * Exectute the import of the data for an insertion into Postgis Database
     * @param Request $request
     * @param unknown $fmeta_id
     * @param string $layertype
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function ImportRaster(Request $request, $fmeta_id, $layertype = "RASTER")
    {
        try {
            $result = $this->doImportRaster($request->request, $fmeta_id, $layertype);
        } catch (ApiException $exception) {
            return $this->formatJsonError(200, $exception->getMessage(), "", $exception->getData());
        }
        return new JsonResponse($result);
    }

    /**
     * importe les données d'un fichier dump dans une base de donnée POSTGIS (PRODIGE - CATALOGUE)
     *
     * @param Request $request
     * @param  $uuid
     * @param  $fmeta_id
     * @param  $layertype
     * @param  $schemaDestination
     * @param  $mode
     * @param  $action
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function ImportDumpInPostgis(Request $request, $uuid, $fmeta_id, $layertype, $schemaDestination, $mode, $action)
    {
        $result = $this->doImportDumpInPostgis($request->request, $uuid, $fmeta_id, $layertype, $schemaDestination, $mode, $action);
        if ($result['status'] == 500) {
            return $this->formatJsonError($result['status'], "Internal Server Error ", $result['description'], ['extra' => $result['extra']]);
        }

        return $this->formatJsonSuccess(['extra' => $result['extra']]);

    }

    /**
     * import différé (layertype = MODELE)
     *
     * @param Request $request
     * @param  $uuid
     * @param  $layertype
     * @param  $schemaDestination
     *
     * @return JsonResponse
     */
    protected function deferImportDump(Request $request, $uuid, $layertype, $schemaDestination, $mode, $action)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $file_source = $request->request->get('file_source', '');

        $import_extra_params['data'] = $request->request->get('data', array());

        try {

            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();

            $import_extra_params['mode'] = $mode;
            $import_extra_params['action'] = $action;

            $flux = array(
                "uuid" => $uuid,
                "user_id" => User::GetUserId(),
                "import_type" => "import_differe",
                "import_table" => $schemaDestination,
                "import_data_type" => $layertype,
                "import_data_source" => $file_source,
                "pk_couche_donnees" => NULL,
                "import_extra_params" => json_encode($import_extra_params),
            );

            $pk_tache_import_donnees = $CATALOGUE->fetchColumn("select pk_tache_import_donnees from tache_import_donnees where uuid=:uuid", $flux);
            if ($pk_tache_import_donnees) {
                $args = array_combine(array_keys($flux), array_keys($flux));
                $set = str_replace("=", "=:", http_build_query($args, null, ','));
                $flux["pk_tache_import_donnees"] = $pk_tache_import_donnees;
                $CATALOGUE->executeQuery("update tache_import_donnees set " . $set . ", import_date_request=now() where pk_tache_import_donnees=:pk_tache_import_donnees", $flux);
            } else {
                $CATALOGUE->executeQuery("insert into tache_import_donnees (" . implode(", ", array_keys($flux)) . ") values (:" . implode(", :", array_keys($flux)) . ")", $flux);
            }

            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
            $results[$iCouche]["import_status"] = "Ajouté à la tâche planifiée d'import";
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();

            return $this->formatJsonError(500, "Internal Server Error", $exception->getMessage());
        }

        return new JsonResponse(array("status" => "SUCCESS", "success" => true, "results" => $results));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/deferimport/{uuid}/{layertype}/{schemaDestination}/{mode}/{action}", name="carmen_ws_deferimport", options={"expose"=true},
     *     requirements={"layertype"="[-_%.a-zA-Z0-9]+", "uuid"="[-_%.a-zA-Z0-9]+"})
     */
    public function deferImportAction(Request $request, $uuid, $layertype = "VECTOR", $schemaDestination = null, $mode = self::ACTION_CREATE, $action = null)
    {
        // vérification de sécurité
        if (!$this->debug) \Carmen\ApiBundle\Controller\ProdigeController::checkSecurity($this->container, $uuid);

        if ($layertype == 'MODELE') {
            return $this->deferImportDump($request, $uuid, $layertype, $schemaDestination, $mode, $action);
        }

        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $sql = " select m.id " .
            " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
            " where m.uuid= :uuid";
        $_fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $uuid), 0);
        $_uuid = $uuid;

        $couches = $request->request->get('couches', array());
        $results = array();

        foreach ($couches as $iCouche => $couche) {
            !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
            $fmeta_id = (isset($couche["fmeta_id"]) ? $couche["fmeta_id"] : $_fmeta_id);
            $uuid = (isset($couche["uuid"]) ? $couche["uuid"] : $_uuid);
            extract($couche);
            $file_source = urldecode($couche["file_source"]);
            $couchd_emplacement_stockage = $couche["couchd_emplacement_stockage"];
            $pk_couche_donnees = $couche["pk_couche_donnees"];
            $couchd_id = (string)$pk_couche_donnees;
            $coucheFields = array("pk_couche_donnees", "couchd_id", "couchd_nom", "couchd_description", "couchd_type_stockage", "couchd_emplacement_stockage", "couchd_wms", "couchd_wfs");

            if (!$pk_couche_donnees) {
                // ajout d'une nouvelle couche
                $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                $couchd_id = (string)$pk_couche_donnees;
                $couchd_visualisable = $layertype == "MNT" ? 0 : 1;
                $coucheFields[] = "couchd_visualisable";
                //force value to 1
                $couchd_fk_acces_server = 1;
                $coucheFields[] = "couchd_fk_acces_server";
                $CATALOGUE->executeQuery("insert into couche_donnees (" . implode(", ", $coucheFields) . ") values (:" . implode(", :", $coucheFields) . ")", compact($coucheFields));
                $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));

            } else {
                // maj d'une couche existante
                $set = array_combine($coucheFields, $coucheFields);
                $set = str_replace("=", "=:", http_build_query($set, null, ", "));
                //si pas de réimport de donnée on ne change pas l'emplacement de stockage
                if ($file_source == "" && $layertype != "RASTER") unset($coucheFields["couchd_emplacement_stockage"]);
                $CATALOGUE->executeQuery("update couche_donnees set " . $set . " where pk_couche_donnees=:pk_couche_donnees", compact($coucheFields));
            }
            $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees);

            $results[$iCouche] = array("pk_couche_donnees" => $pk_couche_donnees, "import_action" => ($couche["import_action"] == "create" ? "update" : $couche["import_action"]));

            if ($file_source == "") {
                $date = $CATALOGUE->fetchColumn("select changedate from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
                if ($date) $results[$iCouche]["import_status"] = "Importée le " . date("d/m/Y à H:i:s", strtotime($date));
                else $results[$iCouche]["import_status"] = "Import à configurer";
                $results[$iCouche]["import_status"] .= "<br/>(<i>Aucune source de donnée définie : non ajouté dans la tâche planifiée d'import</i>)";
                $results[$iCouche]["import_action"] = $couche["import_action"];
                continue;
            }

            $file_source = rtrim($this->getMapfileDirectory(), "/") . "/" . $file_source;

            $flux = array(
                "uuid" => $uuid,
                "user_id" => User::GetUserId(),
                "import_type" => "import_differe",
                "import_table" => $couchd_emplacement_stockage,
                "import_data_type" => $layertype,
                "import_data_source" => $file_source,
                "pk_couche_donnees" => $pk_couche_donnees,
                "import_extra_params" => json_encode(array(
                    "encoding_source" => $couche["encoding_source"],
                    "projection_cible" => $couche["projection_cible"],
                    "projection_source" => $couche["projection_source"],
                    "import_action" => $couche["import_action"],
                    "field_key" => $couche["field_key"],
                    "file_georeferenced" => $couche["file_georeferenced"],
                    "file_sheet" => $couche["file_sheet"],
                    "layerfields" => $couche["layerfields"],
                    "couchd_type_stockage" => $couche["couchd_type_stockage"],
                )),
            );
            $pk_tache_import_donnees = $CATALOGUE->fetchColumn("select pk_tache_import_donnees from tache_import_donnees where uuid=:uuid and pk_couche_donnees=:pk_couche_donnees", $flux);
            if ($pk_tache_import_donnees) {
                $args = array_combine(array_keys($flux), array_keys($flux));
                $set = str_replace("=", "=:", http_build_query($args, null, ','));
                $flux["pk_tache_import_donnees"] = $pk_tache_import_donnees;
                $CATALOGUE->executeQuery("update tache_import_donnees set " . $set . ", import_date_request=now() where pk_tache_import_donnees=:pk_tache_import_donnees", $flux);
            } else {
                $CATALOGUE->executeQuery("insert into tache_import_donnees (" . implode(", ", array_keys($flux)) . ") values (:" . implode(", :", array_keys($flux)) . ")", $flux);
            }

            $results[$iCouche]["import_status"] = "Ajouté à la tâche planifiée d'import";
            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
        }
        return new JsonResponse(array("status" => "SUCCESS", "success" => true, "results" => $results));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/readzip/{uuid}/{layertype}/{filezip}", name="carmen_ws_readzip", options={"expose"=true},
     *     requirements={"layertype"="[-_%.a-zA-Z0-9]+", "uuid"="[-_%.a-zA-Z0-9]+", "filezip"="[-_%.a-zA-Z0-9]+"})
     */
    public function readZipAction(Request $request, $filezip, $uuid, $layertype = "VECTOR", $rootDir = null)
    {
        // vérification de sécurité
        // if ( !$this->debug ) \Carmen\ApiBundle\Controller\ProdigeController::checkSecurity($this->container, $uuid);

        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $sql = " select m.id " .
            " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
            " where m.uuid= :uuid";
        $_fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $uuid), 0);


        $rootDir = $rootDir ?: rtrim($this->getMapfileDirectory(), "/") . "/";
        $filezip = $rootDir . urldecode($filezip);
        $metadata_ids = array();
        try {
            $insert_files = $this->analyseZipFile($filezip, $layertype);

            $couches = array();

            if ($layertype == "MULTI") {
                //first create record for series metadata in couche_donnees
                $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                $CATALOGUE->executeQuery("insert into couche_donnees (pk_couche_donnees, couchd_id, couchd_nom, couchd_type_stockage, couchd_fk_acces_server) values (:pk_couche_donnees, :pk_couche_donnees,:couchd_id, -1, 1)",
                    array("pk_couche_donnees" => $pk_couche_donnees,
                        "couchd_id" => "1_" . $pk_couche_donnees));

                $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)",
                    array("fmeta_id" => $_fmeta_id,
                        "pk_couche_donnees" => $pk_couche_donnees));

                $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();

                $this->attachDomaineSousDomaine($CATALOGUE, $_fmeta_id, $pk_couche_donnees, true);
            }


            //then for all files
            foreach ($insert_files as $inserted_file) {
                $pk_couche_donnees = $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0);
                $couchd_emplacement_stockage = $inserted_file["stockage"];
                list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                $i = 1;
                while (!$unique) {
                    $couchd_emplacement_stockage = $inserted_file["stockage"] . "_" . ($i++);
                    list($unique, $table_exists) = array_values($this->isUniqueStorageAction($couchd_emplacement_stockage, $pk_couche_donnees));
                }
                $couchd_id = (string)$pk_couche_donnees;
                $dbdata = array(
                    "pk_couche_donnees" => $pk_couche_donnees,
                    "couchd_id" => $couchd_id,
                    "couchd_nom" => $inserted_file["couchd_nom"],
                    "couchd_description" => "Donnée configurée automatiquement le " . date("d/m/Y à H:i:s") . " par lecture du fichier ZIP " . basename($filezip),
                    "couchd_type_stockage" => $inserted_file["type_stockage"],
                    "couchd_emplacement_stockage" => $couchd_emplacement_stockage,
                    "couchd_fk_acces_server" => 1,
                );
                $couchedFields = array_keys($dbdata);

                $data = array_merge($dbdata, array(
                    "import_status" => "Configuration partielle par import ZIP",
                    "file_source" => str_replace($rootDir, "", $inserted_file["file"]),
                    "file_name" => str_replace($rootDir . "temp/", "", $inserted_file["file"]),
                    "autoLoad" => true,
                ));

                // lecture des champs du fichier
                $json = $this->readFileAction($request, $pk_couche_donnees, urlencode(str_replace($rootDir, "", $inserted_file["file"])), $inserted_file["type_stockage"], null, $layertype)->getContent();
                $json = json_decode($json, true);
                $data["file_source"] = str_replace($rootDir, "", urldecode($json["layerfile"]));
                $json["layerfields"] = (isset($json["layerfields"]) ? $json["layerfields"] : array());
                $data["sheets"] = $json["sheets"];
                $data["layerfields"] = $json["layerfields"];
                foreach ($data["layerfields"] as $iField => $field) {
                    $data["layerfields"][$iField]["field_visible"] = true;
                }
                $data["file_georeferenced"] = $inserted_file["georeferenced"];
                $data["encoding_source"] = $json["encoding"] ?: "UTF-8";

                $fmeta_id = $this->getMetadataId($uuid, $_fmeta_id, $layertype, $data["file_source"]);
                if (!$fmeta_id) continue;
                $data["fmeta_id"] = $fmeta_id;
                $data["mtype"] = "CoucheDonnees";

                $metadata_ids[$fmeta_id][] = $data;

                if (!$this->debug) $CATALOGUE->executeQuery("insert into couche_donnees (" . implode(", ", $couchedFields) . ") values (:" . implode(", :", $couchedFields) . ")", $dbdata);
                if (!$this->debug) $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));
                $couches[] = $data;
            }
        } catch (\Exception $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $exception->getMessage(), array("stack" => $exception->getTraceAsString()));
        }

        if ($layertype == "MULTI") {
            $geonetwork = new GeonetworkInterface();
            $related = $geonetwork->getMetadataRelations($uuid, "children", "json");
            $related = json_decode($related, true);
            $uuids = array();
            if (!isset($related)) $related = array();
            if (isset($related["relation"])) $related = $related["relation"];
            if (isset($related["children"])) $related = $related["children"];
            if (!is_numeric(key($related))) $related = array($related);
            foreach ($related as $child) {
                $geonetInfo = $child;
                $geonetInfo["uuid"] = $geonetInfo["id"];
                $geonetInfo["id"] = $CATALOGUE->fetchColumn("select id from public.metadata where uuid=:uuid", array("uuid" => $geonetInfo["uuid"]));
                if (!in_array($geonetInfo["id"], array_keys($metadata_ids))) continue;
                $uuids[$geonetInfo["uuid"]] = array(
                    "uuid" => $geonetInfo["uuid"],
                    "id" => $geonetInfo["id"],
                    "metadata_type" => $child["type"],
                    "metadata_title" => current($child["title"]),
                    //"metadata_desc" => $child["abstract"],
                    "mtype" => "Metadata",
                    "leaf" => false,
                    "metadata_datatype" => self::$DATATYPE_TO_STOCKAGE[$metadata_ids[$geonetInfo["id"]][0]["couchd_type_stockage"] ?: self::DEFAULT_TYPE_STOCKAGE],
                    "children" => $metadata_ids[$geonetInfo["id"]]
                );
            }
            $couches = array_values($uuids);
        }
        return $this->formatJsonSuccess(array("children" => $couches));
    }

    protected function getMetadataId($uuid, $fmeta_id, $layertype, $couchd_emplacement_stockage)
    {
        if ($layertype != "MULTI") return $fmeta_id;

        $featureCatalogue = new FeatureCatalogueController();
        $featureCatalogue->setContainer($this->container);
        $request = new Request();
        $request->query->replace(array(
            "metadata_id" => $fmeta_id,
            "category" => "children",
            "mode" => "replace"
        ));
        $result = $featureCatalogue->featureCatalogueAction($request);

        $result = $result->getContent();

        $result = json_decode($result, true);

        if (!$result) return null;
        if (!isset($result["update_success"])) return null;
        if (!$result["update_success"]) {
            return null;
        }
        $metadata_id = $result["child_id"];
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $data = $CATALOGUE->fetchColumn("select data from public.metadata where id=:metadata_id", compact("metadata_id"));
        $doc = new \DOMDocument();
        if ($doc->loadXML($data)) {
            $xpath = new \DOMXPath($doc);
            $titles = $xpath->query("//gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
            for ($i = 0; $i < $titles->length; $i++) {
                $node = $titles->item($i);
                $node->nodeValue = trim($node->nodeValue) . " - Créée par import ZIP des données SIG " . $couchd_emplacement_stockage;
            }
            $types = $xpath->query("//gmd:hierarchyLevel/gmd:MD_ScopeCode");
            for ($i = 0; $i < $types->length; $i++) {
                $node = $types->item($i);
                $node->attributes->getNamedItem("codeListValue")->nodeValue = "dataset";
            }
        }
        $data = $doc->saveXML();

        $data = preg_replace("!<\?xml.+\?>\n!", "", $data);
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE);
        $geonetwork->editMetadata($metadata_id, $data);

        $this->curlToCatalogue("/prodige/updateArbo", array(
            "metadata_id" => $fmeta_id,
        ));
        return $metadata_id;
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/refreshdiffusion/{pk_couche_donnees}/{callback}", name="carmen_ws_refreshdiffusion", options={"expose"=true},
     *     requirements={"pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
     */
    public function refreshCoucheDiffusion($pk_couche_donnees, $callback)
    {
        $callback = urldecode($callback);
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $data = $CATALOGUE->fetchAssoc("select couchd_wfs, couchd_wms from couche_donnees where pk_couche_donnees=:pk_couche_donnees", compact("pk_couche_donnees"));
        if (!$callback) {
            return $this->formatJsonSuccess(array("data" => $data));
        }
        return new Response("<html><head><script type='text/javascript'>window.onload = function(){" . $callback . "({$pk_couche_donnees}, " . json_encode($data) . ", window);}</script></head><body></body></html>");
    }


    /**
     * @isGranted("ROLE_USER")
     * @Route(
     *  "/generate_raster_tileindex/{extension}/{shapefile}/{directory}",
     *  name="carmen_generaterastertileindex",
     *  defaults={"directory"=""},
     *  requirements={"shapefile"="[-_%.a-zA-Z0-9]+", "extension"="_first_|gtiff|tif|tiff|ecw|jp2|jpg|jpeg", "directory"=".*"},
     *  options={"expose"=true}
     * )
     */
    public function generateRasterTileindexAction($extension, $shapefile, $directory = "")
    {
        return parent::generateRasterTileindexAction($extension, $shapefile, $directory);
    }


    /**
     * @isGranted("ROLE_USER")
     * @Route("/join/initview/{uuid}/{view_name}", name="carmen_initviewjoin", options={"expose"=true})
     */
    public function initViewJoinAction(Request $request, $uuid, $view_name)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $sql = " select m.id " .
            " from " . self::CATALOGUE_SCHEMA_PUBLIC . ".metadata m " .
            " where m.uuid= :uuid";
        $fmeta_id = $CATALOGUE->fetchColumn($sql, array("uuid" => $uuid), 0);
        list($unique, $table_exists) = array_values($this->isUniqueStorageAction($view_name));

        if ($unique) {
            $coucheFields = array(
                "pk_couche_donnees" => $CATALOGUE->fetchColumn("select nextval('seq_couche_donnees'::regclass)", array(), 0),
                "couchd_nom" => $request->get("view_title", $view_name),
                "couchd_type_stockage" => array_search("join", self::$DATATYPE_TO_STOCKAGE),
                "couchd_emplacement_stockage" => $view_name
            );

            // ajout d'une nouvelle couche
            $pk_couche_donnees = $coucheFields["pk_couche_donnees"];
            $coucheFields["couchd_id"] = (string)$pk_couche_donnees;

            $coucheFields["couchd_visualisable"] = 1;
            //force value to 1
            $coucheFields["couchd_fk_acces_server"] = 1;
            $CATALOGUE->executeQuery("insert into couche_donnees (" . implode(", ", array_keys($coucheFields)) . ") values (:" . implode(", :", array_keys($coucheFields)) . ")", $coucheFields);
            $CATALOGUE->executeQuery("insert into fiche_metadonnees (fmeta_id, fmeta_fk_couche_donnees) values (:fmeta_id, :pk_couche_donnees)", compact("fmeta_id", "pk_couche_donnees"));

            $fmeta_id && $this->attachDomaineSousDomaine($CATALOGUE, $fmeta_id, $pk_couche_donnees, true);

            return new JsonResponse(array(
                "success" => true,
                "pk_couche_donnees" => $pk_couche_donnees
            ));
        } else {
            return new JsonResponse(array(
                "success" => false,
                "message" => "Le nom de la vue '" . $view_name . "'est déjà utilisé."
            ));
        }
    }


    /**
     * @isGranted("ROLE_USER")
     * @Route("/join/savediffusionwxs/{pk_couche_donnees}/{wxs_type}/{wxs_enable}", name="carmen_savediffusionwxs_join", options={"expose"=true},
     *     requirements={"uuid"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+", "wxs_type"="wfs|wms", "wxs_enable"="0|1"})
     */
    public function saveDiffusionWxsJoinAction(Request $request, $pk_couche_donnees, $wxs_type, $wxs_enable)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        $coucheFields = array(
            "pk_couche_donnees" => $pk_couche_donnees,
            "couchd_" . $wxs_type => $wxs_enable,
        );
        $CATALOGUE->executeQuery("update couche_donnees set couchd_" . $wxs_type . "=:couchd_" . $wxs_type . " where pk_couche_donnees=:pk_couche_donnees", $coucheFields);

        return new JsonResponse(array(
            "success" => true,
        ));
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/webservices/domaines/{fmeta_id}", name="carmen_webservice_domaines", options={"expose"=true}, requirements={"fmeta_id"="[-_%.a-zA-Z0-9]+"})
     */
    public function getDomainesWebServiceAction($fmeta_id)
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);

        $query = "select * "
            . " from metadonnees_sdom"
            . " inner join rubric_param on (dom_coherence=rubric_id)"
            . " inner join domaine using(pk_domaine)"
            . " inner join sous_domaine using(pk_sous_domaine)"
            . " where fmeta_id=:fmeta_id";
        $metadonnees_ssdom = $CATALOGUE->fetchAll($query, array("fmeta_id" => $fmeta_id));

        $tree = array();
        foreach ($metadonnees_ssdom as $ssdom) {
            $rubric_id = $ssdom["rubric_id"];
            $domaine_id = $ssdom["pk_domaine"];
            $sousdomaine_id = $ssdom["pk_sous_domaine"];
            if (!isset($tree[$rubric_id])) {
                $tree[$rubric_id] = array(
                    "type" => "rubric",
                    "id" => $rubric_id,
                    "text" => html_entity_decode($ssdom["rubric_name"]),
                    "ssdom_service_wms" => false,
                    "children" => array(),
                    "expanded" => true
                );
            }
            if (!isset($tree[$rubric_id]["children"][$domaine_id])) {
                $tree[$rubric_id]["ssdom_service_wms"] = $tree[$rubric_id]["ssdom_service_wms"] || $ssdom["ssdom_service_wms"];
                $tree[$rubric_id]["children"][$domaine_id] = array(
                    "type" => "domaine",
                    "id" => $domaine_id,
                    "rubric_id" => $rubric_id,
                    "text" => html_entity_decode($ssdom["dom_nom"]),
                    "ssdom_service_wms" => false,
                    "children" => array(),
                    "expanded" => true
                );
            }
            if (!isset($tree[$rubric_id]["children"][$domaine_id]["children"][$sousdomaine_id])) {
                $tree[$rubric_id]["children"][$domaine_id]["ssdom_service_wms"] = $tree[$rubric_id]["children"][$domaine_id]["ssdom_service_wms"] || $ssdom["ssdom_service_wms"];
                $tree[$rubric_id]["children"][$domaine_id]["children"][$sousdomaine_id] = array(
                    "type" => "sous_domaine",
                    "id" => $sousdomaine_id,
                    "rubric_id" => $rubric_id,
                    "domaine_id" => $domaine_id,
                    "text" => html_entity_decode($ssdom["rubric_name"] . " / " . $ssdom["dom_nom"] . " / " . $ssdom["ssdom_nom"]),
                    "ssdom_service_wms" => (bool)$ssdom["ssdom_service_wms"] ?: false,
                    "leaf" => true
                );
            }
        }
        $sous_domaines = array();

        foreach ($tree as $rubric_id => $rubric) {
            foreach ($rubric["children"] as $domaine_id => $domaine) {
                $tree[$rubric_id]["children"][$domaine_id]["children"] = array_values($tree[$rubric_id]["children"][$domaine_id]["children"]);
                $sous_domaines = array_merge($sous_domaines, $tree[$rubric_id]["children"][$domaine_id]["children"]);
            }
            $tree[$rubric_id]["children"] = array_values($tree[$rubric_id]["children"]);
        }
        $tree = array_values($tree);

        return new JsonResponse($sous_domaines);
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/geocodageinverse/{fmeta_id}/{pk_couche_donnees}", name="carmen_ws_geocodageinverse", options={"expose"=true},
     *     requirements={"fmeta_id"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
     */
    public function geocodageInverseAction(Request $request, $fmeta_id, $pk_couche_donnees)
    {
        //$urlApiBan = "https://api-adresse.data.gouv.fr/reverse/csv/";
        $urlApiBan = $this->container->getParameter('URL_API_BAN') . "reverse/csv/";
        $geocodingData = $this->getMapfileDirectory() . "temp/";

        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        $PRODIGE = $this->getProdigeConnection("public");
        try {
            $CATALOGUE->beginTransaction();
            $PRODIGE->beginTransaction();

            $queryParams = array(
                "pk_couche_donnees" => $pk_couche_donnees,
            );

            $query = "SELECT couchd_emplacement_stockage FROM catalogue.couche_donnees WHERE pk_couche_donnees = :pk_couche_donnees";
            $rs = $CATALOGUE->executeQuery($query, $queryParams);

            /*if(count($rs)>1 || count($rs) == 0){
                return new JsonResponse(array("status"=>"FAILED", "success"=>false, "message"=>"invalid number of couchd_emplacement_stockage"));
            } */
            $couche = "";
            foreach ($rs as $ligne) {
                if ($couche == "") {
                    $couche = $ligne["couchd_emplacement_stockage"];
                } else {
                    return new JsonResponse(array("status" => "FAILED", "success" => false, "message" => "invalid number of couchd_emplacement_stockage"));
                }
            }

            $queryParams = array(
                "table_schema" => "public",
                "table_name" => $couche
            );

            $proj = 4326;

            $query = "SELECT gid, ST_XMin(ST_Transform(the_geom, :proj::int)) as longitude, ST_YMin(ST_Transform(the_geom, :proj::int)) as latitude FROM " . self::PRODIGE_SCHEMA_PUBLIC . "." . $couche . " ORDER BY gid";
            $rs = $PRODIGE->executeQuery($query, array("proj" => $proj));

            $fileExport = $geocodingData . "export_" . $couche . ".csv";
            $fileToWrite = fopen($fileExport, 'w');

            if (!$fileToWrite) {
                return new JsonResponse(array("status" => "FAILED", "success" => false, "message" => "Impossible d'ouvrir le fichier à envoyer à l'api ban"));
            }
            $header = array("gid", "longitude", "latitude");
            $data = str_getcsv(implode(",", $header));
            fputcsv($fileToWrite, $data);

            foreach ($rs as $row) {
                $data = str_getcsv(implode(",", $row), ",");
                fputcsv($fileToWrite, $data);
            }

            fclose($fileToWrite);

            if (filesize($fileExport) > 6 * 1024 * 1024) {
                return new JsonResponse(array("status" => "FAILED", "success" => false, "message" => "Impossible d'effectuer le géocodage inverse, le poids du fichier est supérieu à 6 Mo"));
            }

            //This needs to be the full path to the file you want to send.
            $file_name_with_full_path = realpath($fileExport);
            //cURL headers for file uploading
            $headers = array("Content-Type:multipart/form-data");
            // data to post
            $postfields = array(
                "data" => new \CURLFile($file_name_with_full_path)//,
                //"citycode" => "code_insee_commune"
            );

            $curl = curl_init();

            // cURL options
            $options = array(
                CURLOPT_URL => $urlApiBan,
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_POSTFIELDS => $postfields,
                CURLOPT_HEADER => false, //Not return header
                CURLOPT_SSL_VERIFYPEER => false, //Don't veryify server certificate
                CURLOPT_RETURNTRANSFER => true
            );

            curl_setopt_array($curl, $options);
            $geocoding_data = curl_exec($curl);
            $geocoding_data = trim($geocoding_data);

            curl_close($curl);

            $fileImport = $geocodingData . "import_" . $couche . ".csv";
            $fileToWriteImport = fopen($fileImport, 'w');
            if (!$fileToWriteImport) {
                return new JsonResponse(array("status" => "FAILED", "success" => false, "message" => "Impossible d'ouvrir le fichier pour écrire les informations de l'api ban"));
            } else {
                fwrite($fileToWriteImport, trim($geocoding_data));
                fclose($fileToWriteImport);
            }

            $handle = fopen($fileImport, "r");
            $tabData = array();
            while (($row = fgets($handle)) !== FALSE) {
                $tabData[] = str_getcsv($row, ",");
            }
            $tabData[0][0] = "gid";

            foreach ($tabData[0] as $colonne) {
                $query = "ALTER TABLE " . $couche . " ADD COLUMN IF NOT EXISTS " . $colonne . " text";
                $PRODIGE->executeQuery($query, array());
            }
            $header = array_shift($tabData);

            foreach ($tabData as $line) {
                $data = $line;
                //On retire les trois première champs qui correspondent champs envoyés à l'api
                $gid = array_shift($data);
                array_shift($data);
                array_shift($data);
                //except empty lines
                if ($data[0] === '') {
                    continue;
                }
                $query = "UPDATE " . $couche . " set ";
                $queryParam = array();
                foreach ($data as $index => $column) {
                    if ($column != "") {
                        $query .= $header[$index + 3] . "=:" . $header[$index + 3] . ", ";
                        $queryParam[$header[$index + 3]] = $column;
                    }
                }
                $query = substr($query, 0, -2);
                $query .= " WHERE gid = " . $gid;
                $PRODIGE->executeQuery($query, $queryParam);

            }

            $importeur = new ImportInPostgis($this, self::PRODIGE_SCHEMA_PUBLIC, $this->container->getParameter("CMD_OGR2OGR", "ogr2ogr"));

            $importeur->DeletingFileTempo($fileExport);
            $importeur->DeletingFileTempo($fileImport);

            $CATALOGUE->isTransactionActive() && $CATALOGUE->commit();
            $PRODIGE->isTransactionActive() && $PRODIGE->commit();
        } catch (\Exception $exception) {
            $CATALOGUE->isTransactionActive() && $CATALOGUE->rollBack();
            $PRODIGE->isTransactionActive() && $PRODIGE->rollBack();

            $message = $exception->getMessage();
            $message = "Erreur : <br/>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*:\s*!i", "", $message);

            return $this->formatJsonError(200, "Une erreur s'est produite", "", array(
                "data" => explode("\n", str_replace(array("<br>", "<br/>"), array("\n", "\n"), $exception->getMessage())),
                "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
                "stack" => explode("#", $exception->getTraceAsString()),
                "status" => "FAILURE",
                "message" => $message,
                "success" => false,
            ));
        }
        return new JsonResponse(array("status" => "SUCCESS", "success" => true, "import_status" => "Géocodage inverse terminé avec succès"));
    }
}