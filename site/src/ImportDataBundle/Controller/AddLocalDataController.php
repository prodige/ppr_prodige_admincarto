<?php

namespace Carmen\ImportDataBundle\Controller;

use JMS\Serializer\SerializationContext;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

// use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * @author alkante
 * @Route("/addlocaldata")
 */
class AddLocalDataController extends AbstractImportDataController
{
  const DEFAULT_TYPE_STOCKAGE = "1"; //vector
  protected $debug = false;

  /**
   * @IsGranted("ROLE_USER")
   * @Route(name="carmen_addlocaldata", options={"expose"=true})
   */
  public function indexAction()
  {
    $serializer = $this->get('jms_serializer');
    $context = new SerializationContext();
    $context->setSerializeNull(true);
    $user = $serializer->serialize($this->getUser(), 'json', $context);

    $config = $this->get('carmen_config')->getWebConfig();
    $config["copyright"] = htmlentities($config["copyright"]);
    $defaults = $this->container->getParameter('jsdefaults', array());
    $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));
    $prodigeConfig = $this->get('prodige.configreader');
    $prodigeConfig instanceof ConfigReader;

    $defaults["CATALOGUE_URL"] =  $this->getParameter("PRODIGE_URL_CATALOGUE", "");
    $defaults["DATATYPE_TO_STOCKAGE"] =  array_merge(self::$DATATYPE_TO_STOCKAGE, array_flip(self::$DATATYPE_TO_STOCKAGE));
    $defaults["AddToWebServices"] = $this->getParameter("PRODIGE_URL_CATALOGUE") . "/geosource/layerAddToWebService";

    return $this->render('ImportDataBundle/Default/addlocaldata.html.twig', array(
      'user' => $user,
      'parameters' => $config,
      'jsdefaults' => $defaults,
    ));
  }


  /**
   * @IsGranted("ROLE_USER")
   * @Route("/import/{returnType}", name="carmen_ws_importlocaldata", defaults={"returnType"="result"}, requirements={"returnType"="result|entity"}, options={"expose"=true})
   */
  public function importAction(Request $request, $returnType = "result")
  {
    try {
      ms_ResetErrorList();
      $PRO_MAPFILE_PATH = rtrim($this->getMapfileDirectory(), "/");
      $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_LOCALDATA . "," . self::PRODIGE_SCHEMA_PUBLIC);
      $defaults = $this->container->getParameter('jsdefaults', array());
      $defaults["PROJECTION"] = (defined("PRO_IMPORT_EPSG") ? PRO_IMPORT_EPSG : $this->container->getParameter('PRODIGE_DEFAULT_EPSG'));
      $parametersBag = $request->request;
      $file_source = $parametersBag->get('file_source', '');
      $file_source = urldecode($file_source);
      $file_source = ($file_source != "" ? $PRO_MAPFILE_PATH . "/" . $file_source : "");
      $parametersBag->set('file_source', $file_source);
      $filename = pathinfo($file_source, PATHINFO_FILENAME);
      //prevent from begining with numbers for postgres syntax
      $couchd_emplacement_stockage = preg_replace("!\W!", "_", preg_replace('#^\d+#', '', $filename));
      $parametersBag->set('couchd_emplacement_stockage', strtolower(self::PRODIGE_SCHEMA_LOCALDATA . "." . uniqid($couchd_emplacement_stockage)));

      //Import dans la base
      $result = $this->doImportInPostgis($parametersBag, null, "VECTOR", true, self::PRODIGE_SCHEMA_LOCALDATA);


      //Création du mapfile de couche
      $tablename = $result["tablename"] = $parametersBag->get('couchd_emplacement_stockage', "");
      $t = explode(".", $result["tablename"]);
      $mapfile   = $result["tablename"] = $result["mapfile"]   = end($t);
      $layerName = $result["layername"]   = basename($file_source);

      try {
        $resGeometrie = $PRODIGE->fetchColumn("select DISTINCT public.geometrytype(the_geom) AS type FROM {$tablename} where public.geometrytype(the_geom) is not null");
      } catch (\Exception $exception) {
        throw new \Exception("Impossible de récupérer la géométrie des éléments insérés en base.", $exception);
      }
      try {
        $resSrid = $PRODIGE->fetchColumn("select DISTINCT public.st_srid(the_geom) AS type FROM {$tablename} where public.geometrytype(the_geom) is not null ");
      } catch (\Exception $exception) {
        throw new \Exception("Impossible de récupérer la géométrie des éléments insérés en base.", $exception);
      }


      if ($returnType == "entity") {
        $resGeometrie = str_replace("MULTI", "", $resGeometrie);
        $resGeometrie = str_replace("LINESTRING", "LINE", $resGeometrie);

        $mapId = $request->get('mapId', 0);
        $parameters = array(
          "layerType"              => "POSTGIS",
          "msLayerType"            => $resGeometrie,
          "msGeometryType"         => $resGeometrie,
          "msLayerPgGeometryField" => "the_geom",
          "msLayerPgIdField"       => "gid",

          "layerProjectionEpsg"    => $resSrid,
          "msLayerPgProjection"    => $resSrid,
          "msLayerPgSchema"        => self::PRODIGE_SCHEMA_LOCALDATA,
          "msLayerPgTable"         => $mapfile,
        );
        foreach ($parameters as $key => $value) {
          $request->request->set($key, $value);
        }
        // return $this->forward('CarmenApiBundle:Layer:postLayer', array("request" => $request, "mapId" => $mapId, "id" => null));
        return $this->forward('Carmen\ApiBundle\Controller\LayerController::postLayerAction', array("request" => $request, "mapId" => $mapId, "id" => null));
      }


      switch ($resGeometrie) {
        case "POINT":
        case "MULTIPOINT":
          $resGeometrie = MS_LAYER_POINT;
          break;
        case "POLYGON":
        case "MULTIPOLYGON":
          $resGeometrie = MS_LAYER_POLYGON;
          break;
        case "LINESTRING":
        case "MULTILINESTRING":
        default:
          $resGeometrie = MS_LAYER_LINE;
          break;
      }
      $conn_string =  sprintf(
        "host=%s port=%s user=%s password=%s dbname=%s",
        $PRODIGE->getHost(),
        $PRODIGE->getPort(),
        $PRODIGE->getUsername(),
        $PRODIGE->getPassword(),
        $PRODIGE->getDatabase()
      );
      //Creation fichier mapfile (defaut + donnees)
      if (file_exists($PRO_MAPFILE_PATH . "/Modele.map")) {
        $oMap = ms_newMapObj($PRO_MAPFILE_PATH . "/Modele.map");
        if (copy($PRO_MAPFILE_PATH . "/Modele.map", $PRO_MAPFILE_PATH . "/local_data/" . $mapfile . ".map")) {
          $oMapTemp = ms_newMapObj($PRO_MAPFILE_PATH . "/local_data/" . $mapfile . ".map");
          $p_oLayer = ms_newLayerObj($oMapTemp);
          $p_oLayer->set("name", $mapfile);
          $p_oLayer->setConnectionType(MS_POSTGIS);
          $p_oLayer->set("connection", $conn_string);
          $p_oLayer->set("data", "the_geom FROM (select * from \"local_data\".\"" . $mapfile . "\") as foo USING UNIQUE gid USING srid=" . $defaults["PROJECTION"]);
          $p_oLayer->set("status", MS_ON);
          $p_oLayer->set("type", $resGeometrie);
          $p_oLayer->set("template", "Consultable");
          $p_oLayer->setMetadata("CLASS_LEGENDE", "ON");
          $p_oLayer->setMetadata("ECHELLE_LEGENDE", "ON");
          $p_oLayer->setMetadata("LAYER_TITLE", $layerName);
          $p_oLayer->setMetadata("GI_ARBO_ORDRE", 2);
          $p_oLayer->setMetadata("PROCESSING ", "CLOSE_CONNECTION=DEFER");

          $p_oClass = ms_newClassObj($p_oLayer);
          $p_oClass->set("name", $p_oLayer->getMetadata("LAYER_TITLE"));

          $p_oStyle = ms_newStyleObj($p_oClass);
          $p_oStyle->color->setRGB(255, 153, 0);
          switch ($resGeometrie) {
            case MS_LAYER_POLYGON:
              $p_oStyle->set("symbolname", "Carre");
              break;
            case MS_LAYER_LINE:
              $p_oStyle->set("size", 2);
              $p_oStyle->set("symbolname", "ms_line_solid");
              break;
            case MS_LAYER_POINT:
              $p_oStyle->set("symbolname", "Cercle");
              $p_oStyle->set("size", 5);
              break;
          }
          $oMapTemp->save($PRO_MAPFILE_PATH . "/local_data/" . $mapfile . ".map");
          //Ajout du nouveau layer
          //Creation de la legende
          if ($p_oLayer->open() == MS_SUCCESS) {
            $champs = $p_oLayer->getItems();
            $strChamps = "";
            $strBriefField = "";
            for ($i = 0; $i < count($champs); $i++) {
              $strBriefField = $champs[0] . "|" . $champs[0] . "|TXT|";
              $strChamps .= $champs[$i] . "|" . $champs[$i] . "|TXT|" . ";";
            }
            $strChamps = substr($strChamps, 0, -1);
            $result["infoFields"] = $strChamps;
            $result["briefField"] = $strBriefField;
            $result["tooltipFields"] = $strBriefField;
            $p_oLayer->close();
          }
          $o_msIcon = $p_oClass->createLegendIcon(20, 10);
          $o_msIcon->saveImage($PRO_MAPFILE_PATH . "/../IHM/LEGEND/" . $mapfile . "_legend.jpg");

          $result["mapfile"] = "local_data/" . $mapfile;
          $result["message"] = "Les données ont été importées. Cliquez sur \"OK\" pour ajouter la couche à la carte.";
        } else {
          throw new \Exception("Impossible de créé un nouveau mapfile à partir du modèle de carte.");
        }
      } else {
        throw new \Exception("Aucun modèle de carte connu. Impossible de créer un nouveau mapfile.");
      }
    } catch (\Exception $exception) {
      $message = $exception->getMessage();
      $mapserverErrors = array();
      $error = ms_GetErrorObj();
      while ($error && $error->code != MS_NOERR) {
        $mapserverErrors[] = sprintf("Error in %s: %s<br>\n", $error->routine, $error->message);
        $error = $error->next();
      }
      if (!empty($mapserverErrors)) {
        $message .= "<br/>" . implode("<br/>", $mapserverErrors);
      }
      $message = "Erreur : <br/>" . preg_replace("!Une\s+erreur\s+s'est\s+produite\s*:\s*!i", "", $message);

      return $this->formatJsonError(200, "Une erreur s'est produite", "", array(
        "data" => explode("\n", str_replace(array("<br>", "<br/>"), array("\n", "\n"), $exception->getMessage())),
        "function" => $exception->getFile() . "(" . $exception->getLine() . ")",
        "stack" => explode("#", $exception->getTraceAsString()),
        "status" => "FAILURE",
        "message" => $message,
        "success" => false,
        "tablename" => $request->request->get('couchd_emplacement_stockage', "")
      ));
    }

    return new JsonResponse($result);
  }

  /**
   * @IsGranted("ROLE_USER")
   * @Route("/redraw", name="carmen_ws_redrawlocaldata", options={"expose"=true})
   */
  public function RedrawAction(Request $request, $returnType = "result")
  {

    $result = array();
    $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_LOCALDATA . "," . self::PRODIGE_SCHEMA_PUBLIC);


    $parametersBag = $request->request;

    $MapName = $parametersBag->get('oMapName', '');



    $PRO_MAPFILE_PATH = rtrim($this->getMapfileDirectory(), "/");
    $local_mapfile_path = $PRO_MAPFILE_PATH . "/local_data/";
    $mapfile = $local_mapfile_path . $MapName . '.map';

    if (file_exists($PRO_MAPFILE_PATH . "/Modele.map")) {
      $oMap = ms_newmapobj($mapfile);
      $oTemp_layer = $oMap->getLayerByName($MapName);     // OK 

      $old_class_name = $oTemp_layer->getClass(0)->name;
      $new_class_name = $old_class_name;
      $oTemp_layer->removeClass(0);

      $p_oClass = ms_newClassObj($oTemp_layer);
      $p_oClass->set("name", $new_class_name);

      $table_name = "local_data." . $MapName;
      try {
        $resGeometrie = $PRODIGE->fetchColumn(("select DISTINCT public.geometrytype(the_geom) AS type FROM {$table_name} where public.geometrytype(the_geom) is not null"));
      } catch (\Exception $exception) {

        throw new \Exception("Impossible de récupérer la géométrie des éléments insérés en base.", $exception);
      }

      $style_params = array();
      $defaultColor = "255,153,0";

      switch ($resGeometrie) {
        case "POINT":
        case "MULTIPOINT":
          $requestPointShape = $parametersBag->get('oPointShape', "");
          switch ($requestPointShape) {
            case "ms_square":
              $pointShape = "Carre";
              break;
            case "ms_triangle":
              $pointShape = "Triangle";
              break;
            case "ms_star":
              $pointShape = "Etoile";
              break;
            case "ms_cross":
              $pointShape = "Croix";
              break;
            case "ms_plus":
              $pointShape = "Plus";
              break;
            default:
              $pointShape = "Cercle";
              break;
          }
          $style_params["symbolname"] = $pointShape;
          $style_params["size"] = intval($parametersBag->get('oPointSize', "5"));
          $main_color = $parametersBag->get('oPointColor', $defaultColor);
          break;
        case "POLYGON":
        case "MULTIPOLYGON":
          $style_params["symbolname"] = "Carre";
          $main_color = $parametersBag->get('oPolygonColor', $defaultColor);
          $PolygonShape = $parametersBag->get('oPolygonShape', "");
          $outside_color = $parametersBag->get('oPolygonEdgeColor', "");
          $outline_size = intval($parametersBag->get('oPolygonEdgeSize', 0));
          break;
        case "LINESTRING":
        case "MULTILINESTRING":
        default:
          $style_params["symbolname"] = $parametersBag->get('oLineShape', "ms_line_solid");
          $style_params["size"] = intval($parametersBag->get('oLineThickness', "2"));
          $main_color = $parametersBag->get('oLineColor', $defaultColor);
          break;
      }
      $p_oStyle = ms_newStyleObj($p_oClass);

      // couleur de remplissage
      if ($PolygonShape !== "ms_area_empty") {

        $color = explode(",", $main_color);
        $R = intval($color[0]);
        $G = intval($color[1]);
        $B = intval($color[2]);

        $p_oStyle->color->setRGB($R, $G, $B);
      }

      // couleur des contours
      if (isset($outline_size) && $outline_size !== "") {
        $out_color = explode(",", $outside_color);
        $out_R = intval($out_color[0]);
        $out_G = intval($out_color[1]);
        $out_B = intval($out_color[2]);

        $p_oStyle2 = ms_newStyleObj($p_oClass);
        $p_oStyle2->outlinecolor->setRGB($out_R, $out_G, $out_B);
        $p_oStyle2->set("size", intval($outline_size));
      }



      foreach ($style_params as $params => $value) {
        $p_oStyle->set($params, $value);
      }

      $oMap->save($mapfile);
      $result["success"] = true;
      $result["message"] = "Le layer à été correctement modifié";
    } else {
      $result["success"] = false;
      $result["message"] = "Impossible d'ouvrir le fichier demandé";
    }

    return new JsonResponse($result);
  }

  /**
   * @IsGranted("ROLE_USER")
   * @Route("/readfile/{layertype}/{couchd_type_stockage}/{pk_couche_donnees}/{layerfile}/{layersheet}", name="carmen_ws_readfile_localdata", options={"expose"=true}, defaults={"layersheet"=null}, requirements={"layerfile"="[-_%.a-zA-Z0-9]+", "pk_couche_donnees"="[-_%.a-zA-Z0-9]+"})
   */
  public function readFileAction(Request $request, $pk_couche_donnees, $layerfile, $couchd_type_stockage, $layersheet = null, $layertype = "VECTOR")
  {
    // return $this->forward('CarmenImportDataBundle:ImportData:readFile', compact("request", "pk_couche_donnees", "layerfile", "couchd_type_stockage", "layersheet", "layertype"));
    return $this->forward('Carmen\ImportDataBundle\Controller\ImportDataController::readFileAction', compact("request", "pk_couche_donnees", "layerfile", "couchd_type_stockage", "layersheet", "layertype"));
  }
}
