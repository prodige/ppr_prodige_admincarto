<?php

namespace Carmen\ImportDataBundle\Command;

use Prodige\ProdigeBundle\Services\ConfigReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UpdateByCsvGidCommand extends Command
{
    protected $debug = false;

    protected $directory = "/var/www/html/data/sivaccin_allocations";
    protected $table = "covid_vaccin_allocations";

    protected $logs = "logs";
    protected $archives = "archives";
    protected $logFile;

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('prodige:updateByCsvGid')
            ->setDescription('Update table by csv file with gid key');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->container;
        $configReader = $container->get('prodige.configreader');
        $configReader instanceof ConfigReader;
        $PRODIGE = $this->getDoctrine()->getConnection("prodige");

        $output->setDecorated(true);

        $this->logFile = $this->directory . "/" . $this->logs . "/update.log";

        if (!is_dir($this->directory . "/" . $this->logs)) {
            @mkdir($this->directory . "/" . $this->logs, 0770, true);
        }
        if (!is_dir($this->directory . "/" . $this->archives)) {
            @mkdir($this->directory . "/" . $this->archives, 0770, true);
        }

        $tabUpdates = array();
        //get csv files

        $this->tabImport = scandir($this->directory);

        foreach ($this->tabImport as $fileName) {
            if ($this->detectExtension($this->directory . "/" . $fileName) === "csv") {
                $fileData = $this->getUpdateData($this->directory . "/" . $fileName);
                if (count($fileData) > 0) {
                    $tabUpdates = array_merge($tabUpdates, $fileData);
                }
                //archive file
                $this->archiveData($this->directory . "/", $fileName);
            }
        }
        //save table data before update
        $tabSaveData = $PRODIGE->fetchAll("select  * from " . $this->table);
        $this->saveData($tabSaveData);

        //update

        foreach ($tabUpdates as $key => $values) {
            if ($values[0] === '') {
                $PRODIGE->executeQuery(
                    "insert into " . $this->table . " (date_livraison,lieu_gid,commentaire,
                    nombre_ucd,vaccin, finess_pharmacie_reference, 
                    libelle_finess_pharmacie_reference, _edit_datemaj) values (:date_livraison,:lieu_gid,:commentaire,
                    :nombre_ucd,:vaccin, :finess_pharmacie_reference, 
                    :libelle_finess_pharmacie_reference, now())",
                    array(
                        "date_livraison" => $values[1],
                        "lieu_gid" => $values[2],
                        "commentaire" => $values[3],
                        "nombre_ucd" => $values[4],
                        "vaccin" => $values[5],
                        "finess_pharmacie_reference" => $values[6],
                        "libelle_finess_pharmacie_reference" => $values[7],

                    )
                );
            } else {
                $PRODIGE->executeQuery(

                    "update " . $this->table . " set date_livraison=:date_livraison, commentaire=:commentaire,
                    nombre_ucd=:nombre_ucd, vaccin=:vaccin, finess_pharmacie_reference=:finess_pharmacie_reference, 
                    libelle_finess_pharmacie_reference=:libelle_finess_pharmacie_reference, _edit_datemaj=now() where gid=:gid",
                    array(
                        "gid" => $values[0],
                        "date_livraison" => $values[1],
                        "commentaire" => $values[3],
                        "nombre_ucd" => $values[4],
                        "vaccin" => $values[5],
                        "finess_pharmacie_reference" => $values[6],
                        "libelle_finess_pharmacie_reference" => $values[7],

                    )
                );
            }
        }
    }


    /**
     * Write in log file
     * @param logfile
     * @param $msgToLog
     * */
    private function logToFile($msgToLog)
    {
        $time = @date('[d/M/Y:H:i:s]');
        $fd = fopen($this->logFile, "a+");
        if (!$fd) {
            fwrite($fd, $time . " : Echec dans l'ouverture du fichier de log : " . $this->logFile . "\n");
        } else {
            $msgToWrite = $time . " : " . $msgToLog;
            fwrite($fd, $msgToWrite . "\n");
            fclose($fd);
        }
    }

    /**
     * Save CSV file
     */
    private function saveData($tabData)
    {
        $time = @date('_d_M_Y_H_i_s');
        $fd = fopen($this->directory . "/" . $this->archives . "/save_" . $time, "a+");
        if (!$fd) {
            echo "erreur dans la création du fichier de sauvegarde";
            die();
        } else {
            foreach ($tabData as $line) {
                fputcsv($fd, $line);
            }
            fclose($fd);
        }
    }

    /**
     * archive CSV file
     */
    private function archiveData($folder, $file)
    {
        if (file_exists($folder . $file)) {
            $time = @date('_d_M_Y_H_i_s');
            if (rename(
                $folder . $file,
                $this->directory . "/" . $this->archives . "/archive_" . $time . "_" . $file . "_" . uniqid()
            )) {
                return true;
            } else {
                echo "erreur dans l'archivage de " . $file;
                die();
            }
        }
    }


    /**
     * Detect extension in file
     * @param $csvFile
     * @return $Delimiter
     */
    private function detectExtension($csvFile)
    {
        return pathinfo($csvFile, PATHINFO_EXTENSION);
        return false;
    }


    /**
     * Detect delimiter in csv file
     * @param $csvFile
     * @return $Delimiter
     */
    private function detectDelimiter($csvFile)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );
        $handle = @fopen($csvFile, "r");
        if ($handle) {
            $firstLine = fgets($handle);
            fclose($handle);
            foreach ($delimiters as $delimiter => &$count) {
                $count = count(str_getcsv($firstLine, $delimiter));
            }
            return array_search(max($delimiters), $delimiters);
        }
        return false;
    }

    private function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Filter columns in csv File
     * @param $inputFile
     * @param $outputFile
     * */
    private function getUpdateData($inputFile)
    {
        $PRODIGE = $this->getDoctrine()->getConnection("prodige");
        $delimiter = $this->detectDelimiter($inputFile);
        if ($delimiter != ';') {
            $this->logToFile("fichier absent ou mauvais délimiteur csv " . $inputFile);
            return array();
        }

        $fileToRead = @fopen($inputFile, 'r');

        $data = array();
        $allowedVaccin = array("PFIZER", "MODERNA", "ASTRAZENECA", "JANSSEN");

        $line = 1;
        if ($fileToRead) {
            $this->logToFile("récupération des data de " . $inputFile);
            while (($row = fgets($fileToRead)) !== false) {
                $tabCsv = str_getcsv($row, $delimiter);
                if ($line == 1) {
                    if (count(
                            $tabCsv
                        ) != 8 || ($tabCsv[0] != "gid" && $tabCsv[1] != "date_livraison" && $tabCsv[2] != "lieu_gid" &&
                            $tabCsv[3] != "commentaire" && $tabCsv[4] != "nombre_ucd" && $tabCsv[5] != "vaccin" &&
                            $tabCsv[6] != "finess_pharmacie_reference" && $tabCsv[7] != "libelle_finess_pharmacie_reference")) {
                        $this->logToFile("nom ou nombre de colonnes incorect dans " . $inputFile);
                        return $data;
                    } else {
                        $line++;
                        continue;
                    }
                }

                if (count($tabCsv) !== 8) {
                    $this->logToFile("line " . $line . " : nombre de colonne incorrect ");
                    $line++;
                    continue;
                }

                if (intval($tabCsv[2]) === 0) {
                    $this->logToFile("line " . $line . " : gid doit être entier ");
                    $line++;
                    continue;
                }

                if (intval($tabCsv[0]) === 0 && $tabCsv[0] != "") {
                    $this->logToFile("line " . $line . " : gid entier ou vide ");
                    $line++;
                    continue;
                }

                if (!$this->validateDate($tabCsv[1])) {
                    $this->logToFile("line " . $line . " : date format invalide YY-MM-DDDD attendu ");
                    $line++;
                    continue;
                }

                if (!in_array($tabCsv[5], $allowedVaccin)) {
                    $this->logToFile(
                        "line " . $line . " : colonne vaccin doit être dans la liste des vaccins (" . implode(
                            ",",
                            $allowedVaccin
                        ) . ")"
                    );
                    $line++;
                    continue;
                }

                $sql = "SELECT structure_siren from covid_vaccin_lieu where gid=:gid";
                $tabSiren = $PRODIGE->fetchAll($sql, array("gid" => $tabCsv[2]));
                if (empty($tabSiren) || strval($tabSiren[0]["structure_siren"]) != strval(
                        basename($inputFile, ".csv")
                    )) {
                    $this->logToFile(
                        "line " . $line . " : gid non autorisé pour cette structure " . basename($inputFile, ".csv")
                    );
                    $line++;
                    continue;
                }


                $data[] = $tabCsv;

                $line++;
            }
        }

        return $data;
    }


    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     *
     * @throws \LogicException If DoctrineBundle is not available
     */
    private function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->container->get('doctrine');
    }

}

?>
