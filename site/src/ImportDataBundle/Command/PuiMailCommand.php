<?php

namespace Carmen\ImportDataBundle\Command;

use Carmen\ImportDataBundle\Controller\MapController;
use Carmen\ImportDataBundle\Services\Helpers;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PuiMailCommand extends Command
{
    protected $urlDirectoryCsv = '/var/www/html/sivaccin_pui/';
    protected $contactMail = 'contact@atlasante.fr';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('carmen:puimail')
            ->setDescription('Pui Mail');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Pui Mail");

        $conn = $this->getProdigeConnection();

        /** SQL sélection des données */
        $sql = "SELECT distinct covid_vaccin_stock_dm_historique_vue.*
                FROM covid_vaccin_stock_dm_historique_vue 
                inner join covid_vaccin_lieu on covid_vaccin_lieu.gid = covid_vaccin_stock_dm_historique_vue.lieu_gid
                inner join covid_vaccin_pui on covid_vaccin_pui.finess_pui = covid_vaccin_lieu.finess_pharmacie_reference
                where covid_vaccin_stock_dm_historique_vue.date_maj::date =  (NOW() - INTERVAL '1 DAY')::date";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll();

        $splitReference = array();

        /** SQL get emails */
        $sql = "SELECT email_contact_pui as email, finess_pui as finess FROM covid_vaccin_pui";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $emailTmps = $stmt->fetchAll();

        $emails = [];
        foreach ($emailTmps as $email) {
            if (isset($email['finess']) && isset($email['email'])) {
                $emails[$email['finess']][] = $email['email'];
            }
        }

        /** split des données en fonction des finess_pharmacie_reference */

        foreach ($data as $stock) {
            if (isset($stock['finess_pharmacie_reference'])) {
                if (!isset($splitReference[$stock['finess_pharmacie_reference']])) {
                    $splitReference[$stock['finess_pharmacie_reference']] = array();
                }

                $splitReference[$stock['finess_pharmacie_reference']][] = $stock;
            }
        }

        /** écriute des csv */
        foreach ($splitReference as $finesKey => $referenceData) {
            $filename = $this->urlDirectoryCsv . $finesKey . "_" . date("m_d_y") . '.csv';
            $fp = fopen($filename, 'w');
            $headerOk = false;

            foreach ($referenceData as $data) {
                if (!$headerOk) {
                    $headerOk = true;
                    fputcsv($fp, array_keys($data), ";");
                }
                fputcsv($fp, $data, ";");
            }
            fclose($fp);

            foreach ($emails[$finesKey] as $ind => $email) {
                $this->sendMail($email, $filename);
            }
        }
        return Command::SUCCESS;
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->container->get('doctrine')->getConnection();
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     * Envoi du fichier par mail au destinataire
     */
    protected function sendMail($to, $file)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Remontée des stocks des CV')
            ->setFrom($this->contactMail)
            ->setTo($to)
            ->setBody(
                "Bonjour,<br><br>
           Vous trouverez ci-joint la remontée hebdomadaire de stocks des centres de vaccination qui vous sont rattachés.<br><br>
           Cette remontée de stocks permet de vous assurer une visibilité globale des stocks de Dispositifs Médicaux sur votre territoire. Ces informations vous permettront d’ajuster votre distribution de dispositifs médicaux vers les centres de vaccination. <br><br>
           En cas de difficultés ou de questions, veuillez vous référer au mode d’emploi précédemment envoyé ou envoyez un mail à l’équipe Dispositifs Médicaux de Santé Publique France : <A HREF=\"mailto:dispositifs_medicaux@santepubliquefrance.fr\">dispositifs_medicaux@santepubliquefrance.fr</A>. <br><br>
           Bien cordialement,<br><br>
           L’Equipe AtlaSanté
           ",
                'text/html'
            )
            ->attach(\Swift_Attachment::fromPath($file));

        $this->container->get('mailer')->send($message);
    }
}
