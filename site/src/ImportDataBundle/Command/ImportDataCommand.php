<?php

namespace Carmen\ImportDataBundle\Command;

use Carmen\ImportDataBundle\Controller\ImportTaskController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportDataCommand extends Command
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user
        
        $this
            ->setName('carmen:importdata')
            ->setDescription('Import of GIS data in application')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        
        //$dialog = ($input->isInteractive() ? $this->getHelper('dialog') : null);
        
        $controller = new ImportTaskController();
        $controller->setContainer($this->container);
        $controller->taskImportAction();
        return Command::SUCCESS;
    }

}