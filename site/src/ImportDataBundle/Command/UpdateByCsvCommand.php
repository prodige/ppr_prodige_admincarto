<?php
namespace Carmen\ImportDataBundle\Command;

use Prodige\ProdigeBundle\Services\ConfigReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateByCsvCommand extends ContainerAwareCommand
{
    protected $debug = false;

    protected $field="rdv_site_web";
    protected $field_editeur_id="rdv_editeur_id";
    protected $directory ="/var/www/html/sivaccin";
    protected $table = "covid_vaccin_lieu";
    protected $tabImport = array("doctolib" => "import_rdv_url_doctolib.csv",
                                 "nehs" => "import_rdv_url_nehs.csv",
                                 "maiia" => "import_rdv_url_maiia.csv",
                                 "clickdoc" => "import_rdv_url_clickdoc.csv");

    protected $tabMapping = array("doctolib" => "mapping-gid-editeur.csv",
                                 "nehs" => "mapping-gid-editeur.csv",
                                 "maiia" => "mapping-gid-editeur.csv",
                                 "clickdoc" => "mapping-gid-editeur.csv");
    protected $logs = "logs";
    protected $archives = "archives";
    protected $logFile;


    /**
     * Configures the command definition
     */
    protected function configure()
    {

        $this
            ->setName('prodige:updateByCsv')
            ->setDescription('Update table by csv file')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $configReader = $container->get('prodige.configreader');
        $configReader instanceof ConfigReader;
        $PRODIGE = $this->getDoctrine()->getConnection("prodige");

        $output->setDecorated(true);

        $this->logFile = $this->directory."/".$this->logs."/update.log";

        if(!is_dir($this->directory."/".$this->logs)) {
            @mkdir($this->directory."/".$this->logs, 0770, true);
        }
        if(!is_dir($this->directory."/".$this->archives)) {
            @mkdir($this->directory."/".$this->archives, 0770, true);
        }

        $tabUpdates = array();
        //get updates
        foreach($this->tabImport as $folder => $fileName){
            $tabUpdates = array_merge($tabUpdates, $this->getUpdateData($this->directory."/".$folder."/".$fileName));
            //archive file
            $this->archiveData($this->directory."/".$folder."/", $fileName);
        }
        $tabUpdatesMapping = array();
        foreach($this->tabMapping as $folder => $fileName){
            $tabUpdatesMapping = array_merge($tabUpdatesMapping, $this->getUpdateData($this->directory."/".$folder."/".$fileName));
            //archive file
            $this->archiveData($this->directory."/".$folder."/", $fileName);
        }

        //save table data before update
        $tabSaveData = $PRODIGE->fetchAll("select  gid, ".$this->field." from ".$this->table);
        $this->saveData($tabSaveData);

        //update
        print_r($tabUpdates);
        foreach($tabUpdates as $key => $values){
            if(filter_var($values[0], FILTER_VALIDATE_INT)){
                $PRODIGE->executeQuery(
                    "update ".$this->table." set ".$this->field."=:value, _edit_datemaj=now() where gid=:gid", array(
                            "gid" => $values[0],
                            "value" => $values[1]
                    ));

            }
        }

        //update
        print_r($tabUpdatesMapping);
        foreach($tabUpdatesMapping as $key => $values){
            if(filter_var($values[0], FILTER_VALIDATE_INT)){
                $PRODIGE->executeQuery(
                    "update ".$this->table." set ".$this->field_editeur_id."=:value where gid=:gid", array(
                            "gid" => $values[0],
                            "value" => $values[1]
                    ));
            }

        }


    }


    /**
     * Write in log file
     * @param logfile
     * @param $msgToLog
     * */
    private function logToFile($msgToLog) {
        $time = @date('[d/M/Y:H:i:s]');
        $fd = fopen($this->logFile, "a+");
        if(!$fd) {
            fwrite($fd, $time . " : Echec dans l'ouverture du fichier de log : ".$this->logFile. "\n");
        }
        else {
            $msgToWrite = $time ." : ".$msgToLog;
            fwrite($fd, $msgToWrite . "\n");
            fclose($fd);
        }
    }

    /**
     * Save CSV file
     */
    private function saveData($tabData) {
        $time = @date('_d_M_Y_H_i_s');
        $fd = fopen($this->directory."/".$this->archives."/save_".$time, "a+");
        if(!$fd) {
            echo "erreur dans la création du fichier de sauvegarde";
            die();
        }
        else {
            foreach ($tabData as $line) {
                fputcsv($fd, $line);
            }
            fclose($fd);
        }
    }

    /**
     * archive CSV file
     */
    private function archiveData($folder, $file) {
        if(file_exists($folder.$file)){
            $time = @date('_d_M_Y_H_i_s');
            if(rename($folder.$file, $this->directory."/".$this->archives."/archive_".$time."_".$file."_".uniqid())){
                return true;
            }else{
                echo "erreur dans l'archivage de ".$file;die();
            }
        }

    }



   /**
    * Detect delimiter in csv file
    * @param $csvFile
    * @return $Delimiter
    */
    private function detectDelimiter($csvFile)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );
        $handle = @fopen($csvFile, "r");
        if($handle){
            $firstLine = fgets($handle);
            fclose($handle);
            foreach ($delimiters as $delimiter => &$count) {
                $count = count(str_getcsv($firstLine, $delimiter));
            }
            return array_search(max($delimiters), $delimiters);
        }
        return false;

    }

    /**
     * Filter columns in csv File
     * @param $inputFile
     * @param $outputFile
     * */
    private function getUpdateData($inputFile) {

        $delimiter = $this->detectDelimiter($inputFile);
        if($delimiter!=';'){
            $this->logToFile("fichier absent ou mauvais délimiteur csv ".$inputFile);
            return array();
        }

        $fileToRead = @fopen($inputFile, 'r');

        $data = array();

        if($fileToRead) {
            while(($row = fgets($fileToRead)) !== FALSE) {
                $tabCsv = str_getcsv($row, $delimiter);
                if(count($tabCsv)==2 && $tabCsv[0]!="gid"){
                    $data[] = $tabCsv;
                }
            }
            $this->logToFile("récupération des data de ".$inputFile);
        }

        return $data;

    }


    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     *
     * @throws \LogicException If DoctrineBundle is not available
     */
    private function getDoctrine()
    {
        if (!$this->getContainer()->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->getContainer()->get('doctrine');
    }

}
