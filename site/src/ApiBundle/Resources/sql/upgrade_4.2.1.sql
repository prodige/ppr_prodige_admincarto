update carmen.layer set layer_wmts_matrixids = replace(layer_wmts_matrixids, '0 1 10 11 12 13 14 15 16 17 18 19 2 20 21 3 4 5 6 7 8 9','0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21') where layer_type_code='WMTS';
update carmen.layer set layer_tiled_resolutions= replace(layer_tiled_resolutions, '104579.28102267528 52277.56058365798 102.07837190549107 51.03917245773825 25.51958285486731 12.759790583883195 6.379895081141485 3.189947487870714 1.5949737307853498 0.7974868621426733 0.39874343022133607 0.19937171496066794 26135.501191758423 0.09968585753033402 0.049842928715166976 13066.898437921349 6533.232132056948 3266.5612884048437 1633.2668865610428 816.6299959659598 408.314135166314 204.15685175554046','104579.2245498941 52277.53235379052 26135.487078595408 13066.891381800004 6533.228604113457 3266.559524462668 1633.266004597419 816.6295549860225 408.313914676836 204.15674151090207 102.07831678324084 51.03914489661119 25.519569074269395 12.759783693647506 6.379891635966492 3.18994576530532 1.594972869497728 0.7974864315474561 0.39874321490060405 0.19937160727567999 0.09968580369605201 0.049842901818919996') where layer_type_code='WMTS';


--
-- Name: map_onlineresources; Type: TABLE; Schema: carmen; Owner: user_prodige
--

CREATE TABLE carmen.map_onlineresources (
    map_onlineresource_id serial,
    map_onlineresource_name text NOT NULL,
    map_onlineresource_url text NOT NULL,
    map_onlineresource_category_name text NOT NULL,
    map_id integer
);

--
-- Name: TABLE map_onlineresources; Type: COMMENT; Schema: carmen; Owner: user_prodige
--

COMMENT ON TABLE carmen.map_onlineresources IS 'ressources associées ';


--
-- Name: COLUMN map_onlineresources.map_onlineresource_id; Type: COMMENT; Schema: carmen; Owner: user_prodige
--

COMMENT ON COLUMN carmen.map_onlineresources.map_onlineresource_id IS 'clé primaire';


--
-- Name: COLUMN map_onlineresources.map_onlineresource_name; Type: COMMENT; Schema: carmen; Owner: user_prodige
--

COMMENT ON COLUMN carmen.map_onlineresources.map_onlineresource_name IS 'nom de la ressource';


--
-- Name: COLUMN map_onlineresources.map_onlineresource_url; Type: COMMENT; Schema: carmen; Owner: user_prodige
--

COMMENT ON COLUMN carmen.map_onlineresources.map_onlineresource_url IS 'url de la ressource';


--
-- Name: COLUMN map_onlineresources.map_onlineresource_category_name; Type: COMMENT; Schema: carmen; Owner: user_prodige
--

COMMENT ON COLUMN carmen.map_onlineresources.map_onlineresource_category_name IS 'nom de la catégorie associée à la ressource';


--
-- Name: map_onlineresources map_onlineresources_pkey; Type: CONSTRAINT; Schema: carmen; Owner: user_prodige
--

ALTER TABLE ONLY carmen.map_onlineresources
    ADD CONSTRAINT map_onlineresources_pkey PRIMARY KEY (map_onlineresource_id);


--
-- Name: idx_onlineresource; Type: INDEX; Schema: carmen; Owner: user_prodige
--

CREATE INDEX idx_onlineresource ON carmen.map_onlineresources USING btree (map_id);


--
-- Name: map_onlineresources map_onlineresources_map_id_fkey; Type: FK CONSTRAINT; Schema: carmen; Owner: user_prodige
--

ALTER TABLE ONLY carmen.map_onlineresources
    ADD CONSTRAINT map_onlineresources_map_id_fkey FOREIGN KEY (map_id) REFERENCES carmen.map(map_id);

--
-- Name: map_onlineresource_id_seq; Type: SEQUENCE SET; Schema: carmen; Owner: user_prodige
--

ALTER TABLE carmen.map_locator ADD locator_map_search_engine BOOLEAN; 

ALTER TABLE carmen.layer ADD layer_default_background BOOLEAN;
 
ALTER TABLE carmen.layer ADD layer_background BOOLEAN; 

ALTER TABLE carmen.layer ADD layer_background_image TEXT; 

ALTER TABLE carmen.account_model ADD account_model_logo TEXT; 

-- migration des outils
delete from carmen.map_tool where tool_id in (select tool_id from  carmen.lex_tool where tool_identifier in ('pan', 'addLayerSOS'));
delete from carmen.lex_tool where tool_identifier in ('pan', 'addLayerSOS');

insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (TRUE, 'global_search', 'tool');
insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (FALSE, 'share', 'tool');
insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (FALSE, 'coordinates', 'tool');
insert into carmen.lex_tool (tool_default_enabled, tool_identifier, tool_xml_nodeprefix) values (FALSE, 'exportMultiple', 'tool');
insert into carmen.map_tool  (map_id, tool_id) select map_id, (select max(tool_id) from carmen.lex_tool) from carmen.map_tool where tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='exportPdf') and maptool_value ='predefinedModels';
delete from carmen.map_tool where (map_id, tool_id) in (select map_id, (select max(tool_id) from carmen.lex_tool) from carmen.map_tool where tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='exportPdf') and maptool_value ='predefinedModels');

update carmen.map_tool set maptool_value=null;

INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'point_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'line_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'polygon_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'text_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'snapping', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'move_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'modify_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'styles_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'export_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'buffer_from_selection_annotation', 'false', 'tool', null ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'buffer_all_annotation', 'false', 'tool', 'buffer_all_value_annotation' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'attribut_annotation', 'false', 'tool', 'attribut_value_annotation' ); 
INSERT INTO carmen.lex_tool( tool_identifier, tool_default_enabled, tool_xml_nodeprefix, tool_xml_nodevalue ) VALUES ( 'fullscreen', 'false', 'tool', null ); 

insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='point_annotation');
insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='line_annotation');
insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='polygon_annotation');
insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='text_annotation');
insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='modify_annotation');
insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='move_annotation');
insert into carmen.map_tool  (map_id, tool_id) select map_id, lex_tool.tool_id from carmen.map_tool, carmen.lex_tool  where map_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='annotation') and lex_tool.tool_id = (select tool_id from carmen.lex_tool where tool_identifier ='styles_annotation');

--suppresion des doublons
delete from carmen.map_tool where tool_id in (select t1.tool_id from carmen.lex_tool t1, carmen.lex_tool t2
WHERE t1.tool_id > t2.tool_id
AND t1.tool_identifier= t2.tool_identifier);

delete from carmen.lex_tool where tool_id in (select t1.tool_id from carmen.lex_tool t1, carmen.lex_tool t2
WHERE t1.tool_id > t2.tool_id
AND t1.tool_identifier= t2.tool_identifier);

-- Suppression des styles de copyright

ALTER TABLE carmen.map_ui DROP COLUMN ui_copyright_transparency; 
ALTER TABLE carmen.map_ui DROP COLUMN ui_copyright_font; 
ALTER TABLE carmen.map_ui DROP COLUMN ui_copyright_font_color; 
ALTER TABLE carmen.map_ui DROP COLUMN ui_copyright_font_size; 
ALTER TABLE carmen.map_ui DROP COLUMN ui_copyright_background_color; 

-- migration des themes
update carmen.map_ui set ui_color_id = (select color_id from carmen.lex_color_id where color_key='grey') where ui_color_id = (select color_id from carmen.lex_color_id where color_key='black') ;
update carmen.map_ui set ui_color_id = (select color_id from carmen.lex_color_id where color_key='green') where ui_color_id = (select color_id from carmen.lex_color_id where color_key='olive') ;
update carmen.lex_color_id set color_key ='red', color_name='rouge' where color_key='purple';
delete from carmen.lex_color_id where color_key 	in ('black', 'olive');


-- migration des chemins
update carmen.map_ui set help_message = replace(help_message, '/IHM/IHM/', '/IHM/');

-- migration modeles
update carmen.account_model set account_model_logo='modele1.png' where account_model_file='modele1.tpl';
update carmen.account_model set account_model_logo='modele2.png' where account_model_file='modele2.tpl';
update carmen.account_model set account_model_logo='modele3.png' where account_model_file='modele3.tpl';
update carmen.account_model set account_model_logo='modele4.png' where account_model_file='modele4.tpl';


--appli metier extension type
CREATE DOMAIN email AS text
  CHECK ( value ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$' );

CREATE DOMAIN phone_number AS text
  --CHECK ( value ~ '(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$' );
  CHECK ( value ~ '^(\+33|0)[1-9](\d\d){4}$' );

-- migration entêtes = champs (ne pas appliquer à CARMEN)
update carmen.field set field_headers = field_queries ; 