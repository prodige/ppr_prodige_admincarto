/*SCRIPT PLAYED BY PHP COMMAND*/

drop function if exists updateViewsGeometryColumns();
create or replace function updateViewsGeometryColumns() returns setof record as
$updateViewsGeometryColumns$
declare
  r_tables record;
  r_views record;
  srid int;
  geometrytype text;
  func text[];
begin
  for r_tables in 
    select geo.f_table_schema as table_schema, geo.f_table_name as table_name, geo.f_geometry_column as column_name
    from public.geometry_columns geo
    left join information_schema.views on (geo.f_table_schema=views.table_schema and geo.f_table_name=views.table_name) 
    where views.table_name is null and (geo.type='GEOMETRY' or geo.srid=0)
  loop
    srid := null;
    geometrytype := null;
    EXECUTE 
      $$ select distinct geometrytype $$||
      $$, srid $$||
      $$, (case when geometrytype like 'MULTI%' then 0 when geometrytype like '%POLYGON' then 1 else 2 end) as priority $$||
      $$ from (select distinct public.geometrytype($$||r_tables.column_name||$$) as geometrytype $$||
      $$, public.st_srid($$||r_tables.column_name||$$) as srid $$||
      $$ from $$||r_tables.table_schema||$$."$$||r_tables.table_name||$$") foo $$||
      $$ where geometrytype is not null $$ ||
      $$ order by priority limit 1 $$
    INTO geometrytype, srid;

    geometrytype := upper(geometrytype);
    if ( not(geometrytype like 'MULTI%') ) then
      geometrytype := 'MULTI'||geometrytype;
    end if ;

    if ( srid is not null and geometrytype is not null and srid<>0 and geometrytype<>'GEOMETRY' ) then
      for r_views in 
        select vtu.*
        from information_schema.view_table_usage vtu 
        where vtu.table_schema=r_tables.table_schema and vtu.table_name=r_tables.table_name
      loop
        return query select (r_tables.table_schema||'."'||r_tables.table_name||'"')::text
        , ('drop view if exists '||r_views.view_schema||'."'||r_views.view_name||'" cascade;')::text;
      end loop;

      func := null::text[];
      if ( geometrytype like 'MULTI%' ) then
        func := func || ARRAY['public.st_multi'];
      end if;

      return query select (r_tables.table_schema||'."'||r_tables.table_name||'"')::text
      , ('alter table '||r_tables.table_schema||'."'||r_tables.table_name||'" alter column '||r_tables.column_name||' type geometry('||geometrytype||', '||srid||')'||(case when coalesce(array_length(func,1), 0)>0 then ' using '||array_to_string(func, '(')||'('||r_tables.column_name||repeat(')', array_length(func,1)) else '' end))::text;

      for r_views in 
        with recursive usages(view_order, view_schema, view_name, view_definition) as (
          select 0 as view_order, vtu.view_schema, vtu.view_name, viewdef.view_definition
          from information_schema.view_table_usage vtu 
          inner join information_schema.views viewdef on (vtu.view_schema=viewdef.table_schema and vtu.view_name=viewdef.table_name)
          where vtu.table_schema=r_tables.table_schema and vtu.table_name=r_tables.table_name

          union all

          select view_order+1 as view_order, vtu.view_schema, vtu.view_name, viewdef.view_definition
          from usages
          inner join information_schema.view_table_usage vtu on (vtu.table_schema=usages.view_schema and vtu.table_name=usages.view_name)
          inner join information_schema.views viewdef on (vtu.view_schema=viewdef.table_schema and vtu.view_name=viewdef.table_name) 
        )
        select * from usages order by view_order
      loop
        return query select (r_tables.table_schema||'."'||r_tables.table_name||'"')::text
        , ('create or replace view '||r_views.view_schema||'."'||r_views.view_name||'" as '||r_views.view_definition)::text;
      end loop;
    end if;

  end loop;
end;
$updateViewsGeometryColumns$ language plpgsql;


/*SCRIPT PLAYED BY PHP COMMAND*/
