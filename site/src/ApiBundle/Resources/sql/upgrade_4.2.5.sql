 -- Creation schema dans prodige
CREATE SCHEMA bdterr;

-- Ajout table de mapping insee pour les lots (dans le schema bdterr de prodige)
CREATE TABLE bdterr.bdterr_mapping (
    mapping_lot_id integer NOT NULL,
    mapping_object_id integer NOT NULL,
    mapping_insee text NOT NULL
);

ALTER TABLE ONLY bdterr.bdterr_mapping
    ADD CONSTRAINT bdterr_mapping_pkey PRIMARY KEY (mapping_lot_id, mapping_object_id, mapping_insee);