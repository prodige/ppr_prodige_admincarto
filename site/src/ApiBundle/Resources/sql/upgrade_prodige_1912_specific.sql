drop table carmen.wmsc;
alter table carmen.layer add column layer_tiled_layer  text;
alter table carmen.layer add column layer_tiled_server_url  text;
alter table carmen.layer rename column layer_wmts_resolution to layer_tiled_resolutions ;
alter table carmen.layer add column layer_wmsc_boundingbox text;