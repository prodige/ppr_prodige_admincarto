-- todo regenerate context
update carmen.map set map_refmap_image = replace(map_refmap_image, '.././reference', './reference') ;
update carmen.map set map_refmap_image = replace(map_refmap_image, '../.././reference', './reference') ;
update carmen.map set map_refmap_image = replace(map_refmap_image, '../../reference', './reference') ;
update carmen.map set map_refmap_image = replace(map_refmap_image, '../reference', './reference') ;
update carmen.layer set "layer_server_url"= replace("layer_server_url", 'http://georef.application.developpement-durable.gouv.fr/cartes/mapserv?', 'https://georef.application.developpement-durable.gouv.fr/cartes/mapserv?') where layer_server_url ='http://georef.application.developpement-durable.gouv.fr/cartes/mapserv?';

ALTER TABLE carmen.map_ui ADD help_display bool DEFAULT false;
ALTER TABLE carmen.map_ui ADD help_message text;
