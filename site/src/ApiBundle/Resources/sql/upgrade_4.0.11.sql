insert into carmen.lex_tool (tool_identifier)values('addLayerSOS');


INSERT INTO carmen.lex_wxs_type (wxs_type_name) VALUES ('SOS');
INSERT into carmen.wxs (wxs_type_id, account_id, wxs_name, wxs_rank, wxs_url)
select wxs_type_id, 1, 'BRGM - Capteurs piézométriques', 1, 'http://ressource.brgm-rec.fr/service/sosRawPiezo/service=SOS' from carmen.lex_wxs_type where wxs_type_name='SOS';
INSERT into carmen.wxs (wxs_type_id, account_id, wxs_name, wxs_rank, wxs_url)
select wxs_type_id, 1, 'Alkante - Capteurs énérgétiques - Saint Sulpice la forêt', 2, 'https://sensorodp3.alkante.com/sos3/service?' from carmen.lex_wxs_type where wxs_type_name='SOS';
