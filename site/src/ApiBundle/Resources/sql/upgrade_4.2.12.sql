-- Ajout des indicatifs outre mer
ALTER DOMAIN phone_number drop constraint phone_number_check;
ALTER DOMAIN phone_number add constraint phone_number_check CHECK ( value ~ '^(\+33|\+590|\+594|\+262|\+596|\+269|\+687|\+689|\+590|\+508|\+681)(\d){6,9}$' );