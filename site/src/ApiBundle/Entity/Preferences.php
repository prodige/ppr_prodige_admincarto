<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Preferences
 *
 * @ORM\Table(name="carmen.preferences", uniqueConstraints={@ORM\UniqueConstraint(name="idx_preferences", columns={"user_id"})})
 * @ORM\Entity
 *
 * @ExclusionPolicy("none")
 */
class Preferences
{
    /**
     * @var integer
     *
     * @ORM\Column(name="preference_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.preferences_preference_id_seq", allocationSize=1, initialValue=1)
     */
    private $preferenceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="preference_srs", type="integer", nullable=true)
     */
    private $preferenceSrs;

    /**
     * @var integer
     *
     * @ORM\Column(name="preference_minscale", type="integer", nullable=true)
     */
    private $preferenceMinscale;

    /**
     * @var integer
     *
     * @ORM\Column(name="preference_maxscale", type="integer", nullable=true)
     */
    private $preferenceMaxscale;

    /**
     * @var string
     *
     * @ORM\Column(name="preference_outputformat", type="string", length=10, nullable=true)
     */
    private $preferenceOutputformat;

    /**
     * @var string
     *
     * @ORM\Column(name="preference_units", type="string", length=3, nullable=true)
     */
    private $preferenceUnits;

    /**
     * @var string
     *
     * @ORM\Column(name="preference_background_color", type="string", length=7, nullable=true)
     */
    private $preferenceBackgroundColor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="preference_background_transparency", type="boolean", nullable=true)
     */
    private $preferenceBackgroundTransparency;

    /**
     * @var float
     *
     * @ORM\Column(name="preferences_extent_xmin", type="float", precision=10, scale=0, nullable=true)
     */
    private $preferencesExtentXmin;

    /**
     * @var float
     *
     * @ORM\Column(name="preferences_extent_ymin", type="float", precision=10, scale=0, nullable=true)
     */
    private $preferencesExtentYmin;

    /**
     * @var float
     *
     * @ORM\Column(name="preferences_extent_xmax", type="float", precision=10, scale=0, nullable=true)
     */
    private $preferencesExtentXmax;

    /**
     * @var float
     *
     * @ORM\Column(name="preferences_extent_ymax", type="float", precision=10, scale=0, nullable=true)
     */
    private $preferencesExtentYmax;

    /**
     * @var \Users
     *
     * @ORM\OneToOne(targetEntity="Users", inversedBy="preferences")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     * @Exclude
     */
    private $user;


    /**
     * Set preferenceId
     *
     * @param integer $preferenceId
     * @return Preferences
     */
    public function setPreferenceId($preferenceId)
    {
        $this->preferenceId = $preferenceId;

        return $this;
    }

    /**
     * Get preferenceId
     *
     * @return integer
     */
    public function getPreferenceId()
    {
        return $this->preferenceId;
    }

    /**
     * Set preferenceSrs
     *
     * @param integer $preferenceSrs
     * @return Preferences
     */
    public function setPreferenceSrs($preferenceSrs)
    {
        $this->preferenceSrs = $preferenceSrs;

        return $this;
    }

    /**
     * Get preferenceSrs
     *
     * @return integer
     */
    public function getPreferenceSrs()
    {
        return $this->preferenceSrs;
    }

    /**
     * Set preferenceMinscale
     *
     * @param integer $preferenceMinscale
     * @return Preferences
     */
    public function setPreferenceMinscale($preferenceMinscale)
    {
        $this->preferenceMinscale = $preferenceMinscale;

        return $this;
    }

    /**
     * Get preferenceMinscale
     *
     * @return integer
     */
    public function getPreferenceMinscale()
    {
        return $this->preferenceMinscale;
    }

    /**
     * Set preferenceMaxscale
     *
     * @param integer $preferenceMaxscale
     * @return Preferences
     */
    public function setPreferenceMaxscale($preferenceMaxscale)
    {
        $this->preferenceMaxscale = $preferenceMaxscale;

        return $this;
    }

    /**
     * Get preferenceMaxscale
     *
     * @return integer
     */
    public function getPreferenceMaxscale()
    {
        return $this->preferenceMaxscale;
    }

    /**
     * Set preferenceOutputformat
     *
     * @param string $preferenceOutputformat
     * @return Preferences
     */
    public function setPreferenceOutputformat($preferenceOutputformat)
    {
        $this->preferenceOutputformat = $preferenceOutputformat;

        return $this;
    }

    /**
     * Get preferenceOutputformat
     *
     * @return string
     */
    public function getPreferenceOutputformat()
    {
        return $this->preferenceOutputformat;
    }

    /**
     * Set preferenceUnits
     *
     * @param string $preferenceUnits
     * @return Preferences
     */
    public function setPreferenceUnits($preferenceUnits)
    {
        $this->preferenceUnits = $preferenceUnits;

        return $this;
    }

    /**
     * Get preferenceUnits
     *
     * @return string
     */
    public function getPreferenceUnits()
    {
        return $this->preferenceUnits;
    }

    /**
     * Set preferenceBackgroundColor
     *
     * @param string $preferenceBackgroundColor
     * @return Preferences
     */
    public function setPreferenceBackgroundColor($preferenceBackgroundColor)
    {
        $this->preferenceBackgroundColor = $preferenceBackgroundColor;

        return $this;
    }

    /**
     * Get preferenceBackgroundColor
     *
     * @return string
     */
    public function getPreferenceBackgroundColor()
    {
        return $this->preferenceBackgroundColor;
    }

    /**
     * Set preferenceBackgroundTransparency
     *
     * @param boolean $preferenceBackgroundTransparency
     * @return Preferences
     */
    public function setPreferenceBackgroundTransparency($preferenceBackgroundTransparency)
    {
        $this->preferenceBackgroundTransparency = $preferenceBackgroundTransparency;

        return $this;
    }

    /**
     * Get preferenceBackgroundTransparency
     *
     * @return boolean
     */
    public function getPreferenceBackgroundTransparency()
    {
        return $this->preferenceBackgroundTransparency;
    }

    /**
     * Set preferencesExtentXmin
     *
     * @param float $preferencesExtentXmin
     * @return Preferences
     */
    public function setPreferencesExtentXmin($preferencesExtentXmin)
    {
        $this->preferencesExtentXmin = $preferencesExtentXmin;

        return $this;
    }

    /**
     * Get preferencesExtentXmin
     *
     * @return float
     */
    public function getPreferencesExtentXmin()
    {
        return $this->preferencesExtentXmin;
    }

    /**
     * Set preferencesExtentYmin
     *
     * @param float $preferencesExtentYmin
     * @return Preferences
     */
    public function setPreferencesExtentYmin($preferencesExtentYmin)
    {
        $this->preferencesExtentYmin = $preferencesExtentYmin;

        return $this;
    }

    /**
     * Get preferencesExtentYmin
     *
     * @return float
     */
    public function getPreferencesExtentYmin()
    {
        return $this->preferencesExtentYmin;
    }

    /**
     * Set preferencesExtentXmax
     *
     * @param float $preferencesExtentXmax
     * @return Preferences
     */
    public function setPreferencesExtentXmax($preferencesExtentXmax)
    {
        $this->preferencesExtentXmax = $preferencesExtentXmax;

        return $this;
    }

    /**
     * Get preferencesExtentXmax
     *
     * @return float
     */
    public function getPreferencesExtentXmax()
    {
        return $this->preferencesExtentXmax;
    }

    /**
     * Set preferencesExtentYmax
     *
     * @param float $preferencesExtentYmax
     * @return Preferences
     */
    public function setPreferencesExtentYmax($preferencesExtentYmax)
    {
        $this->preferencesExtentYmax = $preferencesExtentYmax;

        return $this;
    }

    /**
     * Get preferencesExtentYmax
     *
     * @return float
     */
    public function getPreferencesExtentYmax()
    {
        return $this->preferencesExtentYmax;
    }

    /**
     * Set user
     *
     * @param \Carmen\ApiBundle\Entity\Users $user
     * @return Preferences
     */
    public function setUser(\Carmen\ApiBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Carmen\ApiBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
