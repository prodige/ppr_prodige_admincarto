<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexColorId
 *
 * @ORM\Table(name="carmen.lex_color_id")
 * @ORM\Entity
 */
class LexColorId
{
    /**
     * @var integer
     *
     * @ORM\Column(name="color_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_color_id_color_id_seq", allocationSize=1, initialValue=1)
     */
    private $colorId;

    /**
     * @var string
     *
     * @ORM\Column(name="color_name", type="string", length=100, nullable=true)
     */
    private $colorName;

    /**
     * @var string
     *
     * @ORM\Column(name="color_key", type="string", length=100, nullable=true)
     */
    private $colorKey;


    /**
     * Get colorId
     *
     * @return integer
     */
    public function getColorId()
    {
        return $this->colorId;
    }

    /**
     * Set colorName
     *
     * @param string $colorName
     * @return LexColorId
     */
    public function setColorName($colorName)
    {
        $this->colorName = $colorName;

        return $this;
    }

    /**
     * Get colorName
     *
     * @return string
     */
    public function getColorName()
    {
        return $this->colorName;
    }

    /**
     * Set colorKey
     *
     * @param string $colorKey
     * @return LexColorId
     */
    public function setColorKey($colorKey)
    {
        $this->colorKey = $colorKey;

        return $this;
    }

    /**
     * Get colorKey
     *
     * @return string
     */
    public function getColorKey()
    {
        return $this->colorKey;
    }
}
