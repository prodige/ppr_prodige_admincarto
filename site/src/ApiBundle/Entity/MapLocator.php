<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MapLocator
 *
 * @ORM\Table(name="carmen.map_locator", indexes={@ORM\Index(name="idx_map_locator", columns={"locator_criteria_related"}), @ORM\Index(name="idx_map_locator_0", columns={"map_id"})})
 * @ORM\Entity
 */
class MapLocator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="locator_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.map_locator_locator_id_seq", allocationSize=1, initialValue=1)
     */
    private $locatorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="locator_criteria_rank", type="integer", nullable=true)
     */
    private $locatorCriteriaRank;

    /**
     * @var string
     *
     * @ORM\Column(name="locator_criteria_name", type="string", length=255, nullable=true)
     */
    private $locatorCriteriaName;

    /**
     * @var integer
     *
     * @ORM\Column(name="locator_criteria_layer_id", type="integer", nullable=true)
     */
    private $locatorCriteriaLayerId;

    /**
     * @var string
     *
     * @ORM\Column(name="locator_criteria_field_id", type="string", length=255, nullable=true)
     */
    private $locatorCriteriaFieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="locator_criteria_field_name", type="string", length=255, nullable=true)
     */
    private $locatorCriteriaFieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="locator_criteria_related_field", type="string", length=255, nullable=true)
     */
    private $locatorCriteriaRelatedField;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locator_criteria_visibility", type="boolean", nullable=true)
     */
    private $locatorCriteriaVisibility;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locator_map_search_engine", type="boolean", nullable=true)
     */
    private $locatorInGlobalMapSearchEngine;

    /**
     * @var \MapLocator
     *
     * ON DELETE SET NULL must be set on this foreign key in databse
     *
     * @ORM\ManyToOne(targetEntity="MapLocator", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="locator_criteria_related", referencedColumnName="locator_id", onDelete="SET NULL")
     * })
     */
    private $locatorCriteriaRelated;

    /**
     * @var \Map
     *
     * @ORM\ManyToOne(targetEntity="Map", inversedBy="locators")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;


    /**
     *
     * Set locator Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\MapLocator
     */
    public function setId($id)
    {
        $this->locatorId = $id;

        return $this;
    }


    /**
     * Get locatorId
     *
     * @return integer
     */
    public function getLocatorId()
    {
        return $this->locatorId;
    }

    /**
     * Set locatorCriteriaRank
     *
     * @param integer $locatorCriteriaRank
     * @return MapLocator
     */
    public function setLocatorCriteriaRank($locatorCriteriaRank)
    {
        $this->locatorCriteriaRank = $locatorCriteriaRank;

        return $this;
    }

    /**
     * Get locatorCriteriaRank
     *
     * @return integer
     */
    public function getLocatorCriteriaRank()
    {
        return $this->locatorCriteriaRank;
    }

    /**
     * Set locatorCriteriaName
     *
     * @param string $locatorCriteriaName
     * @return MapLocator
     */
    public function setLocatorCriteriaName($locatorCriteriaName)
    {
        $this->locatorCriteriaName = $locatorCriteriaName;

        return $this;
    }

    /**
     * Get locatorCriteriaName
     *
     * @return string
     */
    public function getLocatorCriteriaName()
    {
        return $this->locatorCriteriaName;
    }

    /**
     * Set locatorCriteriaLayerId
     *
     * @param integer $locatorCriteriaLayerId
     * @return MapLocator
     */
    public function setLocatorCriteriaLayerId($locatorCriteriaLayerId)
    {
        $this->locatorCriteriaLayerId = $locatorCriteriaLayerId;

        return $this;
    }

    /**
     * Get locatorCriteriaLayerId
     *
     * @return integer
     */
    public function getLocatorCriteriaLayerId()
    {
        return $this->locatorCriteriaLayerId;
    }

    /**
     * Set locatorCriteriaFieldId
     *
     * @param string $locatorCriteriaFieldId
     * @return MapLocator
     */
    public function setLocatorCriteriaFieldId($locatorCriteriaFieldId)
    {
        $this->locatorCriteriaFieldId = $locatorCriteriaFieldId;

        return $this;
    }

    /**
     * Get locatorCriteriaFieldId
     *
     * @return string
     */
    public function getLocatorCriteriaFieldId()
    {
        return $this->locatorCriteriaFieldId;
    }

    /**
     * Set locatorCriteriaFieldName
     *
     * @param string $locatorCriteriaFieldName
     * @return MapLocator
     */
    public function setLocatorCriteriaFieldName($locatorCriteriaFieldName)
    {
        $this->locatorCriteriaFieldName = $locatorCriteriaFieldName;

        return $this;
    }

    /**
     * Get locatorCriteriaFieldName
     *
     * @return string
     */
    public function getLocatorCriteriaFieldName()
    {
        return $this->locatorCriteriaFieldName;
    }

    /**
     * Set locatorCriteriaRelatedField
     *
     * @param string $locatorCriteriaRelatedField
     * @return MapLocator
     */
    public function setLocatorCriteriaRelatedField($locatorCriteriaRelatedField)
    {
        $this->locatorCriteriaRelatedField = $locatorCriteriaRelatedField;

        return $this;
    }

    /**
     * Get locatorCriteriaRelatedField
     *
     * @return string
     */
    public function getLocatorCriteriaRelatedField()
    {
        return $this->locatorCriteriaRelatedField;
    }

    /**
     * Set locatorCriteriaVisibility
     *
     * @param boolean $locatorCriteriaVisibility
     * @return MapLocator
     */
    public function setLocatorCriteriaVisibility($locatorCriteriaVisibility)
    {
        $this->locatorCriteriaVisibility = $locatorCriteriaVisibility;

        return $this;
    }

    /**
     * Get locatorCriteriaVisibility
     *
     * @return boolean
     */
    public function getLocatorCriteriaVisibility()
    {
        return $this->locatorCriteriaVisibility;
    }


    /**
     * Set locatorCriteriaVisibility
     *
     * @param boolean $locatorInGlobalMapSearchEngine
     * @return MapLocator
     */
    public function setlocatorInGlobalMapSearchEngine($locatorInGlobalMapSearchEngine)
    {
        $this->locatorInGlobalMapSearchEngine = $locatorInGlobalMapSearchEngine;

        return $this;
    }

    /**
     * Get locatorInGlobalMapSearchEngine
     *
     * @return boolean
     */
    public function getlocatorInGlobalMapSearchEngine()
    {
        return $this->locatorInGlobalMapSearchEngine;
    }

    /**
     * Set locatorCriteriaRelated
     *
     * @param \Carmen\ApiBundle\Entity\MapLocator $locatorCriteriaRelated
     * @return MapLocator
     */
    public function setLocatorCriteriaRelated(\Carmen\ApiBundle\Entity\MapLocator $locatorCriteriaRelated = null)
    {
        $this->locatorCriteriaRelated = $locatorCriteriaRelated;

        return $this;
    }

    /**
     * Get locatorCriteriaRelated
     *
     * @return \Carmen\ApiBundle\Entity\MapLocator
     */
    public function getLocatorCriteriaRelated()
    {
        return $this->locatorCriteriaRelated;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return MapLocator
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }
}
