<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapTree
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.map_tree", indexes={@ORM\Index(name="idx_map_tree", columns={"map_id"})})
 * @ORM\Entity(repositoryClass="Carmen\ApiBundle\Repository\MapTreeRepository")
 */
class MapTree
{
    /**
     * @var integer
     *
     * @ORM\Column(name="node_id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $nodeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="node_parent", type="integer", nullable=true)
     */
    private $nodeParent;

    /**
     * @var integer
     *
     * @ORM\Column(name="node_pos", type="integer", nullable=false)
     */
    private $nodePos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="node_opened", type="boolean", nullable=false)
     */
    private $nodeOpened;

    /**
     * @var boolean
     *
     * @ORM\Column(name="node_is_layer", type="boolean", nullable=false)
     */
    private $nodeIsLayer;

    /**
     * @var \Map
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;


    /**
     *
     * Set node Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\MapTree
     */
    public function setId($id)
    {
        $this->nodeId = $id;

        return $this;
    }


    /**
     * Get nodeId
     *
     * @return integer
     */
    public function getNodeId()
    {
        return $this->nodeId;
    }

    /**
     * Set nodeParent
     *
     * @param integer $nodeParent
     * @return MapTree
     */
    public function setNodeParent($nodeParent)
    {
        $this->nodeParent = $nodeParent;

        return $this;
    }

    /**
     * Get nodeParent
     *
     * @return integer
     */
    public function getNodeParent()
    {
        return $this->nodeParent;
    }

    /**
     * Set nodePos
     *
     * @param integer $nodePos
     * @return MapTree
     */
    public function setNodePos($nodePos)
    {
        $this->nodePos = $nodePos;

        return $this;
    }

    /**
     * Get nodePos
     *
     * @return integer
     */
    public function getNodePos()
    {
        return $this->nodePos;
    }

    /**
     * Set nodeOpened
     *
     * @param boolean $nodeOpened
     * @return MapTree
     */
    public function setNodeOpened($nodeOpened)
    {
        $this->nodeOpened = $nodeOpened;

        return $this;
    }

    /**
     * Get nodeOpened
     *
     * @return boolean
     */
    public function getNodeOpened()
    {
        return $this->nodeOpened;
    }

    /**
     * Set nodeIsLayer
     *
     * @param boolean $nodeIsLayer
     * @return MapTree
     */
    public function setNodeIsLayer($nodeIsLayer)
    {
        $this->nodeIsLayer = $nodeIsLayer;

        return $this;
    }

    /**
     * Get nodeIsLayer
     *
     * @return boolean
     */
    public function getNodeIsLayer()
    {
        return $this->nodeIsLayer;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return MapTree
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }
}
