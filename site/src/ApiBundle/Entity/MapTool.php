<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapTool
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.map_tool", indexes={@ORM\Index(name="idx_map_tools", columns={"map_id"}), @ORM\Index(name="idx_map_tools_0", columns={"tool_id"})})
 * @ORM\Entity
 */
class MapTool
{

    /**
     * @var Map
     *
     * @Exclude
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Map", inversedBy="tools")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;

    /**
     * @var \LexTool
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="LexTool")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tool_id", referencedColumnName="tool_id")
     * })
     */
    private $tool;

    /**
     * @var string
     *
     * @ORM\Column(name="maptool_value", type="string", nullable=true)
     */
    private $maptoolValue;

    /**
     * Set map
     *
     * @param Map $map
     * @return MapTool
     */
    public function setMap(Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set tool
     *
     * @param \Carmen\ApiBundle\Entity\LexTool $tool
     * @return MapTool
     */
    public function setTool(\Carmen\ApiBundle\Entity\LexTool $tool = null)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get tool
     *
     * @return \Carmen\ApiBundle\Entity\LexTool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set maptoolValue
     *
     * @param string $maptoolValue
     * @return MapTool
     */
    public function setMaptoolValue($maptoolValue)
    {
        $this->maptoolValue = $maptoolValue;

        return $this;
    }

    /**
     * Get maptoolValue
     *
     * @return string
     */
    public function getMaptoolValue()
    {
        return $this->maptoolValue;
    }

}
