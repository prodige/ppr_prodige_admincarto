<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexLayerType
 *
 * @ORM\Table(name="carmen.lex_layer_type")
 * @ORM\Entity
 */
class LexLayerType
{
    const VECTOR = "VECTOR";
    const RASTER = "RASTER";
    const WMS = "WMS";
    const WFS = "WFS";
    const WMSC = "WMSC";
    const WMTS = "WMTS";
    const POSTGIS = "POSTGIS";
    const GEOJSON = "GEOJSON";
    const TILE_LAYER = "TILE_LAYER";

    /**
     * @var integer
     *
     * @ORM\Column(name="layer_type_code", type="string", length=20, nullable=false)
     * @ORM\Id
     */
    private $layerTypeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_type_name", type="string", length=255, nullable=true)
     */
    private $layerTypeName;


    /**
     * Get layerTypeCode
     *
     * @return integer
     */
    public function getLayerTypeCode()
    {
        return $this->layerTypeCode;
    }

    /**
     * Set layerTypeName
     *
     * @param string $layerTypeName
     * @return LexAnalyseType
     */
    public function setLayerTypeName($layerTypeName)
    {
        $this->layerTypeName = $layerTypeName;

        return $this;
    }

    /**
     * Get layerTypeName
     *
     * @return string
     */
    public function getLayerTypeName()
    {
        return $this->layerTypeName;
    }
}
