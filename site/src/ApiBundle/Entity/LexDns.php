<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexDns
 *
 * @ORM\Table(name="carmen.lex_dns")
 * @ORM\Entity
 */
class LexDns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="dns_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_dns_dns_id_seq", allocationSize=1, initialValue=1)
     */
    private $dnsId;

    /**
     * @var string
     *
     * @ORM\Column(name="dns_url", type="string", length=255, nullable=true)
     */
    private $dnsUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="dns_prefix_backoffice", type="string", length=150, nullable=true)
     */
    private $dnsPrefixBackoffice;

    /**
     * @var string
     *
     * @ORM\Column(name="dns_prefix_frontoffice", type="string", length=150, nullable=true)
     */
    private $dnsPrefixFrontoffice;

    /**
     * @var string
     *
     * @ORM\Column(name="dns_prefix_data", type="string", length=150, nullable=true)
     */

    private $dnsPrefixData;

    /**
     * @var string
     *
     * @ORM\Column(name="dns_prefix_metadata", type="string", length=150, nullable=true)
     */
    private $dnsPrefixMetadata;

    /**
     * @var string
     *
     * @ORM\Column(name="dns_scheme", type="string", length=10, nullable=true, options={"default"="http"})
     */
    private $dnsScheme;


    /**
     * Get dnsId
     *
     * @return integer
     */
    public function getDnsId()
    {
        return $this->dnsId;
    }

    /**
     * Set dnsUrl
     *
     * @param string $dnsUrl
     * @return LexDns
     */
    public function setDnsUrl($dnsUrl)
    {
        $this->dnsUrl = $dnsUrl;

        return $this;
    }

    /**
     * Get dnsUrl
     *
     * @return string
     */
    public function getDnsUrl()
    {
        return $this->dnsUrl;
    }

    /**
     * @return string
     */
    public function getDnsPrefixBackoffice()
    {
        return $this->dnsPrefixBackoffice;
    }

    /**
     * @param string $dnsPrefixBackoffice
     * @return \Carmen\ApiBundle\Entity\LexDns
     */
    public function setDnsPrefixBackoffice($dnsPrefixBackoffice)
    {
        $this->dnsPrefixBackoffice = $dnsPrefixBackoffice;

        return $this;
    }

    /**
     * @return string
     */
    public function getDnsPrefixFrontoffice()
    {
        return $this->dnsPrefixFrontoffice;
    }

    /**
     * @param string $dnsPrefix
     * @return \Carmen\ApiBundle\Entity\LexDns
     */
    public function setDnsPrefixFrontoffice($dnsPrefixFrontoffice)
    {
        $this->dnsPrefixFrontoffice = $dnsPrefixFrontoffice;

        return $this;
    }

    /**
     * @return string
     */
    public function getDnsPrefixData()
    {
        return $this->dnsPrefixData;
    }

    /**
     * @param string $dnsPrefixData
     * @return \Carmen\ApiBundle\Entity\LexDns
     */
    public function setDnsPrefixData($dnsPrefixData)
    {
        $this->dnsPrefixData = $dnsPrefixData;

        return $this;
    }

    /**
     * @return string
     */
    public function getDnsPrefixMetadata()
    {
        return $this->dnsPrefixMetadata;
    }

    /**
     * @param unknown $dnsPrefixMetadata
     * @return \Carmen\ApiBundle\Entity\LexDns
     */
    public function setDnsPrefixMetadata($dnsPrefixMetadata)
    {
        $this->dnsPrefixMetadata = $dnsPrefixMetadata;

        return $this;
    }

    /**
     * @return string
     */
    public function getDnsScheme()
    {
        return $this->dnsScheme;
    }

    /**
     * @param unknown $dnsScheme
     * @return \Carmen\ApiBundle\Entity\LexDns
     */
    public function setDnsScheme($dnsScheme)
    {
        $this->dnsScheme = $dnsScheme;

        return $this;
    }

    /**
     * Construct the account url to front according to parameters
     * @param boolean $bForFrontOffice
     * @param string $environnement prod|dev
     * @return string
     */
    public function getAccountUrlFront($bForFrontOffice = true, $environnement = "prod", $separator = ".")
    {
        if ($bForFrontOffice) {
            $dnsPrefix = ($this->getDnsPrefixFrontoffice() != "" ? $this->getDnsPrefixFrontoffice() . $separator : "");
            $url = "{$this->getDnsScheme()}://{$dnsPrefix}{$this->getDnsUrl()}";
        } else if ($environnement == "dev") {
            $url = "";
        } else {
            $dnsPrefix = ($this->getDnsPrefixBackoffice() != "" ? $this->getDnsPrefixBackoffice() . $separator : "");
            $url = "{$this->getDnsScheme()}://{$dnsPrefix}{$this->getDnsUrl()}";
        }
        if (isset($_SERVER["SERVER_PORT"]) && strpos($url, "_ALK_PORT_") !== false) {
            $url = preg_replace("!_ALK_PORT_(\d+)!", substr($_SERVER["SERVER_PORT"], 0, 2) . "$1/app_dev.php", $url);
        }
        return $url;
    }

    /**
     * Construct the account url to data according to parameters
     * @param boolean $bForFrontOffice
     * @param string $environnement prod|dev
     * @return string
     */
    public function getAccountUrlData($bForFrontOffice = true, $environnement = "prod", $separator = ".")
    {
        $dnsPrefix = ($this->getDnsPrefixData() != "" ? $this->getDnsPrefixData() . $separator : "");
        $url = "{$this->getDnsScheme()}://{$dnsPrefix}{$this->getDnsUrl()}";
        if (isset($_SERVER["SERVER_PORT"]) && strpos($url, "_ALK_PORT_") !== false) {
            $url = preg_replace("!_ALK_PORT_(\d+)!", substr($_SERVER["SERVER_PORT"], 0, 2) . "$1", $url);
        }
        return $url;
    }
}
