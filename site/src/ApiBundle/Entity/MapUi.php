<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapUi
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.map_ui", indexes={@ORM\Index(name="idx_map_ui", columns={"map_id"}), @ORM\Index(name="idx_map_ui_0", columns={"ui_color_id"})})
 * @ORM\Entity
 */
class MapUi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ui_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.map_ui_ui_id_seq", allocationSize=1, initialValue=1)
     */
    private $uiId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_logo", type="boolean", nullable=true)
     */
    private $uiLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="ui_logopath", type="text", nullable=true)
     */
    private $uiLogopath;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_copyright", type="boolean", nullable=true)
     */
    private $uiCopyright;

    /**
     * @var string
     *
     * @ORM\Column(name="ui_copyright_text", type="string", nullable=true)
     */
    private $uiCopyrightText;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_banner", type="boolean", nullable=true)
     */
    private $uiBanner;

    /**
     * @var string
     *
     * @ORM\Column(name="ui_banner_file", type="text", nullable=true)
     */
    private $uiBannerFile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_legend", type="boolean", nullable=true)
     */
    private $uiLegend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_legend_print", type="boolean", nullable=true)
     */
    private $uiLegendPrint;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_keymap", type="boolean", nullable=true)
     */
    private $uiKeymap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_focus", type="boolean", nullable=true)
     */
    private $uiFocus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ui_locate", type="boolean", nullable=true)
     */
    private $uiLocate;

    /**
     * @var \Map
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;

    /**
     * @var \LexColorId
     *
     * @ORM\ManyToOne(targetEntity="LexColorId")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ui_color_id", referencedColumnName="color_id")
     * })
     */
    private $uiColor;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="UiModel", mappedBy="mapUi", cascade={"persist"})
     */
    private $uiModels;

    /**
     * @var boolean
     *
     * @ORM\Column(name="help_display", type="boolean", nullable=true)
     */
    private $helpDisplay;

    /**
     * @var string
     *
     * @ORM\Column(name="help_message", type="text", nullable=true)
     */
    private $helpMessage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cgu_display", type="boolean", nullable=true)
     */
    private $cguDisplay;

    /**
     * @var string
     *
     * @ORM\Column(name="cgu_message", type="text", nullable=true)
     */
    private $cguMessage;


    public function __construct()
    {
        $this->uiModels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     *
     * Set ui Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\Layer
     */
    public function setId($id)
    {
        $this->uiId = $id;

        return $this;
    }

    /**
     * Get uiId
     *
     * @return integer
     */
    public function getUiId()
    {
        return $this->uiId;
    }

    /**
     * Set uiLogo
     *
     * @param boolean $uiLogo
     * @return MapUi
     */
    public function setUiLogo($uiLogo)
    {
        $this->uiLogo = $uiLogo;

        return $this;
    }

    /**
     * Get uiLogo
     *
     * @return boolean
     */
    public function getUiLogo()
    {
        return $this->uiLogo;
    }

    /**
     * Set uiLogopath
     *
     * @param string $uiLogopath
     * @return MapUi
     */
    public function setUiLogopath($uiLogopath)
    {
        $this->uiLogopath = $uiLogopath;

        return $this;
    }

    /**
     * Get uiLogopath
     *
     * @return string
     */
    public function getUiLogopath()
    {
        return $this->uiLogopath;
    }

    /**
     * Set uiCopyright
     *
     * @param boolean $uiCopyright
     * @return MapUi
     */
    public function setUiCopyright($uiCopyright)
    {
        $this->uiCopyright = $uiCopyright;

        return $this;
    }

    /**
     * Get uiCopyright
     *
     * @return boolean
     */
    public function getUiCopyright()
    {
        return $this->uiCopyright;
    }

    /**
     * Set uiCopyrightText
     *
     * @param string $uiCopyrightText
     * @return MapUi
     */
    public function setUiCopyrightText($uiCopyrightText)
    {
        $this->uiCopyrightText = $uiCopyrightText;

        return $this;
    }

    /**
     * Get uiCopyrightText
     *
     * @return string
     */
    public function getUiCopyrightText()
    {
        return $this->uiCopyrightText;
    }


    /**
     * Set uiBanner
     *
     * @param boolean $uiBanner
     * @return MapUi
     */
    public function setUiBanner($uiBanner)
    {
        $this->uiBanner = $uiBanner;

        return $this;
    }

    /**
     * Get uiBanner
     *
     * @return boolean
     */
    public function getUiBanner()
    {
        return $this->uiBanner;
    }

    /**
     * Set uiBannerFile
     *
     * @param string $uiBannerFile
     * @return MapUi
     */
    public function setUiBannerFile($uiBannerFile)
    {
        $this->uiBannerFile = $uiBannerFile;

        return $this;
    }

    /**
     * Get uiBannerFile
     *
     * @return string
     */
    public function getUiBannerFile()
    {
        return $this->uiBannerFile;
    }

    /**
     * Set uiLegend
     *
     * @param boolean $uiLegend
     * @return MapUi
     */
    public function setUiLegend($uiLegend)
    {
        $this->uiLegend = $uiLegend;

        return $this;
    }

    /**
     * Get uiLegend
     *
     * @return boolean
     */
    public function getUiLegend()
    {
        return $this->uiLegend;
    }

    /**
     * Set uiLegendPrint
     *
     * @param boolean $uiLegendPrint
     * @return MapUi
     */
    public function setUiLegendPrint($uiLegendPrint)
    {
        $this->uiLegendPrint = $uiLegendPrint;

        return $this;
    }

    /**
     * Get uiLegendPrint
     *
     * @return boolean
     */
    public function getUiLegendPrint()
    {
        return $this->uiLegendPrint;
    }

    /**
     * Set uiKeymap
     *
     * @param boolean $uiKeymap
     * @return MapUi
     */
    public function setUiKeymap($uiKeymap)
    {
        $this->uiKeymap = $uiKeymap;

        return $this;
    }

    /**
     * Get uiKeymap
     *
     * @return boolean
     */
    public function getUiKeymap()
    {
        return $this->uiKeymap;
    }

    /**
     * Set uiFocus
     *
     * @param boolean $uiFocus
     * @return MapUi
     */
    public function setUiFocus($uiFocus)
    {
        $this->uiFocus = $uiFocus;

        return $this;
    }

    /**
     * Get uiFocus
     *
     * @return boolean
     */
    public function getUiFocus()
    {
        return $this->uiFocus;
    }

    /**
     * Set uiLocate
     *
     * @param boolean $uiLocate
     * @return MapUi
     */
    public function setUiLocate($uiLocate)
    {
        $this->uiLocate = $uiLocate;

        return $this;
    }

    /**
     * Get uiLocate
     *
     * @return boolean
     */
    public function getUiLocate()
    {
        return $this->uiLocate;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return MapUi
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set uiColor
     *
     * @param \Carmen\ApiBundle\Entity\LexColorId $uiColor
     * @return MapUi
     */
    public function setUiColor(\Carmen\ApiBundle\Entity\LexColorId $uiColor = null)
    {
        $this->uiColor = $uiColor;

        return $this;
    }

    /**
     * Get uiColor
     *
     * @return \Carmen\ApiBundle\Entity\LexColorId
     */
    public function getUiColor()
    {
        return $this->uiColor;
    }

    /**
     * Add uiModel
     *
     * @param \Carmen\ApiBundle\Entity\UiModel $uiModel
     * @return MapUi
     */
    public function addUiModels(\Carmen\ApiBundle\Entity\UiModel $uiModel)
    {
        $this->uiModels[] = $uiModel;
        $uiModel->setMapUi($this);

        return $this;
    }

    /**
     * Remove uiModels
     *
     * @param \Carmen\ApiBundle\Entity\UiModel $uiModel
     */
    public function removeUiModels(\Carmen\ApiBundle\Entity\UiModel $uiModel)
    {
        $this->uiModels->removeElement($uiModel);
    }

    /**
     * Set uiModels
     *
     * @param \Doctrine\Common\Collections\Collection $uiModels
     * @return Map
     */
    public function setUiModels(\Doctrine\Common\Collections\Collection $uiModels)
    {
        $this->uiModels = $uiModels;
        foreach ($this->uiModels as $uiModel) {
            $uiModel->setMapUi($this);
        }

        return $this;
    }

    /**
     * Get mapUi
     *
     * @return \Carmen\ApiBundle\Entity\MapUi
     */
    public function getUiModels()
    {
        return $this->uiModels;
    }

    /**
     * @param boolean $helpDisplay
     *
     * @return MapUi
     */
    public function setHelpDisplay($helpDisplay)
    {
        $this->helpDisplay = $helpDisplay;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isHelpDisplay()
    {
        return $this->helpDisplay;
    }

    /**
     * @param string $helpMessage
     *
     * @return MapUi
     */
    public function setHelpMessage($helpMessage)
    {
        $this->helpMessage = $helpMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getHelpMessage()
    {
        return $this->helpMessage;
    }


    /**
     * @param boolean $cguDisplay
     *
     * @return MapUi
     */
    public function setCguDisplay($cguDisplay)
    {
        $this->cguDisplay = $cguDisplay;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isCguDisplay()
    {
        return $this->cguDisplay;
    }

    /**
     * @param string $CguMessage
     *
     * @return MapUi
     */
    public function setCguMessage($CguMessage)
    {
        $this->cguMessage = $CguMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getCguMessage()
    {
        return $this->cguMessage;
    }
}
