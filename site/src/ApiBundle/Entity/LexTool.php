<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexTool
 *
 * @ORM\Table(name="carmen.lex_tool")
 * @ORM\Entity
 */
class LexTool
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tool_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_tool_tool_id_seq", allocationSize=1, initialValue=1)
     */
    private $toolId;

    /**
     * @var string
     *
     * @ORM\Column(name="tool_identifier", type="string", length=255, nullable=true)
     */
    private $toolIdentifier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tool_default_enabled", type="boolean", nullable=false)
     */
    private $toolDefaultEnabled = false;

    /**
     * @var string
     *
     * @ORM\Column(name="tool_xml_nodeprefix", type="string", length=50, nullable=false)
     */
    private $toolXmlNodeprefix = "tool";

    /**
     * @var string
     *
     * @ORM\Column(name="tool_xml_nodevalue", type="string", length=50, nullable=true)
     */
    private $toolXmlNodevalue;


    /**
     * Get toolId
     *
     * @return integer
     */
    public function getToolId()
    {
        return $this->toolId;
    }

    /**
     * Set toolIdentifier
     *
     * @param string $toolIdentifier
     * @return LexTool
     */
    public function setToolIdentifier($toolIdentifier)
    {
        $this->toolIdentifier = $toolIdentifier;

        return $this;
    }

    /**
     * Get toolIdentifier
     *
     * @return string
     */
    public function getToolIdentifier()
    {
        return $this->toolIdentifier;
    }

    /**
     * Set toolDefaultEnabled
     *
     * @param boolean $toolDefaultEnabled
     * @return LexTool
     */
    public function setToolDefaultEnabled($toolDefaultEnabled)
    {
        $this->toolDefaultEnabled = $toolDefaultEnabled;

        return $this;
    }

    /**
     * Get toolDefaultEnabled
     *
     * @return boolean
     */
    public function getToolDefaultEnabled()
    {
        return $this->toolDefaultEnabled;
    }

    /**
     * Set toolXmlNodeprefix
     *
     * @param string $toolXmlNodeprefix
     * @return LexTool
     */
    public function setToolXmlNodeprefix($toolXmlNodeprefix)
    {
        $this->toolXmlNodeprefix = $toolXmlNodeprefix;

        return $this;
    }

    /**
     * Get toolXmlNodeprefix
     *
     * @return string
     */
    public function getToolXmlNodeprefix()
    {
        return $this->toolXmlNodeprefix;
    }

    /**
     * Set toolXmlNodevalue
     *
     * @param string $toolXmlNodevalue
     * @return LexTool
     */
    public function setToolXmlNodevalue($toolXmlNodevalue)
    {
        $this->toolXmlNodevalue = $toolXmlNodevalue;

        return $this;
    }

    /**
     * Get toolXmlNodevalue
     *
     * @return string
     */
    public function getToolXmlNodeValue()
    {
        return $this->toolXmlNodevalue;
    }

    /**
     * Evaluate if this tool is enabled for a map
     * @param Map $map
     * @return boolean
     */
    public function isEnabledFor(Map $map)
    {
        return $map->hasTool($this);
    }
}
