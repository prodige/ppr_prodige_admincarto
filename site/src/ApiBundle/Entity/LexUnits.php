<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexUnits
 *
 * @ORM\Table(name="carmen.lex_units")
 * @ORM\Entity
 */
class LexUnits
{
    /**
     * @var string
     *
     * @ORM\Column(name="unit_code", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_units_unit_code_seq", allocationSize=1, initialValue=1)
     */
    private $unitCode;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_name", type="string", length=100, nullable=true)
     */
    private $unitName;


    /**
     * Get unitCode
     *
     * @return string
     */
    public function getUnitCode()
    {
        return $this->unitCode;
    }

    /**
     * Set unitName
     *
     * @param string $unitName
     * @return LexUnits
     */
    public function setUnitName($unitName)
    {
        $this->unitName = $unitName;

        return $this;
    }

    /**
     * Get unitName
     *
     * @return string
     */
    public function getUnitName()
    {
        return $this->unitName;
    }
}
