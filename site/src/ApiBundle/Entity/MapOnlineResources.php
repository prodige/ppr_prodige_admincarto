<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapOnlineResources
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.map_onlineresources", indexes={@ORM\Index(name="idx_onlineresource", columns={"map_id"})})
 * @ORM\Entity
 *
 */
class MapOnlineResources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="map_onlineresource_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.map_onlineresources_map_onlineresource_id_seq", allocationSize=1, initialValue=1)
     */
    private $onlineResourceId;


    /**
     * @var string
     *
     * @ORM\Column(name="map_onlineresource_name", type="string", nullable=false)
     */
    private $onlineResourceName;

    /**
     * @var string
     *
     * @ORM\Column(name="map_onlineresource_url", type="string", nullable=false)
     */
    private $onlineResourceUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="map_onlineresource_category_name", type="string", nullable=false)
     */
    private $onlineResourceCategoryName;

    /**
     * @var \Map
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map", inversedBy="onlineResources")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;

    // -------------------------------------------------------------------------------
    // ---------------- ResourceId
    /**
     * Set onlineResourceId
     *
     * @param integer $onlineResourceId
     * @return MapOnlineResources
     */
    public function setOnlineResourceId($onlineResourceId)
    {
        $this->onlineResourceId = $onlineResourceId;

        return $this;
    }

    /**
     * Get onlineResourceId
     *
     * @return integer
     */
    public function getOnlineResourceId()
    {
        return $this->onlineResourceId;
    }
    // -------------------------------------------------------------------------------

    // -------------------------------------------------------------------------------
    // ---------------- ResourceName
    /**
     * Set onlineResourceName
     *
     * @param string $onlineResourceName
     * @return MapOnlineResources
     */
    public function setOnlineResourceName($onlineResourceName)
    {
        $this->onlineResourceName = $onlineResourceName;

        return $this;
    }

    /**
     * Get onlineResourceName
     *
     * @return string
     */
    public function getOnlineResourceName()
    {
        return $this->onlineResourceName;
    }
    // -------------------------------------------------------------------------------

    // -------------------------------------------------------------------------------
    // ---------------- ResourceUrl
    /**
     * Set onlineResourceUrl
     *
     * @param string $onlineResourceUrl
     * @return MapOnlineResources
     */
    public function setOnlineResourceUrl($onlineResourceUrl)
    {
        $this->onlineResourceUrl = $onlineResourceUrl;

        return $this;
    }

    /**
     * Get onlineResourceUrl
     *
     * @return string
     */
    public function getOnlineResourceUrl()
    {
        return $this->onlineResourceUrl;
    }
    // -------------------------------------------------------------------------------

    // -------------------------------------------------------------------------------
    // ---------------- ResourceCategoryName
    /**
     * Set onlineResourceCategoryName
     *
     * @param string $onlineResourceCategoryName
     * @return MapOnlineResources
     */
    public function setOnlineResourceCategoryName($onlineResourceCategoryName)
    {
        $this->onlineResourceCategoryName = $onlineResourceCategoryName;

        return $this;
    }

    /**
     * Get onlineResourceCategoryName
     *
     * @return string
     */
    public function getOnlineResourceCategoryName()
    {
        return $this->onlineResourceCategoryName;
    }
    // -------------------------------------------------------------------------------

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return MapOnlineResources
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }
}
