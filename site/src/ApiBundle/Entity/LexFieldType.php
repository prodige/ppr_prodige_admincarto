<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexFieldType
 *
 * @ORM\Table(name="carmen.lex_field_type")
 * @ORM\Entity
 */
class LexFieldType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="field_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_field_type_field_type_id_seq", allocationSize=1, initialValue=1)
     */
    private $fieldTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="field_type_name", type="string", length=255, nullable=true)
     */
    private $fieldTypeName;


    /**
     * Get fieldTypeId
     *
     * @return integer
     */
    public function getFieldTypeId()
    {
        return $this->fieldTypeId;
    }

    /**
     * Set fieldTypeName
     *
     * @param string $fieldTypeName
     * @return LexFieldType
     */
    public function setFieldTypeName($fieldTypeName)
    {
        $this->fieldTypeName = $fieldTypeName;

        return $this;
    }

    /**
     * Get fieldTypeName
     *
     * @return string
     */
    public function getFieldTypeName()
    {
        return $this->fieldTypeName;
    }
}
