<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapGroup
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.map_group", indexes={@ORM\Index(name="idx_group", columns={"map_id"})})
 * @ORM\Entity
 */
class MapGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.layer_layer_id_seq", allocationSize=1, initialValue=1)
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="group_name", type="text", nullable=false)
     */
    private $groupName;


    /**
     * @var string
     *
     * @ORM\Column(name="group_identifier", type="text", nullable=false)
     */
    private $groupIdentifier;

    /**
     * @var \Map
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;


    /**
     *
     * Set group Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\MapGroup
     */
    public function setId($id)
    {
        $this->groupId = $id;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set groupName
     *
     * @param string $groupName
     * @return MapGroup
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;

        return $this;
    }

    /**
     * Get groupName
     *
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * Set groupIdentifier
     *
     * @param string $groupIdentifier
     * @return MapGroup
     */
    public function setGroupIdentifier($groupIdentifier)
    {
        $this->groupIdentifier = $groupIdentifier;

        return $this;
    }

    /**
     * Get groupIdentifier
     *
     * @return string
     */
    public function getGroupIdentifier()
    {
        return $this->groupIdentifier;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return MapGroup
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }
}
