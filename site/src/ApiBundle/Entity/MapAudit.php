<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MapAudit
 *
 * @ORM\Table(name="carmen.map_audit", indexes={@ORM\Index(name="idx_map_audit", columns={"account_id"}), @ORM\Index(name="idx_map_audit_0", columns={"map_audit_status"}), @ORM\Index(name="idx_map_audit_1", columns={"user_email"})})
 * @ORM\Entity
 */
class MapAudit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="map_audit_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.map_audit_map_audit_id_seq", allocationSize=1, initialValue=1)
     */
    private $mapAuditId;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="map_audit_date", type="datetime", nullable=false)
     */
    private $mapAuditDate;

    /**
     * @var string
     * @ORM\Column(name="map_file", type="string", nullable=false)
     */
    private $mapFile;

    /**
     * @var string
     * @ORM\Column(name="map_audit_status", type="string", nullable=false)
     */
    private $mapAuditStatus;

    /**
     * @var string
     * @ORM\Column(name="user_email", type="string", nullable=false)
     */
    private $userEmail;


    /**
     * Get mapAuditId
     *
     * @return integer
     */
    public function getMapAuditId()
    {
        return $this->mapAuditId;
    }

    /**
     * Set account
     *
     * @param Account $account
     * @return MapAudit
     */
    public function setAccount(\Carmen\ApiBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set mapAuditDate
     *
     * @param \DateTime $mapAuditDate
     * @return MapAudit
     */
    public function setMapAuditDate($mapAuditDate)
    {
        $this->mapAuditDate = $mapAuditDate;

        return $this;
    }

    /**
     * Get mapAuditDate
     *
     * @return \DateTime
     */
    public function getMapAuditDate()
    {
        return $this->mapAuditDate;
    }

    /**
     * Set mapFile
     *
     * @param string $mapFile
     * @return MapAudit
     */
    public function setMapFile($mapFile)
    {
        $this->mapFile = $mapFile;

        return $this;
    }

    /**
     * Get mapFile
     *
     * @return string
     */
    public function getMapFile()
    {
        return $this->mapFile;
    }

    /**
     * Set mapAuditStatus
     *
     * @param string $mapAuditStatus
     * @return MapAudit
     */
    public function setMapAuditStatus($mapAuditStatus)
    {
        $this->mapAuditStatus = $mapAuditStatus;

        return $this;
    }

    /**
     * Get mapAuditStatus
     *
     * @return string
     */
    public function getMapAuditStatus()
    {
        return $this->mapAuditStatus;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return MapAudit
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }
}
