<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAudit
 *
 * @ORM\Table(name="carmen.user_audit")
 * @ORM\Entity
 */
class UserAudit
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_creation_date", type="date", nullable=false)
     */
    private $userCreationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_last_connection_date", type="date", nullable=true)
     */
    private $userLastConnectionDate;

    /**
     * @var \Users
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;


    /**
     * Set userCreationDate
     *
     * @param \DateTime $userCreationDate
     * @return UserAudit
     */
    public function setUserCreationDate($userCreationDate)
    {
        $this->userCreationDate = $userCreationDate;

        return $this;
    }

    /**
     * Get userCreationDate
     *
     * @return \DateTime
     */
    public function getUserCreationDate()
    {
        return $this->userCreationDate;
    }

    /**
     * Set userLastConnectionDate
     *
     * @param \DateTime $userLastConnectionDate
     * @return UserAudit
     */
    public function setUserLastConnectionDate($userLastConnectionDate)
    {
        $this->userLastConnectionDate = $userLastConnectionDate;

        return $this;
    }

    /**
     * Get userLastConnectionDate
     *
     * @return \DateTime
     */
    public function getUserLastConnectionDate()
    {
        return $this->userLastConnectionDate;
    }

    /**
     * Set user
     *
     * @param \Carmen\ApiBundle\Entity\Users $user
     * @return UserAudit
     */
    public function setUser(\Carmen\ApiBundle\Entity\Users $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Carmen\ApiBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
