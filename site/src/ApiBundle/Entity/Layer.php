<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Layer
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.layer", indexes={@ORM\Index(name="idx_layer", columns={"map_id"}), @ORM\Index(name="idx_layer_0", columns={"layer_analyse_type_id"})})
 * @ORM\Entity
 */
class Layer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="layer_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.layer_layer_id_seq", allocationSize=1, initialValue=1)
     */
    private $layerId;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_title", type="string", length=255, nullable=true)
     */
    private $layerTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_name", type="string", length=255, nullable=true)
     */
    private $layerName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_legend", type="boolean", nullable=true)
     */
    private $layerLegend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_legend_scale", type="boolean", nullable=true)
     */
    private $layerLegendScale;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_field_url", type="string", nullable=true)
     */
    private $layerFieldUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_metadata_file", type="string", length=255, nullable=true)
     */
    private $layerMetadataFile;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_metadata_uuid", type="string", length=255, nullable=true)
     */
    private $layerMetadataUuid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_wms", type="boolean", nullable=true)
     */
    private $layerWms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_wfs", type="boolean", nullable=true)
     */
    private $layerWfs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_atom", type="boolean", nullable=true)
     */
    private $layerAtom;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_downloadable", type="boolean", nullable=true)
     */
    private $layerDownloadable;

    /**
     * @var integer
     *
     * @ORM\Column(name="layer_opacity", type="integer", nullable=true)
     */
    private $layerOpacity;

    /**
     * @var integer
     *
     * @ORM\Column(name="layer_minscale", type="integer", nullable=true)
     */
    private $layerMinscale;

    /**
     * @var integer
     *
     * @ORM\Column(name="layer_maxscale", type="integer", nullable=true)
     */
    private $layerMaxscale;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_visible", type="boolean", nullable=true)
     */
    private $layerVisible = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_haslabel", type="boolean", nullable=true)
     */
    private $layerHaslabel = true;

    /**
     * @var \Map
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;

    /**
     * @var \LexAnalyseType
     *
     * @ORM\ManyToOne(targetEntity="LexAnalyseType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layer_analyse_type_id", referencedColumnName="analyse_type_id")
     * })
     */
    private $layerAnalyseType;

    /**
     * @var \LexDisplayMode
     *
     * @ORM\ManyToOne(targetEntity="LexDisplayMode")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layer_display_mode_id", referencedColumnName="display_mode_id")
     * })
     */
    private $layerDisplayMode;

    /**
     * @var \LexLayerType
     *
     * @ORM\ManyToOne(targetEntity="LexLayerType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layer_type_code", referencedColumnName="layer_type_code")
     * })
     */
    private $layerType;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_server_url", type="string", nullable=true)
     */
    private $layerServerUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_server_version", type="string", nullable=true)
     */
    private $layerServerVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_tiled_layer", type="string", nullable=true)
     */
    private $layerTiledLayer;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_tiled_server_url", type="string", nullable=true)
     */
    private $layerTiledServerUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_tiled_resolutions", type="string", nullable=true)
     */
    private $layerTiledResolutions;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_wmsc_boundingbox", type="string", nullable=true)
     */
    private $layerWmscBoundingbox;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_wmts_matrixids", type="string", nullable=true)
     */
    private $layerWmtsMatrixids;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_wmts_tileorigin", type="string", nullable=true)
     */
    private $layerWmtsTileorigin;
    /**
     * @var string
     *
     * @ORM\Column(name="layer_wmts_tileset", type="string", nullable=true)
     */
    private $layerWmtsTileset;
    /**
     * @var string
     *
     * @ORM\Column(name="layer_wmts_style", type="string", nullable=true)
     */
    private $layerWmtsStyle;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_wxsname", type="string", nullable=true)
     */
    private $layerWxsname;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_msname", type="string", nullable=true)
     */
    private $layerMsname;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_identifier", type="string", nullable=true)
     */
    private $layerIdentifier;

    /**
     * @var \LexProjection
     *
     * @ORM\ManyToOne(targetEntity="LexProjection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layer_projection_epsg", referencedColumnName="projection_epsg", nullable=true)
     * })
     */
    private $layerProjectionEpsg;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_outputformat", type="string", nullable=true)
     */
    private $layerOutputformat;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_data_encoding", type="string", nullable=true)
     */
    private $layerDataEncoding;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_wms_query", type="boolean", nullable=true)
     */
    private $layerWmsQuery;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_background", type="boolean", nullable=true)
     */
    private $layerIsBackground;

    /**
     * @var boolean
     *
     * @ORM\Column(name="layer_default_background", type="boolean", nullable=true)
     */
    private $layerIsDefaultBackground;

    /**
     * @var string
     *
     * @ORM\Column(name="layer_background_image", type="string", nullable=true)
     */
    private $layerBackgroundImage;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Field", mappedBy="layer", cascade={"persist"})
     * @ORM\OrderBy({"fieldRank" = "ASC"})
     */
    private $fields;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="GeojsonParams", mappedBy="layer", cascade={"persist"})
     */
    private $geojsonParams;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->geojsonParams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     *
     * Set layer Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\Layer
     */
    public function setId($id)
    {
        $this->layerId = $id;

        return $this;
    }

    /**
     * Get layerId
     *
     * @return integer
     */
    public function getLayerId()
    {
        return $this->layerId;
    }

    /**
     * Set layerTitle
     *
     * @param string $layerTitle
     * @return Layer
     */
    public function setLayerTitle($layerTitle)
    {
        $this->layerTitle = $layerTitle;

        return $this;
    }

    /**
     * Get layerTitle
     *
     * @return string
     */
    public function getLayerTitle()
    {
        return $this->layerTitle;
    }

    /**
     * Set layerName
     *
     * @param string $layerName
     * @return Layer
     */
    public function setLayerName($layerName)
    {
        $this->layerName = $layerName;

        return $this;
    }

    /**
     * Get layerName
     *
     * @return string
     */
    public function getLayerName()
    {
        return $this->layerName;
    }

    /**
     * Set layerLegend
     *
     * @param boolean $layerLegend
     * @return Layer
     */
    public function setLayerLegend($layerLegend)
    {
        $this->layerLegend = $layerLegend;

        return $this;
    }

    /**
     * Get layerLegend
     *
     * @return boolean
     */
    public function getLayerLegend()
    {
        return $this->layerLegend;
    }

    /**
     * Set layerLegendScale
     *
     * @param boolean $layerLegendScale
     * @return Layer
     */
    public function setLayerLegendScale($layerLegendScale)
    {
        $this->layerLegendScale = $layerLegendScale;

        return $this;
    }

    /**
     * Get layerLegendScale
     *
     * @return boolean
     */
    public function getLayerLegendScale()
    {
        return $this->layerLegendScale;
    }

    /**
     * Set layerFieldUrl
     *
     * @param string $layerFieldUrl
     * @return Layer
     */
    public function setLayerFieldUrl($layerFieldUrl)
    {
        $this->layerFieldUrl = $layerFieldUrl;

        return $this;
    }

    /**
     * Get layerFieldUrl
     *
     * @return string
     */
    public function getLayerFieldUrl()
    {
        return $this->layerFieldUrl;
    }

    /**
     * Set layerMetadataFile
     *
     * @param string $layerMetadataFile
     * @return Layer
     */
    public function setLayerMetadataFile($layerMetadataFile)
    {
        $this->layerMetadataFile = $layerMetadataFile;

        return $this;
    }

    /**
     * Get layerMetadataFile
     *
     * @return string
     */
    public function getLayerMetadataFile()
    {
        return $this->layerMetadataFile;
    }

    /**
     * Set layerMetadataUuid
     *
     * @param string $layerMetadataUuid
     * @return Layer
     */
    public function setLayerMetadataUuid($layerMetadataUuid)
    {
        $this->layerMetadataUuid = $layerMetadataUuid;

        return $this;
    }

    /**
     * Get layerMetadataUuid
     *
     * @return string
     */
    public function getLayerMetadataUuid()
    {
        return $this->layerMetadataUuid;
    }

    /**
     * Set layerWms
     *
     * @param boolean $layerWms
     * @return Layer
     */
    public function setLayerWms($layerWms)
    {
        $this->layerWms = $layerWms;

        return $this;
    }

    /**
     * Get layerWms
     *
     * @return boolean
     */
    public function getLayerWms()
    {
        return $this->layerWms;
    }

    /**
     * Set layerWfs
     *
     * @param boolean $layerWfs
     * @return Layer
     */
    public function setLayerWfs($layerWfs)
    {
        $this->layerWfs = $layerWfs;

        return $this;
    }

    /**
     * Get layerWfs
     *
     * @return boolean
     */
    public function getLayerWfs()
    {
        return $this->layerWfs;
    }

    /**
     * Set layerAtom
     *
     * @param boolean $layerAtom
     * @return Layer
     */
    public function setLayerAtom($layerAtom)
    {
        $this->layerAtom = $layerAtom;

        return $this;
    }

    /**
     * Get layerAtom
     *
     * @return boolean
     */
    public function getLayerAtom()
    {
        return $this->layerAtom;
    }

    /**
     * Set layerDownloadable
     *
     * @param boolean $layerDownloadable
     * @return Layer
     */
    public function setLayerDownloadable($layerDownloadable)
    {
        $this->layerDownloadable = $layerDownloadable;

        return $this;
    }

    /**
     * Get layerDownloadable
     *
     * @return boolean
     */
    public function getLayerDownloadable()
    {
        return $this->layerDownloadable;
    }

    /**
     * Set layerOpacity
     *
     * @param integer $layerOpacity
     * @return Layer
     */
    public function setLayerOpacity($layerOpacity)
    {
        $this->layerOpacity = $layerOpacity;

        return $this;
    }

    /**
     * Get layerOpacity
     *
     * @return integer
     */
    public function getLayerOpacity()
    {
        return $this->layerOpacity;
    }

    /**
     * Set layerMinscale
     *
     * @param integer $layerMinscale
     * @return Layer
     */
    public function setLayerMinscale($layerMinscale)
    {
        $this->layerMinscale = $layerMinscale;

        return $this;
    }

    /**
     * Get layerMinscale
     *
     * @return integer
     */
    public function getLayerMinscale()
    {
        return $this->layerMinscale;
    }

    /**
     * Set layerMaxscale
     *
     * @param integer $layerMaxscale
     * @return Layer
     */
    public function setLayerMaxscale($layerMaxscale)
    {
        $this->layerMaxscale = $layerMaxscale;

        return $this;
    }

    /**
     * Get layerMaxscale
     *
     * @return integer
     */
    public function getLayerMaxscale()
    {
        return $this->layerMaxscale;
    }

    /**
     * Set layerVisible
     *
     * @param boolean $layerVisible
     * @return Layer
     */
    public function setLayerVisible($layerVisible)
    {
        $this->layerVisible = $layerVisible;

        return $this;
    }

    /**
     * Get layerVisible
     *
     * @return boolean
     */
    public function getLayerVisible()
    {
        return $this->layerVisible;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return Layer
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set layerAnalyseType
     *
     * @param \Carmen\ApiBundle\Entity\LexAnalyseType $layerAnalyseType
     * @return Layer
     */
    public function setLayerAnalyseType(\Carmen\ApiBundle\Entity\LexAnalyseType $layerAnalyseType = null)
    {
        $this->layerAnalyseType = $layerAnalyseType;

        return $this;
    }

    /**
     * Get layerAnalyseType
     *
     * @return \Carmen\ApiBundle\Entity\LexAnalyseType
     */
    public function getLayerAnalyseType()
    {
        return $this->layerAnalyseType;
    }

    /**
     * Set layerType
     *
     * @param \Carmen\ApiBundle\Entity\LexLayerType $layerType
     * @return Layer
     */
    public function setLayerType(\Carmen\ApiBundle\Entity\LexLayerType $layerType = null)
    {
        $this->layerType = $layerType;

        return $this;
    }

    /**
     * Get layerType
     *
     * @return \Carmen\ApiBundle\Entity\LexLayerType
     */
    public function getLayerType()
    {
        return $this->layerType;
    }

    /**
     * Set layerHaslabel
     *
     * @param boolean $layerHaslabel
     * @return Layer
     */
    public function setLayerHaslabel($layerHaslabel)
    {
        $this->layerHaslabel = $layerHaslabel;

        return $this;
    }

    /**
     * Get layerHaslabel
     *
     * @return boolean
     */
    public function getLayerHaslabel()
    {
        return $this->layerHaslabel;
    }

    /**
     * Set layerServerUrl
     *
     * @param string $layerServerUrl
     * @return Layer
     */
    public function setLayerServerUrl($layerServerUrl)
    {
        $this->layerServerUrl = $layerServerUrl;

        return $this;
    }

    /**
     * Get layerServerUrl
     *
     * @return string
     */
    public function getLayerServerUrl()
    {
        return $this->layerServerUrl;
    }

    /**
     * Set layerServerVersion
     *
     * @param string $layerServerVersion
     * @return Layer
     */
    public function setLayerServerVersion($layerServerVersion)
    {
        $this->layerServerVersion = $layerServerVersion;

        return $this;
    }

    /**
     * Get layerServerVersion
     *
     * @return string
     */
    public function getLayerServerVersion()
    {
        return $this->layerServerVersion;
    }

    /**
     * Set layerTiledLayer
     *
     * @param string $layerTiledLayer
     * @return Layer
     */
    public function setLayerTiledLayer($layerTiledLayer)
    {
        $this->layerTiledLayer = $layerTiledLayer;
        return $this;
    }

    /**
     * Get layerTiledLayer
     *
     * @return string
     */
    public function getLayerTiledLayer()
    {
        return $this->layerTiledLayer;
    }

    /**
     * Set layerTiledServerUrl
     *
     * @param string $layerTiledServerUrl
     * @return Layer
     */
    public function setLayerTiledServerUrl($layerTiledServerUrl)
    {
        $this->layerTiledServerUrl = $layerTiledServerUrl;
        return $this;
    }

    /**
     * Get layerTiledServerUrl
     *
     * @return string
     */
    public function getLayerTiledServerUrl()
    {
        return $this->layerTiledServerUrl;
    }

    /**
     * Set layerTiledResolutions
     *
     * @param string $layerTiledResolutions
     * @return Layer
     */
    public function setLayerTiledResolutions($layerTiledResolutions)
    {
        $this->layerTiledResolutions = $layerTiledResolutions;
        return $this;
    }

    /**
     * Get layerTiledResolutions
     *
     * @return string
     */
    public function getLayerTiledResolutions()
    {
        return $this->layerTiledResolutions;
    }

    /**
     * Get layerTiledLayer
     *
     * @return string
     */
    public function getLayerWmscBoundingbox()
    {
        return $this->layerWmscBoundingbox;
    }

    /**
     *
     * @param string $layerWmscBoundingbox
     * @return Layer
     */
    public function setLayerWmscBoundingbox($layerWmscBoundingbox)
    {
        $this->layerWmscBoundingbox = $layerWmscBoundingbox;
        return $this;
    }


    /**
     * Set layerWmtsMatrixids
     *
     * @param string $layerWmtsMatrixids
     * @return Layer
     */
    public function setLayerWmtsMatrixids($layer_wmts_matrixids)
    {
        $this->layerWmtsMatrixids = $layer_wmts_matrixids;

        return $this;
    }

    /**
     * Get layerWmtsMatrixids
     *
     * @return string
     */
    public function getLayerWmtsMatrixids()
    {
        return $this->layerWmtsMatrixids;
    }

    /**
     * Set layerWmtsTileorigin
     *
     * @param string $layeWmtsTileorigin
     * @return Layer
     */
    public function setLayerWmtsTileorigin($layer_wmts_tileorigin)
    {
        $this->layerWmtsTileorigin = $layer_wmts_tileorigin;

        return $this;
    }

    /**
     * Get layerWmtsTileorigin
     *
     * @return string
     */
    public function getLayerWmtsTileorigin()
    {
        return $this->layerWmtsTileorigin;
    }

    /**
     * Set layerWmtsTileset
     *
     * @param string $layerWmtsTileset
     * @return Layer
     */
    public function setLayerWmtsTileset($layer_wmts_tileset)
    {
        $this->layerWmtsTileset = $layer_wmts_tileset;

        return $this;
    }

    /**
     * Get layerWmtsTileset
     *
     * @return string
     */
    public function getLayerWmtsTileset()
    {
        return $this->layerWmtsTileset;
    }

    /**
     * Set layerWmtsStyle
     *
     * @param string $layerWmtsStyle
     * @return Layer
     */
    public function setLayerWmtsStyle($layer_wmts_style)
    {
        $this->layerWmtsStyle = $layer_wmts_style;

        return $this;
    }

    /**
     * Get layerWmtsStyle
     *
     * @return string
     */
    public function getLayerWmtsStyle()
    {
        return $this->layerWmtsStyle;
    }

    /**
     * Set layerWxsname
     *
     * @param string $layerWxsname
     * @return Layer
     */
    public function setLayerWxsname($layerWxsname)
    {
        $this->layerWxsname = $layerWxsname;

        return $this;
    }

    /**
     * Get layerWxsname
     * @return the string
     */
    public function getLayerWxsname()
    {
        return $this->layerWxsname;
    }

    /**
     * Set layerMsname
     *
     * @param string $layerMsname
     * @return Layer
     */
    public function setLayerMsname($layerMsname)
    {
        $this->layerMsname = $layerMsname;

        return $this;
    }


    /**
     * Get layerMsname
     * @return the string
     */
    public function getLayerMsname()
    {
        return $this->layerMsname;
    }


    /**
     * Set layerIdentifier
     *
     * @param string $layerIdentifier
     * @return Layer
     */
    public function setlayerIdentifier($layerIdentifier)
    {
        $this->layerIdentifier = $layerIdentifier;

        return $this;
    }


    /**
     * Get layerIdentifier
     * @return the string
     */
    public function getLayerIdentifier()
    {
        return $this->layerIdentifier;
    }


    /**
     * Set layerProjectionEpsg
     *
     * @param \Carmen\ApiBundle\Entity\LexProjection $layerProjectionEpsg
     * @return Layer
     */
    public function setLayerProjectionEpsg(\Carmen\ApiBundle\Entity\LexProjection $layerProjectionEpsg = null)
    {
        $this->layerProjectionEpsg = $layerProjectionEpsg;
        return $this;
    }

    /**
     * Get layerProjectionEpsg
     *
     * @return \Carmen\ApiBundle\Entity\LexProjection
     */
    public function getLayerProjectionEpsg()
    {
        return $this->layerProjectionEpsg;
    }


    /**
     * Set layerOutputformat
     *
     * @param string $layerOutputformat
     * @return Layer
     */
    public function setLayerOutputformat($layerOutputformat)
    {
        $this->layerOutputformat = $layerOutputformat;
        return $this;
    }


    /**
     * Get layerOutputformat
     * @param string
     */
    public function getLayerOutputformat()
    {
        return $this->layerOutputformat;
    }

    /**
     * Set layerDataEncoding
     *
     * @param string $layerDataEncoding
     * @return Layer
     */
    public function setLayerDataEncoding($layerDataEncoding)
    {
        $this->layerDataEncoding = $layerDataEncoding;
        return $this;
    }


    /**
     * Get layerDataEnconding
     * @param string
     */
    public function getLayerDataEncoding()
    {
        return $this->layerDataEncoding;
    }

    /**
     * Set layerWmsQuery
     *
     * @param boolean $layerWmsQuery
     * @return Layer
     */
    public function setLayerWmsQuery($layerWmsQuery)
    {
        $this->layerWmsQuery = $layerWmsQuery;
        return $this;
    }

    /**
     * Get layerWmsQuery
     * @return the boolean
     */
    public function getLayerWmsQuery()
    {
        return $this->layerWmsQuery;
    }

    /**
     * Set layerIsBackground
     *
     * @param boolean $layerIsBackground
     * @return Layer
     */
    public function setlayerIsBackground($layerIsBackground)
    {
        $this->layerIsBackground = $layerIsBackground;
        return $this;
    }

    /**
     * Get layerIsBackground
     * @return the boolean
     */
    public function getlayerIsBackground()
    {
        return $this->layerIsBackground;
    }

    /**
     * Set layerIsDefaultBackground
     *
     * @param boolean $layerIsDefaultBackground
     * @return Layer
     */
    public function setlayerIsDefaultBackground($layerIsDefaultBackground)
    {
        $this->layerIsDefaultBackground = $layerIsDefaultBackground;
        return $this;
    }

    /**
     * Get layerIsDefaultBackground
     * @return the boolean
     */
    public function getlayerIsDefaultBackground()
    {
        return $this->layerIsDefaultBackground;
    }

    /**
     * Set layerIsDefaultBackground
     *
     * @param boolean layerBackgroundImage
     * @return Layer
     */
    public function setlayerBackgroundImage($layerBackgroundImage)
    {
        $this->layerBackgroundImage = $layerBackgroundImage;
        return $this;
    }

    /**
     * Get layerBackgroundImage
     * @return the layerBackgroundImage
     */
    public function getlayerBackgroundImage()
    {
        return $this->layerBackgroundImage;
    }

    /**
     * Add field
     *
     * @param \Carmen\ApiBundle\Entity\Field $group
     * @return Layer
     */
    public function addFields(\Carmen\ApiBundle\Entity\Field $field)
    {
        $this->fields[] = $field;
        $field->setLayer($this);
        return $this;
    }

    /**
     * Remove field
     *
     * @param \Carmen\ApiBundle\Entity\Field $field
     */
    public function removeFields(\Carmen\ApiBundle\Entity\Field $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Clear the fields collection
     * @return Layer
     */
    public function clearFields()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set fields
     *
     * @param $fields \Doctrine\Common\Collections\Collection
     * @return Layer
     */
    public function setFields(\Doctrine\Common\Collections\Collection $fields)
    {
        $this->fields = $fields ?: new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($this->fields as $field) {
            $field->setLayer($this);
        }

        return $this;
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }


    /**
     * Get the value of layerDisplayMode
     *
     * @return  \LexDisplayMode
     */
    public function getLayerDisplayMode()
    {
        return $this->layerDisplayMode;
    }

    /**
     * Set the value of layerDisplayMode
     *
     * @param \LexDisplayMode $layerDisplayMode
     *
     * @return  self
     */
    public function setLayerDisplayMode($layerDisplayMode)
    {
        $this->layerDisplayMode = $layerDisplayMode;

        return $this;
    }

    /**
     * Get the value of layerGeosjsonRepresentation
     *
     * @return  \LexGeojsonRepresentation
     */
    public function getLayerGeosjsonRepresentation()
    {
        return $this->layerGeosjsonRepresentation;
    }

    /**
     * Set the value of layerGeosjsonRepresentation
     *
     * @param \LexGeojsonRepresentation $layerGeosjsonRepresentation
     *
     * @return  self
     */
    public function setLayerGeosjsonRepresentation($layerGeosjsonRepresentation)
    {
        $this->layerGeosjsonRepresentation = $layerGeosjsonRepresentation;

        return $this;
    }

    /**
     * Get the value of geojsonParams
     *
     * @return  Collection
     */
    public function getGeojsonParams()
    {
        return $this->geojsonParams;
    }
}
