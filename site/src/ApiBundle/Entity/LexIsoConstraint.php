<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexIsoConstraint
 *
 * @ORM\Table(name="carmen.lex_iso_constraint")
 * @ORM\Entity
 */
class LexIsoConstraint
{
    /**
     * @var integer
     *
     * @ORM\Column(name="constraint_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_iso_constraint_constraint_id_seq", allocationSize=1, initialValue=1)
     */
    private $constraintId;

    /**
     * @var string
     *
     * @ORM\Column(name="constraint_name", type="string", length=255, nullable=true)
     */
    private $constraintName;


    /**
     * Get constraintId
     *
     * @return integer
     */
    public function getConstraintId()
    {
        return $this->constraintId;
    }

    /**
     * Set constraintName
     *
     * @param string $constraintName
     * @return LexIsoConstraint
     */
    public function setConstraintName($constraintName)
    {
        $this->constraintName = $constraintName;

        return $this;
    }

    /**
     * Get constraintName
     *
     * @return string
     */
    public function getConstraintName()
    {
        return $this->constraintName;
    }
}
