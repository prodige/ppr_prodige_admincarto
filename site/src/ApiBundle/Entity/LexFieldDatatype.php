<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexFieldDatatype
 *
 * @ORM\Table(name="carmen.lex_field_datatype")
 * @ORM\Entity
 */
class LexFieldDatatype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="field_datatype_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_field_datatype_field_datatype_id_seq", allocationSize=1, initialValue=1)
     */
    private $fieldDatatypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="field_datatype_name", type="string", length=255, nullable=true)
     */
    private $fieldDatatypeName;

    /**
     * @var string
     *
     * @ORM\Column(name="field_datatype_code", type="string", length=20, nullable=true)
     */
    private $fieldDatatypeCode;


    /**
     * Get fieldDatatypeId
     *
     * @return integer
     */
    public function getFieldDatatypeId()
    {
        return $this->fieldDatatypeId;
    }

    /**
     * Set fieldDatatypeName
     *
     * @param string $fieldDatatypeName
     * @return LexFieldDatatype
     */
    public function setFieldDatatypeName($fieldDatatypeName)
    {
        $this->fieldDatatypeName = $fieldDatatypeName;

        return $this;
    }

    /**
     * Get fieldDatatypeName
     *
     * @return string
     */
    public function getFieldDatatypeName()
    {
        return $this->fieldDatatypeName;
    }

    /**
     * Set fieldDatatypeCode
     *
     * @param string $fieldDatatypeCode
     * @return LexFieldDatatype
     */
    public function setFieldDatatypeCode($fieldDatatypeCode)
    {
        $this->fieldDatatypeCode = $fieldDatatypeCode;

        return $this;
    }

    /**
     * Get fieldDatatypeCode
     *
     * @return string
     */
    public function getFieldDatatypeCode()
    {
        return $this->fieldDatatypeCode;
    }
}
