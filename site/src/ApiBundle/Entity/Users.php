<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 *
 * @ORM\Table(name="carmen.users", uniqueConstraints={@ORM\UniqueConstraint(name="uk_user_email", columns={"user_email"})}, indexes={@ORM\Index(name="idx_user", columns={"account_id"})})
 * @ORM\Entity
 *
 * @ExclusionPolicy("none")
 */
class Users implements UserInterface
{
    /**
     * @var username
     */
    private $username;

    /**
     * @var password
     * @Exclude
     */
    private $password;

    /**
     * @var roles
     * @Exclude
     */
    private $roles = ["ROLE_USER"];

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.users_user_id_seq", allocationSize=1, initialValue=1)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="text", nullable=true)
     */
    private $userEmail;

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * var \Preferences
     *
     * @ORM\OneToOne(targetEntity="Preferences", mappedBy="user")
     */
    private $preferences;

    /**
     * var \Preferences
     *
     * @ORM\OneToOne(targetEntity="Contact", mappedBy="user")
     */
    private $contact;

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->userEmail;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function setUserPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function equals(UserInterface $user)
    {
        /*if (!$user instanceof ApiUser) {
            return false;
        }*/

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return User
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set account
     *
     * @param \Carmen\ApiBundle\Entity\Account $account
     * @return User
     */
    public function setAccount(\Carmen\ApiBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Carmen\ApiBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Get preference
     *
     * @return \Carmen\ApiBundle\Entity\Preferences
     */
    public function getPreferences()
    {
        return $this->preferences;
    }
}
