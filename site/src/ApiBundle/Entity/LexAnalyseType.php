<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexAnalyseType
 *
 * @ORM\Table(name="carmen.lex_analyse_type")
 * @ORM\Entity
 */
class LexAnalyseType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="analyse_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_analyse_type_analyse_type_id_seq", allocationSize=1, initialValue=1)
     */
    private $analyseTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="analyse_type_name", type="string", length=255, nullable=true)
     */
    private $analyseTypeName;

    /**
     * @var string
     *
     * @ORM\Column(name="analyse_type_code", type="string", length=255, nullable=true)
     */
    private $analyseTypeCode;


    /**
     * Get analyseTypeId
     *
     * @return integer
     */
    public function getAnalyseTypeId()
    {
        return $this->analyseTypeId;
    }

    /**
     * Set analyseTypeName
     *
     * @param string $analyseTypeName
     * @return LexAnalyseType
     */
    public function setAnalyseTypeName($analyseTypeName)
    {
        $this->analyseTypeName = $analyseTypeName;

        return $this;
    }

    /**
     * Get analyseTypeName
     *
     * @return string
     */
    public function getAnalyseTypeName()
    {
        return $this->analyseTypeName;
    }

    /**
     * Set analyseTypeCode
     *
     * @param string $analyseTypeCode
     * @return LexAnalyseType
     */
    public function setAnalyseTypeCode($analyseTypeCode)
    {
        $this->analyseTypeCode = $analyseTypeCode;

        return $this;
    }

    /**
     * Get analyseTypeCode
     *
     * @return string
     */
    public function getAnalyseTypeCode()
    {
        return $this->analyseTypeCode;
    }
}
