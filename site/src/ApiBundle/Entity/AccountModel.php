<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapUi
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.account_model", indexes={@ORM\Index(name="idx_account_model", columns={"account_id"})})
 * @ORM\Entity
 */
class AccountModel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_model_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.account_model_account_model_id_seq", allocationSize=1, initialValue=1)
     */
    private $accountModelId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_model_name", type="text", nullable=true)
     */
    private $accountModelName;

    /**
     * @var string
     *
     * @ORM\Column(name="account_model_file", type="text", nullable=true)
     */
    private $accountModelFile;

    /**
     * @var string
     *
     * @ORM\Column(name="account_model_logo", type="text", nullable=true)
     */
    private $accountModelLogo;

    /**
     * @var \Account
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;


    /**
     *
     * Set ui Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\CarmenAccountModel
     */
    public function setAccountModelId($id)
    {
        $this->accountModelId = $id;

        return $this;
    }

    /**
     * Get accountModelId
     *
     * @return integer
     */
    public function getAccountModelId()
    {
        return $this->accountModelId;
    }

    /**
     * Set accountModelFile
     *
     * @param boolean ModelFile
     * @return AccountModel
     */
    public function setAccountModelFile($accountModelFile)
    {
        $this->accountModelFile = $accountModelFile;

        return $this;
    }

    /**
     * Get accountModelFile
     *
     * @return string
     */
    public function getAccountModelFile()
    {
        return $this->accountModelFile;
    }

    /**
     * Set accountModelLogo
     *
     * @param string accountModelLogo
     * @return AccountModel
     */
    public function setAccountModelLogo($accountModelLogo)
    {
        $this->accountModelLogo = $accountModelLogo;

        return $this;
    }

    /**
     * Get accountModelLogo
     *
     * @return string
     */
    public function getAccountModelLogo()
    {
        return $this->accountModelLogo;
    }

    /**
     * Set accountModelName
     *
     * @param boolean ModelName
     * @return AccountModel
     */
    public function setAccountModelName($accountModelName)
    {
        $this->accountModelName = $accountModelName;

        return $this;
    }

    /**
     * Get accountModelName
     *
     * @return string
     */
    public function getAccountModelName()
    {
        return $this->accountModelName;
    }


    /**
     * Set account
     *
     * @param \Carmen\ApiBundle\Entity\Account $account
     * @return MapUi
     */
    public function setAccount(\Carmen\ApiBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Carmen\ApiBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }


}
