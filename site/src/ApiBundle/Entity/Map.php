<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Map
 *
 * @ORM\Table(name="carmen.map", indexes={@ORM\Index(name="idx_map", columns={"account_id"}), @ORM\Index(name="idx_map_0", columns={"map_projection_epsg"})})
 * @ORM\Entity
 *
 * @ExclusionPolicy("none")
 */
class Map
{
    /**
     * @var integer
     *
     * @ORM\Column(name="map_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.map_map_id_seq", allocationSize=1, initialValue=1)
     */
    private $mapId;

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id", nullable=false)
     * })
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="map_file", type="string", length=255, nullable=true)
     */
    private $mapFile;

    /**
     * @var string
     *
     * @ORM\Column(name="map_title", type="string", length=255, nullable=true)
     */
    private $mapTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="map_summary", type="text", nullable=true)
     */
    private $mapSummary;

    /**
     * @var boolean
     *
     * @ORM\Column(name="map_catalogable", type="boolean", nullable=true)
     */
    private $mapCatalogable;

    /**
     * @var string
     *
     * @ORM\Column(name="map_password", type="text", nullable=true)
     */
    private $mapPassword;

    /**
     * @var boolean
     *
     * @ORM\Column(name="map_model", type="boolean", nullable=true)
     */
    private $mapModel;

    /**
     * @var float
     *
     * @ORM\Column(name="map_buffer_global", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapBufferGlobal;

    /**
     * @var float
     *
     * @ORM\Column(name="map_extent_xmin", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapExtentXmin;

    /**
     * @var float
     *
     * @ORM\Column(name="map_extent_ymin", type="float", precision=10, scale=0, nullable=false)
     */
    private $mapExtentYmin;

    /**
     * @var float
     *
     * @ORM\Column(name="map_extent_xmax", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapExtentXmax;

    /**
     * @var float
     *
     * @ORM\Column(name="map_extent_ymax", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapExtentYmax;

    /**
     * @var \LexProjection
     *
     * @ORM\ManyToOne(targetEntity="LexProjection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_projection_epsg", referencedColumnName="projection_epsg")
     * })
     */
    private $mapProjectionEpsg;

    /**
     * @var float
     *
     * @ORM\Column(name="map_minscale", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapMinscale;

    /**
     * @var float
     *
     * @ORM\Column(name="map_maxscale", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapMaxscale;

    /**
     * @var string
     *
     * @ORM\Column(name="map_bgcolor", type="string", length=20, nullable=true)
     */
    private $mapBgcolor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="map_datepublication", type="datetime", nullable=true)
     */
    private $mapDatepublication;

    /**
     * @var string
     *
     * @ORM\Column(name="map_refmap_image", type="string", length=255, nullable=true)
     */
    private $mapRefmapImage;

    /**
     * @var float
     *
     * @ORM\Column(name="map_refmap_extent_xmin", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapRefmapExtentXmin;

    /**
     * @var float
     *
     * @ORM\Column(name="map_refmap_extent_ymin", type="float", precision=10, scale=0, nullable=false)
     */
    private $mapRefmapExtentYmin;

    /**
     * @var float
     *
     * @ORM\Column(name="map_refmap_extent_xmax", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapRefmapExtentXmax;

    /**
     * @var float
     *
     * @ORM\Column(name="map_refmap_extent_ymax", type="float", precision=10, scale=0, nullable=true)
     */
    private $mapRefmapExtentYmax;

    /**
     * @var string
     *
     * @ORM\Column(name="map_wmsmetadata_uuid", type="string", length=255, nullable=true)
     */
    private $mapWmsmetadataUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="map_wfsmetadata_uuid", type="string", length=255, nullable=true)
     */
    private $mapWfsmetadataUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="map_atommetadata_uuid", type="string", length=255, nullable=true)
     */
    private $mapAtommetadataUuid;

    /**
     * @var \Map
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="published_id", referencedColumnName="map_id")
     * })
     */
    private $publishedMap;

    /**
     * @var \Users
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;

    /**
     * @var MapUi
     * @ORM\OneToOne(targetEntity="MapUi", mappedBy="map", cascade={"persist"})
     */
    private $mapUi;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Keyword", inversedBy="maps", cascade={"persist"})
     * @ORM\JoinTable(name="carmen.map_keyword",
     *   joinColumns={
     *     @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="keyword_id", referencedColumnName="keyword_id")
     *   }
     * )
     */
    private $keywords;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MapTool", mappedBy="map", cascade={"persist"})
     */
    private $tools;


    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Layer", mappedBy="map", cascade={"persist"})
     * @ORM\OrderBy({"layerId" = "ASC"})
     */
    private $layers;


    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MapGroup", mappedBy="map", cascade={"persist"})
     */
    private $groups;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MapLocator", mappedBy="map", cascade={"persist"})
     */
    private $locators;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="FavoriteArea", mappedBy="map", cascade={"persist"})
     * @Exclude
     */
    private $favoriteAreas;


    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MapOnlineResources", mappedBy="map", cascade={"persist"})
     * @Exclude
     */
    private $onlineResources;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->keywords = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tools = new \Doctrine\Common\Collections\ArrayCollection();
        $this->layers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->locators = new \Doctrine\Common\Collections\ArrayCollection();
        $this->favoriteAreas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->onlineResources = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     *
     * Set mapId
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function setId($id)
    {
        $this->mapId = $id;

        return $this;
    }

    /**
     * Get mapId
     *
     * @return integer
     */
    public function getMapId()
    {
        return $this->mapId;
    }

    /**
     * Set account
     *
     * @param \Carmen\ApiBundle\Entity\Account $account
     * @return Map
     */
    public function setAccount(\Carmen\ApiBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Carmen\ApiBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set mapFile
     *
     * @param string $mapFile
     * @return Map
     */
    public function setMapFile($mapFile)
    {
        $this->mapFile = $mapFile;

        return $this;
    }

    /**
     * Get mapFile
     *
     * @return string
     */
    public function getMapFile()
    {
        return $this->mapFile;
    }

    /**
     * Set mapTitle
     *
     * @param string $mapTitle
     * @return Map
     */
    public function setMapTitle($mapTitle)
    {
        $this->mapTitle = $mapTitle;

        return $this;
    }

    /**
     * Get mapTitle
     *
     * @return string
     */
    public function getMapTitle()
    {
        return $this->mapTitle;
    }

    /**
     * Set mapSummary
     *
     * @param string $mapSummary
     * @return Map
     */
    public function setMapSummary($mapSummary)
    {
        $this->mapSummary = $mapSummary;

        return $this;
    }

    /**
     * Get mapSummary
     *
     * @return string
     */
    public function getMapSummary()
    {
        return $this->mapSummary;
    }

    /**
     * Set mapCatalogable
     *
     * @param boolean $mapCatalogable
     * @return Map
     */
    public function setMapCatalogable($mapCatalogable)
    {
        $this->mapCatalogable = $mapCatalogable;

        return $this;
    }

    /**
     * Get mapCatalogable
     *
     * @return boolean
     */
    public function getMapCatalogable()
    {
        return $this->mapCatalogable;
    }

    /**
     * Set mapPassword
     *
     * @param string $mapPassword
     * @return Map
     */
    public function setMapPassword($mapPassword)
    {
        $this->mapPassword = $mapPassword;

        return $this;
    }

    /**
     * Get mapPassword
     *
     * @return string
     */
    public function getMapPassword()
    {
        return $this->mapPassword;
    }

    /**
     * Set mapModel
     *
     * @param boolean $mapModel
     * @return Map
     */
    public function setMapModel($mapModel)
    {
        $this->mapModel = $mapModel;

        return $this;
    }

    /**
     * Get mapModel
     *
     * @return boolean
     */
    public function getMapModel()
    {
        return $this->mapModel;
    }

    /**
     * Set mapBufferGlobal
     *
     * @param float $mapBufferGlobal
     * @return Map
     */
    public function setMapBufferGlobal($mapBufferGlobal)
    {
        $this->mapBufferGlobal = $mapBufferGlobal;

        return $this;
    }

    /**
     * Get mapBufferGlobal
     *
     * @return float
     */
    public function getMapBufferGlobal()
    {
        return $this->mapBufferGlobal;
    }

    /**
     * Set mapExtentXmin
     *
     * @param float $mapExtentXmin
     * @return Map
     */
    public function setMapExtentXmin($mapExtentXmin)
    {
        $this->mapExtentXmin = $mapExtentXmin;

        return $this;
    }

    /**
     * Get mapExtentXmin
     *
     * @return float
     */
    public function getMapExtentXmin()
    {
        return $this->mapExtentXmin;
    }

    /**
     * Set mapExtentYmin
     *
     * @param float $mapExtentYmin
     * @return Map
     */
    public function setMapExtentYmin($mapExtentYmin)
    {
        $this->mapExtentYmin = $mapExtentYmin;

        return $this;
    }

    /**
     * Get mapExtentYmin
     *
     * @return float
     */
    public function getMapExtentYmin()
    {
        return $this->mapExtentYmin;
    }

    /**
     * Set mapExtentXmax
     *
     * @param float $mapExtentXmax
     * @return Map
     */
    public function setMapExtentXmax($mapExtentXmax)
    {
        $this->mapExtentXmax = $mapExtentXmax;

        return $this;
    }

    /**
     * Get mapExtentXmax
     *
     * @return float
     */
    public function getMapExtentXmax()
    {
        return $this->mapExtentXmax;
    }

    /**
     * Set mapExtentYmax
     *
     * @param float $mapExtentYmax
     * @return Map
     */
    public function setMapExtentYmax($mapExtentYmax)
    {
        $this->mapExtentYmax = $mapExtentYmax;

        return $this;
    }

    /**
     * Get mapExtentYmax
     *
     * @return float
     */
    public function getMapExtentYmax()
    {
        return $this->mapExtentYmax;
    }

    /**
     * Set mapProjectionEpsg
     *
     * @param \Carmen\ApiBundle\Entity\LexProjection $mapProjectionEpsg
     * @return Map
     */
    public function setMapProjectionEpsg(\Carmen\ApiBundle\Entity\LexProjection $mapProjectionEpsg = null)
    {
        $this->mapProjectionEpsg = $mapProjectionEpsg;

        return $this;
    }

    /**
     * Get mapProjectionEpsg
     *
     * @return \Carmen\ApiBundle\Entity\LexProjection
     */
    public function getMapProjectionEpsg()
    {
        return $this->mapProjectionEpsg;
    }

    /**
     * Get onlineressource
     * @param string $type (WMS or WFS)
     *
     * @return string
     */
    public function getMapOnlinResource($type = "WMS", $environement = "prod", $separator = ".")
    {
        $account = $this->getAccount();
        return $account->getAccountUrlData(true, $environement, $separator) . "/" .
            $type . "/" .
            $account->getAccountId() . "/" .
            $this->getMapFile() . ($type == "ATOM" ? ".atom" : "?");
    }

    /**
     * Get online map
     * @return string
     */
    public function getMapOnline($environement = "prod", $separator = ".")
    {
        return $this->getMapOnlineForMapfile($this->getAccount(), $this->getMapFile(), $environement, $separator);
    }

    /**
     * Get online map
     * @return string
     */
    public static function getMapOnlineForMapfile(Account $account, $mapfile, $environement = "prod", $separator = ".")
    {
        return $account->getAccountUrlFront(true, $environement, $separator) . "/" .
            $account->getAccountId() . "/" .
            str_replace(".map", "", $mapfile) . ".map";
    }

    /**
     * Set mapMinscale
     *
     * @param float $mapMinscale
     * @return Map
     */
    public function setMapMinscale($mapMinscale)
    {
        $this->mapMinscale = $mapMinscale;

        return $this;
    }

    /**
     * Get mapMinscale
     *
     * @return float
     */
    public function getMapMinscale()
    {
        return $this->mapMinscale;
    }

    /**
     * Set mapMaxscale
     *
     * @param float $mapMaxscale
     * @return Map
     */
    public function setMapMaxscale($mapMaxscale)
    {
        $this->mapMaxscale = $mapMaxscale;

        return $this;
    }

    /**
     * Get mapMaxscale
     *
     * @return float
     */
    public function getMapMaxscale()
    {
        return $this->mapMaxscale;
    }

    /**
     * Set mapDatepublication
     *
     * @param \DateTime $mapDatepublication
     * @return Map
     */
    public function setMapDatepublication($mapDatepublication)
    {
        $this->mapDatepublication = $mapDatepublication;

        return $this;
    }

    /**
     * Get mapDatepublication
     *
     * @return \DateTime
     */
    public function getMapDatepublication()
    {
        return $this->mapDatepublication;
    }

    /**
     * Set mapRefmapImage
     *
     * @param string $mapRefmapImage
     * @return Map
     */
    public function setMapRefmapImage($mapRefmapImage)
    {
        $this->mapRefmapImage = $mapRefmapImage;

        return $this;
    }

    /**
     * Get mapRefmapImage
     *
     * @return string
     */
    public function getMapRefmapImage()
    {
        return $this->mapRefmapImage;
    }

    /**
     * Set mapRefmapExtentXmin
     *
     * @param float $mapRefmapExtentXmin
     * @return Map
     */
    public function setMapRefmapExtentXmin($mapRefmapExtentXmin)
    {
        $this->mapRefmapExtentXmin = $mapRefmapExtentXmin;

        return $this;
    }

    /**
     * Get mapRefmapExtentXmin
     *
     * @return float
     */
    public function getMapRefmapExtentXmin()
    {
        return $this->mapRefmapExtentXmin;
    }

    /**
     * Set mapRefmapExtentYmin
     *
     * @param float $mapRefmapExtentYmin
     * @return Map
     */
    public function setMapRefmapExtentYmin($mapRefmapExtentYmin)
    {
        $this->mapRefmapExtentYmin = $mapRefmapExtentYmin;

        return $this;
    }

    /**
     * Get mapRefmapExtentYmin
     *
     * @return float
     */
    public function getMapRefmapExtentYmin()
    {
        return $this->mapRefmapExtentYmin;
    }

    /**
     * Set mapRefmapExtentXmax
     *
     * @param float $mapRefmapExtentXmax
     * @return Map
     */
    public function setMapRefmapExtentXmax($mapRefmapExtentXmax)
    {
        $this->mapRefmapExtentXmax = $mapRefmapExtentXmax;

        return $this;
    }

    /**
     * Get mapRefmapExtentXmax
     *
     * @return float
     */
    public function getMapRefmapExtentXmax()
    {
        return $this->mapRefmapExtentXmax;
    }

    /**
     * Set mapRefmapExtentYmax
     *
     * @param float $mapRefmapExtentYmax
     * @return Map
     */
    public function setMapRefmapExtentYmax($mapRefmapExtentYmax)
    {
        $this->mapRefmapExtentYmax = $mapRefmapExtentYmax;

        return $this;
    }

    /**
     * Get mapRefmapExtentYmax
     *
     * @return float
     */
    public function getMapRefmapExtentYmax()
    {
        return $this->mapRefmapExtentYmax;
    }

    /**
     * Set publishedMap
     *
     * @param \Carmen\ApiBundle\Entity\Map $publishedMap
     * @return Map
     */
    public function setPublishedMap(\Carmen\ApiBundle\Entity\Map $publishedMap = null)
    {
        $this->publishedMap = $publishedMap;

        return $this;
    }

    /**
     * Get publishedMap
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getPublishedMap()
    {
        return $this->publishedMap;
    }

    /**
     * Set user
     *
     * @param \Carmen\ApiBundle\Entity\Users $user
     * @return Map
     */
    public function setUser(\Carmen\ApiBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Carmen\ApiBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get mapUi
     *
     * @return MapUi
     */
    public function getMapUi()
    {
        return $this->mapUi;
    }

    /**
     * Set mapUi
     *
     * @param MapUi $mapUi
     * @return Map
     */
    public function setMapUi(MapUi $mapUi)
    {
        $this->mapUi = $mapUi;
        $this->mapUi->setMap($this);

        return $this;
    }

    /**
     * Add keyword
     *
     * @param \Carmen\ApiBundle\Entity\Keyword $group
     * @return Map
     */
    public function addKeywords(\Carmen\ApiBundle\Entity\Keyword $keyword)
    {
        $this->keywords[] = $keyword;

        return $this;
    }

    /**
     * Remove keyword
     *
     * @param \Carmen\ApiBundle\Entity\Keyword $keyword
     */
    public function removeKeywords(\Carmen\ApiBundle\Entity\Keyword $keyword)
    {
        $this->keywords->removeElement($keyword);
    }

    /**
     * Clear the tools collection
     * @return Map
     */
    public function clearKeywords()
    {
        $this->keywords = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set keywords
     *
     * @param $keywords \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setKeywords(\Doctrine\Common\Collections\Collection $keywords)
    {
        $this->keywords = $keywords ?: new \Doctrine\Common\Collections\ArrayCollection();

        return $this;
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeywords()
    {
        return $this->keywords;
    }


    /**
     * Add tool
     *
     * @param \Carmen\ApiBundle\Entity\MapTool $tool
     * @return Map
     */
    public function addTools(\Carmen\ApiBundle\Entity\MapTool $tool)
    {
        $this->tools[] = $tool;
        $tool->setMap($this);
        return $this;
    }

    /**
     * Remove tool
     *
     * @param \Carmen\ApiBundle\Entity\MapTool $tool
     */
    public function removeTools(\Carmen\ApiBundle\Entity\MapTool $tool)
    {
        $this->tools->removeElement($tool);
    }

    /**
     * Clear the tools collection
     * @return Map
     */
    public function clearTools()
    {
        $this->tools = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set tools
     *
     * @param $tools \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setTools(\Doctrine\Common\Collections\Collection $tools)
    {
        $this->tools = $tools ?: new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($this->tools as $tool) {
            $tool->setMap($this);
        }

        return $this;
    }

    /**
     * Get tools
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTools()
    {
        return $this->tools;
    }


    /**
     * Get the mapTool for a lexTool
     *
     * @param LexTool $tool
     * @return \Carmen\ApiBundle\Entity\MapTool
     */
    public function getMaptool(LexTool $tool)
    {
        $tools = $this->getTools();
        $enabled = false;
        $maptool = null;
        foreach ($tools as $maptool) {
            $maptool instanceof MapTool;
            $enabled = ($maptool->getTool()->getToolId() == $tool->getToolId());
            if ($enabled) break;
        }
        if (!$enabled) {
            return null;
        }
        return $maptool;
    }


    /**
     * Evaluate if a tool is enabled for this map
     * @param LexTool $tool
     * @return boolean
     */
    public function hasTool(LexTool $tool)
    {
        $enabled = false;
        $tools = $this->getTools();
        foreach ($tools as $maptool) {
            $maptool instanceof MapTool;
            $enabled = ($maptool->getTool()->getToolId() == $tool->getToolId());
            if ($enabled) break;
        }
        return $enabled;
    }

    /**
     * Add layer
     *
     * @param \Carmen\ApiBundle\Entity\Layer $group
     * @return Map
     */
    public function addLayers(\Carmen\ApiBundle\Entity\Layer $layer)
    {
        $this->layers[] = $layer;
        $layer->setMap($this);
        return $this;
    }

    /**
     * Remove layer
     *
     * @param \Carmen\ApiBundle\Entity\Layer $layer
     */
    public function removeLayers(\Carmen\ApiBundle\Entity\Layer $layer)
    {
        $this->layers->removeElement($layer);
    }

    /**
     * Clear the layers collection
     * @return Map
     */
    public function clearLayers()
    {
        $this->layers = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set layers
     *
     * @param $layers \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setLayers(\Doctrine\Common\Collections\Collection $layers)
    {
        $this->layers = $layers ?: new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($this->layers as $layer) {
            $layer->setMap($this);
        }

        return $this;
    }

    /**
     * Get layers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLayers()
    {
        return $this->layers;
    }

    /**
     * Get layer found by it's ID
     * @param integer layerId
     * @return \Carmen\ApiBundle\Entity\Layer
     */
    public function getLayerById($layerId)
    {
        $found = null;
        foreach ($this->layers as $layer) {
            if ($layer->getLayerId() == $layerId) $found = $layer;
        }
        return $found;
    }

    /**
     * Add group
     *
     * @param \Carmen\ApiBundle\Entity\MapGroup $group
     * @return Map
     */
    public function addGroups(\Carmen\ApiBundle\Entity\MapGroup $group)
    {
        $this->groups[] = $group;
        $group->setMap($this);
        return $this;
    }

    /**
     * Remove group
     *
     * @param \Carmen\ApiBundle\Entity\MapGroup $group
     */
    public function removeGroups(\Carmen\ApiBundle\Entity\MapGroup $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Clear the groups collection
     * @return Map
     */
    public function clearGroups()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set groups
     *
     * @param $groups \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setGroups(\Doctrine\Common\Collections\Collection $groups)
    {
        $this->groups = $groups ?: new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($this->groups as $group) {
            $group->setMap($this);
        }

        return $this;
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add locator
     *
     * @param \Carmen\ApiBundle\Entity\MapLocator $locator
     * @return Map
     */
    public function addLocators(\Carmen\ApiBundle\Entity\MapLocator $locator)
    {
        $this->locators[] = $locator;
        $locator->setMap($this);

        return $this;
    }

    /**
     * Remove locator
     *
     * @param \Carmen\ApiBundle\Entity\MapLocator $locator
     */
    public function removeLocators(\Carmen\ApiBundle\Entity\MapLocator $locator)
    {
        $this->locators->removeElement($locator);
    }

    /**
     * Set locators
     *
     * @param $locators \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setLocators(\Doctrine\Common\Collections\Collection $locators)
    {
        $this->locators = $locators;
        foreach ($this->locators as $locator) {
            $locator->setMap($this);
        }

        return $this;
    }

    /**
     * Get locators
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocators()
    {
        return $this->locators;
    }


    /**
     * Clear the locators collection
     * @return Map
     */
    public function clearLocators()
    {
        $this->locators = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Add favoriteArea
     *
     * @param \Carmen\ApiBundle\Entity\FavoriteArea $favoriteArea
     * @return Map
     */
    public function addFavoriteAreas(\Carmen\ApiBundle\Entity\FavoriteArea $favoriteArea)
    {
        $this->favoriteAreas[] = $favoriteArea;

        return $this;
    }

    /**
     * Remove favoriteArea
     *
     * @param \Carmen\ApiBundle\Entity\FavoriteArea $favoriteArea
     */
    public function removeFavoriteAreas(\Carmen\ApiBundle\Entity\FavoriteArea $favoriteArea)
    {
        $this->favoriteAreas->removeElement($favoriteArea);
    }

    /**
     * Clear the tools collection
     * @return Map
     */
    public function clearFavoriteAreas()
    {
        $this->favoriteAreas = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set favoriteAreas
     *
     * @param $favoriteAreas \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setFavoriteAreas(\Doctrine\Common\Collections\Collection $favoriteAreas)
    {
        $this->favoriteAreas = $favoriteAreas ?: new \Doctrine\Common\Collections\ArrayCollection();

        return $this;
    }

    /**
     * Get favoriteAreas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteAreas()
    {
        return $this->favoriteAreas;
    }

    ###################################### UNSAVED PROPERTIES ######################################

    /**
     * @var string
     * @Exclude
     */
    private $fullMapfilePath;
    private $mapfileName;

    /**
     * Get fullMapfilePath
     * @return the string
     */
    public function getFullMapfilePath()
    {
        return $this->fullMapfilePath;
    }

    /**
     * Set fullMapfilePath
     * @param string $fullMapfilePath
     * @return Map
     */
    public function setFullMapfilePath($fullMapfilePath)
    {
        $this->fullMapfilePath = $fullMapfilePath;
        return $this;
    }

    /**
     * Get mapfileName
     * @return the string
     */
    public function getMapfileName()
    {
        return $this->mapfileName;
    }

    /**
     * Set mapfileName
     * @param string $mapfileName
     * @return Map
     */
    public function setMapfileName($mapfileName)
    {
        $this->mapfileName = $mapfileName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getMapWmsmetadataUuid()
    {
        return $this->mapWmsmetadataUuid;
    }

    /**
     *
     * @param string $mapWmsmetadataUuid
     */
    public function setMapWmsmetadataUuid($mapWmsmetadataUuid)
    {
        $this->mapWmsmetadataUuid = $mapWmsmetadataUuid;

        return $this;
    }

    /**
     *
     * @return the string
     */
    public function getMapWfsmetadataUuid()
    {
        return $this->mapWfsmetadataUuid;
    }

    /**
     *
     * @param string $mapWfsmetadataUuid
     */
    public function setMapWfsmetadataUuid($mapWfsmetadataUuid)
    {
        $this->mapWfsmetadataUuid = $mapWfsmetadataUuid;

        return $this;
    }

    /**
     *
     * @return the string
     */
    public function getMapAtommetadataUuid()
    {
        return $this->mapAtommetadataUuid;
    }

    /**
     *
     * @param string $mapAtommetadataUuid
     */
    public function setMapAtommetadataUuid($mapAtommetadataUuid)
    {
        $this->mapAtommetadataUuid = $mapAtommetadataUuid;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getMapBgcolor()
    {
        return $this->mapBgcolor;
    }

    /**
     *
     * @param string $mapBgcolor
     */
    public function setMapBgcolor($mapBgcolor)
    {
        $this->mapBgcolor = $mapBgcolor;
        return $this;
    }

    /**
     * Add OnlineResource
     *
     * @param \Carmen\ApiBundle\Entity\MapOnlineResources $OnlineResource
     * @return Map
     */
    public function addOnlineResources(\Carmen\ApiBundle\Entity\MapOnlineResources $OnlineResource)
    {
        $this->onlineResources[] = $OnlineResource;
        $OnlineResource->setMap($this);
        return $this;
    }

    /**
     * Remove OnlineResource
     *
     * @param \Carmen\ApiBundle\Entity\MapOnlineResources $OnlineResource
     */
    public function removeOnlineResources(\Carmen\ApiBundle\Entity\MapOnlineResources $OnlineResource)
    {
        $this->onlineResources->removeElement($OnlineResource);
    }

    /**
     * Clear the onlineResources collection
     * @return Map
     */
    public function clearOnlineResources()
    {
        $this->onlineResources = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Set onlineResources
     *
     * @param $onlineResources \Doctrine\Common\Collections\Collection
     * @return Map
     */
    public function setOnlineResources(\Doctrine\Common\Collections\Collection $onlineResources)
    {
        $this->onlineResources = $onlineResources ?: new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($this->onlineResources as $OnlineResource) {
            $OnlineResource->setMap($this);
        }

        return $this;
    }

    /**
     * Get onlineResources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOnlineResources()
    {
        return $this->onlineResources;
    }

    /**
     * Get OnlineResource found by it's ID
     * @param integer onlineResourceId
     * @return \Carmen\ApiBundle\Entity\MapOnlineResources
     */
    public function getOnlineResourceById($onlineResourceId)
    {
        $found = null;
        foreach ($this->onlineResources as $OnlineResource) {
            if ($OnlineResource->getOnlineResourceId() == $onlineResourceId) $found = $OnlineResource;
        }
        return $found;
    }


    /**
     * get default baseLayer (if none and map has base layer, just take one)
     * @return null
     */
    public function getDefaultBaseLayer()
    {
        $defaultBayerLayer = null;
        foreach ($this->getLayers() as $layer) {
            if ($layer->getlayerIsBackground()) {
                $defaultBayerLayer = $layer->getLayerId();
                if ($layer->getlayerIsDefaultBackground()) {
                    return $defaultBayerLayer;
                    //do nothing
                }
            }
        }
        return $defaultBayerLayer;
    }
}
