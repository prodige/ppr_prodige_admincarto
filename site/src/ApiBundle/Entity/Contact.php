<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Contact
 *
 * @ORM\Table(name="carmen.contact", indexes={@ORM\Index(name="idx_contact", columns={"user_id"})})
 * @ORM\Entity
 *
 * @ExclusionPolicy("none")
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="contact_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.contact_contact_id_seq", allocationSize=1, initialValue=1)
     */
    private $contactId;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_organization", type="string", length=255, nullable=true)
     */
    private $contactOrganization;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_name", type="string", length=255, nullable=true)
     */
    private $contactName;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_position", type="string", length=255, nullable=true)
     */
    private $contactPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_address", type="string", length=1024, nullable=true)
     */
    private $contactAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_zipcode", type="string", length=10, nullable=true)
     */
    private $contactZipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_city", type="string", length=255, nullable=true)
     */
    private $contactCity;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_unit", type="string", length=255, nullable=true)
     */
    private $contactUnit;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_country", type="string", length=255, nullable=true)
     */
    private $contactCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=255, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone", type="string", length=255, nullable=true)
     */
    private $contactPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_fax", type="string", length=255, nullable=true)
     */
    private $contactFax;

    /**
     * @var \Users
     *
     * @ORM\OneToOne(targetEntity="Users", inversedBy="contact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     * @Exclude
     */
    private $user;


    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set contactOrganization
     *
     * @param string $contactOrganization
     * @return Contact
     */
    public function setContactOrganization($contactOrganization)
    {
        $this->contactOrganization = $contactOrganization;

        return $this;
    }

    /**
     * Get contactOrganization
     *
     * @return string
     */
    public function getContactOrganization()
    {
        return $this->contactOrganization;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     * @return Contact
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Set contactPosition
     *
     * @param string $contactPosition
     * @return Contact
     */
    public function setContactPosition($contactPosition)
    {
        $this->contactPosition = $contactPosition;

        return $this;
    }

    /**
     * Get contactPosition
     *
     * @return string
     */
    public function getContactPosition()
    {
        return $this->contactPosition;
    }

    /**
     * Set contactAddress
     *
     * @param string $contactAddress
     * @return Contact
     */
    public function setContactAddress($contactAddress)
    {
        $this->contactAddress = $contactAddress;

        return $this;
    }

    /**
     * Get contactAddress
     *
     * @return string
     */
    public function getContactAddress()
    {
        return $this->contactAddress;
    }

    /**
     * Set contactZipcode
     *
     * @param string $contactZipcode
     * @return Contact
     */
    public function setContactZipcode($contactZipcode)
    {
        $this->contactZipcode = $contactZipcode;

        return $this;
    }

    /**
     * Get contactZipcode
     *
     * @return string
     */
    public function getContactZipcode()
    {
        return $this->contactZipcode;
    }

    /**
     * Set contactCity
     *
     * @param string $contactCity
     * @return Contact
     */
    public function setContactCity($contactCity)
    {
        $this->contactCity = $contactCity;

        return $this;
    }

    /**
     * Get contactCity
     *
     * @return string
     */
    public function getContactCity()
    {
        return $this->contactCity;
    }

    /**
     * Set contactUnit
     *
     * @param string $contactUnit
     * @return Contact
     */
    public function setContactUnit($contactUnit)
    {
        $this->contactUnit = $contactUnit;

        return $this;
    }

    /**
     * Get contactUnit
     *
     * @return string
     */
    public function getContactUnit()
    {
        return $this->contactUnit;
    }

    /**
     * Set contactCountry
     *
     * @param string $contactCountry
     * @return Contact
     */
    public function setContactCountry($contactCountry)
    {
        $this->contactCountry = $contactCountry;

        return $this;
    }

    /**
     * Get contactCountry
     *
     * @return string
     */
    public function getContactCountry()
    {
        return $this->contactCountry;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     * @return Contact
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     * @return Contact
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * Set contactFax
     *
     * @param string $contactFax
     * @return Contact
     */
    public function setContactFax($contactFax)
    {
        $this->contactFax = $contactFax;

        return $this;
    }

    /**
     * Get contactFax
     *
     * @return string
     */
    public function getContactFax()
    {
        return $this->contactFax;
    }

    /**
     * Set user
     *
     * @param \Carmen\ApiBundle\Entity\Users $user
     * @return Contact
     */
    public function setUser(\Carmen\ApiBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Carmen\ApiBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
