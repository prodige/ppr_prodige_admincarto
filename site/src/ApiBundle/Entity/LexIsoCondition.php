<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexIsoCondition
 *
 * @ORM\Table(name="carmen.lex_iso_condition")
 * @ORM\Entity
 */
class LexIsoCondition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="condition_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_iso_condition_condition_id_seq", allocationSize=1, initialValue=1)
     */
    private $conditionId;

    /**
     * @var string
     *
     * @ORM\Column(name="condition_name", type="string", length=255, nullable=true)
     */
    private $conditionName;


    /**
     * Get conditionId
     *
     * @return integer
     */
    public function getConditionId()
    {
        return $this->conditionId;
    }

    /**
     * Set conditionName
     *
     * @param string $conditionName
     * @return LexIsoCondition
     */
    public function setConditionName($conditionName)
    {
        $this->conditionName = $conditionName;

        return $this;
    }

    /**
     * Get conditionName
     *
     * @return string
     */
    public function getConditionName()
    {
        return $this->conditionName;
    }
}
