<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexDisplayMode
 *
 * @ORM\Table(name="carmen.lex_display_mode")
 * @ORM\Entity(repositoryClass="Carmen\ApiBundle\Repository\LexDisplayModeRepository")
 */
class LexDisplayMode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="display_mode_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_display_mode_display_mode_id_seq", allocationSize=1, initialValue=1)
     */
    private $displayModeId;

    /**
     * @var string
     *
     * @ORM\Column(name="display_mode_name", type="string", length=255, nullable=true)
     */
    private $displayModeName;

    /**
     * Get displayModeId
     *
     * @return int
     */
    public function getDisplayModeId()
    {
        return $this->displayModeId;
    }

    /**
     * Get the value of displayModeName
     *
     * @return  string
     */
    public function getdisplayModeName()
    {
        return $this->displayModeName;
    }

    /**
     * Set the value of displayModeName
     *
     * @param string $displayModeName
     *
     * @return  self
     */
    public function setdisplayModeName($displayModeName)
    {
        $this->displayModeName = $displayModeName;

        return $this;
    }
}

