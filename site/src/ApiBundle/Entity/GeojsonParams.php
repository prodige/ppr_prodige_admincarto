<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeojsonParams
 *
 * @ORM\Table(name="carmen.geojson_params")
 * @ORM\Entity(repositoryClass="Carmen\ApiBundle\Repository\GeojsonParamsRepository")
 */
class GeojsonParams
{
    /**
     * @var int
     *
     * @ORM\Column(name="geojson_params_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.geojson_params_geojson_params_id_seq", allocationSize=1, initialValue=1)
     */
    private $geojsonParamsId;

    /**
     * @var int
     *
     * @ORM\Column(name="geojson_distance", type="integer", nullable=false)
     */
    private $geojsonDistance = 10;

    /**
     * @var boolean
     *
     * @ORM\Column(name="geojson_single_color", type="boolean")
     */
    private $geojsonSingleColor = false;

    /**
     * @var string
     *
     * @ORM\Column(name="geojson_color", type="string", nullable=true)
     */
    private $geojsonColor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="geojson_fixed_size", type="boolean")
     */
    private $geojsonFixedSize = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="geojson_size", type="integer", nullable=true)
     */
    private $geojsonSize;

    /**
     * @var boolean
     *
     * @ORM\Column(name="geojson_transition_effect", type="boolean")
     */
    private $geojsonTransitionEffect = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="geojson_zoom_on_click", type="boolean")
     */
    private $geojsonZoomOnClick = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="geojson_with_text", type="boolean")
     */
    private $geojsonWithText = false;

    /**
     * @var \Layer
     *
     * @ORM\ManyToOne(targetEntity="Layer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layer_id", referencedColumnName="layer_id")
     * })
     */
    private $layer;


    /**
     * Get the value of geojsonDistance
     *
     * @return  int
     */
    public function getGeojsonDistance()
    {
        return $this->geojsonDistance;
    }

    /**
     * Get the value of geojsonParamsId
     *
     * @return  int
     */
    public function getId()
    {
        return $this->geojsonParamsId;
    }

    /**
     * Set the value of geojsonDistance
     *
     * @param int $geojsonDistance
     *
     * @return  self
     */
    public function setGeojsonDistance($geojsonDistance)
    {
        $this->geojsonDistance = $geojsonDistance;

        return $this;
    }

    /**
     * Get the value of geojsonParamsId
     *
     * @return  int
     */
    public function getGeojsonParamsId()
    {
        return $this->geojsonParamsId;
    }

    /**
     * Get the value of geojsonSingleColor
     *
     * @return  boolean
     */
    public function getGeojsonSingleColor()
    {
        return $this->geojsonSingleColor;
    }

    /**
     * Set the value of geojsonSingleColor
     *
     * @param boolean $geojsonSingleColor
     *
     * @return  self
     */
    public function setGeojsonSingleColor($geojsonSingleColor)
    {
        $this->geojsonSingleColor = $geojsonSingleColor;

        return $this;
    }

    /**
     * Get the value of geojsonColor
     *
     * @return  string
     */
    public function getGeojsonColor()
    {
        return $this->geojsonColor;
    }

    /**
     * Set the value of geojsonColor
     *
     * @param string $geojsonColor
     *
     * @return  self
     */
    public function setGeojsonColor($geojsonColor)
    {
        $this->geojsonColor = $geojsonColor;

        return $this;
    }

    /**
     * Get the value of geojsonFixedSize
     *
     * @return  boolean
     */
    public function getGeojsonFixedSize()
    {
        return $this->geojsonFixedSize;
    }

    /**
     * Set the value of geojsonFixedSize
     *
     * @param boolean $geojsonFixedSize
     *
     * @return  self
     */
    public function setGeojsonFixedSize($geojsonFixedSize)
    {
        $this->geojsonFixedSize = $geojsonFixedSize;

        return $this;
    }

    /**
     * Get the value of geojsonSize
     *
     * @return  integer
     */
    public function getGeojsonSize()
    {
        return $this->geojsonSize;
    }

    /**
     * Set the value of geojsonSize
     *
     * @param integer $geojsonSize
     *
     * @return  self
     */
    public function setGeojsonSize($geojsonSize)
    {
        $this->geojsonSize = $geojsonSize;

        return $this;
    }

    /**
     * Get the value of geojsonTransitionEffect
     *
     * @return  boolean
     */
    public function getGeojsonTransitionEffect()
    {
        return $this->geojsonTransitionEffect;
    }

    /**
     * Set the value of geojsonTransitionEffect
     *
     * @param boolean $geojsonTransitionEffect
     *
     * @return  self
     */
    public function setGeojsonTransitionEffect($geojsonTransitionEffect)
    {
        $this->geojsonTransitionEffect = $geojsonTransitionEffect;

        return $this;
    }

    /**
     * Get the value of geojsonZoomOnClick
     *
     * @return  boolean
     */
    public function getGeojsonZoomOnClick()
    {
        return $this->geojsonZoomOnClick;
    }

    /**
     * Set the value of geojsonZoomOnClick
     *
     * @param boolean $geojsonZoomOnClick
     *
     * @return  self
     */
    public function setGeojsonZoomOnClick($geojsonZoomOnClick)
    {
        $this->geojsonZoomOnClick = $geojsonZoomOnClick;

        return $this;
    }

    /**
     * Get the value of layer
     *
     * @return  \Layer
     */
    public function getLayer()
    {
        return $this->layer;
    }

    /**
     * Set the value of layer
     *
     * @param \Layer $layer
     *
     * @return  self
     */
    public function setLayer($layer)
    {
        $this->layer = $layer;

        return $this;
    }

    /**
     * Get the value of geojsonWithText
     *
     * @return  boolean
     */
    public function getGeojsonWithText()
    {
        return $this->geojsonWithText;
    }

    /**
     * Set the value of geojsonWithText
     *
     * @param boolean $geojsonWithText
     *
     * @return  self
     */
    public function setGeojsonWithText($geojsonWithText)
    {
        $this->geojsonWithText = $geojsonWithText;

        return $this;
    }
}

