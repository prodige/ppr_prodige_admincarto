<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexIsoTheme
 *
 * @ORM\Table(name="carmen.lex_iso_theme")
 * @ORM\Entity
 */
class LexIsoTheme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="theme_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_iso_theme_theme_id_seq", allocationSize=1, initialValue=1)
     */
    private $themeId;

    /**
     * @var string
     *
     * @ORM\Column(name="theme_name", type="string", length=255, nullable=true)
     */
    private $themeName;

    /**
     * @var string
     *
     * @ORM\Column(name="theme_code", type="string", length=255, nullable=true)
     */
    private $themeCode;


    /**
     * Get themeId
     *
     * @return integer
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * Set themeName
     *
     * @param string $themeName
     * @return LexIsoTheme
     */
    public function setThemeName($themeName)
    {
        $this->themeName = $themeName;

        return $this;
    }

    /**
     * Get themeName
     *
     * @return string
     */
    public function getThemeName()
    {
        return $this->themeName;
    }

    /**
     * Set themeCode
     *
     * @param string $themeCode
     * @return LexIsoTheme
     */
    public function setThemeCode($themeCode)
    {
        $this->themeCode = $themeCode;

        return $this;
    }

    /**
     * Get themeCode
     *
     * @return string
     */
    public function getThemeCode()
    {
        return $this->themeCode;
    }
}
