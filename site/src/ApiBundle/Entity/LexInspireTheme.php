<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexInspireTheme
 *
 * @ORM\Table(name="carmen.lex_inspire_theme")
 * @ORM\Entity
 */
class LexInspireTheme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="theme_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_inspire_theme_theme_id_seq", allocationSize=1, initialValue=1)
     */
    private $themeId;

    /**
     * @var string
     *
     * @ORM\Column(name="theme_name", type="string", length=255, nullable=true)
     */
    private $themeName;


    /**
     * Get themeId
     *
     * @return integer
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * Set themeName
     *
     * @param string $themeName
     * @return LexInspireTheme
     */
    public function setThemeName($themeName)
    {
        $this->themeName = $themeName;

        return $this;
    }

    /**
     * Get themeName
     *
     * @return string
     */
    public function getThemeName()
    {
        return $this->themeName;
    }
}
