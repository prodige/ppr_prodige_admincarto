<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * FavoriteArea
 *
 * @ORM\Table(name="carmen.favorite_area", indexes={@ORM\Index(name="idx_favorite_areas", columns={"map_id"})})
 * @ORM\Entity
 */
class FavoriteArea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.favorite_area_area_id_seq", allocationSize=1, initialValue=1)
     */
    private $areaId;

    /**
     * @var string
     *
     * @ORM\Column(name="area_name", type="string", length=255, nullable=true)
     */
    private $areaName;

    /**
     * @var string
     *
     * @ORM\Column(name="area_xmin", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $areaXmin;

    /**
     * @var string
     *
     * @ORM\Column(name="area_ymin", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $areaYmin;

    /**
     * @var string
     *
     * @ORM\Column(name="area_xmax", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $areaXmax;

    /**
     * @var string
     *
     * @ORM\Column(name="area_ymax", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $areaYmax;

    /**
     * @var \Map
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;


    /**
     *
     * Set area Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\FavoriteArea
     */
    public function setId($id)
    {
        $this->areaId = $id;

        return $this;
    }


    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set areaName
     *
     * @param string $areaName
     * @return FavoriteArea
     */
    public function setAreaName($areaName)
    {
        $this->areaName = $areaName;

        return $this;
    }

    /**
     * Get areaName
     *
     * @return string
     */
    public function getAreaName()
    {
        return $this->areaName;
    }

    /**
     * Set areaXmin
     *
     * @param string $areaXmin
     * @return FavoriteArea
     */
    public function setAreaXmin($areaXmin)
    {
        $this->areaXmin = $areaXmin;

        return $this;
    }

    /**
     * Get areaXmin
     *
     * @return string
     */
    public function getAreaXmin()
    {
        return $this->areaXmin;
    }

    /**
     * Set areaYmin
     *
     * @param string $areaYmin
     * @return FavoriteArea
     */
    public function setAreaYmin($areaYmin)
    {
        $this->areaYmin = $areaYmin;

        return $this;
    }

    /**
     * Get areaYmin
     *
     * @return string
     */
    public function getAreaYmin()
    {
        return $this->areaYmin;
    }

    /**
     * Set areaXmax
     *
     * @param string $areaXmax
     * @return FavoriteArea
     */
    public function setAreaXmax($areaXmax)
    {
        $this->areaXmax = $areaXmax;

        return $this;
    }

    /**
     * Get areaXmax
     *
     * @return string
     */
    public function getAreaXmax()
    {
        return $this->areaXmax;
    }

    /**
     * Set areaYmax
     *
     * @param string $areaYmax
     * @return FavoriteArea
     */
    public function setAreaYmax($areaYmax)
    {
        $this->areaYmax = $areaYmax;

        return $this;
    }

    /**
     * Get areaYmax
     *
     * @return string
     */
    public function getAreaYmax()
    {
        return $this->areaYmax;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return FavoriteArea
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }
}
