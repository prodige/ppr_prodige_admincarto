<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UiModel
 *
 * @ORM\Table(name="carmen.ui_model", indexes={@ORM\Index(name="idx_ui_model", columns={"ui_id"}),@ORM\Index(name="idx_ui_model_0", columns={"account_model_id"})})
 * @ORM\Entity
 */
class UiModel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ui_model_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.ui_model_ui_model_id_seq", allocationSize=1, initialValue=1)
     */
    private $uiModelId;

    /**
     * @var integer
     *
     * @ORM\Column(name="model_rank", type="integer", nullable=true)
     */
    private $modelRank;

    /**
     * @var \AccountModel
     *
     * @ORM\ManyToOne(targetEntity="AccountModel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_model_id", referencedColumnName="account_model_id")
     * })
     */
    private $accountModel;

    /**
     * @var \MapUi
     *
     * @ORM\ManyToOne(targetEntity="MapUi", inversedBy="uiModels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ui_id", referencedColumnName="ui_id")
     * })
     */
    private $mapUi;


    /**
     *
     * Set ui Model Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\UiModel
     */
    public function setId($id)
    {
        $this->uiModelId = $id;

        return $this;
    }

    /**
     * Get uiModelId
     *
     * @return integer
     */
    public function getUiModelId()
    {
        return $this->uiModelId;
    }

    /**
     * Set modelRank
     *
     * @param integer $modelRank
     * @return UiModel
     */
    public function setModelRank($modelRank)
    {
        $this->modelRank = $modelRank;

        return $this;
    }

    /**
     * Get modelRank
     *
     * @return integer
     */
    public function getModelRank()
    {
        return $this->modelRank;
    }

    /**
     * Set accountModel
     *
     * @param \Carmen\ApiBundle\Entity\AccountModel $accountModel
     * @return UiModel
     */
    public function setAccountModel(\Carmen\ApiBundle\Entity\AccountModel $accountModel = null)
    {
        $this->accountModel = $accountModel;

        return $this;
    }

    /**
     * Get accountModel
     *
     * @return \Carmen\ApiBundle\Entity\AccountModel
     */
    public function getAccountModel()
    {
        return $this->accountModel;
    }


    /**
     * Set mapUi
     *
     * @param \Carmen\ApiBundle\Entity\MapUi $ui
     * @return UiModel
     */
    public function setMapUi(\Carmen\ApiBundle\Entity\MapUi $mapUi = null)
    {
        $this->mapUi = $mapUi;

        return $this;
    }

    /**
     * Get mapUi
     *
     * @return \Carmen\ApiBundle\Entity\MapUi
     */
    public function getMapUi()
    {
        return $this->mapUi;
    }
}
