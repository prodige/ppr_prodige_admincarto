<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexGeojsonRepresentation
 *
 * @ORM\Table(name="lex_geojson_representation")
 * @ORM\Entity(repositoryClass="Carmen\ApiBundle\Repository\LexGeojsonRepresentationRepository")
 */
class LexGeojsonRepresentation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="representation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_representation_id_seq", allocationSize=1, initialValue=1)
     */
    private $representationId;

    /**
     * @var string
     *
     * @ORM\Column(name="representation_name", type="string", length=255, nullable=false)
     */
    private $representationName;


    /**
     * Get the value of representationName
     *
     * @return  string
     */
    public function getRepresentationName()
    {
        return $this->representationName;
    }

    /**
     * Set the value of representationName
     *
     * @param string $representationName
     *
     * @return  self
     */
    public function setRepresentationName(string $representationName)
    {
        $this->representationName = $representationName;

        return $this;
    }

    /**
     * Get the value of representationId
     *
     * @return  integer
     */
    public function getRepresentationId()
    {
        return $this->representationId;
    }

    /**
     * Set the value of representationId
     *
     * @param integer $representationId
     *
     * @return  self
     */
    public function setRepresentationId($representationId)
    {
        $this->representationId = $representationId;

        return $this;
    }
}

