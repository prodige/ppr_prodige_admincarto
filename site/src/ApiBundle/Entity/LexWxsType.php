<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexWxsType
 *
 * @ORM\Table(name="carmen.lex_wxs_type")
 * @ORM\Entity
 */
class LexWxsType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="wxs_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_wxs_type_wxs_type_id_seq", allocationSize=1, initialValue=1)
     */
    private $wxsTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="wxs_type_name", type="string", length=100, nullable=true)
     */
    private $wxsTypeName;


    /**
     * Get wxsTypeId
     *
     * @return integer
     */
    public function getWxsTypeId()
    {
        return $this->wxsTypeId;
    }

    /**
     * Set wxsTypeName
     *
     * @param string $wxsTypeName
     * @return LexWxsType
     */
    public function setWxsTypeName($wxsTypeName)
    {
        $this->wxsTypeName = $wxsTypeName;

        return $this;
    }

    /**
     * Get wxsTypeName
     *
     * @return string
     */
    public function getWxsTypeName()
    {
        return $this->wxsTypeName;
    }
}
