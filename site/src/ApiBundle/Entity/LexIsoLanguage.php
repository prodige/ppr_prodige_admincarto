<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexIsoLanguage
 *
 * @ORM\Table(name="carmen.lex_iso_language")
 * @ORM\Entity
 */
class LexIsoLanguage
{
    /**
     * @var string
     *
     * @ORM\Column(name="language_code", type="string", length=3, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_iso_language_language_code_seq", allocationSize=1, initialValue=1)
     */
    private $languageCode;

    /**
     * @var string
     *
     * @ORM\Column(name="language_name", type="string", length=255, nullable=true)
     */
    private $languageName;


    /**
     * Get languageCode
     *
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * Set languageName
     *
     * @param string $languageName
     * @return LexIsoLanguage
     */
    public function setLanguageName($languageName)
    {
        $this->languageName = $languageName;

        return $this;
    }

    /**
     * Get languageName
     *
     * @return string
     */
    public function getLanguageName()
    {
        return $this->languageName;
    }
}
