<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Keyword
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.keyword", indexes={@ORM\Index(name="idx_keywords", columns={"category_id"})})
 * @ORM\Entity
 */
class Keyword
{
    /**
     * ID of the editable keyword category
     * @see table lex_category_keyword
     * @var integer
     */
    const EDITABLE_CATEGORY = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="keyword_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.keyword_keyword_id_seq", allocationSize=1, initialValue=1)
     */
    private $keywordId;

    /**
     * @var string
     *
     * @ORM\Column(name="keyword_name", type="string", length=255, nullable=true)
     */
    private $keywordName;

    /**
     * @var \LexCategoryKeyword
     *
     * @ORM\ManyToOne(targetEntity="LexCategoryKeyword")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="category_id")
     * })
     */
    private $category;

    /**
     * @var Collection
     *
     * @Exclude
     *
     * @ORM\ManyToMany(targetEntity="Map", mappedBy="keywords")
     */
    private $maps;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->maps = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get keywordId
     *
     * @return integer
     */
    public function getKeywordId()
    {
        return $this->keywordId;
    }

    /**
     * Set keywordName
     *
     * @param string $keywordName
     * @return Keyword
     */
    public function setKeywordName($keywordName)
    {
        $this->keywordName = $keywordName;

        return $this;
    }

    /**
     * Get keywordName
     *
     * @return string
     */
    public function getKeywordName()
    {
        return $this->keywordName;
    }

    /**
     * Set category
     *
     * @param \Carmen\ApiBundle\Entity\LexCategoryKeyword $category
     * @return Keyword
     */
    public function setCategory(\Carmen\ApiBundle\Entity\LexCategoryKeyword $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Carmen\ApiBundle\Entity\LexCategoryKeyword
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get maps
     *
     * @return Collection
     */
    public function getMaps()
    {
        return $this->maps;
    }
}
