<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Account
 *
 * @ORM\Table(name="carmen.account", indexes={@ORM\Index(name="idx_account", columns={"account_dns_id"})})
 * @ORM\Entity
 *
 * @ExclusionPolicy("none")
 */
class Account
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=false)
     * @ORM\Id
     * #ORM\GeneratedValue(strategy="SEQUENCE")
     * #ORM\SequenceGenerator(sequenceName="carmen.account_account_id_seq", allocationSize=1, initialValue=1)
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_name", type="string", length=255, nullable=true)
     */
    private $accountName;

    /**
     * @var string
     *
     * @ORM\Column(name="account_path", type="string", length=255, nullable=true)
     */
    private $accountPath;

    /**
     * @var string
     *
     * @ORM\Column(name="account_hostpostgres", type="string", length=255, nullable=true)
     */
    private $accountHostpostgres;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_portpostgres", type="integer", nullable=false, options={"default"="5432"})
     */
    private $accountPortpostgres;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_dbpostgres", type="string", length=64, nullable=true)
     */
    private $accountDbpostgres;

    /**
     * @var string
     *
     * @ORM\Column(name="account_userpostgres", type="string", length=255, nullable=true)
     */
    private $accountUserpostgres;

    /**
     * @var string
     *
     * @ORM\Column(name="account_passwordpostgres", type="string", length=255, nullable=true)
     * @Exclude
     */
    private $accountPasswordpostgres;

    /**
     * @var \LexDns
     *
     * @ORM\ManyToOne(targetEntity="LexDns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_dns_id", referencedColumnName="dns_id")
     * })
     */
    private $accountDns;


    /**
     * @var string
     *
     * @ORM\Column(name="account_geonetworkurl", type="string", length=500, nullable=true)
     */
    private $accountGeonetworkurl;


    /**
     * Set accountId
     *
     * @param integer $accountId
     * @return Account
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Get accountId
     *
     * @return integer
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set accountName
     *
     * @param string $accountName
     * @return Account
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;

        return $this;
    }

    /**
     * Get accountName
     *
     * @return string
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * Set accountPath
     *
     * @param string $accountPath
     * @return Account
     */
    public function setAccountPath($accountPath)
    {
        $this->accountPath = $accountPath;

        return $this;
    }

    /**
     * Get accountPath
     *
     * @return string
     */
    public function getAccountPath()
    {
        return $this->accountPath;
    }

    /**
     * Set accountHostpostgres
     *
     * @param string $accountHostpostgres
     * @return Account
     */
    public function setAccountHostpostgres($accountHostpostgres)
    {
        $this->accountHostpostgres = $accountHostpostgres;

        return $this;
    }

    /**
     * Get accountHostpostgres
     *
     * @return string
     */
    public function getAccountHostpostgres()
    {
        return $this->accountHostpostgres;
    }

    /**
     * Set accountPortpostgres
     *
     * @param integer $accountPortpostgres
     * @return Account
     */
    public function setAccountPortpostgres($accountPortpostgres)
    {
        $this->accountPortpostgres = $accountPortpostgres;

        return $this;
    }

    /**
     * Get accountPortpostgres
     *
     * @return integer
     */
    public function getAccountPortpostgres()
    {
        return $this->accountPortpostgres;
    }

    /**
     * Set accountDbpostgres
     *
     * @param string $accountDbpostgres
     * @return Account
     */
    public function setAccountDbpostgres($accountDbpostgres)
    {
        $this->accountDbpostgres = $accountDbpostgres;

        return $this;
    }

    /**
     * Get accountDbpostgres
     *
     * @return string
     */
    public function getAccountDbpostgres()
    {
        return $this->accountDbpostgres;
    }

    /**
     * Set accountUserpostgres
     *
     * @param string $accountUserpostgres
     * @return Account
     */
    public function setAccountUserpostgres($accountUserpostgres)
    {
        $this->accountUserpostgres = $accountUserpostgres;

        return $this;
    }

    /**
     * Get accountUserpostgres
     *
     * @return string
     */
    public function getAccountUserpostgres()
    {
        return $this->accountUserpostgres;
    }

    /**
     * Set accountPasswordpostgres
     *
     * @param string $accountPasswordpostgres
     * @return Account
     */
    public function setAccountPasswordpostgres($accountPasswordpostgres)
    {
        $this->accountPasswordpostgres = $accountPasswordpostgres;

        return $this;
    }

    /**
     * Get accountPasswordpostgres
     *
     * @return string
     */
    public function getAccountPasswordpostgres()
    {
        return $this->accountPasswordpostgres;
    }

    /**
     * Set accountDns
     *
     * @param \Carmen\ApiBundle\Entity\LexDns $this
     * @return Account
     */
    public function setAccountDns(\Carmen\ApiBundle\Entity\LexDns $accountDns = null)
    {
        $this->accountDns = $accountDns;

        return $this;
    }

    /**
     * Get accountDns
     *
     * @return \Carmen\ApiBundle\Entity\LexDns
     */
    public function getAccountDns()
    {
        return $this->accountDns;
    }

    /**
     * Set accountGeonetworkurl
     *
     * @param string $accountGeonetworkurl
     * @return Account
     */
    public function setAccountGeonetworkurl($accountGeonetworkurl)
    {
        $this->accountGeonetworkurl = $accountGeonetworkurl;

        return $this;
    }

    /**
     * Get accountGeonetworkurl
     *
     * @return string
     */
    public function getAccountGeonetworkurl()
    {
        return $this->accountGeonetworkurl;
    }

    /**
     * Construct the account url to front according to parameters
     * @param boolean $bForFrontOffice
     * @param string $environnement prod|dev
     * @return string
     */
    public function getAccountUrlFront($bForFrontOffice = true, $environnement = "prod", $separator = ".")
    {
        $lexDns = $this->getAccountDns();
        if (!$lexDns) return "/";
        return $lexDns->getAccountUrlFront($bForFrontOffice, $environnement, $separator);
    }

    /**
     * Construct the account url to data according to parameters
     * @param boolean $bForFrontOffice
     * @param string $environnement prod|dev
     * @return string
     */
    public function getAccountUrlData($bForFrontOffice = true, $environnement = "prod", $separator = ".")
    {
        $lexDns = $this->getAccountDns();
        if (!$lexDns) return "/";
        return $lexDns->getAccountUrlData($bForFrontOffice, $environnement, $separator);
    }

}
