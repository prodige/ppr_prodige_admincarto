<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * LexMapAuditStatus
 *
 * @ORM\Table(name="carmen.lex_map_audit_status")
 * @ORM\Entity
 */
class LexMapAuditStatus
{
    const STATUS_CREATE = "CREATE";
    const STATUS_UPDATE = "UPDATE";
    const STATUS_DELETE = "DELETE";
    protected static $instances = array();

    /**
     *
     * @param EntityManager $em
     * @param string $status
     * @return null|\Carmen\ApiBundle\Entity\LexMapAuditStatus
     */
    public static function getInstance(EntityManager $em, $status)
    {
        $status = strtoupper($status);
        if (!isset(self::$instances[$status])) {
            $results = $em->getRepository("CarmenApiBundle:LexMapAuditStatus")->findAll();
            foreach ($results as $lexmapauditstatus) {
                $lexmapauditstatus instanceof LexMapAuditStatus;
                self::$instances[strtoupper($lexmapauditstatus->getMapAuditStatusName())] = $lexmapauditstatus;
            }
        }
        return (isset(self::$instances[$status]) ? self::$instances[$status] : null);
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="map_audit_status_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_map_audit_status_map_audit_status_id_seq", allocationSize=1, initialValue=1)
     */
    private $mapAuditStatusId;

    /**
     * @var string
     *
     * @ORM\Column(name="map_audit_status_name", type="text", nullable=false)
     */
    private $mapAuditStatusName;


    /**
     * Get mapAuditStatusId
     *
     * @return integer
     */
    public function getMapAuditStatusId()
    {
        return $this->mapAuditStatusId;
    }

    /**
     * Set mapAuditStatusName
     *
     * @param string $mapAuditStatusName
     * @return LexMapAuditStatus
     */
    public function setMapAuditStatusName($mapAuditStatusName)
    {
        $this->mapAuditStatusName = $mapAuditStatusName;

        return $this;
    }

    /**
     * Get mapAuditStatusName
     *
     * @return string
     */
    public function getMapAuditStatusName()
    {
        return $this->mapAuditStatusName;
    }
}
