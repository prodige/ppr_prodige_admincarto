<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LexProjection
 *
 * @ORM\Table(name="carmen.lex_projection")
 * @ORM\Entity
 */
class LexProjection
{
    /**
     * @var string
     *
     * @ORM\Column(name="projection_epsg", type="decimal", precision=10, scale=0, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.lex_projection_projection_epsg_seq", allocationSize=1, initialValue=1)
     */
    private $projectionEpsg;

    /**
     * @var string
     *
     * @ORM\Column(name="projection_name", type="string", length=255, nullable=true)
     */
    private $projectionName;

    /**
     * @var string
     *
     * @ORM\Column(name="projection_provider", type="string", length=255, nullable=true)
     */
    private $projectionProvider;


    /**
     * Get projectionEpsg
     *
     * @return string
     */
    public function getProjectionEpsg()
    {
        return $this->projectionEpsg;
    }

    /**
     * Set projectionName
     *
     * @param string $projectionName
     * @return LexProjection
     */
    public function setProjectionName($projectionName)
    {
        $this->projectionName = $projectionName;

        return $this;
    }

    /**
     * Get projectionName
     *
     * @return string
     */
    public function getProjectionName()
    {
        return $this->projectionName;
    }

    /**
     * Get projectionCode
     *
     * @return string
     */
    public function getProjectionProvider()
    {
        return $this->projectionProvider;
    }
}
