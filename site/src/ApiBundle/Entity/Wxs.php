<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wxs
 *
 * @ORM\Table(name="carmen.wxs", indexes={@ORM\Index(name="idx_wxs", columns={"account_id"}), @ORM\Index(name="idx_wxs_0", columns={"wxs_type_id"})})
 * @ORM\Entity
 */
class Wxs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="wxs_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.wxs_wxs_id_seq", allocationSize=1, initialValue=1)
     */
    private $wxsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="wxs_rank", type="integer", nullable=true)
     */
    private $wxsRank;

    /**
     * @var string
     *
     * @ORM\Column(name="wxs_name", type="string", length=255, nullable=true)
     */
    private $wxsName;

    /**
     * @var string
     *
     * @ORM\Column(name="wxs_url", type="string", length=255, nullable=true)
     */
    private $wxsUrl;

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     * })
     */
    private $account;

    /**
     * @var \LexWxsType
     *
     * @ORM\ManyToOne(targetEntity="LexWxsType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wxs_type_id", referencedColumnName="wxs_type_id")
     * })
     */
    private $wxsType;


    /**
     * Get wxsId
     *
     * @return integer
     */
    public function getWxsId()
    {
        return $this->wxsId;
    }

    /**
     * Set wxsRank
     *
     * @param integer $wxsRank
     * @return Wxs
     */
    public function setWxsRank($wxsRank)
    {
        $this->wxsRank = $wxsRank;

        return $this;
    }

    /**
     * Get wxsRank
     *
     * @return integer
     */
    public function getWxsRank()
    {
        return $this->wxsRank;
    }

    /**
     * Set wxsName
     *
     * @param string $wxsName
     * @return Wxs
     */
    public function setWxsName($wxsName)
    {
        $this->wxsName = $wxsName;

        return $this;
    }

    /**
     * Get wxsName
     *
     * @return string
     */
    public function getWxsName()
    {
        return $this->wxsName;
    }

    /**
     * Set wxsUrl
     *
     * @param string $wxsUrl
     * @return Wxs
     */
    public function setWxsUrl($wxsUrl)
    {
        $this->wxsUrl = $wxsUrl;

        return $this;
    }

    /**
     * Get wxsUrl
     *
     * @return string
     */
    public function getWxsUrl()
    {
        return $this->wxsUrl;
    }

    /**
     * Set account
     *
     * @param \Carmen\ApiBundle\Entity\Account $account
     * @return Wxs
     */
    public function setAccount(\Carmen\ApiBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Carmen\ApiBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set wxsType
     *
     * @param \Carmen\ApiBundle\Entity\LexWxsType $wxsType
     * @return Wxs
     */
    public function setWxsType(\Carmen\ApiBundle\Entity\LexWxsType $wxsType = null)
    {
        $this->wxsType = $wxsType;

        return $this;
    }

    /**
     * Get wxsType
     *
     * @return \Carmen\ApiBundle\Entity\LexWxsType
     */
    public function getWxsType()
    {
        return $this->wxsType;
    }
}
