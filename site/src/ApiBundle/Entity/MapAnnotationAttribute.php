<?php

namespace Carmen\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * MapAnnotationAttribute
 *
 * @ExclusionPolicy("none")
 *
 * @ORM\Table(name="carmen.map_annotation_attribute", indexes={@ORM\Index(name="idx_map_annotations_attributs", columns={"map_id"})})
 * @ORM\Entity
 */
class MapAnnotationAttribute
{
    /**
     * The radical name of the OWSContext generated
     * @var string
     */
    const OWSCONTEXT_NODE = "attribut_value_annotation";

    /**
     * @var integer
     *
     * @ORM\Column(name="attribute_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carmen.map_annotation_attribute_attribute_id_seq", allocationSize=1, initialValue=1)
     */
    private $attributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="attribute_rank", type="integer", nullable=true)
     */
    private $attributeRank;

    /**
     * @var string
     *
     * @ORM\Column(name="attribute_name", type="string", length=255, nullable=true)
     */
    private $attributeName;

    /**
     * @var \Map
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="Map")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="map_id")
     * })
     */
    private $map;


    /**
     *
     * Set attribute Id
     *
     * @param integer $id
     * @return \Carmen\ApiBundle\Entity\MapAnnotationAttribute
     */
    public function setId($id)
    {
        $this->attributeId = $id;

        return $this;
    }

    /**
     * Get attributeId
     *
     * @return integer
     */
    public function getAttributeId()
    {
        return $this->attributeId;
    }

    /**
     * Set attributeRank
     *
     * @param integer $attributeRank
     * @return MapAnnotationAttribute
     */
    public function setAttributeRank($attributeRank)
    {
        $this->attributeRank = $attributeRank;

        return $this;
    }

    /**
     * Get attributeRank
     *
     * @return integer
     */
    public function getAttributeRank()
    {
        return $this->attributeRank;
    }

    /**
     * Set attributeName
     *
     * @param string $attributeName
     * @return MapAnnotationAttribute
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;

        return $this;
    }

    /**
     * Get attributeName
     *
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * Set map
     *
     * @param \Carmen\ApiBundle\Entity\Map $map
     * @return MapAnnotationAttribute
     */
    public function setMap(\Carmen\ApiBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Carmen\ApiBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }
}
