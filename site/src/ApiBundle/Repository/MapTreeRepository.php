<?php

namespace Carmen\ApiBundle\Repository;

use Carmen\ApiBundle\Entity\Layer;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\MapGroup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * A specialization of repository used by entites MapTree and MapTreePrint 
 * @author Alkante
 */
class MapTreeRepository extends EntityRepository 
{
    /**
     * Returns an array of map tree (print or not) organized like :
     * $tree = array(
     *   ...node_properties...
     *   "object"   => Layer|MapGroup
     *   "children" => array<tree_nodes>
     * )
     * @param Map $map
     * @return array
     */
    public function getTreeByMap(Map $map)
    {
        $table = $this->_class->getTableName();
        $map_id = $map->getMapId();
        
        $with_recursive = 
<<<SQL
with recursive 
node_tree(node_id, map_id, node_parent, node_pos, node_opened, node_is_layer, node_depth, group_id, layer_id) 
as (
  select map_tree.node_id, map_tree.map_id, map_tree.node_parent, map_tree.node_pos, map_tree.node_opened, map_tree.node_is_layer
  , 1
  , (case when map_tree.node_is_layer then null::int else map_tree.node_id end) as group_id
  , (case when map_tree.node_is_layer then map_tree.node_id else null::int end) as layer_id
  from {$table} map_tree
  where map_tree.map_id={$map_id}
  and map_tree.node_parent is null

  union all

  select map_tree.node_id, map_tree.map_id, map_tree.node_parent, map_tree.node_pos, map_tree.node_opened, map_tree.node_is_layer
  , node_tree.node_depth+1
  , (case when map_tree.node_is_layer then null::int else map_tree.node_id end) as group_id
  , (case when map_tree.node_is_layer then map_tree.node_id else null::int end) as layer_id
  from {$table} map_tree 
  inner join node_tree on (node_tree.map_id=map_tree.map_id and node_tree.node_id=map_tree.node_parent)
  where map_tree.map_id={$map_id}
) 
SQL;

        $tmp = array();
        $layers = $map->getLayers();
        foreach ($layers as $index=>$layer){
            $layer instanceof Layer;
            $layer->layerIdx = $index;
            $tmp[$layer->getLayerId()] = $layer;
        }
        $layers = $tmp;
        
        $tmp = array();
        $groups = $map->getGroups();
        foreach ($groups as $index=>$group){
            $group instanceof MapGroup;
            $group->groupIdx = $index;
            $tmp[$group->getGroupId()] = $group;
        }
        $groups = $tmp;
        
        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addScalarResult("node_id", "nodeId");
        $resultSetMapping->addScalarResult("node_parent", "nodeParent");
        $resultSetMapping->addScalarResult("node_pos", "nodePos");
        $resultSetMapping->addScalarResult("node_opened", "nodeOpened");
        $resultSetMapping->addScalarResult("node_is_layer", "nodeIsLayer");
        $resultSetMapping->addScalarResult("node_depth", "nodeDepth");
        $resultSetMapping->addScalarResult("group_id", "groupId");
        $resultSetMapping->addScalarResult("layer_id", "layerId");
        
        $this->_em->getConnection()->exec("set search_path to carmen;");
        $tree_groups = $this->_em->createNativeQuery(
             $with_recursive.
            " select * from node_tree order by node_depth, node_pos"
        , $resultSetMapping);
        $tree_groups = $tree_groups->execute();
        
        $treeType = "ui";
        if ( strpos($this->_entityName, "Print")!==false ){
            $treeType = "print";
        }
        $nodes = array(-1=>array(
        		"name"=>"root"
        	, "nodeId" => "root"
        	, "nodeIsLayer" => false
        	, "nodeOpened" => true
        	, "nodeDepth" => 0
          , "visible" => false
        	, "object" => array(
        			"groupName" => "root",
        			"groupIdentifier" => ""
        	  )
        	, "childs"=>array())
        );
        $duplicates = array();
        foreach($tree_groups as  $mapTree){
            $node_id = $mapTree["nodeId"];
            
            if ( $mapTree["nodeIsLayer"] && isset($layers[$node_id]) ){
            	  if ( $treeType=="ui" ){
	                $layers[$node_id]->nodeDepth    = $mapTree["nodeDepth"];
	                $layers[$node_id]->group        = $mapTree["nodeParent"];
	                $layers[$node_id]->orderInGroup = $mapTree["nodePos"];
            	  }
                $mapTree["visible"] = $layers[$node_id]->getLayerVisible();
                $mapTree["object"] = $layers[$node_id];
                $nodes[$mapTree["nodeParent"] ?: -1]["childs"][$node_id] = $mapTree; 
                $nodes[$mapTree["nodeParent"] ?: -1]["visible"] = $nodes[$mapTree["nodeParent"] ?: -1]["visible"] || $layers[$node_id]->getLayerVisible();
            } 
            if ( !$mapTree["nodeIsLayer"] && isset($groups[$node_id]) ){
            	  if ( $treeType=="ui" ){
	                $groups[$node_id]->nodeDepth    = $mapTree["nodeDepth"];
	                $groups[$node_id]->group        = $mapTree["nodeParent"];
	                $groups[$node_id]->orderInGroup = $mapTree["nodePos"];
            	  }
            	  
                $mapTree["object"] = $groups[$node_id];
                $mapTree["childs"] = array();
                $mapTree["visible"] = false;
                $nodes[$mapTree["nodeParent"] ?: -1]["childs"][$node_id] = $mapTree;
                $nodes[$node_id] = &$nodes[$mapTree["nodeParent"] ?: -1]["childs"][$node_id]; 
                $duplicates[] = $node_id;
                
            }
        }
        
        foreach ($duplicates as $node_id){
            unset($nodes[$node_id]);
        }
        if ( empty($nodes[-1]["childs"]) ){
        	foreach($layers as $node_id=>$layer){
            if ( $treeType=="ui" ){
        		  $layer->nodeDepth = 1;
        		  $layer->group = null;
        		  $layer->orderInGroup = count($nodes[-1]["childs"]);
            }
        		$mapTree = array(
        			"nodeId" => $node_id,
        			"nodePos" => count($nodes[-1]["childs"]),
        			"nodeDepth" => 1,
        			"nodeIsLayer" => true,
        			"nodeOpened" => true,
        			"nodeParent" => null,
        			"object" => $layer
        		);
        		$nodes[-1]["childs"][$node_id] = $mapTree;
        	}
        }
        $unset_reference = function($array) use(&$unset_reference){
            $tmp = array();
            foreach($array as $key=>$value){
                if (is_array($value)){
                    $tmp[$key] = $unset_reference($value);
                } else {
                    $tmp[$key] = $value;
                }
            }
            return $tmp;
        };
        $nodes = $unset_reference($nodes);
        
        $setVisibility = function($nodes) use(&$setVisibility){
            foreach($nodes as $key=>$value){
                if (is_array($value) && isset($value["childs"])){
                    $nodes[$key]["childs"] = $setVisibility($value["childs"]);
                    $visible = false;
                    foreach($nodes[$key]["childs"] as $c=>$child){
                        if ( isset($child["visible"]) ){
                            $visible = $visible || $child["visible"];
                        }
                    }
                    $nodes[$key]["visible"] = $visible;
                }
            }
            return $nodes;
        };
        $nodes = $setVisibility($nodes);
        $tree = array("-1" =>$nodes["-1"]);
        return $tree;
    }
}
