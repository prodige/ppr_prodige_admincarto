<?php

namespace Carmen\ApiBundle\Repository;

/**
 * GeojsonParamsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GeojsonParamsRepository extends \Doctrine\ORM\EntityRepository
{
    function update($id, $data)
    {
        $em = $this->getEntityManager();

        $entity = $this->find($id);
        
        if ($entity) {
            if (isset($data['geojsonDistance'])) {
                $entity->setGeojsonDistance(intval($data['geojsonDistance'])); //GeojsonDistance
            }
            if (isset($data['geojsonSingleColor'])) {
                $entity->setGeojsonSingleColor($data['geojsonSingleColor']); //GeojsonSingleColor
            }
            if (isset($data['geojsonColor'])) {
                $entity->setGeojsonColor($data['geojsonColor']); //geojsonColor
            }
            if (isset($data['geojsonFixedSize'])) {
                $entity->setGeojsonFixedSize($data['geojsonFixedSize']); //GeojsonFixedSize
            }
            if (isset($data['geojsonSize'])) {
                $entity->setGeojsonSize(intval($data['geojsonSize'])); //GeojsonSize
            }
            if (isset($data['geojsonTransitionEffect'])) {
                $entity->setGeojsonTransitionEffect($data['geojsonTransitionEffect']); //GeojsonTransitionEffect
            }
            if (isset($data['geojsonZoomOnClick'])) {
                $entity->setGeojsonZoomOnClick($data['geojsonZoomOnClick']); //GeojsonZoomOnClick
            }
            if (isset($data['geojsonWithText'])) {
                $entity->setGeojsonWithText($data['geojsonWithText']); //geojsonWithText
            }

            $em->persist($entity);
            $em->flush();
        }
    }
}
