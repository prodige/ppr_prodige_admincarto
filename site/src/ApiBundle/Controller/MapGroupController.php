<?php

namespace Carmen\ApiBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Carmen\ApiBundle\Entity\Layer;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\MapGroup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// use FOS\RestBundle\Controller\Annotations\Delete;
// use FOS\RestBundle\Controller\Annotations\Get;
// use FOS\RestBundle\Controller\Annotations\Route;
// use FOS\RestBundle\Controller\Annotations\Head;
// use FOS\RestBundle\Controller\Annotations\Link;
// use FOS\RestBundle\Controller\Annotations\Patch;
// use FOS\RestBundle\Controller\Annotations\Post;
// use FOS\RestBundle\Controller\Annotations\Put;
// use FOS\RestBundle\Controller\Annotations\Unlink;
// use FOS\RestBundle\Request\ParamFetcher;
// use FOS\RestBundle\Controller\Annotations\QueryParam;   // GET params
// use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params

// use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Carmen Default API Controller.
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/api")
 */
class MapGroupController extends ApiController
{
    const GROUP_ENTITY      = 'CarmenApiBundle:MapGroup';
    const MAP_ENTITY        = 'CarmenApiBundle:Map';
    
    /**
     * Restfull api to serve Entity.
     *
     * @Route("/group/{mapId}/rest/{id}",methods={"GET","POST","PUT","PATCH","DELETE"}, name="carmen_ws_mapgroup", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true})
     * 
     * @param Request $request  request object
     * @param int     $id       layer id, null by default
     */
    public function mapGroupRestAction(Request $request, $mapId, $id = null)
    {
        switch ($request->getMethod()) {
            case 'GET':
                return $this->getMapGroupAction($request, $mapId, $id);
                break;
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return $this->postMapGroupAction($request, $mapId, $id);
                break;
            case 'DELETE':
                return $this->deleteMapGroupAction($mapId, $id);
                break;
        }

        return $this->formatJsonError(Response::HTTP_METHOD_NOT_ALLOWED, $this->_t("Method not allowed"));
    }

    /**
     * Returns a list of Map or the defined layer by id.
     * 
     * Extra filters can be added using query parameters such as 'filter[someColumn]=someValue'.
     * @see BaseController::getQueryBuilderList()
     * 
     * @param Request $request The request object.
      * @param string $mapId   If set and $id is null, returns a list with the requested Map only (if exists).
     * @param string  $id      If set, returns the requested layer only (if exists).
     * 
     * @Route("/group/{mapId}/get/{groupId}", 
     * name="carmen_ws_get_mapgroup", 
     * requirements={"mapId"="\d+", "id"="\d+"}, 
     * defaults={"id"=null}, 
     * options={"expose"=true},
     * methods={"GET"})
     * 
     * @return JsonResponse Json array.
     */
    public function getMapGroupAction(Request $request, $mapId, $id = null)
    {
        $logger = $this->getLogger();
        
        $filters = $request->query->get('filter', array());
        if ( !is_array($filters) && !empty($filters) ){
            $filters = json_decode($filters, true) ?: array();
        }
        $property = array(); 
        $value = array();
        foreach ($filters as $filter){
            foreach($filter as $var_name=>$var_value){
                ${"$var_name"}[] = $var_value;
            }
        }
        $filters = array_combine($property, $value);

        $filters["map"] = $mapId;
        
        try {
            $qb = $this->getQueryBuilderList(self::GROUP_ENTITY, 'e', $id, $filters);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception"), $this->isProd() ? array() : array('exception'=>$ex->getMessage()));
        }

        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * @Route("/group/{mapId}/post/{id}", 
     * name="carmen_ws_post_mapgroup", 
     * requirements={"mapId"="\d+", "id"="\d+"}, 
     * defaults={"id"=null}, 
     * options={"expose"=true},
     * methods={"POST", "PUT", "PATCH"})
     * 
     * @param Request $request  request object.
     * @param string  $id       If set, updates an existing layer, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function postMapGroupAction(Request $request, $mapId, $id = null)
    {
        $user = $this->getUser();
        if ( !$user ) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), 
                                          $this->_t("Unable to find current user."));
        }
        $user instanceof Users;
  
        $account = $user->getAccount();
        if ( !$account ) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), 
                                          $this->_t("Unable to find current account."));
        }
        
        return $this->transactional(
            function(EntityManager $em) use ($request, $mapId, $id) {
                $oMap = $this->getRepository(self::MAP_ENTITY)->find($mapId);
                $request->request->set("map", $mapId);
                $groupIdentifier = $request->request->get("groupIdentifier", null);
                if ( empty($groupIdentifier) ){
                  $request->request->set("groupIdentifier", $request->request->get("groupName", ""));
                }
                $mapGroup = $this->doGenericPost($request, self::GROUP_ENTITY, $id, null, true);
                $mapGroup instanceof MapGroup;
            
                if ( !$id ){
                    $id = $mapGroup->getGroupId();
                    foreach(array("MapTree", "MapTreePrint") as $tree_type){
                        $nodetree = $this->getRepository('CarmenApiBundle:'.$tree_type)->findOneBy(array("map"=>$mapId, "nodeId"=>$id));
                        $max_node_pos = $this->getRepository('CarmenApiBundle:'.$tree_type)->findOneBy(array("map"=>$mapId), array("nodePos"=>"DESC"));
                        if ( $max_node_pos ) {
                            $max_node_pos = $max_node_pos->getNodePos();
                        } else {
                            $max_node_pos = 0;
                        }
                        if ( !$nodetree ) {
                            $class_tree = "Carmen\\ApiBundle\\Entity\\".$tree_type;
                            $nodetree = new $class_tree();
                            $nodetree->setId($id);
                            $nodetree->setMap($oMap);
                            $nodetree->setNodeIsLayer(false);
                            $nodetree->setNodeOpened(true);
                            $nodetree->setNodePos($max_node_pos+1);
                            $em->persist($nodetree);
                        }
                    }
                }
                return $mapGroup;
            },
            function(MapGroup $mapGroup) {
              return new JsonResponse( json_decode($this->jsonSerializeEntity($mapGroup), true) );
            }
        );
    }

    
    /**
     * Deletes an Entity.
     *
     * @Route("/group/{mapId}/delete/{id}", 
     * name="carmen_ws_delete_mapgroup", 
     * requirements={"mapId"="\d+", "id"="\d+"}, 
     * options={"expose"=true},
     * methods={"DELETE"})
     *
     * @param string  $id  layer id to delete
     *
     * @return Response Empty response if success, JsonResponse error if failed.
     */
    public function deleteMapGroupAction($mapId, $id)
    {
        $me = $this;
        $groupId = $id;
        return $this->transactional(
            function(EntityManager $em) use ($me, $mapId, $groupId) {
                if( null != $groupId ) {
                    $edit_directory = $me->getMapfileDirectory();
                    $read_directory = $me->getMapfileDirectory();
                    $files = array();
                    
                    $oMap = $this->getRepository($me::MAP_ENTITY)->find($mapId);
                    if ( !$oMap ) {
                        throw new EntityNotFoundException();
                    }
                    $oMap instanceof Map;
                    
                    $oGroup = $this->getRepository($me::GROUP_ENTITY)->find($groupId);
                    if ( !$oGroup ) {
                        throw new EntityNotFoundException();
                    }
                    $oGroup instanceof Layer;
                    
                    $em->remove($oGroup);
                    $em->flush();
                    
                    return array("groupId" => $groupId, "map" => $oMap);
                }
            },
            function($data) {
              return new JsonResponse($data);
            }
        );
    }
    
}
