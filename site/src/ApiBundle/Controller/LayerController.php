<?php

namespace Carmen\ApiBundle\Controller;

use Carmen\ApiBundle\Entity\Account;
use Carmen\ApiBundle\Entity\Field;
use Carmen\ApiBundle\Entity\GeojsonParams;
use Carmen\ApiBundle\Entity\Layer;
use Carmen\ApiBundle\Entity\LexLayerType;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\Users;
use Carmen\ApiBundle\Entity\Wxs;
use Carmen\ApiBundle\Services\Helpers;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Carmen Default API Controller.
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/api")
 */
class LayerController extends ApiController
{
    const CONST_CURLOPT_CONNECTTIMEOUT = 15;
    const LAYER_ENTITY = 'CarmenApiBundle:Layer';
    const MAP_ENTITY = 'CarmenApiBundle:Map';
    const WXS_ENTITY = 'CarmenApiBundle:Wxs';
    const LEXWXSTYPE_ENTITY = 'CarmenApiBundle:LexWxsType';
    const FIELD_ENTITY = 'CarmenApiBundle:Field';

    const CATALOGUE_SCHEMA_PUBLIC = "public";

    const WMS_SAVED_SERVICES_FILENAME = 'ServeursWMS_Administration.xml';

    /**
     * Return the route to Mapserver Api for manage Layer
     * @param Map $oMap
     * @param Layer $oLayer
     */
    protected function getMapserverRoute(Map $oMap, Layer $oLayer)
    {
        return self::_getMapserverRoute($this, $oMap, $oLayer);
    }

    /**
     * Return the route to Mapserver Api for manage Layer
     * @param Map $oMap
     * @param Layer $oLayer
     */
    public static function _getMapserverRoute(BaseController $controller, Map $oMap, Layer $oLayer)
    {
        $mapfile = $controller->ensureEditingMapfile($oMap, $oMap->getPublishedMap());
        $layerName = $oLayer->getLayerMsname();

        if (empty($mapfile) || empty($layerName)) {
            return null;
        }

        return "/api/map/{$mapfile}/layer/{$layerName}";
    }

    /**
     * Convert Mapserver data in layer properties
     * @param array $msData mapserver data
     * @return array
     */
    public static function convertMapserverParams($msData)
    {
        $convertFields = array(
            // projection gets from database
            "connectiontype" => function ($data, $field, &$result) {
                $result["msLayerConnectionType"] = (isset($data[$field]) ? $data[$field] : "");
                if (isset($data["connectiontypename"]) && $data["connectiontypename"] == "MS_OGR") {
                    $result["msLayerData"] = $data["connection"];
                }
            },
            "connection" => function ($data, $field, &$result) {
                // don't return postgis connection
                $result["msLayerConnection"] = (isset($data[$field]) ? $data[$field] : "");
            },
            "data" => function ($data, $field, &$result) {
                if (isset($data[$field]) && $data[$field] != "") {
                    $pattern = '/^(?P<geom>\w+)\s+from' .
                        '\s+\(select\s+\*\s+from\s+(("?(?P<schema>\w+)"?\.)?"?(?P<table>\w+)"?)\)\s+as\s+foo' .
                        '\s+using\s+unique\s+(?P<gid>\w+)' .
                        '\s+using\s+srid=(?P<srid>\d+)$/i';
                    $matches = array();

                    if (preg_match($pattern, $data[$field], $matches)) {
                        $result["msLayerPgSchema"] = (isset($matches["schema"]) && $matches["schema"] != "" ? $matches["schema"] : "public");
                        $result["msLayerPgTable"] = (isset($matches["table"]) ? $matches["table"] : "");
                        $result["msLayerPgGeometryField"] = (isset($matches["geom"]) ? $matches["geom"] : "");
                        $result["msLayerPgIdField"] = (isset($matches["gid"]) ? $matches["gid"] : "");
                        $result["msLayerPgProjection"] = (isset($matches["srid"]) ? $matches["srid"] : "");
                        $result["msLayerPgType"] = ""; // @todo: read from external database
                    } else {
                        $result["msLayerData"] = ($data["is_raster"] && !empty($data["tileindex"]) ? $data["tileindex"] : $data[$field]);
                    }
                } else {
                    $result["msLayerData"] = ($data["is_raster"] && !empty($data["tileindex"]) ? $data["tileindex"] : "");
                }
            },
            "geometrytype" => function ($data, $field, &$result) {
                $result["msGeometryType"] = $data["geometrytype"];
            },
            "type" => function ($data, $field, &$result) {
                $result["msLayerType"] = $data["type"];
            },

            "index" => "msLayerIndex",
            "processing" => function ($data, $field, &$result) {
                if (isset($data[$field]) && is_array($data[$field]) && !empty($data[$field])) {
                    $result["msLayerGrayLevel"] = (in_array("BANDS=2", $data[$field])
                    || (array_key_exists("BANDS", $data[$field]) && $data[$field]["BANDS"] == "2")
                        ? "1" : "0");
                }
                $result[$field] = @$data[$field] ?: null;
            },
            "mappath" => "msMapPath",
            "shapepath" => "msShapePath",
            "name" => "layerMsname",
            "labelitem" => "msLabelItem",
            "classitem" => "msClassItem",
            "symbolscaledenompixels" => "msSymbolScaleDenomPixels",
            "classes" => function ($data, $field, &$result) {
                $classes = (isset($data["classes"]) ? $data["classes"] : array());
                $tmp = array();
                foreach ($classes as $class) {
                    $styles = $class["styles"];
                    $labels = (isset($class["labels"]) ? $class["labels"] : array());
                    if (!empty($labels)) {
                        $result["msLabel"] = $labels[0];
                        $classStyle = null;
                        foreach ($result["msLabel"]["styles"] as $style) {
                            if (isset($style["size"]) && !is_numeric($style["size"])) {
                                $classStyle = $style;
                                break;
                            }
                        }
                        if ($classStyle && empty($styles)) {
                            $styles = array($classStyle);
                        }
                    } else {
                        $result["msLabel"] = null;
                    }
                    $oneStyle = array();
                    if (count($styles) > 0) {
                        $oneStyle = array_merge($oneStyle, $styles[0]);
                    }
                    if (count($styles) > 1) {
                        $outline = array();
                        if (isset($styles[1]["color"])) {
                            $outline["outlinecolor"] = $styles[1]["color"];
                        } else if (isset($styles[1]["outlinecolor"])) {
                            $outline["outlinecolor"] = $styles[1]["outlinecolor"];
                        }
                        if (isset($styles[1]["width"])) {
                            $outline["outlinewidth"] = $styles[1]["width"];
                        }
                        $oneStyle = array_merge($oneStyle, $outline);
                    }
                    $class["styles"] = $oneStyle;
                    $tmp[] = $class;
                }
                $result["msClasses"] = $tmp;
            },
        );

        $result = array();
        foreach ($msData as $msKey => $value) {
            if (array_key_exists($msKey, $convertFields)) {
                if ($convertFields[$msKey] === null) {
                    continue;
                } else if (is_callable($convertFields[$msKey])) {
                    // calculated conversion
                    $function = $convertFields[$msKey];
                    $function($msData, $msKey, $result);
                } else if (is_string($convertFields[$msKey])) {
                    // simple conversion key
                    $result[$convertFields[$msKey]] = $value;
                }
            } else {
                // mapserver Key is returned as-is
                $result[$msKey] = $value;
            }
        }

        return $result;
    }

    /**
     * Extract from submitted values the Mapserver parameters for a node layer
     * @param Request $request request object
     * @param Layer $layer layer entity
     * @param Account $account account entity
     * @return array
     */
    protected function getMapserverParams(Request $request, Layer $layer, Account $account)
    {
        /** List of fields mapserver layer */
        $msLayerFields = array(
            //required
            "directory" => function () {
                return $this->getMapfileDirectory();
            },
            "name" => 'layerMsname',
            "type" => 'msLayerType',
            "geometrytype" => 'msGeometryType',
            "encoding" => 'layerDataEncoding',
            "projection" => function ($data) {
                return (isset($data["layerProjectionEpsg"]) && $this->getRepository('CarmenApiBundle:LexProjection')->find($data["layerProjectionEpsg"]) ?
                    $this->getRepository('CarmenApiBundle:LexProjection')->find($data["layerProjectionEpsg"])->getProjectionProvider() . ':' . $data["layerProjectionEpsg"]
                    : null);
            },
            //optionals
            "tileindex" => function () {
                return "";
            },
            "tileitem" => function () {
                return "";
            },
            "maxscaledenom" => "layerMaxscale", // key name in request
            "minscaledenom" => "layerMinscale", // key name in request
            "align" => function ($data) {
                return isset($data['msLabel']['align']) ? $data['msLabel']['align'] : null;
            },
            "wrap" => function ($data) {
                return isset($data['msLabel']['wrap']) ? $data['msLabel']['wrap'] : null;
            },
            "repeatdistance" => function ($data) {
                return isset($data['msLabel']['repeatdistance']) ? $data['msLabel']['repeatdistance'] : null;
            },
            "outlinecolor" => function ($data) {
                return isset($data['msLabel']['outlinecolor']) ? $data['msLabel']['outlinecolor'] : null;
            },
            "outlinewidth" => function ($data) {
                return isset($data['msLabel']['outlinewidth']) ? $data['msLabel']['outlinewidth'] : null;
            },
            "backgroundshadowcolor" => function ($data) {
                return isset($data['msLabel']['backgroundshadowcolor']) ? $data['msLabel']['backgroundshadowcolor'] : null;
            },
            "backgroundshadowsize" => function ($data) {
                return isset($data['msLabel']['backgroundshadowsize']) ? $data['msLabel']['backgroundshadowsize'] : null;
            },
            "labelitem" => function ($data) {
                $value = (isset($data["msLabelItem"]) && !empty($data["msLabelItem"]) ? $data["msLabelItem"] : null);
                if (empty($value)) return null;
                return $value;
            },
            "offsite" => null,
            "processing" => function ($data) {
                $processing = array();
                if (isset($data["msLayerGrayLevel"]) && $data["msLayerGrayLevel"] == 1) {
                    $processing[] = "BANDS=2";
                }
                foreach ($data as $key => $value) {
                    if (strpos($key, "msProcessing") !== false && $value) {
                        $key = str_replace("msProcessing", "", $key);
                        $key = preg_replace("!([A-Z])!", "_$1", $key);
                        $key = strtoupper(preg_replace("!^_!", "", $key));
                        $processing[] = $key . "=" . $value;
                    }
                }
                if (empty($processing)) $processing = null;
                return $processing;
            },
            "symbolscaledenom" => null,
            "tolerance" => null,
            "toleranceunits" => null,
            "classitem" => "msClassItem",
            "symbolscaledenompixels" => "msSymbolScaleDenomPixels",
            "metadata" => function ($data) use ($layer) {
                if (isset($data["metadata"])) {
                    $metadata = $data["metadata"];
                } else {
                    $metadata = array();
                }

                $queryables = array();
                $fields = $layer->getFields();
                foreach ($fields as $field) {
                    $field instanceof Field;
                    if ($field->getFieldTooltips() || $field->getFieldQueries()) {
                        $queryables[] = $field->getFieldName();
                    }
                }
                if (empty($queryables)) {
                    unset($metadata["gml_include_items"]);
                    unset($metadata["ows_include_items"]);
                } else {
                    $metadata["gml_include_items"] = (count($queryables) == count($fields) ? "all" : implode(",", $queryables));
                    $metadata["ows_include_items"] = (count($queryables) == count($fields) ? "all" : implode(",", $queryables));
                    $metadata["gml_types"] = "auto";
                }

                return $metadata;
            },
            "classes" => function ($data) use ($layer) {
                $classes = $data["msClasses"];
                if (!$classes) return null;
                $label = (isset($data["msLabel"]) ? $data["msLabel"] : array());
                unset($label["id"]);
                foreach ($classes as $iClass => $classe) {
                    unset($classes[$iClass]["id"]);
                    if (!empty($label)) $classes[$iClass]["labels"] = array($label);
                    if (!array_key_exists(0, $classe["styles"])) {
                        $classes[$iClass]["styles"] = array($classe["styles"]);
                    }
                }
                return $classes;
            }

        );

        $data = $request->request->all();

        $msParams = array();
        foreach ($msLayerFields as $msKey => $value) {
            $postKey = "msLayer" . ucfirst($msKey);

            if (is_null($value)) continue;

            if (!is_null($value) && is_callable($value)) {
                $msParams[$msKey] = $value($data, $msParams);
            } elseif (!is_null($value) && is_string($value) && array_key_exists($value, $data)) {
                $msParams[$msKey] = $data[$value];
            } elseif (array_key_exists($postKey, $data)) {
                $msParams[$msKey] = $data[$postKey];
            } elseif (!is_null($value) && array_key_exists($msKey, $data)) {
                $msParams[$msKey] = $data[$msKey];
            } else {
                $this->getLogger()->debug(__METHOD__ . " not match " . $msKey);
            }
        }

        $setLayerSource = function (&$msParams) use ($data, $layer, $account) {
            $layerTypeCode = $layer->getLayerType()->getLayerTypeCode();


            switch ($layerTypeCode) {
                case LexLayerType::RASTER :
                    $msParams["connectiontype"] = "RASTER";
                    $msParams["connection"] = "";
                    if ($data["msGeometryType"] == "RASTER") {
                        $msParams["data"] = $data["msLayerData"];
                        $msParams["tileitem"] = "";
                    } else {
                        $msParams["tileindex"] = $data["msLayerData"];
                        $msParams["tileitem"] = "location";
                    }
                    break;
                case LexLayerType::VECTOR :
                    $msParams["template"] = "consultable";
                    $mapfiledir = str_replace($this->getCarmenConfig()->getMapserverAccountDir(), '', $this->getCarmenConfig()->getMapserverMapfileDir());
                    $msLayerData = str_replace($mapfiledir, './', $data["msLayerData"]);
                    $realpath = realpath($this->getCarmenConfig()->getMapserverMapfileDir() . '/' . $msLayerData);
                    // throw an exception if file does not exist in the account directory
                    if (false === $realpath) throw new \Exception("File does not exist: " . $data["msLayerData"]);
                    // str_replace assumes $realpath contains starts with the MapfileDir parameter string (ex. .../Publication)

                    $path_parts = pathinfo($msLayerData);
                    if (isset($path_parts["extension"]) && strtolower($path_parts["extension"]) == "shp") {
                        $msParams["data"] = $msLayerData;
                        $msParams["connectiontype"] = "INLINE";
                    } else {
                        $msParams["connection"] = $msLayerData;
                        $msParams["connectiontype"] = "OGR";
                    }
                    break;
                case LexLayerType::WFS :
                    $msParams["template"] = "consultable";
                    $msParams["metadata"]["wfs_request_method"] = "GET";
                    $msParams["metadata"]["wfs_maxfeatures"] = "1000";
                    $msParams["metadata"]["wfs_typename"] = $data["layerWxsname"];
                    $msParams["metadata"]["wfs_connectiontimeout"] = "60";
                    if (isset($data["layerServerVersion"]) && $data["layerServerVersion"] != "") {
                        $msParams["metadata"]["wfs_version"] = $data["layerServerVersion"];
                        //specific hack for mapserver bug with WFS 1.1.0 CLIENT, needs to call in 1.0.0 version
                        $msParams["metadata"]["wfs_version"] = "1.0.0";
                    }
                    if (isset($data["wfsSrs"]) && $data["wfsSrs"] != "" && $this->getRepository('CarmenApiBundle:LexProjection')->find($data["wfsSrs"])) $msParams["metadata"]["wfs_srs"] = $this->getRepository('CarmenApiBundle:LexProjection')->find($data["wfsSrs"])->getProjectionProvider() . ':' . $data["wfsSrs"];

                case LexLayerType::WMS :
                case LexLayerType::WMSC :
                    if ($layerTypeCode != LexLayerType::WFS) {
                        $msParams["metadata"]["wms_name"] = $data["layerWxsname"];
                        $msParams["metadata"]["wms_onlineresource"] = $data["layerServerUrl"];
                        $msParams["metadata"]["wms_format"] = $data["layerOutputformat"];
                        if (isset($data["wmsSrs"]) && $data["wmsSrs"] != "" && $this->getRepository('CarmenApiBundle:LexProjection')->find($data["wmsSrs"])) $msParams["metadata"]["wms_srs"] = strtoupper($this->getRepository('CarmenApiBundle:LexProjection')->find($data["wmsSrs"])->getProjectionProvider()) . ':' . $data["wmsSrs"];
                        if (isset($data["wmsTitle"]) && $data["wmsTitle"] != "") $msParams["metadata"]["wms_title"] = $data["wmsTitle"];
                        if (isset($data["layerServerVersion"]) && $data["layerServerVersion"] != "") $msParams["metadata"]["wms_server_version"] = $data["layerServerVersion"];
                    }
                    if ($layerTypeCode == LexLayerType::WMSC) {
                        $msParams["metadata"]["wmsc"] = "YES";
                        //$msParams["metadata"]["wmsc_boundingbox"] = "";
                        if (isset($data["wmscResolution"]) && $data["wmscResolution"] != "") $msParams["metadata"]["wmsc_resolution"] = $data["wmscResolution"];
                        if (isset($data["wmscWmsUrl"]) && $data["wmscWmsUrl"] != "") $msParams["metadata"]["wmsc_wms_onlineresource"] = $data["wmscWmsUrl"];
                    }

                    $msParams["connection"] = $data["layerServerUrl"];
                    $msParams["connectiontype"] = ($layerTypeCode == LexLayerType::WMSC ? LexLayerType::WMS : $layerTypeCode);
                    break;

                case LexLayerType::POSTGIS :
                    $msParams["template"] = "consultable";
                    $msParams["connection"] = sprintf(
                        "user=%s password=%s dbname=%s host=%s port=%s",
                        $account->getAccountUserpostgres(),
                        $account->getAccountPasswordpostgres(),
                        $account->getAccountDbpostgres(),
                        $account->getAccountHostpostgres(),
                        $account->getAccountPortpostgres()
                    );
                    $msParams["connectiontype"] = $layerTypeCode;
                    if (isset($data["msLayerData"]) && !empty($data["msLayerData"])) {
                        $msParams["data"] = $data["msLayerData"];
                    } else {
                        $msParams["data"] = sprintf(
                            "%s from (select * from %s) as foo using unique %s using srid=%s",
                            $data["msLayerPgGeometryField"],
                            ($data["msLayerPgSchema"] ?: "public") . "." . $data["msLayerPgTable"],
                            $data["msLayerPgIdField"],
                            $data["msLayerPgProjection"]
                        );
                    }


                    break;
                case LexLayerType::TILE_LAYER:
                case LexLayerType::GEOJSON:
                    $msParams["template"] = "consultable";
                    $msParams["connection"] = sprintf(
                        "user=%s password=%s dbname=%s host=%s port=%s",
                        $account->getAccountUserpostgres(),
                        $account->getAccountPasswordpostgres(),
                        $account->getAccountDbpostgres(),
                        $account->getAccountHostpostgres(),
                        $account->getAccountPortpostgres()
                    );

                    $prefix = 'MS_';
                    $str = $data['connectiontypename'];

                    if (substr($str, 0, strlen($prefix)) == $prefix) {
                        $str = substr($str, strlen($prefix));
                    }
                    $msParams["connectiontype"] = $str;

                    if (isset($data["msLayerData"]) && !empty($data["msLayerData"])) {
                        $msParams["data"] = $data["msLayerData"];
                    } else {
                        $msParams["data"] = sprintf(
                            "%s from (select * from %s) as foo using unique %s using srid=%s",
                            $data["msLayerPgGeometryField"],
                            ($data["msLayerPgSchema"] ?: "public") . "." . $data["msLayerPgTable"],
                            $data["msLayerPgIdField"],
                            $data["msLayerPgProjection"]
                        );
                    }

                    break;
            }
        };
        $setLayerSource($msParams);


        unset($msParams["metadata"]["id"]);
        return $msParams;
    }

    /**
     * Restfull api to serve Entity.
     *
     * @Route("/layer/{mapId}/rest/{id}", name="carmen_ws_layer", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"GET","POST","PUT","PATCH","DELETE"})
     *
     * @param Request $request request object
     * @param int $id layer id, null by default
     */
    public function layerRestAction(Request $request, $mapId, $id = null)
    {
        switch ($request->getMethod()) {
            case 'GET':
                return $this->getLayerAction($request, $mapId, $id);
                break;
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return $this->postLayerAction($request, $mapId, $id);
                break;
            case 'DELETE':
                return $this->deleteLayerAction($mapId, $id);
                break;
        }

        return $this->formatJsonError(Response::HTTP_METHOD_NOT_ALLOWED, $this->_t("Method not allowed"));
    }

    /**
     * Returns a list of Map or the defined layer by id.
     *
     * Extra filters can be added using query parameters such as 'filter[someColumn]=someValue'.
     * @param Request $request The request object.
     * @param string $mapId If set and $id is null, returns a list with the requested Map only (if exists).
     * @param string $id If set, returns the requested layer only (if exists).
     *
     * @Route("/layer/{mapId}/get/{layerId}", name="carmen_ws_get_layer", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     * @see BaseController::getQueryBuilderList()
     *
     */
    public function getLayerAction(Request $request, $mapId, $id = null, $bAsJson = false)
    {
        $user = $this->getUser();
        $user instanceof Users;
        $account = $user->getAccount();
        $account instanceof Account;
        $metadataFile = str_replace("__file__", "%s", $this->generateUrl("carmen_data_account_metadata_file", array("account_path" => $account->getAccountPath(), "file" => "__file__")));
        if ($metadataUrl = $account->getAccountGeonetworkurl())
            $metadataUrl = $metadataUrl . "/fre/find?uuid=%s";

        $logger = $this->getLogger();

        $filters = $request->query->get('filter', array());
        if (!is_array($filters) && !empty($filters)) {
            $filters = json_decode($filters, true) ?: array();
        }
        $property = array();
        $value = array();
        foreach ($filters as $filter) {
            foreach ($filter as $var_name => $var_value) {
                ${"$var_name"}[] = $var_value;
            }
        }
        $filters = array_combine($property, $value);

        $filters["map"] = $mapId;
        $qb = $this->getQueryBuilderList(self::LAYER_ENTITY, 'a', $id, $filters/*, true*/);

        // get mapserver data
        if (!is_null($id) && is_numeric($id)) {
            $data = $qb->getQuery()->execute();
            $oLayer = false;
            if (isset($data[0])) {
                $oLayer = $data[0];
            }
            if (!$oLayer) {
                return ($bAsJson ? array() : new JsonResponse(array()));
            }
            $oLayer instanceof Layer;
            $oLayer->getFields();
            $data = json_decode($this->jsonSerializeEntity($oLayer), true);
            $data["hrefLayerMetadataFile"] = "";
            $data["hrefLayerMetadataUuid"] = "";
            if ($data["layerMetadataFile"])
                $data["hrefLayerMetadataFile"] = sprintf($metadataFile, $data["layerMetadataFile"]);
            if ($data["layerMetadataUuid"] && $metadataUrl)
                $data["hrefLayerMetadataUuid"] = sprintf($metadataUrl, $data["layerMetadataUuid"]);

            // returns one layer with ms data
            $result = array();
            try {
                $oMap = $oLayer->getMap();
                $paramsGET = array(
                    "directory" => $this->getMapfileDirectory()
                );
                $msData = $this->callMapserverApi($this->getMapserverRoute($oMap, $oLayer), 'GET', $paramsGET);

                $logger->debug(__METHOD__ . " get ms data : " . print_r($msData, true));

                // merge msData to the first layer in data

                $data = array_merge($data, self::convertMapserverParams($msData));
            } catch (\Exception $exception) {
                if ($bAsJson) throw $exception;
                return $this->formatJsonError(Response::HTTP_UNPROCESSABLE_ENTITY, $this->_t("Unable to load layer"), $exception->getMessage());
            }
        } else {
            $data = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
            $logger->debug(__METHOD__ . " sgbd data : " . print_r($data, true));
        }

        return ($bAsJson ? $data : new JsonResponse($data));
    }

    /**
     * @Route("/layer/{mapId}/post/{id}", name="carmen_ws_post_layer", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"POST", "PUT", "PATCH"})
     *
     * @param Request $request request object.
     * @param string $id If set, updates an existing layer, else creates it.
     *
     * @return JsonResponse Json object.
     */
    public function postLayerAction(Request $request, $mapId, $id = null, $displayModeId = null)
    {
        $logger = $this->getLogger();
        $user = $this->getUser();
        if (!$user) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current user."));
        }
        $user instanceof Users;

        $account = $user->getAccount();
        if (!$account) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current account."));
        }

        return $this->transactional(
            function (EntityManager $em) use ($request, $mapId, $id, $account, $logger, $displayModeId) {

                //$logger = $this->getLogger();
                $bModeCreate = $id == null;
                $save = $request->request->all();

                $request->request->set("map", $mapId);

                $layerMsname = $request->request->get('layerMsname', null);
                if (empty($layerMsname)) {
                    $query = $em->createQuery("select l.layerMsname from " . LayerController::LAYER_ENTITY . " l where l.map=:map_id")
                        ->setParameter("map_id", $mapId);
                    $msnames = $query->getResult();
                    $count_layer = 0;
                    $max_id = 0;
                    foreach ($msnames as $msname) {
                        if (preg_match("!^layer(\d+)$!", $msname["layerMsname"])) {
                            $max_id = max($max_id, preg_replace_callback("!^layer(\d+)$!", function ($matches) {
                                return intval($matches[1]);
                            }, $msname["layerMsname"]));
                        }
                        $count_layer++;
                    }
                    $max_id = max($count_layer, $max_id) + 1;
                    $layerMsname = "layer" . $max_id;
                    $request->request->set('layerMsname', $layerMsname);
                    $save['layerMsname'] = $layerMsname;
                }

                /* remove posted data that won't fit in layer's doGenericPost action */
                $fields = $request->request->get('fields', array());
                $geojsonParams = $request->request->get('geojsonParams', array());

                $request->request->remove('fields');
                $request->request->remove('geojsonParams');

                // save layer in database
                $oLayer = $this->doGenericPost($request, LayerController::LAYER_ENTITY, $id, null, true);
                $request->request->replace($save);

                $oLayer instanceof Layer;
                $layerId = $oLayer->getLayerId();
                $oMap = $this->getRepository(LayerController::MAP_ENTITY)->find($oLayer->getMap());


                foreach (array("MapTree" => "map_tree", "MapTreePrint" => "map_tree_print") as $tree_type => $tree_table) {
                    $nodetree = $this->getRepository('CarmenApiBundle:' . $tree_type)->findOneBy(array("map" => $mapId, "nodeId" => $layerId));
                    if (!$nodetree) {
                        $em->createNativeQuery("update carmen.{$tree_table} set node_pos=node_pos+1 where map_id=:map_id and node_parent is null", new ResultSetMapping())
                            ->setParameter("map_id", $mapId)
                            ->execute();
                        $class_tree = "Carmen\\ApiBundle\\Entity\\" . $tree_type;
                        $nodetree = new $class_tree();
                        $nodetree->setId($layerId);
                        $nodetree->setMap($oMap);
                        $nodetree->setNodeIsLayer(true);
                        $nodetree->setNodeOpened(false);
                        $nodetree->setNodePos(0);
                        $em->persist($nodetree);
                    }
                }

                // Sauvegarde des geojsonParams
                $geojsonParamIds = [];
                foreach ($geojsonParams as $params) {
                    if (isset($params['geojsonParamsId'])) {
                        $this->getRepository('CarmenApiBundle:GeojsonParams')->update($params['geojsonParamsId'], $params);
                    }
                }


                /* do save other data posted along with the layer */
                // before persisting modified or new data, delete those who needs to be removed
                $oldIds = array_map(function ($field) {
                    return $field->getFieldId();
                }, $oLayer->getFields()->toArray());
                $newIds = array_map(function ($field) {
                    return isset($field['fieldId']) ? $field['fieldId'] : null;
                }, $fields);
                foreach (array_diff($oldIds, $newIds) as $fieldId) {
                    $field = $this->getRepository('CarmenApiBundle:Field')->find($fieldId);
                    if ($field) {
                        $em->remove($field);
                        $em->flush();
                    }
                }
                $oLayer->getFields()->clear(); // clear layer fields collection and re-add them below
                $rank = 0;
                foreach ($fields as $fieldData) {
                    unset($fieldData['id']); // ensure extjs generic id is not present
                    $fieldData['fieldRank'] = $rank; // reset 0-indexed rank for each field
                    $fieldData['layer'] = $oLayer->getLayerId(); // reset field layer FK
                    $fieldDatatype = $this->getRepository('CarmenApiBundle:LexFieldDatatype')->findOneBy(array('fieldDatatypeCode' => $fieldData['fieldDatatype']));
                    $fieldData['fieldDatatype'] = $fieldDatatype ? $fieldDatatype->getFieldDatatypeId() : null;
                    $field = null;
                    if (isset($fieldData['fieldId']) && is_numeric($fieldData['fieldId']) && $fieldData['fieldId'] > 0) {
                        // update this existing field
                        $fieldRequest = new Request(array()/*query*/, $fieldData/*request*/);
                        $field = $this->doGenericPost($fieldRequest, LayerController::FIELD_ENTITY, $fieldData['fieldId'], null, true);
                    } else {
                        // add a new field
                        unset($fieldData['fieldId']); // just in case
                        $fieldRequest = new Request(array()/*query*/, $fieldData/*request*/);
                        $field = $this->doGenericPost($fieldRequest, LayerController::FIELD_ENTITY, null, null, true);
                    }
                    // add the field to the layer's field collection
                    $oLayer->getFields()->add($field);
                    $rank++;
                }

                // insert or update geosource metadata ?
                if ($this->getRequest()->request->has('geosourceLayerMetadata')) {
                    $metadata = $this->getRequest()->request->get('geosourceLayerMetadata');
                    $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': geosource layer medatata', $metadata);

                    if ($metadata['needsUpdate'] == true || $metadata['uuid'] != "") {
                        $paramsGET = array(
                            "directory" => $this->getMapfileDirectory()
                        );
                        $msData = $this->callMapserverApi($this->getMapserverRoute($oMap, $oLayer), 'GET', $paramsGET);
                        $uuid = $this->createOrUpdateGeosourceLayerMetadata(
                            $oLayer,
                            $msData,
                            $metadata['uuid'],
                            $metadata['publish'],
                            $metadata['cswTitle'],
                            $metadata['cswAbstract'],
                            $metadata['cswKeywords'],
                            $metadata['cswInspiretheme']
                        );
                        if ($uuid && $uuid != $oLayer->getLayerMetadataUuid()) {
                            $oLayer->setLayerMetadataUuid($uuid);
                            $em->persist($oLayer);
                            $em->flush();
                        }
                    }
                }

                // write layer in mapfile
                $msParams = $this->getMapserverParams($request, $oLayer, $account);
                //var_dump($msParams);
                //die();

                try {

                    $result = $this->callMapserverApi($this->getMapserverRoute($oMap, $oLayer), 'PUT', array(), $msParams);

                    //$logger->debug(__METHOD__." callMapServerApi1 PUT : ".print_r($msParams, true).", result=".print_r($result, true));

                    // if we are creating a new layer of type vector, WFS or PostGis (see. lex_layer_type)
                    // get layer fields from the mapserver api and add them to the database
                    if ($bModeCreate && in_array($oLayer->getLayerType()->getLayerTypeCode(), array(LexLayerType::VECTOR, LexLayerType::WFS, LexLayerType::POSTGIS, LexLayerType::GEOJSON, LexLayerType::TILE_LAYER))) {
                        if ($displayModeId) {

                            $displayMode = $this->getRepository("CarmenApiBundle:LexDisplayMode")->find($displayModeId);

                            if ($displayMode && $displayMode->getDisplayModeName() == 'vector') {
                                $layerType = $this->getRepository("CarmenApiBundle:LexlayerType")->findOneBy(array('layerTypeCode' => 'GEOJSON'));
                                if ($layerType) {
                                    $oLayer->setLayerType($layerType);
                                }

                                $oLayer->setLayerDisplayMode($displayMode);


                                $geojsonParams = new GeojsonParams();
                                $geojsonParams->setLayer($oLayer);

                                $oLayer->getGeojsonParams()->add($geojsonParams);


                            } else if ($displayMode && $displayMode->getDisplayModeName() == 'tile_vector') {
                                $layerType = $this->getRepository("CarmenApiBundle:LexlayerType")->findOneBy(array('layerTypeCode' => 'TILE_VECTOR'));
                                if ($layerType) {
                                    $oLayer->setLayerType($layerType);
                                }

                                $oLayer->setLayerDisplayMode($displayMode);
                            } else {
                                $oLayer->setLayerDisplayMode($displayMode);
                            }
                        }
                        $paramsGET = array(
                            "directory" => $this->getMapfileDirectory()
                        );
                        $result = $this->callMapserverApi($this->getMapserverRoute($oMap, $oLayer) . '/fields', 'GET', $paramsGET);

                        $logger->debug(__CLASS__ . '::' . __METHOD__ . ": callMapServerApi GET", array($this->getMapserverRoute($oMap, $oLayer) . '/fields', $paramsGET, $result));
                        if ($result && isset($result['fields']) && is_array($result['fields'])) {
                            if ($result["fid"] != "") {
                                $result['fields'] = array_merge(array($result["fid"] => "Integer"), $result['fields']);
                            }

                            $cpt = 0;
                            $type = $this->getRepository('CarmenApiBundle:LexFieldType')->find(1); // Field type 'NORMAL'
                            foreach ($result['fields'] as $fieldName => $fieldDatatype) {
                                $field = new Field();
                                $field->setLayer($oLayer);
                                $field->setFieldType($type);
                                $datatype = Helpers::getCarmenType($fieldDatatype);
                                $lexFieldDatatype = $datatype ? $this->getRepository('CarmenApiBundle:LexFieldDatatype')->find($datatype) : null;
                                $field->setFieldDatatype($lexFieldDatatype);
                                $field->setFieldName($fieldName);
                                $field->setFieldAlias($fieldName);
                                $field->setFieldRank($cpt);
                                $em->persist($field);
                                // add the field to the layer's field collection
                                $oLayer->getFields()->add($field);
                                $cpt++;
                            }
                        }


                    }

                } catch (\Exception $exception) {
                    die($exception);
                    throw $exception;
                }

                return array("layerId" => $layerId, "map" => $oMap);
            },
            function ($data) {

                return $this->returnLayer($data);
            }
        );

    }

    /**
     * @param Request $request The request object.
     * @param string $id If set, updates an existing user, else creates it.
     *
     * @return JsonResponse Json object.
     * @todo change get/post route definition
     * @Route("/layer/post/multi", name="carmen_ws_post_multi_layers", options={"expose"=true}, methods={"POST", "PUT", "PATCH"})
     *
     */
    public function postMultiLayersAction(Request $request)
    {
        try {
            $save = $request->request->all();
            $layers = $request->request->get('layers') ?: array();
            foreach ($layers as $data) {
                $this->transactional(
                    function () use ($request, $data) {
                        unset($data["fields"]);
                        $request->request->replace($data);
                        $oLayer = $this->doGenericPost($request, LayerController::LAYER_ENTITY, $data["layerId"], null, true);
                        $oLayer instanceof Layer;
                        //$this->getOWSContext($oLayer->getLayerId());
                    }, null, function (\Exception $exception) {
                    throw $exception;
                }
                );
            }
            $request->request->replace($save);
        } catch (\Exception $exception) {
            $data = array();
            if ($exception instanceof ApiException) {
                $data = $exception->getData();
            }
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Transactional Exception") . ' : ' . $exception->getMessage(), $data);
        }
        return $this->formatJsonSuccess();
    }

    /**
     * Deletes an Entity.
     *
     * @Route("/layer/{mapId}/delete/{id}", name="carmen_ws_delete_layer", requirements={"mapId"="\d+", "id"="\d+"}, options={"expose"=true}, methods={"GET", "DELETE"})
     *
     * @param string $id layer id to delete
     *
     * @return Response Empty response if success, JsonResponse error if failed.
     */
    public function deleteLayerAction($mapId, $id)
    {
        $me = $this;
        $layerId = $id;
        return $this->transactional(
            function (EntityManager $em) use ($me, $mapId, $layerId) {
                return self::_deleteLayer($this, $mapId, $layerId);
            },
            function ($data) {
                return $this->returnLayer($data);
            }
        );
    }

    public static function _deleteLayer(BaseController $controller, $mapId, $layerId)
    {
        $me = $controller;
        return $controller->transactional(
            function (EntityManager $em) use ($me, $mapId, $layerId) {
                if (null != $layerId) {
                    $edit_directory = $me->getMapfileDirectory();
                    $read_directory = $me->getMapfileDirectory();
                    $files = array();

                    $oMap = $me->getRepository(self::MAP_ENTITY)->find($mapId);
                    if (!$oMap) {
                        throw new EntityNotFoundException();
                    }
                    $oMap instanceof Map;

                    $oLayer = $me->getRepository(self::LAYER_ENTITY)->find($layerId);
                    if (!$oLayer) {
                        throw new EntityNotFoundException();
                    }
                    $oLayer instanceof Layer;

                    try {
                        $result = $me->callMapserverApi(self::_getMapserverRoute($me, $oMap, $oLayer), 'DELETE', array("directory" => $edit_directory), array());
                    } catch (\Exception $exception) {
                        //throw $exception;
                    }
                    $me->removeAll($em, $oLayer->getFields());
                    $me->removeAll($em, $oLayer->getGeojsonParams());
                    $em->remove($oLayer);
                    $em->flush();


                    foreach (array('MapTree', 'MapTreePrint') as $treeType) {
                        $trees = $me->getRepository('CarmenApiBundle:' . $treeType)->findBy(array("map" => $oMap, "nodeId" => $layerId, "nodeIsLayer" => true));
                        foreach ($trees as $tree) {
                            $em->remove($tree);
                            $em->flush();
                        }
                    }
                    return array("layerId" => $layerId, "map" => $oMap);
                }
            },
            function ($data) {
                return $data;
            },
            function (\Exception $exception) {
                throw $exception;
            }
        );
    }

    /**
     * Construct the JsonResponse for a map :
     * <li>generate and get OWSContext</li>
     * @param array $data
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function returnLayer($data)
    {
        $map = $data["map"];
        $real_mapfile = pathinfo($map->getFullMapfilePath(), PATHINFO_FILENAME);
        if (empty($real_mapfile)) {
            $real_mapfile = $this->ensureEditingMapfile($map, $map->getPublishedMap());
        }
        $this->getOWSContext($map->getMapId(), $map->getMapFile());

        $result = array(
            "layer" => $this->getLayerAction($this->getRequest(), $map->getMapId(), $data["layerId"], true),
            "layerId" => $data["layerId"],
            "jsonContext" => $this->getOWSContext($map->getMapId(), $map->getMapFile())
        );
        return $this->formatJsonSuccess($result);
    }

    /**
     * Save all registered wms|wfs|wmsc services for current account in a XML file
     *
     * @param string $wxs_type take one value of (WFS|WMS|WMSC)
     *
     * @return boolean success of save
     */
    public function generateXmlForWxsServices($wxs_type)
    {
        $wxs_type = strtoupper($wxs_type);

        $user = $this->getUser();
        if (!$user) {
            throw new \Exception($this->_t("Unable to find current user."));
        }
        $user instanceof Users;

        $account = $user->getAccount();
        $account_id = ($account ? $account->getAccountId() : 0);
        if ($account_id <= 0) {
            throw new \Exception($this->_t("Unable to find current account."));
        }

        $fileNames = array(
            "WMS" => array("ServeursWMS_Consultation.xml"),
            "WFS" => array("ServeursWFS_Consultation.xml"),
        );

        if (!isset($fileNames[$wxs_type])) {
            return false;
        }

        $directory = $this->getMapfileDirectory();
        $traduction = array(
            "root" => "SERVEURS",
            "element" => "SERVEUR",
            "attributes" => array(
                "NOM" => "wxsName",
                "URL" => "wxsUrl"
            )
        );
        $andWhere = array();
        foreach ($traduction["attributes"] as $field) {
            $andWhere[] = "e." . $field . " is not null";
            $andWhere[] = "e." . $field . " <> ''";
        }
        $qb = $this->createQueryBuilder(LayerController::WXS_ENTITY, 'e')
            ->select("e")
            ->join('e.wxsType', 't')
            ->where('t.wxsTypeName=?1')
            ->andWhere('e.account=?2')
            ->orderBy('e.wxsName', 'ASC')
            ->andWhere(implode(" and ", $andWhere))
            ->setParameter(1, $wxs_type)
            ->setParameter(2, $account_id);


        $root = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?>
          <{$traduction["root"]}>
          </{$traduction["root"]}>
        ");
        $servers = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        foreach ($servers as $server) {
            $element = $root->addChild($traduction["element"]);
            foreach ($traduction["attributes"] as $attribute => $field) {
                $element->addAttribute($attribute, $server[$field]);
            }
        }

        foreach ($fileNames[$wxs_type] as $fileName) {
            $root->asXML($directory . "/" . $fileName);
        }
        return true;
    }

    /**
     * Return list of wms|wfs|wmsc saved services for current account
     * @Route("/layer/saved_services/{wxs_type}",
     *        name="carmen_ws_get_saved_services",
     *        requirements={"wxs_type"="WMS|WFS|WMSC|WMTS|vector_tile|vector_tile_style"},
     *        options={"expose"=true},
     *        methods={"GET"})
     *
     * @param Request $request The request object.
     * @param string $wxs_type take one value of (WFS|WMS|WMSC|WMTS|vector_tile|vector_tile_style)
     *
     * @return JsonResponse Json object.
     */
    public function getWMSSavedServicesAction(Request $request, $wxs_type)
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current user."));
        }
        $user instanceof Users;

        $account = $user->getAccount();
        $account_id = ($account ? $account->getAccountId() : 0);
        if ($account_id <= 0) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current account."));
        }

        try {
            $qb = $this->createQueryBuilder(LayerController::WXS_ENTITY, 'e')
                ->select("e")
                ->join('e.wxsType', 't')
                ->where('t.wxsTypeName=?1')
                ->andWhere('e.account=?2')
                ->orderBy('e.wxsName', 'ASC')
                ->setParameter(1, $wxs_type)
                ->setParameter(2, $account_id);

            $this->generateXmlForWxsServices($wxs_type);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("API Exception"), $this->isProd() ? array() : array('exception' => $ex->getMessage()));
        }

        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * add wms|wfs service to saved services for current account
     * @Route("/layer/saved_services/{wxs_type}",
     *        name="carmen_ws_post_saved_services",
     *        requirements={"wxs_type"="WMS|WFS|WMTS|WMSC|vector_tile|vector_tile_style"},
     *        options={"expose"=true},
     *        methods={"POST"})
     *
     * @param Request $request The request object.
     * @param string $wxs_type take one value of (WFS|WMS|WMTS|WMSC|vector_tile|vector_tile_style)
     *
     * @return JsonResponse Json object.
     */
    public function postWMSSavedServicesAction(Request $request, $wxs_type)
    {

        try {
            $user = $this->getUser();
            if (!$user) {
                throw new \Exception($this->_t("Unable to find current user."));
            }
            $user instanceof Users;

            $account = $user->getAccount();
            $account_id = ($account ? $account->getAccountId() : 0);
            if ($account_id <= 0) {
                throw new \Exception($this->_t("Unable to find current account."));
            }

            $wxsTypeEntity = $this->getRepository(LayerController::LEXWXSTYPE_ENTITY)->findOneBy(array("wxsTypeName" => $wxs_type));
            if (!$wxsTypeEntity) {
                throw new \Exception($this->_t("Unable to find wxs type."));
            }
            $wxsTypeId = $wxsTypeEntity->getWxsTypeId();

            $msgErr = "";
            $wxsName = $request->request->get('wxsName');
            $wxsUrl = $request->request->get('wxsUrl');

            if ($wxsName == "") {
                throw new \Exception($this->_t("You should enter a valid service name."));
            }

            $request->request->set("account", $account_id);
            $request->request->set("wxsType", $wxsTypeId);
            $request->request->set("wxsRank", 0);

            $wxsEntity = $this->getRepository(LayerController::WXS_ENTITY)->findOneBy(array("wxsUrl" => $wxsUrl,
                "wxsType" => $wxsTypeId
            ));
            if ($wxsEntity) {
                $wxsEntity instanceof Wxs;
                $wxsEntity = $wxsEntity->getWxsId();
            }
            try {
                $wxsEntity = $this->doGenericPost($request, LayerController::WXS_ENTITY, $wxsEntity, null, true);
                $this->generateXmlForWxsServices($wxs_type);
            } catch (\Exception $e) {
                throw new \Exception($this->_t("Unable to save the service."), Response::HTTP_INTERNAL_SERVER_ERROR, $e);
            }

            return $this->formatJsonSuccess(array("wxsId" => $wxsEntity->getWxsId()));
        } catch (\Exception $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $exception->getMessage());
        }
    }

    /**
     * Remove wms service from saved services for current account
     * * @Route("/layer/saved_services/{wxs_type}",
     *        name="carmen_ws_delete_saved_services",
     *        requirements={"wxs_type"="WMS|WFS|WMTS|vector_tile_style|vector_tile"},
     *        options={"expose"=true},
     *        methods={"DELETE"})
     *
     * @param Request $request The request object.
     * @param string $wxs_type take one value of (WFS|WMS|WMTS|vector_tile_style|vector_tile)
     *
     * @return JsonResponse Json object.
     */
    public function delWMSSavedServicesAction(Request $request, $wxs_type)
    {
        $msgErr = "";

        $em = $this->getManager();
        $wxsId = $request->request->get('wxsId');

        $oWXS = $em->getRepository(LayerController::WXS_ENTITY)->find($wxsId);
        if (!$oWXS) {
            $msgErr = $this->_t('Unable to find the service to be deleted');
        } else {
            try {
                $em->remove($oWXS);
                $em->flush();
                $this->generateXmlForWxsServices($wxs_type);
            } catch (\Exception $e) {
                $msgErr = $this->_t('Unable to delete the service');
            }
        }

        if ($msgErr == "") {
            return $this->formatJsonSuccess();
        }

        return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $msgErr);
    }

    /**
     * Return the list of postgres schema for the current account
     * @Route("/layer/information/{url}",
     *        name="carmen_ws_get_layer_information",
     *        options={"expose"=true},
     *        methods={"GET"})
     *
     * @param Request $request The request object.
     *
     * @return Response
     */
    public function getLayerInformationAction(Request $request, $url)
    {
        $args = func_get_args();
        $url = urldecode($url);
        try {
            //Verify if you can connect to URL
            $params = array();

            $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);  # TRUE to follow any "Location: " header that the server sends as part of the HTTP header
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // if set to true, curl_exec() return body content in case of success
            curl_setopt($curl, CURLOPT_FAILONERROR, false); // if set to true, curl_exec() will return false if response status code is >= 400
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::CONST_CURLOPT_CONNECTTIMEOUT);

            $result = curl_exec($curl);

            if (!$result) {
                $result = array(
                    "success" => false,
                    "status_code" => curl_errno($curl),
                    "error" => curl_error($curl),
                    "data" => array_merge($args, array('url' => $url))
                );
            }

            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($httpCode === 403) {
                //try it with referer
                $referer = $this->getRequest()->headers->get('referer');
                curl_setopt($curl, CURLOPT_REFERER, $referer);
                $result = curl_exec($curl);
                if (!$result) {
                    $result = array(
                        "success" => false,
                        "status_code" => curl_errno($curl),
                        "error" => curl_error($curl),
                        "data" => array_merge($args, array('url' => $url))
                    );
                }
            }

            curl_close($curl);

            $status_code = array(
                "timeout" => 28
            );

            if ((is_array($result) && isset($result["success"]) && !$result["success"] && in_array($result["status_code"], $status_code))
                || (is_bool($result) && $result === false)
            ) {
                throw new \Exception(sprintf($this->_t("Can not connect to server %s"), $url));
            }
            if (empty($result)) {
                throw new \Exception(sprintf($this->_t("No data when query the server %s"), $url));
            }

            $encoding = mb_detect_encoding((is_array($result) ? implode($result) : $result));

            $dom = @\DOMDocument::loadXml($result);

            if (!($dom && ($dom instanceof \DOMDocument) &&
                (substr($dom->documentElement->tagName, -16) == "WMS_Capabilities")
                || substr($dom->documentElement->tagName, -16) == "WFS_Capabilities"
                || substr($dom->documentElement->tagName, -12) == "Capabilities"
            )) {
                throw new \Exception(strip_tags($result));
            }
            if ($encoding != $dom->encoding && $dom->encoding != "") {
                $result = mb_convert_encoding($result, $encoding, $dom->encoding);
            }
            $dom->loadXML($result);
            $dom->formatOutput = true;
            $dom->preserveWhiteSpace = true;
            $result = $dom->saveXML();
            $dom = null;
        } catch (\Exception $e) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $e->getMessage());
        }

        return new Response($result);
    }

    /**
     * Return the list of postgres schema for the current account
     * @Route("/layer/pg_schema",
     *        name="carmen_ws_get_pg_schema",
     *        options={"expose"=true},
     *        methods={"GET"})
     *
     * @param Request $request The request object.
     *
     * @return JsonResponse Json object.
     */
    public function getPostgresSchemaListAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current user."));
        }
        $user instanceof Users;

        $account = $user->getAccount();
        if (!$account) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current account."));
        }

        $schema = array();
        try {
            $config = new \Doctrine\DBAL\Configuration();

            $connectionParams = array(
                'dbname' => $account->getAccountDbpostgres(),
                'user' => $account->getAccountUserpostgres(),
                'password' => $account->getAccountPasswordpostgres(),
                'host' => $account->getAccountHostpostgres(),
                'driver' => 'pdo_pgsql'
            );
            $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

            $sql = "select distinct schemaname as name" .
                " from pg_tables" .
                " where schemaname<>'pg_catalog'" .
                "   and schemaname<>'information_schema'" .
                " union" .
                " select distinct schemaname as name" .
                " from pg_views" .
                " where schemaname<>'pg_catalog'" .
                "   and schemaname<>'information_schema'" .
                " order by name";
            $schema = $conn->fetchAll($sql);
        } catch (\Exception $e) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to read schema list from Postgis."));
        }

        return new JsonResponse($schema);
    }

    /**
     * Return the list of postgres tables and views for the current account
     * @Route("/layer/pg_table/{pg_schema}/{pg_table}",
     *        name="carmen_ws_get_pg_table",
     *        defaults={"pg_table"=""},
     *        options={"expose"=true},
     *        methods={"GET"})
     *
     * @param Request $request The request object.
     * @param string $pg_schema ="" by default, filter the table list by pg_schema otherwise
     *
     * @return JsonResponse Json object.
     */
    public function getPostgresTableListAction(Request $request, $pg_schema, $pg_table = "")
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current user."));
        }
        $user instanceof Users;

        $account = $user->getAccount();
        if (!$account) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to find current account."));
        }

        $result = array();
        try {
            $config = new \Doctrine\DBAL\Configuration();

            $connectionParams = array(
                'dbname' => $account->getAccountDbpostgres(),
                'user' => $account->getAccountUserpostgres(),
                'password' => $account->getAccountPasswordpostgres(),
                'host' => $account->getAccountHostpostgres(),
                'driver' => 'pdo_pgsql'
            );
            $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

            if ($pg_table == "") {
                // select tables
                $sql = "select distinct tablename as value, tablename as text" .
                    " from pg_tables" .
                    " where schemaname<>'pg_catalog'" .
                    "   and schemaname<>'information_schema'" .
                    ($pg_schema != ""
                        ? " and schemaname=" . $conn->quote($pg_schema)
                        : "") .

                    " union" .
                    " select distinct viewname as value, " .
                    "viewname || '*' as text" .
                    " from pg_views" .
                    " where schemaname<>'pg_catalog'" .
                    "   and schemaname<>'information_schema'" .
                    ($pg_schema != ""
                        ? " and schemaname=" . $conn->quote($pg_schema)
                        : "") .
                    " order by value";
                $result = $conn->fetchAll($sql);
            } else {
                $result = array();

                // select the informations of geometrie
                $sql = "select f_geometry_column as geometry_column, srid, type" .
                    " from geometry_columns" .
                    " where f_table_schema=" . $conn->quote($pg_schema) .
                    "   and f_table_name=" . $conn->quote($pg_table) .
                    " order by geometry_column";
                $result["geoms"] = $conn->fetchAll($sql);

                // select the list of fields
                $sql = "SELECT col.attname as name" .
                    " from pg_attribute col" .
                    "   inner join pg_class tbl on col.attrelid = tbl.oid" .
                    "   inner join pg_type as t on t.oid = col.atttypid" .
                    " where tbl.relname=" . $conn->quote($pg_table) .
                    "   and t.typname<>'geometry'" .
                    "   and col.attnum>0";
                $result["fields"] = $conn->fetchAll($sql);

                switch ($result["geoms"][0]["type"]) {
                    case "GEOMETRY" :
                    case "GEOMETRYCOLLECTION" :
                    case "POLYGON" :
                    case "MULTIPOLYGON" :
                        $result["geoms"][0]["msGeometryType"] = "POLYGON";
                        break;
                    case "POINT" :
                    case "MULTIPOINT" :
                        $result["geoms"][0]["msGeometryType"] = "POINT";
                        break;
                    case "LINE" :
                    case "MULTILINE" :
                    case "LINESTRING" :
                    case "MULTILINESTRING" :
                        $result["geoms"][0]["msGeometryType"] = "LINE";
                        break;
                }
            }
        } catch (\Exception $e) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"),
                $this->_t("Unable to read data postgis."));
        }

        return new JsonResponse($result);
    }
}
