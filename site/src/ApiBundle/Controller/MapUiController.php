<?php

namespace Carmen\ApiBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Carmen\ApiBundle\Entity\Account;
use Carmen\ApiBundle\Entity\AccountModel;
use Carmen\ApiBundle\Entity\Adherent;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\Users;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\VarDumper;

// use FOS\RestBundle\Controller\Annotations\Delete;
// use FOS\RestBundle\Controller\Annotations\Get;
// use FOS\RestBundle\Controller\Annotations\Route;
// use FOS\RestBundle\Controller\Annotations\Head;
// use FOS\RestBundle\Controller\Annotations\Link;
// use FOS\RestBundle\Controller\Annotations\Patch;
// use FOS\RestBundle\Controller\Annotations\Post;
// use FOS\RestBundle\Controller\Annotations\Put;
// use FOS\RestBundle\Controller\Annotations\Unlink;
// use FOS\RestBundle\Request\ParamFetcher;
// use FOS\RestBundle\Controller\Annotations\QueryParam;   // GET params
// use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params

// use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Carmen MapUi Controller.
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/api/mapui")
 */
class MapUiController extends BaseController
{
    const MAPUI_ENTITY = 'CarmenApiBundle:MapUi';
    
    const ROOT_BANNERS     = '/IHM/BannerModels/';
    const ROOT_PRINTMODELS = '/IHM/LayoutModels/';
    const ROOT_IMAGES      = '/IHM/images/';

    protected static $DEFAULTS_BANNER = array(
        "^bandeau\.html$"
    );
    protected static $DEFAULTS_PRINT = array(
        "^modele[1-4]?\.tpl$"
    );
    
    /**
     * Returns fonts formatted for use with a combobox.
     *
     * @Route("/combobox/fonts", name="carmen_ws_mapui_combobox_fonts", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getMapUiFontsForCombo(Request $request)
    {
        $fonts = array();
        $dir = $this->getMapfileDirectory(); // get the AccountDir /Publication directory
        if( $dir ) {
            $fontFile = $dir.'/etc/fonts.txt';
            if( file_exists($fontFile) ) {
                foreach (file($fontFile, FILE_IGNORE_NEW_LINES) as $line) {
                    if(isset($line[0]) && isset($line[1])){
                        list($value, $id) = explode(" ", $line);
                        $fonts[] = array('id'=>$id, 'value'=>$value);
                    }
                }
            } else {
                // TODO log file not found ?
            }
        }

        return new JsonResponse($fonts);
    }
    
    /**
     * Returns banner files formatted for use with a combobox.
     *
     * @Route("/combobox/images", name="carmen_ws_mapui_combobox_images", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getMapUiImagesForCombo(Request $request)
    {
        $images = array();
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $dir = $dir.self::ROOT_IMAGES;
            if ( is_dir($dir) ){
            foreach (glob($dir.'*.*') as $filename) {
                $filename = basename($filename);
                $images[] = array('id'=>$filename, 'value'=>$filename);
            }
        }
        }
    
        return new JsonResponse($images);
    }

    /**
     * Converti le contenu vers smarty ou vers l'éditeur
     * @param unknown $contents
     */
    public function translateBannerModelFile($contents, $toFile=true)
    {
    	$file_text = array('<div id="CARTE_NOM">', '[Titre de la carte]', '</div>');
    	$editor_text = '[[Titre de la carte]]';
      if ( $toFile ){
      	$contents = str_replace($editor_text, implode($file_text), $contents);
      } else {
      	$file_text[1] = '.*?';
      	$contents = preg_replace("!".implode($file_text)."!s", $editor_text, $contents);
      }
      return $contents;
    }
    
    /**
     * Returns banner files formatted for use with a combobox.
     *
     * @Route("/combobox/bannermodels", name="carmen_ws_mapui_combobox_bannermodels", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getMapUiBannerModelsForCombo(Request $request)
    {
        $banners = array();
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $bannerModels = $dir.self::ROOT_BANNERS;
            foreach (glob($bannerModels.'*.html') as $filename) {
                $filename = basename($filename);
                $banners[] = array(
                  'id'      => $filename, 
                  'value'   => str_replace('.html', '', $filename), 
                  'default' => (bool)preg_match('!'.implode('|', self::$DEFAULTS_BANNER).'!', $filename)
                );
            }
        }

        return new JsonResponse($banners);
    }
    
    /**
     * Returns banner files formatted for use with a combobox.
     *
     * @Route("/bannerfile/{bannerfile}", name="carmen_ws_mapui_get_bannerfile", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getMapUiBannerFile(Request $request, $bannerfile)
    {
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $bannerfile = $dir.self::ROOT_BANNERS.$bannerfile;
            if( file_exists($bannerfile) ) {
                $contents = file_get_contents($bannerfile);
                return new Response($this->translateBannerModelFile($contents, false));
            } else {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "File not found: ".basename($bannerfile));
            }
        } else {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Account directory not found");
        }
    }
    
    /**
     * Creates or updates a banner files.
     *
     * @Route("/bannerfile/{bannerfile}", name="carmen_ws_mapui_post_bannerfile", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function postMapUiBannerFile(Request $request, $bannerfile)
    {
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $bannerfilepath = $dir.self::ROOT_BANNERS.$bannerfile;
            
            $content = $request->request->get('content', null);
            $template = $request->request->get('template', null);
            $create_mode = false;
            if( !$content && $template ) {
                $create_mode = true;
                $template = $dir.self::ROOT_BANNERS.$template;
                if ( file_exists($template) ){
                    $content = file_get_contents($template);
                }
                
            }
            
            if( !$content ) {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Parameter 'content' must be set");
            }
            
            $content = $this->translateBannerModelFile($content, true);
            if( false === file_put_contents($bannerfilepath, $content) ) {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Unable to write file: ".basename($bannerfile));
            }
            if ( $create_mode ){
                return new JsonResponse(array(
                  'id'      => $bannerfile, 
                  'value'   => str_replace('.html', '', $bannerfile), 
                  'default' => (bool)preg_match('!'.implode('|', self::$DEFAULTS_BANNER).'!', $bannerfile)
                )); 
            }
            return $this->getMapUiBannerFile($request, $bannerfile);
        } else {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Account directory not found");
        }
    }
    
    /**
     * Removes a banner files.
     *
     * @Route("/bannerfile/{bannerfile}", name="carmen_ws_mapui_delete_bannerfile", options={"expose"=true}, methods={"DELETE"})
     *
     * @return JsonResponse Json array.
     */
    public function deleteMapUiBannerFile(Request $request, $bannerfile)
    {
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $bannerfile = $dir.self::ROOT_BANNERS.$bannerfile;
            if ( !file_exists($bannerfile) ) return new Response();
            if( false === @unlink($bannerfile) ) {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Unable to delete file: ".basename($bannerfile));
            }
            return new Response();
        } else {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Account directory not found");
        }
    }
    
    /**
     * Converti le contenu vers smarty ou vers l'éditeur
     * @param unknown $contents
     */
    public function translatePrintModelFile($contents, $toSmarty=true)
    {
    	$DEFAULT_HTML_BEFORE = 
<<<'HTML'
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<title>{$owsTitle}</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="Légende et carte de situation à droite" http-equiv="ModelDescription">
<link rel="stylesheet" type="text/css" href="{$path_to_IHM}JavaScript/extjs/resources/css/ext-all.css">
<link rel="stylesheet" type="text/css" href="{$path_to_IHM}extjsOverload/olive/css/xtheme-olive.css">
<link rel="stylesheet" type="text/css" href="{$path_to_IHM}css/print.css">
<link rel="stylesheet" type="text/css" href="css/consultation.css">
</head>
<body>{$print}
HTML;
    	$DEFAULT_HTML_AFTER = 
<<<'HTML'
</body>
</html>
HTML;
      $traduction = array(
        // smarty => editeur
        "title" => "Titre",
        "comment" => "Commentaire",
        "keymap" => "Carte de situation",
        "map" => "Carte",
        "legend" => "Legende",
        "copyright" => "Copyright",
      );
      $editeur_pattern = array('[[@1]]', '[@1]', );
      $smarty_pattern = '{if $@1_alias != \'\' && $@1==\'\'}%{$@1_alias}%{else}%{$@1}%{/if}';
      
      foreach ($traduction as $smartyTag=>$editeurTag){
      	foreach($editeur_pattern as $e_pattern){
	        if ( $toSmarty ){
		      	$from_pattern = preg_quote( str_replace("@1", $editeurTag, $e_pattern) );
		      	$to_pattern = str_replace("@1", $smartyTag, $smarty_pattern);
	        } else {
	           $from_pattern = preg_quote( str_replace("@1", $smartyTag, $smarty_pattern) );
	           $to_pattern = str_replace("@1", $editeurTag, $e_pattern);
	        }
	        $from_pattern = str_replace("%", "(.*?)", $from_pattern);
	        $to_pattern = str_replace("%", "(.*?)", $to_pattern);
	        $contents = preg_replace('@'.$from_pattern.'@s', $to_pattern, $contents);
      	}
      }
      if ( $toSmarty ){
        $contents = $DEFAULT_HTML_BEFORE.$contents.$DEFAULT_HTML_AFTER;
      } else {
        $contents = preg_replace("@^.+<body[^>]*?>@s", "", $contents);
        $contents = preg_replace("@</body>.+$@s", "", $contents);
        $contents = preg_replace("@(\{\\$\w+\})@s", "", $contents);
      }
      return $contents;
    }
    
    /**
     * Returns ui layout models (print models) formatted for use with a combobox.
     *
     * @Route("/combobox/layoutmodels", name="carmen_ws_mapui_combobox_layoutmodels", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getMapUiLayoutModelsForCombo(Request $request)
    {
        $user = $this->getUser();
        $user instanceof Users;
        $account = $user->getAccount();
        $account instanceof Account;
        $dqlQuery = $this->getQueryBuilderList('CarmenApiBundle:AccountModel', 'model', null, array("account"=>$account->getAccountId()));
        $models = $dqlQuery->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $tabModels = array();
        foreach($models as $model){
        	
        	$model["default"] = (bool)preg_match('!'.implode('|', self::$DEFAULTS_PRINT).'!', $model["accountModelFile"]);
            array_push($tabModels, $model);
        }
        return new JsonResponse( $tabModels );
//         $models = $this->getRepository('CarmenApiBundle:AccountModel')->findAll(array("account"=>$account->getAccountId()));
//         $models = array();
//         $dir = $this->getAccountDirectory(); // get the AccountDir directory
//         if( $dir ) {
//             $modelsDir = $dir.self::ROOT_PRINTMODELS;
//             foreach (glob($modelsDir.'*.tpl') as $filename) {
//                 $filename = basename($filename);
//                 $models[] = array(
//                   'id'=>$filename, 
//                   'value'=>str_replace('.tpl', '', $filename), 
//                   'default'=>(bool)preg_match('!'.implode('|', self::$DEFAULTS_PRINT).'!', $filename)
//                 );
//             }
//         }
    
//         return new JsonResponse($models);
    }
    
    /**
     * Returns a layout (print model) file .
     *
     * @Route("/layoutfile/{layoutfile}", name="carmen_ws_mapui_get_layoutfile", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getMapUiPrintModelFile(Request $request, $layoutfile)
    {
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $layoutfile = $dir.self::ROOT_PRINTMODELS.$layoutfile;
            if( file_exists($layoutfile) ) {
                $contents = file_get_contents($layoutfile);
                return new Response($this->translatePrintModelFile($contents, false));
            } else {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "File not found: ".basename($layoutfile));
            }
        } else {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Account directory not found");
        }
    }
    
    
    
    
    /**
     * Creates or updates a layout (print model) file.
     *
     * @Route("/layoutfile/{layoutfile}", name="carmen_ws_mapui_post_layoutfile", options={"expose"=true}, methods={"POST"})
     *
     * @return JsonResponse Json array.
     */
    public function postMapUiPrintModelFile(Request $request, $layoutfile)
    {
        $user = $this->getUser();
        $user instanceof Users;
        $account = $user->getAccount();
        $account instanceof Account;
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        $oldtemplate = false;
        if( $dir ) {

            $content = $request->request->get('content', null);
            $template = $request->request->get('template', null);            
            
            $old_account_file = $request->request->get('old_file', null);
            if(!is_null($old_account_file)) {
                
                $accountModel = $this->getRepository('CarmenApiBundle:AccountModel')->findOneBy(array("account"=>$account->getAccountId(), "accountModelFile" => $old_account_file));
                $newModelName = $request->request->get('new_model_name', null);
                $oldtemplate = $old_account_file; 
                
                if(strpos($newModelName,".tpl")) {
                    $layoutfile = $newModelName; 
                } else {
                    $layoutfile = $newModelName."tpl"; 
                }
                
                
            } else {
                $accountModel = $this->getRepository('CarmenApiBundle:AccountModel')->findOneBy(array("account"=>$account->getAccountId(), "accountModelFile" => $layoutfile));
            }
            
            if ( !$accountModel ){
                $accountModel = new AccountModel();
                $accountModel->setAccount($account);                    
            }
            
            
            
            
            $accountModel instanceof AccountModel;
            $accountModel->setAccountModelFile($layoutfile);
            $accountModel->setAccountModelName(pathinfo($layoutfile, PATHINFO_FILENAME));
            
            $logo = $request->request->get('logo', null);
            $accountModel->setAccountModelLogo($logo); 
            
            
            $layoutfilepath = $dir.self::ROOT_PRINTMODELS.$layoutfile;
    

            
            
            $create_mode = false;
            
            $update_mode = false ; 
            
            if( !$content && $template ) {
                $create_mode = true;
                $template = $dir.self::ROOT_PRINTMODELS.$template;
                if ( file_exists($template) ){
                    $content = file_get_contents($template);
                }
            } else if($oldtemplate) {
                $oldtemplate = $dir.self::ROOT_PRINTMODELS.$oldtemplate;

                $update_mode = true; 
                if ( file_exists($oldtemplate) ){
                    $content = file_get_contents($oldtemplate);
                }
            }
            if( !$content ) {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Parameter 'content' must be set");
            }
    
            $content = $this->translatePrintModelFile($content, true);
            if( false === file_put_contents($layoutfilepath, $content) ) {
                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Unable to write file: ".basename($layoutfile));
            }
            
            $em = $this->getManager();
            $em->persist($accountModel);
            $em->flush($accountModel);
            
            if ( $create_mode || $update_mode){
                
                $data = json_decode($this->jsonSerializeEntity($accountModel), true);
                $data["default"] = (bool)preg_match('!'.implode('|', self::$DEFAULTS_PRINT).'!', $layoutfile);
                return new JsonResponse($data); 
                
            }
            return $this->getMapUiPrintModelFile($request, $layoutfile);
        } else {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Account directory not found");
        }
    }
    
    /**
     * Removes a layout (print model) files.
     *
     * @Route("/layoutfile/{layoutfile}", name="carmen_ws_mapui_delete_layoutfile", options={"expose"=true}, methods={"DELETE"})
     *
     * @return JsonResponse Json array.
     */
    public function deleteMapUiPrintModelFile(Request $request, $layoutfile)
    {
        $user = $this->getUser();
        $user instanceof Users;
        $account = $user->getAccount();
        $account instanceof Account;
        $dir = $this->getAccountDirectory(); // get the AccountDir directory
        if( $dir ) {
            $em = $this->getManager();
        	  $parameters = array(
            		"account"=>$account->getAccountId(), 
            		"accountModelFile" => $layoutfile
            );
            $fromAccountModel = " from CarmenApiBundle:AccountModel accountModel where accountModel.account=:account and accountModel.accountModelFile=:accountModelFile";
            
            try {
	            $em->beginTransaction();
	            $em->createQuery("delete from CarmenApiBundle:UiModel uimodel where uimodel.accountModel in (select	accountModel ".$fromAccountModel.")")->execute($parameters);
	            $em->createQuery("delete ".$fromAccountModel)->execute($parameters);
	          
	            $layoutfile = $dir.self::ROOT_PRINTMODELS.$layoutfile;
	            if ( !file_exists($layoutfile) ) return new Response();
	            if( false === @unlink($layoutfile) ) {
	            	  $em->rollback();
	                return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Unable to delete file: ".basename($layoutfile));
	            }
	            $em->commit();
            } catch(\Exception $exception){
	            $em->rollback();
            	return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Unable to delete file: ".basename($layoutfile));
            }
            return new Response();
        } else {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), "Account directory not found");
        }
    }
    
    /**
     * Returns fonts formatted for use with a combobox.
     *
     * @Route("/keymap/library", name="carmen_ws_mapui_keymaplibrary", options={"expose"=true}, methods={"GET", "POST"})
     *
     * @return JsonResponse Json array.
     */
    public function getKeymapLibrary(Request $request)
    {
        $normalizeSimpleXML = function ($obj) use(&$normalizeSimpleXML) {
            if (is_object($obj)) {
                $obj = get_object_vars($obj);
            }
            if (is_array($obj)) {
                $result = array();
                foreach($obj as $key=>$value){ $result[$key] = $normalizeSimpleXML($value); }
            } else {
                $result = $obj;
            }
            return $result;
        };
        $keymaps = array();
        $mapfileDir = $this->getMapfileDirectory();
    	  $storeDir = $mapfileDir."/reference/store";
    	  @mkdir($storeDir, 0770, true);
        $files = array_merge(glob($storeDir.'/*.xml'), glob($storeDir.'/*.XML'));
        foreach ($files as $file){
        	$xml = simplexml_load_file($file);
        	$children = $xml->children();
        	$filename = null;
        	foreach ($xml->children() as $node){
        		if ( strtolower($node->getName())=="filename" ){
        			$filename = $node;
        			break;
        		}
        	}
        	if ( $filename===null ) continue;
        	
        	$image = $storeDir.'/'.$filename->__toString();
        	if ( !file_exists($image) ) continue;
          $extension = pathinfo($image, PATHINFO_EXTENSION);
        	        	
        	$json = $normalizeSimpleXML($xml);
        	
        	$json["image"] = str_replace($mapfileDir, ".", $image);
        	
          $data = file_get_contents($image);
          $json["base64image"] = 'data:image/' . $extension . ';base64,'. base64_encode($data);
          
          $json["modifiedDate"] = date("d/m/Y H:i:s", filemtime($image));
          
        	$keymaps[] = $json;
        }

        return new JsonResponse($keymaps);
    }
    
    /**
     * Generates and returns a reference map
     *
     * @Route("/keymap/library/{id}/{action}/{filename}", 
     * name="carmen_ws_mapui_keymaplibraryaction", 
     * options={"expose"=true}, 
     * requirements={"id"="\d+", "filename"="[-_%.a-zA-Z0-9]+", "action"="select|update|delete"},
     * methods={"GET", "POST"})
     *
     * @return JsonResponse Json array.
     */
    public function keymapLibraryAction(Request $request, $id, $action, $filename)
    {
        set_time_limit(-1);
        
        $mapfileDir = $this->getMapfileDirectory();
    	  $storeDir = $mapfileDir."/reference/store";
    	  @mkdir($storeDir, 0770, true);
    	  $storeImage = $storeDir."/".$filename;
        $extension = pathinfo($storeImage, PATHINFO_EXTENSION);
        $storeXml = str_replace(".".$extension, ".xml", $storeImage);
        try {
            if ( !file_exists($storeImage) ){
                throw $this->createNotFoundException();
            }
            if ( !file_exists($storeXml) ){
                throw $this->createNotFoundException();
            }
            
            $map = $this->getRepository(MapController::MAP_ENTITY)->find($id);
            $map instanceof Map;
            if( !$map ) {
                throw $this->createNotFoundException();
            }
            
            $editMap = $map;
            $publishMap = $this->getRepository(MapController::MAP_ENTITY)->find($map->getPublishedMap()->getMapId());
            $mapfile = $this->ensureEditingMapfile($editMap, $publishMap);
            
            
            switch($action){
            	case "select" :
                $mapfile = pathinfo($mapfile, PATHINFO_FILENAME);
		            $referenceImage = $mapfileDir."/reference/reference_".$mapfile.".".$extension;
		            
		            if ( @copy($storeImage, $referenceImage) ){
		            	$function = "imagecreatefrom".strtolower($extension);
		            	$oImage = $function($referenceImage);
		            	
				          $data = file_get_contents($referenceImage);
				          $base64image = 'data:image/' . $extension . ';base64,'. base64_encode($data); 
		            	$xml = simplexml_load_file($storeXml);
		            	$array = get_object_vars($xml);
		            	$extent = $array["extent"];
		            	$extent = get_object_vars($extent);
		            	$result = array(
		            		"extent" => array_values($extent),
		            		"data" => $base64image,
		            		"base64image" => $base64image,
		            		"image" => str_replace($mapfileDir, ".", $referenceImage),
		            		"width" => imagesx($oImage),
		            		"height" => imagesy($oImage),
		            	);
		            	return $this->formatJsonSuccess($result);
		            } else {
		            	return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, "Unable to copy image from keymap library {$storeImage}");
		            }
            	break;
            	
            	case "update" :
            		$ref_mapfile = str_replace("temp_admin_", "temp_reference_", $mapfile);
            		$result = $this->generateReferenceMap($request, $id, $ref_mapfile);
            		$result = $result->getContent();
            		$result = json_decode($result, true);
            		
            		array_map("unlink", array_merge(glob($mapfileDir."/*".$ref_mapfile."*"), glob($mapfileDir."/reference/*".$ref_mapfile."*")));
        		    
        		    return $this->getKeymapLibrary($request);
            	break;
            	
            	case "delete" :
        		    @unlink($storeImage);
        		    @unlink($storeXml);
        		    return $this->getKeymapLibrary($request);
            	break;
            }
            
        } catch (\Exception $e) {
            $this->getLogger()->error(__METHOD__."::".$e->getMessage());
            
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
        
    }
    
    /**
     * Generates and returns a reference map
     *
     * @Route("/generate/referencemap/{id}", name="carmen_ws_mapui_generate_refmap", options={"expose"=true}, methods={"GET","POST"})
     *
     * @return JsonResponse Json array.
     */
    public function generateReferenceMap(Request $request, $id, $rename=null)
    {
        set_time_limit(-1);
        try {
            $map = $this->getRepository(MapController::MAP_ENTITY)->find($id);
            $map instanceof Map;
            if( !$map ) {
                throw $this->createNotFoundException();
            }
            $mapfileDir = $this->getMapfileDirectory();
            $paramsGET = array(
                "directory" => $mapfileDir,
                "extent"    => $request->get('extent', $map->getMapExtentXmin().' '.$map->getMapExtentYmin().' '.$map->getMapExtentXmax().' '.$map->getMapExtentYmin()),
                "size"      => $request->get('size', null),
                "hiddens"    => $request->get('hiddens', array()),
            );
            $editMap = $map;
            $publishMap = $this->getRepository(MapController::MAP_ENTITY)->find($map->getPublishedMap()->getMapId());
            $mapfile = $this->ensureEditingMapfile($editMap, $publishMap);
            
            if( $rename!==null ){
            	copy($mapfileDir."/".$mapfile.".map", $mapfileDir."/".$rename.".map");
            	$mapfile = $rename;
            }
            
            $result = $this->callMapserverApi('/api/map/'.$mapfile.'/reference/generate', 'GET', $paramsGET, array(), true, array(
                CURLOPT_CONNECTTIMEOUT => 0,
                CURLOPT_TIMEOUT        => 0
            ));
            
            $libraryParams    = $request->get('libraryParams', array());
            
            if ( !empty($libraryParams) && isset($result["image"]) ) {
			        $mapfileDir = $this->getMapfileDirectory();
			    	  $storeDir = $mapfileDir."/reference/store";
    	        @mkdir($storeDir, 0770, true);
			    	  
            	$referenceImage = $mapfileDir."/".$result["image"];
            	$extension = pathinfo($referenceImage, PATHINFO_EXTENSION);
            	$f = $storeDir."/".$libraryParams["filename"];
            	if ( $tmp = pathinfo($f, PATHINFO_FILENAME) ){
            		$libraryParams["filename"] = $tmp;
            	}
            	$storeImage = $storeDir."/".pathinfo($libraryParams["filename"], PATHINFO_FILENAME).".".$extension;
            	
            	if ( @copy($referenceImage, $storeImage)!==false ){
            		$xmlfile = str_replace($extension, "xml", $storeImage);
            		$filename = basename($storeImage);
            		$xml = array(
            			'<?xml version="1.0" encoding="UTF-8"?>', 
            			"\n",
            			"<reference>", 
            				"<filename>{$filename}</filename>",
            				"<description>{$libraryParams["description"]}</description>",
            				"<extent>",
            					"<minx>{$result["extent"][0]}</minx>",
            					"<miny>{$result["extent"][1]}</miny>",
            					"<maxx>{$result["extent"][2]}</maxx>",
            					"<maxy>{$result["extent"][3]}</maxy>",
            				"</extent>",
            			"</reference>",
            		);
            		file_put_contents($xmlfile, implode($xml));
            	}
            }
            return $this->formatJsonSuccess($result);
        } catch (\Exception $e) {
            $this->getLogger()->error(__METHOD__."::".$e->getMessage());
            
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    /**
     * Restfull api to serve Entity.
     *
     * @Route("/rest/{id}", name="carmen_ws_mapui", defaults={"id"=null}, options={"expose"=true}, methods={"GET","POST","PUT","DELETE"})
     * 
     * @param Request $request
     * @param unknown $entity
     * @param unknown $id
     */
    public function mapuiRestAction(Request $request,  $id = null)
    {
        switch ($request->getMethod()) {
            case 'GET':
                return $this->getMapUiAction($request, $id);
                break;
            case 'POST':
            case 'PUT':
                return $this->postMapUiAction($request, $id);
                break;
            case 'DELETE':
                return $this->deleteMapUiAction($id);
                break;
        }

        return $this->formatJsonError(Response::HTTP_METHOD_NOT_ALLOWED, $this->_t("Method not allowed"));
    }

    /**
     * Returns a list of MapUi.
     * 
     * @param Request $request The request object.
     * @param string  $id      If set, returns a list with the requested Map only (if exists).
     * 
     * #Route("/get/{id}", name="carmen_ws_mapui_get", requirements={"id"="\d+"}, defaults={"id"=null}, options={"expose"=true})
     * #Method({"GET"})
     * 
     * @return JsonResponse Json array.
     */
    public function getMapUiAction(Request $request, $id = null)
    {
        try {
            $qb = $this->getQueryBuilderList(self::MAPUI_ENTITY, 'e', $id)
                       ->leftJoin('e.uiModels', 'uiModels')->addSelect('uiModels')
            ;
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception"), $this->isProd() ? array() : array('exception'=>$ex->getMessage()));
        }

        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * Creates or updates a MapUi Entity.
     * 
     * #Route("/edit/{id}", name="carmen_ws_mapui_edit", requirements={"id"="\d+"}, options={"expose"=true})
     * #Method({"GET"})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function postMapUiAction(Request $request, $id)
    {
        VarDumper::dump($request->request->all());
        VarDumper::dump($request->files->all());
        return new JsonResponse();
    }

    /**
     * Deletes a MapUi Entity.
     *
     * #Route("/delete/{id}", name="carmen_ws_mapui_delete", options={"expose"=true})
     * #Method({"DELETE"})
     *
     * @param string  $id If set, updates an existing user, else creates it.
     *
     * @return Response Empty response if success, JsonResponse error if failed.
     */
    public function deleteMapUiAction($id)
    {
        try {
            $entity = $this->getRepository(self::MAPUI_ENTITY)->find($id);
            if ( !$entity ) {return new Response(null, Response::HTTP_NO_CONTENT);
            $ex = new EntityNotFoundException();
            return $this->formatJsonError(Response::HTTP_NOT_FOUND, $this->_t("Not Found"), $this->_t($ex->getMessage()));
            }
            $this->getManager()->remove($entity);
            $this->getManager()->flush();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception")."[".__METHOD__."]", $this->isProd() ? array('exception'=>$ex->getMessage()) : array('exception'=>$ex->getMessage()));
        }
        
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
