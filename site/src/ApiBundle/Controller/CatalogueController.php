<?php

namespace Carmen\ApiBundle\Controller;

use Carmen\ApiBundle\Entity\Map;
use Doctrine\ORM\Query\Expr;
use Prodige\ProdigeBundle\Controller\BaseController as ProdigeBaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Catalogue controller
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/services/catalogue")
 */
class CatalogueController extends BaseController
{
    /**
     * @Route("/{accountId}", name="carmen_ws_catalogue")
     */
    public function indexAction(Request $request, $accountId)
    {
        $request->query->set('accountId', $accountId);
        $account = $this->getDoctrine()->getRepository('CarmenApiBundle:Account')->find($accountId);
        
        // keywords
        $categoryId = $request->request->get('collections', $request->query->get('collections', -1));
        $keywordId  = $request->request->get('concepts', $request->query->get('concepts', -1));
        $categories = $this->getDoctrine()->getRepository('CarmenApiBundle:LexCategoryKeyword')->findAll();
        $keywords   = $categoryId > 0
                      ? $this->getDoctrine()->getRepository('CarmenApiBundle:Keyword')->findBy(array('category'=>$categoryId))
                      : $this->getDoctrine()->getRepository('CarmenApiBundle:Keyword')->findAll() ;
        
        // list of published maps
        $qb = $this->getDoctrine()->getRepository('CarmenApiBundle:Map')
            ->createQueryBuilder('m')
            ->leftJoin('m.layers', 'l')->addSelect('l')
            ->leftJoin('l.fields', 'f')->addSelect('f')
            ->leftJoin('m.mapUi', 'ui')->addSelect('ui')
            ->leftJoin('m.mapProjectionEpsg', 'epsg')->addSelect('epsg')
            ->andWhere('m.account = :account')->setParameter('account', $account)
            //->andWhere('m.mapDatepublication is not null')
            ->andWhere('m.mapCatalogable = true')
            ->andWhere('m.publishedMap is null');
        if( $categoryId > 0 && $keywordId == -1 ) {
            $qb->innerJoin('m.keywords', 'k', Expr\Join::WITH, 'k.category IN (:categories)')/*->addSelect('k')*/->setParameter('categories', array($categoryId));
        } else if ( $keywordId > 0 ) {
            $qb->innerJoin('m.keywords', 'k', Expr\Join::WITH, 'k.keywordId IN (:keywords)')/*->addSelect('k')*/->setParameter('keywords', array($keywordId));
        }
        $maps = $qb->getQuery()->getResult();
        
        // all maps count, published or unpublished
        $allMaps = $this->getDoctrine()->getManager()
                   ->createQuery('select count(m) from CarmenApiBundle:Map m where m.account = :account and m.publishedMap is null')
                   ->setParameter('account', $account)
                   ->getSingleScalarResult();
                
        // published maps count
        $pubMaps = $this->getDoctrine()->getManager()
        ->createQuery('select count(m) from CarmenApiBundle:Map m where m.account = :account and m.publishedMap is null and m.mapCatalogable = true')
        ->setParameter('account', $account)
        ->getSingleScalarResult();
        // published maps with wms layers count
        $wmsMaps = 0;
        // published maps with wfs layers count
        $wfsMaps = 0;
        
        $paramsGET = array(
            "directory" => $this->getMapfileDirectory()
        );
        
        // compute stats, and 
        // get mapserver info for each map/layer
        $mapserver = array();
        $mapfileInfo = array();
        foreach ($maps as $map) {
            $map instanceof Map;
            
            $wxsTitle = $map->getMapTitle();
            
            //set name as in mapserv API(Helpers, treatWxsMap)
            $wxsName = $this->wd_remove_accents($wxsTitle);
            $wxsName = ereg_replace("[^a-zA-Z0-9]", "_", $wxsName);
            
            
            $mapfileInfo['map'][$map->getMapId()]['info']["name"] = $wxsName;
            /*
            try {
                $result = $this->callMapserverApi('/api/map/'.$map->getMapFile(), 'GET', $paramsGET);
                $mapserver['map'][$map->getMapId()]['info'] = $result;
                //var_dump($mapserver['map'][$map->getMapId()]['info']);
            } catch (ApiException $e) {
                $this->getLogger()->error('Catalogue : ' . $e->getMessage(), $e->getData());
            }*/
            
            $wms = false;
            $wfs = false;
            /*
            foreach($map->getLayers() as $layer) {
                $mapfileInfo['map'][$map->getMapId()]['layers'][$layer->getLayerId()] = array();
                $layer instanceof Layer;
                $wms |= $layer->getLayerWms();
                $wfs |= $layer->getLayerWfs();
                //var_dump($layer);
                if(isset($mapserver['map'][$map->getMapId()]['info']["layers"])){
                    foreach($mapserver['map'][$map->getMapId()]['info']["layers"] as $indice => $msLayer){
                        if($msLayer["name"] == $layer->getLayerMsname()){
                            $mapfileInfo['map'][$map->getMapId()]['layers'][$layer->getLayerId()]["geoide_wms_srs"] =
                             (isset($msLayer["metadata"]["geoide_wms_srs"]) && $msLayer["metadata"]["geoide_wms_srs"]!="" ?  $msLayer["metadata"]["geoide_wms_srs"] :
                             (isset($mapserver['map'][$map->getMapId()]['info']["metadata"]["geoide_wms_srs"])? $mapserver['map'][$map->getMapId()]['info']["metadata"]["geoide_wms_srs"] : "") );
                            break;
                        }
                        
                    }
                }
                //$mapserver['map'][$map->getMapId()]['layers'][$layer->getLayerId()] = false;
                /*try {
                    $result = $this->callMapserverApi('/api/map/'.$map->getMapFile().'/layer/'.$layer->getLayerName(), 'GET', $paramsGET);
                    $mapserver['map'][$map->getMapId()]['layers'][$layer->getLayerId()] = $result;
                } catch (ApiException $e) {
                    $this->getLogger()->error('Catalogue : ' . $e->getMessage(), $e->getData());
                }*/
            //}
            
            //var_dump($map);
            // published maps with wms layers count
            $wmsMaps += $wms ? 1 : 0;
            // published maps with wfs layers count
            $wfsMaps += $wfs ? 1 : 0;
        }
        
        //print_r($mapserver);
        
        return $this->render('WebBundle/Catalogue/index.html.twig', array(
            'categoryId' => $categoryId,
            'categories' => $categories,
            'keywordId'  => $keywordId,
            'keywords'   => $keywords,
            'categoryId' => $categoryId,
            'accountId'  => $accountId,
            'account'    => $account,
            'allMaps'    => $allMaps,
            'pubMaps'    => $pubMaps,
            'wmsMaps'    => $wmsMaps,
            'wfsMaps'    => $wfsMaps,
            'maps'       => $maps,
            'config'     => $this->getParameter('carmen_config'),
            'mapserver'  => $mapfileInfo,
        ));
    }
    
    /**
     * remove accents from string
     * @param unknown $str
     * @param string $charset
     * @return mixed
     */
    protected function wd_remove_accents($str, $charset='utf-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    
        return $str;
    }

     /**
     * @Route("/getOGC/{type}", name="carmen_ws_get_ogc", options={"expose"=true})
     *
     * @param Request $request
     *            The request object.
     *
     * @return JsonResponse Json object.
     */
    public function getOgcAction(Request $request, $type)
    {
        if (! (in_array($type, array(
            "WMS",
            "WFS",
            "WMTS",
            "WMSC",
            "SOS"
        )))) {
            throw new \Exception("invalid request");
        }
        $conn = $this->getConnection(ProdigeBaseController::CONNECTION_PRODIGE);
        $query = "SELECT wxs_name as \"NOM\", wxs_url as \"URL\" from carmen.wxs inner join carmen.lex_wxs_type on lex_wxs_type.wxs_type_id = wxs.wxs_type_id where wxs_type_name=:wxs_type_name";
        $data = $conn->fetchAll($query, array(
            "wxs_type_name" => $type
        ));
        return new JsonResponse($data);
    }

}
