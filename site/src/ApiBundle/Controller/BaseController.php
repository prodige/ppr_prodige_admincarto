<?php

namespace Carmen\ApiBundle\Controller;

use Carmen\ApiBundle\Entity\Layer;
use Carmen\ApiBundle\Entity\LexLayerType;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\Users;
use Carmen\ApiBundle\Exception\ApiException;
use Carmen\ApiBundle\Services\CarmenConfig;
use Carmen\CswBundle\Csw\XmlISO19139;
use Carmen\OwsContextBundle\Controller\OwsContextController;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;
use JMS\Serializer\SerializationContext;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Base Controller that provides common helper functions.
 *
 * @author alkante <support@alkante.com>
 */
class BaseController extends AbstractController
{

    const CONNECTION_CATALOGUE = "catalogue";
    const CONNECTION_PRODIGE = "prodige";

    /** Keep trac of the last mapserver api call error */
    private static $mapserverApiLastErr;

    /** The original mapfiles directory. Depends on the connected user according to his ACCOUNT */
    private static $accountDirectory;

    /** The edited mapfiles directory. Depends on the connected user according to his ACCOUNT and his ID */
    private static $mapfileDirectory;

    /**
     * Returns the kernel env variable (debug/dev/prod).
     *
     * @return string Kernel environment.
     */
    protected function getEnv()
    {
        return $this->container->get('kernel')->getEnvironment();
    }

    /**
     * Returns true if app is in prod env.
     *
     * @return boolean True if app is in prod env, false otherwise.
     */
    protected function isProd()
    {
        return $this->getEnv() == 'prod';
    }

    /**
     * Returns the default logger.
     *
     * @return \Psr\Log\LoggerInterface The default logger
     */
    public function getLogger()
    {
        return $this->get('logger');
    }

    /**
     * Translate text.
     *
     * @param string $text Text to translate.
     * @param array $params Placeholder parameters (substitution).
     *
     * @return string Translated text
     * @see http://symfony.com/fr/doc/current/book/translation.html
     *
     * @example <code>$this->_t('Hello %name%', array('%name%'=>'World'))</code>
     */
    protected function _t($text, $params = array())
    {
        return $this->get('translator')->trans($text, $params);
    }

    /**
     * Returns the ID of connected user
     * @return integer
     */
    protected function getUserId()
    {
        return $this->getUser()->getUserId();
    }

    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Gets the repository for an entity class.
     *
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     *
     * @return \Doctrine\ORM\EntityRepository The repository class.
     */
    protected function getRepository($entityName)
    {
        return $this->getManager()->getRepository($entityName);
    }

    /**
     * Creates a new QueryBuilder instance that is prepopulated for this entity name.
     *
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     *
     * @param string $alias .
     *
     * @return \Doctrine\ORM\QueryBuilder
     *
     */
    protected function createQueryBuilder($entityName, $alias)
    {
        return $this->getRepository($entityName)->createQueryBuilder($alias);
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection($connection_name = null, $schema = "carmen")
    {
        $connection = $this->getDoctrine()->getConnection();
        $connection->exec('set search_path to ' . $schema);
        return $connection;
    }

    /**
     * Returns a configured CswClient
     *
     * @return \Carmen\CswBundle\Csw\CswClient
     */
    protected function getCswClient()
    {
        return $this->get('carmen.csw')->getCswClient($this->getGeosourceUrl() . "/fre");
    }

    /**
     * Get the Geosource URL of the current user's account
     *
     * @throws \Exception if user or its account is not available
     */
    protected function getGeosourceUrl()
    {
        $user = $this->getUser();
        if (!$user) throw new \Exception('No user authenticated');

        $user instanceof Users;
        $account = $user->getAccount();
        if (!$account) throw new \Exception('User has no account');

        return $account->getAccountGeonetworkurl();
    }

    /**
     * Returns a QueryBuilder pre-configured to list data from an Entity.
     *
     * Filters might be in the following form:
     * <pre>
     * filter[adherentNom]=toto                # default 'key = value' where clause
     * filter[adherentDns.dnsUrl][value]=test  # complex example with custom join and operator
     * filter[adherentDns.dnsUrl][op]=like     # optional [like|in], defaults to '=' if not set
     * filter[adherentDns.dnsUrl][join]=inner  # optional [left|inner], defaults to 'left'
     * filter[adherentPath][value]=root,base   # 'in' filter example, multiple values are comma separated
     * filter[adherentPath][op]=in             # optional [like|in], defaults to '=' if not set
     * </pre>
     *
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     * @param string $alias Alias.
     * @param string $id If set, returns a list with the one requested Entity only (if exists).
     * @param array $filters An array of query filters.
     * @param boolean $debug If true, prints out SQL query and parameters
     *
     * @return \Doctrine\ORM\QueryBuilder
     *
     * @throws MappingException If the class has a composite primary key.
     * @example <code>BaseController::getQueryBuilderList('BundleName:EntityName', 'e', 1)</code>
     *
     */
    protected function getQueryBuilderList($entityName, $alias, $id = null, $filters = array(), $sorters = array(), $debug = false)
    {
        // get a new query builder for this entity, using the given root alias
        $qb = $this->createQueryBuilder($entityName, $alias);

        if (null != $id) {
            // retreive the entity single identifier (primary key)
            $meta = $this->getManager()->getClassMetadata($this->getRepository($entityName)->getClassName());
            $identifier = $meta->getSingleIdentifierFieldName(); // throws MappingException
            // add where clause on the entity identifier
            $qb->andWhere($qb->getRootAlias() . '.' . $identifier . ' = :' . $identifier)
                ->setParameter($identifier, $id);
        }
        $qb = $this->applyFiltersToQueryBuilder($qb, $filters, $sorters);

        if ($debug) {
            echo $qb->getQuery()->getSQL();
        }

        return $qb;
    }

    /**
     * Apply filters to a QueryBuilder.
     *
     * Filters might be in the following form:
     * <pre>
     * filter[adherentNom]=toto                # default 'key = value' where clause
     * filter[adherentDns.dnsUrl][value]=test  # complex example with custom join and operator
     * filter[adherentDns.dnsUrl][op]=like     # optional [like|in], defaults to '=' if not set
     * filter[adherentDns.dnsUrl][join]=inner  # optional [left|inner], defaults to 'left'
     * filter[adherentPath][value]=root,base   # 'in' filter example, multiple values are comma separated
     * filter[adherentPath][op]=in             # optional [like|in], defaults to '=' if not set
     * </pre>
     *
     * @param QueryBuilder $qb A QueryBuilder instance.
     * @param array $filters An array of query filters.
     * @param array $sorters An array of orderBy conditions.
     *
     * @return QueryBuilder The QueryBuilder with applied filters
     */
    protected function applyFiltersToQueryBuilder(QueryBuilder $qb, $filters = array(), $sorters = array())
    {
        if (is_array($filters)) {
            foreach ($filters as $key => $value) {
                $falias = $qb->getRootAlias();
                $fkey = $key;
                // check if key has a join
                $hasJoin = false;
                $fragments = explode('.', $key);
                if (count($fragments) > 1) {
                    $hasJoin = true;
                    $falias = $fragments[0];
                    $fkey = $fragments[1];
                    if (!is_array($value)) {
                        $value = array('value' => $value);
                        $value['join'] = 'left';
                    }
                }
                if (is_array($value)) {
                    // a custom where query
                    if ($hasJoin) {
                        $join = isset($value['join']) && in_array($value['join'], array('left', 'inner')) ? $value['join'] : 'left';
                        switch ($join) {
                            case 'left':
                                $qb->leftJoin($qb->getRootAlias() . '.' . $falias, $falias)
                                    ->addSelect($falias);
                                break;
                            case 'inner':
                                $qb->innerJoin($qb->getRootAlias() . '.' . $falias, $falias)
                                    ->addSelect($falias);
                                break;
                        }
                    }
                    $op = isset($value['op']) && in_array($value['op'], array('like', 'in')) ? $value['op'] : '=';
                    $fvalue = isset($value['value']) ? $value['value'] : '';
                    $ffield = $falias . '.' . $fkey;
                    $fparam = ':' . $fkey;
                    switch ($op) {
                        case 'like':
                            $fvalue = '%' . $fvalue . '%';
                            break;
                        case 'in':
                            $fvalue = explode(',', $fvalue);
                            $fparam = '(:' . $fkey . ')';
                            break;
                    }
                    if ($value === null) {
                        $qb->andWhere($ffield . ' is null ');
                    } else {
                        $qb->andWhere($ffield . ' ' . $op . ' ' . $fparam . '')
                            ->setParameter($fkey, ($fvalue === null ? "NULL" : $fvalue), ($fvalue === null ? \PDO::PARAM_NULL : null));
                    }
                } else {
                    // a simple where query
                    if ($value === null) {
                        $qb->andWhere($falias . '.' . $fkey . ' is null');
                    } else {
                        $qb->andWhere($falias . '.' . $fkey . ' = :' . $fkey)
                            ->setParameter($fkey, ($value === null ? "NULL" : $value), ($value === null ? \PDO::PARAM_NULL : null));
                    }
                }
            }
        }

        if (is_array($sorters)) {
            foreach ($sorters as $field => $direction) {
                $qb->addOrderBy((strpos($field, ".") === false ? $qb->getRootAlias() . '.' . $field : $field), $direction);
            }
        }

        return $qb;
    }

    /**
     * Serialize an entity as a json string.
     *
     * @see http://jmsyst.com/libs/serializer/master/cookbook/exclusion_strategies
     *
     * @param Object $entity The entity to serialize
     * @param array $groups A list of serialization groups (see link above)
     *
     * @return string The entity formatted as a json string
     */
    protected function jsonSerializeEntity($entity, array $groups = null)
    {
        $serializer = $this->get('jms_serializer');
        $serializer instanceof \JMS\Serializer\Serializer;
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $context->enableMaxDepthChecks();
        if ($groups) {
            $context->setGroups($groups);
        }

        return $serializer->serialize($entity, 'json', $context);
    }

    /**
     * Performs a generic post on an entity. This is used to create or update an entity.
     *
     * Returns data in case of success, or throws an exception in case of error.
     *
     * The entity is persisted (created/update) based on submitted user data (POST only),
     * where values are provided as <code>formField=someValue</code>. If a form name is provided, user
     * submitted data is expected as <code>formName[formField]=someValue</code>.
     *
     * @param Request $request Request object
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     * @param string $id If not set, creates a new entity, otherwise updates it
     * @param string $formName Name of the form, null by default (has an impact on how to retreive submitted data from request!).
     *
     * @return array An array with the key 'id', identifier of the created/updated entity.
     *
     * @throws ApiException            If form validation failed (see key 'formErrors' for the complete list of form errors).
     * @throws EntityNotFoundException If $entityName entity with identifier $id could not be found.
     */
    protected function cloneEntity($oldEntity, $newEntity, $excludeFields = array())
    {
        //throw new \Exception("TODO change ->query (GET) by ->request (POST), because data should be 'posted' in this case");
        try {
            // get doctrine metadata info for this entity
            $metadata = $this->getManager()->getClassMetadata(get_class($oldEntity));

            //$form = $this->createFormBuilder($entity); // this creates a form the the name 'form' by default, so it expects params such as form[fieldName]=value
            foreach (array_diff(array_merge($metadata->getAssociationNames(), $metadata->getFieldNames()), $excludeFields) as $key) {
                if (!$metadata->isIdentifier($key)) {
                    if ($metadata->hasAssociation($key)) {
                        $mapping = $metadata->getAssociationMapping($key);
                        if ($mapping['type'] != ClassMetadataInfo::MANY_TO_ONE) continue;
                    }

                    $setter = "set" . ucfirst($key);
                    $getter = "get" . ucfirst($key);

                    if (method_exists($newEntity, $setter) && method_exists($oldEntity, $getter)) {
                        $this->getLogger()->info(__METHOD__, array($key, $getter, $setter, $oldEntity->$getter()));
                        $newEntity->$setter($oldEntity->$getter());
                    }
                }
            }
        } catch (\Exception $exception) {
            throw new ApiException($this->_t("Internal Server Error"), array(), Response::HTTP_INTERNAL_SERVER_ERROR, $exception);
        }
    }

    /**
     * Performs a generic post on an entity. This is used to create or update an entity.
     *
     * Returns data in case of success, or throws an exception in case of error.
     *
     * The entity is persisted (created/update) based on submitted user data (POST only),
     * where values are provided as <code>formField=someValue</code>. If a form name is provided, user
     * submitted data is expected as <code>formName[formField]=someValue</code>.
     *
     * @param Request $request Request object
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     * @param string $id If not set, creates a new entity, otherwise updates it
     * @param string $formName Name of the form, null by default (has an impact on how to retreive submitted data from request!).
     *
     * @return array An array with the key 'id', identifier of the created/updated entity.
     *
     * @throws ApiException            If form validation failed (see key 'formErrors' for the complete list of form errors).
     * @throws EntityNotFoundException If $entityName entity with identifier $id could not be found.
     */
    protected function _doGenericPost(Request $request, $entityName, $id = null, $formName = null, $bGetEntity = false)
    {
        //throw new \Exception("TODO change ->query (GET) by ->request (POST), because data should be 'posted' in this case");
        $request_save = $request->request->all();
        try {
            $className = $this->getRepository($entityName)->getClassName();
            $metadata = $this->getManager()->getClassMetadata($className);
            $entity = new $className();
        } catch (\Exception $exception) {
            throw new ApiException($this->_t("Internal Server Error"), array(), Response::HTTP_INTERNAL_SERVER_ERROR, $exception);
        }

        if (null != $id) {
            $entity = $this->getRepository($entityName)->find($id);
            if (!$entity) {
                $exception = new EntityNotFoundException();
                throw new ApiException($this->_t("Not Found"), array(), Response::HTTP_NOT_FOUND, $exception);
            }
        }

        // build a form dynamically based on submitted data
        $form = $this->buildDynamicFormFromRequest($request, $entity, $formName);

        // bind submitted data to the form/entity
        // @TODO change ->query (GET) by ->request (POST), because data should be 'posted' in this case
        $form->submit(null != $formName ? $request->request->get($form->getName()) : $request->request->all());

        // check if form is valid (constraints validation)
        if ($form->isValid()) {
            $request->request->replace($request_save);

            try {
                $this->getManager()->persist($entity);
                $this->getManager()->flush();
            } catch (\Exception $exception) {
                $this->getLogger()->error($exception->getMessage());
                throw new ApiException($this->_t("Internal Server Error") . " - " . $this->_t("SQL Exception"), array(), Response::HTTP_INTERNAL_SERVER_ERROR, $exception);
            }

            return $entity;
        } else {
            $request->request->replace($request_save);
            // form is not valid
            throw new ApiException($this->_t("Bad Request") . " - " . $this->_t("Form validation failed"), array('formErrors' => $this->getFormErrors($form)), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Performs a generic post on an entity. This is used to create or update an entity.
     *
     * Returns a JsonResponse.
     *
     * The entity is persisted (created/update) based on submitted user data (POST only),
     * where values are provided as <code>formField=someValue</code>. If a form name is provided, user
     * submitted data is expected as <code>formName[formField]=someValue</code>.
     *
     * @param Request $request Request object
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     * @param string $id If not set, creates a new entity, otherwise updates it
     * @param string $formName Name of the form, null by default (has an impact on how to retreive submitted data from request!).
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function doGenericPostResponse(Request $request, $entityName, $id = null, $formName = null)
    {
        try {
            $entity = $this->_doGenericPost($request, $entityName, $id, $formName);
        } catch (ApiException $exception) {
            $previous = $exception->getPrevious();
            return $this->formatJsonError($exception->getCode(), $exception->getMessage(), ($previous ? $previous->getMessage() : ""), $exception->getData());
        }
        return new Response($this->jsonSerializeEntity($entity), $id ? Response::HTTP_CREATED : Response::HTTP_OK);
    }

    /**
     * Performs a generic post on an entity. This is used to create or update an entity.
     *
     * Returns data in case of success, or throws an exception in case of error.
     *
     * The entity is persisted (created/update) based on submitted user data (POST only),
     * where values are provided as <code>formField=someValue</code>. If a form name is provided, user
     * submitted data is expected as <code>formName[formField]=someValue</code>.
     *
     * @param Request $request Request object
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     * @param string $id If not set, creates a new entity, otherwise updates it
     * @param string $formName Name of the form, null by default (has an impact on how to retreive submitted data from request!).
     *
     * @return array An array with the key 'id', identifier of the created/updated entity.
     *
     * @throws ApiException            If form validation failed (see key 'formErrors' for the complete list of form errors).
     * @throws EntityNotFoundException If $entityName entity with identifier $id could not be found.
     */
    protected function doGenericPost(Request $request, $entityName, $id = null, $formName = null, $bGetEntity = false)
    {
        $entity = $this->_doGenericPost($request, $entityName, $id, $formName);
        $data = json_decode($this->jsonSerializeEntity($entity), true);
        return ($bGetEntity ? $entity : $data);
    }

    /**
     * Returns a flattened array of form errors, indexed by form name and/or form fields names.
     *
     * @param Form $form The form.
     *
     * @return array Array of errors.
     */
    protected function getFormErrors(Form $form)
    {
        $errors = array();
        $iterator = new \RecursiveIteratorIterator($form->getErrors(true, false), \RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($iterator as $error) {
            $errors[$iterator->getForm()->getName()][] = $error->getMessage();
        }
        return $errors;
    }

    /**
     * Builds a form dynamically based on submitted data.
     *
     * WARNING: calling this function will remove any extra post data in $request->request that do not fit with $entity fields.
     *
     * @param Request $request Request object.
     * @param object $entity Entity instance.
     * @param string $formName Name of the form, null by default (has an impact on how to retreive submitted data from request!).
     *
     * @return Form The form.
     */
    protected function buildDynamicFormFromRequest(Request $request, $entity, $formName = null)
    {
        //throw new \Exception("TODO change ->query (GET) by ->request (POST), because data should be 'posted' in this case");
        // @TODO change ->query (GET) by ->request (POST), because data should be 'posted' in this case
        $fields = $request->request->all();

        // get doctrine metadata info for this entity
        $metadata = $this->getManager()->getClassMetadata(get_class($entity));

        if (is_null($formName)) {
            $formName = (new \ReflectionClass($entity))->getShortName();
        }

        //$form = $this->createFormBuilder($entity); // this creates a form the the name 'form' by default, so it expects params such as form[fieldName]=value
        $form = $this->container->get('form.factory')->createNamedBuilder($formName, 'Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType', $entity); // form with no name (ie. empty string "")
        foreach (array_keys($fields) as $key) {
            if ($metadata->hasField($key)) {
                $form->add($key);
            } elseif ($metadata->hasAssociation($key)) {
                $mapping = $metadata->getAssociationMapping($key);
                if ($mapping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                    $form->add($key, 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => $mapping['targetEntity'], 'required' => false, 'multiple' => true));
                } else {
                    // other king of relation, just add the field
                    $form->add($key);
                }
            } else {
                // this key is not a field nor an association of this entity
                // remove this key from the request, or form validation shall not pass
                $request->request->remove($key);
            }
        }
        $form = $form->getForm();

        return $form;
    }

    /**
     * Build a standard Json response error message.
     *
     * Json response is a json object with the following format: {status:<status>, error:<message>, description:<desc>, ...}
     *
     * @param number $status HTTP status of the response.
     * @param string $error Error message.
     * @param string $message Error detailled description.
     * @param array $extra Adds extra information to the response.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse The formatted Json response
     */
    protected function formatJsonError($status = 500, $error = "Internal Server Error", $description = "", array $extra = array())
    {
        $data = array_merge(array('status' => $status, 'error' => $error, 'description' => $description), $extra);
        return new JsonResponse($data, $status);
    }

    /**
     * Build a standard Json response success message.
     *
     * Json response is a json object with the following format: {status:<status>, ...}
     *
     * @param number $status HTTP status of the response.
     * @param array $extra Adds extra information to the response.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse The formatted Json response
     */
    protected function formatJsonSuccess(array $extra = array(), $status = 200)
    {
        $data = array_merge(array('status' => $status), $extra);
        return new JsonResponse($data, $status);
    }

    /**
     * Returns the URL of a Mapserver API method. Uses the configured Mapserver API base URL and concats the $requestUri.
     *
     * @param string $requestUri The requested method of the API (should start with a /).
     *
     * @return string The URL.
     * @example <code>getMapserverApiUrl('/api/map') returns something like 'http://mapserverapi.example.com/api/map'</code>
     *
     */
    protected function getMapserverApiUrl($requestUri)
    {
        return $this->getCarmenConfig()->getMapserverApiUrl() . $requestUri;
    }

    /**
     * Get Carmen Config after applying translations
     * @return CarmenConfig
     */
    protected function getCarmenConfig()
    {
        return $this->container->get('carmen_config');
    }

    /**
     * Initialize the mapfiles directories for the connected user and returns the original mapfiles directory.
     * @return string
     */
    protected function getAccountDirectory()
    {
        return $this->getCarmenConfig()->getMapserverAccountDir();
    }

    /**
     * Initialize the mapfiles directories for the connected user and returns the edited mapfiles directory.
     * @return string
     */
    public function getMapfileDirectory()
    {
        return $this->getCarmenConfig()->getMapserverMapfileDir();
    }

    /**
     * Ensure that the edited mapfile is created for the connected user.
     * Copy the original mapfile if exists or else the template mapfile
     * @param boolean $is_model True if the map file is a model file
     * @param string $mapfile
     * @param string $map_id
     */
    protected function ensureEditingMapfile(Map $editMap, Map $publishMap = null)
    {
        $user = $this->getUser();
        if (!$user) return null;
        $user instanceof Users;

        $directory = $this->getMapfileDirectory();
        exec("cp " . realpath(dirname(__FILE__)) . "/../Resources/__TEMPLATES__/CARMEN/{account_name}/Publication/template.map " . $directory);

        if ($publishMap) {
            $mapfile = str_replace(".map", "", ($publishMap->getMapModel() ? "modele_" . preg_replace("!^modele_!", "", $publishMap->getMapFile()) : $publishMap->getMapFile()));
            $publishMap->setFullMapfilePath($directory . $mapfile . ".map");
            $publishMap->setMapfileName($mapfile);
        } else {
            $mapfile = "template";
        }
        $copy_file = "temp_admin_" . $user->getUserId() . "_" . $editMap->getMapId();

        if (!file_exists($directory . $copy_file . ".map")) {
            if (file_exists($directory . $mapfile . ".map")) {
                copy($directory . $mapfile . ".map", $directory . $copy_file . ".map");
            }
        }

        $editMap->setFullMapfilePath($directory . $copy_file . ".map");
        $editMap->setMapfileName($copy_file);

        return $copy_file;
    }

    /**
     * Copy the edited file to the published file and delete the edited file
     *
     * @param boolean $is_model True if the map file is a model file
     * @param string $edition_mapfile The name of file for edition
     * @param string $new_mapfile The name of published file to create
     * @param string $old_mapfile The old published file name to remove
     *
     * @return boolean the existence of the published file
     */
    protected function restorePublishedMapfile($is_model, $edition_mapfile, $new_mapfile, $old_mapfile = null)
    {
        $user = $this->getUser();
        if (!$user) return null;
        $user instanceof Users;

        $directory_read = $this->getMapfileDirectory();
        $directory_edit = $this->getMapfileDirectory();

        $edition_mapfile = str_replace(".map", "", $edition_mapfile);
        $new_mapfile = ($is_model ? "modele_" : "") . str_replace(".map", "", $new_mapfile);
        if ($old_mapfile !== null) {
            $old_mapfile = str_replace(".map", "", $old_mapfile);
        }

        if (file_exists($directory_edit . $edition_mapfile . ".map")) {
            $new_mapfile_as_filename = str_replace("/", "_", $new_mapfile);
            $contents = file_get_contents($directory_read . $edition_mapfile . ".map");
            $matches = array();
            preg_match_all("!\"(.+?" . $edition_mapfile . ".+?)\"!", $contents, $matches);
            foreach ($matches[1] as $match) {
                if (file_exists($directory_read . $match)) copy($directory_read . $match, str_replace($edition_mapfile, $new_mapfile_as_filename, $directory_read . $match));
            }
            file_put_contents($directory_read . $new_mapfile . ".map", str_replace($edition_mapfile, $new_mapfile_as_filename, $contents));
            if ($old_mapfile !== null && $old_mapfile != $new_mapfile)
                @unlink($directory_edit . $old_mapfile . ".map");
        } else if ($old_mapfile !== null && $old_mapfile != $new_mapfile) {
            @rename($directory_edit . $old_mapfile . ".map", $directory_read . $new_mapfile . ".map");
        }

        return file_exists($directory_read . $new_mapfile . ".map");
    }

    /**
     * Generate and return OWS context contents for a map Id
     * @param int $map_id
     * @param string $format
     * @param string $directory
     * @return Ambigous <\Symfony\Component\HttpFoundation\Response, string>
     */
    protected function getOWSContext($map_id, $map_file, $asPublication = false, $format = "JSON")
    {
        $user = $this->getUser();
        $user instanceof Users;
        $account = $user->getAccount();
        $prefix = ($asPublication ? $account->getAccountId() : "temp_admin_{$user->getUserId()}");
        $owsContext = $this->get('carmen.owscontext');
        $owsContext instanceof OwsContextController;
        $directory = ($asPublication ? $this->getCarmenConfig()->getOwscontextPublicationDir() : $this->getMapfileDirectory());
        $owsContext->generateContextAction($prefix, $account->getAccountId(), $map_id, ($asPublication ? $map_file : $map_id), $directory, false, $asPublication);

        $owsContext = $owsContext->getContextAction($prefix, ($asPublication ? $map_file : $map_id), $format, $directory, false);

        return $owsContext;
    }

    /**
     * Call a Mapserver API method through cURL.
     *
     * @param string $requestUri The requested method of the API (should start with a /).
     * @param string $method Accepted methods: GET, POST, PUT, DELETE (GET by default).
     * @param array $queryData GET data
     * @param array $requestData POST data
     * @param boolean $bThrow Throws an exception on error, instead of returning false (true by default)
     *
     * @return mixed The response as a json array, or false if curl failed or the response is not a valid json string, or the response is an error.
     *
     * @throws ApiException In case of error (see key 'description'), and only if $bThrow is true (otherwise, see the return statement)
     * @see BaseController::curl()
     *
     */
    public function callMapserverApi($requestUri, $method = 'GET', array $queryData = array(), array $requestData = array(), $bThrow = true, array $options = array())
    {
        self::$mapserverApiLastErr = null;
        //$timeBefore = microtime(true) ;
        $result = $this->curl($this->getMapserverApiUrl($requestUri), $method, $queryData, $requestData, $options);

        //$timeAfter = microtime(true) ;
        //error_log("temps d'exectuion de ".$requestUri." ".($timeAfter- $timeBefore)."s");
        if (false === $result) {
            self::$mapserverApiLastErr = new ApiException($this->_t('CURL error, bad request.'), (array)$result);
        } else {
            // try to cast result (should be a json string) as a json array
            if (is_string($result))
                $result = json_decode($result, true) ?: array();
            $result["success"] = true;
            if (null === $result) {
                // json decode failed, string is not valid
                $this->getLogger()->error('JSON (' . json_last_error() . '): ' . json_last_error_msg(), array('result' => $result));
                self::$mapserverApiLastErr = new ApiException($this->_t('Unsupported format, JSON string expected.'), $result);
                $result["success"] = false;
            } else if (isset($result['error'])) {
                // json decode succeded, but the mapserver api returned an error
                $this->getLogger()->error('Mapserver API', $result);
                self::$mapserverApiLastErr = new ApiException($result['error'] . (isset($result['description']) && $result['description'] != "" ? ', ' . $result['description'] : ''), $result);
                $result["success"] = false;
            }
        }

        if ($bThrow && false === (is_array($result) ? $result["success"] : $result))
            throw new ApiException($this->_t('Mapserver Api Error'), array("uri" => $requestUri, "method" => $method, "queryData" => $queryData, "requestData" => $requestData, "results" => $result), null, $this->getMapserverApiLastErr());

        return $result;
    }

    /**
     * @return string The callMapserverApi last error, or empty string if last call produced no error
     */
    protected function getMapserverApiLastErr()
    {
        return self::$mapserverApiLastErr;
    }


    /**
     * @return \Prodige\ProdigeBundle\Services\GeonetworkInterface
     */
    protected function getGeonetworkInterface($useDefaultAuthentication = null)
    {
        $this->initializeCurlUtilities($useDefaultAuthentication);
        return new GeonetworkInterface();
    }

    protected function initializeCurlUtilities($useDefaultAuthentication = null)
    {
        $this->curlUseDefaultAuthentication($useDefaultAuthentication);
        CurlUtilities::initialize($this->container->getParameter("phpcli_default_login"), $this->container->getParameter("phpcli_default_password"), $this->container->getParameter("server_ca_cert_path"));
        return $this;
    }

    protected function curlUseDefaultAuthentication($useDefaultAuthentication = null)
    {
        if (!is_null($useDefaultAuthentication))
            CurlUtilities::useDefaultAuthentication($useDefaultAuthentication);
    }

    /**
     * cURL wrapper.
     *
     * @param string $url The URL.
     * @param string $method Accepted methods: GET, POST, PUT, DELETE (GET by default).
     * @param array $queryData GET data
     * @param array $requestData POST data
     * @param array $options Curl options
     *
     * @return mixed cURL exec result (the response body in case of success, false if an error occurred).
     *
     * @throws MethodNotAllowedHttpException If $method is not an acceptable method.
     */
    protected function curl($url, $method = 'GET', array $queryData = array(), array $requestData = array(), array $options = array())
    {
        $args = func_get_args();
        $allow = array('GET', 'POST', 'PUT', 'DELETE');
        if (!in_array($method, $allow)) {
            throw new MethodNotAllowedHttpException($allow);
        }

        if (!empty($queryData)) {
            $url .= (strpos($url, '?') === false ? '?' : '&') . http_build_query($queryData);
        }

        $this->initializeCurlUtilities();
        $curl = CurlUtilities::curl_init($url);

        $opt = array(
            CURLOPT_RETURNTRANSFER => true,  // if set to true, curl_exec() return body content in case of success
            CURLOPT_FAILONERROR => false, // if set to true, curl_exec() will return false if response status code is >= 400
        );
        switch ($method) {
            case 'GET':
                break;
            case 'POST':
                $opt[CURLOPT_POST] = 1;
                $opt[CURLOPT_POSTFIELDS] = (is_array($requestData)) ? http_build_query($requestData) : $requestData;
                break;
            case 'PUT':
                $opt[CURLOPT_CUSTOMREQUEST] = $method;
                $requestData = (is_array($requestData)) ? http_build_query($requestData) : $requestData;
                $opt[CURLOPT_RETURNTRANSFER] = true;
                $opt[CURLOPT_HTTPHEADER] = array('Content-Length: ' . strlen($requestData));
                $opt[CURLOPT_POSTFIELDS] = $requestData;
                break;
            case 'DELETE':
                $opt[CURLOPT_CUSTOMREQUEST] = $method;
                break;
        }

        foreach ($options as $key => $value) {
            $opt[$key] = $value;
        }
        curl_setopt_array($curl, $opt);
        $result = curl_exec($curl);

        $this->getLogger()->info('CURL ' . $url, array("args" => $args, "results" => $result));
        if (!$result) {
            $this->getLogger()->error('CURL (' . curl_errno($curl) . '): ' . curl_error($curl), array_merge($args, array('url' => $url)));
            $result = array(
                "success" => false,
                "status_code" => curl_errno($curl),
                "error" => curl_error($curl),
                "data" => array_merge($args, array('url' => $url))
            );
        }

        curl_close($curl);

        return $result;
    }

    /**
     * Performs transactional operations in database.
     *
     * Transaction is rollbacked if $callable throws any kind of exception.
     *
     * @param callable $transaction A callable that performs transactionnal operations in database, EntityManager is injected when called
     * @param callable $returnOk The optionnal callable to format the data returned by the transaction in a SUCCESS Response
     * @param callable $returnError The optionnal callable to format the exception thrown by the transaction in a FAILURE Response
     *
     * @return JsonResponse The response.
     */
    protected function transactional(callable $transaction, callable $returnOk = null, callable $returnError = null)
    {
        $this->getManager()->beginTransaction();
        $result = null;
        try {

            // custom callable, rollback will be called if any exception occurs here
            // $data contains mixed data used in the formatting function $returnOk
            $data = $transaction($this->getManager());

            $this->getManager()->flush();
            $this->getManager()->commit();
        } catch (\Exception $exception) {
            try {
                $this->getManager()->rollback();
            } catch (\Exception $exception2) {
            }

            $this->getLogger()->error('Transactional error : ' . $exception->getMessage(), array('trace' => $exception->getTraceAsString()));

            $data = array();
            if ($exception instanceof ApiException) {
                $data = $exception->getData();
            }
            // $data contains mixed data used in the formatting function $returnOk
            if ($returnError) return $returnError($exception);
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Transactional Exception") . ' : ' . $exception->getMessage(), array_merge($data, array("stack" => $exception->getTraceAsString())));
        }

        if ($returnOk) return $returnOk($data);
        return $this->formatJsonSuccess();
    }


    /**
     * Delete in database the items of collection and clear the collection
     * @param EntityManager $em
     * @param Collection $collection
     */
    protected function removeAll(EntityManager $em, Collection $collection)
    {
        foreach ($collection as $entity) {
            $em->remove($entity);
            $em->flush($entity);
        }
        $collection->clear();
    }

    /**
     * Get a Geosource Metadata by its uuid
     * @param string $uuid
     *
     * @return mixed array|boolean return false on error, or the metadata array
     */
    protected function getGeosourceMetadata($uuid)
    {
        $object = array();
        try {
            $record = $this->getCswClient()->getRecordById($uuid);

            if ($record) {
                $xmlIso19139 = new XmlISO19139();
                $xmlIso19139->loadXmlContent($record);
                $object['uuid'] = $xmlIso19139->getID();
                $object['cswTitle'] = $xmlIso19139->getTitle();
                $object['cswAbstract'] = $xmlIso19139->getAbstract();
                $object['cswGenealogy'] = $xmlIso19139->getLineage();
                $object['cswIsotheme'] = $xmlIso19139->getISOThemes();
                $object['cswKeywords'] = $xmlIso19139->getKeywords();
                $object['cswInspiretheme'] = $xmlIso19139->getINSPIRE();
                $object['cswContact'] = $xmlIso19139->getContactName();
            } else {
                $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Unable to load XML document (' . $uuid . ')');
                return false;
            }
        } catch (\Exception $exception) {
            $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Unable to load XML document (' . $uuid . ')');
            return false;
        }

        return $object;
    }

    /**
     * Deletes a geosource metadata
     *
     * @param string $uuid
     *
     * @return boolean true in case of success, false otherwise
     */
    protected function deleteGeosourceMetadata($uuid)
    {
        try {
            $res = $this->getCswClient()->getRecordById($uuid);
            if ($res) {
                $xmlIso19139Update = new XmlISO19139();
                $xmlIso19139Update->loadXmlContent($res);
                $this->getCswClient()->deleteMetadata($xmlIso19139Update);
                $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Deleted Geosource metadata (' . $uuid . ')');

                return true;
            }
        } catch (\Exception $e) {
            $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Failed to delete Geosource metadata (' . $uuid . ')');
            return false;
        }

        return false;
    }

    /**
     * Creates, updates or deletes a geosource map metadata
     *
     * @param Map $map
     * @param array $msMap
     */
    protected function createOrUpdateGeosourceMapMetadata(Map $map, array $msMap)
    {
        $this->getLogger()->debug(sprintf(__CLASS__ . '::' . __FUNCTION__ . ': Creating/Updating Geosource Map metadata for Map %s (%s)', $map->getMapTitle(), $map->getMapId()));
        $tabWmsMetataInfo = array();
        $tabWfsMetataInfo = array();
        $tabAtomMetataInfo = array();

        $getFormalName = function (Layer $layer) {
            //$formal_name = strtr($oLayer->getMetadata("LAYER_TITLE"), utf8_decode('àáâãäåòóôõöøèéêëçìíîïùúûüÿñ'), 'aaaaaaooooooeeeeciiiiuuuuyn');
            $name = $layer->getLayerTitle();
            $name = preg_replace("/[^a-zA-Z0-9]/", "_", $name);

            return $name;
        };

        foreach ($map->getLayers() as $layer) {
            $layer instanceof Layer;
            //if( $layer->getLayerMetadataUuid() ) {
            // WMS
            if ($layer->getLayerWms()) {
                $tabWmsMetataInfo[] = array(
                    "mdd" => $layer->getLayerMetadataUuid(),
                    "name" => $getFormalName($layer)
                );
            }
            // WFS
            if ($layer->getLayerWfs()) {
                $tabWfsMetataInfo[] = array(
                    "mdd" => $layer->getLayerMetadataUuid(),
                    "name" => $getFormalName($layer)
                );
            }
            // ATOM
            if ($layer->getLayerAtom() /* TODO and/or $layer->getLayerDownloadable() ? */) {
                $tabAtomMetataInfo[] = array(
                    "mdd" => $layer->getLayerMetadataUuid(),
                    "name" => $getFormalName($layer)
                );
            }
            //}
        }

        // WMS
        if (count($tabWmsMetataInfo) > 0) {
            $uuid = $this->updateGeosourceMapMetadata($map, $msMap, "wms", $tabWmsMetataInfo, $map->getMapOnlinResource('WMS'));
            $map->setMapWmsmetadataUuid($uuid);
        } else {
            // delete metadata
            if ($this->deleteGeosourceMetadata($map->getMapWmsmetadataUuid())) {
                $map->setMapWmsmetadataUuid(null);
            }
        }
        // WFS
        if (count($tabWfsMetataInfo) > 0) {
            $uuid = $this->updateGeosourceMapMetadata($map, $msMap, "wfs", $tabWfsMetataInfo, $map->getMapOnlinResource('WFS'));
            $map->setMapWfsmetadataUuid($uuid);
        } else {
            // delete metadata
            if ($this->deleteGeosourceMetadata($map->getMapWfsmetadataUuid())) {
                $map->setMapWfsmetadataUuid(null);
            }
        }
        // ATOM
        if (count($tabAtomMetataInfo) > 0) {
            //$atomUrl = str_ireplace("WMS/","ATOM/",$onlineresource);
            //$atomUrl = str_ireplace("?",".atom",$atomUrl);  //l'url doit pointer sur le fichier ATOM
            $url = $map->getMapOnlinResource('ATOM');
            $url = str_replace("?", ".atom", $url);
            $uuid = $this->updateGeosourceMapMetadata($map, $msMap, "atom", $tabAtomMetataInfo, $url);
            $map->setMapAtommetadataUuid($uuid);
        } else {
            // delete metadata
            if ($this->deleteGeosourceMetadata($map->getMapAtommetadataUuid())) {
                $map->setMapAtommetadataUuid(null);
            }
        }

        // update map
        $em = $this->getManager();
        $em->persist($map);
        $em->flush();
    }

    /**
     * Creates or updates the Geosource metadata of a map
     *
     * @param Map $map
     * @param array $msMap
     * @param string $service wms|wfs|atom
     * @param array $metadataInfo
     *
     * @return mixed boolean|string the created or updated map metadata uuid, or false if an error occured
     * @throws \Exception if $service is not valid
     *
     */
    private function updateGeosourceMapMetadata(Map $map, array $msMap, $service, array $metadataInfo, $onlineResource)
    {
        $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Creating/Updating Geosource Map metadata uuid', array($service, $metadataInfo, $onlineResource));
        $mapMetadataUuid = null;
        switch ($service) {
            case "wms":
                $mapMetadataUuid = $map->getMapWmsmetadataUuid();
                break;
            case "wfs":
                $mapMetadataUuid = $map->getMapWfsmetadataUuid();
                break;
            case "atom":
                $mapMetadataUuid = $map->getMapAtommetadataUuid();
                break;
            default:
                $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Unknown service ' . $service, $onlineResource);
                throw new \Exception('Unknown service ' . $service);
        }

        $bCreate = null == $mapMetadataUuid;
        if (!$bCreate) {
            // mode update
            $res = $this->getCswClient()->getRecordById($mapMetadataUuid);
            if ($res) {
                $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Updating Geosource Map metadata uuid ' . $mapMetadataUuid . '...');
                $xmlIso19139 = new XmlISO19139();
                $xmlIso19139->loadXmlContent($res);
                $xmlIso19139 = $this->updateMDS($xmlIso19139, $map, $msMap, $metadataInfo, true, $service, $onlineResource);
                try {
                    $this->getCswClient()->updateMetadata($xmlIso19139);
                    $this->getCswClient()->publishMetadata($mapMetadataUuid);
                    $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Metadata uuid ' . $mapMetadataUuid . ' updated');
                } catch (\Exception $e) {
                    $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Error calling Geosource CswClient::updateMetadata; ' . $e->getTraceAsString());
                    return false;
                }
            } else {
                $bCreate = true;
            }
        }
        if ($bCreate) {
            // mode create
            $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Creating Geosource Map metadata uuid...');
            $xmlIso19139 = new XmlISO19139();
            $xmlIso19139->createNewMDWMS();
            $xmlIso19139 = $this->updateMDS($xmlIso19139, $map, $msMap, $metadataInfo, false, $service, $onlineResource);
            try {
                $this->getCswClient()->insertMetadata($xmlIso19139);
                $mapMetadataUuid = $xmlIso19139->getID();
                $this->getCswClient()->publishMetadata($mapMetadataUuid);
                $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Metadata uuid ' . $mapMetadataUuid . ' created');
            } catch (\Exception $e) {
                $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Error calling Geosource CswClient::insertMetadata; ' . $e->getTraceAsString());
                return false;
            }
        }

        return $mapMetadataUuid;
    }

    /**
     * Update csw map metadata
     *
     * @param XmlISO19139 $xmlIso19139
     * @param Map $map
     * @param array $msMap
     * @param array $metadataInfo
     * @param boolean $bUpdate
     * @param string $service wms|wfs|atom
     *
     * @return \Carmen\CswBundle\Csw\XmlISO19139
     */
    private function updateMDS(XmlISO19139 $xmlIso19139, Map $map, array $msMap, array $metadataInfo, $bUpdate, $service, $onlineResource)
    {
        $this->getLogger()->debug(sprintf(__CLASS__ . '::' . __FUNCTION__ . ': Updating Geosource metadata of Map "%s" (%s) for uuid %s', $map->getMapTitle(), $map->getMapId(), $xmlIso19139->getID()), array(
            $bUpdate, $metadataInfo, $service, $onlineResource
        ));

        $mapProjection = $map->getMapProjectionEpsg() ? $map->getMapProjectionEpsg()->getProjectionProvider() . ":" . $map->getMapProjectionEpsg()->getProjectionEpsg() : '';
        $tabCoordinates = $this->transformExtent($mapProjection, array(
            $map->getMapExtentXmin(),
            $map->getMapExtentYmin(),
            $map->getMapExtentXmax(),
            $map->getMapExtentYmax(),
        ));

        $xmlIso19139->changeLanguage($msMap["metadata"]["wms_languages"]);
        if ($msMap["metadata"]["wms_contactorganization"] != "") $xmlIso19139->changeContactName($msMap["metadata"]["wms_contactorganization"]);
        if ($msMap["metadata"]["wms_contactelectronicmailaddress"] != "") $xmlIso19139->changeEmailAddress($msMap["metadata"]["wms_contactelectronicmailaddress"]);
        $xmlIso19139->changeDateStamp(); //date de mise à jour de la MD

        if ($service == "wms") $xmlIso19139->changeTitle($msMap['name'] . " : Service de visualisation cartographique (WMS)"); // $oMap->name
        if ($service == "wfs") $xmlIso19139->changeTitle($msMap['name'] . " : Service de de téléchargement direct (WFS)"); // $oMap->name
        if ($service == "atom") $xmlIso19139->changeTitle($msMap['name'] . " : Service de téléchargement simple (ATOM)"); // $oMap->name

        //date de publication (creation) du service
        if (!$bUpdate) {
            $datetime = new \DateTime(date("Y-m-d"));
            $xmlIso19139->changeDate($datetime, "");
        }

        if ($msMap["metadata"]["wms_abstract"] != "") $xmlIso19139->changeAbstract($msMap["metadata"]["wms_abstract"]);
        $xmlIso19139->changeConstraints($msMap["metadata"]["wms_fees"]);
        $xmlIso19139->changeAccessConstraints($msMap["metadata"]["wms_accessconstraints"]);
        $xmlIso19139->changeSrvExtent($tabCoordinates[0], $tabCoordinates[1], $tabCoordinates[2], $tabCoordinates[3]);
        $xmlIso19139->removeAllDataToService();
        foreach ($metadataInfo as $meta) {
            $xmlIso19139->addDataToService($meta["mdd"], $meta["name"], $service);
        }

        $xmlIso19139->changeURLService($onlineResource/*$map->getMapOnlinResource(strtoupper($service))*/);

        return $xmlIso19139;
    }

    /**
     * Creates or updates a geosource layer metadata
     *
     * @param Layer $layer
     * @param array $msLayer
     * @param string $uuid
     * @param boolean $bPublish
     * @param string $csw_title
     * @param string $csw_abstract
     * @param string $csw_keyword
     * @param string $csw_inspiretheme
     *
     * @return mixed string|boolean returns the created or updated uuid, of false if an error occured
     */
    protected function createOrUpdateGeosourceLayerMetadata(Layer $layer, array $msLayer, $uuid = null, $bPublish = false, $csw_title = null, $csw_abstract = null, $csw_keyword = null, $csw_inspiretheme = null)
    {
        $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Creating/Updating Geosource layer metadata uuid ' . $uuid);
        if ($uuid && $uuid != "") {
            // update metadata
            $record = $this->getCswClient()->getRecordById($uuid);
            if ($record) {
                $xmlMDD = new XmlISO19139();
                $xmlMDD->loadXmlContent($record);
                $xmlMDD = $this->updateGeosourceLayerMetadata(true, $layer, $msLayer, $xmlMDD, $csw_title, $csw_abstract, $csw_keyword, $csw_inspiretheme);
                try {
                    $this->getCswClient()->updateMetadata($xmlMDD);
                    $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Metadata uuid ' . $uuid . ' updated');
                } catch (\Exception $e) {
                    $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Error calling Geosource CswClient::updateMetadata; ' . $e->getTraceAsString());
                    return false;
                }
            } else {
                $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Unable to load XML document (' . $uuid . ')');
                return false;
            }
        } else {
            // create metadata
            $xmlMDD = new XmlISO19139();
            $xmlMDD->createNewMDD();
            $xmlMDD = $this->updateGeosourceLayerMetadata(false, $layer, $msLayer, $xmlMDD, $csw_title, $csw_abstract, $csw_keyword, $csw_inspiretheme);
            try {
                $this->getCswClient()->insertMetadata($xmlMDD);
                $uuid = $xmlMDD->getID();
                if ($bPublish) {
                    $this->getCswClient()->publishMetadata($uuid);
                }
                $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': Metadata uuid ' . $uuid . ' created');
            } catch (\Exception $e) {
                $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': Error calling Geosource CswClient::insertMetadata; ' . $e->getTraceAsString());
                return false;
            }
        }

        return $uuid;
    }

    /**
     * Updates a geosource layer metadata
     * @param boolean $bUpdate
     * @param Layer $layer
     * @param array $msLayer
     * @param XmlISO19139 $xmlMDD
     * @param string $csw_title
     * @param string $csw_abstract
     * @param string $csw_keyword
     * @param string $csw_inspiretheme
     */
    private function updateGeosourceLayerMetadata($bUpdate, Layer $layer, array $msLayer, XmlISO19139 $xmlMDD, $csw_title, $csw_abstract, $csw_keyword, $csw_inspiretheme)
    {
        $geosourceUrl = $this->getGeosourceUrl();

        //$xmlMDD->changeURLData("http://www.google.fr/toto");
        //cswLog::logInfo("URL:".$xmlMDD->getURLData());
        //$mapProjection = $m_oMap->getProjection();
        //cswLog::logInfo($xmlMDD->getSRS());
        //cswLog::logInfo($xmlMDD->getConstraints());
        //$xmlMDD->changeContactName("test");
        //cswLog::logInfo($xmlMDD->getContactName());
        //$datetime = new DateTime(date("Y-m-d"));

        $this->getLogger()->debug(sprintf(__CLASS__ . '::' . __FUNCTION__ . ': Updating Geosource metadata of Layer "%s" (%s) for uuid %s', $layer->getLayerName(), $layer->getLayerId(), $xmlMDD->getID()), array(
            $bUpdate, $csw_title, $csw_abstract, $csw_keyword, $csw_inspiretheme
        ));

        $map = $layer->getMap();
        //save extent object
        $tabExtentSave = array();
        $tabExtentSave[] = $map->getMapExtentXmin(); //$m_oMap->extent->minx;
        $tabExtentSave[] = $map->getMapExtentYmin(); //$m_oMap->extent->miny;
        $tabExtentSave[] = $map->getMapExtentXmax(); //$m_oMap->extent->maxx;
        $tabExtentSave[] = $map->getMapExtentYmax(); //$m_oMap->extent->maxy;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //change map Extent according to layer extent
        //CSCarteExtentFromLayer_set($m_oMap); // TODO change this ????
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $mapProjection = $map->getMapProjectionEpsg() ? $map->getMapProjectionEpsg()->getProjectionProvider() . ":" . $map->getMapProjectionEpsg()->getProjectionEpsg() : '';
        $tabCoordinates = $this->transformExtent($mapProjection, $tabExtentSave);
        //reload saved extent
        //$m_oMap->setExtent($tabExtentSave[0], $tabExtentSave[1], $tabExtentSave[2], $tabExtentSave[3]); // TODO change this ????
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $xmlMDD->changeTitle($csw_title);
        $xmlMDD->changeAbstract($csw_abstract);
        $xmlMDD->changeExtent($tabCoordinates[0], $tabCoordinates[1], $tabCoordinates[2], $tabCoordinates[3]);

        //Modif_FH
        $xmlMDD->changeDateStamp(); //date de mise à jour de la MD


        $xmlMDD->changeURLData($geosourceUrl . "/find?uuid=" . $xmlMDD->getID());
        $xmlMDD->changeUniqueIdentifier(substr($geosourceUrl, 0, strrpos($geosourceUrl, '/')) . "/metadata/" . $xmlMDD->getID());

        //chaque keyword doit etre dans une balise gmd:keyword
        $xmlMDD->removeKeywords();
        if ($csw_keyword != "") {
            $xmlMDD->addKeywords($csw_keyword);
        }
        if ($csw_inspiretheme != "") {
            $xmlMDD->addInspireKeyword($csw_inspiretheme);
        }

        switch ($layer->getLayerType()) {
            case LexLayerType::POSTGIS/*MS_POSTGIS*/ :
                $xmlMDD->changeDataFormat("POSTGIS");
                break;

            case LexLayerType::VECTOR/*MS_SHAPEFILE*/ :
                $xmlMDD->changeDataFormat("SHP");
                break;

            case LexLayerType::WFS/*MS_OGR*/ :
            case LexLayerType::WMS/*MS_OGR*/ :
            case LexLayerType::WMSC/*MS_OGR*/ :
                $data = $msLayer['connection']; //$oLayer->connection;
                $format = strtoupper(substr($data, strrpos($data, ".") + 1));
                $xmlMDD->changeDataFormat($format);
                break;

            case LexLayerType::RASTER/*MS_RASTER*/ :
                $data = $msLayer['data']; //$oLayer->data;
                $format = strtoupper(substr($data, strrpos($data, ".") + 1));
                $xmlMDD->changeDataFormat($format);
                break;
        }

        return $xmlMDD;
    }

    /**
     * Transform map extent from projection to 4326
     *
     * @param string $inputEpsg eg. 2154
     * @param array $extent array with values minx, miny, maxx, maxy
     */
    protected function transformExtent($inputEpsg, array $extent)
    {
        $id = uniqid();
        $inputfile = sys_get_temp_dir() . '/input_extent_' . $id . '.txt';
        $outputfile = sys_get_temp_dir() . '/output_extent_' . $id . '.txt';

        file_put_contents($inputfile, sprintf("%s %s\n%s %s", $extent[0], $extent[1], $extent[2], $extent[3]));

        // $process = new Process('cs2cs -f "%.2f" +init=' . strtolower($inputEpsg) . ' +to +init=epsg:4326 ' . $inputfile . ' > ' . $outputfile);
        $process = new Process(['cs2cs', '-f', '"%.2f"', '+init=' . strtolower($inputEpsg), '+to', '+init=epsg:4326 ', $inputfile, '>', $outputfile]);
        $process->run();

        if (!$process->isSuccessful()) {
            $ex = new ProcessFailedException($process);
            $this->getLogger()->error(__CLASS__ . '::' . __FUNCTION__ . ': ' . $ex->getMessage());

            @unlink($inputfile);
            @unlink($outputfile);
            return $extent; // if an error occured, return the extent as is
        }

        $strExtent = file_get_contents($outputfile);

        /*
        //$extent = $oMap->extent;
        global $CMD_BIN_CS2CS;
        $inputfile = $Directory."/input_extent.txt";
        $fp = fopen($inputfile, 'w');
        fwrite($fp, $extent->minx." ". $extent->miny ."\n".$extent->maxx." ". $extent->maxy);
        fclose($fp);
        $outputfile = $Directory."/output_extent.txt";
        $strCmd = $CMD_BIN_CS2CS." -f \"%.2f\" ".$inputEpsg." +to +init=epsg:4326 ".$inputfile." > ".$outputfile;
        exec($strCmd);
        $fp = fopen($outputfile, 'r');
        $strExtent = fread($fp, filesize($outputfile));
        fclose($fp);
        */

        $tabCoordinates = explode("\n", $strExtent);
        $tabXYmin = explode("\t", $tabCoordinates[0]);
        $xmin = $tabXYmin[0];
        $tabY = explode(" ", $tabXYmin[1]);
        $ymin = $tabY[0];
        $tabXYmax = explode("\t", $tabCoordinates[1]);
        $xmax = $tabXYmax[0];
        $tabY = explode(" ", $tabXYmax[1]);
        $ymax = $tabY[0];


        $this->getLogger()->debug(__CLASS__ . '::' . __FUNCTION__ . ': extent transformed ' . sprintf('(%s, %s, %s, %s)', $xmin, $ymin, $xmax, $ymax));

        return array($xmin, $ymin, $xmax, $ymax);
    }

    public function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }

    /**
     * Get Doctrine connection to database CATALOGUE
     * @param ContainerInterface $container
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected static function _getCatalogueConnection(ContainerInterface $container, $schema = "public")
    {
        $connection = $container->get('doctrine')->getConnection(\Prodige\ProdigeBundle\Controller\BaseController::CONNECTION_CATALOGUE);
        $connection->exec('set search_path to ' . $schema);
        return $connection;
    }

    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return self::_getCatalogueConnection($this->container, $schema);
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }
}
