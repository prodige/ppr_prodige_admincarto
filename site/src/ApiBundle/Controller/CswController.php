<?php

namespace Carmen\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * Carmen CSW Wrapper Controller.
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/api/csw")
 */
class CswController extends BaseController
{
    /**
     * Returns the list of records title
     *
     * @Route("/recordstitle", name="carmen_ws_get_csw_recordstitle", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getRecordsTitle(Request $request)
    {
        $values = array();
        try {
            $titles = $this->getCswClient()->getRecordsTitle("dataset");
            // sort by titles
            if( is_array($titles) ) {
                $titles = array_diff($titles, array("", null));
                asort($titles);
                foreach ($titles as $key=>$title) {
                    $values[] = array('value'=>$key, 'label'=>$title);
                }
            }
        } catch(\Exception $e) {
            $this->getLogger()->error(__CLASS__.'::'.__FUNCTION__.': Error calling Geosource CswClient::getRecordsTitle; '.$e->getTraceAsString());
        }
        
        return new JsonResponse($values);
    }
    
    /**
     * Returns a records
     *
     * @Route("/record/{uuid}", name="carmen_ws_get_csw_record", options={"expose"=true}, methods={"GET"})
     *
     * @return JsonResponse Json array.
     */
    public function getRecordById(Request $request, $uuid)
    {
        $object = $this->getGeosourceMetadata($uuid);
        if( !$object ) return $this->formatJsonError(Response::HTTP_NOT_FOUND, "Metadata not found: ".$uuid);
        
        return new JsonResponse($object);
    }
}
