<?php

namespace Carmen\ApiBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Carmen\ApiBundle\Entity\FavoriteArea;
use Carmen\ApiBundle\Entity\Map;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// use FOS\RestBundle\Controller\Annotations\Delete;
// use FOS\RestBundle\Controller\Annotations\Get;
// use FOS\RestBundle\Controller\Annotations\Route;
// use FOS\RestBundle\Controller\Annotations\Head;
// use FOS\RestBundle\Controller\Annotations\Link;
// use FOS\RestBundle\Controller\Annotations\Patch;
// use FOS\RestBundle\Controller\Annotations\Post;
// use FOS\RestBundle\Controller\Annotations\Put;
// use FOS\RestBundle\Controller\Annotations\Unlink;
// use FOS\RestBundle\Request\ParamFetcher;
// use FOS\RestBundle\Controller\Annotations\QueryParam;   // GET params
// use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params

// use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Carmen Default API Controller.
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/api")
 */
class FavoriteAreaController extends ApiController
{
    const FAVORITEAREA_ENTITY      = 'CarmenApiBundle:FavoriteArea';
    const MAP_ENTITY        = 'CarmenApiBundle:Map';
    
    /**
     * Restfull api to serve Entity.
     *
     * @Route("/favoritearea/{mapId}/rest/{id}", name="carmen_ws_favoritearea", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"GET","POST","PUT","PATCH","DELETE"})
     * 
     * @param Request $request  request object
     * @param int     $id       layer id, null by default
     */
    public function favoriteAreaRestAction(Request $request, $mapId, $id = null)
    {
        switch ($request->getMethod()) {
            case 'GET':
                return $this->getFavoriteAreaAction($request, $mapId, $id);
                break;
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return $this->postFavoriteAreaAction($request, $mapId, $id);
                break;
            case 'DELETE':
                return $this->deleteFavoriteAreaAction($mapId, $id);
                break;
        }

        return $this->formatJsonError(Response::HTTP_METHOD_NOT_ALLOWED, $this->_t("Method not allowed"));
    }

    /**
     * Returns a list of Map or the defined layer by id.
     * 
     * Extra filters can be added using query parameters such as 'filter[someColumn]=someValue'.
     * @see BaseController::getQueryBuilderList()
     * 
     * @param Request $request The request object.
      * @param string $mapId   If set and $id is null, returns a list with the requested Map only (if exists).
     * @param string  $id      If set, returns the requested layer only (if exists).
     * 
     * @Route("/favoritearea/{mapId}/get/{areaId}", name="carmen_ws_get_favoritearea", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"GET"})
     * 
     * @return JsonResponse Json array.
     */
    public function getFavoriteAreaAction(Request $request, $mapId, $id = null)
    {
        $logger = $this->getLogger();
        
        $filters = $request->query->get('filter', array());
        if ( !is_array($filters) && !empty($filters) ){
            $filters = json_decode($filters, true) ?: array();
        }
        $property = array(); 
        $value = array();
        foreach ($filters as $filter){
            foreach($filter as $var_name=>$var_value){
                ${"$var_name"}[] = $var_value;
            }
        }
        $filters = array_combine($property, $value);

        $filters["map"] = $mapId;
        
        try {
            $qb = $this->getQueryBuilderList(self::FAVORITEAREA_ENTITY, 'e', $id, $filters);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception"), $this->isProd() ? array() : array('exception'=>$ex->getMessage()));
        }

        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * @Route("/favoritearea/{mapId}/post/{id}", name="carmen_ws_post_favoritearea", requirements={"mapId"="\d+", "id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"POST", "PUT", "PATCH"})
     * 
     * @param Request $request  request object.
     * @param string  $id       If set, updates an existing layer, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function postFavoriteAreaAction(Request $request, $mapId, $id = null)
    {
        $user = $this->getUser();
        if ( !$user ) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), 
                                          $this->_t("Unable to find current user."));
        }
        $user instanceof Users;
  
        $account = $user->getAccount();
        if ( !$account ) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), 
                                          $this->_t("Unable to find current account."));
        }
        
        return $this->transactional(
            function(EntityManager $em) use ($request, $mapId, $id) {
                try {
                	$oMap = $this->getRepository(self::MAP_ENTITY)->find($mapId);
	                $request->request->set("map", $mapId);
	                $favoriteArea = $this->doGenericPost($request, self::FAVORITEAREA_ENTITY, $id, null, true);
	                $favoriteArea instanceof FavoriteArea;
	                return $favoriteArea;
                } catch (\Exception $exception){
                	return array("success"=>false);
                }
            },
            function($favoriteArea){
              return new JsonResponse( json_decode($this->jsonSerializeEntity($favoriteArea), true) );
            }
        );
    }

    
    /**
     * Deletes an Entity.
     *
     * @Route("/favoritearea/{mapId}/delete/{id}", name="carmen_ws_delete_favoritearea", requirements={"mapId"="\d+", "id"="\d+"}, options={"expose"=true}, methods={"DELETE"})
     *
     * @param string  $id  layer id to delete
     *
     * @return Response Empty response if success, JsonResponse error if failed.
     */
    public function deleteFavoriteAreaAction($mapId, $id)
    {
        $me = $this;
        $areaId = $id;
        return $this->transactional(
            function(EntityManager $em) use ($me, $mapId, $areaId) {
                if( null != $areaId ) {
                    $edit_directory = $me->getMapfileDirectory();
                    $read_directory = $me->getMapfileDirectory();
                    $files = array();
                    
                    $oMap = $this->getRepository($me::MAP_ENTITY)->find($mapId);
                    if ( !$oMap ) {
                        throw new EntityNotFoundException();
                    }
                    $oMap instanceof Map;
                    
                    $oArea = $this->getRepository($me::FAVORITEAREA_ENTITY)->find($areaId);
                    if ( !$oArea ) {
                        throw new EntityNotFoundException();
                    }
                    $oArea instanceof FavoriteArea;
                    
                    $em->remove($oArea);
                    $em->flush();
                    
                    return array("areaId" => $areaId, "map" => $mapId);
                }
            },
            function($data) {
              return new JsonResponse($data);
            }
        );
    }
    
}
