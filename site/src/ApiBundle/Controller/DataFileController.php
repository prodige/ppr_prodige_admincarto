<?php

namespace Carmen\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Base Controller that provides common helper functions.
 *
 * @author alkante <support@alkante.com>
 */
class DataFileController extends BaseController
{
    /**
     * Make a copy for edition of the published map.
     * @Route("/IHM/IHM/{account_path}/{file}", name="carmen_data_account_ihm_file", requirements={"file"=".+"}, options={"expose"=true})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function readfileDataAccountIHM(Request $request, $account_path, $file)
    {
    	$fileIHM = $this->getCarmenConfig()->getMapserverRootData()."/{$account_path}/IHM/{$file}";
    	$filePublication = $this->getCarmenConfig()->getMapserverRootData()."/{$account_path}/Publication/{$file}";
      if ( file_exists($fileIHM) ){
    		return new BinaryFileResponse($fileIHM);
    	} else if ( file_exists($filePublication) ){
    		return new BinaryFileResponse($filePublication);
    	} else {
    		return new Response(Response::$statusTexts[Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
    	}
    }
    /**
     * Make a copy for edition of the published map.
     * @Route("/METADATA/{account_path}/{file}", name="carmen_data_account_metadata_file", requirements={"file"=".+"}, options={"expose"=true})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function readfileDataAccountMetadata(Request $request, $account_path, $file)
    {
    	$fileMETADATA = $this->getCarmenConfig()->getMapserverRootData()."/{$account_path}/METADATA/{$file}";
      if ( file_exists($fileMETADATA) ){
    		return new BinaryFileResponse($fileMETADATA);
    	} else {
    		return new Response(Response::$statusTexts[Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
    	}
    }
    /**
     * Make a copy for edition of the published map.
     * @Route("/reference/{file}", name="carmen_data_reference_file", requirements={"file"=".+"}, options={"expose"=true})
     * 
     * @param Request $request  The request object.
     * @param string  $id       If set, updates an existing user, else creates it.
     * 
     * @return JsonResponse Json object.
     */
    public function readfileDataReference(Request $request, $file)
    {
    	$file = $this->getMapfileDirectory()."reference/".$file;
      if ( file_exists($file) ){
    		return new BinaryFileResponse($file);
    	} else {
    		return new Response(Response::$statusTexts[Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
    	}
    }
}
