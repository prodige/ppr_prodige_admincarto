<?php

namespace Carmen\ApiBundle\Controller;

use Carmen\ApiBundle\Entity\Adherent;
use Carmen\ApiBundle\Exception\ApiException;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Carmen Default API Controller.
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/api")
 */
class ApiController extends BaseController
{
    /**
     * Returns data formatted for use with a combobox.
     *
     * Extra filters can be added using query parameters such as <code>filter[someColumn]=someValue</code>.
     * @return JsonResponse Json array.
     * @example /combobox/adherent?fields=adherentId,adherentNom&filter[adherentDns.dnsUrl][value]=test&filter[adherentDns.dnsUrl][join]=inner
     *
     * @see BaseController::applyFiltersToQueryBuilder()
     *
     * @Route("/all_lexicals", name="carmen_ws_get_all_lexicals", options={"expose"=true}, methods={"GET"})
     *
     */
    public function getAllLexicals(Request $request)
    {
        $fields = $request->query->get('fields', "");
        $filters = $request->query->get('filter', array());

        $results = array();

        $dictionaries = array("lexTool", "lexUnits", "lexIsoCondition", "lexProjection", "lexCategoryKeyword", "lexColorId", "lexLayerType", "lexAnalyseType", "lexFieldType", "lexFieldDatatype", "lexInspireTheme", "lexIsoLanguage");
        foreach ($dictionaries as $entity) {
            $qb = $this->createQueryBuilder('CarmenApiBundle:' . ucfirst($entity), 'e');
            $qb = $this->applyFiltersToQueryBuilder($qb, $filters);

            if ($fields != "") {
                // if some fields are requested, only retreive those ones
                $qb->select('partial e.{' . $fields . '}');
            }

            $results["entities"][] = $entity;
            $results["data"][$entity] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        }
        return new JsonResponse($results);
    }

    /**
     * Returns data formatted for use with a combobox.
     *
     * Extra filters can be added using query parameters such as <code>filter[someColumn]=someValue</code>.
     * @return JsonResponse Json array.
     * @example /combobox/adherent?fields=adherentId,adherentNom&filter[adherentDns.dnsUrl][value]=test&filter[adherentDns.dnsUrl][join]=inner
     *
     * @see BaseController::applyFiltersToQueryBuilder()
     *
     * @Route("/combobox/{entity}", name="carmen_ws_get_combobox", options={"expose"=true}, methods={"GET"})
     *
     */
    public function getDataForCombo(Request $request, $entity)
    {
        $fields = $request->query->get('fields', "");
        $filters = $request->query->get('filter', array());

        $qb = $this->createQueryBuilder('CarmenApiBundle:' . ucfirst($entity), 'e');
        $qb = $this->applyFiltersToQueryBuilder($qb, $filters);

        if ($fields != "") {
            // if some fields are requested, only retreive those ones
            $qb->select('partial e.{' . $fields . '}');
        }

        //echo $qb->getQuery()->getSQL();
        //var_dump($qb->getParameters()->toArray());

        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * Returns a list of Entities.
     *
     * Extra filters can be added using query parameters such as <code>filter[someColumn]=someValue</code>.
     * @param Request $request The request object.
     * @param string $id If set, returns a list with the requested Entity only (if exists).
     *
     * @return JsonResponse Json array.
     * @example /list/adherent to get all adherents, or /list/adherent/1 to get a sole adherent
     *
     * @see BaseController::getQueryBuilderList()
     *
     * @Route("/list/{entity}/{id}", name="carmen_ws_get_entities", defaults={"id"=null}, options={"expose"=true}, methods={"GET"})
     *
     */
    public function getEntitiesAction(Request $request, $entity, $id = null)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        $filters = $request->query->get('filter', array());
//
//        try {
//            $qb = $this->getQueryBuilderList('CarmenApiBundle:'.ucfirst($entity), 'e', $id, $filters);
//        } catch (\Exception $ex) {
//            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception"), $this->isProd() ? array() : array('exception'=>$ex->getMessage()));
//        }
//
//        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * Returns an entity.
     *
     * @Route("/get/{entity}/{id}", name="carmen_ws_get_entity", options={"expose"=true}, methods={"GET"})
     *
     * @param Request $request The request object.
     * @param string $id Identifier of the Entity.
     *
     * @return JsonResponse Json array.
     * @example /get/adherent to get all adherents, or /get/adherent/1 to get a sole adherent
     *
     */
    public function getEntityAction(Request $request, $entity, $id)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        try {
//            $qb = $this->getQueryBuilderList('CarmenApiBundle:'.ucfirst($entity), 'e', $id);
//        } catch (\Exception $ex) {
//            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception"), $this->isProd() ? array() : array('exception'=>$ex->getMessage()));
//        }
//
//        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * Creates or updates an Entity. POST data *must* conform to the entity's field names, not database columns definition.
     *
     * @Route("/post/{entity}/{id}", name="carmen_ws_post_entity", defaults={"id"=null}, options={"expose"=true}, methods={"POST", "PUT", "PATCH"})
     *
     * @param Request $request The request object.
     * @param string $id If set, updates an existing user, else creates it.
     *
     * @return JsonResponse Json object with the 'id' of the created/updated entity.
     * @example /post/adherent (+POST data) to create a new adherent, /post/adherent/1 (+POST data) to update an existing adherent
     *
     */
    public function postEntityAction(Request $request, $entity, $id = null)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        return $this->doGenericPostResponse($request, 'CarmenApiBundle:'.ucfirst($entity), $id);
    }

    /**
     * Deletes an Entity.
     *
     * @Route("/delete/{entity}/{id}", name="carmen_ws_delete_entity", options={"expose"=true}, methods={"DELETE"})
     *
     * @param string $id If set, updates an existing user, else creates it.
     *
     * @return Response Empty response if success, JsonResponse error if failed.
     */
    public function deleteEntityAction($entity, $id)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        try {
//            $entity = $this->getRepository('CarmenApiBundle:'.ucfirst($entity))->find($id);
//            if ( !$entity ) {
//                $ex = new EntityNotFoundException();
//                return $this->formatJsonError(Response::HTTP_NOT_FOUND, $this->_t("Not Found"), $this->_t($ex->getMessage()));
//            }
//            $this->getManager()->remove($entity);
//            $this->getManager()->flush();
//        } catch (\Exception $ex) {
//            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("API Exception")."[".__METHOD__."]", $this->isProd() ? array('exception'=>$ex->getMessage()) : array('exception'=>$ex->getMessage()));
//        }
//
//        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Restfull api to serve Entity.
     *
     * @Route("/data/{entity}/{id}", name="carmen_ws_entity", defaults={"id"=null}, options={"expose"=true}, methods={"GET","POST","PUT","DELETE"})
     *
     * @param Request $request
     * @param unknown $entity
     * @param unknown $id
     */
    public function dataRestAction(Request $request, $entity, $id = null)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        switch ($request->getMethod()) {
//            case 'GET':
//                return $this->getEntityAction($request, $entity, $id);
//                break;
//            case 'POST':
//            case 'PUT':
//                return $this->postEntityAction($request, $entity, $id);
//                break;
//            case 'DELETE':
//                return $this->deleteEntityAction($entity, $id);
//                break;
//        }
//
//        return $this->formatJsonError(Response::HTTP_METHOD_NOT_ALLOWED, $this->_t("Method not allowed"));
    }

    /**
     * Returns a list of Adherent.
     *
     * Extra filters can be added using query parameters such as 'filter[someColumn]=someValue'.
     * @param Request $request The request object.
     * @param string $id If set, returns a list with the requested Adherent only (if exists).
     *
     * @return JsonResponse Json array.
     * @todo change get/post route definition
     * @Route("/adherents/{id}", name="carmen_ws_get_adherents", requirements={"id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"GET"})
     *
     * @see BaseController::getQueryBuilderList()
     *
     */
    public function getAdherentAction(Request $request, $id = null)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        $filters = $request->query->get('filter', array());
//
//        $qb = $this->getQueryBuilderList('CarmenApiBundle:Adherent', 'a', $id, $filters/*, true*/);
//
//        return new JsonResponse($qb->getQuery()->getResult(Query::HYDRATE_ARRAY));
    }

    /**
     * @param Request $request The request object.
     * @param string $id If set, updates an existing user, else creates it.
     *
     * @return JsonResponse Json object.
     * @todo change get/post route definition
     * @Route("/adherents/{id}", name="carmen_ws_post_adherents", requirements={"id"="\d+"}, defaults={"id"=null}, options={"expose"=true}, methods={"POST", "PUT", "PATCH"})
     *
     */
    public function postAdherentAction(Request $request, $id = null)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        return $this->doGenericPostResponse($request, 'CarmenApiBundle:Adherent', $id);
    }

    /**
     * Shows an example of calling the Mapserver API and handle the result or error
     *
     * @Route("/demo/curl", name="carmen_ws_demo_curl", options={"expose"=true})
     *
     * @deprecated demo only...
     */
    public function demoCurlAction(Request $request)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        $result = $this->callMapserverApi('/api/map/filename', 'GET', array('field'=>'value'), array(), false);
//
//        if( !$result ) {
//            throw new \Exception('Mapserver API error: ' . $this->getMapserverApiLastErr());
//        }
//
//        return new Response(json_encode($result));
    }

    /**
     * Shows an example on how to handle backoffice database operations and a mapserver api call through a transaction
     *
     * @Route("/demo/transaction", name="carmen_ws_demo_transaction", options={"expose"=true})
     *
     * @deprecated demo only...
     */
    public function demoTransactionAction()
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        return $this->transactional(
//            function(EntityManager $em) {
//
//                $entity = new Adherent();
//                $entity->setAdherentNom('Test transaction');
//                //$entity->setAdherentDns('test'); // this should fail and throw an error, rollback shall be called
//                $em->persist($entity);
//
//                // callMapserverApi throws exception on error by default
//                $result = $this->callMapserverApi('/api/map/filename', 'GET', array('field'=>'value'));
//                // do something with $result...
//            }
//        );
    }

    /**
     * Shows an example on how to handle backoffice database operations and a mapserver api call through a transaction.
     *
     * This example uses BaseController::doGenericPost internally to handle a form submission.
     *
     * @Route("/demo/transaction2", name="carmen_ws_demo_transaction2", options={"expose"=true})
     *
     * @deprecated demo only...
     */
    public function demoTransaction2Action(Request $request)
    {
        return new Response(null, Response::HTTP_NOT_FOUND);
        // ATTENTION : Garder historique pour tests d'authentification
//        return $this->transactional(
//            function(EntityManager $em) use ($request){
//
//                // just simulaute some GET variables for doGenericPost to work...
//                $request->query->set('adherentNom', 'Test transaction 2');
//                $request->query->set('adherentNom', 'test'); // this should fail and throw an error, rollback shall be called
//
//                $this->doGenericPost($request, 'CarmenApiBundle:Adherent');
//
//                // callMapserverApi throws exception on error by default
//                $result = $this->callMapserverApi('/api/map/filename', 'GET', array('field'=>'value'));
//                // do something with $result...
//            }
//        );
    }

    /**
     * Returns data formatted for use with a combobox.
     *
     * Extra filters can be added using query parameters such as <code>filter[someColumn]=someValue</code>.
     * @param string $requestUri The requested method of the API (should start with a /).
     * @param string $method Accepted methods: GET, POST, PUT, DELETE (GET by default).
     * @param array $queryData GET data
     * @param array $requestData POST data
     * @param boolean $bThrow Throws an exception on error, instead of returning false (true by default)
     *
     * @return JsonResponse Json array.
     *
     * Call a Mapserver API method through cURL.
     *
     * @return mixed The response as a json array, or false if curl failed or the response is not a valid json string, or the response is an error.
     *
     * @throws ApiException In case of error (see key 'description'), and only if $bThrow is true (otherwise, see the return statement)
     * @see BaseController::applyFiltersToQueryBuilder()
     *
     * @Route("/mapserver/helpers/geometrytype/{layertype}/{layerfile}/{encoding}", name="carmen_ws_helpers_geometrytype", options={"expose"=true}, defaults={"encoding"="data"}, methods={"GET", "PUT", "POST"}, requirements={"layerfile"="[-_/%.a-zA-Z0-9]+"})
     * @Route("/mapserver/helpers/getsheets/{layerfile}", name="carmen_ws_helpers_getsheets", options={"expose"=true}, methods={"GET", "PUT", "POST"}, requirements={"layerfile"="[-_/%.a-zA-Z0-9]+"})
     * @Route("/mapserver/helpers/fields/{layertype}/{layerfile}/{encoding}/{layersheet}", name="carmen_ws_helpers_layerfields", options={"expose"=true}, defaults={"layersheet"=null, "encoding"="data"}, methods={"GET", "PUT", "POST"}, requirements={"layerfile"="[-_/%.a-zA-Z0-9]+"})
     * @Route("/mapserver/helpers/getLayerExtent/{mapfile}/{layer}/{projection}", name="carmen_ws_helpers_layerextent", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/symbology/{mapfile}/{layer}/{analyse}/{geometrytype}", name="carmen_ws_helpers_symbology", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/distribution/{mapfile}/{layer}/{analyse}/{geometrytype}", name="carmen_ws_helpers_distribution", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/generate/symbol/{geometrytype}/{symbolset}", name="carmen_ws_helpers_generatesymbol", options={"expose"=true},  methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/library/symbols/{geometrytype}/{symbolset}", name="carmen_ws_helpers_symbolslibrary", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/symbolset/concat/{mapfile}/{mapname}/{from_symbolset}/{with_symbolset}", name="carmen_ws_helpers_concat_symbolset", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/symbolset/reset/{mapfile}/{mapname}", name="carmen_ws_helpers_reset_symbolset", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
     * @Route("/mapserver/helpers/getInformation/{mapfile}", name="carmen_ws_helpers_getinformation", options={"expose"=true}, requirements={"moreArgs"="[-_%.a-zA-Z0-9]+"}, methods={"GET", "PUT", "POST"})
   
 *
     * @see BaseController::curl()
     *
     */
    public function callMapserverHelpers(Request $request)
    {
        $method = $request->getMethod();
        $pathInfo = str_replace("/api/mapserver", "", $request->getPathInfo());
        $params = array_merge(array("directory" => $this->getMapfileDirectory()), ($method == "GET" ? $request->query->all() : $request->request->all()));
        //var_dump($pathInfo, $params, $request->getMethod());
        try {
            $result = $this->callMapserverApi(
                $pathInfo,
                $method,
                ($method == "GET" ? $params : array()),
                ($method != "GET" ? $params : array())
            );
            $this->getLogger()->debug(__METHOD__, (array)$result);
            if (!is_bool($result)) {
                return $this->formatJsonSuccess($result);
            }
        } catch (ApiException $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("data" => $exception->getData(), "stack" => $exception->getTraceAsString()));
        } catch (\Exception $exception) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, $this->_t("Internal Server Error"), $this->_t("Mapserver Helper Exception") . ' : ' . $exception->getMessage(), array("stack" => $exception->getTraceAsString()));
        }
    }
}
