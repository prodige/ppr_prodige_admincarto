<?php

namespace Carmen\ApiBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Carmen\ApiBundle\Entity\Users;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;

//use Carmen\ApiBundle\Entity\Adherent;
//use Doctrine\ORM\Query;
//use Doctrine\ORM\EntityNotFoundException;
//use Doctrine\ORM\EntityManager;
//use Carmen\ApiBundle\Entity\Layer;

/**
 * File browser API Controller.
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/api")
 */
class FileBrowserController extends BaseController
{
    // base path of file browser
    protected $basePath;

    protected $logger;

    /**
     * Restfull api to file browser.
     *
     * @Route("/filebrowser/rest/{routing}/{action}",
     *        name="carmen_ws_filebrowser",
     *        requirements={"action"="[-_%.a-zA-Z0-9]+", "routing"="noroute|absolute|relative"},
     *        defaults={"action"=""},
     *        options={"expose"=true},
     *        methods = {"GET","POST","PUT","PATCH","DELETE"})
     *
     * @param Request $request  request object
     * @param string  $action   action id
     * @return Response with contents in json
     */
    public function fileBrowserRestAction(Request $request, $routing, $action="")
    {
        $this->logger = $this->getLogger();

        $Actions = array(
            "get-files"                => array("GET", "POST"),
            "download-file"            => array("POST"),
            //"move-file"              => array("POST"),
            //"rename-file"            => array("POST"),
            //"delete-file"            => array("POST", "DELETE"),
            "get-folders"              => array("GET", "POST"),
            //"create-folder"          => array("POST"),
            //"rename-folder"          => array("POST"),
            //"delete-folder"          => array("POST", "DELETE"),
            "upload-file"              => array("POST"),
            "upload-files"             => array("POST"),
            //"get-upload-progress-name" => array("GET", "POST"),
            //"get-upload-progress"      => array("GET", "POST"),

        );

        if( $action === "" ) {
            $action = $_GET["action"];
        }

        $action = strtolower($action);

        if( !array_key_exists($action, $Actions) ) {
            return $this->responseJson(false, $this->_t('Unknown action'));
        }

        $method = $request->getMethod();
        if( $method == "PUT" || $method == "PATCH" ) {
            $method = "POST";
        }
        if( !in_array($method, $Actions[$action]) ) {
            return $this->responseJson(false, $this->_t("Method not allowed for this action"));
        }

        $tmpNames = explode("-", $action);
        $actionName = $tmpNames[0];
        for($i=1; $i<count($tmpNames); $i++) {
            $actionName .= ucwords($tmpNames[$i]);
        }
        $actionName .= "Action";

        $this->basePath = rtrim(realpath($this->getAccountDirectory()), "/")."/";

        $this->logger->debug(__METHOD__." call ".$action." -> ".$actionName." basepatch : ".$this->basePath);
        $this->logger->debug(__METHOD__." call ".$action." -> ".$actionName." with parameters : ".print_r($request->query->all(), true));

        return call_user_func(array($this, $actionName), $request, $routing);
    }

    /**
     * Return subfolders list of folder
     *
     * @Route("/filebrowser/get-folders",
     *        name="carmen_ws_filebrowser_getfolders",
     *        methods = {"GET","POST","PUT","PATCH"})
     *
     * @param Request $request  request object
     * @return Response with contents in json
     */
    protected function getFoldersAction(Request $request) {

        $requestedPath = str_replace('/Root', '', $request->get('path'));         // selected path
        $defaultPath   = str_replace('/Root', '', $request->get('defaultPath', '/Root'));  // used value for matching with $this->basePath
        $relativeRoot  = str_replace('/Root', '', $request->get('relativeRoot', '/Root')); // position

        $requestedPath = urldecode($defaultPath.str_replace($defaultPath, '', $requestedPath));
        $browsePath = $this->basePath.ltrim($requestedPath, "/");

        $basePath = ( $relativeRoot==$requestedPath ? "" : str_replace($relativeRoot."/", "", $requestedPath)."/" );

        if( !$this->checkPath($browsePath) ) {
            return $this->responseJson(false, $this->_t("You don't have access to this path ".$requestedPath." ".$browsePath));
        } else {
            $browsePath = realpath($browsePath)."/";
        }

        $dirs = array();
        $iterator = new \DirectoryIterator($browsePath);

        $ext = $request->get('extension');
        $patterns = explode(",", $ext);
        $patterns = array_map("trim", $patterns);
        $patterns = array_diff($patterns, array(""));
        $patterns = array_merge(array_map("strtoupper", $patterns), array_map("strtolower", $patterns));

        foreach($iterator as $item) {
            if(!$item->isDot() && $item->isDir()) {
                $dirs[(string) $item] = array(
                    'text' => (string) $item,
                    'name' => (string) $item,
                    'size' => 0,
                    'type' => "Répertoire",
                    'full_path_name' => $basePath.((string) $item),
                    'leaf' => 0,
                    'relative' => $relativeRoot,
                    'requestPath' => $requestedPath,
                    'date_modified' => $item->getMTime()
                );
                if ( !empty($patterns) ){
                    $files = array();
                    foreach($patterns as $pattern)$files = array_merge($files, glob($item->getPathname()."/".$pattern));
                    if ( count($files)==0 ) {
                        unset($dirs[(string) $item]);
                        continue;
                    }
                    $dirs[(string) $item]["size"] = 1;
                    $plural = (count($files)>1 ? "s" : "");
                    $dirs[(string) $item]["type"] = count($files)." fichier{$plural} compatible{$plural}";
                }
            }
        }

        uksort($dirs, 'strcasecmp');

        $status = 200;
        $data = array_values($dirs);

        return new JsonResponse($data, $status);
    }

    /**
     * Return files list of folder
     *
     * @Route("/filebrowser/get-files",
     *        name="carmen_ws_filebrowser_getfiles",
     *        methods = {"GET","POST","PUT","PATCH"})
     *
     * @param Request $request  request object
     * @return Response with contents in json
     */
    protected function getFilesAction(Request $request, $routing="noroute") {

        $requestedPath = str_replace('/Root', '', $request->get('path'));         // selected path  /IHM
        $defaultPath   = str_replace('/Root', '', $request->get('defaultPath', '/Root'));  // used value for matching with $this->basePath -> ""
        $relativeRoot  = str_replace('/Root', '', $request->get('relativeRoot', '/Root')); // position -> "/IHM"

        $requestedPath = urldecode($defaultPath.str_replace($defaultPath, '', $requestedPath));

        $browsePath = $this->basePath.$requestedPath;

        $basePath = ( $relativeRoot==$requestedPath ? "" : str_replace($relativeRoot."/", "", $requestedPath)."/" );

        if( !$this->checkPath($browsePath) ) {
            return $this->responseJson(false, $this->_t("You don't have access to this path"));
        } else {
            $browsePath = realpath($browsePath)."/";
        }
        //$defaultPath = /Root
        //$defaultPath = /Root/Publication
        //$requestedPath = /Root/Publication/data/inpn/referentiels
        //$browsePath = /home/devperso/martigny/BRGM/carmen_mapserver/web/data/MNHM/Publication/data/inpn/referentiels/
        //$basePath = data/inpn/referentiels/

        $excludePatterns = "!^temp_admin_.+\.json$!";
        $ext = $request->get('extension');

        $patternExt = "";
        if( $ext != "" ) {
            $patternExt = str_replace(",", "|", $ext);
            $patternExt = str_replace(".", "\.", $patternExt);
            $patternExt = str_replace("#", "\#", $patternExt);
            $patternExt = str_replace("~", "\~", $patternExt);
            $patternExt = str_replace("*", ".*", $patternExt);
            $patternExt = "/^(".$patternExt.")$/i";
        }

        $rowClasses = array(
            '7z'    => array('ux-filebrowser-icon-archive-file',      $this->_t("Compressed file (.%ext%)", array("%ext%" => "7zip"))),
            'aac'    => array('ux-filebrowser-icon-audio-file',        $this->_t("Audio file (.%ext%)",      array("%ext%" => "aac"))),
            'ai'    => array('ux-filebrowser-icon-vector-file',       $this->_t("Image file (.%ext%)",      array("%ext%" => "ai"))),
            'avi'    => array('ux-filebrowser-icon-video-file',        $this->_t("Video file (.%ext%)",      array("%ext%" => "avi"))),
            'bmp'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "bmp"))),
            'divx'    => array('ux-filebrowser-icon-video-file',        $this->_t("Video file (.%ext%)",      array("%ext%" => "divx"))),
            'doc'    => array('ux-filebrowser-icon-document-file',     $this->_t("%type% document (.%ext%)", array("%type%" => "Word", "%ext%" => "doc"))),
            'docx'    => array('ux-filebrowser-icon-document-file',     $this->_t("%type% document (.%ext%)", array("%type%" => "Word", "%ext%" => "docx"))),
            'odt'    => array('ux-filebrowser-icon-presentation-file', $this->_t("%type% document (.%ext%)", array("%type%" => "LibreOffice", "%ext%" => "odt"))),
            'eps'    => array('ux-filebrowser-icon-vector-file',       $this->_t("%type% document (.%ext%)", array("%type%" => "Postscript", "%ext%" => "eps"))),
            'flac'    => array('ux-filebrowser-icon-audio-file',        $this->_t("Audio file (.%ext%)",      array("%ext%" => "flac"))),
            'flv'    => array('ux-filebrowser-icon-video-file',        $this->_t("Video file (.%ext%)",      array("%ext%" => "flv"))),
            'gif'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "gif"))),
            'jpg'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "jpg"))),
            'jpeg'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "jpeg"))),
            'mov'    => array('ux-filebrowser-icon-video-file',        $this->_t("Video file (.%ext%)",      array("%ext%" => "mov"))),
            'mp3'    => array('ux-filebrowser-icon-audio-file',        $this->_t("Audio file (.%ext%)",      array("%ext%" => "mp3"))),
            'mpg'    => array('ux-filebrowser-icon-video-file',        $this->_t("Video file (.%ext%)",      array("%ext%" => "mpg"))),
            'pdf'    => array('ux-filebrowser-icon-acrobat-file',      $this->_t("%type% document",          array("%type%" => "PDF"))),
            'png'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "png"))),
            'pps'    => array('ux-filebrowser-icon-presentation-file', $this->_t("%type% document (.%ext%)", array("%type%" => "PowerPoint", "%ext%" => "pps"))),
            'ppt'    => array('ux-filebrowser-icon-presentation-file', $this->_t("%type% document (.%ext%)", array("%type%" => "PowerPoint", "%ext%" => "ppt"))),
            'pptx'    => array('ux-filebrowser-icon-presentation-file', $this->_t("%type% document (.%ext%)", array("%type%" => "PowerPoint", "%ext%" => "pptx"))),
            'odp'    => array('ux-filebrowser-icon-presentation-file', $this->_t("%type% document (.%ext%)", array("%type%" => "LibreOffice", "%ext%" => "odp"))),
            'rar'    => array('ux-filebrowser-icon-archive-file',      $this->_t("Compressed file (.%ext%)", array("%ext%" => "rar"))),
            'psd'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "psd"))),
            'svg'    => array('ux-filebrowser-icon-vector-file',       $this->_t("%type% file",              array("%type%" => "SVG"))),
            'swf'    => array('ux-filebrowser-icon-flash-file',        $this->_t("%type% file (.%ext%)",     array("%type%" => "Adobe Flash", "%ext%" => "swf"))),
            'tif'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "tif"))),
            'tiff'    => array('ux-filebrowser-icon-image-file',        $this->_t("Image file (.%ext%)",      array("%ext%" => "tiff"))),
            'txt'    => array('ux-filebrowser-icon-text-file',         $this->_t("Text document (.%ext%)",   array("%ext%" => "txt"))),
            'wav'    => array('ux-filebrowser-icon-audio-file',        $this->_t("Audio file (.%ext%)",      array("%ext%" => "wav"))),
            'wma'    => array('ux-filebrowser-icon-video-file',        $this->_t("Video file (.%ext%)",      array("%ext%" => "wma"))),
            'xls'    => array('ux-filebrowser-icon-spreadsheet-file',  $this->_t("%type% document (.%ext%)", array("%type%" => "Excel", "%ext%" => "xls"))),
            'xlsx'    => array('ux-filebrowser-icon-spreadsheet-file',  $this->_t("%type% document (.%ext%)", array("%type%" => "Excel", "%ext%" => "xlsx"))),
            'ods'    => array('ux-filebrowser-icon-presentation-file', $this->_t("%type% document (.%ext%)", array("%type%" => "LibreOffice", "%ext%" => "ods"))),
            'zip'    => array('ux-filebrowser-icon-archive-file',      $this->_t("Compressed file (.%ext%)", array("%ext%" => "zip"))),

            'map'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "Map", "%ext%" => "map"))),
            'ttf'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "True Type Font", "%ext%" => "ttf"))),
            'sym'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "Symbol", "%ext%" => "sym"))),

            'shp'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "Shape", "%ext%" => "shp"))),
            'tab'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "MapInfo", "%ext%" => "tab"))),
            'gpkg'   => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "Geopackage", "%ext%" => "gpkg"))),
            'mif'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file (.%ext%)",     array("%type%" => "MapInfo MIF/MID", "%ext%" => "mif"))),
            'ecw'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file",              array("%type%" => "ECW"))),
            'gtiff'    => array('ux-filebrowser-icon-archive-file',      $this->_t("%type% file",              array("%type%" => "GeoTiff"))),

        );
        $account_path = null;
        if ( $routing!="noroute" ){
            $user = $this->getUser();
            if ( $user && $user instanceof Users){
                $account = $user->getAccount();
                if ( $account ){
                    $account_path = ($account->getAccountPath() ?: null);
                }
            }
        }
        $route = null;
        if ( $account_path ){
            $route = $this->generateUrl("carmen_data_account_ihm_file", array("account_path"=>$account_path, "file"=>"%file%"), constant("Symfony\Component\Routing\Generator\UrlGeneratorInterface::".strtoupper($routing)."_PATH"));

            $kernel = $this->get('kernel');
            $kernel instanceof Kernel;
            if ( $kernel->getEnvironment()=="dev" ){
                //$route = str_replace("app_dev.php/", "", $route);
            }

        }
        $files = array();
        $iterator = new \DirectoryIterator($browsePath);
        foreach($iterator as $item) {
            $fileName = (string) $item;
            if( $item->isFile()
                && ($patternExt=="" || @preg_match($patternExt, $fileName))
                && !preg_match($excludePatterns, $fileName)
            ) {
                $ext = strtolower(pathinfo($item->getFilename(), PATHINFO_EXTENSION));
                $bExt = array_key_exists($ext, $rowClasses);

                $type = ( $bExt
                    ? $rowClasses[$ext][1]
                    : $this->_t("%type% file", array("%type%" => strtoupper($ext))) );

                $files[$fileName] = array(
                    'name' => $fileName,
                    'size' => $item->getSize(),
                    'type' => $type,
                    'full_path_name' => $basePath.$fileName,
                    'relative' => $relativeRoot,
                    'requestPath' => $requestedPath,
                    'date_modified' => $item->getMTime()
                );
                if ( $route ){
                    $files[$fileName]["route"] = str_replace(urlencode("%file%"), basename($fileName), $route);
                }

                /*if( $bExt ) {
                    $files[(string) $item]['row_class'] = $rowClasses[$ext][0];
                }*/

                /*$thumb = 'file_' . $ext . '.png';
                if(file_exists('media/icons/48/' . $thumb) == true) {
                    $files[(string) $item]['thumbnail'] = 'media/icons/48/' . $thumb;
                }*/
            }
        }

        uksort($files, 'strcasecmp');
        $status = 200;
        $data = array_values($files);

        return new JsonResponse($data, $status);
    }

    /**
     * delete files list
     * @param Request $request  request object
     * @return Response with contents in json
     *
    protected function deleteFileAction(Request $request) {

    $files = $request->get('files'); //$_POST['files'];
    $deletePath = preg_replace('/^\/Root\/?/', $this->basePath, $requestedPath);

    $success = true;
    $failCount = 0;
    $message = '';
    $data = array(
    'successful' => array(),
    'failed' => array()
    );

    foreach($files as $recordId => $file) {

    $filePath = preg_replace('/^\/Root\/?/', $this->basePath, $file);
    try {
    if(file_exists($filePath) == false) {
    throw new Exception($this->_t("The file '%filePath%' does not exist", array("%filePath%" => basename($filePath))));
    }
    if(is_file($filePath) == false) {
    throw new Exception($this->_t("'%filePath%' is not a file", array("%filePath%" => htmlspecialchars($filePath))));
    }
    if(is_writable($filePath) == false) {
    throw new Exception("You do not have permission to delete '%filePath%'", array("%filePath%" => basename($filePath)));
    }
    if(@unlink($filePath) == false) {
    throw new Exception($this->_t("Could not delete '%filePath%' for unknow reason", array("%filePath%" => basename($filePath))));
    } else {
    $data['successful'][] = array(
    'recordId' => $recordId
    );
    }
    } catch(Exception $e) {
    $success = false;
    $failCount++;
    $data['failed'][] = array(
    'recordId' => $recordId,
    'reason' => $e->getMessage()
    );
    }
    }

    if($failCount > 0) {
    $message = $this->_t('Failed to delete %failCount% file(s)', array("%failCount%" => (integer) $failCount));
    }

    $data = array(
    'success'    => $success,
    'message'    => $message,
    'data'        => $data
    );
    return $this->formatJsonSuccess($data);
    }*/

    /**
     * rename file
     * @param Request $request  request object
     * @return Response with contents in json
     *
    protected function renameFileAction(Request $request) {

    $requestedPath = $request->get('path'); //$_POST['path'];
    $oldName = $request->get('oldName'); //$_POST['oldName'];
    $newName = $request->get('newName'); //$_POST['newName'];
    $renamePath = preg_replace('/^\/Root\/?/', $this->basePath, $requestedPath);

    $success = true;
    $message = '';

    try {
    if(file_exists($renamePath . $oldName) == false) {
    throw new Exception($this->_t("The file '%oldName%' does not exist", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(is_file($renamePath . $oldName) == false) {
    throw new Exception($this->_t("'%oldName%' is not a file", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(is_writable($renamePath . $oldName) == false) {
    throw new Exception($this->_t("You do not have permission to rename '%oldName%'", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(file_exists($renamePath . $newName) == true) {
    throw new Exception($this->_t("Could not rename '%oldName%'. A file or folder with the specified name already exists", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(@rename($renamePath . $oldName, $renamePath . $newName) == false) {
    throw new Exception($this->_t("Could not rename '%oldName%' for unknow reason", array("%oldName%" => htmlspecialchars($oldName))));
    }
    } catch(Exception $e) {
    $success = false;
    $message = $e->getMessage();
    }

    $data = array(
    'success'    => $success,
    'message'    => $message
    );
    return $this->formatJsonSuccess($data);
    }*/

// NOT USED
//    /**
//     * download file : return file on standart output
//     * @param Request $request  request object
//     */
//    protected function downloadFileAction(Request $request) {
//
//        $requestedPath = $request->get('path'); //$_POST['path'];
//        $defaultPath   = $request->get('defaultPath', '/Root');
//        $relativeRoot  = $request->get('relativeRoot', '/Root');
//
//        $requestedPath = urldecode(dirname($relativeRoot).$requestedPath);
//        $downloadPath = str_replace($defaultPath, $this->basePath, $requestedPath);
//
//        try {
//            if(file_exists($downloadPath) == false) {
//                throw new Exception($this->_t("The file '%downloadPath%' does not exist", array("%downloadPath%" => basename($downloadPath))));
//            }
//            if(is_file($downloadPath) == false) {
//                throw new Exception($this->_t("'%requestedPath%' is not a file", array("%requestedPath%" => htmlspecialchars($requestedPath))));
//            }
//            if(is_readable($downloadPath) == false) {
//                throw new Exception($this->_t("You do not have permission to download '%downloadPath%'", array("%downloadPath%" => basename($downloadPath))), 1);
//            }
//
//            $response = new Response();
//            $response->headers->setPublic(true); //->set('Pragma', 'public');
//            $response->headers->setExpires(new DateTime('@'.(time()-60))); //set('Expires', '0');
//            $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
//            $response->headers->set('Content-Description', 'File Transfer');
//            $response->headers->set('Content-Type',  'application/force-download');
//            $response->headers->set('Content-Disposition', $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,  basename($downloadPath)));
//            $response->headers->set('Content-Transfer-Encoding', 'binary');
//            $response->headers->set('Content-Length', filesize($downloadPath));
//            $response->send();
//
//            /*header('Pragma: public');
//            header('Expires: 0');
//            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
//            header('Content-Description: File Transfer');
//            header('Content-Type: application/force-download');
//            header('Content-Disposition: attachment; filename="' . basename($downloadPath) . '"');
//            header('Content-Transfer-Encoding: binary');
//            header('Content-Length: ' . filesize($downloadPath));*/
//
//            readfile($downloadPath);
//        } catch(Exception $e) {
//            $response = new Response();
//            if($e->getCode() === 1) {
//                $response->setStatusCode(403, "Forbidden");
//                //header('HTTP/1.1 403 Forbidden');
//            } else {
//                $response->setStatusCode(404, "Not Found");
//                //header('HTTP/1.1 404 Not Found');
//            }
//            $response->setContent($e->getMessage());
//        }
//
//        exit;
//    }

    /**
     * Move file
     * @param Request $request  request object
     * @return Response with contents in json
     *
    protected function moveFileAction(Request $request) {

    $sourcePath = $request->get('sourcePath'); //$_POST['sourcePath'];
    $destinationPath = $request->get('destinationPath'); //$_POST['destinationPath'];
    $files = $request->get('files'); //$_POST['files'];
    $overwrite = $request->get('overwrite'); //$_POST['overwrite'];
    $overwrite = ($overwrite === 'true') ? true : false;

    $sourceFolder = preg_replace('/^\/Root\/?/', $this->basePath, $sourcePath) . '/';
    $destinationFolder = preg_replace('/^\/Root\/?/', $this->basePath, $destinationPath) . '/';

    $success = true;
    $failCount = 0;
    $message = '';
    $data = array(
    'successful' => array(),
    'failed' => array(),
    'existing' => array()
    );

    foreach($files as $recordId => $fileName) {

    $from = $sourceFolder . $fileName;
    $to = $destinationFolder . $fileName;

    try {

    if(file_exists($from) == false) {
    throw new Exception($this->_t("The file '%fileName%' does not exist", array("%fileName%" => htmlspecialchars($fileName))));
    }
    if(is_file($from) == false) {
    throw new Exception($this->_t("'%fileName%' is not a file", array("%fileName%" => htmlspecialchars($fileName))));
    }
    if(is_readable($from) == false) {
    throw new Exception($this->_t("'%fileName%' is not readable", array("%fileName%" => htmlspecialchars($fileName))));
    }

    if(file_exists($destinationFolder) == false) {
    throw new Exception($this->_t("The destination folder '%destinationPath%' does not exist", array("%destinationPath%" => htmlspecialchars($destinationPath))));
    }
    if(is_dir($destinationFolder) == false) {
    throw new Exception($this->_t("The destination folder '%destinationPath%' is not a directory", array("%destinationPath%" => htmlspecialchars($destinationPath))));
    }
    if(is_writable($destinationFolder) == false) {
    throw new Exception($this->_t("You do not have permission to move files to '%destinationPath%'", array("%destinationPath%" => htmlspecialchars($destinationPath))));
    }

    if($overwrite === true) {
    if(file_exists($to) == true && is_writable($to) == false) {
    throw new Exception($this->_t("You do not have permission to overwrite '%fileName%'", array("%fileName%" => htmlspecialchars($fileName))));
    }
    } else {
    if(file_exists($to) == true) {
    throw new Exception($this->_t("Could not move '%fileName%'. A file with the same name in the destination folder already exists", array("%fileName%" => htmlspecialchars($fileName))), 1);
    }
    }

    if(@rename($from, $to) == false) {
    throw new Exception($this->_t("Could not move '%fileName%' for unknown reason", array("%fileName%" => htmlspecialchars($fileName))));
    } else {
    $data['successful'][] = array(
    'recordId' => $recordId
    );
    }

    } catch(Exception $e) {
    $success = false;
    $failCount++;

    if($e->getCode() === 1) {
    $data['existing'][] = array(
    'recordId' => $recordId,
    'reason' => $e->getMessage()
    );
    } else {
    $data['failed'][] = array(
    'recordId' => $recordId,
    'reason' => $e->getMessage()
    );
    }
    }
    }

    if($failCount > 0) {
    $message = $this->_t('Failed to move %failCount% file(s)', array("%failCount%" => (integer) $failCount));
    }

    $data = array(
    'success'    => $success,
    'message'    => $message,
    'data'        => $data
    );
    return $this->formatJsonSuccess($data);
    }*/

    /**
     * Create folder
     * @param Request $request  request object
     * @return Response with contents in json
     *
    protected function createFolderAction(Request $request) {

    $requestedPath = $request->get('path'); //$_POST['path'];
    $createPath = preg_replace('/^\/Root\/?/', $this->basePath, $requestedPath);

    $success = true;
    $message = '';

    try {
    $parentPath = realpath( realpath($createPath) . '/../');
    if(file_exists($parentPath) == false) {
    throw new Exception($this->_t("Could not create '%requestedPath%', the parent folder does not exist", array("%requestedPath%" => htmlspecialchars($requestedPath))));
    }
    if(is_dir($parentPath) == false) {
    throw new Exception($this->_t("Could not create '%requestedPath%', the parent folder is not a directory", array("%requestedPath%" => htmlspecialchars($requestedPath))));
    }
    if(is_writable($parentPath) == false) {
    throw new Exception($this->_t("You do not have permission to create '%createPath%'", array("%createPath%" => htmlspecialchars($createPath))));
    }
    if(file_exists($createPath) == true) {
    throw new Exception($this->_t("Could not create '%createPath%'. A file or folder with the specified name already exists", array("%createPath%" => htmlspecialchars($createPath))));
    }
    if(@mkdir($createPath) == false) {
    throw new Exception($this->_t("Could not create '%requestedPath%' for unknow reason", array("%requestedPath%" => htmlspecialchars($requestedPath))));
    }
    } catch(Exception $e) {
    $success = false;
    $message = $e->getMessage();
    }

    $data = array(
    'success'    => $success,
    'message'    => $message
    );
    return $this->formatJsonSuccess($data);
    }*/

    /**
     * rename folder
     * @param Request $request  request object
     * @return Response with contents in json
     *
    protected function renameFolderAction(Request $request) {

    $requestedPath = $request->get('path'); //$_POST['path'];
    $oldName = $request->get('oldName'); //$_POST['oldName'];
    $newName = $request->get('newName'); //$_POST['newName'];
    $renamePath = preg_replace('/^\/Root\/?/', $this->basePath, $requestedPath) . '/';

    $success = true;
    $message = '';

    try {
    if(file_exists($renamePath . $oldName) == false) {
    throw new Exception($this->_t("The folder '%oldName%' does not exist", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(is_dir($renamePath . $oldName) == false) {
    throw new Exception($this->_t("'%oldName%' is not a directory", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(is_writable($renamePath . $oldName) == false) {
    throw new Exception($this->_t("You do not have permission to rename '%oldName%'", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(file_exists($renamePath . $newName) == true) {
    throw new Exception($this->_t("Could not rename '%oldName%'. A file or folder with the specified name already exists", array("%oldName%" => htmlspecialchars($oldName))));
    }
    if(@rename($renamePath . $oldName, $renamePath . $newName) == false) {
    throw new Exception($this->_t("Could not rename '%oldName%' for unknow reason", array("%oldName%" => htmlspecialchars($oldName))));
    }
    } catch(Exception $e) {
    $success = false;
    $message = $e->getMessage();
    }

    $data = array(
    'success'    => $success,
    'message'    => $message
    );
    return $this->formatJsonSuccess($data);
    }*/

    /**
     * delete folder
     * @param Request $request  request object
     * @return Response with contents in json
     *
    protected function deleteFolderAction(Request $request) {

    $requestedPath = $request->get('path'); //$_POST['path'];
    $deletePath = preg_replace('/^\/Root\/?/', $this->basePath, $requestedPath);

    $success = true;
    $message = '';

    try {
    if(file_exists($deletePath) == false) {
    throw new Exception($this->_t("The folder '%path%' does not exist", array("%path%" => htmlspecialchars($requestedPath))));
    }
    if(is_dir($deletePath) == false) {
    throw new Exception($this->_t("'%path%' is not a directory", array("%path%" => htmlspecialchars($requestedPath))));
    }
    if(is_writable($deletePath) == false) {
    throw new Exception($this->_t("You do not have permission to delete '%path%'", array("%path%" => htmlspecialchars($requestedPath))));
    }
    if(count(scandir($deletePath)) > 2) {
    throw new Exception($this->_t("Could not delete '%path%', the folder is not empty", array("%path%" => htmlspecialchars($requestedPath))));
    }
    if(@rmdir($deletePath) == false) {
    throw new Exception($this->_t("Could not delete '%path%' for unknown reason", array("%path%" => htmlspecialchars($requestedPath))));
    }
    } catch(Exception $e) {
    $success = false;
    $message = $e->getMessage();
    }

    $data = array(
    'success'    => $success,
    'message'    => $message
    );
    return $this->formatJsonSuccess($data);
    }*/

    /**
     * return upload progress name configured in php.ini
     *
     * @Route("/filebrowser/get-upload-progress-name",
     *        name="carmen_ws_filebrowser_getuploadprogressname",
     *        methods = {"GET","POST","PUT","PATCH"})
     *
     * @param Request $request  request object
     *
    protected function getUploadProgressNameAction(Request $request) {

    $uploadProgress = ini_get("session.upload_progress.enabled");
    $sessionName    = ini_get("session.upload_progress.name");
    $bSession = ( $uploadProgress=="1" && session_status() === PHP_SESSION_ACTIVE );

    $data = array(
    'success'    => $bSession,
    'message'    => ( bSession ? $sessionName : "" )
    );
    $this->logger->debug(__METHOD__."::sessionName data : ".$sessionName." ".session_status()." ".PHP_SESSION_ACTIVE." ".$uploadProgress);
    return $this->formatJsonSuccess($data);
    }*/

    /**
     * return progress of upload using session php
     *
     * @Route("/filebrowser/get-upload-progress",
     *        name="carmen_ws_filebrowser_getuploadprogress",
     *        methods = {"GET","POST","PUT","PATCH"})
     *
     * @param Request $request  request object
     *
    protected function getUploadProgressAction(Request $request) {

    $sessionPrefix  = ini_get("session.upload_progress.prefix");
    $sessionName    = ini_get("session.upload_progress.name");
    $uploadSessionName = $sessionPrefix.$request->get($sessionName);

    $this->logger->debug(__METHOD__."session : all => ".print_r($_SESSION, true));
    $this->logger->debug(__METHOD__." session : ".$uploadSessionName." => ".( isset($_SESSION[$uploadSessionName]) ? print_r($_SESSION[$uploadSessionName], true) : "not found"));

    $data = array(
    'success'    => isset($_SESSION[$uploadSessionName]),
    'message'    => ( isset($_SESSION[$uploadSessionName]) ? $_SESSION[$uploadSessionName] : array() )
    );
    $this->logger->debug(__METHOD__."::progressInfo data : ".print_r($data, true));

    return $this->formatJsonSuccess($data);
    }*/

    /**
     * upload file : return array with name, extension and size of file
     *
     * Route("/filebrowser/upload-files",
     *        name="carmen_ws_filebrowser_uploadfile",
     *        methods = {"POST","PUT","PATCH"})
     *
     * @param Request $request  request object
     *
    protected function uploadFilesAction(Request $request) {

    // do upload
    $requestedPath = $request->get('path');
    $browsePath = preg_replace('/^\/Root\/?/', $this->basePath, $requestedPath);
    if( !$this->checkPath($browsePath) ) {
    $data = array(
    'success'    => false,
    'message'    => $this->_t("You don't have access to this path")
    );
    return $this->formatJsonSuccess($data);
    } else {
    $browsePath = realpath($browsePath)."/";
    }

    $ext = $request->get('extension');

    $patternExt = "";
    if( $ext != "" ) {
    $patternExt = str_replace(",", "|", $ext);
    $patternExt = str_replace(".", "\.", $patternExt);
    $patternExt = str_replace("#", "\#", $patternExt);
    $patternExt = str_replace("~", "\~", $patternExt);
    $patternExt = str_replace("*", ".*", $patternExt);
    $patternExt = "/^".$patternExt."$/";
    }
    $this->logger->debug(__METHOD__." data : pattern : ".$ext." ".$patternExt);

    $uploadFiles = $request->files->get('file');

    $status = 200;
    $data = array();
    if( $uploadFiles instanceof UploadedFile ) {
    if( $uploadFiles->isValid() ) {
    $data["name"] = $uploadFiles->getClientOriginalName();
    $data["extension"] = $uploadFiles->getClientOriginalExtension();
    $data["size"] = $uploadFiles->getClientSize();
    if( $patternExt==""
    || $patternExt!="" && @preg_match($patternExt, $data["name"])==1 )  {
    try {
    $uploadFiles->move($browsePath, $data["name"]);
    } catch( FileException $e ) {
    $data = array("error" => $uploadFiles->getErrorMessage());
    }
    } else {
    $data = array("error" => 'No file was uploaded.');
    }
    } else {
    $data = array("error" => 'Extension of file was not authorized.');
    }
    } else {
    $data = array("error" => 'No file was uploaded.');
    }

    $this->logger->debug(__METHOD__." browsePath : ".print_r($uploadFiles, true));

    $data = array(
    'success'    => $success,
    'data'       => $data
    );

    // response must have type of plain/text and not application/json
    // this return is receive in iframe html
    return new Response("<html><body>".json_encode($data)."</body></html>");
    }*/

    /**
     * upload file : return array with name, extension and size of file
     *
     * @Route("/filebrowser/upload-file",
     *        name="carmen_ws_filebrowser_uploadfile",
     *        methods = {"POST","PUT","PATCH"}))
     *
     * @param Request $request  request object
     */
    protected function uploadFileAction(Request $request) {

        $success = true;
        $message = "OK";

        if (!isset($_SERVER['HTTP_X_FILE_NAME'])) {
            return $this->responseJson(false, $this->_t('Unknown file name'));
        }

        if (!isset($_SERVER['HTTP_X_DESTINATION_PATH'])) {
            return $this->responseJson(false, $this->_t('Unknown destination path'));
        }

        if (!isset($_SERVER['HTTP_X_DESTINATION_DEFAULTPATH'])) {
            $_SERVER['HTTP_X_DESTINATION_DEFAULTPATH'] = "/Root";
        }

        if (!isset($_SERVER['HTTP_X_DESTINATION_RELATIVEROOT'])) {
            $_SERVER['HTTP_X_DESTINATION_RELATIVEROOT'] = $_SERVER['HTTP_X_DESTINATION_DEFAULTPATH'];
        }

        $fileName = $_SERVER['HTTP_X_FILE_NAME'];
        if (isset($_SERVER['HTTP_X_FILENAME_ENCODER']) && 'base64' == $_SERVER['HTTP_X_FILENAME_ENCODER']) {
            $fileName = base64_decode($fileName);
        }



        //security check, prevent files to be uploaded
        // default accepted extensions
        $accepted_extension = array(
            'zip', '7z', 'tar', 'arc', 'gz',
            'shp', 'prj', 'dbf', 'shx', 'qpj','cpg',
            'mif', 'mid',
            'tab', 'id', 'map', 'dat', 'ind',
            'json', 'kml', 'gml', 'gpkg',
            'csv','ods','xls','xlsx', 'asc','xyz', 'ods','xls','xlsx',
            'gtiff','tiff','tif','ecw', 'jp2', 'jpg', 'jpeg', 'pdf', 'png',
            'sql',
            'svg'
        );

        // static map accepted extensions
        $origine = $request->query->get('origine') ;
        if($origine !== null && $origine === 'staticmap') {
            $accepted_extension = array(
                'pdf', 'jpeg', 'png'
            );
        }


        $fileName = htmlspecialchars($fileName);
        $file_extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

        if(! in_array($file_extension, $accepted_extension)) {
            return $this->responseJson(false, $this->_t("You can't upload this type of files"));
        }

        $DestPath = $_SERVER['HTTP_X_DESTINATION_PATH'];
        $DestPath = htmlspecialchars($DestPath);


        $relativeRoot  = dirname($_SERVER['HTTP_X_DESTINATION_RELATIVEROOT']);
        $DestPath = "/Root".str_replace("/Root/", "/", $DestPath);
        $DestPath = $relativeRoot.str_replace($relativeRoot, "", $DestPath);
        $DestPath = str_replace($_SERVER['HTTP_X_DESTINATION_DEFAULTPATH'], $this->basePath, $DestPath);

        if( !$this->checkPath($DestPath) ) {
            return $this->responseJson(false, $this->_t("You don't have access to this path"));
        } else {
            $DestPath = realpath($DestPath)."/";
        }

        $mimeType = htmlspecialchars($_SERVER['HTTP_X_FILE_TYPE']);
        $size = intval($_SERVER['HTTP_X_FILE_SIZE']);

        $inputStream = $request->getContent(true) ?: fopen('php://input', 'r');
        $outputFilename = $DestPath. $fileName;
        $realSize = 0;

        $blockSize = 8192;
        if ($inputStream) {
            $outputStream = fopen($outputFilename, 'w');
            if (! $outputStream) {
                return $this->responseJson(false, $this->_t('Error creating local file'));
            }

            while (! feof($inputStream)) {
                $bytesWritten = fwrite($outputStream, fread($inputStream, $blockSize), $blockSize);

                if (false === $bytesWritten) {
                    return $this->responseJson(false, $this->_t('Error writing data to file'));
                }
                $realSize += $bytesWritten;
            }

            fclose($outputStream);
        } else {
            return $this->responseJson(false, $this->_t('Error reading input'));
        }

        if ($realSize != $size) {
            return $this->responseJson(false, $this->_t('The actual size differs from the declared size in the headers'));
        }
        gc_collect_cycles();

        $this->logger->debug(__METHOD__.sprintf(" Uploaded %s, %s, %s, %d byte(s)", $DestPath, $fileName, $mimeType, $realSize));

        return $this->responseJson($success, $message);
    }

    /**
     * upload files : return array with name, extension and size of file
     *
     * @Route("/filebrowser/upload-files",
     *        name="carmen_ws_filebrowser_uploadfiles",
     *        methods={"POST","PUT","PATCH"})
     *
     * @param Request $request  request object
     */
    protected function uploadFilesAction(Request $request) {

        $success = true;
        $message = "OK";



        return $this->responseJson($success, $message);
    }

    /**
     * Return true if path is ok, false otherwise
     * @param string $path   full path to check
     * @return boolean
     */
    protected function checkPath($path)
    {
        $this->logger->debug(__METHOD__." basepatch : ".$path." ".realpath($path));
        return ( $this->basePath != substr(realpath($path), 0, strlen($this->basePath)-1)."/"
            ? false
            : true );
    }

    /**
     * Send json response with array [success: "true|false", messsage: "..."]
     * @param boolean $success
     * @param string  $message
     */
    protected function responseJson($success, $message) {

        $data = array(
            'success'    => $success,
            'message'    => $message
        );
        return $this->formatJsonSuccess($data);
    }


    /**
     * Restfull api to file browser.
     *
     * @Route("/filebrowser/upload-svg",
     *        name="carmen_ws_filebrowser_uploadsvg",
     *        requirements={"action"="[-_%.a-zA-Z0-9]+", "routing"="noroute|absolute|relative"},
     *        defaults={"action"=""},
     *        options={"expose"=true},
     *        methods = {"GET","POST","PUT","PATCH","DELETE"})
     *
     * @param Request $request  request object
     * @param string  $action   action id
     * @return Response with contents in json
     */
    public function importSvg(Request $request){
        $pathInfo = pathinfo($_FILES["upload_svg"]["name"]);
        if($pathInfo['extension'] !== 'svg'){
            return $this->formatJsonSuccess(array('success' => false));
        }

        // random sym file
        $symId = rand(1000,9999);
        $symRandFile = "symbols_svg_$symId.sym";

        // retrieve uploaded files
        $files = $request->files;

        // filename
        $filename =  basename($_FILES["upload_svg"]["name"]);
        $name = basename($_FILES["upload_svg"]["name"], ".svg");



        // and store the file
        $mapfileDir = $this->getCarmenConfig()->getMapserverMapfileDir();

        $uploadedFile = $files->get('upload_svg');
        $uploadedFile->move($mapfileDir."etc/",$filename);

        $content = "";

        $symbols = [
            "SYMBOLSET",
            "",
            "SYMBOL",
            "\tTYPE SVG",
            "\tNAME \"$name\"",
            "\tIMAGE \"$filename\"",
            "END",
            "",
            "END"
        ];

        foreach($symbols as $text){
            $content .= $text."\n";
        }

        file_put_contents ( $mapfileDir."/etc/$symRandFile", $content);
        return $this->formatJsonSuccess(array('success' => true, 'file' => $symRandFile, "id" => $symId));
    }

    /**
     * Restfull api to file browser.
     *
     * @Route("/filebrowser/add-svg",
     *        name="carmen_ws_filebrowser_addsvg",
     *        requirements={"action"="[-_%.a-zA-Z0-9]+", "routing"="noroute|absolute|relative"},
     *        defaults={"action"=""},
     *        options={"expose"=true},
     *        methods = {"GET","POST","PUT","PATCH","DELETE"})
     *
     * @param Request $request  request object
     * @param string  $action   action id
     * @return Response with contents in json
     */
    public function addSvg(Request $request){
        $svgFile = $request->get('path', null);

        if(!$svgFile || !file_exists($this->getAccountDirectory().'/'.$svgFile)){
            return $this->formatJsonSuccess(array('success' => false));
        }

        // random sym file
        $symId = rand(1000,9999);
        $symRandFile = "symbols_svg_$symId.sym";

        // filename
        $filename =  basename($this->getAccountDirectory().'/'.$svgFile);
        $name = basename($this->getAccountDirectory().'/'.$svgFile, ".svg");

        // and store the file
        $mapfileDir = $this->getCarmenConfig()->getMapserverMapfileDir();

        $uploadedFile = $this->getAccountDirectory().'/'.$svgFile;
        copy($uploadedFile ,$mapfileDir."etc/".$filename);

        $content = "";

        $symbols = [
            "SYMBOLSET",
            "",
            "SYMBOL",
            "\tTYPE SVG",
            "\tNAME \"$name\"",
            "\tIMAGE \"$filename\"",
            "END",
            "",
            "END"
        ];

        foreach($symbols as $text){
            $content .= $text."\n";
        }

        file_put_contents ( $mapfileDir."/etc/$symRandFile", $content);
        return $this->formatJsonSuccess(array('success' => true, 'file' => $symRandFile, "id" => $symId));
    }

    /**
     * Restfull api to file browser.
     *
     * @Route("/filebrowser/remove-icon",
     *        name="carmen_ws_filebrowser_removeicon",
     *        requirements={"action"="[-_%.a-zA-Z0-9]+", "routing"="noroute|absolute|relative"},
     *        defaults={"action"=""},
     *        options={"expose"=true},
     *        methods = {"GET","POST","PUT","PATCH","DELETE"})
     *
     * @param Request $request  request object
     * @param string  $action   action id
     * @return Response with contents in json
     */
    public function removeSvg(Request $request){
        $svgName = $request->get('name', null);

        $mapFile = $request->get('from_symbolset', null);

        $mapFile = $this->getCarmenConfig()->getMapserverMapfileDir().$mapFile;

        if(!$svgName || !$mapFile || !file_exists($mapFile)){
            return $this->formatJsonSuccess(array('success' => false, 'debug' => array($svgName, $mapFile, file_exists($mapFile)) ));
        }

        /** on lit le contenue du mapfile */
        $mapFileLines = file($mapFile);

        if(!is_array($mapFileLines) || empty($mapFileLines)){
            return $this->formatJsonSuccess(array('success' => false));
        }

        $copy = array();
        $output = array();
        $state = 'read';
        /** machine d'état  */
        foreach($mapFileLines  as $line){
            $copy[] = $line;
            switch($state){
                case 'read': /** état par défaut, change quand une ligne vaut SYMBOL */
                    if(!is_bool(strrpos( $line, 'SYMBOL') )){
                        $state = 'symbol';
                    }
                    else{
                        $output = array_merge($output, $copy);
                        $copy = [];
                    }
                    break;
                case 'symbol': /** change une fois le type svg trouvé */
                    if(!is_bool(strrpos($line, 'TYPE SVG'))){
                        $state = 'type_svg';
                    }
                    else{
                        $state = 'end';
                    }
                    break;
                case 'type_svg': /** change avec NAME, soit dans delete_end soit dans end */
                    if(!is_bool(strrpos($line, 'NAME')) && !is_bool(strrpos($line, $svgName))){
                        $state = 'delete_end';
                    }
                    else{
                        $state = 'end';
                    }
                    break;
                case 'end':  /** on ajout le contenue de copy dans output, et on revient à l'état read */
                    if(!is_bool(strrpos($line, 'END'))){
                        $output = array_merge($output, $copy);
                        $copy = [];
                        $state = 'read';
                    }

                    break;
                case 'delete_end': /** on efface le contenue du copy et on revient à l'état read*/
                    if(!is_bool(strrpos($line, 'END'))){
                        $copy = [];
                        $state = 'read';
                    }

                    break;
            }
        }

        /** on sauvegarde le mapfile */
        copy($mapFile, $mapFile."_old");

        /** on écrit le contenue de output */
        $fp = fopen($mapFile, 'w');

        $oldLine = null;
        foreach($output as $line){
            /** on fait le ménage avec les retour à la ligne */
            if($line == "\n" && $line != $oldLine){
                fwrite($fp, $line);
            }
            else if($line != "\n"){
                fwrite($fp, $line);
            }
            $oldLine = $line;
        }

        fclose($fp);


        return $this->formatJsonSuccess(array('success' => true));
    }

    /**
     * carmen_ws_filebrowser_deletesymbyid
     * @Route("/filebrowser/delete-sym-svg/{id}",
     *        name="carmen_ws_filebrowser_deletesymbyid",
     *        requirements={"action"="[-_%.a-zA-Z0-9]+", "routing"="noroute|absolute|relative"},
     *        defaults={"action"=""},
     *        options={"expose"=true},
     *        methods = {"GET","POST","PUT","PATCH","DELETE"})
     */
    public function deleteTmpSym(Request $request, $id){
        $idNumber = intval($id);
        $symRandFile = "symbols_svg_$idNumber.sym";
        $mapfileDir = $this->getCarmenConfig()->getMapserverMapfileDir();
        unlink($mapfileDir."etc/".$symRandFile);

        return $this->formatJsonSuccess(array('success' => true));
    }
}
