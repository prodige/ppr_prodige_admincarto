<?php
namespace Carmen\ApiBundle\Services;

//use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Service CarmenConfig
 *
 * @author alkante <support@alkante.com>
 */
class Helpers /*extends ContainerAware*/
{
    /**
     * Get Carmen types according to ogrtypes
     * @see Carmen\ApiBundle\Entity\LexFieldDatatype
     * 
     * @param string $ogrType 
     * @return number|null
     */
    public static function getCarmenType($ogrType)
    {
        $ogrType = strtoupper($ogrType);
        switch($ogrType){
            case "STRING" :
                return 1;
                break;
            case "INTEGER" :
            case "INTEGER64" :
            case "NUMERIC" :
            case "REAL" :
                return 2;
                break;
            case "DATE" :
            case "DATETIME" :   
                return 3;
                break;
            case "BOOLEAN" :
                return 4;
                break;
            default :
                return null;
                break;
        }
    }

    /**
     * Get Database column types according to ogrtypes
     * 
     * @param string $ogrType 
     * @return string
     */
    public static function getDatabaseType($ogrType)
    {
        $ogrType = strtoupper($ogrType);
        switch($ogrType){
            case "STRING" :
            case "CHARACTER" :
            case "CHARACTER VARYING" :
                return "text";
            case "INTEGER" :
                return "integer";
            case "INTEGER64" :
            case "BIGINT";
                return "bigint";
            case "NUMERIC" :
            case "REAL" :
            case "DOUBLE PRECISION" :
                return "float";
            case "DATE" :
                return "date";
            case "BOOLEAN" :
                return "boolean";
            default :
                return $ogrType;
        }
        return "text";
    }
    /**
     * Encode an image file as base64
     * @param string  $imagePath
     * @param boolean $withBas64headers if true (default), returns the whole base64 data header with image type
     */
    public static function base64image($imagePath, $withBas64headers=true)
    {
        if( file_exists($imagePath) ) {
            $type = pathinfo($imagePath, PATHINFO_EXTENSION);
            $data = file_get_contents($imagePath);
            return ($withBas64headers ? 'data:image/' . $type . ';base64,' : '' ) . base64_encode($data);
        }
        
        return null;
    }
    
    /**
     * Buidl XML accounts file from database
     */
    public function buildServicesXML(){
        $doctrine = $this->container->get('doctrine');
        
        $dom = new \DOMDocument('1.0', 'utf-8');
        $services = $dom->createElement('services');
    
        $em   = $doctrine->getManager();
        $accounts = $em->getRepository('CarmenApiBundle:Account')->findAll();
        for($i=0; $i<count($accounts);$i++){
            $account = $accounts[$i];
            $account instanceof \Carmen\ApiBundle\Entity\Account;
            $service = $dom->createElement('service');
            $service->setAttribute('idx', $account->getAccountId());
            $service->setAttribute('name', $account->getAccountName());
            $service->setAttribute('path', $account->getAccountPath()."/");
            if($account->getAccountDns()){
                $service->setAttribute('dns', $account->getAccountDns()->getDnsUrl());
                $service->setAttribute('scheme', $account->getAccountDns()->getDnsScheme());
                $service->setAttribute('prefixBackoffice', $account->getAccountDns()->getDnsPrefixBackoffice());
                $service->setAttribute('prefixData', $account->getAccountDns()->getDnsPrefixData());
                $service->setAttribute('prefixMetadata', $account->getAccountDns()->getDnsPrefixMetadata());
                $service->setAttribute('prefixFrontffice', $account->getAccountDns()->getDnsPrefixFrontoffice());
            }
            $service->setAttribute('geonetworkUrl', $account->getAccountGeonetworkurl());
            $services->appendChild($service);
        }
    
        $dom->appendChild($services);
        $config = $this->container->getParameter('carmen_config', array());
        $dom->save($config['mapserver']['root_data']."/".$config['mapserver']['system_dirname']."/services.xml");
        return true;
    }
}