<?php

namespace Carmen\ApiBundle\Services;

//use Symfony\Component\DependencyInjection\ContainerAware;
use Carmen\ApiBundle\Entity\Users;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service CarmenConfig
 *
 * @author alkante <support@alkante.com>
 */
class CarmenConfig implements ContainerAwareInterface /*extends ContainerAware*/
{
    use ContainerAwareTrait;
    /**
     * @var array
     */
    private $carmen_config;

    /**
     * Construct and translate carmen_config parameters
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);

        // do your init stuff here
        $this->carmen_config = $carmen_config = $this->container->getParameter('carmen_config');

        $this->carmen_config = $this->translate($carmen_config);

        $this->createDirectories();
    }

    /**
     * Translate carmen_config parameters
     * @param array $array
     */
    private function translate(array $array)
    {
        $array = $this->translateAccount($array);
        $array = $this->translateConfig($array, $array);
        return $array;
    }

    /**
     * Translate account variables in carmen_config parameters 
     * @param array $array
     */
    private function translateAccount(array $array)
    {
        $account = null;

        $container = $this->container;

        $service_token_storage = $container->get('security.token_storage');
        if ($container->get('request_stack') && $container->get('request_stack')->getCurrentRequest() && $container->get('request_stack')->getCurrentRequest()->query->has('accountId')) {
            // accountId if given from the URL
            $account = $this->container->get('doctrine')->getRepository('CarmenApiBundle:Account')->find($container->get('request_stack')->getCurrentRequest()->query->get('accountId'));
            if (!$account) {
                throw new \Exception("Unable to translate parameters of application");
            }
        } else if ($service_token_storage->getToken()) {
            // account is retreived from the connected user, if any
            $user = $service_token_storage->getToken()->getUser();
            if (!$user) {
                throw new \Exception("Unable to translate parameters of application");
            }
            $user instanceof Users;

            // $account = $user->getAccount();
        }

        $account_name = ($account ? $account->getAccountName() : "__ANONYMOUS__");
        $account_path = ($account ? $account->getAccountPath() : "__ANONYMOUS__");
        //$user_id = $user->getUserId();
        $variables = array("{account_name}" => $account_name, "{account_path}" => $account_path/*, "{user_id}"=>$user_id*/);
        // user_id is not used for the moment and will no be available if accountId is passed in the url

        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result[$key] = $this->translateAccount($value);
            } else {
                $result[$key] = str_replace(array_keys($variables), array_values($variables), $value);
            }
        }
        return $result;
    }

    /**
     * Create Directories (from template or autonomous). 
     * TODO Complete the list of required directory if parameters are updated
     */
    private function createDirectories()
    {
        $mkdir = function ($directory) {
            if (!is_dir($directory)) mkdir($directory, 0770, true);
        };
        $copydir = function ($from_directory, $to_directory) use (&$copydir, $mkdir) {
            if (is_null($from_directory) || is_null($to_directory)) return;
            if (!is_dir($from_directory)) return;
            if (!is_dir($to_directory)) {
                mkdir($to_directory, 0770, true);
            }
            $files = scandir($from_directory);
            foreach ($files as $file) {
                if (preg_match("!^\.+$!", $file)) continue;

                if (is_dir($from_directory . "/" . $file)) {
                    $copydir($from_directory . "/" . $file, $to_directory . "/" . $file);
                } else {
                    if (file_exists($to_directory . "/" . $file) && is_file($from_directory . "/" . $file) === is_file($to_directory . "/" . $file)) continue;
                    copy($from_directory . "/" . $file, $to_directory . "/" . $file);
                }
            }
        };

        $directories = array(
            "MapserverRootData",
            //"MapserverTemplateDir",
            "MapserverMapfileDir",
            "MapserverAccountDir",
            "OwscontextPublicationDir",
        );
        $tmp = array();
        foreach ($directories as $directory) {
            $method = "get" . $directory;
            if (method_exists($this, $method)) {
                $tmp[$directory] = $this->$method();
            }
        }
        $directories = $tmp;

        //CREATE DIRECTORIES 
        $copydir($this->getMapserverTemplateDir() . "/{account_path}", $this->getMapserverAccountDir());
        foreach ($directories as $directory) {
            $mkdir($directory);
        }
    }

    /**
     * Translate parameters with others carmen_config parameters 
     * @param string & $parameter
     * @return string $parameter
     */
    private function translateConfig(&$parameter, &$carmen_config)
    {
        if (is_array($parameter)) {
            foreach ($parameter as $key => $value) {
                $parameter[$key] = $this->translateConfig($value, $carmen_config);
            }
        } else {
            $matches = array();
            if (preg_match_all("!\{carmen_config:([^}]+)\}!", $parameter, $matches)) {
                foreach ($matches[1] as $variable) {
                    $eval = 'return $this->translateConfig($carmen_config["' . str_replace(":", '"]["', $variable) . '"], $carmen_config);';
                    $parameter = str_replace("{carmen_config:" . $variable . "}", eval($eval), $parameter);
                }
            }
        }
        return $parameter;
    }

    /**
     * Get carmen_config.mapserver.template_dir parameter with variable translations
     * @return string
     */
    public function getMapserverTemplateDir()
    {
        return "../src/Carmen/ApiBundle/Resources/__TEMPLATES__/CARMEN/";
    }


    /**
     * Get carmen_config.owscontext.publication_dir parameter with variable translations
     * @return string
     */
    public function getOwscontextPublicationDir()
    {
        return $this->carmen_config["owscontext"]["publication_dir"];
    }


    /**
     * Get carmen_config.owscontext.legend_url parameter with variable translations
     * @return string
     */
    public function getOwscontextLegendUrl()
    {
        return $this->carmen_config["owscontext"]["legend_url"];
    }

    /**
     * Get carmen_config.mapserver.api_url parameter with variable translations
     * @return string
     */
    public function getMapserverApiUrl()
    {
        return $this->carmen_config["mapserver"]["api_url"];
    }


    /**
     * Get carmen_config.mapserver.root_data parameter with variable translations
     * @return string
     */
    public function getMapserverRootData()
    {
        return $this->carmen_config["mapserver"]["root_data"];
    }


    /**
     * Get carmen_config.mapserver.mapfile_dir parameter with variable translations
     * @return string
     */
    public function getMapserverMapfileDir()
    {
        return $this->carmen_config["mapserver"]["mapfile_dir"];
    }



    /**
     * Get carmen_config.mapserver.account_dir parameter with variable translations
     * @return string
     */
    public function getMapserverAccountDir()
    {
        return $this->carmen_config["mapserver"]["account_dir"];
    }

    /**
     * Returns an array containing only URL like parameters to be used in client side
     */
    public function getWebConfig()
    {
        $config = array();
        array_walk_recursive($this->carmen_config, function ($item, $key) use (&$config) {
            if (strpos($key, '_url') !== false) {
                $config[$key] = $item;
            }
        });

        return $config;
    }
}
