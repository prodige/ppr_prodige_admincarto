<?php

namespace Carmen\ApiBundle\Exception;

/**
 * Api Exception.
 *
 * @author alkante <support@alkante.com>
 */
class ApiException extends \Exception
{
    protected $data;

    /**
     * Creates a new ApiException.
     *
     * @param string $message Exception message
     * @param array  $data    Custom data
     * @param string $code
     * @param string $previous
     */
    public function __construct($message = null, array $data = array(), $code = null, $previous = null) {
      parent::__construct($message, $code, $previous);
      $this->data = $data;
    }

    /**
     * @return array The data.
     */
    public function getData()
    {
        return $this->data;
    }
}
