<?php

namespace Carmen\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Prodige4210Command extends ContainerAwareCommand
{
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('prodige4210:migration')
            ->setDescription('Migrate to 4.2.10 version')
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'The account path (string) or the account ID (number)'
            )
        ;
    }


    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $conn = $this->getConnection();
        $em   = $this->getManager();
    
        $account = $input->getArgument("account");
    
        /*Search the account (must be uniq)*/
        $fields = (is_numeric($account) ? array("accountId") : array("accountName", "accountPath"));
        $accounts = array();
        foreach ($fields as $field){
            $dataset = $em->getRepository('CarmenApiBundle:Account')->findBy(array($field=>$account));
            foreach ($dataset as $entity){
                $accounts[$entity->getAccountId()] = $entity;
            }
        }
        if ( empty($accounts) ){
            $output->writeln("<error>ERROR : the account identified by '".$account."' does not exist.</error>");
            return;
        }
        if ( count($accounts)>1 ){
            $output->writeln("<error>ERROR : Multiple accounts [".count($accounts)."] corresponding to the '".$account."' identifier exist.</error>");
            return;
        }
        $oAccount = current($accounts);
    

        //update locators in mapfiles
        $mapContext = $this->getContainer()->get('carmen.map');
        $mapContext instanceof MapController;
        //      m.mapModel = TRUE and
        $dql_query = $em->createQuery("
			SELECT m FROM  CarmenApiBundle:Map m
			WHERE 
        
              m.mapFile not LIKE 'layers/TMP%'
              and m.mapFile not LIKE 'local_data/TMP%'
              and m.publishedMap is null
              ");
		$maps = $dql_query->getResult();

        foreach($maps as $key => $map){
            
            $locatorForMapserv = array();
            $locatorForMapserv["locators"] = array();
            
            $mapfile = basename($map->getMapfile());
            if($map->getMapModel()){
                $mapfile = "modele_".$mapfile;
            }
            
            $additionalPath = dirname($map->getMapfile());
            $locatorForMapserv["directory"]= $mapContext->getMapfileDirectory().($additionalPath!="." ? $additionalPath."/" : "");

            $mapfilePath = $locatorForMapserv["directory"].$mapfile.".map";
            $output->writeln('<info>treating map '.$mapfilePath.' </info>');
            
            foreach($map->getLocators() as $locator){
                $layer = $map->getLayerById($locator->getLocatorCriteriaLayerId());
                if($layer){
                    $layerMsname = $layer->getLayerMsname();
                    $layerFields = array($locator->getLocatorCriteriaFieldId(), $locator->getLocatorCriteriaFieldName());
                    if($locator->getLocatorCriteriaRelatedField()!==""){
                        $layerFields[]= $locator->getLocatorCriteriaRelatedField();
                    }
                    
                    $locatorForMapserv["locators"][] = array(
                        "locatorRank" => $locator->getLocatorCriteriaRank(),
                        "layername" => $layerMsname,
                        "layerFields" => $layerFields,
                        "filterField" => $locator->getLocatorCriteriaRelatedField()!=="" ? $locator->getLocatorCriteriaRelatedField() : "",
                        "orderField" => $locator->getLocatorCriteriaFieldName()
                    );
                }
            }
            
            try{
                $resultLocator = $mapContext->callMapserverApi("/api/locator/{$mapfile}", 'POST', array(), $locatorForMapserv, true, array(
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_TIMEOUT        => 0
                ));
            } catch(\Exception $ex ) {
                echo $ex->getMessage();
            }
        }
        
    }
    
       
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }
       
    
    

}