<?php

namespace Carmen\ApiBundle\Command;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PuiMailCommand extends Command
{
    protected $urlDirectoryCsv = '/home/ndurand/ppr_prodige_admincarto/site/web/csv/';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('carmen:puimail')
            ->setDescription('Pui Mail');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Pui Mail");

        $conn = $this->getProdigeConnection();

        /** SQL sélection des données */
        $sql = "SELECT covid_vaccin_stock_dm_historique_vue.*
                FROM covid_vaccin_stock_dm_historique_vue 
                inner join covid_vaccin_lieu on covid_vaccin_lieu.gid = covid_vaccin_stock_dm_historique_vue.lieu_gid
                inner join covid_vaccin_pui on covid_vaccin_pui.finess_pui = covid_vaccin_lieu.finess_pharmacie_reference
                where covid_vaccin_stock_dm_historique_vue.date_maj::date =  (NOW() - INTERVAL '1 DAY')::date";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll();

        $splitReference = array();

        /** SQL get emails */
        $sql = "SELECT email_contact_pui as email, finess_pui as finess FROM covid_vaccin_pui";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $emailTmps = $stmt->fetchAll();

        $emails = [];
        foreach ($emailTmps as $email) {
            if (isset($email['finess']) && isset($email['email'])) {
                $emails[$email['finess']] = $email['email'];
            }
        }

        /** split des données en fonction des finess_pharmacie_reference */

        foreach ($data as $stock) {
            if (isset($stock['finess_pharmacie_reference'])) {
                if (!isset($splitReference[$stock['finess_pharmacie_reference']])) {
                    $splitReference[$stock['finess_pharmacie_reference']] = array();
                }

                $splitReference[$stock['finess_pharmacie_reference']][] = $stock;
            }
        }

        /** écriute des csv */
        foreach ($splitReference as $finesKey => $referenceData) {
            $filename = $this->urlDirectoryCsv . $finesKey . "_" . date("m_d_y") . '.csv';
            $fp = fopen($filename, 'w');
            $headerOk = false;

            foreach ($referenceData as $data) {
                if (!$headerOk) {
                    $headerOk = true;
                    fputcsv($fp, array_keys($data), ";");
                }
                fputcsv($fp, $data, ";");
            }
            fclose($fp);

            if (isset($emails[$finesKey])) {
                $this->sendMail($emails[$finesKey], $filename);
            }
        }


        var_dump($splitReference);
        return Command::SUCCESS;
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->container->get('doctrine')->getConnection();
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    protected function sendMail($to, $file)
    {
        $appMail = $this->container->getParameter('app_mail', null);

        $message = \Swift_Message::newInstance()
            ->setSubject('Export csv')
            ->setFrom($appMail ? $appMail : 'b.fontaine@alkante.com')
            ->setTo($to)
            ->setBody(
                "Bonjour ci-joint les export csv",
                'text/plain'
            )
            ->attach(\Swift_Attachment::fromPath($file));

        $this->container->get('mailer')->send($message);


        // or, you can also fetch the mailer service this way
        // $this->get('mailer')->send($message);
    }
}