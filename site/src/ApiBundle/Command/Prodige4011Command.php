<?php

namespace Carmen\ApiBundle\Command;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Cette commande s'occupe d'assigner des identifiants uniques aux metadaonnées de cartes de type layer WMS
 */
class Prodige4011Command extends ContainerAwareCommand
{

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setName('prodige:4011')->setDescription('Migration 4.0.10 > 4.0.11');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // call the prodige.configreader service
        $this->getContainer()->get('prodige.configreader');
        $carmenConfig = $this->getContainer()->get('carmen.config');
        // init config
        
        $conn = $this->getProdigeConnection('carmen');
        
        $output->writeln("");
        
        $output->writeln("<info>".str_repeat("=", 50)."</info>");
        $output->writeln("<info>"."SET EXACT GEOMETRYTYPE COLUMNS"."</info>");
        $output->writeln("<info>".str_repeat("=", 50)."</info>");
        
        $conn = $this->getProdigeConnection('public');
        $conn->exec(file_get_contents(__DIR__."/../Resources/sql/set_geometrytype_srid_v4.0.10.sql"));
        $conn->executeQuery("drop table if exists v4_11_query_updateViewsGeometryColumns");
        $conn->executeQuery("create table v4_11_query_updateViewsGeometryColumns as
            select row_number()over() as id, * from updateViewsGeometryColumns() as (table_name text, updateviewsgeometrycolumns text)");

        try {
            $conn->executeQuery("create table v4_11_memory_updateViewsGeometryColumns as select now(), * from v4_11_query_updateViewsGeometryColumns");
        } catch(\Exception $exception){}
        $conn->executeQuery("insert into v4_11_memory_updateViewsGeometryColumns select now(), * from v4_11_query_updateViewsGeometryColumns where (table_name, updateviewsgeometrycolumns) not in (select table_name, updateviewsgeometrycolumns from v4_11_memory_updateViewsGeometryColumns)");

        try {
            $conn->executeQuery("create table v4_11_exec_updateViewsGeometryColumns (rownumber int, execution timestamp without time zone default now(), table_name text, state text, message text, queries text, constraint pk_exec primary key(rownumber) )");
        } catch(\Exception $exception){}
        
        
        $updategeomtypes = $conn->query("select distinct table_name, updateviewsgeometrycolumns, id 
                from v4_11_query_updateViewsGeometryColumns 
                order by table_name, id");
        $updategeomtypes = $updategeomtypes->fetchAll(\PDO::FETCH_COLUMN|\PDO::FETCH_GROUP);
        
        $rownumber = $conn->fetchColumn("select coalesce(max(rownumber)+1, 1) from v4_11_exec_updateViewsGeometryColumns");
        $nb_success = 0;
        $failures = array();
        foreach($updategeomtypes as $table_name=>$queries){
            $queries = implode("
;
", $queries);
            try {
                $conn->exec("/*{$table_name}*/
".$queries);
                $conn->executeQuery("insert into v4_11_exec_updateViewsGeometryColumns 
                        (rownumber, table_name, state, message, queries) values (:rownumber, :table_name, :state, :message, :queries)",
                        array( 
                            "queries" => $queries,
                            "table_name" => $table_name,
                            "rownumber" => $rownumber,
                            "state"=>'SUCCESS', 
                            "message"=>'Précision du geometry type et regénération des vues liées éventuelles'
                        )
                );
                $conn->executeQuery("delete from v4_11_query_updateViewsGeometryColumns where table_name=:table_name", compact("table_name"));
                $nb_success++;
            } catch (\Exception $exception){
                $conn->executeQuery("insert into v4_11_exec_updateViewsGeometryColumns 
                        (rownumber, table_name, state, message, queries) values (:rownumber, :table_name, :state, :message, :queries)",
                        array( 
                            "queries" => $queries,
                            "table_name" => $table_name,
                            "rownumber" => $rownumber,
                            "state"=>'FAILURE', 
                            "message"=>$exception->getMessage()
                        )
                );
                
                $failures[] = $table_name;
                $output->writeln("<error>FAILURE : {$table_name} : Geometry type not affected</error>");
                $output->writeln("<comment>".$exception->getMessage()."</comment>");
                $output->writeln("");
            }
            $rownumber++;
        }
        $output->writeln("SUCCESS : ".$nb_success." / ".count($updategeomtypes));
        $output->writeln("FAILURE : ".(count($updategeomtypes)-$nb_success)." / ".count($updategeomtypes));
        foreach($failures as $table_name) $output->writeln("> <error>{$table_name}</error>");
    }
    

    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * Get Doctrine connection
     *
     * @param string $connection_name            
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->getContainer()
            ->get("doctrine")
            ->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);
        
        return $conn;
    }

    /**
     *
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
}