<?php

namespace Carmen\ApiBundle\Command;

use Carmen\ApiBundle\Controller\MapController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CleanCommand extends ContainerAwareCommand
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('carmen:cleanMaps')
            ->setDescription('Clean temporary maps');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $conn = $this->getConnection();
        $em = $this->getManager();

        $dql_query = $em->createQuery(
            "
			SELECT m FROM  CarmenApiBundle:Map m
			WHERE 
			  m.mapFile LIKE 'layers/TMP%'
              or m.mapFile LIKE 'local_data/TMP%'"
        );
        $maps = $dql_query->getResult();

        foreach ($maps as $key => $map) {
            $output->writeln('<info>Deleting temporary map ' . $map->getMapfile() . ' </info>');
            $mapContext = $this->container->get('carmen.map');
            $mapContext instanceof MapController;
            $mapContext->deleteMapAction($map->getMapId(), false);
        }

        return Command::SUCCESS;
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->container->get('doctrine')->getConnection();
    }


}