<?php

namespace Carmen\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Prodige421Command extends ContainerAwareCommand
{

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('prodige421:migration')
            ->setDescription('Migrate to 4.2.1 version')
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'The account path (string) or the account ID (number)'
            );
    }

    /**
     * format name without accent and special caracters
     */
    protected function formatName($title)
    {
        $title = strtr(strtolower($title), "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
            "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");

        $title = preg_replace('#[^a-z0-9_-]#', '_', $title);
        return $title;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $conn = $this->getConnection();
        $em = $this->getManager();

        $account = $input->getArgument("account");

        /*Search the account (must be uniq)*/
        $fields = (is_numeric($account) ? array("accountId") : array("accountName", "accountPath"));
        $accounts = array();
        foreach ($fields as $field) {
            $dataset = $em->getRepository('CarmenApiBundle:Account')->findBy(array($field => $account));
            foreach ($dataset as $entity) {
                $accounts[$entity->getAccountId()] = $entity;
            }
        }
        if (empty($accounts)) {
            $output->writeln("<error>ERROR : the account identified by '" . $account . "' does not exist.</error>");
            return;
        }
        if (count($accounts) > 1) {
            $output->writeln("<error>ERROR : Multiple accounts [" . count($accounts) . "] corresponding to the '" . $account . "' identifier exist.</error>");
            return;
        }
        $oAccount = current($accounts);


        //update locators in mapfiles
        $mapContext = $this->getContainer()->get('carmen.map');
        $mapContext instanceof MapController;
        //      m.mapModel = TRUE and
        $dql_query = $em->createQuery("
			SELECT m FROM  CarmenApiBundle:Map m
			WHERE 
        
              m.mapFile not LIKE 'layers/TMP%'
              and m.mapFile not LIKE 'local_data/TMP%'
              and m.publishedMap is null
              ");
        $maps = $dql_query->getResult();


        foreach ($maps as $key => $map) {

            $locatorForMapserv = array();
            $locatorForMapserv["locators"] = array();

            $mapfile = basename($map->getMapfile());
            if ($map->getMapModel()) {
                $mapfile = "modele_" . $mapfile;
            }


            $additionalPath = dirname($map->getMapfile());
            $locatorForMapserv["directory"] = $mapContext->getMapfileDirectory() . ($additionalPath != "." ? $additionalPath . "/" : "");

            $mapfilePath = $locatorForMapserv["directory"] . $mapfile . ".map";
            $output->writeln('<info>treating map ' . $mapfilePath . ' </info>');

            try {
                ms_ResetErrorList();
                $oMap = ms_newMapObj($mapfilePath);
                $oMap->set("name", $this->formatName(($additionalPath != "." ? $additionalPath . "/" : "") . $mapfile));
                $oMap->web->set("imageurl", $this->getContainer()->getParameter("PRODIGE_URL_FRONTCARTO") . "/mapimage/");
                /*$oMap->legend->set("template", "legend.html");
                $oMap->legend->set("status", MS_ON);*/
                //migrate map
                $oMap->setMetadata("wfs_enable_request", "*");
                $oMap->setMetadata("wfs_getfeature_formatlist", "geojson,csv,ogrgml");
                for ($j = 0; $j < $oMap->numlayers; $j++) {
                    $oLayerObj = $oMap->getLayer($j);
                    if ($oLayerObj->type === MS_LAYER_POLYGON ||
                        $oLayerObj->type === MS_LAYER_LINE ||
                        $oLayerObj->type === MS_LAYER_POINT) {
                        $oLayerObj->setMetadata("gml_types", "auto");
                    }
                }


                $oMap->save($mapfilePath);

            } catch (\Exception $ex) {
                $output->writeln('<error>' . $mapfilePath . ' not treated, corrupted for Mapserver 7.0 !</error>');
                $strError = $ex->getMessage();
                $error = ms_GetErrorObj();
                while ($error && $error->code != MS_NOERR) {
                    $strError .= sprintf("\n  Error in %s: %s", $error->routine, $error->message);
                    $error = $error->next();
                }
                $output->writeln('  <comment>' . $strError . "\n" . '</comment>');
                continue;
            }


            foreach ($map->getLocators() as $locator) {
                $layer = $map->getLayerById($locator->getLocatorCriteriaLayerId());
                if ($layer) {
                    $layerMsname = $layer->getLayerMsname();
                    $layerFields = array($locator->getLocatorCriteriaFieldId(), $locator->getLocatorCriteriaFieldName());
                    if ($locator->getLocatorCriteriaRelatedField() !== "") {
                        $layerFields[] = $locator->getLocatorCriteriaRelatedField();
                    }

                    $locatorForMapserv["locators"][] = array(
                        "layername" => $layerMsname,
                        "layerFields" => $layerFields,
                        "orderField" => $locator->getLocatorCriteriaFieldName()
                    );
                }

            }

            try {
                $resultLocator = $mapContext->callMapserverApi("/api/locator/{$mapfile}", 'POST', array(), $locatorForMapserv, true, array(
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_TIMEOUT => 0
                ));
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }

            //since it can't be done with mapscript
            $mapfileString = file_get_contents($mapfilePath);
            //implode("\n",file($mapfilePath));
            if (strpos($mapfileString, "application/json; subtype=geojson") === false) {

                $fp = fopen($mapfilePath, 'w');
                //replace something in the file string - this is a VERY simple example
                $strOuputFormat = '
                OUTPUTFORMAT
                  NAME "geojson"
                  MIMETYPE "application/json; subtype=geojson"
                  DRIVER "OGR/GEOJSON"
                  IMAGEMODE FEATURE
                  TRANSPARENT FALSE
                  FORMATOPTION "FORM=SIMPLE"
                  FORMATOPTION "STORAGE=stream"
                END # OUTPUTFORMAT
              
                OUTPUTFORMAT
                  NAME "OGRGML"
                  DRIVER "OGR/GML"
                  IMAGEMODE FEATURE
                  TRANSPARENT FALSE
                  FORMATOPTION "FILENAME=result.gml"
                  FORMATOPTION "FORM=multipart"
                  FORMATOPTION "STORAGE=filesystem"
                END # OUTPUTFORMAT';
                // Replace something in the file string - this is a VERY simple example
                $mapfileString = str_replace("END # OUTPUTFORMAT", "END # OUTPUTFORMAT\n\n" . $strOuputFormat, $mapfileString);

                //now, TOTALLY rewrite the file
                fwrite($fp, $mapfileString, strlen($mapfileString));

            }
            $mapfileString = file_get_contents($mapfilePath);
            if (strpos($mapfileString, "TEMPLATE \"legend.html\"") === false) {
                $fp = fopen($mapfilePath, 'w');
                $mapfileString = str_replace("STATUS OFF\n  END # LEGEND", "STATUS ON\n    TEMPLATE \"legend.html\"\n  END # LEGEND", $mapfileString);
                //now, TOTALLY rewrite the file
                fwrite($fp, $mapfileString, strlen($mapfileString));
            }


        }

    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }


}