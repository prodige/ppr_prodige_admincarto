<?php

namespace Carmen\ApiBundle\Command;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RenameDataCommand extends ContainerAwareCommand
{
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('prodige:renameData')
            ->setDescription('Specific Command to rename data in database and mapfiles, not to be used before being awared of consequences. this task does not treat or views')
            ->addArgument(
                'tableBefore',
                InputArgument::REQUIRED,
                'table name before')
            ->addArgument(
                'tableAfter',
                InputArgument::REQUIRED,
                'table name after')
            ->addArgument(
                'fieldToAdd',
                InputArgument::REQUIRED,
                'array of fields to add in json format [{ "field":"a", "type":"varchar", "field_datatype_id" : 1 }]')
            ->addArgument(
                'fieldToRemove',
                InputArgument::REQUIRED,
                'array of fields to add in json format [{ "field":"a"}]')
            ->addArgument(
                'fieldToRename',
                InputArgument::REQUIRED,
                'array of fields to add in json format [{ "field":"a",  "renameTo":"b"}]')    
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
          // call the prodige.configreader service
          $this->getContainer()->get('prodige.configreader');
          // init config
          $conn = $this->getProdigeConnection('carmen');
  
          $config =  $this->getContainer()->getParameter('carmen_config', array());
          $mapPath = $config['mapserver']['root_data'] . "/cartes/Publication/";
          
          
          $tableBefore = $input->getArgument('tableBefore');
          $tableAfter = $input->getArgument('tableAfter');
          $fieldToAdd = json_decode($input->getArgument('fieldToAdd'));
          $fieldToRemove = json_decode($input->getArgument('fieldToRemove'));
          $fieldToRename = json_decode($input->getArgument('fieldToRename'));

          if($tableBefore=="" || $tableAfter=="" || $fieldToAdd===false || $fieldToRemove===false || $fieldToRename===false ){
              $output->writeln('<error>mauvaise configuration de la commande</error>');
              return;
          }
          //1 impact tables

          //operations on table
          if($tableBefore==$tableAfter){
              foreach($fieldToAdd as $key => $obj){
                  $strSql = "alter table public.".$tableBefore." add column ".$obj->field. " ".$obj->type;
                  
                  //echo $strSql."\n";
                  $conn->executeQuery($strSql);
              }
              foreach($fieldToRename as $key => $obj){
                  $strSql = "alter table public.".$tableBefore." rename column ".$obj->field. " to ".$obj->renameTo;
                  
                  //echo $strSql."\n";
                  $conn->executeQuery($strSql);
              }
              foreach($fieldToRemove as $key => $obj){
                  $strSql = "alter table public.".$tableBefore." drop column ".$obj->field;
                  //echo $strSql."\n";
                  $conn->executeQuery($strSql);
              }
          }

          //2 impact maps

          $query = "select map_id, map_file from carmen.map where published_id is null and map_file not like 'local_data/%' ". 
                    "and map_file not like 'layers/TMP%' order by map_id ";
          $maps = $conn->fetchAll($query);
          
          foreach ($maps as $map) {
              $id = $map["map_id"];
              $map_file = $map["map_file"];
              $map_file = $mapPath.$map_file.".map";
              if(!file_exists($map_file)){
                  $output->writeln('<info>Mapfile inexistant : '.$map_file.'</info>');
                  continue;
              }
              try {            
                  $oMap = ms_newMapObj($map_file);
                  $output->writeln('<info>Traitement du mapfile : '.$map_file.'</info>');
              } catch (\Exception $ex) {
                  $output->writeln('<error>'.$map_file.' not treated, corrupted for Mapserver 7.0 !</error>');
                  continue;
              }
              $query = "select layer_id, layer_msname, layer_title from carmen.layer where map_id= :map_id and layer_type_code = 'POSTGIS' ";
              
              $layers = $conn->fetchAll($query, array("map_id"=> $id));
              foreach ($layers as $layer) {
                  $layer_id = $layer["layer_id"];
                  $layer_name = $layer["layer_msname"];
                  $layer_title  = $layer["layer_title"];
                  $oLayer = @$oMap->getLayerByName($layer_name);
                  if($oLayer){
                      if ( $oLayer->connectiontype == MS_POSTGIS ){
                          $matches = array();
                          preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayer->data, $matches);
                          $data = $matches[2];
                      } 
                      if($data!==$tableBefore){
                          continue;
                      }
                      $output->writeln('<info>  Layer: '.$layer_name.'</info>');

                      $oLayer->set("data", str_replace($tableBefore, $tableAfter, $oLayer->data));
                      $gmlIncludeItems = explode(",", $oLayer->getMetadata("gml_include_items"));
                      
                      foreach($fieldToRemove as $key => $obj){
                        if($oLayer->classitem == $obj->field){
                            $output->writeln('<error>Vérifiez le mapfile '.$map_file.'('.$id.') layer '.$layer_name.' champ classitem incompatible</error>');
                        }
                        if($oLayer->labelitem == $obj->field){
                            $output->writeln('<error>Vérifiez le mapfile '.$map_file.'('.$id.') layer '.$layer_name.' champ labelitem incompatible</error>');
                        }
                      }
                      
                      foreach($fieldToAdd as $key => $obj){
                        
                        //array_push($gmlIncludeItems, array($key));
                        
                        $output->writeln('<info>Ajout du champ '.$obj->field.'</info>');
                        $strSql = "insert into carmen.field (field_alias, field_datatype_id, field_duplicate, field_headers, field_name, field_ogc, field_queries, field_rank, field_tooltips, field_type_id, field_url, layer_id) select ".
                                                     ":field, :field_datatype_id, false, false, :field, false, false, max(field_rank)+1, false, 1, '', :layer_id from carmen.field where layer_id=:layer_id";
                        
//                                                     echo $strSql."\n";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "field_datatype_id" => $obj->field_datatype_id,
                            "layer_id" => $layer_id
                        ));

                      }
                      foreach($fieldToRemove as $key => $obj){
                        $output->writeln('<info>Suppression du champ '.$obj->field.'</info>');
                        if(in_array($obj->field, $gmlIncludeItems)){
                            //$gmlIncludeItems = array_diff($gmlIncludeItems, array($obj->field));
                            unset($gmlIncludeItems[array_search($obj->field, $gmlIncludeItems)]);
                        }

                        $strSql = "delete from carmen.field where layer_id=:layer_id and field_name=:field";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "layer_id" => $layer_id
                        ));
                        //echo $strSql."\n";

                        $query = "select * from carmen.map_locator where locator_criteria_layer_id=:layer_id and (locator_criteria_field_id=:field ". 
                                 " or locator_criteria_field_name=:field or locator_criteria_related_field =:field)";

                        $locators = $conn->fetchAll($query,  array(
                            "field" => $obj->field,
                            "layer_id" => $layer_id
                        ));
                        if(!empty($locators)){
                            $output->writeln('<error>Vérifiez le requêteur de la carte '.$map_file.'('.$id.')</error>');
                        }
              
                      }

                      foreach($fieldToRename as $key => $obj){
                        $output->writeln('<info>Renommage du champ '.$obj->field.'</info>');
                        $gmlIncludeItems = array_replace($gmlIncludeItems,
                            array_fill_keys(
                                array_keys($gmlIncludeItems, $obj->field),
                                $obj->renameTo
                            )
                        );
                        
                        //update fieldAlias when is equal to fieldName
                        $strSql = "update carmen.field set field_alias=:fieldTo where layer_id=:layer_id and field_name=:field and field_name=field_alias";
                        //echo $strSql."\n";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "fieldTo" => $obj->renameTo,
                            "layer_id" => $layer_id
                        ));  
                        $strSql = "update carmen.field set field_name=:fieldTo where layer_id=:layer_id and field_name=:field";
                        //echo $strSql."\n";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "fieldTo" => $obj->renameTo,
                            "layer_id" => $layer_id
                        ));
                        //update map locators
                        $strSql = "update carmen.map_locator set locator_criteria_field_id=:fieldTo where locator_criteria_layer_id=:layer_id and locator_criteria_field_id=:field";
                        //echo $strSql."\n";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "fieldTo" => $obj->renameTo,
                             "layer_id" => $layer_id
                        ));
                        $strSql = "update carmen.map_locator set locator_criteria_field_name=:fieldTo where locator_criteria_layer_id=:layer_id and locator_criteria_field_name=:field";
                        //echo $strSql."\n";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "fieldTo" => $obj->renameTo,
                            "layer_id" => $layer_id
                        ));
                        $strSql = "update carmen.map_locator set locator_criteria_related_field=:fieldTo where locator_criteria_layer_id=:layer_id and locator_criteria_related_field=:field";
                        //echo $strSql."\n";
                        $conn->executeQuery($strSql, array(
                            "field" => $obj->field,
                            "fieldTo" => $obj->renameTo,
                            "layer_id" => $layer_id
                        ));

                        if($oLayer->classitem === $obj->field){
                            $oLayer->set("classitem", $obj->renameTo);
                        }
                        
                        if($oLayer->labelitem === $obj->field){
                            $oLayer->set("labelitem", $obj->renameTo);
                        }

                        $oLayerLocator = @$oMap->getLayerByName($layer_name."_locator");
                        //replace in locator data expression
                        if($oLayerLocator){
                            $oLayerLocator->set("data", str_replace($obj->field, $obj->renameTo, str_replace($tableBefore, $tableAfter, $oLayerLocator->data)));
                            //echo $oLayerLocator->data;
                        }
                        

                      }

                      if($oLayer->getMetadata("gml_include_items")!=""){
                          $oLayer->setMetadata("gml_include_items", implode(",", $gmlIncludeItems));
                      }
                      if($oLayer->getMetadata("ows_include_items")!=""){
                          $oLayer->setMetadata("ows_include_items", implode(",", $gmlIncludeItems));
                      }
                  }
              }

              $oMap->save($map_file);

          } 
    }
    
    
    
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }
    
    /**
     *
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    
    
    

}