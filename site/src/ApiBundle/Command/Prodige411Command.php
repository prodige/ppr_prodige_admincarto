<?php

namespace Carmen\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Prodige411Command extends ContainerAwareCommand
{

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('prodige411:migration')
            ->setDescription('Migrate to 4.1.1 version');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $dialog = ($input->isInteractive() ? $this->getHelper('dialog') : null);
        $conn = $this->getConnection();
        $em = $this->getManager();

        $accounts = $em->getRepository("CarmenApiBundle:Account")->findAll();
        $config = $this->getContainer()->getParameter('carmen_config', array());

        foreach ($accounts as $account) {
            $account instanceof Account;
            $path = $account->getAccountPath();
            $account_path = basename($path);
            $account_directory = $config['mapserver']['root_data'] . "/" . $account_path . "/Publication";
            if (is_dir($account_directory)) {
                //change resolution
                $MAPFILES = array_diff(glob($account_directory . "/*.map"), glob($account_directory . "/TMP_*.map"), glob($account_directory . "/tmp_*.map"));
                foreach ($MAPFILES as $mapfile) {
                    try {
                        //Change map resolution
                        $oMap = ms_newMapObj($mapfile);
                        $oMap->set("resolution", 96);
                        $oMap->set("defresolution", 96);

                        $oMap->save($mapfile);

                    } catch (\Exception $ex) {
                        $output->writeln('<error>file : ' . $mapfile . ' not treated, corrupted for Mapserver 7.0 !</error>');
                    }
                }
                $output->write("<info>Success : resolution changed for all mapfiles of dir " . $account_directory . " \n</info>");
            }

        }
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }


}