<?php

namespace Carmen\ApiBundle\Command;

use Carmen\OwsContextBundle\Controller\OwsContextController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContextCommand extends Command
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('carmen:Generatecontext')
            ->setDescription('Generate all contexts')
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'The account path (string) or the account ID (number)'
            );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $conn = $this->getConnection();
        $em = $this->getManager();

        $account = $input->getArgument("account");

        /*Search the account (must be uniq)*/
        $fields = (is_numeric($account) ? array("accountId") : array("accountName", "accountPath"));
        $accounts = array();
        foreach ($fields as $field) {
            $dataset = $em->getRepository('CarmenApiBundle:Account')->findBy(array($field => $account));
            foreach ($dataset as $entity) {
                $accounts[$entity->getAccountId()] = $entity;
            }
        }
        if (empty($accounts)) {
            $output->writeln("<error>ERROR : the account identified by '" . $account . "' does not exist.</error>");
            return;
        }
        if (count($accounts) > 1) {
            $output->writeln(
                "<error>ERROR : Multiple accounts [" . count(
                    $accounts
                ) . "] corresponding to the '" . $account . "' identifier exist.</error>"
            );
            return;
        }
        $oAccount = current($accounts);

        $maps = $em->getRepository('CarmenApiBundle:Map')->findBy(array('publishedMap' => null));
        foreach ($maps as $key => $map) {
            $output->writeln('<info>Generate final owsContext for mapfile ' . $map->getMapfile() . ' </info>');
            $owsContext = $this->container->get('carmen.owscontext');
            $owsContext instanceof OwsContextController;
            $config = $this->container->getParameter('carmen_config', array());
            $owsContext->generateContextAction(
                $oAccount->getAccountId(),
                $oAccount->getAccountId(),
                $map->getMapId(),
                $map->getMapFile(),
                $config['owscontext']['publication_dir'],
                false,
                true
            );
        }

        return Command::SUCCESS;
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->container->get('doctrine')->getConnection();
    }


}
