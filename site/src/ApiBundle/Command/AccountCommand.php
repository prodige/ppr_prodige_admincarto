<?php

namespace Carmen\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AccountCommand extends Command
{
    const ACTION_DELETE = "delete";
    protected static $ACTIONS = array(self::ACTION_DELETE);

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user

        $this
            ->setName('carmen:account')
            ->setDescription('Manage accounts in Carmen application')
            ->addArgument(
                'action',
                InputArgument::REQUIRED,
                "Action to realize : " . implode(" or ", self::$ACTIONS)
            )
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'The account path or name (string) or the account ID (number)'
            );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $dialog = ($input->isInteractive() ? $this->getHelper('dialog') : null);
        $conn = $this->getConnection();
        $em = $this->getManager();

        $action = $input->getArgument("action");
        if (!in_array($action, self::$ACTIONS)) {
            $output->writeln(
                "<error>ERROR : the action must be one of {" . implode(", ", self::$ACTIONS) . "}</error>"
            );
            return;
        }

        $account = $input->getArgument("account");
        switch ($action) {
            case self::ACTION_DELETE :

                $fields = (is_numeric($account) ? array("accountId") : array("accountName", "accountPath"));
                $accounts = array();
                foreach ($fields as $field) {
                    $dataset = $em->getRepository('CarmenApiBundle:Account')->findBy(array($field => $account));
                    foreach ($dataset as $entity) {
                        $accounts[$entity->getAccountId()] = $entity;
                    }
                }

                if (empty($accounts)) {
                    $output->writeln(
                        "<error>ERROR : the account identified by '" . $account . "' does not exist</error>"
                    );
                    return;
                }
                if (count($accounts) > 1) {
                    if ($dialog && !$dialog->askConfirmation(
                            $output,
                            "<question>WARNING : Multiple accounts [" . count(
                                $accounts
                            ) . "] corresponding to the '" . $account . "' identifier exist and will all be deleted. 
                            Do you want to continue their deletion (in database and files)  (yes/no)? </question>",
                            true
                        )) {
                        $output->writeln("<info>INFO : No deletion performed</info>");
                        return;
                    }
                } else {
                    if ($dialog && !$dialog->askConfirmation(
                            $output,
                            "<question>The account identified by '" . $account . "' has been found. Do you want to continue its deletion (in database and files) (yes/no)? </question>",
                            true
                        )) {
                        $output->writeln("<info>INFO : No deletion performed</info>");
                        return;
                    }
                }
                $condition = (is_numeric(
                    $account
                ) ? "account_id = :account" : "(account_path = :account or account_name = :account)");
                $counts = 0;
                $schema = "carmen";
                $where_account_in = " where account_id in (select account_id from account where " . $condition . ")";
                $where_user_in = " where user_id in (select user_id from users " . $where_account_in . ")";
                $where_map_in = " where map_id in (select map_id from map {$where_account_in})";
                $queries = array(
                    "set search_path to {$schema}",
                    "delete from field where layer_id in (select layer_id from layer {$where_map_in})",
                    "delete from layer {$where_map_in}",
                    "delete from map_group {$where_map_in}",
                    "delete from map_locator {$where_map_in}",
                    "delete from map_annotation_attribute {$where_map_in}",
                    "delete from map_tool {$where_map_in}",
                    "delete from map_tree {$where_map_in}",
                    "delete from map_tree_print {$where_map_in}",
                    "delete from favorite_area {$where_map_in}",
                    "delete from map_keyword {$where_map_in}",
                    "delete from ui_model where ui_id in ( select ui_id from map_ui {$where_map_in})",
                    "delete from map_ui {$where_map_in}",
                    "delete from map_audit {$where_account_in}",
                    "delete from map {$where_account_in}",
                    "delete from wxs {$where_account_in}",
                    "delete from contact where user_id in (select user_id from users {$where_account_in})",
                    "delete from preferences where user_id in (select user_id from users {$where_account_in})",
                    "delete from user_audit {$where_user_in}",
                    "delete from users {$where_account_in}",
                    "delete from account_model {$where_account_in}",
                    "count" => "delete from account where " . $condition
                );
                try {
                    $conn = $em->getConnection();
                    $conn->beginTransaction();
                    foreach ($queries as $do => $sql) {
                        $r = $conn->executeUpdate($sql, array("account" => $account));
                        if ($do == "count") {
                            $counts = $r;
                        }
                    }
                    $sql = null;

                    $config = $this->container->getParameter('carmen_config', array());
                    if (file_exists($config['mapserver']['root_data'])) {
                        foreach ($accounts as $account) {
                            $account_path = $account->getAccountPath();
                            if (!$account_path) {
                                continue;
                            }
                            $accountDir = $config['mapserver']['root_data'] . "/" . $account_path;
                            if (is_dir($accountDir)) {
                                $output->writeln(
                                    "<comment>A FAIRE : Supprimer manuellement le répertoire de données : {$accountDir}</comment>"
                                );
                            }
                        }
                    }
                    $em->commit();
                    $output->writeln("<info>SUCCESS : " . (!$counts ? "No" : $counts) . " deletion performed</info>");
                } catch (\Exception $exception) {
                    $em->rollback();
                    if ($sql) {
                        $output->writeln("<error>ERROR : An error occured during the query \n> {$sql}.</error>");
                    }
                    $output->writeln("<error>EXCEPTION : " . $exception->getMessage() . "</error>");
                    $output->writeln("<error>FAILURE : No deletion performed</error>");
                }
                break;
        }
        return Command::SUCCESS;
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->container->get('doctrine')->getConnection();
    }


}