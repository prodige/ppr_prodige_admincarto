<?php

namespace Carmen\ApiBundle\Command;

use Carmen\ApiBundle\Entity\UserAudit;
use Carmen\ApiBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UsersCommand extends Command
{
    const ACTION_CREATE = "create";
    const ACTION_DELETE = "delete";
    protected static $ACTIONS = array(self::ACTION_CREATE, self::ACTION_DELETE);

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user

        $this
            ->setName('carmen:user')
            ->setDescription('Manage users in Carmen application')
            ->addArgument(
                'action',
                InputArgument::REQUIRED,
                "Action to realize : " . implode(" or ", self::$ACTIONS)
            )
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'The account path (string) or the account ID (number)'
            )
            ->addArgument(
                'user_email',
                InputArgument::REQUIRED,
                'The user email (string). Must be unique in database'
            );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $dialog = ($input->isInteractive() ? $this->getHelper('dialog') : null);
        $conn = $this->getConnection();
        $em = $this->getManager();

        $action = $input->getArgument("action");
        if (!in_array($action, self::$ACTIONS)) {
            $output->writeln(
                "<error>ERROR : the action must be one of {" . implode(", ", self::$ACTIONS) . "}</error>"
            );
            return;
        }

        $account = $input->getArgument("account");
        $user_email = $input->getArgument("user_email");

        /*Search the account (must be uniq)*/
        $fields = (is_numeric($account) ? array("accountId") : array("accountName", "accountPath"));
        $accounts = array();
        foreach ($fields as $field) {
            $dataset = $em->getRepository('CarmenApiBundle:Account')->findBy(array($field => $account));
            foreach ($dataset as $entity) {
                $accounts[$entity->getAccountId()] = $entity;
            }
        }
        if (empty($accounts)) {
            $output->writeln("<error>ERROR : the account identified by '" . $account . "' does not exist.</error>");
            return;
        }
        if (count($accounts) > 1) {
            $output->writeln(
                "<error>ERROR : Multiple accounts [" . count(
                    $accounts
                ) . "] corresponding to the '" . $account . "' identifier exist.</error>"
            );
            return;
        }
        $oAccount = current($accounts);
        $accountId = $oAccount->getAccountId();


        /*Search the user (in respect of userEmail uniq constraint) */
        $users = $em->getRepository('CarmenApiBundle:Users')->findBy(array("userEmail" => $user_email));
        if (count($users) > 1) {
            $output->writeln(
                "<error>ERROR : Multiple user identified by '" . $user_email . "' already exist in the account '" . $account . "'.\nUniq constraint on carmen.user.user_email is corrupted.</error>"
            );
            return;
        }

        switch ($action) {
            case self::ACTION_CREATE :
                if (is_numeric($user_email)) {
                    $output->writeln("<error>ERROR : the user identifier must be a string.</error>");
                    return;
                }
                if (!empty($users)) {
                    $output->writeln(
                        "<error>ERROR : the user identified by '" . $user_email . "' already exist in the account '" . $account . "'</error>"
                    );
                    return;
                }

                try {
                    $new_user = new Users();
                    $new_user->setAccount($oAccount);
                    $new_user->setUserEmail($user_email);

                    $em->beginTransaction();
                    $em->persist($new_user);

                    $audit = $em->getRepository('CarmenApiBundle:UserAudit')->findOneBy(array('user' => $new_user));
                    if (!$audit) {
                        $audit = new UserAudit();
                        $audit->setUser($new_user);
                    }
                    $audit->setUserCreationDate(new \DateTime());
                    $em->persist($audit);
                    $em->flush();

                    $em->commit();

                    $output->writeln(
                        "<info>SUCCESS : the user identified by '" . $user_email . "' was created in the account '" . $account . "'</info>"
                    );
                } catch (\Exception $exception) {
                    $em->rollback();
                    $output->writeln("<error>EXCEPTION : " . $exception->getMessage() . "</error>");
                    $output->writeln("<error>FAILURE : No insertion performed.</error>");
                }
                return;
                break;
            case self::ACTION_DELETE :
                if (empty($users)) {
                    $output->writeln(
                        "<error>ERROR : The user identified by '" . $user_email . "' does not exist</error>"
                    );
                    return;
                }
                $oUser = current($users);
                if ($oUser->getAccount()->getAccountId() != $accountId) {
                    $output->writeln(
                        "<error>ERROR : The user identified by '" . $user_email . "' does not exist in the account '" . $account . "'</error>"
                    );
                    return;
                }

                if ($dialog && !$dialog->askConfirmation(
                        $output,
                        "<question>The user identified by '" . $user_email . "' and belonging to the account '" . $account . "' has been found. Do you want to continue its deletion (yes/no)? </question>",
                        true
                    )) {
                    $output->writeln("<info>INFO : No deletion performed</info>");
                    return;
                }
                $counts = 0;
                $schema = "carmen";
                $where_usermail_in = " where user_email = :user_email";
                $where_userid_in = " where user_id in (select user_id from users {$where_usermail_in})";
                $queries = array(
                    "set search_path to {$schema}",
                    "update map set user_id=null {$where_userid_in}",
                    "delete from preferences {$where_userid_in}",
                    "delete from contact {$where_userid_in}",
                    "delete from user_audit {$where_userid_in}",
                    "count" => "delete from users {$where_userid_in}",
                );
                try {
                    $conn = $em->getConnection();
                    $conn->beginTransaction();
                    foreach ($queries as $do => $sql) {
                        $r = $conn->executeUpdate($sql, array("user_email" => $user_email));
                        if ($do == "count") {
                            $counts = $r;
                        }
                    }
                    $sql = null;
                    $em->commit();
                    $output->writeln("<info>SUCCESS : " . (!$counts ? "No" : $counts) . " deletion performed</info>");
                } catch (\Exception $exception) {
                    $em->rollback();
                    if ($sql) {
                        $output->writeln("<error>ERROR : An error occured during the query \n> {$sql}.</error>");
                    }
                    $output->writeln("<error>EXCEPTION : " . $exception->getMessage() . "</error>");
                    $output->writeln("<error>FAILURE : No deletion performed</error>");
                }
                return;
                break;
        }
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->container->get('doctrine')->getConnection();
    }


}