<?php

namespace Carmen\ApiBundle\Command;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Cette commande s'occupe d'assigner des identifiants uniques aux metadaonnées de cartes de type layer WMS
 */
class Prodige409Command extends ContainerAwareCommand
{

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setName('prodige:409')->setDescription('Migration 4.0.8 > 4.0.9');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // call the prodige.configreader service
        $this->getContainer()->get('prodige.configreader');
        // init config
        $catalog_conn = $this->getCatalogueConnection("catalogue,public");
        $conn = $this->getProdigeConnection('carmen');

        //clean map uuid forl WMS representation        
        $query = "select map_id from carmen.map where map_file like :param";

        $maps = $conn->fetchAll($query, array("param" => "layers/WMS/%"));

        foreach ($maps as $map) {
            $id = $map["map_id"];
            $uuid = uniqid();

            $strUpdate = "update map set map_wmsmetadata_uuid=:uuid where map_id=:id";

            $conn->executeQuery($strUpdate, array(
                "uuid" => $uuid,
                "id" => $id
            ));
        }


        //reattribute layer uuid

        $config = $this->getContainer()->getParameter('carmen_config', array());
        $mapPath = $config['mapserver']['root_data'] . "/cartes/Publication/";


        $query = "select map_id, map_file from carmen.map where published_id is null and map_file not like 'local_data/%' " .
            "and map_file not like 'layers/TMP%' ";
        $maps = $conn->fetchAll($query);

        foreach ($maps as $map) {
            $id = $map["map_id"];
            $map_file = $map["map_file"];
            $map_file = $mapPath . $map_file . ".map";
            if (!file_exists($map_file)) {
                $output->writeln('<info>Mapfile inexistant : ' . $map_file . '</info>');
                continue;
            }
            try {
                $oMap = ms_newMapObj($map_file);
            } catch (\Exception $ex) {
                $output->writeln('<error>' . $map_file . ' not treated, corrupted for Mapserver 7.0 !</error>');
                continue;
            }
            $query = "select layer_id, layer_msname, layer_title from carmen.layer where map_id= :map_id and layer_type_code in ('POSTGIS', 'RASTER') and (layer_metadata_uuid is null or layer_metadata_uuid='')";

            $layers = $conn->fetchAll($query, array("map_id" => $id));
            foreach ($layers as $layer) {
                $layer_id = $layer["layer_id"];
                $layer_name = $layer["layer_msname"];
                $layer_title = $layer["layer_title"];
                $oLayer = @$oMap->getLayerByName($layer_name);
                if ($oLayer) {
                    if ($oLayer->connectiontype == MS_POSTGIS) {
                        $matches = array();
                        preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayer->data, $matches);
                        $data = $matches[2];
                    } else {
                        $data = str_replace($mapPath, "", $oLayer->tileindex ?: $oLayer->data);
                    }

                    $sql = "SELECT metadata.uuid from metadata  inner join fiche_metadonnees " .
                        " ON metadata.id = fiche_metadonnees.fmeta_id::bigint " .
                        " inner join couche_donnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees " .
                        " where couchd_emplacement_stockage =:stkcard_path";

                    $tabUUID = $catalog_conn->fetchAll($sql, array("stkcard_path" => $data));
                    if (count($tabUUID) == 0) {
                        $output->writeln('<info>(' . $data . ') not found in catalog, verify in database !</info>');
                    } elseif (count($tabUUID) > 1) {
                        $output->writeln('<info>(' . $data . ') ambiguous recognition in catalog, verify uuid in database !</info>');
                    } else {
                        $uuid = $tabUUID[0]["uuid"];
                        $strUpdate = "update layer set layer_metadata_file= :layer_metadata_file, layer_metadata_uuid=:uuid  where layer_id=:layer_id";

                        $layerMetadataFile = $this->getContainer()->getParameter("PRO_GEONETWORK_URLBASE") . "srv/fre/catalog.search#/metadata/" . $uuid;
                        $conn->executeQuery($strUpdate, array(
                            "uuid" => $uuid,
                            "layer_metadata_file" => $layerMetadataFile,
                            "layer_id" => $layer_id
                        ));
                    }

                } else {
                    $output->writeln('<info>' . $layer_title . ' (' . $layer_id . ') not found in mapfile ' . $map_file . '!</info>');
                }
            }

        }

        //unset SYMBOLSCALEDENOM value in mapfiles if the layer analyse type is not PROPORTIONAL
        $output->writeln('');
        $output->writeln('<question>------------------------------------------------------------------------------------------</question>');
        $output->writeln('<question>  Unset SYMBOLSCALEDENOM value in mapfiles if the layer analyse type is not PROPORTIONAL  </question>');
        $output->writeln('<question>------------------------------------------------------------------------------------------</question>');
        $output->writeln('');
        $query = "select map_file, layer_id, layer_msname, layer_title from carmen.layer inner join carmen.map using (map_id) inner join carmen.lex_analyse_type on (layer_analyse_type_id=analyse_type_id) where layer_type_code in ('POSTGIS', 'VECTOR', 'WFS') and analyse_type_code<>'PROPORTIONAL' order by map_file, layer_msname";
        $stmt = $conn->executeQuery($query);
        $maps = $stmt->fetchAll(\PDO::FETCH_ASSOC | \PDO::FETCH_GROUP);

        foreach ($maps as $map_file => $layers) {
            $map_file = $mapPath . $map_file . ".map";

            try {
                $oMap = ms_newMapObj($map_file);
            } catch (\Exception $ex) {
                $output->writeln('<error>' . $map_file . ' not treated, corrupted for Mapserver 7.0 !</error>');
                continue;
            }

            foreach ($layers as $layer) {
                $layer_id = $layer["layer_id"];
                $layer_name = $layer["layer_msname"];
                $layer_title = $layer["layer_title"];
                $oLayer = @$oMap->getLayerByName($layer_name);
                if ($oLayer) {
                    $oLayer->set("symbolscaledenom", -1);
                } else {
                    $output->writeln('<comment>WARNING : ' . $layer_name . ' (' . $layer_id . ') not found in mapfile ' . $map_file . '!</comment>');
                }
            }
            $output->writeln('<info>SUCCESS : Unset SYMBOLSCALEDENOM for not proportional layers in mapfile ' . $map_file . '</info>');
            $oMap->save($map_file);
        }

    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * Get Doctrine connection
     *
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->getContainer()
            ->get("doctrine")
            ->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
}