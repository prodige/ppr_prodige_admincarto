<?php

namespace Carmen\ApiBundle\Command;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CleanWxsCommand extends ContainerAwareCommand
{

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this
            ->setName('prodige:cleanWxs')
            ->setDescription('Remove unexistant table in wms.map, wfs.map');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // call the prodige.configreader service
        $this->getContainer()->get('prodige.configreader');
        // init config
        $conn = $this->getProdigeConnection('public');

        $config = $this->getContainer()->getParameter('carmen_config', array());
        $mapPath = $config['mapserver']['root_data'] . "/cartes/Publication/";

        $map_file = $mapPath . "wms.map";
        $this->treatMapfile($map_file, $output, $conn);
        $map_file = $mapPath . "wfs.map";
        $this->treatMapfile($map_file, $output, $conn);

    }

    protected function treatMapfile($map_file, $output, $conn)
    {

        if (!file_exists($map_file)) {
            $output->writeln('<info>Mapfile inexistant : ' . $map_file . '</info>');
            return;
        }
        //copie de sauvegarde
        copy($map_file, $map_file . '.backupCleanWxs');
        try {
            $oMap = ms_newMapObj($map_file);
            $output->writeln('<info>Traitement du mapfile : ' . $map_file . '</info>');
        } catch (\Exception $ex) {
            $output->writeln('<error>' . $map_file . ' not treated, corrupted for Mapserver 7.0 !</error>');
            return;
        }
        for ($i = 0; $i < $oMap->numlayers; $i++) {
            $oLayer = $oMap->getLayer($i);
            if ($oLayer) {
                if ($oLayer->connectiontype == MS_POSTGIS) {
                    $matches = array();
                    preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayer->data, $matches);
                    $data = $matches[2];

                    $query = "SELECT EXISTS (
                      SELECT FROM information_schema.tables 
                      WHERE  table_schema = :schema
                      AND    table_name   = :table
                      );";

                    $exists = $conn->fetchAll($query, array(
                        "schema" => "public",
                        "table" => $data
                    ));
                    if (!empty($exists) && $exists[0]["exists"] === false) {
                        $output->writeln('<error>la table ' . $data . ' n\'existe pas</error>');
                        $oLayer->set("status", MS_DELETE);
                    }
                }
            }
        }

        $oMap->save($map_file);

    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }


}
