#!/bin/bash
#paramètre du script : chemin du répertoire
# le script est exécuté dans le répertoire Command : $SCRIPTDIR


DATE=`date '+%y%m%d-%H%M'`                                                                                                                                                                                                                        
SCRIPTDIR=`dirname $(readlink -f $0)`                                                                                                                                                                                                        
SCRIPTNAME=`basename $(readlink -f $0)`
LOGDIR="${SCRIPTDIR}"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"                                                                                                                                                                          
LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"  

ADHERENTdir="$1"
mapdir="${ADHERENTdir}"

echo 
usage()                                                                                                                                                                                                                                      
{                                                                                                                                                                                                                                            
echo "Usage:  `readlink -f $0` directory
directory: répertoire de base des données à migrer, par exemple /prodige4/prodige/cartes
"
exit                                                                                                                                                                                                                                         
}
[ $# -ne "1" ] && usage

test_var()
{
[ -z "$1" ] && echo "Variable $1 vide, fin du script" && usage && exit
}
test_link()
{
[ ! -$2 $1 ] && echo "Chemin $1 inexistant, fin du script" && exit
}


job()
{
echo "#Modification des mapfiles"
dir="$1"
test_var "${dir}" ; test_link "${dir}" "d"
depth="$2"
if [[ $2 == "r" ]]
   then maxdepth=""
   else maxdepth="-maxdepth 1"
fi

cd "${dir}"
echo -e "\t #conversion des encodages"
find $maxdepth -name "*.map" | while read mapfile; do
   echo -e "\t$mapfile"
   cp "${mapfile}" "${mapfile}.bak"
   iconv -f ISO8859-1 -t UTF-8 -o "${mapfile}" "${mapfile}.bak"
   rm -f "${mapfile}.bak"
done

echo -e "\t #substitution DRIVER and TYPE ANNOTATION"
find $maxdepth -name "*.map" | while read mapfile; do
   echo -e "\t$mapfile"
   sed -i 's|DRIVER "GD/JPEG"|DRIVER "AGG/JPEG"|
s|DRIVER "GD/PNG"|DRIVER "AGG/PNG"|
s|DRIVER "GD/GIF"|DRIVER "AGG/PNG"|
s|TYPE ANNOTATION|TYPE POINT|' "${mapfile}"
done

echo
}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
job "${mapdir}/Publication" 
job "${mapdir}/Publication/wfs" 
job "${mapdir}/Publication/layers" r
job "${mapdir}/Publication/carteperso" r
exec 1>&6 6>&- 2>&7 7>&-

echo
exit 0