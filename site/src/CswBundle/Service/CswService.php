<?php

namespace Carmen\CswBundle\Service;

use Carmen\CswBundle\Csw\CswClient;

/**
 * CswClient wrapper - provides CswClient as a service
 * @author alkante <support@alkante.com>
 */
class CswService
{
    private $publication_url;
    private $user_login;
    private $user_pwd;

    /**
     * @param string $publication_url
     * @param string $user_login
     * @param string $user_pwd
     */
    public function __construct($publication_url, $user_login, $user_pwd)
    {
        $this->publication_url = $publication_url;
        $this->user_login      = $user_login;
        $this->user_pwd        = $user_pwd;
    }

    /**
     * Instanciates and returns a new CswClient
     * 
     * @param string $geosource_url Base URL of the geosource server, will be concatenated with the provided publication URL
     * 
     * @return \Carmen\CswBundle\Csw\CswClient
     */
    public function getCswClient($geosource_url)
    {
        $cswUrl = $geosource_url.$this->publication_url;
        $sessionId = "JSESSIONID";

        return new CswClient($cswUrl, $this->user_login, $this->user_pwd, $geosource_url, $sessionId);
    }

}
