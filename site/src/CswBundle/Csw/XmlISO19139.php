<?php

namespace Carmen\CswBundle\Csw;

use Carmen\CswBundle\Utils\UUID;

// TODO change this
define('DIR_APPLICATION',realpath(dirname(__FILE__)).'/../Resources/');

/**
 * Manage a XML file in ISO 19139 standard
 *
 * @author lagardep //Modif_FH : pour INSPIRE
 * @copyright BRGM
 */

class XmlISO19139 {
    //put your code here
    /**
     *
     * @var DOMDocument 
     */
    public $xmlISO19139Document;

    public static $dateType = array("creation"=>"creation",
                                    "revision"=>"revision",
                                    "publication"=>"publication");

    private $_mddFileTemplate = "/csw/metadata_data.xml";
    private $_mdsWMSFileTemplate = "/csw/metadata_view.xml";
    private $_mdsWFSFileTemplate = "/csw/metadata_download_direct.xml";
    private $_mdsATOMFileTemplate = "/csw/metadata_download_simple.xml";
    private $_mdcFileTemplate = "/csw/iso19139mdc.xml";


    /**
     * create the DOMDocument
     */
    function  __construct() {
        $this->xmlISO19139Document=new \DOMDocument();
    }

    
    /**
     * create a iso data metadata from scratch
     * @return boolean
     */
    public function createNewMDD() {
        return $this->createNewMD(DIR_APPLICATION.$this->_mddFileTemplate);
    }


    /**
     * create a iso service metadata from scratch
     * @return boolean
     */
    public function createNewMDWMS() {
        return $this->createNewMD(DIR_APPLICATION.$this->_mdsWMSFileTemplate);
    }

    /**
     * create a iso service metadata from scratch
     * @return boolean
     */
    public function createNewMDWFS() {
       return $this->createNewMD(DIR_APPLICATION.$this->_mdsWFSFileTemplate);
    }
    
    /**
     * create a iso service metadata from scratch
     * @return boolean
     */
    public function createNewMDATOM() {
       return $this->createNewMD(DIR_APPLICATION.$this->_mdsATOMFileTemplate);
    }

    /**
     * create a iso data metadata from scratch
     * @return boolean
     */
    public function createNewMDC() {
        return $this->createNewMD(DIR_APPLICATION.$this->_mdcFileTemplate);
    }


    /**
     * @param boolean headerToRemove
     * @return String XML as string
     */
    public function convertToText($withHeader=true) {

        $res=$this->xmlISO19139Document->saveXML();
        if ($withHeader==false) {
            //remove the header
            //TODO improve the header replacement
            $res=str_replace('<?xml version="1.0" encoding="UTF-8"?>','',$res);
            $res=str_replace('<?xml version="1.0"?>','',$res);
        }
        return $res;
    }



    /**
     * Generic function to create metadata
     * @param String $template
     * @return boolean
     */
    private function createNewMD($template) {
        if ($this->xmlISO19139Document->load($template)) {
            //add a uuid identifier
            $this->changeID();
            $this->changeDateStamp();
            //cswLog::logInfo($this->xmlISO19139Document->saveXML());
            return true;
        }
        else return false;
    }


    /**
     * load a XML text file to create the content of the class
     *
     */
    public function loadXmlContent($xmlContent) {
        return $this->xmlISO19139Document->loadXML($xmlContent);
        
    }

    /**
     * get the uuid of the metadata
     */
    public function getID() {
        $nFI=$this->xmlISO19139Document->getElementsByTagName('fileIdentifier');
        if ($nFI->length==1) {
             return $nFI->item(0)->childNodes->item(1)->nodeValue;
        }
        else
            return false;
    }

    /**
     * create a uuid in the metadata
     * @return boolean
     */
    public function changeID() {
        $nFI=$this->xmlISO19139Document->getElementsByTagName('fileIdentifier');
        if ($nFI->length==1) {
             $nFI->item(0)->childNodes->item(1)->nodeValue=UUID::v4();
             return true;
        }
        else
            return false;

    }
    
    /**
     * change the unique identifier of the metadata
     * @author FH
     * @return boolean
     */
    public function changeUniqueIdentifier($identifier) {
        return $this->_changeElement("//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString", $identifier);
    }    
    

    /**
     * change the language
     * @author FH
     * @param String $lang
     * @return boolean
     */
    public function changeLanguage($lang) {
        return $this->_changeElement("//gmd:language/gmd:LanguageCode/@codeListValue", $lang);
    }


    /**
     *
     */
    public function getDateStamp() {
       //Modif_FH 
       //return $this->_getElement("//gmd:dateStamp");
        return $this->_getElement("//gmd:dateStamp/gco:DateTime");
    }
    
    /**
     * change the date stamp of the metadata
     * @return boolean
     */
    public function changeDateStamp() {
        $nFI=$this->xmlISO19139Document->getElementsByTagName('dateStamp');
        if ($nFI->length==1) {
             $nFI->item(0)->childNodes->item(1)->nodeValue=date('c');
             return true;
        }
        else
            return false;
    }

    /**
     * 
     */
    public function getTitle() {
       return $this->_getElement("//gmd:identificationInfo//gmd:citation//gmd:title/gco:CharacterString");
    }

    /**
     *
     * @param String $title
     * @return boolean
     */
    public function changeTitle($title) {
        return $this->_changeElement("//gmd:identificationInfo//gmd:citation//gmd:title/gco:CharacterString", $title);
    }

    /**
     *
     */
    public function getAbstract() {
       return $this->_getElement("//gmd:identificationInfo//gmd:abstract/gco:CharacterString");
    }


    /**
     *
     * @param String $abstract
     * @return boolean
     */
    public function changeAbstract($abstract) {
        return $this->_changeElement("//gmd:identificationInfo//gmd:abstract/gco:CharacterString", $abstract);

    }

    /**
     *
     * @param dateTime $date
     * @param dateType $typeOfDate
     * @return boolean
     */
    public function changeDate($date,$typeOfDate) { 
        //Modif_FH
        $this->_changeElement("//gmd:identificationInfo//gmd:citation//gmd:date//gmd:date/gco:Date", $date->format("Y-m-d"));
        //$this->_changeElement("//gmd:identificationInfo//gmd:citation//gmd:date//gmd:dateType/gmd:CI_DateTypeCode/@codeListValue", $typeOfDate);
        return true;
    }


    /**
     *
     */
    public function getKeywords() {
       //Modif_FH 
       //return $this->_getElement("//gmd:identificationInfo//gmd:descriptiveKeywords[descendant::gmd:type//@codeListValue='theme']//gmd:keyword/gco:CharacterString");

       $nodes = $this->_getElements("//gmd:identificationInfo//gmd:descriptiveKeywords[not(.//gmd:thesaurusName)]//gmd:keyword/gco:CharacterString");
       
       $keywords = array();
       foreach($nodes as $node) {
           array_push($keywords, $node->nodeValue);          
       }
        return implode(",", $keywords);       
    }

    /**
     *
     * @param String $keywords array of keywords
     * @return boolean
     */
    public function changeKeywords($keywords) {
        return $this->_changeElement("//gmd:identificationInfo//gmd:descriptiveKeywords[descendant::gmd:type//@codeListValue='theme']//gmd:keyword/gco:CharacterString", $keywords);

    }   
    
    /**
     * Les keywords doivent etre chacun dans une balise
     * @author FH
     * @param String $keywords array of keywords
     * @return boolean
     */
    public function addKeywords($keywords) {       
        //<gmd:descriptiveKeywords>
        //<gmd:MD_Keywords>
        //  <gmd:keyword>
        //    <gco:CharacterString>### PARAMETRE "Mots-cles : (separes par des ,)" DE LA MINI FICHE</gco:CharacterString>
        //  </gmd:keyword>
        //  <gmd:type>
        //    <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme" />
        //  </gmd:type>
        //</gmd:MD_Keywords>
        //</gmd:descriptiveKeywords>
       
       $keywordsArray = explode (",", $keywords);

       if (sizeof($keywordsArray) > 0){
           $nodeDescriptiveKeywords = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:descriptiveKeywords");
           $nodeMD_Keywords = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:MD_Keywords");
           
           foreach($keywordsArray as $keyword) {
                $nodeKeyword = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:keyword");
                $nodekeywordChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
                $nodekeywordChar->nodeValue = $keyword;
                $nodeKeyword->appendChild($nodekeywordChar); 
                $nodeMD_Keywords->appendChild($nodeKeyword);
           }
           
           $nodeType = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:type");
           $nodeMD_KeywordTypeCode = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:MD_KeywordTypeCode");
           $nodeMD_KeywordTypeCode->setAttribute("codeList", "http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode");
           $nodeMD_KeywordTypeCode->setAttribute("codeListValue", "theme");
           $nodeType->appendChild($nodeMD_KeywordTypeCode); 
           $nodeMD_Keywords->appendChild($nodeType);
           $nodeDescriptiveKeywords->appendChild($nodeMD_Keywords);
           
           $srvDom = $this->xmlISO19139Document->getElementsByTagName("resourceConstraints")->item(0);
           $srvDom->parentNode->insertBefore($nodeDescriptiveKeywords, $srvDom);
       }           
    }
    
    /**                                                                                             
     * Suppression de tous les keywords
     * @author FH
     * @return boolean
     */
    public function removeKeywords() {
        $domElemsToRemove = array();
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $query="//gmd:identificationInfo//gmd:descriptiveKeywords";
        $nodesOperation = $xp->query($query);
        foreach($nodesOperation as $nodeOp) {
             $domElemsToRemove[] = $nodeOp;
        }
        foreach($domElemsToRemove as $domElement ){
             $domElement->parentNode->removeChild($domElement);
        }     
        return true;
    }

    /**
     *
     */
    public function getINSPIRE() {
       return $this->_getElement("//gmd:identificationInfo//gmd:descriptiveKeywords[descendant::gmd:thesaurusName//gco:CharacterString='GEMET - INSPIRE themes, version 1.0']//gmd:keyword/gco:CharacterString");
    }


    /**
     *
     * @param String $keywords array of keywords
     * @return boolean
     */
    public function changeINSPIRE($inspire) {
        return $this->_changeElement("//gmd:identificationInfo//gmd:descriptiveKeywords[descendant::gmd:thesaurusName//gco:CharacterString='GEMET - INSPIRE themes, version 1.0']//gmd:keyword/gco:CharacterString", $inspire);

    }
    
    /**
     * @author FH
     * @param String $inspire Inspire's keyword
     */
    public function addInspireKeyword($inspire) {
        $nodeDescriptiveKeywords = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:descriptiveKeywords");
        $nodeMD_Keywords = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:MD_Keywords");
        
        $nodeKeyword = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:keyword");
        $nodekeywordChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
        $nodekeywordChar->nodeValue = $inspire;
        $nodeKeyword->appendChild($nodekeywordChar); 
        $nodeMD_Keywords->appendChild($nodeKeyword);
           
        $nodeType = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:type");
        $nodeMD_KeywordTypeCode = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:MD_KeywordTypeCode");
        $nodeMD_KeywordTypeCode->setAttribute("codeList", "http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode");
        $nodeMD_KeywordTypeCode->setAttribute("codeListValue", "theme");
        $nodeType->appendChild($nodeMD_KeywordTypeCode);

        $nodeThesaurus = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:thesaurusName");
        $nodeCI_Citation = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:CI_Citation");
        $nodeTitle = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:title");
        $nodeTitleChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
        $nodeTitleChar->nodeValue = "GEMET - INSPIRE themes, version 1.0";
        $nodeTitle->appendChild($nodeTitleChar);
        $nodeCI_Citation->appendChild($nodeTitle); 

        $nodeDate = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:date");
        $nodeCI_Date = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:CI_Date");
        $nodeDateDate = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:date");
        $nodeDateChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:Date");
        $nodeDateChar->nodeValue = "2008-06-01";
        $nodeDateDate->appendChild($nodeDateChar);
        $nodeCI_Date->appendChild($nodeDateDate);         

        $nodeDateType = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:dateType");
        $nodeCI_DateTypeCode = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:CI_DateTypeCode");
        $nodeCI_DateTypeCode->setAttribute("codeList", "http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode");
        $nodeCI_DateTypeCode->setAttribute("codeListValue", "publication");

        $nodeDateType->appendChild($nodeCI_DateTypeCode);
        $nodeCI_Date->appendChild($nodeDateType);
        $nodeDate->appendChild($nodeCI_Date);
        $nodeCI_Citation->appendChild($nodeDate);
        $nodeThesaurus->appendChild($nodeCI_Citation);

        $nodeMD_Keywords->appendChild($nodeType);
        $nodeMD_Keywords->appendChild($nodeThesaurus);

        $nodeDescriptiveKeywords->appendChild($nodeMD_Keywords);

        $srvDom = $this->xmlISO19139Document->getElementsByTagName("resourceConstraints")->item(0);
        $srvDom->parentNode->insertBefore($nodeDescriptiveKeywords, $srvDom);
    }

    /**
     *
     */
    public function getISOThemes() {
        $topicCategory = $this->_getElement("//gmd:identificationInfo//gmd:topicCategory//gmd:MD_TopicCategoryCode");
        if ($topicCategory == "-- Thématique ISO --")
            $topicCategory = ""; 
        return $topicCategory;
    }

    /**
     *
     * @param String $ISOthemes
     * @return boolean
     */
    public function changeISOThemes($ISOthemes) {
        return $this->_changeElement("//gmd:identificationInfo//gmd:topicCategory//gmd:MD_TopicCategoryCode", $ISOthemes);

    }

    /**
     *
     */
    public function getLineage() {
       return $this->_getElement("//gmd:dataQualityInfo//gmd:lineage//gmd:statement/gco:CharacterString");
    }


    /**
     *
     * @param String $lineage
     * @return boolean
     */
    public function changeLineage($lineage) {
        return $this->_changeElement("//gmd:dataQualityInfo//gmd:lineage//gmd:statement/gco:CharacterString", $lineage);

    }

    /**
     *
     * @param float $west
     * @param float $north
     * @param float $east
     * @param float $south
     * @return boolean
     */
    public function changeExtent($west,$south,$east,$north) {
        $this->_changeElement("//gmd:identificationInfo//gmd:extent//gmd:geographicElement//gmd:westBoundLongitude/gco:Decimal", strval($west));
        $this->_changeElement("//gmd:identificationInfo//gmd:extent//gmd:geographicElement//gmd:eastBoundLongitude/gco:Decimal", strval($east));
        $this->_changeElement("//gmd:identificationInfo//gmd:extent//gmd:geographicElement//gmd:southBoundLatitude/gco:Decimal", strval($south));
        return $this->_changeElement("//gmd:identificationInfo//gmd:extent//gmd:geographicElement//gmd:northBoundLatitude/gco:Decimal", strval($north));
    }
    
    /**
     *
     * @param float $west
     * @param float $north
     * @param float $east
     * @param float $south
     * @return boolean
     */
    public function changeSrvExtent($west,$south,$east,$north) {
        $this->_changeElement("//gmd:identificationInfo//srv:extent//gmd:geographicElement//gmd:westBoundLongitude/gco:Decimal", strval($west));
        $this->_changeElement("//gmd:identificationInfo//srv:extent//gmd:geographicElement//gmd:eastBoundLongitude/gco:Decimal", strval($east));
        $this->_changeElement("//gmd:identificationInfo//srv:extent//gmd:geographicElement//gmd:southBoundLatitude/gco:Decimal", strval($south));
        return $this->_changeElement("//gmd:identificationInfo//srv:extent//gmd:geographicElement//gmd:northBoundLatitude/gco:Decimal", strval($north));
    }
    
    

    /**
     *
     * @param String $dataUUID of the metadata of date
     * @param String $layerName of the getcapabilities name
     * @param String $service type (wms or wfs)
     * @return boolean
     */
    public function addDataToService($dataUUID,$layerName,$service) {
        global $geosourceUrl;
        //first step. Check if existing in the current XML with operatesOn
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $query="//srv:operatesOn[@uuidref='".$dataUUID."']";
        $nodesOperation = $xp->query($query);
        if ($nodesOperation->length==0) {
            // the dataUUID doesn't exist. create it
            $srvDom = $this->xmlISO19139Document->getElementsByTagName("SV_ServiceIdentification")->item(0);
            $nodeOperation = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:operatesOn");
            $nodeAttr=$this->xmlISO19139Document->createAttribute("uuidref");
            $nodeAttr->nodeValue = $dataUUID;
            $nodeOperation->appendChild($nodeAttr);
            $nodeAttrUrl=$this->xmlISO19139Document->createAttribute("xlink:href");
            $nodeAttrUrl->nodeValue = substr($geosourceUrl, 0, strrpos( $geosourceUrl, '/')) . "/metadata/" . $dataUUID;
            $nodeOperation->appendChild($nodeAttrUrl);
            $srvDom ->appendChild($nodeOperation);
         }

         //second step. Check if existing in the current XML with
         //<srv:coupledResource>
	//			<srv:SV_CoupledResource>
	//				<srv:operationName>
	//					<gco:CharacterString>GetMap</gco:CharacterString>
	//				</srv:operationName>
	//				<srv:identifier>
	//					<gco:CharacterString>-- Identifiant de la ressource couplee --</gco:CharacterString>
	//				</srv:identifier>
	//				<gco:ScopedName>-- Nom de la couche --</gco:ScopedName>
	//			</srv:SV_CoupledResource>
	//		</srv:coupledResource>
        $query="//srv:coupledResource[descendant::srv:identifier/gco:CharacterString='".$dataUUID."']";
        $nodesOperation = $xp->query($query);
        
        if ($service != "atom"){
            if ($nodesOperation->length==0) {
                // the dataUUID doesn't exist. create it
                $srvDom = $this->xmlISO19139Document->getElementsByTagName("couplingType")->item(0);
                $nodeCoupledResource = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:coupledResource");
                $nodeSRVCoupledResource = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:SV_CoupledResource");
                $nodeOperationName = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:operationName");
                $nodeOperationNameChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");

                //Modif_FH
                if ($service == "wms")
                    $nodeOperationNameChar->nodeValue="GETMAP";
                if ($service == "wfs")
                    $nodeOperationNameChar->nodeValue="GETFEATURE";
                /*if ($service == "atom")
                    $nodeOperationNameChar->nodeValue="GETCAPABILITIES";*/

                $nodeOperationName->appendChild($nodeOperationNameChar);
                $nodeIdentifier=$this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:identifier");
                $nodeIdNameChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
                $nodeIdNameChar->nodeValue=$dataUUID;
                $nodeIdentifier->appendChild($nodeIdNameChar);
                $nodeScopedName = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:ScopedName");
                $nodeScopedName->nodeValue=$layerName;
                $nodeSRVCoupledResource->appendChild($nodeOperationName);
                $nodeSRVCoupledResource->appendChild($nodeIdentifier);
                $nodeSRVCoupledResource->appendChild($nodeScopedName);
                $nodeCoupledResource->appendChild($nodeSRVCoupledResource);
                $srvDom->parentNode->insertBefore($nodeCoupledResource, $srvDom);
                return true;
             }
             else {
                 // update only the ScopedName

                 $nodeCoupledResource=$nodesOperation->item(0);
                 $nodeScopedName =$nodeCoupledResource->getElementsByTagName("ScopedName")->item(0);
                 $nodeScopedName->nodeValue=$layerName;
                 return true;

             }
        }
        return false;
    }

     /**
     *  remove a specific data linked to the current service
      * @param String $dataUUID of the metadata of date
     * @return boolean 
     */
    public function removeDataToService($dataUUID) {
        //first step. Check if existing in the current XML with operatesOn
        $domElemsToRemove = array();
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $query="//srv:operatesOn[@uuidref='".$dataUUID."']";
        $nodesOperation = $xp->query($query);
        foreach($nodesOperation as $nodeOp) {
             $domElemsToRemove[] = $nodeOp;
        }
        $query="//srv:coupledResource[descendant::srv:identifier/gco:CharacterString='".$dataUUID."']";
        $nodesOperation = $xp->query($query);
        foreach($nodesOperation as $nodeOp) {
            $domElemsToRemove[] = $nodeOp;
        }

        foreach($domElemsToRemove as $domElement ){
             $domElement->parentNode->removeChild($domElement);
        }
        return true;
    }

    /**
     * Remove all data linked to the current service
     * @return boolean
     */
    public function removeAllDataToService() {
       $domElemsToRemove = array();
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $query="//srv:operatesOn";
        $nodesOperation = $xp->query($query);
        foreach($nodesOperation as $nodeOp) {
             $domElemsToRemove[] = $nodeOp;
        }
        $query="//srv:coupledResource";
        $nodesOperation = $xp->query($query);
        foreach($nodesOperation as $nodeOp) {
            $domElemsToRemove[] = $nodeOp;
        }

        foreach($domElemsToRemove as $domElement ){
             $domElement->parentNode->removeChild($domElement);
        }
        return true;
    }

    /**
     * @author FH !!!!!!!!!!!!!!!!!Plus utilisé!!!!!!!!!!!!!!!!!!!!!!!!
     * @param String $url of the service
     * @return boolean
     */
    public function addDownloadLink($url) {        
        //<srv:containsOperations>### pour chaque layer diffusé en téléchargement
        //    <srv:SV_OperationMetadata>
        //       <srv:operationName>
        //          <gco:CharacterString>Get Discovery Service Metadata</gco:CharacterString>
        //       </srv:operationName>
        //       <srv:DCP>
        //          <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
        //       </srv:DCP>
        //       <srv:connectPoint>
        //          <gmd:CI_OnlineResource>
        //             <gmd:linkage>
        //                <gmd:URL>### URL ATOM</gmd:URL>
        //             </gmd:linkage>
        //          </gmd:CI_OnlineResource>
        //       </srv:connectPoint>
        //    </srv:SV_OperationMetadata>
        //</srv:containsOperations>

        $nodecontainsOperations = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:containsOperations");
        $nodeSVOperationMetadata = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:SV_OperationMetadata");
        $nodeOperationName = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:operationName");
        $nodeOperationNameChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
        $nodeOperationNameChar->nodeValue="Get Discovery Service Metadata";
        $nodeOperationName->appendChild($nodeOperationNameChar);
        
        $nodeDCP = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:DCP");
        $nodeDCPList = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:DCPList");
        $attrCodeList = $this->xmlISO19139Document->createAttribute("codeList");
        $attrCodeList->nodeValue = "http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList";
        $nodeDCPList->appendChild($attrCodeList);
        $attrCodeListValue = $this->xmlISO19139Document->createAttribute("codeListValue");
        $attrCodeListValue->nodeValue = "WebServices";
        $nodeDCPList->appendChild($attrCodeListValue);        
        $nodeDCP->appendChild($nodeDCPList);
        
        $nodeConnectPoint = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/srv","srv:connectPoint");
        $nodeCIOnlineResource = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:CI_OnlineResource");
        $nodeLinkage = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:linkage");
        $nodeURL = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:URL");       
        $nodeURL->nodeValue= $url;
        $nodeLinkage->appendChild($nodeURL);
        
        /*$nodeProtocol = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:protocol");
        $nodeProtocolChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
        $nodeProtocolChar->nodeValue="WWW:LINK-1.0-http--link";
        $nodeProtocol->appendChild($nodeProtocolChar);
        
        $nodeName = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gmd","gmd:name");
        $nodeNameChar = $this->xmlISO19139Document->createElementNS("http://www.isotc211.org/2005/gco","gco:CharacterString");
        $nodeNameChar->nodeValue="Téléchargement simple de la donnée : " . $layerName;
        $nodeName->appendChild($nodeNameChar);*/
        
        $nodeCIOnlineResource->appendChild($nodeLinkage);
        //$nodeCIOnlineResource->appendChild($nodeProtocol);
        //$nodeCIOnlineResource->appendChild($nodeName);
        $nodeConnectPoint->appendChild($nodeCIOnlineResource);
        $nodeSVOperationMetadata->appendChild($nodeOperationName);
        $nodeSVOperationMetadata->appendChild($nodeDCP);
        $nodeSVOperationMetadata->appendChild($nodeConnectPoint);
        $nodecontainsOperations->appendChild($nodeSVOperationMetadata);
                       
        $srvDom = $this->xmlISO19139Document->getElementsByTagName("operatesOn")->item(0);
        $srvDom->parentNode->insertBefore($nodecontainsOperations, $srvDom);
        return true;       
    }
    
    /**
     * @author FH
     * @return boolean
     */
    public function removeAllDownloadLink() {
        $domElemsToRemove = array();
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $query="//srv:containsOperations";
        $nodesOperation = $xp->query($query);
        foreach($nodesOperation as $nodeOp) {
             $domElemsToRemove[] = $nodeOp;
        }

        foreach($domElemsToRemove as $domElement ){
             $domElement->parentNode->removeChild($domElement);
        }
        return true;
    }

     public function getURLService() {
           return $this->_getElement("//gmd:distributionInfo//gmd:transferOptions//gmd:onLine//gmd:linkage/gmd:URL");
      }

    /**
     *
     * @param String $Url for the service
     * @return boolean
     */
    public function changeURLService($Url) {       
        //change the srv:connectPoint address
        $this->_changeElement("//srv:connectPoint//gmd:linkage/gmd:URL", $Url);
       //change the distribution URL
       return  $this->_changeElement("//gmd:distributionInfo//gmd:transferOptions//gmd:onLine//gmd:linkage/gmd:URL", $Url);
    }


    /**
     *
     * @return String of the URL of doc
     */
    public function getURLData(){
        return $this->_getElement("//gmd:distributionInfo//gmd:transferOptions//gmd:onLine//gmd:linkage/gmd:URL");
    }

    /**
     *
     * @param String $Url for the service
     * @return boolean
     */
    public function changeURLData($Url) {
        return  $this->_changeElement("//gmd:distributionInfo//gmd:transferOptions//gmd:onLine//gmd:linkage/gmd:URL", $Url);
    }


    /**
     *
     * @return String : Code of the SRS
     */
    public function getSRS() {
        return $this->_getElement("//gmd:referenceSystemInfo//gmd:referenceSystemIdentifier//gmd:code/gco:CharacterString");
    }

    /**
     *
     * @param <type> String of the SRS
     * @return <type> 
     */
    public function changeSRS($srsCode) {
        return  $this->_changeElement("//gmd:referenceSystemInfo//gmd:referenceSystemIdentifier//gmd:code/gco:CharacterString", $srsCode);

    }

    /**
     *
     * @return Constraints value
    */
    public function getConstraints() {
        return $this->_getElement("//gmd:resourceConstraints//gmd:useLimitation/gco:CharacterString");
    }
    
    /**
     * Change the value of a constraint (useLimitation)
     * @param String $content
     * @return boolean
     */
    public function changeConstraints($content) {
        if ($content == "NONE")
            $content = "Aucune condition ne s'applique";
        if ($content == "")
            $content = "Conditions inconnues";
            
        return $this->_changeElement("//gmd:resourceConstraints//gmd:useLimitation/gco:CharacterString", $content);
    }

    /**
     * @author FH
     * @return AccessConstraints value
    */
    public function getAccessConstraints() {
        return $this->_getElement("//gmd:resourceConstraints//gmd:otherConstraints/gco:CharacterString");
    }
    
    /**
     * @author FH
     * Change the value of a constraint (accessConstraint)
     * @param String $content
     * @return boolean
     */
    public function changeAccessConstraints($content) {
        return $this->_changeElement("//gmd:resourceConstraints//gmd:otherConstraints/gco:CharacterString", $content);
    }
    
    /**
     * @author FH
     * @param String $format
     * @return boolean
     */  
    public function changeDataFormat($format){
        $version = "";
        if ($format == "SHP")
            $version = "1.0";
        if ($format == "MIF"){
            $format = "MIF/MID";
            $version = "4.5";          
        }    
        if ($format == "TAB")
            $version = "4.5";
        if ($format == "ECW")
            $version = "1.0";
        if ($format == "TIFF")
            $version = "1.0";
        if ($format == "POSTGIS")
            $version = "1.1.3";

        $this->_changeElement("//gmd:distributionInfo/gmd:MD_Distribution/gmd:distributionFormat/gmd:MD_Format/gmd:name/gco:CharacterString", $format);
        return $this->_changeElement("//gmd:distributionInfo/gmd:MD_Distribution/gmd:distributionFormat/gmd:MD_Format/gmd:version/gco:CharacterString", $version);
    }

    /**
     *
     * @return Name of the contact
     */
    public function getContactName() {
        return $this->_getElement("//gmd:contact//gmd:organisationName/gco:CharacterString");

    }

    /**
     *
     * @param String $contactName
     * @return boolean
     */
    public function changeContactName($contactName) {
        //Modif FH : le nom du contact est situé à 2 endroits :
        //<gmd:contact>
        //<gmd:identificationInfo><srv:SV_ServiceIdentification><gmd:pointOfContact>
        return $this->_changeElement("//gmd:organisationName/gco:CharacterString", $contactName);
    }
    
    /**
     * @author FH
     * @param String $email
     * @return boolean
     */
    public function changeEmailAddress($email) {
        return $this->_changeElement("//gmd:contactInfo//gmd:address//gmd:electronicMailAddress/gco:CharacterString", $email);
    }    

    /**
     *
     * @param String $xpathString
     * @param String $value
     * @return boolean
     */
    private function _changeElement($xpathString,$value) {
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $nodes = $xp->query($xpathString);
        if ($nodes->length>=1) {
                foreach($nodes as $node) {
                    $node->nodeValue=$value;
                }
                return true;
        }
        else return false;
    }



    /**
     *
     * @param String $xpathString
     * @return DOMNodeList List of nodes
     */
    private function _getElements($xpathString) {
        $xp = new \DOMXPath($this->xmlISO19139Document);
        $nodes = $xp->query($xpathString);
        return $nodes;
    }
    
    /**
     *
     * @param String $xpathString
     * @return String
     */
    private function _getElement($xpathString) {
         $nodesElement=$this->_getElements($xpathString);
         if ($nodesElement->length>=1) {
            return $nodesElement->item(0)->nodeValue;
         }
         else return "";
    }

}