<?php

namespace Carmen\CswBundle\Csw;

use HTTP_Request2;

/**
 * cswClient allows to request a OGC CSW 2.0.2 - ISO API service
 * @package csw
 * @author lagarde pierre
 * @copyright BRGM
 * @name cswClient
 * @version 1.0.0
 */
class CswClient {
    private $_cswAddress;
    private $_authentAddress;
    private $_cswLogin;
    private $_cswPassword;
    private $_bAuthent;
    private $_sessionID;

    private $_cookieName;
    private $_response;

    /**
     *
     * @param String $cswAddress address of the CSW URL
     * @param String $cswLogin login of the user to CSW-T
     * @param String $cswPassword  password of the user to CSW-T
     * @param String $authentAddress address of the login/logout address
     */
    
    function  __construct($cswAddress,$cswLogin=null,$cswPassword=null,$authentAddress=null, $cookieName = "JSESSIONID") {
        $this->_cswAddress=$cswAddress;
        $this->_bAuthent=false;
        $this->_cookieName = $cookieName;
        if (isset($cswLogin)) {
            $this->_cswLogin=$cswLogin;
            $this->_cswPassword=$cswPassword;
            $this->_authentAddress=$authentAddress;
            $this->_bAuthent=true;
        }
    }

    /**
     *
     * @return bool Request success / error
     */
    private function _callHTTPCSW($request) {
       
        try {
            $resp= $request->send();
            if (200 == $resp->getStatus()) {
              $this->_response = $resp->getBody();
              $cookies = $resp->getCookies();
              foreach ($cookies as $cook) {                  
                  if ($cook['name']==$this->_cookieName) $this->_sessionID = $cook['value'];
              }
              return true;
            }
            //Pour login/logout
            else if (302 == $resp->getStatus() && $resp->isRedirect()==1) {
              $this->_response = $resp->getBody();
              $cookies = $resp->getCookies();
              foreach ($cookies as $cook) {                 
                  if ($cook['name']==$this->_cookieName) $this->_sessionID = $cook['value'];
              }
              return true;
            } else {
                $this->_response = $resp->getStatus() . ' ' .$resp->getReasonPhrase();
                return false;
            }
        } catch (HTTP_Request2_Exception $e) {
                $this->_response = 'Error: ' . $e->getMessage();
                return false;
        }

    }

    /**
     *
     * @return bool authentication success or error
     */
    private function _authentication($request) {
        //only available for Geosource and Geonetwork
        $authentAddress = substr($this->_authentAddress, 0, strrpos($this->_authentAddress, "/", -5)); //-5 = /fre 
                
        //start by logout
        if ($this->_bAuthent) {
            $req = new HTTP_Request2($authentAddress.'/j_spring_security_logout', HTTP_Request2::METHOD_POST);
            $req->setHeader(array('Referer' => $this->_cswAddress));            
            if ($this->_callHTTPCSW($req)) {
                //success so next step
                //start to login
                $req = new HTTP_Request2($authentAddress.'/j_spring_security_check');
                $req->setMethod(HTTP_Request2::METHOD_POST)
                        ->setHeader("'Content-type': 'application/x-www-form-urlencoded', 'Accept': 'text/plain'")
                        ->setHeader(array('Referer' => $this->_cswAddress))
                        ->addPostParameter('username', $this->_cswLogin)
                        ->addPostParameter('password', $this->_cswPassword);
                if ($this->_callHTTPCSW($req)) {
                    
                    $request->addCookie($this->_cookieName, $this->_sessionID);                    
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * publish the metadata in public access
     * @param String $id uuid of the metadata
     * @return boolean
     */
    public function publishMetadata($id) {
        //example : http://opencarto.net/geonetwork/srv/fr/metadata.admin?&uuid=ADELIE:1024&_1_0=on&_1_1=on&_1_5=on&_1_6=on
        // _"GROUP_ID"_"OPERATION_ID" = on / off
        // Group 1 = internet
        // Operation 0 = view, 1 = download, 2 = editing, 5 = dynamic, 6 = feature
        $getPublishMetadataRequest = new HTTP_Request2($this->_authentAddress.'/metadata.admin?update=true&uuid='.$id."&_1_0=on&_1_1=on&_1_5=on&_1_6=on");
        $getPublishMetadataRequest->setMethod(HTTP_Request2::METHOD_GET);
        if (!$this->_authentication($getPublishMetadataRequest)) throw new \Exception($this->_response, "001");
        if ($this->_callHTTPCSW($getPublishMetadataRequest)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * retrieve a specific metadata with UUID in GeoNetwork / Geosource
     * @param String $id of the metadata
     * @return XML content
     */
    public function getRecordById($id) {
        $getRecodByIDRequest = new HTTP_Request2($this->_cswAddress);
        $getRecodByIDRequest->setMethod(HTTP_Request2::METHOD_POST)
                      ->setHeader(
                      'Content-type: text/xml; charset=utf-8')
		      ->setBody("<?xml version='1.0'?>".
                           "<csw:GetRecordById xmlns:csw='http://www.opengis.net/cat/csw/2.0.2' service='CSW' version='2.0.2' outputSchema='http://www.isotc211.org/2005/gmd' elementSetName='full'>".
                           "<csw:Id>".$id."</csw:Id>".
		                       "<csw:ElementSetName>full</csw:ElementSetName>".
                           "</csw:GetRecordById>");
            //authentication if needed
        if (!$this->_authentication($getRecodByIDRequest)) throw new \Exception($this->_response, "001");
        if ($this->_callHTTPCSW($getRecodByIDRequest)) {
                $getRecodByIDRequest=null;
                //extract MD_Metadata
                if($this->_response){
	                $xmlInt=new \DOMDocument("1.0", "UTF-8");
	                $xmlInt->loadXML($this->_response);
	                $nFI=$xmlInt->getElementsByTagName('MD_Metadata');
	                if ($nFI->length==1) {
	                  return $xmlInt->saveXML($nFI->item(0));
	                }
	                else {
	                    //not found
	                    return 0;
	                }
                }
               
        }
        else {
            $getRecodByIDRequest=null;
            throw new \Exception($this->_response, "002");
        }
        
    }
    /**
     *
     * @return Number of metadata in the csw server
     */
    public function getCountRecords() {
        $getCountRecordsRequest = new HTTP_Request2($this->_cswAddress);
        $getCountRecordsRequest->setMethod(HTTP_Request2::METHOD_POST)
                      ->setHeader('Content-type: text/xml; charset=utf-8')
		      ->setBody("<?xml version='1.0'?>".
                                "<csw:GetRecords xmlns:csw='http://www.opengis.net/cat/csw/2.0.2' service='CSW' version='2.0.2' resultType='hits'>".
                                "<csw:Query typeNames='csw:Record'>".
                                "<csw:Constraint version='1.1.0'>".
                                "    <Filter xmlns='http://www.opengis.net/ogc' xmlns:gml='http://www.opengis.net/gml'/>".
                                "</csw:Constraint>".
                                "</csw:Query>".
                                "</csw:GetRecords>");
            //authentication if needed
        if (!$this->_authentication($getCountRecordsRequest)) throw new \Exception($this->_response, "001");
        if ($this->_callHTTPCSW($getCountRecordsRequest)) {
                $docXml= new \DOMDocument();
                if ($docXml->loadXML($this->_response)) {                    
                    $xp = new \DOMXPath($docXml);
                    $xpathString="//@numberOfRecordsMatched";
                    $nodes = $xp->query($xpathString);
                    if ($nodes->length==1)
                        return $nodes->item(0)->textContent;
                    else
                        return 0;
                }
                else {
                    throw new \Exception($this->_response, "004");
                }
                
        }
        else
            throw new \Exception($this->_response, "003");

    }
    
/**
     * retrieve metadata titles in the csw server
     * @param type csw 'type' ows_constraint value
     * @return array identifier => title
     */
    public function getRecordsTitle($type) {
        $getRecordsRequest = new HTTP_Request2($this->_cswAddress);
        $getRecordsRequest->setMethod(HTTP_Request2::METHOD_POST)
                      ->setHeader('Content-type: text/xml; charset=utf-8')
          ->setBody(  "<csw:GetRecords xmlns:csw='http://www.opengis.net/cat/csw/2.0.2' service='CSW' version='2.0.2' resultType='results' maxRecords='1000'>".
                                "<csw:Query typeNames='csw:Record'>".
                                "<csw:Constraint version='1.1.0'>".
                                "<Filter xmlns='http://www.opengis.net/ogc' xmlns:gml='http://www.opengis.net/gml'>".
                                "<PropertyIsLike wildCard='%' singleChar='_' escapeChar='\'>".
                                "<PropertyName>type</PropertyName>".
                                "<Literal>%".$type."%</Literal>".
                                "</PropertyIsLike>".
                                "</Filter>".
                                "</csw:Constraint>".
                                "</csw:Query>".
                                "</csw:GetRecords>");
            //authentication if needed
        if (!$this->_authentication($getRecordsRequest))return false;
        if ($this->_callHTTPCSW($getRecordsRequest)) {
                $docXml= new \DOMDocument();
                if($this->_response){
                	$docXml->loadXML($this->_response);
                  $xp = new \DOMXPath($docXml);
                  $xpathString="//*[local-name()='identifier'] ";
                  if($nodes = $xp->query($xpathString)){
	                  foreach($nodes as $node) {
	                    $tabKeys[] = $node->nodeValue;
	                  }
	                  $xpathString="//*[local-name()='title'] ";
	                  if($nodes = $xp->query($xpathString)){
		                  $count=0;
		                  $tabTitle = array();
		                  foreach($nodes as $node) {
		                    $tabTitle[$tabKeys[$count]] = $node->nodeValue;
		                    $count++;
		                  }
		                  return $tabTitle;
	                	}
                  }
                }
                else {
                    throw new \Exception($this->_response, "004");
                }
                
        }
        else
            throw new \Exception($this->_response, "003");
        
    }
    /**
     * Insert a new metadata in the csw server
     * @param xmlISO19139 $xmlISO19139 content to add
     * @return number of insered metadata
     */
    public function insertMetadata($xmlISO19139) {
        $uuid = $xmlISO19139->getID();
        $insertMetadataRequest = new HTTP_Request2($this->_cswAddress);
        $insertMetadataRequest->setMethod(HTTP_Request2::METHOD_POST);
                      $insertMetadataRequest->setHeader('Content-type: text/xml; charset=utf-8');
		      $insertMetadataRequest->setBody("<?xml version='1.0'?>".
                           "<csw:Transaction service='CSW' version='2.0.2' xmlns:csw='http://www.opengis.net/cat/csw/2.0.2' xmlns:ogc='http://www.opengis.net/ogc' xmlns:apiso='http://www.opengis.net/cat/csw/apiso/1.0'>".
                           "<csw:Insert>".$xmlISO19139->convertToText(false).
		           "</csw:Insert>".
                           "</csw:Transaction>");
        //authentication is needed !!
        if (!$this->_authentication($insertMetadataRequest)) throw new \Exception("authentication mandatory", "001");
        if ($this->_callHTTPCSW($insertMetadataRequest)) {
                $docXml= new \DOMDocument();
                if ($docXml->loadXML($this->_response)) {
                    $xp = new \DOMXPath($docXml);
                    
                    $xpathString="//csw:totalInserted";
                    $nodes = @$xp->query($xpathString);
                    if ($nodes && $nodes->length==1)
                      return $nodes->item(0)->textContent;
                        
                    else{
                      throw new \Exception($this->_response, "003");
                      return 0;
                    }
                }
                else {
                    throw new \Exception($this->_response, "004");
                }
        }
        else
            throw new \Exception($this->_response, "002");


    }

    /**
     * update a  metadata in the csw server
     * @param xmlISO19139 $xmlISO19139 content to add
     * @return number of updated metadata
     */
    public function updateMetadata($xmlISO19139) {
        
        //first, find the uuid of the metadata !
        
        $uuid = $xmlISO19139->getID();
        if ($uuid==false)
                throw new \Exception("No fileIdentifier found","UM.001");
        $updateMetadataRequest = new HTTP_Request2($this->_cswAddress);
        $updateMetadataRequest->setMethod(HTTP_Request2::METHOD_POST)
                      ->setHeader('Content-type: text/xml; charset=utf-8')
		      ->setBody("<?xml version='1.0'?>".
                           "<csw:Transaction service='CSW' version='2.0.2' xmlns:csw='http://www.opengis.net/cat/csw/2.0.2' xmlns:ogc='http://www.opengis.net/ogc' xmlns:apiso='http://www.opengis.net/cat/csw/apiso/1.0'>".
                           "<csw:Update>".$xmlISO19139->convertToText(false).
                           "<csw:Constraint version='1.0.0'>".
                           "<Filter xmlns='http://www.opengis.net/ogc' xmlns:gml='http://www.opengis.net/gml'>".
                           "<PropertyIsLike wildCard='%' singleChar='_' escapeChar='\'>".
                           "    <PropertyName>apiso:identifier</PropertyName>".
                           "    <Literal>".$uuid."</Literal>".
                           "</PropertyIsLike>".
                           "</Filter>".
                           "</csw:Constraint>".
		           "</csw:Update>".
                           "</csw:Transaction>");
        //authentication is needed !!
        if (!$this->_authentication($updateMetadataRequest)) throw new \Exception("authentication mandatory", "001");
        
        if ($this->_callHTTPCSW($updateMetadataRequest)) {
                $docXml= new \DOMDocument();
                if ($docXml->loadXML($this->_response)) {
                    $xp = new \DOMXPath($docXml);
                    $xpathString="//csw:totalUpdated";
                    $nodes = @$xp->query($xpathString);
                    if ($nodes && $nodes->length==1)
                        return $nodes->item(0)->textContent;
                    else
                    {
                    	throw new \Exception($this->_response, "003");
                    	return 0;
                    }
                }
                else {
                    throw new \Exception($this->_response, "004");
                }
        }
        else
            throw new \Exception($this->_response, "002");


    }

    /**
     * deleted a  metadata in the csw server
     * @param xmlISO19139 $xmlISO19139 content to add
     * @return number of deleted metadata
     */
    public function deleteMetadata($xmlISO19139) {
       //first, find the uuid of the metadata !

        $uuid = $xmlISO19139->getID();
        if ($uuid==false)
                throw new \Exception("No fileIdentifier found","UM.001");
        return $this->deleteMetadataFromUuid($uuid);

        
    }

    /**
     * delete a  metadata in the csw server
     * @param String $uuid id of the metadata
     * @return number of deleted metadata
     */
    public function deleteMetadataFromUuid($uuid) {

       
        $deleteMetadataRequest = new HTTP_Request2($this->_cswAddress);
        $deleteMetadataRequest->setMethod(HTTP_Request2::METHOD_POST)
                      ->setHeader('Content-type: text/xml; charset=utf-8')
		      ->setBody("<?xml version='1.0'?>".
                           "<csw:Transaction service='CSW' version='2.0.2' xmlns:csw='http://www.opengis.net/cat/csw/2.0.2' xmlns:ogc='http://www.opengis.net/ogc' xmlns:apiso='http://www.opengis.net/cat/csw/apiso/1.0'>".
                           "<csw:Delete>".
                           "<csw:Constraint version='1.0.0'>".
                           "<Filter xmlns='http://www.opengis.net/ogc' xmlns:gml='http://www.opengis.net/gml'>".
                           "<PropertyIsLike wildCard='%' singleChar='_' escapeChar='\'>".
                           "    <PropertyName>apiso:identifier</PropertyName>".
                           "    <Literal>".$uuid."</Literal>".
                           "</PropertyIsLike>".
                           "</Filter>".
                           "</csw:Constraint>".
		           "</csw:Delete>".
                           "</csw:Transaction>");
        //authentication is needed !!

        if (!$this->_authentication($deleteMetadataRequest)) throw new \Exception("authentication mandatory", "001");

        if ($this->_callHTTPCSW($deleteMetadataRequest)) {
                $docXml= new \DOMDocument();
                if ($docXml->loadXML($this->_response)) {
                    $xp = new \DOMXPath($docXml);
                    $xpathString="//csw:totalDeleted";
                    $nodes = $xp->query($xpathString);
                    if ($nodes->length==1)
                        return $nodes->item(0)->textContent;
                    else
                        {
                         throw new \Exception("publish error", "005");
                    }
                }
                else {
                    throw new \Exception($this->_response, "004");
                }
        }
        else
            throw new \Exception($this->_response, "002");
    }

}




?>
