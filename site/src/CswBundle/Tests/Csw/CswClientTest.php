<?php

namespace Carmen\CswBundle\Tests\Controller;

use Carmen\CswBundle\Csw\CswClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CswClientTest extends WebTestCase
{
    private $cswClient;
    
    protected function setUp()
    {
        $geosourceUrl = "http://metadata.carmencarto.fr/geosource/78/fre";
        $cswUrl       = $geosourceUrl."/csw-publication";
        $user_login   = "Admin_AO";
        $user_pwd     = "aolu27";
         
        $sessionId = "JSESSIONID";
        
        //$connectparam = "username=".$user_login."&password=".$user_pwd;
        $this->cswClient = new CswClient($cswUrl, $user_login, $user_pwd, $geosourceUrl, $sessionId);
    }
    
    public function testGetCountRecordsIndex()
    {
        $records = $this->cswClient->getCountRecords();
        $this->assertTrue( $records > 0 , "getCountRecords should not return 0 record");
    }
    
    public function testGetRecordById()
    {
        $xml = $this->cswClient->getRecordById(-1);
        $this->assertEquals( $xml, 0 , "getRecordById(-1) should return 0 as 'not found'");
    }
}
