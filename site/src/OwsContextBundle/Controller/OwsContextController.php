<?php

namespace Carmen\OwsContextBundle\Controller;

use Carmen\ApiBundle\Controller\BaseController;
use Carmen\ApiBundle\Entity\Account;
use Carmen\ApiBundle\Entity\FavoriteArea;
use Carmen\ApiBundle\Entity\LexDns;
use Carmen\ApiBundle\Entity\Map;
use Carmen\ApiBundle\Entity\MapLocator;
use Carmen\ApiBundle\Entity\MapOnlineResources;
use Carmen\ApiBundle\Entity\UiModel;
use Carmen\ApiBundle\Exception\ApiException;
use Carmen\ApiBundle\Repository\MapTreeRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Carmen OWS Context Controller.
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/owscontext")
 */
class OwsContextController extends BaseController
{
    const ENTITY_BUNDLE = "CarmenApiBundle:";
    /**
     * Generated Formats
     * @var array
     */
    protected static $FORMATS = array("ows" => true, "json" => false, "geojson" => true);

    /**
     * Create and attributes rights on directory recursively
     * @param string $directory
     */
    protected function getOWSDirectory(&$directory)
    {
        $request = $this->getRequest();
        $directory = $request->request->get("directory", $directory ?: $this->getCarmenConfig()->getOwscontextPublicationDir());
        @mkdir($directory, 0770, true);
    }

    /**
     *
     * @param int|string $identifier
     * @return Map
     * @throws ApiException
     * @throws EntityNotFoundException
     */
    protected function getMap($identifier)
    {
        if ($identifier === null) {
            $exception = new EntityNotFoundException();
            throw new ApiException($this->_t("Not Found"), array(), Response::HTTP_NOT_FOUND, $exception);
        }
        $criteria = array();
        if (is_numeric($identifier)) {
            $criteria["mapId"] = $identifier;
        } else {
            $criteria["mapFile"] = $identifier;
            $criteria["publishedMap"] = null;
        }

        $entity = $this->getRepository(self::ENTITY_BUNDLE . "Map")->findOneBy($criteria);
        if (!$entity) {
            $exception = new EntityNotFoundException();
            throw new ApiException($this->_t("Not Found"), array(), Response::HTTP_NOT_FOUND, $exception);
        }
        $entity instanceof Map;

        return $entity;
    }

    /**
     * get local layers legend Infos since to mapserver templating in json format
     *
     * @param layers
     * @param accountUrlData datacarto server URL
     * @param mapfile mapfile path
     * @return Array
     *
     */
    protected function getLegendParams($layers, $accountUrlData, $mapfile, $legendPath, $legendUrl){
        
        $tabLegend = array();
        $tabExternalLegend = array();
        $getLegendJsonUrl = $accountUrlData.
                            $mapfile.
                            "?MODE=LEGEND";

        foreach ($layers as $layer) {
            $layer instanceof Layer;

            //test if uploaded image exists for legend, take it if exists
            $imageName = str_replace("/", "_", $mapfile)."_".$layer->getLayerMsName().".png";
            if (file_exists($legendPath.$imageName)){
                $tabExternalLegend[$layer->getLayerMsName()] = array("external_url" => $legendUrl.$imageName);
            }else{
                $layerTypeCode = $layer->getLayerType()->getLayerTypeCode();
                if($layerTypeCode==="VECTOR" || $layerTypeCode==="POSTGIS" || $layerTypeCode==="WFS"){
                    
                    $legend = strip_tags($this->curl($getLegendJsonUrl."&layer=".$layer->getLayerMsName()));
                    //suppression de la virgule
                    $legend = substr($legend, 0, -1);
                    $legend = "[".$legend."]";
                    //valid json ?
                    
                    $jsonLegend = json_decode($legend);
    
                    if (json_last_error() === JSON_ERROR_NONE) {
                        // JSON is valid
                        $tabLegend[$layer->getLayerMsName()] = $legend;
                    }
                }
            }

            
        }


        return array($tabLegend, $tabExternalLegend);
    }


    /**
     * Generate an OWS Context file for a map file given by identifier (map ID or map Filename)
     * (Generated Formats : .ows(=xml) + .json)
     *
     * @Route("/generate/{account_id}/{map_id}/{mapfile}", name="carmen_owscontext_generate", options={"expose"=true}, methods={"GET"})
     *
     * @param string $identifier
     * @param string $directory
     * @param boolean $bGetResponseObj
     * @return \Symfony\Component\HttpFoundation\Response|boolean
     * @throws ApiException
     */
    public function generateContextAction($prefix, $account_id, $map_id, $mapfile, $directory = null, $bGetResponseObj = true, $bForPublication = true)
    {
        $kernel = $this->get('kernel');
        $kernel instanceof Kernel;
        $map = $this->getMap($map_id);

        $config = $this->container->getParameter('carmen_config', array());

        //fullMapfilePath not always set
        if ($map->getFullMapfilePath() == "") {
            $map->setFullMapfilePath($config["mapserver"]["root_data"] . "/" . $map->getAccount()->getAccountPath() . "/Publication/" .
                ($map->getMapModel() ? "modele_" . preg_replace("!^modele_!", "", $mapfile) : $mapfile) . ".map");
            $map->setMapfileName(($map->getMapModel() ? "modele_" . preg_replace("!^modele_!", "", $mapfile) : $mapfile));
        }

        $mapTree = $this->getRepository(self::ENTITY_BUNDLE . 'MapTree');
        $mapTree instanceof MapTreeRepository;
        $mapTreePrint = $this->getRepository(self::ENTITY_BUNDLE . 'MapTreePrint');
        $mapTreePrint instanceof MapTreeRepository;

        $cgi_url = $config["mapserver"]["cgi_url"];

        $accountUrlFront = "/";
        $accountUrlData = "/";
        $repository = $this->getRepository('CarmenApiBundle:Account');
        $repository instanceof \Doctrine\ORM\EntityRepository;
        $account = $repository->find($account_id);
        $account instanceof Account;
        $accountDns = "";
        if ($account) {
            $accountDns = $account->getAccountDns();
            $accountDns instanceof LexDns;
            $conf = $this->container->getParameter('carmen_config');
            $sep = isset($conf['carmen']['dns_url_prefix_sep']) ? $conf['carmen']['dns_url_prefix_sep'] : '.';
            if ($accountDns) {
                $accountUrlFront = $account->getAccountUrlFront($bForPublication, $kernel->getEnvironment(), $sep);
                $accountUrlData = $account->getAccountUrlData($bForPublication, $kernel->getEnvironment(), $sep);
            } else {
                $accountDns = "";
            }
        }
        // MapLocator
        $mapLocators = array();
        $tabLocatorsGeoJson = array();
        foreach ($map->getLocators() as $locator) {
            $locator instanceof MapLocator;
            $layerLocator = $map->getLayerById($locator->getLocatorCriteriaLayerId());
            if ($layerLocator) {
                $locatorString = array(
                    $locator->getLocatorCriteriaName(),
                    $layerLocator->getLayerMsname(),
                    $locator->getLocatorCriteriaFieldId(),
                    $locator->getLocatorCriteriaFieldName(),
                    ($locator->getLocatorCriteriaVisibility() ? "1" : "0"),
                );
                $tabLocatorsGeoJson[$locator->getLocatorCriteriaRank()] = array(
                    "title" => $locator->getLocatorCriteriaName(),
                    "layerId" => $layerLocator->getLayerMsname() . "_" . $locator->getLocatorCriteriaRank(),
                    "layerCodeField" => $locator->getLocatorCriteriaFieldId(),
                    "layerWFSUrl" => $accountUrlData . "/" . $cgi_url . "/" . $map->getMapFile() . "?typename=" . $layerLocator->getLayerMsname() . "_" . $locator->getLocatorCriteriaRank() . "_locator",
                    "layerTextField" => $locator->getLocatorCriteriaFieldName(),
                    "criteriaRelated" => (($relatedLocator = $locator->getLocatorCriteriaRelated()) ? $relatedLocator->getLocatorCriteriaRank() + 1 : ""),
                    "criteriaFieldRelated" => (($relatedLocator = $locator->getLocatorCriteriaRelated()) ? $locator->getLocatorCriteriaRelatedField() : ""),
                    "criteriaInSearch" => ($locator->getlocatorInGlobalMapSearchEngine() ? true : false),
                );
                if (($relatedLocator = $locator->getLocatorCriteriaRelated())) {
                    $locatorString[] = $relatedLocator->getLocatorCriteriaRank() + 1;
                    $locatorString[] = $locator->getLocatorCriteriaRelatedField();
                }
                $mapLocators[$locator->getLocatorCriteriaRank()] = implode("|", $locatorString);
            }
        }
        ksort($mapLocators);
        ksort($tabLocatorsGeoJson);
        $mapLocators = implode(";", $mapLocators);
        $mapLocatorsGeoJson = json_encode($tabLocatorsGeoJson);

        // MapModels
        $tabModel = array();
        $tabModelGeoJson = array();
        foreach ($map->getMapUi()->getUiModels() as $uimodel) {
            $uimodel instanceof UiModel;
            $tabModel[] = $uimodel->getAccountModel()->getAccountModelFile() . "@" . $uimodel->getAccountModel()->getAccountModelName();
            $tabModelGeoJson[] = array(
                "file" => $uimodel->getAccountModel()->getAccountModelFile(),
                "title" => $uimodel->getAccountModel()->getAccountModelName(),
                "image" => $this->container->getParameter("PRODIGE_URL_FRONTCARTO") . "/IHM/cartes/" . $uimodel->getAccountModel()->getAccountModelLogo(),
                "ratio" => array("A4" => 1, "A3" => 1)
            );
        }

        $mapModels = implode(";", $tabModel);
        $mapModelsGeoJson = json_encode($tabModelGeoJson);

        // MapLocaator
        $favoriteAreas = array();
        $tabAreasGeoJson = array();
        foreach ($map->getFavoriteAreas() as $favoriteArea) {
            $favoriteArea instanceof FavoriteArea;
            $favoriteAreas[] = array(
                "name" => $favoriteArea->getAreaName(),
                "BoundingBox" => array(
                    "LowerCorner" => $favoriteArea->getAreaXmin() . " " . $favoriteArea->getAreaYmin(),
                    "UpperCorner" => $favoriteArea->getAreaXmax() . " " . $favoriteArea->getAreaYmax(),
                )
            );
            $tabAreasGeoJson[] = array(
                "title" => $favoriteArea->getAreaName(),
                "bbox" => json_encode(array($favoriteArea->getAreaXmin(), $favoriteArea->getAreaYmin(), $favoriteArea->getAreaXmax(), $favoriteArea->getAreaYmax()))
            );
        }
        $favoriteAreas = json_encode($favoriteAreas);
        $favoriteAreasGeoJson = json_encode($tabAreasGeoJson);

        //get Class param to put in styleLegend Object
        /*        $paramsGET = array(

          "directory" => $config["mapserver"]["root_data"]."/".$map->getAccount()->getAccountPath()."/Publication/". 
                        //all before  last slash
                        (strrpos($map->getMapFile(), "/")!==false  ?
                          substr($map->getMapFile(), 0, strrpos($map->getMapFile(), "/"))."/" 
                        :     "")
        );*/

        // MapOnlineRessources
        $MapOnlineRessources = array();
        $indice = 0;
        $tabCategory = array();
        foreach ($map->getOnlineResources() as $resource) {

            $resource instanceof MapOnlineResources;
            $categoryName = $resource->getOnlineResourceCategoryName();
            $resource_params = array();
            if (!in_array($categoryName, $tabCategory)) {

                $onlineresource = array();
                $onlineresource["OnlineResourceCategoryName"] = $categoryName;
                $onlineresource["OnlineResource"] = array();

                $resource_params["OnlineRessourceName"] = $resource->getOnlineResourceName();
                $resource_params["OnlineRessourceUrl"] = $resource->getOnlineResourceUrl();
                $onlineresource["OnlineResource"][] = $resource_params;

                $MapOnlineRessources[] = $onlineresource;
                $tabCategory[] = $categoryName;
            } else {
                for ($i = 0; $i < count($MapOnlineRessources); $i++) {
                    if ($MapOnlineRessources[$i]["OnlineResourceCategoryName"] === $categoryName) {
                        $nbresource_cat = count($MapOnlineRessources[$i]["OnlineResource"]);
                        $resource_params["OnlineRessourceName"] = $resource->getOnlineResourceName();
                        $resource_params["OnlineRessourceUrl"] = $resource->getOnlineResourceUrl();
                        $MapOnlineRessources[$i]["OnlineResource"][$nbresource_cat] = $resource_params;
                    }
                }
            }
        }

        $msLayers = array();
        $legendLayers = array();
        $externalLegendLayers = array();


        if ($bForPublication) {
            $currentMapfile = $config["mapserver"]["root_data"] . "/" . $map->getAccount()->getAccountPath() . "/Publication/" . $map->getMapfileName() . ".map";
            $legendMapfile = $config["mapserver"]["root_data"] . "/" . $map->getAccount()->getAccountPath() . "/Publication/" . "legend_" . str_replace("/", "_", $map->getMapfileName()) . ".map";
            if (file_exists($currentMapfile)) {

                //since mapserver CGI mode LEGEND can't generate image in not IMAGEPATH directory, copy mapfile and change IMAGEPATH
                copy($currentMapfile, $legendMapfile);
                $cmd = 'sed -i "s/IMAGEPATH.*/IMAGEPATH \"' . str_replace("/", "\/", $config["mapserver"]["root_data"] . $account->getAccountPath() . "/IHM/LEGEND") . '\"/g" ' . $legendMapfile . ';';
                $cmd .= 'sed -i "s/IMAGEURL.*/IMAGEURL \"' . str_replace("/", "\/", $accountUrlFront . "/IHM/" . $account->getAccountPath() . "/LEGEND/") . '\"/g" ' . $legendMapfile . ';';
                $process = Process::fromShellCommandline($cmd);
                $process->run();
                $legendInfo = $this->getLegendParams($map->getLayers(),$accountUrlData."/".$cgi_url, 
                                                      "legend_" .str_replace("/", "_", $map->getMapfileName()),
                                                       $config["mapserver"]["root_data"]."/".$account->getAccountPath()."/IHM/LEGEND/",
                                                       str_replace("/", "\/", $accountUrlFront."/IHM/".$account->getAccountPath()."/LEGEND/"));
                $legendLayers = $legendInfo[0];
                $externalLegendLayers = $legendInfo[1];
                unlink($legendMapfile);
            }

            
            
        }

        $data = array(
            'cgi_url'        => $cgi_url
          , 'account'        => $account
          , "accountUrlFront"=> $accountUrlFront
          , "accountUrlData" => $accountUrlData
          //SPECIFIC PRODIGE
          , "downloadURL"    => $this->container->getParameter("PRODIGE_URL_CATALOGUE")."/geosource/panierDownloadFrontalParametrage/"
          , "layerGeojsonUrl"    => $this->container->getParameter("PRODIGE_URL_CATALOGUE").'/api/data/'
          , "geonetworkURL"    => $this->container->getParameter("PRO_GEONETWORK_URLBASE")
          , 'mapOnlineResources' => $MapOnlineRessources
          // END PRODIGE
          , 'defaultBaseLayer' => $map->getDefaultBaseLayer()
          , 'map'            => $map
          , 'mapUI'          => $map->getMapUi()
          , 'mapModels'      => $mapModels
          , 'mapModelsGeoJson'=> $mapModelsGeoJson
          , 'mapLocatorsGeoJson' => $mapLocatorsGeoJson
          , 'mapLocators'    => $mapLocators
          , 'favoriteAreas'  => $favoriteAreas
          , 'favoriteAreasGeoJson' => $favoriteAreasGeoJson
          , 'layers'         => $map->getLayers()
          , 'layerLegend'    => $legendLayers
          , 'externalLegendLayers'    => $externalLegendLayers
          , 'msLayers'       => $msLayers
          , 'map_tree'       => $mapTree->getTreeByMap($map)
          , 'map_tree_print' => $mapTreePrint->getTreeByMap($map)
          , 'tools'          => $this->getRepository(self::ENTITY_BUNDLE.'LexTool')->findBy(array(), array("toolXmlNodeprefix"=>"ASC", "toolIdentifier"=>"ASC"))
        );

        $contents = array();

        foreach (self::$FORMATS as $format => $forPublication) {
            if ($forPublication !== $bForPublication) continue;
            //            $result = $this->render('OwsContextBundle:Default:context.'.$format.'.twig', $data);
            $result = $this->render('OwsContextBundle/Default/context.' . $format . '.twig', $data);
            if ($format === "geojson") {
                $owscontext = $directory . "/" . $prefix . "/" . $mapfile . "." . $format;
                if (!file_exists(dirname($owscontext))) {
                    mkdir(dirname($owscontext), 0770, true);
                }
            } else {
                $owscontext = $directory . "/" . $prefix . "_" . str_replace("/", "_", $mapfile) . "." . $format;
            }

            $contents = array_merge($contents, array("FICHIER AU FORMAT {$format} : {$owscontext}", $result->getContent()));
            if (!file_put_contents($owscontext, $result->getContent())) {
                throw new ApiException($this->_t("Error during OWS Context generation"), array(), Response::HTTP_FAILED_DEPENDENCY);
                return false;
            }
        }

        // return new Response('<html><body>'.phpinfo().'</body></html>');

        if ($bGetResponseObj)
            return new Response(implode("<br/><br/>", $contents));

        return true;
    }

    /**
     *
     * Get an OWS Context file for a map file given by identifier (map ID or map Filename)
     * (Generated Formats : .ows(=xml) + .json)
     *
     * @Route("/get/{account_id}/{identifier}/{format}",
     * name="carmen_owscontext_get",
     * defaults={"format"="OWS"},
     * requirements={"format"="OWS|JSON|ows|json", "identifier"="^\w+$", "account_id"="^\w+"},
     * options={"expose"=true},
     * methods={"GET"})
     *
     * @param string $identifier
     * @param string $identifier
     * @param string $directory
     * @param boolean $bGetResponseObj
     * @return \Symfony\Component\HttpFoundation\Response|string
     * @throws ApiException
     */
    public function getContextAction($account_id, $identifier, $format, $directory = null, $bGetResponseObj = true)
    {
        $format = strtolower($format);
        if (!array_key_exists($format, self::$FORMATS)) {
            throw new ApiException($this->_t("Unavailable format for OWS Context"), array($format), Response::HTTP_NOT_IMPLEMENTED);
        }
        $this->getOWSDirectory($directory);

        if ($format === "geojson") {
            $owscontext = $directory . "/" . $account_id . "_" . identifier . "." . $format;
        } else {
            $owscontext = $directory . "/" . $account_id . "_" . str_replace("/", "_", $identifier) . "." . $format;
        }

        if (!file_exists($owscontext)) {
            throw new ApiException($this->_t("The OWS Context does not exists"), array($owscontext), Response::HTTP_NOT_FOUND);
        }

        $class = '\\Symfony\\Component\\HttpFoundation\\' . ($format == "json" ? "JsonResponse" : "Response");
        $contents = file_get_contents($owscontext);
        if ($format == "json") {
            $contents = preg_replace("!\/\*.+?\*\/!ius", "", $contents);
            $contents = json_decode($contents, true);
            if (!$contents) {
                throw new ApiException($this->_t("The OWS Context failed to convert : " . json_last_error_msg()), array($owscontext), Response::HTTP_NO_CONTENT);
            }
        }
        return ($bGetResponseObj ? new $class($contents) : $contents);
    }

    public function indexAction($name)
    {
        return new Response('no yet implemented');
    }
}
