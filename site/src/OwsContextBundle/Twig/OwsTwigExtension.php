<?php

namespace Carmen\OwsContextBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 *
 * @author martigny
 *        
 */
class OwsTwigExtension extends AbstractExtension
{

    /**
     * (non-PHPdoc)
     * @see Twig_Extension::getFilters()
     */
    public function getFilters()
    {
        return array(
            new TwigFilter('TRUE_FALSE', array($this, 'trueFalseFilter')),
            new TwigFilter('ON_OFF', array($this, 'onOffFilter')),
            new TwigFilter('ZERO_ONE', array($this, 'zeroOneFilter')),
            new TwigFilter('basename', "basename"),
            new TwigFilter('dirname', "dirname"),
        );
    }

    /**
     * Return boolean value. 
     * @param boolean $boolean
     * @return string
     */
    public function trueFalseFilter($boolean)
    {
        return ($boolean ? "true" : "false");
    }

    /**
     * Translate boolean value in "ON"/"OFF" string. 
     * true="ON", false="OFF" 
     * @param boolean $boolean
     * @return string
     */
    public function onOffFilter($boolean)
    {
        return ($boolean ? "ON" : "OFF");
    }

    /**
     * Translate boolean value in "1"/"0" string. 
     * true="1", false="0" 
     * @param boolean $boolean
     * @return string
     */
    public function zeroOneFilter($boolean)
    {
        return ($boolean ? "1" : "0");
    }
    
    /*
     * (non-PHPdoc)
     * @see Twig_ExtensionInterface::getName()
     */
    public function getName() {
        return "ows_twig_extension";
    }
}