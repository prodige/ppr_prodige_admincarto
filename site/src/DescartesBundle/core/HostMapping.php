<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



require_once __DIR__ .'/../tools/XmlTools.php';
require_once __DIR__ .'/../core/HostMappingException.php';

/**
 * Class: HostMapping
 * Classe utilitaire pour les correspondances entre Hosts internes au centre-serveur et Hosts publics.
 */
class HostMapping {

	/**
	 * Staticmethode: mapHostsToDoc(String, String, String[])
	 * Transforme un flux XML contenant les Hosts publics en arbre DOM contenant les Hosts internes.
	 * 
	 * Param�tres:
	 * $flux	- {String} Flux XML contenant les Hosts publics
	 * $xmlMapping - {String} Nom du fichier contenant les correspondances
	 * $elementNames	- {String[]} Noms des �l�ments du flux XML devant faire l'objet des correspondances
	 * 
	 * Retour:
	 * {DOMDocument} Arbre DOM contenant les Hosts internes
	 * 
	 * Lance:
	 * {<HostMappingException>} - Si une exception est d�clench�e
	 */
	public static function mapHostsToDoc($flux, $xmlMapping, $elementNames) {
		$doc = new DOMDocument();
		if (substr($flux, 0, 1) == '<') {
			$isloaded = $doc->loadXML('<?xml version="1.0" encoding="iso-8859-1"?> ' . $flux);
		} else {
			$isloaded = $doc->load($flux);
		}
		if (!$isloaded) {
			throw new DomLoaderException();
		}
		try {
			if ($xmlMapping != null) {
				$docXpath = new DOMXPath($doc);
				$map = new DOMDocument();
				$map->load($xmlMapping);
				$mapXpath = new DOMXPath($map);
	
				foreach ($elementNames as $elementName) {
					$nodes = $docXpath->query('//' . $elementName);
					foreach ($nodes as $node) {
						$url = $node->firstChild->nodeValue;
						$nodeInternes = $mapXpath->query("//Interne[contains('". $url . "',../Externe)]");
						if ($nodeInternes->length != 0) {
							$nodeInterne = $nodeInternes->item(0);
							$nodeExterne = $mapXpath->query("//Externe[../Interne='" . $nodeInterne->firstChild->nodeValue . "']");
							$qsa = $mapXpath->query("//QSA[../Interne='" . $nodeInterne->firstChild->nodeValue . "']");
							$newURL = str_replace($nodeExterne->firstChild->nodeValue, $nodeInterne->firstChild->nodeValue, $url);
							if ($qsa != null) {
                	                $newURL .= "&" + $qsa->firstChild->nodeValue;
                        	}
							$node->firstChild->nodeValue = $newURL;
						} else {
							$node->firstChild->nodeValue = "KO";
						}
					}
				}
			}
		} catch (Exception $e) {
			throw new HostMappingException($e);
		}
		return $doc;
	}
	
	/**
	 * Staticmethode: mapHostsToString(String, String, String[])
	 * Transforme un flux XML contenant les Hosts publics en flux XML contenant les Hosts internes.
	 * 
	 * Param�tres:
	 * $flux	- {String} Flux XML contenant les Hosts publics
	 * $xmlMapping - {String} Nom du fichier contenant les correspondances
	 * $elementNames	- {String[]} Noms des �l�ments du flux XML devant faire l'objet des correspondances
	 * 
	 * Retour:
	 * {String} Flux XML contenant les Hosts internes
	 * 
	 * Lance:
	 * {<HostMappingException>} - Si une exception est d�clench�e
	 */
	public static function mapHostsToString($flux, $xmlMapping, $elementNames = "") {
		$sw = "";
		try {
			if ($elementNames == "") {
				$url = $flux;
				if ($xmlMapping != null) {
					$map = new DOMDocument();
					$map->load($xmlMapping);
					$mapXpath = new DOMXPath($map);
					$nodeInternes = $mapXpath->query("//Interne[contains('". $url . "',../Externe)]");
					if ($nodeInternes->length != 0) {
						$nodeInterne = $nodeInternes->item(0);
						$nodeExterne = $mapXpath->query("//Externe[../Interne='" . $nodeInterne->firstChild->nodeValue . "']");
						$qsa = $mapXpath->query("//QSA[../Interne='" . $nodeInterne->firstChild->nodeValue . "']");
    	                $url = str_replace($nodeExterne->firstChild->nodeValue, $nodeInterne->firstChild->nodeValue, $url);
   	                	if ($qsa != null) {
           	                	$url .= "&" . $qsa->firstChild->nodeValue;
                       	}
                    } else {
    	                $url = "KO";
       	        	}
				}
				$sw = $url;
			} else {
                $doc = self::mapHostsToDoc(flux, xmlMapping, elementNames);
				$sw = $doc->saveXML();
			}
		} catch (Exception $e) {
			throw new HostMappingException($e);
		}
		return $sw;
	}
}
?>
