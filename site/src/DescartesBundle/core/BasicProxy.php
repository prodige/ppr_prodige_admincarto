<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



require_once 'HostMapping.php';
require_once __DIR__ .'/../tools/RessourceBundle.php';
require_once __DIR__ .'/../tools/HTTPProxy.php';
require_once __DIR__ .'/../tools/HTTPSenderException.php';

/**
 * Class: BasicProxy
 * Classe utilitaire correspondant � un proxy applicatif basique pour relayer des requ�tes HTTP afin de s'affranchir de la politique de s�curit� des navigateurs dite SOP (same origin policy).
 */
class BasicProxy {
	public $httpProxy;
	public $xmlHostsFileName = null;
	
	/**
	 * Constructeur: BasicProxy(String, HTTPProxy)
	 * Constructeur d'instances
	 *
	 * Param�tres:
	 * $xmlHostsFileName - {String} Eventuel fichier de correspondance des Hosts
	 * $httpProxy - {<HTTPProxy>} Proxy.
	 */
	public function __construct($xmlHostsFileName, $httpProxy) {
		$this->xmlHostsFileName = $xmlHostsFileName;
		$this->httpProxy = $httpProxy;
	}
	
	/**
	 * Methode: execute(String, String, String)
     * Relaie une requ�te GET ou POST.
     * 
     * Param�tres de la m�thode:
     * $url - {String} URL de la requ�te HTTP
     * $method - {"GET" | "POST"} M�thode de la requ�te HTTP
     * $postbody - {String} Eventuel contenu du POST de la requ�te HTTP
     */
	public function execute($url, $method, $postbody) {
		$result = null;
		$client = curl_init ();
		curl_setopt ( $client, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($client, CURLOPT_HEADER, true);
		curl_setopt($client, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($client, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
                curl_setopt($client, CURLOPT_PROTOCOLS, CURLPROTO_HTTP|CURLPROTO_HTTPS);
		try {
			$url = urldecode($url);
			$url = HostMapping::mapHostsToString ( $url, $this->xmlHostsFileName );
			if ($url == "KO") {
				$result = "KO";
			}
			curl_setopt ( $client, CURLOPT_URL, $url );
			if ($this->hasProxy ( $url, $this->httpProxy )) {
				curl_setopt ( $client, CURLOPT_HTTPPROXYTUNNEL, false );
				curl_setopt ( $client, CURLOPT_PROXYPORT, $this->httpProxy->getPort () );
				curl_setopt ( $client, CURLOPT_PROXY, $this->httpProxy->getHost () );
			}
			
			$headers = array();
			foreach($_SERVER as $key => $value) {
				if(substr($key, 0, 5) == 'HTTP_') {
					array_push($headers, str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5))))).": ".$value);
				}
			}

			if ($method === "POST") {
				array_push($headers, "Content-Type: application/xml");
			curl_setopt($client, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($client, CURLOPT_POST, true);
				curl_setopt($client, CURLOPT_POSTFIELDS, $postbody);
			}
			$result = curl_exec ( $client );

			$info = curl_getinfo($client);
			$header = substr($result, 0, $info['header_size']);
			$body = substr($result, -$info['size_download']); 

			curl_close ( $client );
		} catch ( HttpSenderException $e ) {
			curl_close ( $client );
			$result = $e->getMessage ();
		} catch ( HostMappingException $e ) {
			curl_close ( $client );
			$result = $e->getMessage ();
		} catch ( Exception $e ) {
			curl_close ( $client );
			$result = $e->getMessage ();
		}

		$headers = explode("\r\n", $header);		
		foreach ($headers as &$value) {
			if ($value !== "") {
				header($value);
			}
		}
		
		echo $body;
	}
	
	/**
	 * @param String $urlName Url
	 * @param httpProxy $httpProxy Proxy
	 */
	private function hasProxy($urlName, $httpProxy) {
		$proxy = false;
		if ($httpProxy != null && $httpProxy->getHost () != null && $httpProxy->getPort () != 0) {
			$proxy = true;
			try {
				if ($httpProxy->getUrlExceptions () != null && $httpProxy->getUrlExceptions () != "") {
					$url = parse_url($urlName);
					$host = $url['host'];
					$noProxyURLs = explode ( ";", $httpProxy->getUrlExceptions () );
					foreach ( $noProxyURLs as $noProxyURL ) {
						$differenceLongueur = strlen ( $host ) - strlen ( $noProxyURL );
						if (stripos ( $noProxyURL, $host ) === 0 || stripos ( $noProxyURL, $host ) === $differenceLongueur) {
							$proxy = false;
							break;
						}
					}
				}
			} catch ( Exception $e ) {
				throw new HttpSenderException ( "URL non valide" . $urlName, $e );
			}
		}
		return $proxy;
	}
}
?>