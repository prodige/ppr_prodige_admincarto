<?php

/*
Copyright MEDDTL
Contributeurs :
     Gaï¿½lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est rï¿½gi par la licence CeCILL-C soumise au droit franï¿½ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusï¿½e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilitï¿½ au code source et des droits de copie,
de modification et de redistribution accordï¿½s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitï¿½e.  Pour les mï¿½mes raisons,
seule une responsabilitï¿½ restreinte pï¿½se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concï¿½dants successifs.

A cet ï¿½gard  l'attention de l'utilisateur est attirï¿½e sur les risques
associï¿½s au chargement,  ï¿½ l'utilisation,  ï¿½ la modification et/ou au
dï¿½veloppement et ï¿½ la reproduction du logiciel par l'utilisateur ï¿½tant 
donnï¿½ sa spï¿½cificitï¿½ de logiciel libre, qui peut le rendre complexe ï¿½ 
manipuler et qui le rï¿½serve donc ï¿½ des dï¿½veloppeurs et des professionnels
avertis possï¿½dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invitï¿½s ï¿½ charger  et  tester  l'adï¿½quation  du
logiciel ï¿½ leurs besoins dans des conditions permettant d'assurer la
sï¿½curitï¿½ de leurs systï¿½mes et ou de leurs donnï¿½es et, plus gï¿½nï¿½ralement, 
ï¿½ l'utiliser et l'exploiter dans les mï¿½mes conditions de sï¿½curitï¿½. 

Le fait que vous puissiez accï¿½der ï¿½ cet en-tï¿½te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez acceptï¿½ les
termes.
*/



require_once 'HostMapping.php';
require_once 'SchemaFactory.php';
require_once __DIR__ .'/../tools/XmlTools.php';
require_once __DIR__ .'/../tools/HTTPProxy.php';
require_once __DIR__ .'/../tools/HTTPSender.php';
require_once __DIR__ .'/../tools/HTTPSenderException.php';

/**
 * Class: OWSHelper
 * Classe d'accï¿½s aux donnï¿½es sï¿½mantiques de serveurs WMS ou WFS.
 */
class OWSHelper {
	const DEFAULT_CONTENT_TYPE = "text/xml";
	const INTERNAL_STYLE_CSS = '../core/resources/styles.css';
	const INTERNAL_IMAGE_LOCATE_MENU = '../core/resources/icon_locate.png';
	const INTERNAL_IMAGE_LOCATE_ITEM = '../core/resources/icon_preview.png';
	
	private $xmlHostsFileName;
	private $xslFile;

	/**
	 * Nom du fichier XSLT embarque pour la conversion des reponses WMS / WFS en flux HTML
	 */
	private $internalXsl2HtmlFile = "../core/resources/toHtmlResult.xsl";
	
	/**
	 * Nom du fichier XSLT embarque pour la conversion des reponses WMS / WFS en flux JSON
	 */
	private $internalXsl2JsonFile = "../core/resources/toJsonResult.xsl";
	
	/**
	 * Nom du fichier XSLT embarque pour la conversion des reponses WMS / WFS en tableau (à évaluer) PHP
	 */
	private $internalXsl2PhpArrayFile = "../core/resources/toPhpResult.xsl";
	
	/**
	 * Nom du fichier XSLT embarque pour l'unification des reponses WMS / WFS
	 */
	public $internalXslFile;

	private $stylesCssFileName;
	private $httpProxy;
	private $outputFormat = self::DEFAULT_CONTENT_TYPE;
	private $imageLocateMenu;
	private $imageLocateItem;
	
	/**
	 * Constructeur: OWSHelper(String, HTTPProxy, String, String, String, String)
	 * Constructeur d'instances
	 *
	 * Paramï¿½tres:
	 * $xmlHostsFileName - {String} Eventuel fichier de correspondance des Hosts
	 * $httpProxy - {<HTTPProxy>} Proxy.
	 * $xslFile - {String} Eventuel fichier de transformation XSL-T formattant la rï¿½ponse en flux HTML, remplaï¿½ant la transformation par dï¿½faut
	 * $stylesCssFileName - {String} Eventuel fichier de la feuille de style CSS prï¿½sentant la rï¿½ponse en flux HTML, remplaï¿½ant la feuille de style CSS par dï¿½faut 
	 * $imageLocateMenu - {String} Eventuel pictogramme, remplaï¿½ant le pictogramme par dï¿½faut pour l'entï¿½te de la colonne "Localiser"
	 * $imageLocateItem - {String} Eventuel pictogramme, remplaï¿½ant le pictogramme par dï¿½faut pour l'accï¿½s ï¿½ la localisation d'un objet
	 */
	public function __construct($xmlHostsFileName, $httpProxy, $xslFile, $stylesCssFileName, $imageLocateMenu = self::INTERNAL_IMAGE_LOCATE_MENU, $imageLocateItem = self::INTERNAL_IMAGE_LOCATE_ITEM) {
		$this->xmlHostsFileName = $xmlHostsFileName;
		$this->httpProxy = $httpProxy;
		$this->xslFile = $xslFile;
		$this->stylesCssFileName = ($stylesCssFileName != null) ? $stylesCssFileName : __DIR__."/".self::INTERNAL_STYLE_CSS;
		$this->imageLocateMenu = ($imageLocateMenu != null) ? $imageLocateMenu : __DIR__."/".self::INTERNAL_IMAGE_LOCATE_MENU;
		$this->imageLocateItem = ($imageLocateItem != null) ? $imageLocateItem : __DIR__."/".self::INTERNAL_IMAGE_LOCATE_ITEM;
	}
	
	/**
	 * Modifie le format de sortie
	 * @param String $outputFormat
	 */
	private function setOutputFormat($outputFormat) {
		$this->outputFormat = $outputFormat;
	}
	
	/**
	 * Methode: execute()
     * Exï¿½cute l'accï¿½s aux donnï¿½es sï¿½mantiques de serveurs WMS ou WFS avec les paramï¿½tres de la requï¿½te HTTP.
     * 
     * Paramï¿½tres de la requï¿½te:
     * infos - {String} Flux XML listant les ressources WMS ou WFS ï¿½ contacter, conforme au schï¿½ma fourni par la mï¿½thode <SchemaFactory.getRequest>.
     * format - {"JSON" | "HTML"} Format souhaitï¿½ pour le flux de la rï¿½ponse. Optionnel ("XML" par dï¿½faut)
     * dataMask - {String} Flux FEI pour filtrer les donnï¿½es interrogï¿½es selon un ensemble de conditions attributaires. Utilisï¿½ pour les requï¿½tes attributaires
     * gmlMask - {String} Flux GML pour filtrer les donnï¿½es interrogï¿½es selon l'inclusion dans un objet polygonal. Utilisï¿½ pour les sï¿½lections polygonales
     * pixelMask - {String} Coordonnï¿½es "pixels" correspondant ï¿½ un point de la carte. Utilisï¿½ pour les sï¿½lections ponctuelles et les info-bulles
     * withReturn - {"true" | "false"} Indique si le flux de la rï¿½ponse doit inclure les informations nï¿½cessaires ï¿½ la localisation directe sur les objets fournis. Optionnel pour les formats JSON et HTML ("false" par dï¿½faut)
     * withCsvExport - {"true" | "false"} Indique si le flux de la rï¿½ponse doit inclure les informations nï¿½cessaires ï¿½ l'exportation CSV des objets fournis. Optionnel pour les formats JSON et HTML ("false" par dï¿½faut)
     * 
     * Exemple de valeur pour le paramï¿½tre "dataMask":
     * (start code)
     * 	<PropertyIsEqualTo>
     * 		<PropertyName>Vitesse_limite</PropertyName>
     * 		<Literal>110 km/h</Literal>
     * 	</PropertyIsEqualTo>
     * (end)
     * 
     * Exemple de valeur pour le paramï¿½tre "gmlMask":
     * (start code)
     * 	<wfs:FeatureCollection xmlns:wfs=\"http://www.opengis.net/wfs\">
     * 		<gml:featureMember xmlns:gml=\"http://www.opengis.net/gml\">
     * 			<feature:features xmlns:feature=\"http://mapserver.gis.umn.edu/mapserver\">
     * 				<feature:geometry>
     * 					<gml:Polygon>
     * 						<gml:outerBoundaryIs>
     * 							<gml:LinearRing>
     * 								<gml:coordinates decimal=\".\" cs=\",\" ts=\" \">791779.9552533333,1854000.6277333333 ...</gml:coordinates>
     * 							</gml:LinearRing>
     * 						</gml:outerBoundaryIs>
     * 					</gml:Polygon>
     * 				</feature:geometry>
     * 			</feature:features>
     * 		</gml:featureMember>
     * 	</wfs:FeatureCollection>
     * (end)
     * 
     * Exemple de valeur pour le paramï¿½tre "pixelMask":
     * (start code)
     * 	#X=154#Y=96
     * (end)
     */
	public function execute() {
		
		$mesErrLog = 'Erreur lors de la recherche des informations.';
		$mesErr = '<div align=\'center\'><table border=\'0\' height=\'100%\'><tr height=\'33%\'><td>&nbsp;</td></tr><tr height=\'34%\'><td><table cellpadding=\'30\'><tr>' . '<td style=\'font-family:Arial;font-size:12pt;font-weight:bold;text-align:center;color:blue;background:white;border-width:1;border-color:blue;border-style:solid;\'>' . $mesErrLog . '</td></tr></table></td></tr><tr height=\'33%\'><td>&nbsp;</td></tr></table></div>';
		$resultFormat = $_POST ["format"];
		if ($resultFormat != null) {
			if ($resultFormat == "JSON") {
				$this->setOutputFormat ( "text/plain" );
			} else if ($resultFormat == "HTML") {
				$this->setOutputFormat ( "text/html" );
			}
		}
		
		$theGetFeatureXxxURL = $_POST ['infos'];
		$dataMask = null;
		$dataMask = $_POST ['dataMask'];
		if ($dataMask != null) {
			$dataMask = '<Filter>' . $dataMask . '</Filter>';
			$dataMask = str_replace ( '<', '%3C', $dataMask );
			$dataMask = str_replace ( '>', '%3E', $dataMask );
			$dataMask = str_replace ( '\"', '"', $dataMask );
			$dataMask = str_replace ( '"', '%22', $dataMask );
			$dataMask = str_replace ( '+', '%2B', $dataMask );
			$dataMask = str_replace ( ' ', '%20', $dataMask );
		}
		
		$gmlMask = null;
		if (isset ( $_POST ['gmlMask'] ) && $_POST ['gmlMask'] != '') {
			$gmlMask = $_POST ['gmlMask'];
			$gmlMask = '<Filter><Intersects><PropertyName>msGeometry</PropertyName>' . $gmlMask . '</Intersects></Filter>';
			$gmlMask = str_replace ( '<', '%3C', $gmlMask );
			$gmlMask = str_replace ( '>', '%3E', $gmlMask );
			$gmlMask = str_replace ( '\"', '"', $gmlMask );
			$gmlMask = str_replace ( '#', ' ', $gmlMask );
			$gmlMask = str_replace ( ' ', '%20', $gmlMask );
		
		}
		$pixelMask = null;
		if (isset ( $_POST ['pixelMask'] ) && $_POST ['pixelMask'] != '') {
			$pixelMask = $_POST ['pixelMask'];
		}
		$withReturn = null;
		if (isset ( $_POST ['withoutReturn'] ) && $_POST ['withoutReturn'] != '') {
			$withReturn = $_POST ['withoutReturn'];
		}
		$withCsvExport = null;
		if (isset ( $_POST ['withCsvExport'] ) && $_POST ['withCsvExport'] != '') {
			$withCsvExport = $_POST ['withCsvExport'];
		}
		$theGetFeatureXxxURL = str_replace ( "&", "&amp;", rawurldecode ( $theGetFeatureXxxURL ) );
		try {
			XmlTools::validateXmlString ( $theGetFeatureXxxURL, SchemaFactory::getRequest () );

			$stylesCssBuf = file_get_contents ( $this->stylesCssFileName );
			if ($stylesCssBuf == FALSE) {
				echo $mesErr;
				return;
			}
			
			$elementNames = array("Request");
			$infos = HostMapping::mapHostsToDoc ( $theGetFeatureXxxURL, $this->xmlHostsFileName, $elementNames );
			$requestList = XmlTools::getNodeList ( $infos, '//Layer' );
			$emptyLayers = '';
			$nbemptyLayers = 0;
			
			$layersInfosXML = array ();
			$layersMinScales = array ();
			$layersMaxScales = array ();
			
			foreach ( $requestList as $itemRequest ) {
				$fluxInfos = '';
				try {
					$layerName = XmlTools::getStringParam ( $itemRequest, 'Name' );
					$layerRequestNodes = XmlTools::getNodeList ( $itemRequest, 'Request' );
					$layerMinScale = null;
					$layerMaxScale = null;
					try {
						$layerMinScale = XmlTools::getStringParam ( $itemRequest, 'LayerMinScale' );
					} catch ( XPathException $ex ) {
						$layerMinScale = 'null';
					}
					try {
						$layerMaxScale = XmlTools::getStringParam ( $itemRequest, 'LayerMaxScale' );
					} catch ( XPathException $ex ) {
						$layerMaxScale = 'null';
					}
					foreach ( $layerRequestNodes as $layerRequestNode ) {
						$layerRequest = $layerRequestNode->firstChild->nodeValue;
						$layerRequest = str_replace ( "&amp;", "&", $layerRequest );
						$layerRequest = str_replace ( "+", "%2B", $layerRequest );
						if ($gmlMask != null) {
							$layerRequest .= '&FILTER=' . $gmlMask;
						}
						if ($dataMask != null) {
							$layerRequest .= '&FILTER=' . $dataMask;
						}
						if ($pixelMask != null) {
							$layerRequest .= str_replace ( "#", "&", $pixelMask );
						}
						$getFeatureXxxResultXML = HTTPSender::getGetResponse ( $layerRequest, $this->httpProxy );
						//var_dump($layerRequest, utf8_encode($getFeatureXxxResultXML));
            
            $params = array ();
						$params ['layerName'] = htmlentities ( $layerName );
						$params ['layerMinScale'] = $layerMinScale;
						$params ['layerMaxScale'] = $layerMaxScale;
						$params ['pictoLocateMenu'] = $this->imageLocateMenu;
						$params ['pictoLocate'] = $this->imageLocateItem;
						$xsl = file_get_contents(__DIR__."/".$this->internalXslFile);
					
						try {
                $fluxInfos .= XmlTools::executeInternalXSL (  ( $getFeatureXxxResultXML ), $xsl, $params );
            } catch(\Exception $exception){
                $fluxInfos .= XmlTools::executeInternalXSL (  utf8_encode( $getFeatureXxxResultXML ), $xsl, $params );
            }
					}
				} catch ( HttpSenderException $ex ) {
					//flux vide
				} catch ( XPathException $ex ) {
					// abandon pour cet element de la liste
				}
				
				if ($fluxInfos == '' && $this->outputFormat == 'text/html') {
					$emptyLayers .= '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $layerName;
					$nbemptyLayers ++;
				} else {
					$fluxInfos = str_replace ( chr ( 13 ), "", $fluxInfos );
					$fluxInfos = str_replace ( chr ( 10 ), "", $fluxInfos );
					$layersInfosXML [] = "<FeatureCollection name=\"" . ($layerName) . "\">" . $fluxInfos . "</FeatureCollection>";
					$layersMinScales [] = $layerMinScale;
					$layersMaxScales [] = $layerMaxScale;
				}
			}
			
			$outputString = "";
			
			if ($this->outputFormat == 'text/xml') {
				$outputString .= '<?xml version="1.0" encoding="UTF-8"?>';
				foreach ( $layersInfosXML as $layersInfosXMLItem ) {
					$outputString .= $layersInfosXMLItem;
				}
			} elseif ($this->outputFormat == 'text/html') {
				$outputString .= "<html><head><title>Informations</title><style>" . $this->getCssStyles () . "</style></head><body class=\"resultat\">";
				if ($withCsvExport != null) {
					$outputString .= "<form action=\"#\" method=\"post\"><input type=\"hidden\" name=\"CSVcontent\" id=\"CSVcontent\" value=\"\"/></form>";
				}
				
				$xsl = file_get_contents(__DIR__."/".$this->internalXsl2HtmlFile);
				foreach ( $layersInfosXML as $i => $layersInfosXMLItem ) {
					$htmlTable = "";
					if ($this->xslFile == null) {
						$htmlTable .= XmlTools::executeInternalXSL( ($layersInfosXMLItem), $xsl, $this->getXsltParams($layersMinScales[$i], $layersMaxScales[$i], $withReturn));
					} else {
						$htmlTable .= XmlTools::executeXSL( ($layersInfosXMLItem), $this->xslFile, $this->getXsltParams($layersMinScales[$i], $layersMaxScales[$i], $withReturn));
					}
					
					$outputString .= $htmlTable;
					if ($withCsvExport != null) {
						$outputString .= "<br/><div align=\"center\"><input type=\"submit\" csvExport=\"true\" value=\"Export CSV\" /></div><br/>";
					}
				}
				if ($nbemptyLayers != 0) {
					$outputString .= "<span class=\"entete\">" . "<br/>Aucun objet trouvé pour " . (($nbemptyLayers == 1) ? "le thème suivant" : "les thèmes suivants") . " :" . $emptyLayers . "</span>";
				}
				$outputString .= "</body></html>";
			
			} elseif ($this->outputFormat == 'text/plain') {
				$outputArray = array();
				$xsl = file_get_contents(__DIR__."/".$this->internalXsl2PhpArrayFile);
				foreach ( $layersInfosXML as $i => $layersInfoXMLItem ) {
					$php = XmlTools::executeInternalXSL (  ( $layersInfoXMLItem ), $xsl, $this->getXsltParams ( $layersMinScales [$i], $layersMaxScales [$i], $withReturn ) );
					$php = eval($php);
					$outputArray[] = $php;
				}
				$outputString = json_encode($outputArray);
			}
			header ( 'Content-type: ' . $this->outputFormat );
			echo $outputString;
		} catch ( HostMappingException $ex ) {
			echo $ex->getMessage ();
			exit ();
		} catch ( SchemaValidationException $ex ) {
			echo $ex->getMessage ();
			exit ();
		} catch ( DomLoaderException $ex ) {
			echo $mesErr;
			exit ();
		} catch ( Exception $ex ) {
			echo $ex->getMessage ();
			exit ();
		}
	}
	
	/**
	 */
	private function getXsltParams($layerMinScale, $layerMaxScale, $withReturn) {
		$params = array ();
		$params ["layerMinScale"] = $layerMinScale;
		$params ["layerMaxScale"] = $layerMaxScale;
		if ($withReturn != null) {
			$params ["withReturn"] = $withReturn;
		}
		if ($this->imageLocateMenu != null) {
			$params ["pictoLocateMenu"] = $this->imageLocateMenu;
		}
		if ($this->imageLocateItem != null) {
			$params ["pictoLocate"] = $this->imageLocateItem;
		}
		return $params;
	}
	
	/**
	 */
	private function getCssStyles() {
		$stylesCssBuf = "";
		if ($this->stylesCssFileName == null) {
			$stylesCssBuf = file_get_contents ( self::INTERNAL_STYLE_CSS );
		} else {
			$stylesCssBuf = file_get_contents ( $this->stylesCssFileName );
		}
		return $stylesCssBuf;
	}
}
?>
