<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

#require_once __DIR__ .'/../../../log4php/ConfigureLogger.php';

require_once 'HostMapping.php';
require_once 'SchemaFactory.php';
require_once __DIR__ .'/../tools/XmlTools.php';
require_once __DIR__ .'/../tools/HTTPProxy.php';

/**
 * Class: ExportMapHelper
 * Classe de g�n�ration d'exports PDF ou PNG.
 * 
 * :
 * La classe lance un thread d�rivant de <AbstractMapWriter> dont l'avancement est examin� pour informer l'appelant.
 */
class ExportMapHelper { 
	const EXPORT_PNG = "PNG";
	const EXPORT_PDF = "PDF";
	const CONTENT_TYPE_PNG = "image/png";
	const CONTENT_TYPE_PDF = "application/pdf";

	private $xmlHostFileName = null;
	private $httpProxy;
	private $logoDirectory;
	private $exportFileFormat;
	private $contentType;

	private $tempDirectory;
	
	private $_logger;

	/**
	 * Constructeur: ExportMapHelper(String, String, HTTPProxy, String, String)
	 * Constructeur d'instances
	 *
	 * Param�tres:
	 * $xmlHostsFileName - {String} Eventuel fichier de correspondance des Hosts
	 * $exportFileFormat - {String} Format d'export demand�
	 * $httpProxy - {<HTTPProxy>} Proxy.
	 * $logoDirectory - {String} R�pertoire de stockage des logos
	 * $tempDirectory - {String} R�pertoire temporaire
	 */
	public function __construct($xmlHostFileName, $exportFileFormat, $httpProxy, $logoDirectory = null, $tempDirectory = null) {
		
		#$this->_logger = Logger::getLogger('ExportMapHelper');
		
		$this->xmlHostFileName = $xmlHostFileName;
		$this->httpProxy = $httpProxy;
		$this->logoDirectory = $logoDirectory;
		$this->exportFileFormat = $exportFileFormat;
		if ($this->exportFileFormat == self::EXPORT_PDF) {
			$this->contentType = self::CONTENT_TYPE_PDF;
		} else {
			$this->contentType = self::CONTENT_TYPE_PNG;
		}
		$this->tempDirectory = $tempDirectory;
	}
	/**
	 * Methode: execute()
     * Ex�cute l'exportation avec les param�tres de la requ�te HTTP.
     * 
     * Param�tres de la requ�te:
     * paramsExport - {String} Flux XML du contenu de l'export souhait�, conforme au sch�ma fourni par la m�thode <SchemaFactory.getExport>.
     * printerSetupParams - {String} Flux XML des param�tres de mise en page de l'export souhait�, conforme au sch�ma fourni par la m�thode <SchemaFactory.getPrinterParams>.
     */
	public function execute() {
		session_start();
		if (isset($_POST['paramsExport'])) {
			try {
				unset($_SESSION["mapWriter"]); 
				unset($_SESSION["telecharger"]); 
				$paramsExport = str_replace("&", "&amp;", $_POST['paramsExport']);
				$elementNames[] = array('Url', 'UrlLegende', 'UrlLogo');
				
				XmlTools::validateXmlString($paramsExport, SchemaFactory::getExport());
				$paramsExportDOM = HostMapping::mapHostsToDoc($paramsExport, $this->xmlHostFileName, $elementNames);
				
				if ($this->exportFileFormat == self::EXPORT_PDF) {
					$mapWriter = new PDFMapWriter($this->httpProxy);
					$mapWriter->setTempDirectory($this->tempDirectory);
				}
				if ($this->exportFileFormat == self::EXPORT_PNG) {
					$mapWriter = new PNGMapWriter($this->httpProxy);
					$mapWriter->setTempDirectory($this->tempDirectory);
				}
	
				try {
					$mapWriter->setTitreCarte(XmlTools::getStringParam($paramsExportDOM, '//Titre'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setLargeurPixCarte(XmlTools::getNonStringParam($paramsExportDOM, '//Largeur'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setHauteurPixCarte(XmlTools::getNonStringParam($paramsExportDOM, '//Hauteur'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setAuteurCarte(XmlTools::getStringParam($paramsExportDOM, '//Auteur'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setCopyrightCarte(XmlTools::getStringParam($paramsExportDOM, '//Copyright'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setDescriptionCarte(XmlTools::getStringParam($paramsExportDOM, '//Description'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setDateValiditeCarte(XmlTools::getNonStringParam($paramsExportDOM, '//DateValidite'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setApplicationInfos(XmlTools::getStringParam($paramsExportDOM, '//ApplicationInfos'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setCreator(XmlTools::getStringParam($paramsExportDOM, '//Creator'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setUrlCartes(XmlTools::getUrlArrayParam($paramsExportDOM, '//Couche/Url'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setUrlLegendes(XmlTools::getUrlArrayParam($paramsExportDOM, '//UrlLegende'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setUrlLogos(XmlTools::getUrlArrayParam($paramsExportDOM, '//UrlLogo'));
				} catch (Exception $e) {
				}
				try {
				$mapWriter->setOpacites(XmlTools::getFloatArrayParam($paramsExportDOM, '//Couche/Opacite'));
				} catch (Exception $e) {
				}
				try {
					$mapWriter->setFileLogos(XmlTools::getStringArrayParam($paramsExportDOM, '//FichierLogo', $this->logoDirectory));
				} catch (Exception $e) {
				}		
				
				if (isset($_POST['printerSetupParams'])) {
					$printerSetupParams = $_POST['printerSetupParams'];
					XmlTools::validateXmlString($printerSetupParams, SchemaFactory::getPrinterParams());
				
					$printerSetupParamsDOM = DOMDocument::loadXML('<?xml version="1.0" encoding="iso-8859-1"?> ' . $printerSetupParams);
					
					$mapWriter->setPageFormat(XmlTools::getStringParam($printerSetupParamsDOM, '//Format'));
					$mapWriter->setPageOrientation(XmlTools::getStringParam($printerSetupParamsDOM, '//Orientation'));
					$mapWriter->setPageUnits(XmlTools::getNonStringParam($printerSetupParamsDOM, '//Margins/Units'));
					$mapWriter->setPageMarginTop(XmlTools::getNonStringParam($printerSetupParamsDOM, '//Margins/Top'));
					$mapWriter->setPageMarginBottom(XmlTools::getNonStringParam($printerSetupParamsDOM, '//Margins/Bottom'));
					$mapWriter->setPageMarginLeft(XmlTools::getNonStringParam($printerSetupParamsDOM, '//Margins/Left'));
					$mapWriter->setPageMarginRight(XmlTools::getNonStringParam($printerSetupParamsDOM, '//Margins/Right'));
				}
				$_SESSION["mapWriter"] = &$mapWriter;
				ignore_user_abort(1);
				$this->progression($mapWriter);
				set_time_limit(0);
				$mapWriter->run();
			} catch (SchemaValidationException $ex) {
				echo $ex->getMessage();
				exit;
			}
		} elseif (isset($_SESSION["mapWriter"])) {
			$mapWriter = $_SESSION["mapWriter"];
	       	if ($_SESSION["telecharger"] != 1) {
				$this->progression($mapWriter);
			} else {
				unset($_SESSION["mapWriter"]); 
				unset($_SESSION["telecharger"]); 
				$mapWriter->out();
			}
		} 
	}

/**
 * Controle l'�tat d'avancement de l'export
 * @param MapWriter $mapWriter 
 */
	private function progression ($mapWriter) {
		if ($mapWriter == null) {
			$this->isError(new Exception("Pas de parametre 'pdf' en requete."));
		}
		switch ($mapWriter->getAvancement()) {
			case -1:
				$this->isError(new Exception($mapWriter->getErreur()));
				return;
			case 100:
				if ($_SESSION["telecharger"] != 1) {
					$this->isFinished();
				}
				 return;
			default:
				$this->isBusy($mapWriter);
				 return;
		}
	}

/**
 * Ex�cut� si l'export est en cours
 * @param MapWriter $mapWriter 
 */
	private function isBusy($mapWriter) {
		$sortie = "<html><head><meta http-equiv=\"Refresh\" content=\"2\"><link rel='stylesheet' type='text/css' href='/descartes/theme/descartes.css' /></head>";
		$sortie .= "<body><div class='DescartesQueryResultWaitingMessage'></div></body></html>";
		echo $sortie;
		flush();
	}

/**
 * Ex�cut� lorsque l'export est fini
 */
	private function isFinished() {
		try {
			$sortie = "<html><head><title>La carte est pr&ecirc;te!</title></head><body bgcolor=\"#FFFFFF\" text=\"#0000A0\">";
			$sortie .= "<div align=\"center\" style=\"font-family:Arial;font-size:8pt;\"><br/>";
			$sortie .= "<form method=\"POST\" onsubmit=\"this.style.display='none';document.getElementById('fin').style.display='block'\">Le document est pr&ecirc;t:<br/><br/>";
			$sortie .= "<input type=\"Submit\" style=\"font-family:Arial;font-size:8pt;\" value=\"T&eacute;l&eacute;charger le " . $this->exportFileFormat . "\"></form>";
			$sortie .= "<div id=\"fin\" style=\"display:none\"><br/>T&eacute;l&eacute;chargement termin&eacute;.<br/><br/>";
			$sortie .= "<a href=\"\" onclick=\"window.close()\">Fermer cette fen&ecirc;tre.</a></div>";
			$sortie .= "</div></body></html>";
			$_SESSION["telecharger"] = 1;
			echo $sortie;
			flush();
		} catch (Exception $ex) {
			$this->isError($ex);
		}
	}

/**
 * Ex�cut� lorsque l'export a �chou�
 * @param Exception $ex 
 */
	private function isError($ex) {
		$sortie = "<html><head><title>Erreur</title></head><body>";
		$sortie .= "Une erreur est survenue. " . $ex->getMessage() . "</body></html>";
		echo $sortie;
		flush();
	}

/**
 * Construit une progressbar en HTML en fonction d'un pourcentage
 * @param Real $pourcent Pourcentage
 * @return Text $barHTML Barre de progression
 */
	private function progressBar($pourcent) {
		$barWidth = 400;
		$barDone = ($pourcent * $barWidth / 100);
		$barToDo = $barWidth - $barDone;
		$colorDone = "#0000A0";
		$colorToDo = "#FFFFFF";
		$barHTML = "";
		$barHTML .= "<table cellpadding=\"0\" cellspacing=\"0\" width=\"" . $barWidth . "\" height=\"8\" style=\";border:1px solid " . $colorDone . ";\"><tr>";
		if ($pourcent != 0) {
			$barHTML .= "<td style=\"background-color:" . $colorDone . ";\" width=\"" . $barDone . "\">&nbsp;</td>";
		}
		$barHTML .= "<td style=\"background-color:" . $colorToDo . ";\" width=\"" . $barToDo . "\">&nbsp;</td>";
		$barHTML .= "</tr></table>";

		return $barHTML;
	}
}
?>