<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

require_once 'AbstractMapWriter.php';
require_once __DIR__ .'/../tools/HTTPSender.php';

/**
 * Class: PNGMapWriter
 * Classe du thread de g�n�ration PNG.
 * 
 * H�rite de:
 * - <AbstractMapWriter>
 */
class PNGMapWriter extends AbstractMapWriter {
	const fontTaille = 10;
	const fontFileName = '../core/resources/arial.ttf';
	const hauteurEchelle = 5;
	const dxEchelle = 10;
	const dyEchelle = 15;
	const dxCopyright = 10;
	const dyCopyright = 15;
	private $tempDirectory;
	private $imageComplete;

/**
 * Initialise le pas de progression
 */
	private function initPas() {
                $nb = count($this->urlCartes);
                if ($nb > 0) {
                        $this->pas = (100 / $nb);
                }
        $this->_logger->debug("pas = ".$this->pas);
        }
	
	/**
	 * Methode: setTempDirectory(String)
	 * Renseigne le r�pertoire temporaire.
	 * 
	 * Param�tres: 
	 * $tempDirectory - {String} R�pertoire temporaire
	 */
	public function setTempDirectory($tempDirectory) {
                $this->tempDirectory = $tempDirectory;
        }
	
	/**
	 * Methode: run()
	 * Ex�cute l'export et stocke le PNG dans un fichier temporaire.
	 */
	public function run() {
		$this->initPas();
		$image = ImageCreateTrueColor($this->largeurPixCarte, $this->hauteurPixCarte); // BufferedImage.TYPE_INT_RGB ?
		$blanc = ImageColorAllocate($image, 255, 255, 255);
		ImageFill($image, 0, 0, $blanc);		
		$noir = ImageColorAllocate($image, 0, 0, 0);

		$urls = $this->urlCartes;
		$opacites = $this->opacites;
		$this->avancement = 1;
		$this->_logger->debug("sizeof(\$urls) = ".sizeof($urls));
		for ($i = 0; $i < sizeof($urls); $i++) {
        	$this->_logger->debug("\$urls[".$i."] = ".$urls[$i]);
			$imString = HTTPSender::getGetResponse($urls[$i], $this->httpProxy,'');
			if ($imString != '<VIDE/>') {
				$im = imagecreatefromstring($imString);
				imagecopymerge($image, $im, 0, 0, 0, 0, $this->largeurPixCarte, $this->hauteurPixCarte, $opacites[$i]);
			}
			$this->avancement += $this->pas;
		}

		$petiteLongueur = $this->longueurEchelle / 4; 
		for ($i = 0; $i < 4; $i++) {
			ImageFilledRectangle($image, self::dxEchelle + $i * $petiteLongueur, $this->hauteurPixCarte - self::dyEchelle, 
			self::dxEchelle + ($i + 1) * $petiteLongueur, $this->hauteurPixCarte - (self::dyEchelle - self::hauteurEchelle), $i % 2 ? $noir: $blanc);
		}
		ImageRectangle($image, self::dxEchelle, $this->hauteurPixCarte - self::dyEchelle, self::dxEchelle + 4 * $petiteLongueur,
						$this->hauteurPixCarte - (self::dyEchelle - self::hauteurEchelle), $noir);

		$xx = self::dxEchelle + 4 * $petiteLongueur + 4;
		$yy = $this->hauteurPixCarte - self::dyEchelle + self::hauteurEchelle;
		$texte = $this->affichageEchelle;
		$boite = Imagettfbbox(self::fontTaille, 0, __DIR__."/".self::fontFileName, $texte);
		Imagefilledrectangle ($image, $xx + $boite[6], $yy + $boite[7], $xx + $boite[2], $yy + $boite[3], $blanc);
		Imagefttext($image, self::fontTaille, 0, $xx, $yy, $noir, __DIR__."/".self::fontFileName, $texte);

		$texte = '&copy;'.$this->copyrightCarte;
		if ($texte != '&copy;') {
			$xx = self::dxCopyright;
			$yy = self::dyCopyright;		
			$boite = Imagettfbbox(self::fontTaille, 0, __DIR__."/".self::fontFileName, $texte);
			Imagefilledrectangle ($image, $xx + $boite[6], $yy + $boite[7],	$xx + $boite[2], $yy + $boite[3], $blanc);
			Imagefttext($image, self::fontTaille, 0, $xx, $yy, $noir, __DIR__."/".self::fontFileName, $texte);
		}
		$this->avancement = 100;
		$this->imageComplete = tempnam($this->tempDirectory, 'descartes_temp');
		ImagePng($image, $this->imageComplete);
		imagedestroy($image);

	}
	/**
	 * Methode: out()
	 * Envoie l'image compl�te au client.
	 */
	function out() {
		header('Content-type: image/png');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header ("Content-Disposition: Attachment;filename=" . $this->getTitreCarte() . ".png");
		$imageFinale = imagecreatefrompng($this->imageComplete);
		unlink($this->imageComplete);
		ImagePng($imageFinale);
		imagedestroy($imageFinale);
	}
}
?>