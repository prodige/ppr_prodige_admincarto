<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright MEDDTL
Contributeurs :
     Gaëlle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.
-->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes" indent="no" />

	<xsl:variable name="majus">AZERTYUIOPQSDFGHJKLMWXCVBN.</xsl:variable>
	<xsl:variable name="minus">azertyuiopqsdfghjklmwxcvbn.</xsl:variable>
	<xsl:param name="layerMinScale">null</xsl:param>
	<xsl:param name="layerMaxScale">null</xsl:param>
	<xsl:param name="withReturn">false</xsl:param>
	<xsl:param name="pictoLocateMenu">getImage?img=icon_preview.png</xsl:param>
	<xsl:param name="pictoLocate">getImage?img=icon_locate.png</xsl:param>

	<xsl:template match="/">
		<xsl:apply-templates select="//FeatureCollection"/>
	</xsl:template>

	<xsl:template match="FeatureCollection">
		<xsl:if test= "count(//node()[local-name()='ServiceException'])=0 and count(node()[local-name()='featureMember'])!=0">
			<span class="entete">
				<xsl:text>Th�me </xsl:text>
				<b><xsl:value-of select="@name" /></b>
				<xsl:text> : </xsl:text>
				<xsl:value-of select="count(node()[local-name()='featureMember'])" />
				<xsl:choose>
					<xsl:when test="count(node()[local-name()='featureMember'])=1">
						<xsl:text> objet trouv�</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> objets trouv�s</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<table class="tableau" cellpadding="5" cellspacing="0">
				<tr>
				<xsl:apply-templates select="node()[local-name()='featureMember'][1]/*" mode="entete"/>
				</tr>
				<xsl:apply-templates select="featureMember"/>
			</table>		
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="node()" mode="entete">
			<xsl:choose>
				<xsl:when test="name()='gml:boundedBy'">
					<xsl:if test="$withReturn = 'false'">
						<th><img src='{$pictoLocateMenu}' /></th>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<th><xsl:value-of select="translate(local-name(),'_',' ')" /></th>
				</xsl:otherwise>
			</xsl:choose>
	</xsl:template>	

	<xsl:template match="featureMember">	
		<tr>
		<xsl:if test="$withReturn = 'true'">
			<xsl:apply-templates select="*" mode="bounds"/>
		</xsl:if>
		<xsl:apply-templates select="*" mode="property"/>
		</tr>
	</xsl:template>	

	<xsl:template match="node()" mode="bounds">
		<xsl:if test="name()='gml:boundedBy'">
			<td>
			<img src='{$pictoLocate}' border='0'>
				<xsl:attribute name="boundsAndScales">
					<xsl:text>[[</xsl:text><xsl:value-of select="translate(normalize-space(*/*),' ',',')" /><xsl:text>], </xsl:text><xsl:value-of select="$layerMinScale" /><xsl:text>, </xsl:text><xsl:value-of select="$layerMaxScale" /><xsl:text>];</xsl:text>
				</xsl:attribute>
			</img>
			</td>
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="node()" mode="property">
		<xsl:if test="name()!='gml:boundedBy'">
			<td>
				<xsl:choose>
					<xsl:when test="starts-with(normalize-space(.),'http:')">
						<xsl:element name="a">
							<xsl:attribute name="href">
								<xsl:value-of select="normalize-space(text())" />
							</xsl:attribute>
							<xsl:attribute name="target">
								<xsl:text>_blank</xsl:text>
							</xsl:attribute>
			
							<xsl:param name="typeLien">
								<xsl:call-template name="decodeTypeLien">
									<xsl:with-param name="lien" select="normalize-space(text())"></xsl:with-param>
								</xsl:call-template>
							</xsl:param>
							<xsl:choose>
								<xsl:when test="$typeLien='IMG'">
									<xsl:element name="img">
										<xsl:attribute name="src">
											<xsl:value-of select="normalize-space(text())" />
										</xsl:attribute>
										<xsl:attribute name="width">
											<xsl:value-of select="'100'" />
										</xsl:attribute>
									</xsl:element>
								</xsl:when>
								<xsl:when test="$typeLien='FILE'">
								<xsl:text>T�l�charger </xsl:text><xsl:value-of select="translate(substring(normalize-space(text()),string-length()-2),$minus,$majus)" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Consulter</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
			
						</xsl:element>
					</xsl:when>
					<xsl:when test=".=''">
						<xsl:text>-</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="text()" />
					</xsl:otherwise>
				</xsl:choose>	
			</td>
		</xsl:if>
	</xsl:template>	

	<xsl:template name="decodeTypeLien">
		<xsl:param name="lien"></xsl:param>
		<xsl:choose>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.GIF'">
				<xsl:text>IMG</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.JPG'">
				<xsl:text>IMG</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-4),$minus,$majus)='.JPEG'">
				<xsl:text>IMG</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.PNG'">
				<xsl:text>IMG</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.ZIP'">
				<xsl:text>FILE</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.PDF'">
				<xsl:text>FILE</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.ODT'">
				<xsl:text>FILE</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.ODS'">
				<xsl:text>FILE</xsl:text>
			</xsl:when>
			<xsl:when test="translate(substring($lien,string-length()-3),$minus,$majus)='.ODP'">
				<xsl:text>FILE</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>URL</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
