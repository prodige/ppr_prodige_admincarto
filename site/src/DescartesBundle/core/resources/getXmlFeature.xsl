<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright MEDDTL
Contributeurs :
     Gaëlle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.
-->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes" indent="no"/>

	<xsl:template match="/">
		<xsl:if test="count(//node()[local-name()='ServiceException']) = 0">
			<xsl:if test="count(*[local-name()='FeatureCollection']) != 0">
				<xsl:apply-templates select="node()[local-name()='FeatureCollection']" mode="root"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="node()" mode="root">
			<xsl:apply-templates select="node()[local-name()='featureMember']/*" mode="feature"/>
	</xsl:template>	

	<xsl:template match="node()" mode="feature">
		<xsl:element name="featureMember">
			<xsl:apply-templates select="*" mode="bounds"/>
			<xsl:apply-templates select="*" mode="property"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="node()" mode="bounds">
		<xsl:if test="name()='gml:boundedBy'">
			<xsl:copy-of select="."></xsl:copy-of>
		</xsl:if>
	</xsl:template>

	<xsl:template match="node()" mode="property">
		<xsl:if test="starts-with(name(),'gml:')=false and count(child::node()[starts-with(name(),'gml:')])=0">
			<xsl:element name="{local-name()}">
				<xsl:choose>
					<xsl:when test=".=''">
						<xsl:text>-</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="text()" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:if>
	</xsl:template>	

</xsl:stylesheet>
