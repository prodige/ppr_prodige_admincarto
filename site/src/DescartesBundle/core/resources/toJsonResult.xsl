<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright MEDDTL
Contributeurs :
     Gaëlle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.
-->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="text" encoding="UTF-8" />

	<xsl:param name="quote"><xsl:text>"</xsl:text></xsl:param>
	<xsl:param name="escapedQuote"><xsl:text>\"</xsl:text></xsl:param>
	<xsl:param name="layerMinScale">null</xsl:param>
	<xsl:param name="layerMaxScale">null</xsl:param>
	<xsl:param name="withReturn">false</xsl:param>

	<xsl:template match="/">
		<xsl:apply-templates select="//FeatureCollection"/>
	</xsl:template>

	<xsl:template match="FeatureCollection">
		<xsl:text>{"layerTitle":"</xsl:text><xsl:value-of select="@name" /><xsl:text>","layerMinScale":</xsl:text><xsl:value-of select="$layerMinScale" /><xsl:text>,"layerMaxScale":</xsl:text><xsl:value-of select="$layerMaxScale" /><xsl:text>,"layerDatas":[</xsl:text>
			<xsl:apply-templates select="featureMember"/>
		<xsl:text>]}</xsl:text>
	</xsl:template>	

	<xsl:template match="featureMember">
		<xsl:text>{</xsl:text>
		<xsl:if test="$withReturn = 'true'">
			<xsl:apply-templates select="*" mode="bounds"/>
		</xsl:if>
		<xsl:apply-templates select="*" mode="property"/>
		<xsl:text>}</xsl:text>
		<xsl:if test="following-sibling::*">,</xsl:if>
	</xsl:template>	

	<xsl:template match="node()" mode="bounds">
		<xsl:if test="name()='gml:boundedBy'">
			<xsl:text>"bounds":[</xsl:text>
			<xsl:value-of select="translate(normalize-space(*/*),' ',',')" />
			<xsl:text>],</xsl:text>
		</xsl:if>
	</xsl:template>	

	<xsl:template match="node()" mode="property">
		<xsl:if test="name()!='gml:boundedBy'">
			<xsl:text>"</xsl:text><xsl:value-of select="translate(local-name(),'_',' ')" /><xsl:text>":"</xsl:text>
			<xsl:call-template name="string-replace-all">
				<xsl:with-param name="text" select="text()" />
				<xsl:with-param name="replace" select="$quote" />
				<xsl:with-param name="by" select="$escapedQuote" />
			</xsl:call-template>
			<xsl:text>"</xsl:text>
			<xsl:if test="following-sibling::*">,</xsl:if>
		</xsl:if>
	</xsl:template>	

	<xsl:template name="string-replace-all">
		<xsl:param name="text" />
		<xsl:param name="replace" />
		<xsl:param name="by" />

		<xsl:choose>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)" />
				<xsl:value-of select="$by" />
				<xsl:call-template name="string-replace-all">
					<xsl:with-param name="text" select="substring-after($text,$replace)" />
					<xsl:with-param name="replace" select="$replace" />
					<xsl:with-param name="by" select="$by" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
