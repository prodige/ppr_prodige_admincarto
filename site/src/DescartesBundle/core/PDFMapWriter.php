<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



require_once 'AbstractMapWriter.php';
require_once __DIR__ .'/../tools/HTTPSender.php';
require_once __DIR__ .'/../fpdf/fpdf.php';

/**
 * Class: PDFMapWriter
 * Classe du thread de g�n�ration PDF.
 * 
 * H�rite de:
 * - <AbstractMapWriter>
 */
class PDFMapWriter extends AbstractMapWriter {
	
	const RESOLUTION = 96;
	const interLigne = 5.9;
	private $tempDirectory;
	private $pdfComplet;

/**
 * Initialisation de la taille du pas en %
 */	
	private function initPas() {
		$nb = count($this->urlCartes) + count($this->urlLegendes);
		if ($nb > 0) {
			$this->pas = (100 / $nb);
		}
	}
	
	/**
	 * Methode: setTempDirectory(String)
	 * Renseigne le r�pertoire temporaire.
	 * 
	 * Param�tres: 
	 * $tempDirectory - {String} R�pertoire temporaire
	 */
	public function setTempDirectory($tempDirectory) {
		$this->tempDirectory = $tempDirectory;
	}

	/**
	 * Methode: run()
	 * Ex�cute l'export.
	 */
	public function run() {
		$this->initPas();
		$pourcentCarte = self::RESOLUTION;
		$pourcentLogo = self::RESOLUTION;
		$pourcentLegende = self::RESOLUTION;
		$hauteurCarte = $this->hauteurPixCarte * 10 / $pourcentCarte * 2.54;
		$largeurCarte = $this->largeurPixCarte * 10 / $pourcentCarte * 2.54;
		$margeGauche = $this->pageMarginLeft;
		$margeDroite = $this->pageMarginRight;
		$margeHaut = $this->pageMarginTop;
		$margeBas = $this->pageMarginBottom;
		
		$format=strtolower($this->pageFormat);
		$formatpapier = array( "a3", "a4", "a5", "letter", "legal");
		$fin = false;

		if(in_array($format, $formatpapier)) {
			$pdf = new fpdf($this->pageOrientation, 'mm', $this->pageFormat);
		}
		else {
			$wh = RessourceBundle::getProperty('descartes.paper.'.$this->pageFormat.'.h');
			$ww = RessourceBundle::getProperty('descartes.paper.'.$this->pageFormat.'.w');
			if($wh!=null && $ww!=null) {
				$pdf = new fpdf($this->pageOrientation, 'mm', array($ww, $wh));
			} else {
				$fin = true;
			}
		}
		if(!$fin) {
			$largeurPage = (strtolower($this->pageOrientation) == "p") ? $pdf->fh : $pdf->fw;
			$hauteurPage = (strtolower($this->pageOrientation) == "l") ? $pdf->fw : $pdf->fh;
			$pdf->SetAuthor($this->auteurCarte);
			$pdf->SetCreator($this->creator);
			$pdf->SetTitle(utf8_decode($this->titreCarte));
			$pdf->SetKeywords($this->descriptionCarte);
			$this->avancement = 1;	
			$positionX = $margeGauche;
			
			$pdf->AddPage();
			$pdf->SetFont('Helvetica', 'B', 12);
			
			$pdf->SetMargins($margeGauche, $margeHaut, $margeDroite, $margeBas);
	
			$pdf->SetXY($margeGauche, $margeHaut);
			$pdf->Cell($largeurCarte, 3, utf8_decode($this->titreCarte), 0, 1, 'C');
			$pdf->Ln(self::interLigne);

			$positionY = $pdf->GetY();
			$image = ImageCreateTrueColor($this->largeurPixCarte, $this->hauteurPixCarte);
			$blanc = ImageColorAllocate($image, 255, 255, 255);
			ImageFill($image, 0, 0, $blanc);
				
			$urls = $this->urlCartes;
			$opacites = $this->opacites;
			$size = sizeof($urls);
			for ($i = 0; $i < $size; $i++) {
				$imString = HTTPSender::getGetResponse($urls[$i], $this->httpProxy, '');
				if ($imString != '<VIDE/>') {
					$im = imagecreatefromstring($imString);
					imagecopymerge($image, $im, 0, 0, 0, 0, $this->largeurPixCarte, $this->hauteurPixCarte, $opacites[$i]);
					
					$this->avancement += $this->pas;
				}
			}
	
			$tmpfname = tempnam($this->tempDirectory, 'descartes_temp');
			imagepng($image, $tmpfname);
			$pdf->Image($tmpfname, $margeGauche, $pdf->getY(), $largeurCarte, $hauteurCarte, 'PNG');
			unlink($tmpfname);

			$pdf->SetDrawColor(0);
			$pdf->SetFillColor(255);
			$pdf->SetLineWidth(0.5);
			$pdf->Rect($margeGauche, $pdf->getY(), $largeurCarte, $hauteurCarte);

			if ($this->longueurEchelle != 0) {
				$longueurEchelle = $this->longueurEchelle / self::RESOLUTION * 10 * 2.54;
				$petiteLongueur = $longueurEchelle / 4;
				$pdf->SetLineWidth(0.2);
				for ($i = 0; $i < 4; $i++) {
					$pdf->SetFillColor($i % 2 ? 0: 255);
					$pdf->Rect($margeGauche + $i * $petiteLongueur + 5, $positionY + $hauteurCarte - 5, $petiteLongueur, 2.5, 'DF');
				}
				$pdf->SetFont('Helvetica', '', 8);
				$pdf->SetFillColor(255);
				$pdf->setXY($margeGauche + $i * $petiteLongueur + 7.5, $positionY + $hauteurCarte - 5);
				$pdf->Cell($pdf->GetStringWidth($this->affichageEchelle . ' '), 3, $this->affichageEchelle, 0, 1, 'L', 1);
			}

			if ($this->copyrightCarte != null) {
				$pdf->SetFont('Helvetica', 'B', 8);
				$pdf->setXY($margeGauche + 5, $positionY + 5);
				$pdf->Cell($pdf->GetStringWidth('©'.$this->copyrightCarte), 3, utf8_decode('©'.$this->copyrightCarte), 0, 1, 'L', 1);
			}

			$pdf->SetXY(0, $positionY + $hauteurCarte);
			$pdf->Ln(self::interLigne);
			if ($this->descriptionCarte != null) {
				$pdf->SetFont('Helvetica', 'B', 10);
				$pdf->Cell($largeurCarte / 4, 8, 'Description :', 0, 1, 'L', 0);
				$pdf->SetFont('Helvetica', '', 10);
				$pdf->MultiCell($largeurCarte, 8, utf8_decode($this->descriptionCarte), 0, 'L', 0);
	
			}
			$pdf->Ln(self::interLigne);
			if ($this->applicationInfos != null) {
				$pdf->SetFont('Helvetica', 'B', 8);
				$pdf->MultiCell($largeurCarte, 6, utf8_decode($this->applicationInfos), 0, 'C', 0);
			}

			$interLogo = 5;
			$placeLogoX = 0;
			$nbImgLogos = 0;
			foreach ($this->fileLogos as $file) {
				$img = imagecreatefrompng($file);
				if ($img) {
					$imgLogos[] = $img;
					$placeLogoX +=  imagesx($img);
				}
			}		
			foreach ($this->urlLogos as $url) {
				$imString = HTTPSender::getGetResponse($url, $this->httpProxy, '');
	
				if ($imString != '<VIDE/>') {
					$nbImgLogos ++;
					$placeLogoX +=  imagesx($img);
				}
			}
			$placeLogoX =  $placeLogoX * 10 / $pourcentLogo * 2.54;
			$placeLogoX += (count($imgLogos) - 1) * $interLogo;
			$resteX = $largeurPage - ($largeurCarte + self::interLigne + $margeDroite) - $placeLogoX;
			$positionLogoX = $resteX / 2;
			$positionLogoY = 0;
			$positionX += $largeurCarte + self::interLigne;
			$positionY = $margeHaut;
			$hauteurMaxLogo = 0;
			foreach ($this->urlLogos as $url) {
				$positionLogoY = $margeHaut;
				$imString = HTTPSender::getGetResponse($url, $this->httpProxy, '');
				if ($imString != '<VIDE/>') {
					$imgLogo = imagecreatefromstring($imString);
					$tmpfname = tempnam($this->tempDirectory, 'descartes_temp');
					imagepng($imgLogo, $tmpfname);
					$pdf->Image($tmpfname, $positionX + $positionLogoX, $positionLogoY,	imagesx($imgLogo) * 10 / $pourcentLogo * 2.54, imagesy($imgLogo) * 10 / $pourcentLogo * 2.54, 'PNG');
					unlink($tmpfname);
					$positionLogoX += imagesx($imgLogo) * 10 / $pourcentLogo * 2.54 + $interLogo;
					$hauteurLogo = imagesy($imgLogo) * 10 / $pourcentLogo * 2.54;
					if ($hauteurLogo > $hauteurMaxLogo) {
						$hauteurMaxLogo = $hauteurLogo;
					}
				}
			}
			$positionY += $hauteurMaxLogo;
			 
			$pdf->SetFont('Helvetica', '', 10);

			$positionY += self::interLigne;
			$texte = $this->auteurCarte;
			if ($texte != null) {
				$texte = 'Conception : ' . $texte;
				$pdf->setXY($positionX, $positionY);
				$pdf->Cell($pdf->GetStringWidth($texte), 1, utf8_decode($texte), 0, 1, 'L', 1);
				$positionY += self::interLigne;
			}
			$texte = $this->dateValiditeCarte;
			if ($texte != null) {
				$texte = 'Date de validit� : ' . $texte;
				$pdf->setXY($positionX, $positionY);
				$pdf->Cell($pdf->GetStringWidth($texte), 1, utf8_decode($texte), 0, 1, 'L', 1);
				$positionY += self::interLigne;
			}		
			$texte = 'Date d\'impression : ' . date('d-m-y');
			$pdf->setXY($positionX, $positionY);
			$pdf->Cell($pdf->GetStringWidth($texte), 1, utf8_decode($texte), 0, 1, 'L', 1);
			$positionY += self::interLigne * 2;

			$legendes = array();
			$legendes = $this->urlLegendes;
			for ($i = count($legendes); $i > 0; $i--) {
				$urlLegende = $legendes[$i - 1];
				$imString = HTTPSender::getGetResponse($urlLegende, $this->httpProxy,  '');
				if ($imString != '<VIDE/>') {
					$img = imagecreatefromstring($imString);
					$image = ImageCreateTrueColor(imagesx($img) * 10, imagesy($img) * 10);
					$blanc = ImageColorAllocate($image, 255, 255, 255);
					ImageFill($image, 0, 0, $blanc);
	
					imageinterlace($img, 0);
					$pourcentLegende = 100.0;		
					$legendWidth = imagesx($img) * 10 / $pourcentLegende * 2.54;
					$legendHeight = imagesy($img) * 10 / $pourcentLegende * 2.54;
					imagealphablending($image, 'FALSE');
					@imagecopyresized($image, $img, 0, 0, 0, 0, imagesx($img) * 10, imagesy($img) * 10, imagesx($img), imagesy($img));
	
					$tmpfname = tempnam($this->tempDirectory, 'descartes_temp');
					$pngfname = str_replace(".tmp", ".png", $tmpfname);
					unlink($tmpfname);
					imagepng($image, $pngfname);
					if ($legendHeight < $hauteurPage - $margeBas - $margeHaut) {
						if ($positionY > $hauteurPage + $margeHaut) {
							$positionX += $legendWidth;
							$positionY = $margeHaut + $legendHeight;
							if ($positionX + $legendWidth > $largeurPage - $margeDroite) {
								$pdf->AddPage();
								$positionX = $margeGauche;
								$positionY = $margeHaut + $legendHeight;
							}						
						}
						$pdf->Image($pngfname, $positionX, $positionY, $legendWidth, $legendHeight, 'PNG');
						$positionY += $legendHeight;
					}
					unlink($pngfname);
				}
			$this->avancement += $this->pas;
			}
			$this->avancement = 100;
			$this->pdfComplet = $pdf;
		}
		else {
			$this->erreur = "Le format ".$this->pageFormat." est inconnu.";
			$this->avancement = -1;
		}
	}	

	/**
	 * Methode: out()
	 * Envoie le PDF au client.
	 */
	function out() {
		$this->pdfComplet->Output($this->getTitreCarte().'.pdf', 'D');
	}
}
?>