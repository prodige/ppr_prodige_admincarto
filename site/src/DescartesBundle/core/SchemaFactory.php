<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



/**
 * Class: SchemaFactory
 * Fournisseur de sch�mas XSD pour validation de flux XML. 
 */
class SchemaFactory {

	/**
	 * Staticmethode: getRequest()
	 * Retourne le sch�ma pour les interrogations WMS et WFS.
	 * 
	 * Retour:
	 * {String} Sch�ma XSD correspondant :
	 * (start code)
	 * 	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	 * 
	 * 	<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">
	 * 		<xs:element name=\"Infos\">
	 * 			<xs:complexType>
	 * 				<xs:sequence>
	 * 					<xs:element name=\"Layer\" maxOccurs=\"unbounded\">
	 * 						<xs:complexType>
	 * 							<xs:sequence>
	 * 								<xs:element name=\"Name\">
	 * 									<xs:simpleType>
	 * 										<xs:restriction base=\"xs:string\">
	 * 											<xs:minLength value=\"1\"/>
	 * 										</xs:restriction>
	 * 									</xs:simpleType>
	 * 								</xs:element>
	 * 								<xs:element name=\"Request\" maxOccurs=\"unbounded\">
	 * 									<xs:simpleType>
	 * 										<xs:restriction base=\"xs:string\">
	 * 											<xs:minLength value=\"1\"/>
	 * 										</xs:restriction>
	 * 									</xs:simpleType>
	 * 								</xs:element>
	 * 								<xs:element name=\"LayerMinScale\" type=\"xs:float\" minOccurs=\"0\"/>
	 * 								<xs:element name=\"LayerMaxScale\" type=\"xs:float\" minOccurs=\"0\"/>
	 * 							</xs:sequence>
	 * 						</xs:complexType>
	 * 					</xs:element>
	 * 				</xs:sequence>
	 * 			</xs:complexType>
	 * 		</xs:element>
	 * 	</xs:schema>
	 * (end)
	 */
	public static function getRequest() {
		$schema = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				."<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">"
				."<xs:element name=\"Infos\"><xs:complexType><xs:sequence>"
				."<xs:element name=\"Layer\" maxOccurs=\"unbounded\"><xs:complexType><xs:sequence>"
				."<xs:element name=\"Name\"><xs:simpleType><xs:restriction base=\"xs:string\"><xs:minLength value=\"1\"/></xs:restriction></xs:simpleType></xs:element>"
				."<xs:element name=\"Request\"><xs:simpleType><xs:restriction base=\"xs:string\"><xs:minLength value=\"1\"/></xs:restriction></xs:simpleType></xs:element>"
        ."<xs:element name=\"Version\" type=\"xs:string\" minOccurs=\"0\"/>"
        ."<xs:element name=\"LayerMinScale\" type=\"xs:float\" minOccurs=\"0\"/>"
				."<xs:element name=\"LayerMaxScale\" type=\"xs:float\" minOccurs=\"0\"/>"
				."</xs:sequence></xs:complexType></xs:element>"
				."</xs:sequence></xs:complexType></xs:element>"
				."</xs:schema>";
		return $schema;
	}
	
	/**
	 * Staticmethode: getExport()
	 * Retourne le sch�ma pour les exportations PDF et PNG.
	 * 
	 * Retour:
	 * {String} Sch�ma XSD correspondant :
	 * (start code)
	 * 	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	 * 
	 * 	<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">
	 * 		<xs:element name=\"Export\">
	 * 			<xs:complexType>
	 * 				<xs:sequence>
	 * 					<xs:element name=\"Titre\"/>
	 * 					<xs:element name=\"Largeur\"/>
	 * 					<xs:element name=\"Hauteur\"/>
	 * 					<xs:element name=\"Couche\" maxOccurs=\"unbounded\">
	 * 						<xs:complexType>
	 * 							<xs:sequence>
	 * 								<xs:element name=\"Url\"/>
	 * 								<xs:element name=\"Opacite\"/>
	 * 							</xs:sequence>
	 * 						</xs:complexType>
	 * 					</xs:element>
	 * 					<xs:element name=\"UrlLegende\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>
	 * 					<xs:element name=\"UrlLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>
	 * 					<xs:element name=\"FichierLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>
	 * 					<xs:element name=\"Auteur\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"Copyright\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"Description\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"DateValidite\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"ApplicationInfos\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"Creator\" minOccurs=\"0\"/>
	 * 				</xs:sequence>
	 * 			</xs:complexType>
	 * 		</xs:element>
	 * 	</xs:schema>
	 * (end)
	 */
	public static function getExport() {
		$schema = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				."<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">"
				."<xs:element name=\"Export\"><xs:complexType><xs:sequence>"
				."<xs:element name=\"Titre\"/>"
				."<xs:element name=\"Largeur\"/>"
				."<xs:element name=\"Hauteur\"/>"
				."<xs:element name=\"Couche\" maxOccurs=\"unbounded\"><xs:complexType><xs:sequence>"
				."<xs:element name=\"Url\"/>"
				."<xs:element name=\"Opacite\"/>"
				."</xs:sequence></xs:complexType></xs:element>"
				."<xs:element name=\"UrlLegende\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>"
				."<xs:element name=\"UrlLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>"
				."<xs:element name=\"FichierLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>"
				."<xs:element name=\"Auteur\" minOccurs=\"0\"/>"
				."<xs:element name=\"Copyright\" minOccurs=\"0\"/>"
				."<xs:element name=\"Description\" minOccurs=\"0\"/>"
				."<xs:element name=\"DateValidite\" minOccurs=\"0\"/>"
				."<xs:element name=\"ApplicationInfos\" minOccurs=\"0\"/>"
				."<xs:element name=\"Creator\" minOccurs=\"0\"/>"
				."</xs:sequence></xs:complexType></xs:element>"
				."</xs:schema>";
		return $schema;
	}
	
	/**
	 * Staticmethode: getPrinterParams()
	 * Retourne le sch�ma pour les parametres d'impression PDF.
	 * 
	 * Retour:
	 * {String} Sch�ma XSD correspondant :
	 * (start code)
	 * 	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	 * 
	 * 	<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">
	 * 		<xs:element name=\"PrinterSetup\">
	 * 			<xs:complexType>
	 * 				<xs:sequence>
	 * 					<xs:element name=\"Format\"/>
	 * 					<xs:element name=\"Orientation\"/>
	 * 					<xs:element name=\"Margins\">
	 * 						<xs:complexType>
	 * 							<xs:sequence>
	 * 								<xs:element name=\"Units\"/>
	 * 								<xs:element name=\"Top\"/>
	 * 								<xs:element name=\"Bottom\"/>
	 * 								<xs:element name=\"Left\"/>
	 * 								<xs:element name=\"Right\"/>
	 * 							</xs:sequence>
	 * 						</xs:complexType>
	 * 					</xs:element>
	 * 				</xs:sequence>
	 * 			</xs:complexType>
	 * 		</xs:element>
	 * </xs:schema>
	 * (end)
	 */
	public static function getPrinterParams() {
		$schema = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				."<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">"
				."<xs:element name=\"PrinterSetup\"><xs:complexType><xs:sequence>"
				."<xs:element name=\"Format\"/>"
				."<xs:element name=\"Orientation\"/>"
				."<xs:element name=\"Margins\"><xs:complexType><xs:sequence>"
				."<xs:element name=\"Units\"/>"
				."<xs:element name=\"Top\"/>"
				."<xs:element name=\"Bottom\"/>"
				."<xs:element name=\"Left\"/>"
				."<xs:element name=\"Right\"/>"
				."</xs:sequence></xs:complexType></xs:element>"
				."</xs:sequence></xs:complexType></xs:element>"
				."</xs:schema>";
		return $schema;
	}
}
?>