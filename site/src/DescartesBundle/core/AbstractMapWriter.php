<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

/**
 * Class: AbstractMapWriter
 * Classe abstraite des threads de g�n�ration PNG et PDF.
 * 
 * Classe d�riv�es:
 * - <PDFMapWriter>
 * - <PNGMapWriter>
 */

class AbstractMapWriter {
	
	protected $avancement = 0;
	protected $erreur;
	protected $urlCartes;
	protected $urlLegendes;
	protected $fileLogos;
	protected $opacites;
	protected $urlLogos;
	
	protected $hauteurPixCarte = 400;
	protected $largeurPixCarte = 600;
	protected $titreCarte;
	protected $copyrightCarte;
	protected $auteurCarte;
	protected $dateValiditeCarte;
	protected $descriptionCarte;
	protected $creator;
	protected $applicationInfos;
	protected $affichageEchelle; // = "40km";
	protected $longueurEchelle; //  = 200;
	
	protected $pageFormat = 'A4';
	protected $pageOrientation = 'P';
	protected $pageMarginTop = 10.0;
	protected $pageMarginBottom = 10.0;
	protected $pageMarginLeft = 10.0;
	protected $pageMarginRight = 10.0;
	protected $pageUnits = 'mm';
	
	protected $pas = 100;
	
	public $httpProxy;
	
	protected $_logger;
	
	/**
	 * Constructeur: AbstractMapWriter(HTTPProxy)
	 * Constructeur d'instances
	 *
	 * Param�tres:
	 * $httpProxy - {<HTTPProxy>} Proxy.
	 */
	public function __construct($httpProxy) {
		$this->_logger = Logger::getLogger('AbstractMapWriter');
		
		$this->httpProxy = $httpProxy;
	}
	
	/**
	 * Methode: getAvancement()
	 * Fournit l'avancement du d�roulement du thread.
	 * 
	 * Retour: 
	 * {int} Avancement du d�roulement du thread (entre 1 et 100).
	 */
	public function getAvancement() {
		return $this->avancement;
	}
	
	/**
	 * Methode: getErreur()
	 * Fournit l'exception lev�e lors du d�roulement du thread.
	 * 
	 * Retour: 
	 * {Exception} Exception lev�e lors du d�roulement du thread.
	 */
	public function getErreur() {
		return $this->erreur;
	}
	
	/**
	 * Methode: setUrlCartes(String[])
	 * Renseigne les URLs des couches de la carte.
	 * 
	 * Param�tres: 
	 * $urlCartes - {String[]} URLs des couches de la carte, selon les sp�cifications WMS/GetMap
	 */
	public function setUrlCartes($urlCartes) {
		$this->urlCartes = $urlCartes;
		$this->createEchelle ();
	}
	
	/**
	 * Methode: setUrlLegendes(String[])
	 * Renseigne les URLs des l�gendes de la carte (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $urlLegendes - {String[]} URLs des l�gendes de la carte, selon les sp�cifications WMS/GetLegendGraphics ou en ressources statiques.
	 */
	public function setUrlLegendes($urlLegendes) {
		$this->urlLegendes = $urlLegendes;
	}
	
	/**
	 * Methode: setFileLogos(String[])
	 * Renseigne les noms des fichiers de logos � ins�rer (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $fileLogos - {String[]} Noms des fichiers de logos � ins�rer.
	 */
	public function setFileLogos($fileLogos) {
		$this->fileLogos = $fileLogos;
	}
	
	/**
	 * Methode: setUrlLogos(String[])
	 * Renseigne les URL(s) des logos � ins�rer (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $urlLogos - {String[]} URL(s) des logos � ins�rer.
	 */
	public function setUrlLogos($urlLogos) {
		$this->urlLogos = $urlLogos;
	}
	
	/**
	 * Methode: setOpacites(float[])
	 * Renseigne les opacit�s des couches.
	 * 
	 * Param�tres: 
	 * $opacites	- {float[]} Opacit�s des couches.
	 */
	public function setOpacites($opacites) {
		$this->opacites = $opacites;
	}
	
	/**
	 * Methode: setCreator(String)
	 * Renseigne le nom de l'application g�n�ratrice de la carte (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $creator - {String} Nom de l'application g�n�ratrice de la carte.
	 */
	public function setCreator($creator) {
		$this->creator = $creator;
	}
	
	/**
	 * Methode: setApplicationInfos(String)
	 * Renseigne les informations li�es � l'application g�n�ratrice (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $applicationInfos - {String} Informations li�es � l'application g�n�ratrice (ex : nom, version, etc.).
	 */
	public function setApplicationInfos($applicationInfos) {
		$this->applicationInfos = $applicationInfos;
	}
	
	/**
	 * Methode: setAuteurCarte(String)
	 * Renseigne l'auteur de la carte (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $auteurCarte - {String} Auteur de la carte.
	 */
	public function setAuteurCarte($auteurCarte) {
		$this->auteurCarte = $auteurCarte;
	}
	
	/**
	 * Methode: setCopyrightCarte(String)
	 * Renseigne le copyright associ� � la carte.
	 * 
	 * Param�tres: 
	 * $copyrightCarte - {String} Copyright associ� � la carte.
	 */
	public function setCopyrightCarte($copyrightCarte) {
		$this->copyrightCarte = $copyrightCarte;
	}
	
	/**
	 * Methode: setDateValiditeCarte(String)
	 * Renseigne la date de la validit� de la carte (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $dateValiditeCarte - {String} Date de la validit� de la carte.
	 */
	public function setDateValiditeCarte($dateValiditeCarte) {
		$this->dateValiditeCarte = $dateValiditeCarte;
	}
	
	/**
	 * Methode: setDescriptionCarte(String)
	 * Renseigne les �l�ments de description de la carte (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $descriptionCarte - {String} El�ments de description de la carte.
	 */
	public function setDescriptionCarte($descriptionCarte) {
		$this->descriptionCarte = $descriptionCarte;
	}
	
	/**
	 * Methode: setHauteurPixCarte(int)
	 * Renseigne la hauteur en pixels de l'image de la carte.
	 * 
	 * Param�tres: 
	 * $hauteurPixCarte - {int} Hauteur en pixels de l'image de la carte.
	 */
	public function setHauteurPixCarte($hauteurPixCarte) {
		$this->hauteurPixCarte = $hauteurPixCarte;
	}
	
	/**
	 * Methode: setLargeurPixCarte(int)
	 * Renseigne la largeur en pixels de l'image de la carte.
	 * 
	 * Param�tres: 
	 * $largeurPixCarte - {int} Largeur en pixels de l'image de la carte.
	 */
	public function setLargeurPixCarte($largeurPixCarte) {
		$this->largeurPixCarte = $largeurPixCarte;
	}
	
	/**
	 * Methode: getTitreCarte()
	 * Fournit le titre de la carte.
	 * 
	 * Retour: 
	 * {String} Titre de la carte.
	 */
	public function getTitreCarte() {
		return $this->titreCarte;
	}
	
	/**
	 * Methode: setTitreCarte(String)
	 * Renseigne le titre de la carte.
	 * 
	 * Param�tres: 
	 * $titreCarte - {String} Titre de la carte.
	 */
	public function setTitreCarte($titreCarte) {
		$this->titreCarte = $titreCarte;
	}
	
	/**
	 * Methode: setPageFormat(String)
	 * Renseigne le format de la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageFormat - {String} Format de la page (ex: A3, A4, A5, etc.).
	 */
	public function setPageFormat($pageFormat) {
		$this->pageFormat = $pageFormat;
	}
	
	/**
	 * Methode: setPageOrientation(String)
	 * Renseigne l'orientation de la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageOrientation - {String} Orientation de la page ('L' pour paysage, "P" pour portrait).
	 */
	public function setPageOrientation($pageOrientation) {
		$this->pageOrientation = $pageOrientation;
	}
	
	/**
	 * Methode: setPageMarginTop(float)
	 * Renseigne la marge 'haut' de la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageMarginTop - {float} Marge 'haut' de la page.
	 */
	public function setPageMarginTop($pageMarginTop) {
		$this->pageMarginTop = $pageMarginTop;
	}
	
	/**
	 * Methode: setPageMarginBottom(float)
	 * Renseigne la marge 'bas' de la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageMarginBottom - {float} Marge 'bas' de la page.
	 */
	public function setPageMarginBottom($pageMarginBottom) {
		$this->pageMarginBottom = $pageMarginBottom;
	}
	
	/**
	 * Methode: setPageMarginLeft(float)
	 * Renseigne la marge 'gauche' de la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageMarginLeft - {float} Marge 'gauche' de la page.
	 */
	public function setPageMarginLeft($pageMarginLeft) {
		$this->pageMarginLeft = $pageMarginLeft;
	}
	
	/**
	 * Methode: setPageMarginRight(float)
	 * Renseigne la marge 'droit' de la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageMarginRight - {float} Marge 'droit' de la page.
	 */
	public function setPageMarginRight($pageMarginRight) {
		$this->pageMarginRight = $pageMarginRight;
	}
	
	/**
	 * Methode: setPageUnits(String)
	 * Renseigne l'unit� de mesure pour la page (Seulement pour le PDF).
	 * 
	 * Param�tres: 
	 * $pageUnits - {String} Unit� de mesure pour la page.
	 */
	public function setPageUnits($pageUnits) {
		$this->pageUnits = $pageUnits;
	}
	
	/**
	 * Cr�e l'�chelle
	 */
	private function createEchelle() {
		if (count ( $this->urlCartes ) > 0) {
			$bboxPattern = '/BBOX=-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?/';
			$url = $this->urlCartes;
			if (preg_match ( $bboxPattern, $url [0], $matches ) !== 0) {
				$bboxSub = $matches [0];
				$bboxString = explode ( '=', $bboxSub );
				$bboxString1 = $bboxString [1];
				$bboxStringFloat = explode ( ',', $bboxString1 );
				$largeurCarteTerrain = $bboxStringFloat [2] - $bboxStringFloat [0];
				$longueurEchelleTerrainMax = $largeurCarteTerrain / 3.0;
				$truncLog = ( int ) log ( $longueurEchelleTerrainMax, 10 );
				if ($truncLog < 4) {
					$multi = ( int ) pow ( 10, $truncLog );
				} else {
					$multi = ( int ) pow ( 10, $truncLog - 1 );
				}
				$chiffresSignificatifs = ( int ) ($longueurEchelleTerrainMax / $multi);
				while ( ($chiffresSignificatifs > 4) && ($chiffresSignificatifs % 4 != 0) ) {
					$chiffresSignificatifs --;
				}
				$longueurEchelleTerrain = $chiffresSignificatifs * $multi;
				$this->longueurEchelle = $longueurEchelleTerrain * $this->largeurPixCarte / $largeurCarteTerrain;
				$this->affichageEchelle = ($longueurEchelleTerrain > 1000) ? ((( int ) ($longueurEchelleTerrain / 1000)) . ' km') : ($longueurEchelleTerrain + ' m');
			}
		}
	}
}
?>