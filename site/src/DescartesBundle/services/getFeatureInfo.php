<?php

/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



/**
 * Service: getFeatureInfo
 * Service d'acc�s au donn�es s�mantiques WMS.
 * 
 * Param�tres de configuration pris en compte:
 * global.hostMapping - Eventuel fichier de correspondance des Hosts
 * global.proxy.host - Host de l'�ventuel proxy technique
 * global.proxy.port - Port de l'�ventuel proxy technique
 * global.proxy.exceptions - Exceptions de l'�ventuel proxy technique
 * getFeatureInfo.transform - Eventuel fichier de transformation XSL-T formattant la r�ponse en flux HTML, rempla�ant la transformation par d�faut
 * getFeatureInfo.styles - Eventuel fichier de la feuille de style CSS pr�sentant la r�ponse en flux HTML, rempla�ant la feuille de style CSS par d�faut 
 * 
 * Classe responsable du service:
 * <OWSHelper>
 */
require_once '../core/OWSHelper.php';
require_once '../tools/RessourceBundle.php';
require_once '../tools/HTTPProxy.php';

RessourceBundle::loadBundle('../descartes');

$owsHelper = new OWSHelper(
	RessourceBundle::getProperty('global.hostMapping'),
	new HTTPProxy(
		RessourceBundle::getProperty('global.proxy.host'),
		RessourceBundle::getProperty('global.proxy.port'),
		RessourceBundle::getProperty('global.proxy.exceptions')
	),
	RessourceBundle::getProperty('getFeatureInfo.transform'),
	RessourceBundle::getProperty('getFeatureInfo.styles')
);

$owsHelper->internalXslFile = '../core/resources/getXmlFeatureInfo.xsl';
$owsHelper->execute();
?>