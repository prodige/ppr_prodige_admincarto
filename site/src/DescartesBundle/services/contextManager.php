<?php
/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/


/**
 * Service: contextManager
 * Service proposant la sauvegarde et la restauration de contextes de consultation.
 * 
 * Il dispose de plusieurs op�rations pour cr�er et g�rer les fichiers sauvegard�s.
 * 
 * Op�ration de sauvegarde:
 * Elle est appel�e par une m�thode *POST*.
 * 
 * :
 * Le param�tre *view* contient le nom de la vue personnalis�e choisie pour le contexte.
 * 
 * Le param�tre *context* contient la d�finition JSON du contexte de consultation.
 * 
 * :
 * La r�ponse fournie est une chaine repr�sentant un objet JSON identifiant le fichier g�n�r� :
 * :	{"contextFile": "<fichierDeContexte>"}
 * 
 * Si le serveur d'application est configur� avec une purge automatique, l'objet JSON comporte une propri�t� suppl�mentaire :
 * :	{"contextFile": "<fichierDeContexte>", "maxDate": "<Date d'expiration>"}
 * 
 * Op�ration de restauration:
 * Elle est appel�e par une m�thode *GET*.
 * 
 * :
 * Le nom du fichier � restaurer est simplement pass� en QueryString.
 * 
 * :
 * En retour, le service fournit le flux JSON d�finissant le contexte de visualisation.
 * 
 * Op�ration de suppression:
 * Elle est appel�e par une m�thode *GET*.
 * 
 * :
 * Le param�tre *delete* identifie le fichier � supprimer.
 * 
 * :
 * Aucun retour particulier n'est fournit.
 * 
 * Op�ration de v�rification de l'existence d'un ou plusieurs fichiers:
 * Elle est appel�e par une m�thode *GET*.
 * 
 * :
 * Le param�tre *check* identifie les fichiers � v�rifier, dont les noms sont s�par�s par des virgules dans la chaine du param�tre.
 * 
 * :
 * En retour, le service fournit une chaine (dont le s�parateur est |) indiquant une des valeurs suivantes pour chaque fichier test� :
 * 
 * T - si le fichier est disponible et ne porte aucune date d'expiration
 * une date - si le fichier est disponible jusqu'� cette date
 * F - si le fichier n'est plus disponible
 *  
 * Param�tres g�n�raux de configuration pris en compte:
 * contextdir - R�pertoire de stockage des fichiers de contexte
 * maxDays - Eventuelle p�riodicit� en jours de la purge automatique
 */
	require_once __DIR__ .'/../tools/RessourceBundle.php';
	RessourceBundle::loadBundle(__DIR__ .'/../descartes');
	$contextDir = RessourceBundle::getProperty('contextdir');
	$maxDays = RessourceBundle::getProperty('maxDays');
	
	if ( substr( $contextDir, strlen( $contextDir ) - strlen( "/" ) ) != "/" ) {
		$contextDir .= "/";
	}
	if (isset($_GET["delete"]) && $_GET["delete"] != "") {
		$fileName = urldecode($_GET["delete"]);
		$ok = unlink($contextDir.'/'.$fileName);
	} elseif (isset($_GET["check"]) && $_GET["check"] != "") {
		$check = "";
		$fileNames = $_GET["check"];
		
		$files;
		if (strpos($fileNames, ",") !== -1) {
			$files = explode ("," , $fileNames);
		} else {
			$files = array($fileNames);
		}
		for ($i=0, $len=count($files); $i<$len ; $i++) {
			if ($i != 0) {
				$check .= "|";
			}
			$file = $contextDir.'/'.urldecode($files[$i]);
			$checkValue = "";
			if (file_exists ($file)) {
				if ($maxDays === "") {
					$checkValue = "T";
				} else {
					$fileDate = filemtime($file);
					$date = new DateTime(date('Y/m/d', $fileDate));
					$date->modify('+'.$maxDays.' day');
					setlocale (LC_TIME, "fr");
					$checkValue = strftime("%#d %B %Y", strtotime($date->format('Y-m-d')));
				}
			} else {
				$checkValue = "F";
			}
			$check .= $checkValue;
		}
		echo $check;
	} elseif (isset($_SERVER["QUERY_STRING"]) && $_SERVER["QUERY_STRING"] != "") {
		$fileName = urldecode($_SERVER["QUERY_STRING"]);
		$contextFile = fopen($contextDir.'/'.$fileName, "r");
		$buffer = "";
		if ($contextFile) {
			while (($newLine = fgets($contextFile, 4096)) !== false) {
				$buffer .= $newLine;
			}
			fclose($contextFile);
		}
		echo $buffer;
	} elseif (isset($_POST["view"]) && isset($_POST["context"]) && $_POST["view"] != "" && $_POST["context"] != "") {
		$view = urldecode($_POST["view"]).md5(time().rand());
		$context = $_POST["context"];
		$context = str_replace("\\", "", $context);
		$tempFile = fopen($contextDir.'/'.$view, "wb");
		
		fwrite($tempFile, $context);
		$url = "http://" .$_SERVER["HTTP_HOST"]. ":" .$_SERVER["SERVER_PORT"] .$_SERVER["REQUEST_URI"]. "?" .$view;
		fclose($tempFile);
		header("Content-type: text/plain");
		$reponse = "{\"url\":\"". $url . "\", \"contextFile\":\"" .$view. "\"}";
		echo $reponse;
	}
?>	
