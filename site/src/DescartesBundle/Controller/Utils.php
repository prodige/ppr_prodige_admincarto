<?php 

#include_once __DIR__.'/app_conf.php';
/**********************************
 * Miscellaneous functions and 
 * constants used by several services
 **********************************/

define('MAX_DOUBLE', '1.7976931348623157e308');
define('MIN_DOUBLE', '4.9e-324');

// Query modes
define('QM_OGR_FILTER', 0);
define('QM_MAPSERVER_QUERY', 1);

// Encoding constants
// @since prodige4 loaded via configreader
//define("OWS_ENCODING", "UTF-8");
//define("MAPFILE_ENCODING", "ISO-8859-1");

// miscellaneous error codes used in CARMEN
define("CARMEN_ERROR", 256);
define("CARMEN_ERROR_SESSION_EXPIRED", 257);

define("CARMEN_MSG_SESSION_EXPIRED", "Votre session a expiré. La carte va être rechargée dans son environnement initial.");

function OWSEncode($str) {
 return $str;//iconv(MAPFILE_ENCODING, OWS_ENCODING, $str);
}

function MapfileEncode($str) {
 return $str;//iconv(OWS_ENCODING, MAPFILE_ENCODING, $str);
}

function NoEncode($str) {
 return $str;
}


function escape_quote($str) {
	return str_replace(array("\"","\'","\t","\r","\n"), array("\\\"","\\\'", '\t','\r','\n'), $str);
}


function quote($str) 
{
  return '"' . $str . '"';
}

function getName($field) {
  return quote($field["name"]);
}

function lastChar($str)
{
  $endCar = null;
  if (strlen($str)>0)
  $endCar = substr("$str", strlen("$str") - 1, 1);
  return $endCar;
}

function removeLastChar($str)
{
  return (strlen($str)>0) ?
  substr("$str", strlen("$str") - 1, 1) :
  $str;
}

function mergeRectObj($rect1, $rect2)
{
  $rectMerged = ms_newRectObj();
  $rectMerged->minx = min($rect1->minx, $rect2->minx);
  $rectMerged->miny = min($rect1->miny, $rect2->miny);
  $rectMerged->maxx = max($rect1->maxx, $rect2->maxx);
  $rectMerged->maxy = max($rect1->maxy, $rect2->maxy);
  return $rectMerged;
}

function rectObjToStr($rect)
{
  return '(' . $rect->minx . ',' .
  $rect->miny . ',' .
  $rect->maxx . ',' .
  $rect->maxy . ')';
}

function shapeStrToRectObj($shapeStr)
{
  $coords = explode(',', substr($shapeStr,1,strlen($shapeStr)-2));
  $rect = ms_newRectObj();
  if (count($coords)==4)
    $rect->setExtent(
     min($coords[0], $coords[2]), 
     min($coords[1], $coords[3]),
     max($coords[0], $coords[2]), 
     max($coords[1], $coords[3]));
  return $rect;
}

function datasetNameFromFilename($filename) {
  $basen = basename($filename);
  // removing suffixe if present
  $posSuf = strpos($basen,'.');
  if (!($posSuf===FALSE))
    $basen = substr($basen,0, $posSuf);

  //  echo $basen;
  return $basen;
}

// add a '.shp' suffix to filename that have not
function forceShapefileExtension($filename) {
  $posSuf = strpos($filename,'.');
  if (($posSuf===FALSE) || $posSuf < strlen($filename)-4)
    $filename = $filename . '.shp';
  return $filename;
}

/**
 * @abstract : replace parts of $template wich are in the form 
 *  '<fieldName>' by the corresponding fieldValue
 * @param string $template :  the string to compute
 * @param string array $featValues : the values array
 * @param string array $fields : the field names array
 * @return boolean $URLEncode : if true, the replacement string is 
 *  urlencoded instead of owsencoded. urlencode replaces non aplpha 
 *  numerical characters by '%xx' values, whereas owsencode make a 
 *  coding translatation, e.g. latin1 -> utf8
 */ 
function fill_template($template, $featValues, $fields, $URLEncode=false) {
  for($i=0; $i<count($fields); $i++) {
    $template = str_replace(
      '<' . $fields[$i] . '>', 
      $URLEncode ? urlencode($featValues[$fields[$i]]) :
      OWSEncode($featValues[$fields[$i]]),
      $template);
  }
  return $template;
}

// CARMEN AJAX error handling function
function handle_error($errno, $errmsg) {
  if (error_reporting()>0) {
    if (strstr($errmsg, 'CARMEN_ERROR')!=FALSE) {
      $info = preg_split('/\|/', $errmsg);
      $errno = intval($info[0]);
      $errmsg = $info[1];      
    }
    $debug = debug_backtrace();
    $callee = next($debug);
    
    
    @$result = array(
      'success' => false,
      'errmsg' => $errmsg . " file : " . $callee['file'] . ", line : " . $callee['line'],
      'failureType' => $errno  
    );
     
    @header("Content-type:text/html");
    //@header("HTTP/1.0 400 Error");
    echo json_encode($result);
    die();
  }
}

/**
 * Force a string into alphanumeric
 *
 * @param string $text
 * @param string $from_enc
 * @return string
 */
function to7bit($text,$from_enc) {
  $text = mb_convert_encoding($text,'HTML-ENTITIES',$from_enc);
  
  //delete accents
  $out_text = preg_replace( array('/ß/','/&(..)lig;/', '/&([aouAOU])uml;/','/&(.)[^;]*;/'),
   array('ss',"$1","$1".'e',"$1"),
   $text);
  //$out_text = str_replace(" ", "_", $out_text);
  
  // removing non alpha-numeric chars
  $out_text = preg_replace("/[^a-z0-9./]/i",'_',$out_text);
  //\)|\(|\ |\+|\;|\:|\*|\?|\"|\'
  $out_text = preg_replace("/[\)\(\+;:\*\?]/i",'_',$out_text);
  $out_text = str_replace(" ", "_", $out_text);
  // returning cleaned string
  return $out_text;
}


function getEPSGProjectionFromMsString($msString) {
	$res = -1;
	$found = preg_match("/epsg:([0-9]+)/", $msString, $matched);
	if ($found)
		$res = intval($matched[1]);
	return $res;
}

/**
 * @brief vérifie le nommage du fichier :
 *        - n'accepte que les caractères [a-z][A-Z][0-9]_.-%
 * @param strFileName nom du fichier à traiter
 * @param bToLower    force le nom en minuscule
 * @returns Retourne le nom du fichier correcte
 */
function verifyFileName($strFileName, $bToLower=false)
{
	// passage en minuscule
	$strTmp = ( $bToLower ? strtolower($strFileName) : $strFileName );
  $strTmp =utf8_decode($strTmp);
	// remplace les caractères accentués courant par leur équivalent non accentué
	// remplace l'espace par souligné
  //mb_regex_encoding('UTF-8');
	$tabChar = array(utf8_decode(" -éèêëäàâüùûîïôöç"), "__eeeeaaauuuiiooc");
	for($i=0; $i<strlen($tabChar[0]); $i++) {
    $strTmp = str_replace(substr($tabChar[0], $i, 1), substr($tabChar[1], $i, 1), $strTmp);
    //echo substr($tabChar[0], $i, 1). " ". substr($tabChar[1], $i, 1). "         ". $strTmp."\n";
	}

	// supprime tous les caractères n'étant pas : lettre, chiffre, point, tiré et souligné et %
	$strTmp = mb_ereg_replace("([^_a-zA-Z0-9\%\-\.])", "", $strTmp);
	$strTmp = mb_ereg_replace("\\\\", "", $strTmp);
	return $strTmp;
}



/**
 * Standard deviation from array values
 * @param $a array
 * @return unknown_type
 */
function standard_deviation_population ($a)
{
  //variable and initializations
  $the_standard_deviation = 0.0;
  $the_variance = 0.0;
  $the_mean = 0.0;
  $the_array_sum = array_sum($a); //sum the elements
  $number_elements = count($a); //count the number of elements

  //calculate the mean
  $the_mean = $the_array_sum / $number_elements;

  //calculate the variance
  for ($i = 0; $i < $number_elements; $i++)
  {
    //sum the array
    $the_variance = $the_variance + ($a[$i] - $the_mean) * ($a[$i] - $the_mean);
  }

  $the_variance = $the_variance / $number_elements;

  //calculate the standard deviation
  $the_standard_deviation = pow( $the_variance, 0.5);

  //return the variance
  return $the_standard_deviation;
}
/**
 * Variance from array values
 * @param $a array
 * @return unknown_type
 */
function variance_sample ($a)
{
  //variable and initializations
  $the_variance = 0.0;
  $the_mean = 0.0;
  $the_array_sum = array_sum($a); //sum the elements
  $number_elements = count($a); //count the number of elements

  //calculate the mean
  $the_mean = $the_array_sum / $number_elements;

  //calculate the variance
  for ($i = 0; $i < $number_elements; $i++)
  {
    //sum the array
    $the_variance = $the_variance + ($a[$i] - $the_mean) * ($a[$i] - $the_mean);
  }

  $the_variance = $the_variance / ($number_elements - 1.0);

  //return the variance
  return $the_variance;
}