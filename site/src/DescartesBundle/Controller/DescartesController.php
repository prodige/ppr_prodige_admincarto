<?php

namespace Carmen\DescartesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

require_once __DIR__ . '/../core/OWSHelper.php';
require_once __DIR__ . '/../tools/RessourceBundle.php';
require_once __DIR__ . '/../tools/HTTPProxy.php';

// use FOS\RestBundle\Controller\Annotations\Delete;
// use FOS\RestBundle\Controller\Annotations\Get;
// use FOS\RestBundle\Controller\Annotations\Route;
// use FOS\RestBundle\Controller\Annotations\Head;
// use FOS\RestBundle\Controller\Annotations\Link;
// use FOS\RestBundle\Controller\Annotations\Patch;
// use FOS\RestBundle\Controller\Annotations\Post;
// use FOS\RestBundle\Controller\Annotations\Put;
// use FOS\RestBundle\Controller\Annotations\Unlink;
// use FOS\RestBundle\Request\ParamFetcher;
// use FOS\RestBundle\Controller\Annotations\QueryParam;   // GET params
// use FOS\RestBundle\Controller\Annotations\RequestParam; // POST params

/**
 * Carmen Descartes Controller.
 * 
 * @author alkante <support@alkante.com>
 * 
 * @Route("/descartes")
 */
class DescartesController extends AbstractController
{
   /**
     * Wrap descartes featureinfo response
     *
     * @Route("/getFeatureInfo", name="carmen_descartes_getfeatureinfo", options={"expose"=true}, methods={"POST"})
     *
     * @return JsonResponse Json array.
     */
    public function getFeatureInfoAction(Request $request)
    {
      error_reporting(E_ALL^E_NOTICE^E_STRICT^E_WARNING);
        \RessourceBundle::loadBundle(__DIR__.'/../descartes');

        $owsHelper = new \OWSHelper(
          \RessourceBundle::getProperty('global.hostMapping'),
          new \HTTPProxy(
            \RessourceBundle::getProperty('global.proxy.host'),
            \RessourceBundle::getProperty('global.proxy.port'),
            \RessourceBundle::getProperty('global.proxy.exceptions')
          ),
          \RessourceBundle::getProperty('getFeatureInfo.transform'),
          \RessourceBundle::getProperty('getFeatureInfo.styles')
        );

        $owsHelper->internalXslFile = '../core/resources/getXmlFeatureInfo.xsl';
        ob_start();
        $owsHelper->execute();
        $values = ob_get_clean();
        
        
        //$resultFormat = $_POST ["format"];
        //$values = array("msg" => $resultFormat);
        
        return new Response($values);
    }
    
    /**
     * @Route("/{accountId}", name="carmen_descartes_index")
     */
    public function indexAction(Request $request, $accountId) {
        $values = array("msg" => "tests");
        
        return new JsonResponse($values);
    }
}
