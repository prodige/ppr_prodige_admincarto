<?php


/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



/**
 * Class: XmlTools
 * Classe utilitaire de traitements XML.
 */
class XmlTools
{

	/**
	 * Staticmethode: validateXmlString(String, String)
	 * Retourne un arbre DOM � partir d'un flux XML valid� selon un sch�ma XSD.
	 * 
	 * Param�tres:
	 * $fluxXML - {String} Flux XML � valider
	 * $xsdString - {String} Nom du fichier d�crivant le sch�ma XSD
	 * 
	 * Retour:
	 * {DOMDocument} Arbre DOM valid�
	 * 
	 * Lance:
	 * {SchemaValidationException} - Si le flux n'est pas valide
	 */
	public static function validateXmlString($fluxXML, $xsdString)
	{
		$doc = DOMDocument::loadXML('<?xml version="1.0" encoding="iso-8859-1"?> ' . $fluxXML);
		$isValid = $doc->schemaValidateSource($xsdString);
		if (!$isValid) {
			throw new SchemaValidationException();
		}
		return $doc;
	}

	/**
	 * Staticmethode: validateXmlFile(String, String)
	 * Retourne un arbre DOM � partir d'un fichier XML valid� selon un sch�ma XSD.
	 * 
	 * Param�tres:
	 * $fluxXML - {String} Nom du fichier XML � valider
	 * $xsdString - {String} Nom du fichier d�crivant le sch�ma XSD
	 * 
	 * Retour:
	 * {DOMDocument} Arbre DOM valid�
	 * 
	 * Lance:
	 * {SchemaValidationException} - Si le fichier n'est pas valide
	 */
	public static function validateXmlFile($fileXML, $xsdString)
	{
		$doc = DOMDocument::load($fileXML);
		$isValid = $doc->schemaValidateSource($xsdString);
		if (!$isValid) {
			throw new SchemaValidationException();
		}
		return $doc;
	}

	/**
	 * Staticmethode: executeXSL(String, String, Array)
	 * Ex�cution d'une transformation XSL-T � partir d'un fichier.
	 * 
	 * Param�tres:
	 * $xmlFlux - {String} Flux XML source
	 * $xslStylesheet - {String} Nom du fichier XSL-T � utiliser 
	 * $paramsKVP - {Array} Key-Name des param�tres de la transformation
	 * 
	 * Retour:
	 * {String} Flux XML r�sultat
	 */

	public static function executeXSL($xmlFlux, $xslStylesheet, $paramsKVP)
	{
		$result = '';
		$xslt = new xsltProcessor();
		$xslt->importStyleSheet(DOMDocument::load($xslStylesheet));
		foreach ($paramsKVP as $k => $v) {
			$xslt->setParameter('', $k, $v);
		}
		$xmlDOM = new DOMDocument();
		if ($xmlDOM->loadXML($xmlFlux)) {
			$result = $xslt->transformToXML($xmlDOM);
		}
		return $result;
	}

	/**
	 * Staticmethode: executeInternalXSL(String, String, Array)
	 * Ex�cution d'une transformation XSL-T � partir d'un flux.
	 * 
	 * Param�tres:
	 * $xmlFlux - {String} Flux XML source
	 * $xslStylesheet - {String} Flux XSL-T � utiliser 
	 * $paramsKVP - {Array} Key-Name des param�tres de la transformation
	 * 
	 * Retour:
	 * {String} Flux XML r�sultat
	 */
	public static function executeInternalXSL($xmlFlux, $xslFlux, $paramsKVP)
	{
		$result = '';
		$xslt = new xsltProcessor();
		$xslDOM = new DOMDocument();
		$xslDOM->loadXML($xslFlux);
		$xslt->importStyleSheet($xslDOM);
		foreach ($paramsKVP as $k => $v) {
			$xslt->setParameter('', $k, $v);
		}
		$xmlDOM = new DOMDocument();
		if ($xmlDOM->loadXML($xmlFlux)) {
			$result = $xslt->transformToXML($xmlDOM);
		}
		return $result;
	}

	/**
	 * Retourne une liste de noeuds correspondant � une requete Xpath
	 * @param DomDocument $document document � interroger
	 * @param String $xPathQuery Requete Xpath
	 * @return $nodeList Liste des noeuds s�lectionn�s
	 */
	public static function getNodeList($document, $xPathQuery)
	{
		$xpath = self::getDOMXpath($document);
		$nodeList = $xpath->query($xPathQuery);
		return $nodeList;
	}

	/**
	 * Retrouve un param�tre en partant d'un �l�ment et d'une requete XPath
	 * @param $element El�ment
	 * @param $xPathQuery Requete XPath
	 * @return $params Param�tre
	 */
	public static function getStringParam($element, $xPathQuery)
	{
		return urldecode(self::getNonStringParam($element, $xPathQuery));
	}

	/**
	 * Retrouve un param�tre en partant d'un �l�ment et d'une requete XPath
	 * @param $element El�ment
	 * @param $xPathQuery Requete XPath
	 * @return $params Param�tre
	 * @throw XPathException Exception si aucun param�tre n'est trouv�
	 */
	public static function getNonStringParam($element, $xPathQuery)
	{
		$item = self::getDOMXPath($element)->query($xPathQuery)->item(0);
		if ($item == null) {
			throw new XPathException();
		} else {
			$param = $item->firstChild->nodeValue;
		}
		return $param;
	}

	/**
	 * Retrouve un ensemble d'url sous forme de param�tres en partant d'un �l�ment et d'une requete XPath et le retourne dans un tableau
	 * @param $element El�ment
	 * @param $xPathQuery Requete XPath
	 * @return Array $params Tableau des param�tres
	 */
	public static function getUrlArrayParam($element, $xPathQuery)
	{
		$params = array();
		$paramNodeList = self::getDOMXPath($element)->query($xPathQuery);
		if ($paramNodeList->length != 0) {
			foreach ($paramNodeList as $paramNode) {
				$param = $paramNode->firstChild->nodeValue;
				$param = str_replace("+", "%2B", $param);
				$param = str_replace("&amp;", "&", $param);
				$params[] = $param;
			}
		}
		return $params;
	}

	/**
	 * Retrouve un ensemble de chaines sous forme de param�tres en partant d'un �l�ment et d'une requete XPath et le retourne dans un tableau
	 * @param $element El�ment
	 * @param $xPathQuery Requete XPath
	 * @param $prefix Pr�fixe additionel optionel. Vide par d�faut
	 * @return Array $params Tableau des param�tres
	 */
	public static function getStringArrayParam($element, $xPathQuery, $prefix = '')
	{
		$params = array();
		$paramNodeList = self::getDOMXPath($element)->query($xPathQuery);
		if ($paramNodeList->length != 0) {
			foreach ($paramNodeList as $paramNode) {
				$param = $paramNode->firstChild->nodeValue;
				$param = str_replace("+", "%2B", $param);
				$param = str_replace("&amp;", "&", $param);
				$params[] = $prefix . $param;
			}
		}
		return $params;
	}

	/**
	 * Retrouve un ensemble de r�els sous forme de paramètres en partant d'un �l�ment et d'une requete XPath et le retourne dans un tableau
	 * @param $element Elément
	 * @param $xPathQuery Requete XPath
	 * @return Array $params Tableau des paramètres
	 */
	public static function getFloatArrayParam($element, $xPathQuery)
	{
		$params = array();
		$paramNodeList = self::getDOMXPath($element)->query($xPathQuery);
		if ($paramNodeList->length != 0) {
			foreach ($paramNodeList as $paramNode) {
				$param = $paramNode->firstChild->nodeValue;
				$params[] = $param;
			}
		}
		return $params;
	}

	/**
	 * Retourne le DOMXpath d'un �l�ment noeud ou document
	 * @param $element El�ment
	 * @return DOMXpath 
	 */
	private static function getDOMXPath($element)
	{
		$document = null;
		if (get_class($element) == 'DOMElement') {
			$document = new DOMDocument();
			$document->appendChild($document->importNode($element, true));
		} else {
			$document = $element;
		}
		return new DOMXPath($document);
	}
}

/*
 * Classes d'exceptions
 */

/**
 * Class: SchemaValidationException
 * Exception de flux non conforme � un sch�ma.
 * 
 * H�rite de:
 * - Exception
 */
class SchemaValidationException extends Exception
{
	/**
	 * Constructeur: SchemaValidationException(Integer)
	 * Constructeur d'instance.
	 * 
	 * Param�tres:
	 * $code - {Integer} Code de l'exception valant 0 par d�faut
	 */
	public function __construct($code = 0)
	{
		parent::__construct('Flux XML non conforme au sch�ma XSD', $code);
	}
}

/**
 * Class: XPathException
 * Exception d'�l�ment inexistant.
 * 
 * H�rite de:
 * - Exception
 */
class XPathException extends Exception
{

	/**
	 * Constructeur: XPathException(Integer)
	 * Constructeur d'instance.
	 * 
	 * Param�tres:
	 * $code - {Integer} Code de l'exception valant 0 par d�faut
	 */
	public function __construct($code = 0)
	{
		parent::__construct('Element inexistant', $code);
	}
}

/**
 * Class: DomLoaderException
 * Exception d'impossibilit� de charger un flux.
 * 
 * H�rite de:
 * - Exception
 */
class DomLoaderException extends Exception
{

	/**
	 * Constructeur: DomLoaderException(Integer)
	 * Constructeur d'instance.
	 * 
	 * Param�tres:
	 * $code - {Integer} Code de l'exception valant 0 par d�faut
	 */
	public function __construct($code = 0)
	{
		parent::__construct('Impossible de charger le flux XML', $code);
	}
}
