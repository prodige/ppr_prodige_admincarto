<?php


/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/



/**
 * Class: HTTPProxy
 * Classe "bean" repr�sentant les proxies n�cessaires selon les contraintes d'h�bergement.
 */
class HTTPProxy {
	
	private $host = "";
	private $port = 0;
	private $urlExceptions = "";

	/**
	 * Constructeur: HTTPProxy(String, String, String)
	 * Constructeur selon le Host, le Port et les exceptions d'URL du proxy.
	 * 
	 * Param�tres:
	 * $host - {String} Host du proxy
	 * $port - {String} Port du proxy
	 * $urlExceptions - {String} Exceptions d'URL
	 */
	/**
	 * Constructeur
	 * @param String $host Url du proxy
	 * @param String $port Port tcp du proxy
	 * @param Array $urlExceptions Urls ne n�cessitant pas le passage par le proxy
	 */	
	public function __construct($host, $port, $urlExceptions) {
		$this->host = $host;
		$this->port = $port;
		$this->urlExceptions = $urlExceptions;
	}

	/**
	 * Methode: getHost()
	 * Fournit le host du proxy
	 * 
	 * Retour:
	 * {String} Le host du proxy
	 */
	public function getHost() {
		return $this->host;
	}

	/**
	 * Methode: getPort()
	 * Fournit le port du proxy
	 * 
	 * Retour:
	 * {int} Le port du proxy
	 */
	public function getPort() {
		return $this->port;
	}

	/**
	 * Methode: getUrlExceptions()
	 * Fournit les exceptions d'URL du proxy
	 * 
	 * Retour:
	 * {String} Les exceptions d'URL du proxy
	 */
	public function getUrlExceptions() {
		return $this->urlExceptions;
	}

}
?>