<?php

/*
Copyright MEDDTL
Contributeurs :
     Ga�lle Barris
     David Berger
     Thibaud Bioulac
     Christophe Bocquet
     Denis Chabrier
     Marc Regnault

Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
termes.
*/

#require_once __DIR__ .'/../../log4php/ConfigureLogger.php';

require_once (__DIR__ .'/../tools/HTTPSenderException.php');

/**
 * Class: HTTPSender
 * Classe utilitaire d'ex�cution de requ�tes HTTP.
 */
class HTTPSender {
	
	/**
	 * Staticmethode: getGetResponse(String, HTTPProxy, String)
	 * Retourne la r�ponse � une requ�te GET.
	 * 
	 * Param�tres:
	 * $globalURL - {String} URL du serveur HTTP
	 * $httpProxy - {<HTTPProxy>} Proxy
	 * $request - {String} Requ�te HTTP
	 * 
	 * Retour:
	 * {String} R�ponse � la requ�te
	 * 
	 * Lance:
	 * {<HTTPSenderException>} - En cas d'impossibilit� de communiquer avec le serveur contact�.
	 */
	public static function getGetResponse($globalURL, $httpProxy, $request = '') {
		#$_logger = Logger::getLogger('HTTPSender');
		
		try {
			$httpRequest = curl_init ();
			if (HTTPSender::hasProxy ( $globalURL, $httpProxy )) {
				$httpOptions = array (CURLOPT_PROXY => $httpProxy->getHost (), CURLOPT_PROXYPORT => $httpProxy->getport (), CURLOPT_HTTPPROXYTUNNEL => false );
				curl_setopt_array ( $httpRequest, $httpOptions );
			}
			
			curl_setopt ( $httpRequest, CURLOPT_HTTPGET, true );
			curl_setopt ( $httpRequest, CURLOPT_URL, $globalURL . $request );
			curl_setopt ( $httpRequest, CURLOPT_RETURNTRANSFER, true );
			$reponse = curl_exec ( $httpRequest );
			curl_close ( $httpRequest );
		} catch ( Exception $ex ) {
			throw new HTTPSenderException ( "impossible de communiquer avec le serveur", $ex );
			curl_close ( $httpRequest );
		}
		return $reponse;
	}
	
	/**
	 * Staticmethode: getPostResponse(String, HTTPProxy, String)
	 * Retourne la r�ponse � une requ�te POST.
	 * 
	 * Param�tres:
	 * $globalURL - {String} URL du serveur HTTP
	 * $httpProxy - {<HTTPProxy>} Proxy
	 * $request - {String} Requ�te HTTP
	 * 
	 * Retour:
	 * {String} R�ponse � la requ�te
	 * 
	 * Lance:
	 * <HTTPSenderException> - En cas d'impossibilit� de communiquer avec le serveur contact�.
	 */
	public static function getPostResponse($globalURL, $httpProxy, $request = '') {
		$httpRequest = curl_init ();
		if (HTTPSender::hasProxy ( $globalURL, $httpProxy )) {
			$httpOptions = array (CURLOPT_PROXY => $httpProxy->getHost (), CURLOPT_PROXYPORT => $httpProxy->getport (), CURLOPT_HTTPPROXYTUNNEL => false );
			curl_setopt_array ( $httpRequest, $httpOptions );
		}
		
		curl_setopt ( $httpRequest, CURLOPT_POST, true );
		curl_setopt ( $httpRequest, CURLOPT_URL, $globalURL );
		curl_setopt ( $httpRequest, CURLOPT_POSTFIELDS, $request );
		curl_setopt ( $httpRequest, CURLOPT_RETURNTRANSFER, true );
		try {
			$reponse = curl_exec ( $httpRequest );
			echo curl_getinfo($httpRequest, CURLINFO_CONTENT_TYPE);
			exit();
			curl_close ( $httpRequest );
		} catch ( Exception $ex ) {
			throw new HTTPSenderException ( "impossible de communiquer avec le serveur", $ex );
			curl_close ( $httpRequest );
		}
		return $reponse;
	}
	
	/**
	 * V�rification de la n�cessit� de passer par le proxy HTTP
	 * @param String $globalURL Url cible
	 * @param HttpProxy $httpProxy Instance du proxy � utiliser
	 * @return Boolean $proxy Si true alors le passage par le proxy est n�cessaire
	 */
	private static function hasProxy($globalURL, $httpProxy) {
		$proxy = false;
		if ($httpProxy != null && $httpProxy->getHost () != '' && $httpProxy->getPort () != 0) {
			$proxy = true;
			if ($httpProxy->getUrlExceptions () != '') {
				$host = parse_url ( $globalURL, PHP_URL_HOST );
				$noProxyURLs = explode ( ';', $httpProxy->getUrlExceptions () );
				foreach ( $noProxyURLs as $noProxyURL ) {
					$endPos = strrpos ( $host, $noProxyURL );
					if ((strpos ( $host, $noProxyURL ) === 0) || (($endPos !== false) && ($endPos == (strlen ( $host ) - strlen ( $noProxyURL ))))) {
						$proxy = false;
						break;
					}
				}
			}
		}
		return $proxy;
	}
	
	/**
	 * Supprime la d�claration de la DTD d'un flux XML
	 * @param String $content Flux � traiter
	 * @return String $buf Flux sans d�claration de DTD
	 */
	private function unDeclareDTD($content) {
		$buf = "";
		$ouvrants = 1;
		$closed = false;
		
		$beginDTD = strpos ( $content, "<!DOCTYPE" );
		if ($beginDTD !== false) {
			if ($beginDTD > 0) {
				$buf .= substr ( $content, 0, $beginDTD );
			}
			$content = substr ( $content, $beginDTD );
			$index = 1;
			while ( ! $closed ) {
				$index ++;
				$car = substr ( $content, $index, 1 );
				if ($car == '<') {
					$ouvrants ++;
				}
				if ($car == '>') {
					$ouvrants --;
				}
				$closed = ($ouvrants === 0);
			}
			$buf .= substr ( $content, $index + 1 );
		} else {
			$buf .= $content;
		}
		return $buf;
	}
}
?>