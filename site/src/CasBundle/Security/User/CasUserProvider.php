<?php

namespace Carmen\CasBundle\Security\User;

use Carmen\ApiBundle\Entity\Users;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author alkante <support@alkante.com>
 */
class CasUserProvider implements UserProviderInterface {


    public function __construct(EntityManagerInterface $em, RequestStack $requestStack, ContainerInterface $container)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->container = $container;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('CarmenApiBundle:Users')->findOneBy(array('userEmail'=>$username));
        //force user to admincli for basicauth authentification
        if($user && $username===$this->container->getParameter('phpcli_default_login')){
            $user->setRoles(array('ROLE_USER'));
            $user->setUsername($username);
            $user->setUserPassword($this->container->getParameter('phpcli_default_password'));
            return $user;
        }
        return null;
    }


    
    public function refreshUser(UserInterface $user)
    {
        // if user has been reloaded from the session, just return it...
        return $user;
    }

    public function supportsClass($class)
    {
        return $class === 'Carmen\ApiBundle\Entity\Users';
    }

}
