<?php

namespace Carmen\CasBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Custom User used for CAS authentication.
 * 
 * @deprecated use CarmenApiBundle:Users instead
 *
 * @author alkante <support@alkante.com>
 */
class User implements UserInterface
{
    private $id;
    private $username;
    private $password;
    private $roles;

    public function __construct($id, $username, $password, array $roles)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->roles = $roles;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->username;
    }
  
    public function getId()
    {
        return $this->id; 
    }

    public function eraseCredentials()
    {
    }

    public function equals(UserInterface $user)
    {
        if (!$user instanceof ApiUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }
}
