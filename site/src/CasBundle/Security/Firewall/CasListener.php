<?php

namespace Carmen\CasBundle\Security\Firewall;

use Carmen\CasBundle\Security\Authentication\Token\CasUserToken;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

/**
 * CAS Authentication listener.
 *
 * @author alkante <support@alkante.com>
 */
class CasListener
{
    protected $tokenStorage;
    protected $authenticationManager;
    protected $container;


    /**
     * Builds a new CasListener.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param ContainerInterface $container
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, ContainerInterface $container)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->container = $container;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Http\Firewall\ListenerInterface::handle()
     */
    //    public function handle(GetResponseEvent $event)
    public function __invoke(RequestEvent $event)
    {
        $status = Response::HTTP_FORBIDDEN;
        $message = $exception = null;
        try {
            $authToken = $this->authenticationManager->authenticate(new CasUserToken());
            $this->tokenStorage->setToken($authToken);

            return;
        } catch (AuthenticationException $failed) {
            // ... you might log something here

            // To deny the authentication clear the token. This will redirect to the login page.
            // Make sure to only clear your token, not those of other authentication listeners.
            // $token = $this->tokenStorage->getToken();
            // if ($token instanceof WsseUserToken && $this->providerKey === $token->getProviderKey()) {
            //     $this->tokenStorage->setToken(null);
            // }
            // return;
            $status = Response::HTTP_UNAUTHORIZED;
            $exception = $failed;
            $message = $failed->getMessage();
        }
        // try to wrap failure response in a nice error page
        $exceptionController = $this->container->get('prodige.exception_controller'); // twig.controller.exception
        if (method_exists($exceptionController, 'showAction')) {
            $exception = FlattenException::create($exception, $status);
            $response = $exceptionController->showAction($this->container->get('request_stack')->getCurrentRequest(), $exception);
        } else {
            // By default deny authorization
            $response = new Response($message);
        }

        $response->setStatusCode($status);
        $event->setResponse($response);
    }
}
