<?php

namespace Carmen\CasBundle\Security\Authentication\Provider;

use Carmen\ApiBundle\Entity\UserAudit;
use Carmen\ApiBundle\Entity\Users;
use Carmen\CasBundle\Security\Authentication\Token\CasUserToken;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestMatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

//use Alk\Common\CasBundle\Event\AuthenticateUserEvent;

/**
 * CAS Authentication provider.
 *
 * @author alkante <support@alkante.com>
 */
class CasProvider implements AuthenticationProviderInterface, ContainerAwareInterface
{
    private static $_container;

    private $container;
    private $userProvider;
    private $cacheDir;
    private $requestStack;
    private $requestMatcher;

    private $cas_host;
    private $cas_context;
    private $cas_port;
    private $cas_server_ca_cert_path;
    private $cas_server_validation;
    private $cas_force_auth;
    private $cas_curl_options;

    private $cas_debug;
    private $cas_version;
    private $cas_restrict_ip;

    /**
     * @param UserProviderInterface $userProvider
     * @param unknown $cacheDir
     * @param string $cas_host Full Hostname of your CAS Server.
     * @param string $cas_context Context of the CAS Server.
     * @param string $cas_port Port of your CAS server. Normally for a https server it's 443.
     * @param string $cas_server_ca_cert_path Path to the ca chain that issued the cas server certificate.
     * @param string $cas_force_auth Force authentication if true (redirect), or check authentication if false (no redirect)
     */
    public function __construct(
        UserProviderInterface $userProvider,
                              $cacheDir,
        RequestStack          $requestStack,
        /*3*/                 $cas_host,
                              $cas_context,
        /*5*/                 $cas_port,
                              $cas_server_ca_cert_path,
        /*7*/                 $cas_server_validation,
                              $patterns_force_auth,
        /*9*/                 $patterns_not_force_auth,
                              $cas_curl_options,
        /*11*/                $cas_debug,
                              $cas_restrict_ip
    )
    {
        $this->userProvider = $userProvider;
        $this->cacheDir = $cacheDir;
        $this->requestStack = $requestStack;

        $this->cas_host = $cas_host;
        $this->cas_context = $cas_context;
        $this->cas_port = (int)$cas_port;
        $this->cas_server_ca_cert_path = $cas_server_ca_cert_path;
        $this->cas_server_validation = $cas_server_validation;

        $this->requestMatcher = new RequestMatcher();
        $request = $this->requestStack->getCurrentRequest();

        $this->cas_force_auth = false;
        if ($request) { // requestMatcher->matches fails if $request is null (in console mode)
            $this->requestMatcher->matchPath('(' . implode(')|(', $patterns_not_force_auth) . ')');
            $not_force_auth = $this->requestMatcher->matches($request);
            if (!$not_force_auth || ('(' . implode(')|(', $patterns_not_force_auth) . ')') == ("(^/)")) {
                $this->requestMatcher->matchPath('(' . implode(')|(', $patterns_force_auth) . ')');
                $this->cas_force_auth = $this->requestMatcher->matches($request);
            }
        }
        $major_pattern = array_merge($patterns_not_force_auth, $patterns_force_auth);
        if (empty($major_pattern))
            $major_pattern[] = "^/";
        $this->requestMatcher->matchPath('(' . implode(')|(', $major_pattern) . ')');

        $this->cas_curl_options = $cas_curl_options;

        $this->cas_debug = $cas_debug;
        $this->cas_restrict_ip = $cas_restrict_ip;
        $this->cas_version = "3.0";
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface::authenticate()
     */
    public function authenticate(TokenInterface $token)
    {
        $request = $this->requestStack->getCurrentRequest();

        // check if provided IP should be exluded to the auth process
        if (!empty($this->cas_restrict_ip) && !in_array(@$_SERVER['REMOTE_ADDR'], $this->cas_restrict_ip)) {
            throw new AuthenticationException(sprintf('Remote addresse %s is not allowed to authenticate through CAS.', @$_SERVER['REMOTE_ADDR']));
        }

        $logfile = $this->container->get('kernel')->getLogDir() . '/phpCas.log';

        if ($this->cas_debug) {
            // Enable debugging
            \phpCAS::setDebug($logfile);
        }
        // Initialize phpCAS - MUST be configured to proxify services
        \phpCAS::proxy(CAS_VERSION_3_0, $this->cas_host, $this->cas_port, $this->cas_context);
        if ($this->container->hasParameter('cas_callback_url')) {
            \phpCAS::setFixedCallbackURL($request->getSchemeAndHttpHost() . $request->getBaseUrl() . $this->container->getParameter('cas_callback_url'));
        }

        // Allow this client to be proxied
        if ($this->container->hasParameter('cas_proxy_chain')) {
            $proxy_chain = $this->container->getParameter('cas_proxy_chain');
            if (is_array($proxy_chain)) {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain($proxy_chain));
            } else {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain_Any());
            }
        } else {
            \phpCAS::allowProxyChain(new \CAS_ProxyChain_Any());
        }

        // For production use set the CA certificate that is the issuer of the cert
        // on the CAS server and uncomment the line below
        if ($this->cas_server_ca_cert_path != '') {
            \phpCAS::setCasServerCACert($this->cas_server_ca_cert_path);
        }

        // For quick testing you can disable SSL validation of the CAS server.
        // THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
        // VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
        if (false === $this->cas_server_validation) {
            \phpCAS::setNoCasServerValidation();
        }

        foreach ($this->cas_curl_options as $option) {
            if (defined($option['name'])) {
                \phpCAS::setExtraCurlOption(constant($option['name']), $option['value']);
            }
        }

        // enable single sign out capabilities
        \phpCAS::handleLogoutRequests(false);

        // logout if desired
        if (isset($_REQUEST['caslogout'])) {
            \phpCAS::logout();
        }

        if (true === $this->cas_force_auth) {
            // if user is not authenticated and we're coming from an Ajax call -> throw exception
            if ($request->isXmlHttpRequest() && false === \phpCAS::isAuthenticated()) {
                throw new AuthenticationException('(XmlHttpRequest) User is not authenticated.');
            }
            // force CAS authentication
            \phpCAS::forceAuthentication();
            $authenticated = true;
        } else {
            if ($request->attributes->get('_route') == "auth_connect") {
                $authenticated = \phpCAS::checkAuthentication();
            } else {
                $authenticated = \phpCAS::isAuthenticated();
            }
        }

        try {
            $this->container->get('prodige.configreader');
            $username = ($authenticated ? \phpCAS::getUser() : $this->container->getParameter('PRO_USER_INTERNET_USR_ID'));
            $em = $this->container->get('doctrine.orm.entity_manager');
            $em instanceof EntityManager;

            $user = $em->getRepository('CarmenApiBundle:Users')->findOneBy(array('userEmail' => $username));
            if (!$user) {
                $account = $em->getRepository('CarmenApiBundle:Account')->find(1);
                if (!$account) {
                    throw new AuthenticationException('Admincarto database is misconfigured: missing default account with id 1.');
                }
                $user = new Users();
                $user->setUserEmail($username)->setAccount($account);
                $em->persist($user);
                $em->flush();
            }
            if (!$user) {
                throw new AuthenticationException('Admincarto database is misconfigured: missing default user with id 1.');
            }
            $user->setUsername($username);

            // build a new token with the authenticated user
            if ($username == $this->container->getParameter('PRO_USER_INTERNET_USR_ID')) {
                $authenticatedToken = new CasUserToken(array('ROLE_ANONYMOUS'));
            } else {
                $authenticatedToken = new CasUserToken(array('ROLE_USER'));
            }
            $authenticatedToken->setUser($user);

            /*
            $dispatcher = $this->container->get('event_dispatcher');
            $attributes = array_merge($authenticated ? \phpCAS::getAttributes() : array(), array('usr_id'=> $authenticated ? \phpCAS::getUser() : null));
            $event = new AuthenticateUserEvent($authenticatedToken, $attributes);
            $dispatcher->dispatch(AuthenticateUserEvent::EVENT_NAME, $event);
            */

            // @since prodige4 - check if connected user has the rights to edit the map
            // load prodige parameters if not already done
            $rl = $this->container->get('prodige.request.listener');
            $rl instanceof \Prodige\ProdigeBundle\EventListener\RequestListener;
            $rl->onKernelRequest(new RequestEvent(
                $this->container->get('kernel'),
                $request,
                \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST
            ));

            // load prodige user instance
            $prodigeUser = new \Prodige\ProdigeBundle\Controller\User();
            $prodigeUser->setContainer($this->container);
            $prodigeUser->initUser($username);

            /*
            // get the map uuid : either from the request attributes, from GET, or from the session
            // WARNING : do not use Symfony $request->getSession because phpCas uses legacy $_SESSION
            $uuid = $request->attributes->get('uuid', $request->get('uuid', isset($_SESSION['uuid'])?$_SESSION['uuid']:null));
            if( $uuid ) {
                $_SESSION['uuid'] = $uuid;
                // récupérer l'ID de l'objet correspondant à l'uuid
                $conn = $this->container->get('doctrine')->getConnection(\Prodige\ProdigeBundle\Controller\BaseController::CONNECTION_CATALOGUE);
                $metadataId = $conn->fetchAll('select id from public.metadata where uuid = :uuid limit 1', array('uuid'=>$uuid));
                if(count($metadataId)>0) {
                    $metadataId = $metadataId[0]['id'];
                } else {
                    throw new AuthenticationException('Fiche de métadonnées inexistante : uuid='.$uuid);
                }

                // TODO @vlc vérifier l'application des droits avec BFE
                $serv = $this->container->get('prodige.verifyrights'); $serv instanceof \Prodige\ProdigeBundle\Controller\VerifyRightsController;
                $req = new \Symfony\Component\HttpFoundation\Request(array(
                    "ID"          => $metadataId,
                    "TRAITEMENTS" => "CMS",
                    "OBJET_TYPE"  => "service", // TODO @vlc vérifier avec BFE
                    "OBJET_STYPE" => "",
                ));
                $response = $serv->verifyRightsAction($req);

                if( ! $response instanceof \Symfony\Component\HttpFoundation\Response ) {
                    throw new AuthenticationException('Erreur lors de la vérification des droits : format de réponse erroné');
                }
                $rights = json_decode($response->getContent(), true);
                if(json_last_error()===JSON_ERROR_NONE) {
                    if( ! @$rights['CMS'] ) {
                        \Prodige\ProdigeBundle\Common\SecurityExceptions::throwAccessDeniedExceptionCarte();
                    }
                } else {
                    throw new AuthenticationException('Erreur lors de la vérification des droits : ' .  json_last_error_msg());
                }

            } else {
                throw new AuthenticationException('Paramètre manquant : uuid');
            }
            */

            $audit = $em->getRepository('CarmenApiBundle:UserAudit')->findOneBy(array('user' => $user));
            if (!$audit) {
                $audit = new UserAudit();
                $audit->setUser($user);
                $audit->setUserCreationDate(new \DateTime());
            }
            $audit->setUserLastConnectionDate(new \DateTime());
            $em->persist($audit);
            $em->flush();
            return $authenticatedToken;
        } catch (\Exception $ex) {
            throw new AuthenticationException($ex->getMessage());
        }

        throw new AuthenticationException('The CAS authentication failed.');
    }

    /**
     * Retreive a Proxy Granting Ticket for a specific service url
     * @param string $service the service to be proxied
     * @return mixed the ticket, or false
     * @throws Exception
     */
    public static function getPGT($service)
    {
        $err_code = $err_msg = null;
        try {
            return \phpCAS::retrievePT($service, $err_code, $err_msg);
        } catch (\Exception $e) {
            self::$_container->get('logger')->error(sprintf('%s::%s : [%s] %s', __CLASS__, __METHOD__, $err_code, $err_msg));
            throw $e;
        }
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface::supports()
     */
    public function supports(TokenInterface $token): bool
    {
        return $token instanceof CasUserToken;
//        return $token instanceof CasUserToken && $this->requestMatcher->matches($this->requestStack->getCurrentRequest());
    }

    /**
     * {@inheritDoc}
     * @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        self::$_container = $container;
    }

}