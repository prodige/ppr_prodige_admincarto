<?php

namespace Carmen\CasBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * Represents a custom token for CAS authentication.
 *
 * @author alkante <support@alkante.com>
 */
class CasUserToken extends AbstractToken
{
    /**
     * @param array $roles
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);
        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Security\Core\Authentication\Token\TokenInterface::getCredentials()
     */
    public function getCredentials()
    {
        return '';
    }
}
