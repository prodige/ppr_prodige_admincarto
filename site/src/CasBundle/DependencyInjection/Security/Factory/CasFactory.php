<?php

namespace Carmen\CasBundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Factory responsible for the definition and configuration of CAS Listener and Provider.
 *
 * @author alkante <support@alkante.com>
 */
class CasFactory implements SecurityFactoryInterface
{

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::create()
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $argIndex = 3;
        $providerId = 'security.authentication.provider.cas.' . $id;
        
        $container
            ->setDefinition($providerId, new ChildDefinition('cas.security.authentication.provider'))
            ->replaceArgument(0, new Reference($userProvider))
            ->replaceArgument($argIndex++, $config['host'])                  //3
            ->replaceArgument($argIndex++, $config['context'])
            ->replaceArgument($argIndex++, $config['port'])                  //5
            ->replaceArgument($argIndex++, $config['server_ca_cert_path'])
            ->replaceArgument($argIndex++, $config['server_validation'])     //7
            ->replaceArgument($argIndex++, $config['force_auth'])
            ->replaceArgument($argIndex++, $config['not_force_auth'])        //9
            ->replaceArgument($argIndex++, $config['curl_options'])
            ->replaceArgument($argIndex++, $config['debug'])                 //11
            ->replaceArgument($argIndex++, $config['restrict_ip']);

        $listenerId = 'security.authentication.listener.cas.' . $id;
        $container->setDefinition($listenerId, new ChildDefinition('cas.security.authentication.listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::getPosition()
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::getKey()
     */
    public function getKey()
    {
        return 'cas';
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::addConfiguration()
     */
    public function addConfiguration(NodeDefinition $node)
    {
        $node
            ->children()
            ->scalarNode('pattern')->defaultValue('^/')->end()
            ->scalarNode('host')->defaultValue('localhost')->end()
            ->scalarNode('context')->defaultValue('context')->end()
            ->scalarNode('port')->defaultValue(433)->end()
            ->scalarNode('server_ca_cert_path')->defaultValue('')->end()
            ->scalarNode('server_validation')->defaultTrue()->end()
            ->arrayNode('force_auth')
            ->info('List of route\'s patterns where the authentication is required')
            ->prototype('scalar')->end()
            ->end()
            ->arrayNode('not_force_auth')
            ->info('List of route\'s patterns where the authentication is not required')
            ->prototype('scalar')->end()
            ->end()
            ->scalarNode('debug')->defaultFalse()->end()
            ->arrayNode('curl_options')
            ->prototype('array')
            ->children()
            ->scalarNode('name')->end()
            ->scalarNode('value')->end()
            ->end()
            ->end()
            ->end()
            ->arrayNode('restrict_ip')
            ->prototype('scalar')->end()
            ->end();
    }

}