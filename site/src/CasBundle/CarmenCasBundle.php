<?php

namespace Carmen\CasBundle;

use Carmen\CasBundle\DependencyInjection\Security\Factory\CasFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle base class.
 * 
 * @author alkante <support@alkante.com>
 */
class CarmenCasBundle extends Bundle
{

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\HttpKernel\Bundle\Bundle::build()
     */
    public function build(ContainerBuilder $container)
    {
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new CasFactory());
    }

}

