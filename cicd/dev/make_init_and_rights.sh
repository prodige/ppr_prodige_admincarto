#!/bin/bash
#!/bin/sh
set -e

# Define usefull variables
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

# db - backup
mkdir -p ./data/db/backup
find $SCRIPTDIR/data/db/backup -type d -exec chmod 0755 {} \;
find $SCRIPTDIR/data/db/backup -type f -exec chmod 0644 {} \;

# ssl
find $SCRIPTDIR/data/web/ssl -type d -exec chmod 0755 {} \;
find $SCRIPTDIR/data/web/ssl -type f -exec chmod 0644 {} \;
