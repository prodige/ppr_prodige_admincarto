# Module PRODIGE administration de cartes

Module de conception de cartes interactives de PRODIGE, basé sur les technologies PHP/Symfony, Extjs et dépendant du module ppr_prodige_mapserver_api pour l'interaction avec les mapfiles (mapserver).

### configuration

Modifier le fichier site/app/config/global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- Pas d'accès direct, accès depuis le catalogue PRODIGE
